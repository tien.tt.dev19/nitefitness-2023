//
//  PlayerManager.swift
//  1SK
//
//  Created by vuongbachthu on 8/15/21.
//

import Foundation
import AVKit

protocol PlayerManagerDelegate: AnyObject {
    func onConfigAVPlayerTrigger(layer: AVPlayerLayer?)
    func onTimeObserverTrigger(currentTime: Double, duration: Double)
}

class PlayerManager {

    static var shared: PlayerManager?

    var isPlaying = false
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var currentTrack: Track?
    var playlistUrls: [URL]?
    var timeObserver: Any?
    var playbackRate: Float = 1 {
        didSet {
            self.avPlayer?.rate = self.playbackRate
        }
    }

    weak var delegate: PlayerManagerDelegate?

    func configAVPlayer(forController controller: UIViewController) {
        guard let urls = self.playlistUrls else { return }
        self.currentTrack = Track(url: urls[0], index: 0)
        self.avPlayer = AVPlayer()
        self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
        self.avPlayerLayer?.videoGravity = .resizeAspect
        self.prepareToPlay(forController: controller)
    }

    func continueAVPlayer(forController controller: UIViewController) {
        self.prepareToPlay(forController: controller)
    }

    func play() {
        guard let avPlayer = self.avPlayer, self.currentTrack?.state != .playedToTheEnd else { return }
        avPlayer.play()
        avPlayer.rate = self.playbackRate
    }

    func pause() {
        guard let avPlayer = self.avPlayer, self.currentTrack?.state != .playedToTheEnd else { return }
        avPlayer.pause()
    }

    func playNext(forController controller: UIViewController) {
        self.playbackRate = 1
        self.pause()
        self.currentTrack = self.getNextItem()
        self.prepareToPlay(forController: controller)
    }

    func playPrevious(forController controller: UIViewController) {
        self.playbackRate = 1
        self.pause()
        self.currentTrack = getPreviousItem()
        self.prepareToPlay(forController: controller)
    }

    func stopObserver(forController controller: UIViewController) {
        print("stopObserver PlayerManager")
        if let timeObserver = self.timeObserver {
            self.avPlayer?.removeTimeObserver(timeObserver)
            
            self.avPlayer?.currentItem?.removeObserver(controller, forKeyPath: #keyPath(AVPlayerItem.status))
            self.avPlayer?.currentItem?.removeObserver(controller, forKeyPath: AVConstant.loadedTimeRangesKey)
            
            self.timeObserver = nil
        }
    }

    // MARK: - Private function
    private func prepareToPlay(forController controller: UIViewController) {
        self.stopObserver(forController: controller)
        self.currentTrack?.playerItem.preferredPeakBitRate = 0
        self.avPlayer?.replaceCurrentItem(with: self.currentTrack?.playerItem)
        self.startObserver(forController: controller)
        self.delegate?.onConfigAVPlayerTrigger(layer: self.avPlayerLayer)
    }

    private func startObserver(forController controller: UIViewController) {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        guard let avPlayer = self.avPlayer, let durationCMTimeFormat = avPlayer.currentItem?.asset.duration else {
            return
        }
        let duration = CMTimeGetSeconds(durationCMTimeFormat)
        
        // Time observer
        self.timeObserver = avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: .main) { [weak self] time in
            self?.delegate?.onTimeObserverTrigger(currentTime: time.seconds, duration: Double(duration))
        }
        
        // Add KVO
        avPlayer.currentItem?.addObserver(controller,
                                   forKeyPath: #keyPath(AVPlayerItem.status),
                                   options: [.initial, .new],
                                   context: nil)
        
        avPlayer.currentItem?.addObserver(controller, forKeyPath: AVConstant.loadedTimeRangesKey, options: [.initial, .new], context: nil)
    }

    private func getNextItem() -> Track? {
        guard let urls = self.playlistUrls, let trackIndex = self.currentTrack?.index, trackIndex >= 0 else { return nil}
        let nextIndex = trackIndex + 1 >= urls.count ? 0 : trackIndex + 1
        return Track(url: urls[nextIndex], index: nextIndex)
    }

    private func getPreviousItem() -> Track? {
        guard let urls = self.playlistUrls, let trackIndex = self.currentTrack?.index, trackIndex >= 0 else { return nil }
        let previousIndex = trackIndex - 1 < 0 ? 0 : trackIndex - 1
        return Track(url: urls[previousIndex], index: previousIndex)
    }
}
