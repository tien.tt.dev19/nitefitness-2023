//
//  Network.swift
//  1SK
//
//  Created by Thaad on 02/03/2022.
//

import Foundation
import Alamofire

class Network {
    static var isConnected: Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
}
