//
//  CircleProgressViewScales.swift
//  1SKConnect
//
//  Created by Thaad on 22/07/2022.
//

import Foundation
import UIKit

protocol CircleProgressViewScalesDelegate: AnyObject {
    func onCountDownSuccess()
}

class CircleProgressViewScales: UIView {
    private var circleLayer = CAShapeLayer()
    private var progressLayer = CAShapeLayer()
    private var timer: Timer?

    private var startPoint = CGFloat(-Double.pi / 2)
    private var endPoint = CGFloat(3 * Double.pi / 2)

    weak var delegate: CircleProgressViewScalesDelegate?
    var timeInterval: TimeInterval = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func createCircularPath() {
        // created circularPath for circleLayer and progressLayer
        let circularPath = UIBezierPath(arcCenter: CGPoint(
                                            x: self.frame.size.width / 2.0,
                                            y: self.frame.size.height / 2.0),
                                        radius: (self.frame.size.height/2) - 10,
                                        startAngle: self.startPoint,
                                        endAngle: self.endPoint,
                                        clockwise: true)

        // circleLayer path defined to circularPath
        self.circleLayer.path = circularPath.cgPath

        // ui edits
        self.circleLayer.fillColor = UIColor.clear.cgColor
        self.circleLayer.lineCap = .round
        self.circleLayer.lineWidth = 25
        self.circleLayer.strokeEnd = 1.0
        self.circleLayer.strokeColor = UIColor.init(hex: "DEECEC").cgColor

        // added circleLayer to layer
        self.layer.addSublayer(self.circleLayer)

        // progressLayer path defined to circularPath
        self.progressLayer.path = circularPath.cgPath

        // ui edits
        self.progressLayer.fillColor = UIColor.clear.cgColor
        self.progressLayer.lineCap = .round
        self.progressLayer.lineWidth = 20
        self.progressLayer.strokeEnd = 0
        self.progressLayer.strokeColor = UIColor.init(hex: "00C2C5").cgColor

        // added progressLayer to layer
        self.layer.addSublayer(self.progressLayer)
    }

    func startProgress() {
        // created circularProgressAnimation with keyPath
        let circularProgressAnimation = CABasicAnimation(keyPath: "strokeEnd")

        // set the end time
        circularProgressAnimation.duration = self.timeInterval
        circularProgressAnimation.fromValue = 0.0
        circularProgressAnimation.toValue = 1.0
        circularProgressAnimation.fillMode = .forwards
        circularProgressAnimation.isRemovedOnCompletion = false

        self.progressLayer.add(circularProgressAnimation, forKey: "progressAnim")

        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
    }

    @objc func updateTime() {
        if self.timeInterval > 1 {
            self.timeInterval -= 1

        } else {
            self.timeInterval = 0
            self.timer?.invalidate()
            self.timer = nil
            self.delegate?.onCountDownSuccess()
        }
    }
}
