//
//  BluetoothManager+ICDeviceManger.swift
//  1SK
//
//  Created by Thaad on 27/12/2022.
//

import Foundation
import ICDeviceManager

protocol SKICDeviceManagerDelegate: AnyObject {
    func onInitFinish(_ bSuccess: Bool)
    func onBleState(_ state: ICBleState)
    func onDeviceConnectionChanged(_ device: ICDevice!, state: ICDeviceConnectState)
    func onReceiveSkipData(_ device: ICDevice!, data: ICSkipData!)
    func onScanResult(_ deviceInfo: ICScanDeviceInfo!)
    
    func onDidConnectedICDeviceSuccess(icScanDevice: ICScanDeviceInfo)
    func onDidTimeoutScanICDevice()
}

// MARK: ICDeviceManager
extension BluetoothManager {
    func setInitICDeviceManager() {
        print("<Bluetooth> IC Manager setInitICDeviceManager")
        ICDeviceManager.shared().initMgr()
        ICDeviceManager.shared().delegate = self
    }
    
    func setDeinitICDeviceManager() {
        print("<Bluetooth> IC Manager setDeinitICDeviceManager")
        ICDeviceManager.shared().deInit()
    }
    
    func setScanStartICDevice() {
        print("<Bluetooth> IC Manager setScanStartICDevice")
        ICDeviceManager.shared().scanDevice(self)
    }
    
    func setScanStopICDevice() {
        print("<Bluetooth> IC Manager setScanStopICDevice")
        ICDeviceManager.shared().stopScan()
    }
    
    func setConnectICDevice(icScanDevice: ICScanDeviceInfo) {
        print("<Bluetooth> IC Manager setConnectICDevice icScanDevice:", icScanDevice.macAddr ?? "NULL")
        
        let icDevice = ICDevice()
        icDevice.macAddr = icScanDevice.macAddr
        
        ICDeviceManager.shared().add(icDevice) { device, code in
            switch code {
            case .success:
                print("<Bluetooth> IC Manager setConnectICDevice .success")
                self.delegate_ICDeviceManager?.onDidConnectedICDeviceSuccess(icScanDevice: icScanDevice)
                
            default:
                print("<Bluetooth> IC Manager setConnectICDevice .failed")
            }
        }
        
        self.setScanStopICDevice()
    }
    
    func setDisconnectICDevice(mac: String?) {
        print("<Bluetooth> IC Manager setDisconnectICDevice mac:", mac ?? "Null")
        guard let macAddr = mac, macAddr != "" else {
            return
        }
        let icDevice = ICDevice()
        icDevice.macAddr = macAddr
        
        ICDeviceManager.shared().remove(icDevice) { device, code in
            switch code {
            case .success:
                print("<Bluetooth> IC Manager setDisconnectICDevice .success")
                
            default:
                print("<Bluetooth> IC Manager setDisconnectICDevice .failed")
            }
        }
    }
}

// MARK: - ICScanDeviceDelegate
extension BluetoothManager: ICDeviceManagerDelegate {
    func onInitFinish(_ bSuccess: Bool) {
        print("<Bluetooth> IC Manager onInitFinish:", bSuccess)
        self.delegate_ICDeviceManager?.onInitFinish(bSuccess)
    }
    
    func onBleState(_ state: ICBleState) {
        self.printICBleState(state: state)
        self.delegate_ICDeviceManager?.onBleState(state)
    }
    
    func onDeviceConnectionChanged(_ device: ICDevice!, state: ICDeviceConnectState) {
        switch state {
        case .connected:
            print("<Bluetooth> IC Manager onDeviceConnectionChanged .connected macAddr:", device.macAddr ?? "NULL")
            self.icDevice = device
            self.setUpdateStateDevice(state: .connected, uuid: device.macAddr)
            
        default:
            print("<Bluetooth> IC Manager onDeviceConnectionChanged .disconnected macAddr:", device.macAddr ?? "NULL")
            self.icDevice = nil
            self.setUpdateStateDevice(state: .disconnect, uuid: device.macAddr)
        }
        
        self.delegate_ICDeviceManager?.onDeviceConnectionChanged(device, state: state)
    }
    
    func onReceiveSkipData(_ device: ICDevice!, data: ICSkipData!) {
        print("<Bluetooth> IC Manager onReceiveSkipData macAddr:", device.macAddr ?? "NULL")
        self.delegate_ICDeviceManager?.onReceiveSkipData(device, data: data)
    }
}

// MARK: - ICScanDeviceDelegate
extension BluetoothManager: ICScanDeviceDelegate {
    func onScanResult(_ deviceInfo: ICScanDeviceInfo!) {
        //print("<Bluetooth> IC Manager onScanResult deviceInfo: \(deviceInfo.macAddr ?? "NULL") - name: \(deviceInfo.name ?? "NULL")")
        self.delegate_ICDeviceManager?.onScanResult(deviceInfo)
    }
}

// Debug
extension BluetoothManager {
    func printICBleState(state: ICBleState) {
        switch state {
        case .poweredOn:
            print("<Bluetooth> IC Manager onBleState state: .poweredOn")
            
        case .poweredOff:
            print("<Bluetooth> IC Manager onBleState state: .poweredOff")
            
        case .unauthorized:
            print("<Bluetooth> IC Manager onBleState state: .unauthorized")
            
        case .unknown:
            print("<Bluetooth> IC Manager onBleState state: .unknown")
            
        case .unsupported:
            print("<Bluetooth> IC Manager onBleState state: .unsupported")
            
        default:
            print("<Bluetooth> IC Manager onBleState state: .default")
        }
    }
}
