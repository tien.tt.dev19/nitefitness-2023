//
//  BluetoothManager.swift
//  1SK
//
//  Created by Thaad on 14/09/2022.
//

import Foundation
import CoreBluetooth
import ICDeviceManager

protocol BluetoothManagerDelegate: AnyObject {
    func onDidUpdateState(central: CBCentralManager)
    func onDidTimeoutScanPeripheralDevice()
    func onDidDiscover(peripheral: CBPeripheral)
    func onDidFailToConnect(peripheral: CBPeripheral)
    func onDidConnected(peripheral: CBPeripheral)
    func onDidDisconnect(peripheral: CBPeripheral)
}

// MARK: BluetoothManager
final class BluetoothManager: NSObject {
    static var shared = BluetoothManager()
    
    weak var delegate_BluetoothManager: BluetoothManagerDelegate?
    weak var delegate_ICDeviceManager: SKICDeviceManagerDelegate?
    
    var scalesNotifyUUIDs: [CBUUID] = ["FFF4"].map({ CBUUID(string: $0) })
    var scalesWriteUUIDs: [CBUUID] = ["FFF1"].map({ CBUUID(string: $0) })
    
    private var centralManager: CBCentralManager?
    private var timerScan: Timer?
    private var timeoutScan: TimeInterval = 0
    
    var listDeviceName: [String] = []
    
    /// CBPeripheral
    var peripheral: CBPeripheral?
    
    /// ICDevice
    var icDeviceConnect: ICScanDeviceInfo?
    var icDevice: ICDevice?
    
    // MARK: - Init
    override init() {
        super.init()
        self.setInitCBCentralManager()
        self.setInitICDeviceManager()
    }
    
    deinit {
        print("<Bluetooth> 1SK Manager deinit BluetoothManager")
        self.delegate_BluetoothManager = nil
        self.centralManager = nil
        self.setDeinitICDeviceManager()
    }
    
    func setInitManager() {
        print("<Bluetooth> 1SK Manager setInitManager")
    }
}

// MARK: - Handle
extension BluetoothManager {
    func setInitCBCentralManager() {
        print("<Bluetooth> 1SK Manager init BluetoothManager")
        self.centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: true])
    }
    
    func setScanStartPeripherals() {
        print("<Bluetooth> 1SK Manager setScanStartPeripherals")
        guard self.centralManager?.state == .poweredOn else {
            return
        }
        self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
        print("<Bluetooth> 1SK Manager setScanStartPeripherals scanning...")
    }
    
    func setScanStopPeripherals() {
        print("<Bluetooth> 1SK Manager setScanStopPeripherals")
        self.centralManager?.stopScan()
    }
    
    func setConnectDevice(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Manager setConnectDevice \(peripheral.mac) - \(peripheral.name ?? "Null")")
        self.centralManager?.connect(peripheral, options: nil)
    }
    
    func setDisconnectDevice(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Manager setDisconnectDevice mac:", peripheral.mac)
        self.centralManager?.cancelPeripheralConnection(peripheral)
    }
    
    func setCancelConnectDevice(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Manager setCancelConnectDevice mac:", peripheral.mac)
        self.centralManager?.cancelPeripheralConnection(peripheral)
    }
}

// MARK: Reaml DB
extension BluetoothManager {
    func setUpdateStateDevice(state: StateDevice, type: TypeDevice) {
        print("<Bluetooth> 1SK Manager setUpdateStateDevice state: \(state) - type: \(type)")
    }
    
    func setUpdateStateDevice(state: StateDevice, uuid: String) {
        print("<Bluetooth> 1SK Manager setUpdateStateDevice state: \(state) - uuid: \(uuid)")
    }
}

// MARK: Timer
extension BluetoothManager {
    func setStartTimer(timeout: TimeInterval) {
        print("<Bluetooth> 1SK Manager setStartTimer timeout:", timeout)

        self.timeoutScan = timeout
        self.timerScan = Timer.scheduledTimer(withTimeInterval: self.timeoutScan, repeats: false, block: { [weak self] (_) in
            self?.setStopTimer()
        })
    }
    
    func setStopTimer() {
        print("<Bluetooth> 1SK Manager setStopTimer")
        
        self.timerScan?.invalidate()
        self.timerScan = nil
        
        self.delegate_BluetoothManager?.onDidTimeoutScanPeripheralDevice()
        self.delegate_ICDeviceManager?.onDidTimeoutScanICDevice()
    }
}

// MARK: - State
extension BluetoothManager {
    func stateCBManager() -> CBManagerState? {
        print("<Bluetooth> 1SK Manager checkBluetoothStatusAvailble centralManager.state: ", self.centralManager?.state ?? "nil")
        return self.centralManager?.state
    }
    
    func isBluetoothStateConnect(peripheral: CBPeripheral) -> Bool {
        let isConnected = peripheral.state == .connected
        print("<Bluetooth> 1SK Manager isBluetoothStateConnect peripheral", " name: \(peripheral)", " - mac: \(peripheral.mac)", " - isConnected: \(isConnected)")
        return isConnected
    }
}

// MARK: - CBCentralManagerDelegate
extension BluetoothManager: CBCentralManagerDelegate {
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        self.printCentralState(central: central)
        
        self.delegate_BluetoothManager?.onDidUpdateState(central: central)
    }
    
    func centralManager(_ central: CBCentralManager, willRestoreState dict: [String: Any]) {
        print("<Bluetooth> 1SK Manager willRestoreState dict:", dict)
    }

    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
        guard let name = peripheral.name else { return }
        //print("<Bluetooth> 1SK Manager didDiscover name: \(name)")
        
        if name.contains("-6277b") || name.contains("-6277B") { // Check Device DBP-6277b
            let nameUppercased = name.uppercased()
            self.listDeviceName.forEach { deviceName in
                if nameUppercased.contains(deviceName) {
                    //print("<Bluetooth> 1SK Manager didDiscover listDeviceName: name: \(nameUppercase) - deviceName: \(deviceNameUppercase) - \(peripheral.mac)")
                    self.delegate_BluetoothManager?.onDidDiscover(peripheral: peripheral)
                }
            }
            
        } else {
            self.listDeviceName.forEach { deviceName in
                if name.contains(deviceName) {
                    //print("<Bluetooth> 1SK Manager didDiscover listDeviceName: name: \(nameUppercase) - deviceName: \(deviceNameUppercase) - \(peripheral.mac)")
                    self.delegate_BluetoothManager?.onDidDiscover(peripheral: peripheral)
                }
            }
        }
    }

    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Manager didConnect peripheral", " name: \(peripheral)", " - mac: \(peripheral.mac)")
        peripheral.discoverServices(nil)
        self.peripheral = peripheral
        self.delegate_BluetoothManager?.onDidConnected(peripheral: peripheral)
    }
    
    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
        print("<Bluetooth> 1SK Manager didFailToConnect peripheral: \(peripheral.name ?? "null") - error: \(error.debugDescription)")
        self.peripheral = nil
        self.delegate_BluetoothManager?.onDidFailToConnect(peripheral: peripheral)
    }

    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("<Bluetooth> 1SK Manager didDisconnectPeripheral peripheral", " name: \(peripheral.name ?? "NULL")", " - mac: \(peripheral.mac)")
        self.peripheral = nil
        self.delegate_BluetoothManager?.onDidDisconnect(peripheral: peripheral)
    }
    
    func centralManager(_ central: CBCentralManager, connectionEventDidOccur event: CBConnectionEvent, for peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Manager connectionEventDidOccur peripheral", " name: \(peripheral) - uuid: \(peripheral.mac)")
    }
    
    func centralManager(_ central: CBCentralManager, didUpdateANCSAuthorizationFor peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Manager didUpdateANCSAuthorizationFor peripheral", " name: \(peripheral) - uuid: \(peripheral.mac)")
    }
}

// MARK: Debug
extension BluetoothManager {
    func printCentralState(central: CBCentralManager) {
        switch central.state {
        case .unknown:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .unknown")

        case .resetting:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .resetting")

        case .unsupported:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .unsupported")

        case .unauthorized:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .unauthorized")

        case .poweredOff:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .poweredOff")

        case .poweredOn:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .poweredOn")

        @unknown default:
            print("<Bluetooth> 1SK Manager centralManagerDidUpdateState central.state \(central.state.rawValue) - .default")
        }
    }
    
}
