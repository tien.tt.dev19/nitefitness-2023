//
//  BluetoothManager.swift
//  1SK
//
//  Created by Thaad on 06/04/2022.
//

import Foundation
import CoreBluetooth

enum DeviceType: String {
    case bikeYesoul = "YESOUL"
    case skipping = "skipping"
    case other = "other"
    
    case scale
    case spO2
    case biolightBloodPressure
    case smartWatchS5
    case icJumpRope
}

protocol BluetoothManagerBikeDelegate: AnyObject {
    func onDidUpdateState(central: CBCentralManager)
    func onDidTimeoutScanDevice()
    func onDidDiscover(peripheral: CBPeripheral)
    func onDidFailToConnect(peripheral: CBPeripheral)
    func onDidConnected(peripheral: CBPeripheral)
    func onDidDisconnect(peripheral: CBPeripheral)
}

final class BluetoothManagerBike: NSObject {
    static var shared: BluetoothManagerBike?
    
    weak var delegate: BluetoothManagerBikeDelegate?
    private var listDeviceConnect = UserDefaults.standard.LIST_DEVICE_CONNECT
    private var centralManager: CBCentralManager?
    private var timerScan: Timer?
    private var timeoutScan: TimeInterval = 10
    
    var listPeripherals: [CBPeripheral] = []
    var deviceConnected: CBPeripheral?
    var deviceType: DeviceType = .other
    
    // MARK: - BluetoothManager Init
    override init() {
        super.init()
        print("<BLE> init BluetoothManager")
//        self.centralManager = CBCentralManager(delegate: self, queue: nil, options: [CBCentralManagerOptionShowPowerAlertKey: true])
    }
    
    deinit {
        self.delegate = nil
        self.centralManager = nil
    }
}

// MARK: - CBCentralManagerDelegate
//extension BluetoothManagerBike: CBCentralManagerDelegate {
//    func centralManagerDidUpdateState(_ central: CBCentralManager) {
//        print("<BLE> centralManagerDidUpdateState central:", central.state.rawValue)
//        switch central.state {
//        case .unknown:
//            print("<BLE> centralManagerDidUpdateState central.state is .unknown")
//
//        case .resetting:
//            print("<BLE> centralManagerDidUpdateState central.state is .resetting")
//
//        case .unsupported:
//            print("<BLE> centralManagerDidUpdateState central.state is .unsupported")
//
//        case .unauthorized:
//            print("<BLE> centralManagerDidUpdateState central.state is .unauthorized")
//
//        case .poweredOff:
//            print("<BLE> centralManagerDidUpdateState central.state is .poweredOff")
//
//        case .poweredOn:
//            print("<BLE> centralManagerDidUpdateState central.state is .poweredOn")
//
//        @unknown default:
//            print("<BLE> centralManagerDidUpdateState central.state is .default")
//        }
//
//        self.delegate?.onDidUpdateState(central: central)
//    }
//
//    func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
//        print("<BLE> didFailToConnect peripheral: \(peripheral.name ?? "null") - error: \(error.debugDescription)")
//        self.delegate?.onDidFailToConnect(peripheral: peripheral)
//    }
//
//    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String: Any], rssi RSSI: NSNumber) {
//        guard let name = peripheral.name else {
//            return
//        }
//        guard name.contains(self.deviceType.rawValue) else {
//            return
//        }
//        guard self.listPeripherals.first(where: {$0.uuid == peripheral.uuid}) == nil else {
//            self.delegate?.onDidDiscover(peripheral: peripheral)
//            return
//        }
//        print("<BLE> didDiscover peripheral:", name)
//        self.listPeripherals.append(peripheral)
//        self.delegate?.onDidDiscover(peripheral: peripheral)
//    }
//
//    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
//        print("<BLE> didConnect peripheral", " name: \(peripheral)", " - uuid: \(peripheral.uuid)")
//        peripheral.discoverServices(nil)
//        self.deviceConnected = peripheral
//        self.delegate?.onDidConnected(peripheral: peripheral)
//
//        if self.listDeviceConnect.first(where: {$0.uuid == peripheral.uuid}) == nil {
//            let device = DeviceSchema()
//            device.uuid = peripheral.uuid
//            device.name = peripheral.name
//            device.type = self.deviceType.rawValue
//
//            self.listDeviceConnect.insert(device, at: 0)
//            UserDefaults.standard.LIST_DEVICE_CONNECT = self.listDeviceConnect
//        }
//    }
//
//    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
//        print("<BLE> didDisconnectPeripheral peripheral", " name: \(peripheral)", " - uuid: \(peripheral.uuid)", " - error: \(error?.errorCode ?? 0) - \(error.debugDescription)")
//        self.listPeripherals.removeAll()
//        self.deviceConnected = nil
//        self.delegate?.onDidDisconnect(peripheral: peripheral)
//    }
//}

// MARK: - BluetoothManager Helper
extension BluetoothManagerBike {
    func setStartScanPeripherals() {
        print("<BLE> setStartScanPeripherals")
        guard self.centralManager?.state == .poweredOn else {
            return
        }
        self.centralManager?.scanForPeripherals(withServices: nil, options: nil)
    }
    
    func setStopScanPeripherals() {
        print("<BLE> setStopScanPeripherals")
        self.centralManager?.stopScan()
    }
    
    func setConnectDevice(peripheral: CBPeripheral) {
        print("<BLE> setConnectDevice uuid:", peripheral.uuid)
        self.centralManager?.connect(peripheral, options: nil)
    }
    
    func setDisconnectDevice(peripheral: CBPeripheral) {
        print("<BLE> setDisconnectDevice uuid:", peripheral.uuid)
        self.centralManager?.cancelPeripheralConnection(peripheral)
    }
    
    func setCancelConnectDevice(peripheral: CBPeripheral) {
        print("<BLE> setCancelConnectDevice uuid:", peripheral.uuid)
        self.centralManager?.cancelPeripheralConnection(peripheral)
    }
}

// MARK: - BluetoothManager State
extension BluetoothManagerBike {
    func state() -> CBManagerState? {
        print("<BLE> checkBluetoothStatusAvailble centralManager.state: ", self.centralManager?.state ?? "nil")
        return self.centralManager?.state
    }
    
    func isBluetoothStateConnect(peripheral: CBPeripheral) -> Bool {
        let isConnected = peripheral.state == .connected
        print("<BLE> isBluetoothStateConnect peripheral", " name: \(peripheral)", " - uuid: \(peripheral.uuid)", " - isConnected: \(isConnected)")
        return isConnected
    }
}

// MARK: Timer Scan
extension BluetoothManagerBike {
    func setStartTimer(timeout: TimeInterval) {
        print("<BLE> setStartTimer timeout:", timeout)
        
        self.timeoutScan = timeout
        self.timerScan = Timer.scheduledTimer(withTimeInterval: self.timeoutScan, repeats: false, block: { [weak self] (_) in
            self?.setStopTimer()
        })
    }
    
    func setStopTimer() {
        print("<BLE> setStopTimer")
        self.timerScan?.invalidate()
        self.timerScan = nil
        self.delegate?.onDidTimeoutScanDevice()
    }
}

// MARK: DeviceSchema
class DeviceSchema: Codable {
    var uuid: String?
    var name: String?
    var type: String?
    
    init() {
        //
    }
}
