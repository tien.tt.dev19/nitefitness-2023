//
//  CBPeripheral.swift
//  1SK
//
//  Created by Thaad on 06/04/2022.
//

import Foundation
import CoreBluetooth

extension CBPeripheral {
    var uuid: String {
        return identifier.uuidString
    }
    
    var mac: String {
        return self.uuid.suffix(12).pairs.joined(separator: ":")
    }
    
    var paired: Bool {
        let device: DeviceRealm? = gRealm.objects().first(where: {$0.uuid == self.uuid})
        if device != nil {
            return true
        } else {
            return false
        }
    }
}

extension Collection {
    func unfoldSubSequences(limitedTo maxLength: Int) -> UnfoldSequence<SubSequence, Index> {
        sequence(state: startIndex) { start in
            guard start < self.endIndex else { return nil }
            let end = self.index(start, offsetBy: maxLength, limitedBy: self.endIndex) ?? self.endIndex
            defer { start = end }
            return self[start..<end]
        }
    }

    func every(n: Int) -> UnfoldSequence<Element, Index> {
        sequence(state: startIndex) { index in
            guard index < endIndex else { return nil }
            defer { index = self.index(index, offsetBy: n, limitedBy: endIndex) ?? endIndex }
            return self[index]
        }
    }

    var pairs: [SubSequence] { .init(self.unfoldSubSequences(limitedTo: 2)) }
}
