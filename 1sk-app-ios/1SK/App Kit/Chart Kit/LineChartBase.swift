//
//  LineChartBase.swift
//  1SK
//
//  Created by Thaad on 31/10/2022.
//

import Foundation

struct VerticalGestureArea {
    let from: CGFloat
    let to: CGFloat
}

struct AreaGesture {
    let point: CGPoint
    let size: CGSize
}

// MARK: Binary Integer
extension BinaryInteger {
    var isEven: Bool { isMultiple(of: 2) }
    var isOdd: Bool { !isEven }
}
