//
//  FileHelper.swift
//  1SK
//
//  Created by vuongbachthu on 7/7/21.
//

import Foundation
import Alamofire

class FileHelper {
    static let shared = FileHelper()
    
    // MARK: Download File Video
    func onDownloadVideoExercise(urlFile: String, workoutId: Int, completion: @escaping(_ success: Bool, _ message: String?, _ error: Int?, _ progress: Int64?, _ fileURL: URL?) -> Void) {
        
        let destination: DownloadRequest.Destination = {(_ url, response) in
            let docmentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let suggestedFilename = response.suggestedFilename == nil ? "video.mp4" : response.suggestedFilename!
            let filePath = String(format: "%@/%@/%@", "workout", String(workoutId), suggestedFilename)
            let fileURL = docmentURL?.appendingPathComponent(filePath, isDirectory: true)
            return (fileURL!, [.removePreviousFile, .createIntermediateDirectories])
        }
          
        if let url = URL(string: urlFile) {
            AF.download(url, to: destination).downloadProgress {progress in
                
                let completedUnitCount = progress.completedUnitCount
                print("onDownloadFile: completedUnitCount ", completedUnitCount)
                
//                let completedUnitCount = progress.completedUnitCount
//                let completedUnitCount = progress.completedUnitCount
//                let completedUnitCount = progress.completedUnitCount
//                let completedUnitCount = progress.completedUnitCount
                
                
                let value = progress.completedUnitCount
                completion(false, "Loading", nil, value, nil)
                
//                let value = progress.fractionCompleted * 100
//                print("onDownloadFile: progress: ", Float(value.rounded(toPlaces: 1)))
//                completion(false, "Loading", nil, Float(value.rounded(toPlaces: 1)), nil)
                
            }.response { response in
                switch response.result {
                case .success(let fileURL):
                    print("onDownloadFile: success: ", fileURL ?? "nil")
                    completion(true, "Successfully", 0, nil, fileURL)
                
                case .failure(let error):
                    print("onDownloadFile: failure: \(error.responseCode ?? -1) - \(error.errorDescription ?? "nil")")
                    completion(false, error.errorDescription, error.responseCode, nil, nil)
                }
            }
        }
    }
    
    // MARK: Download File Audio
    func onDownloadAudioExercise(urlFile: String, workoutId: Int, completion: @escaping(_ success: Bool, _ message: String?, _ error: Int?, _ progress: Float?, _ fileURL: URL?) -> Void) {
        
        let destination: DownloadRequest.Destination = {(_ url, _) in
            let docmentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
            let fileName = String(format: "%@-audio.aac", String(workoutId))
            let filePath = String(format: "%@/%@/%@", "workout", String(workoutId), fileName)
            let fileURL = docmentURL?.appendingPathComponent(filePath, isDirectory: true)
            return (fileURL!, [.removePreviousFile, .createIntermediateDirectories])
        }
          
        if let url = URL(string: urlFile) {
            AF.download(url, to: destination).downloadProgress {progress in
                
                let value = progress.fractionCompleted * 100
                print("onDownloadFile: progress: ", Float(value.rounded(toPlaces: 1)))
                completion(false, "Loading", nil, Float(value.rounded(toPlaces: 1)), nil)
                
            }.response { response in
                switch response.result {
                case .success(let fileURL):
                    print("onDownloadFile: success: ", fileURL ?? "nil")
                    completion(true, "Successfully", 0, 100.0, fileURL)
                
                case .failure(let error):
                    print("onDownloadFile: failure: \(error.responseCode ?? -1) - \(error.errorDescription ?? "nil")")
                    completion(false, error.errorDescription, error.responseCode, nil, nil)
                }
            }
        }
    }
    
    // FileHelper.shared.printContentsOfDocumentDirectory(title: "Title:", path: "/workout/\(self.workout.id)/")
    func printContentsOfDocumentDirectory(title: String?, path: String) {
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDir = dirPaths[0].path
        
        do {
            let filePath = String(format: "%@%@", docsDir, path)
            let fileList = try FileManager.default.contentsOfDirectory(atPath: filePath)
            for filename in fileList {
                //print("\(title ?? "")/.documents\(path)", filename)
                print("\(title ?? "") \(filePath)\(filename)")
            }
            print("\(title ?? ""): ------------------------------")
            
        } catch let err {
            print("\(title ?? "")/.documents/\(path): Error - ", err.localizedDescription)
        }
    }
    
    func getFileInDirectory(path: String) -> [String]? {
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDir = dirPaths[0].path
        
        do {
            let filePath = String(format: "%@%@", docsDir, path)
            let fileList = try FileManager.default.contentsOfDirectory(atPath: filePath)
            return fileList
            
        } catch let err {
            print("getFileInDirectory Error - ", err.localizedDescription)
            return nil
        }
    }

    func getUrlsVideoWorkout(workoutId: String) -> [URL]? {
        let fileManager = FileManager.default
        let dirPaths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDir = dirPaths[0].path
        
        do {
            let filePath = String(format: "%@/%@/%@/", docsDir, "workout", workoutId)
            
            if let filePathURL = URL(string: filePath) {
                let fileURLs = try fileManager.contentsOfDirectory(at: filePathURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants)
                
                for fileUrl in fileURLs {
                    print("/.documents/workout/\(workoutId)", fileUrl)
                }
                return fileURLs
                
            } else {
                print("/.documents/workout/\(workoutId): Error - filePathURL Null")
                return nil
            }
            
        } catch let err {
            print("/.documents/workout/\(workoutId): Error -", err.localizedDescription)
            return nil
        }
    }
    
    func getUrlFile(workoutId: Int, fileName: String) -> URL? {
        let fileManager = FileManager.default
        let dirPaths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDir = dirPaths[0].path
        
        let filePath = String(format: "%@/%@/%@/%@", docsDir, "workout", String(workoutId), fileName)
        //print("getUrlFile: filePath:", filePath)
        
        if let filePathURL = URL(string: filePath) {
            let fileExists = fileManager.fileExists(atPath: filePathURL.path)
            if fileExists == true {
                return filePathURL
                
            } else {
                print("getUrlFile: Not fileExists")
                return nil
            }
            
        } else {
            print("getUrlFile: Not filePathURL")
            return nil
        }
    }
    
    func getBinaryFileContent(for path: String) -> Data? {
        let fileURL = URL(fileURLWithPath: path)
        do {
            // Get the raw data from the file.
            let rawData: Data = try Data(contentsOf: fileURL)
            return rawData
        } catch {
            return nil
        }
    }
    
    func setRemoveDirectory(path: String) {
        guard let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.path else { return }
        let filePath = String(format: "%@%@", directory, path)
        do {
            try FileManager.default.removeItem(atPath: filePath)
            print("setRemoveDirectory Success:", filePath)
            
        } catch let error {
            print("setRemoveDirectory error ", error.localizedDescription)
        }
    }
    
    func setRemoveVideo(path: String) {
        let fileManager = FileManager.default
        let dirPaths = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let docsDir = dirPaths[0].path
        
        let filePath = String(format: "%@/%@", docsDir, path)
        print("setRemoveDirectory filePath:", filePath)
        do {
            try fileManager.removeItem(atPath: filePath)
            print("setRemoveDirectory Success")
            
        } catch let error {
            print("setRemoveDirectory error ", error.localizedDescription)
        }
    }
}
