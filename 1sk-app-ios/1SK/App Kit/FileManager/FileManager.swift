//
//  FileManager.swift
//  1SK
//
//  Created by Thaad on 15/03/2022.
//

import Foundation

class Shared {
    static let instance = Shared()
    
    private init() { }
    
    var cartProductSchema: [CartProductSchema] {
        get {
            print("<Schema> Get File CartProductSchema")
            guard let data = try? Data(contentsOf: .cartProductSchema) else { return [] }
            return (try? JSONDecoder().decode([CartProductSchema].self, from: data)) ?? []
        }
        set {
            print("<Schema> Set File CartProductSchema")
            try? JSONEncoder().encode(newValue).write(to: .cartProductSchema)
        }
    }
    
    //Usage:
    //Shared.instance.domainSchemas = [.init(domain: "a", schema: "b"), .init(domain: "c", schema: "d")]
    //Shared.instance.domainSchemas  // [{domain "a", schema "b"}, {domain "c", schema "d"}]
    // https://stackoverflow.com/questions/63367953/storing-array-of-custom-objects-in-userdefaults
}

extension URL {
    static var cartProductSchema: URL {
        let applicationSupport = FileManager.default.urls(for: .applicationSupportDirectory, in: .userDomainMask).first!
        let bundleID = Bundle.main.bundleIdentifier ?? "OneSK Store"
        let subDirectory = applicationSupport.appendingPathComponent(bundleID, isDirectory: true)
        try? FileManager.default.createDirectory(at: subDirectory, withIntermediateDirectories: true, attributes: nil)
        return subDirectory.appendingPathComponent("CartProductSchema.json")
    }
}
