//
//  AnalyticsHelper.swift
//  1SK
//
//  Created by Thaad on 07/02/2023.
//

import Foundation
import FirebaseAnalytics

// MARK: AnalyticsEvent
enum AnalyticsEvent {
//    case APP_OPEN_FIRST
//    case APP_OPEN_NEXT
//    case APP_OPEN_LOGGED
//    case APP_OPEN_NONE
    
    case USER_SIGN_IN
    case USER_SIGN_UP
    case USER_SIGN_OUT
    case USER_DELETE_ACCOUNT
    
    var name: String {
        switch self {
//        case .APP_OPEN_FIRST:
//            return "sk_app_open_first" // Mở app lần đầu tiên sau khi tải
//        case .APP_OPEN_NEXT:
//            return "sk_app_open_next" // Mở app từ dưới nền (app ở trạng thái nền được mở trở lại)
//        case .APP_OPEN_LOGGED:
//            return "sk_app_open_logged" // Mở app đã đăng nhập
//        case .APP_OPEN_NONE:
//            return "sk_app_open_none" // Mở app chưa đăng nhập
            
        case .USER_SIGN_IN:
            return "sk_user_sign_in" // Đăng nhập thành công
        case .USER_SIGN_UP:
            return "sk_user_sign_up" // Đăng ký tài khoản thành công
        case .USER_SIGN_OUT:
            return "sk_user_sign_out" // Đăng xuất thành công
        case .USER_DELETE_ACCOUNT:
            return "sk_user_delete_account" // Người dùng xóa tài khoản
        }
    }
}

// MARK: AnalyticsHelper
final class AnalyticsHelper {
    class func setEvent(with type: AnalyticsEvent, parameters: [String: Any]? = nil) {
        print("<AnalyticsHelper> logEvent:", type.name)
        Analytics.logEvent(type.name, parameters: parameters)
    }
    
    class func setUserID(with id: Int?) {
        guard let _id = id else {
            return
        }
        Analytics.setUserID("\(_id)")
    }
    
    private class func analyticsUser(id: String, name: String, phone: String, email: String) -> [String: Any] {
        let parameters = [
            AnalyticsParameterKey.User.id.key: id,
            AnalyticsParameterKey.User.name.key: name,
            AnalyticsParameterKey.User.phone.key: phone,
            AnalyticsParameterKey.User.email.key: email
        ]
        
        return parameters
    }
}

// MARK: AnalyticsParameterKey
enum AnalyticsParameterKey {
    enum User {
        case id
        case name
        case phone
        case email
        
        var key: String {
            switch self {
            case .id:
                return "user_id"
            case .name:
                return "user_name"
            case .phone:
                return "user_phone"
            case .email:
                return "user_email"
            }
        }
    }
}
