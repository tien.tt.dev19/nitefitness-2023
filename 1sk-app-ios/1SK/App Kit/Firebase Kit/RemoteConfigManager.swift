//
//  RemoteConfigManager.swift
//  1SK
//
//  Created by vuongbachthu on 10/30/21.
// https://viblo.asia/p/su-dung-firebase-remote-config-trong-ios-Qbq5QJ7XKD8

import Foundation
import FirebaseRemoteConfig

// MARK: Enum RemoteConfigKey
enum RemoteConfigKey: String {
    case version = "version_ios"
    case baseUrl = "base_url"
    case settings = "app_settings_ios"
    case warning = "warning_ios"
    case appLinks = "app_links"
    
    case shareFitnessWorkout = "share_fitness_workout"
    case shareFitnessVideo = "share_fitness_video"
    case shareFitnessPost = "share_fitness_post"
    
    case shareCareDoctor = "share_care_doctor"
    case shareCareVideo = "share_care_video"
    case shareCarePost = "share_care_post"
}

// MARK: Init RemoteConfig
struct RemoteConfigManager {
    static let shared = RemoteConfigManager()
    
    fileprivate var remoteConfig: RemoteConfig

    fileprivate init() {
        self.remoteConfig = RemoteConfig.remoteConfig()
    }
    
    func initRemoteConfig() {
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = 0
        self.remoteConfig.configSettings = settings
    }
    
    func fetchRemoteConfig(completion: @escaping(_ success: Bool, _ error: Error?) -> Void) {
        self.remoteConfig.fetch { (status, error) -> Void in
            switch status {
            case .success:
                self.remoteConfig.activate { changed, error in
                    guard error == nil else {
                        completion(false, error)
                        return
                    }
                    
                    self.setConfigVersion()
                    self.setConfigBaseUrl()
                    self.setConfigWarning()
                    self.setConfigSettings()
                    self.setConfigAppLinks()
                    self.setConfigAppShare()
                    
                    completion(true, nil)
                }
                
            case .failure:
                completion(false, error)
                
            case .noFetchYet:
                completion(false, error)
                
            case .throttled:
                completion(false, error)
                
            default:
                completion(false, error)
            }
        }
    }
}

// MARK: Get Value
extension RemoteConfigManager {
    private func getValueString(fromKey key: RemoteConfigKey) -> String? {
        return self.remoteConfig.configValue(forKey: key.rawValue).stringValue
    }

    private func getValueInt(fromKey key: RemoteConfigKey) -> Int? {
        return self.remoteConfig.configValue(forKey: key.rawValue).numberValue.intValue
    }

    private func getValueBool(fromKey key: RemoteConfigKey) -> Bool? {
        return self.remoteConfig.configValue(forKey: key.rawValue).boolValue
    }
    
    private func getValueJson(fromKey key: RemoteConfigKey) -> [String: Any]? {
        return self.remoteConfig.configValue(forKey: key.rawValue).jsonValue as? [String: Any]
    }
    
}


extension RemoteConfigManager {
    // MARK: setConfigVersion
    private func setConfigVersion() {
        if let version = RemoteConfigManager.shared.getValueJson(fromKey: .version) {
            gVersion.minVersion = version["min_version"] as? String
            gVersion.latestVersion = version["latest_version"] as? String
            gVersion.isReviewAppstore = version["is_review_appstore"] as? Bool
            
            /// Version Min
            if let listMinVersion = gVersion.minVersion?.split(separator: ".") {
                var version = ""
                for item in listMinVersion {
                    if item.count > 1 {
                        version = "\(version)\(item)"
                    } else {
                        version = "\(version)0\(item)"
                    }
                }
                gVersion.minVersionNumber = Int(version)
            }
            
            /// Version Latest
            if let listLatestVersion = gVersion.latestVersion?.split(separator: ".") {
                var version = ""
                for item in listLatestVersion {
                    if item.count > 1 {
                        version = "\(version)\(item)"
                    } else {
                        version = "\(version)0\(item)"
                    }
                }
                gVersion.latestVersionNumber = Int(version)
            }
            
            /// Version System
            let listSystemVersion = APP_VERSION.split(separator: ".")
            var version = ""
            for item in listSystemVersion {
                if item.count > 1 {
                    version = "\(version)\(item)"
                } else {
                    version = "\(version)0\(item)"
                }
            }
            gVersion.systemVersionNumber = Int(version)
            
            print("<version> minVersion: \(gVersion.minVersion ?? "null")")
            print("<version> latestVersion: \(gVersion.latestVersion ?? "null")")
            print("<version> systemVersion: \(APP_VERSION)")
            print("<version> -------------")
            print("<version> minVersionNumber: \(gVersion.minVersionNumber ?? 0)")
            print("<version> latestVersionNumber: \(gVersion.latestVersionNumber ?? 0)")
            print("<version> systemVersionNumber: \(gVersion.systemVersionNumber ?? 0)")
        }
    }
    
    // MARK: setConfigBaseUrl
    private func setConfigBaseUrl() {
        if let baseUrl = RemoteConfigManager.shared.getValueJson(fromKey: .baseUrl) {
            switch APP_ENV {
            case .DEV:
                AUTH_SERVICE_URL = "https://id-dev.1sk.vn/api"
                FITNESS_SERVICE_URL = "https://fitness-dev.1sk.vn/api/v1.1"
                CARE_SERVICE_URL = "https://care-dev.1sk.vn/api/v1.0"
                STORE_SERVICE_URL = "https://ecom-dev.1sk.vn/api/v1.0"
                CONNECT_SERVICE_URL = "https://dev-connect.1sk.vn/api/v1.2"
//                WORDPRESS_SERVICE_URL = "https://1sk.vn/song-khoe/wp-json"
                UPLOAD_IMAGE_URL = "https://static.1sk.vn:8403/v1.0"
                
                SOCKET_ID_URL = "https://echo-id-dev.1sk.vn"
                SOCKET_FITNESS_URL = "https://echo-fitness-dev.1sk.vn"
                SOCKET_CARE_URL = "https://echo-care-dev.1sk.vn"
                SOCKET_TIMER_URL = "https://care-timer-dev.1sk.vn"
                
                SHARE_URL = "https://dev-web.1sk.vn/"
                LANDING_FITNESS_URL = "https://1sk.vn/fit"
                LANDING_STORE_URL = "https://1sk.vn/store"
                GUIDE_CONNECTING_STRAVA_URL = "https://ecom-dev.1sk.vn/song-khoe/huong-dan-dang-ky-tai-khoan-strava-100636"
                GUIDE_TRACKING_STRAVA_URL = "https://ecom-dev.1sk.vn/song-khoe/huong-dan-tracking-bang-ung-dung-strava-100638"
                PRODUCT_SCALES_URL = "https://1sk.vn/cua-hang/can-suc-khoe-thong-minh-1sk-p153"
                PRODUCT_BP_URL = "https://1sk.vn/cua-hang/may-do-huyet-ap-bap-tay-jumper-jpd-ha120-bluetooth-p180"
                
            case .PRO:
                if let auth_service_url = baseUrl["auth_service_url"] as? String {
                    AUTH_SERVICE_URL = auth_service_url
                }
                    
                if let fitness_service_url = baseUrl["fitness_service_url"] as? String {
                    FITNESS_SERVICE_URL = fitness_service_url
                }
                
                if let care_service_url = baseUrl["care_service_url"] as? String {
                    CARE_SERVICE_URL = care_service_url
                }
                
                if let store_service_url = baseUrl["store_service_url"] as? String {
                    STORE_SERVICE_URL = store_service_url
                }
                
                if let connect_service_url = baseUrl["connect_service_url"] as? String {
                    CONNECT_SERVICE_URL = connect_service_url
                }
                
                if let socket_id_url = baseUrl["socket_id_url"] as? String {
                    SOCKET_ID_URL = socket_id_url
                }
                
                if let socket_fitness_url = baseUrl["socket_fitness_url"] as? String {
                    SOCKET_FITNESS_URL = socket_fitness_url
                }
                
                if let socket_care_url = baseUrl["socket_care_url"] as? String {
                    SOCKET_CARE_URL = socket_care_url
                }
                
                if let socket_timer_url = baseUrl["socket_timer_url"] as? String {
                    SOCKET_TIMER_URL = socket_timer_url
                }
                
                if let upload_image_url = baseUrl["upload_image_url"] as? String {
                    UPLOAD_IMAGE_URL = upload_image_url
                }
                
                if let landing_page_fitness_url = baseUrl["landing_page_fitness_url"] as? String {
                    LANDING_FITNESS_URL = landing_page_fitness_url
                }
                
                if let landing_page_store_url = baseUrl["landing_page_store_url"] as? String {
                    LANDING_STORE_URL = landing_page_store_url
                }
                
                if let share_url = baseUrl["share_url"] as? String {
                    SHARE_URL = share_url
                }
                
                if let guide_connecting_strava = baseUrl["guide_connecting_strava"] as? String {
                    GUIDE_CONNECTING_STRAVA_URL = guide_connecting_strava
                }
                
                if let guide_tracking_strava = baseUrl["guide_tracking_strava"] as? String {
                    GUIDE_TRACKING_STRAVA_URL = guide_tracking_strava
                }
                
                if let scale_1sk_url = baseUrl["scale_1sk_url"] as? String {
                    PRODUCT_SCALES_URL = scale_1sk_url
                }
                
                if let blood_pressure_url = baseUrl["blood_pressure_url"] as? String {
                    PRODUCT_BP_URL = blood_pressure_url
                }
            }
        }
    }
    
    // MARK: setConfigWarning
    private func setConfigWarning() {
        if let warning = RemoteConfigManager.shared.getValueJson(fromKey: .warning) {
            gWarning.title = warning["title"] as? String
            gWarning.content = warning["content"] as? String
            gWarning.isRequired = warning["isRequired"] as? Bool
            gWarning.isEnable = warning["isEnable"] as? Bool
            
        }
    }
    
    // MARK: setConfigSettings
    private func setConfigSettings() {
        if let settings = RemoteConfigManager.shared.getValueJson(fromKey: .settings) {
            gAppSettings.enableAgoraArea = settings["enable_agora_area"] as? Bool
            gAppSettings.enableTwilioArea = settings["enable_twilio_area"] as? Bool
            gAppSettings.isRequiredVerifyPhone = settings["is_required_verify_phone"] as? Bool
            
            gAppSettings.isHiddenHomeVR = settings["is_hidden_home_vr"] as? Bool
            gAppSettings.isHiddenHomeCare = settings["is_hidden_home_care"] as? Bool
            gAppSettings.isHiddenHomeScales = settings["is_hidden_home_scales"] as? Bool
            gAppSettings.isHiddenHomeBP = settings["is_hidden_home_bp"] as? Bool
            gAppSettings.isHiddenHomeWatch = settings["is_hidden_home_watch"] as? Bool
            gAppSettings.isHiddenHomeHealth = settings["is_hidden_home_health"] as? Bool
        }
    }
    
    // MARK: setConfigAppLinks
    private func setConfigAppLinks() {
        if let appLinks = RemoteConfigManager.shared.getValueJson(fromKey: .appLinks) {
            gAppLinks.appIosBundleID = appLinks["app_ios_bundle_id"] as? String
            gAppLinks.appIosStoreID = appLinks["app_ios_store_id"] as? String
            gAppLinks.appIosStoreURL = appLinks["app_ios_store_url"] as? String
            gAppLinks.appIosMinVersion = appLinks["app_ios_min_version"] as? String
            
            gAppLinks.appAndroidPackageName = appLinks["app_android_package_name"] as? String
            gAppLinks.appAndroidStoreURL = appLinks["app_android_store_url"] as? String
            gAppLinks.appAndroidMinVersion = appLinks["app_android_min_version"] as? Int
        }
    }
    
    // MARK: setConfigAppShare
    private func setConfigAppShare() {
        if let shareFitnessWorkout = RemoteConfigManager.shared.getValueJson(fromKey: .shareFitnessWorkout) {
            gAppShare.fitnessWorkout?.itunesProviderToken = shareFitnessWorkout["itunes_provider_token"] as? String
            gAppShare.fitnessWorkout?.itunesCampaignToken = shareFitnessWorkout["itunes_campaign_token"] as? String
            
            gAppShare.fitnessWorkout?.analyticsSource = shareFitnessWorkout["analytics_source"] as? String
            gAppShare.fitnessWorkout?.analyticsMedium = shareFitnessWorkout["analytics_medium"] as? String
            gAppShare.fitnessWorkout?.analyticsCampaign = shareFitnessWorkout["analytics_campaign"] as? String
            
            gAppShare.fitnessWorkout?.isRequired = shareFitnessWorkout["isRequired"] as? Bool
        }
        
        if let shareFitnessVideo = RemoteConfigManager.shared.getValueJson(fromKey: .shareFitnessVideo) {
            gAppShare.fitnessVideo?.itunesProviderToken = shareFitnessVideo["itunes_provider_token"] as? String
            gAppShare.fitnessVideo?.itunesCampaignToken = shareFitnessVideo["itunes_campaign_token"] as? String
            
            gAppShare.fitnessVideo?.analyticsSource = shareFitnessVideo["analytics_source"] as? String
            gAppShare.fitnessVideo?.analyticsMedium = shareFitnessVideo["analytics_medium"] as? String
            gAppShare.fitnessVideo?.analyticsCampaign = shareFitnessVideo["analytics_campaign"] as? String
            
            gAppShare.fitnessVideo?.isRequired = shareFitnessVideo["isRequired"] as? Bool
        }
        
        if let shareFitnessPost = RemoteConfigManager.shared.getValueJson(fromKey: .shareFitnessPost) {
            gAppShare.fitnessPost?.itunesProviderToken = shareFitnessPost["itunes_provider_token"] as? String
            gAppShare.fitnessPost?.itunesCampaignToken = shareFitnessPost["itunes_campaign_token"] as? String
            
            gAppShare.fitnessPost?.analyticsSource = shareFitnessPost["analytics_source"] as? String
            gAppShare.fitnessPost?.analyticsMedium = shareFitnessPost["analytics_medium"] as? String
            gAppShare.fitnessPost?.analyticsCampaign = shareFitnessPost["analytics_campaign"] as? String
            
            gAppShare.fitnessPost?.isRequired = shareFitnessPost["isRequired"] as? Bool
        }
        
        if let shareCareDoctor = RemoteConfigManager.shared.getValueJson(fromKey: .shareCareDoctor) {
            gAppShare.careDoctor?.itunesProviderToken = shareCareDoctor["itunes_provider_token"] as? String
            gAppShare.careDoctor?.itunesCampaignToken = shareCareDoctor["itunes_campaign_token"] as? String
            
            gAppShare.careDoctor?.analyticsSource = shareCareDoctor["analytics_source"] as? String
            gAppShare.careDoctor?.analyticsMedium = shareCareDoctor["analytics_medium"] as? String
            gAppShare.careDoctor?.analyticsCampaign = shareCareDoctor["analytics_campaign"] as? String
            
            gAppShare.careDoctor?.isRequired = shareCareDoctor["isRequired"] as? Bool
        }
        
        if let shareCareVideo = RemoteConfigManager.shared.getValueJson(fromKey: .shareCareVideo) {
            gAppShare.careVideo?.itunesProviderToken = shareCareVideo["itunes_provider_token"] as? String
            gAppShare.careVideo?.itunesCampaignToken = shareCareVideo["itunes_campaign_token"] as? String
            
            gAppShare.careVideo?.analyticsSource = shareCareVideo["analytics_source"] as? String
            gAppShare.careVideo?.analyticsMedium = shareCareVideo["analytics_medium"] as? String
            gAppShare.careVideo?.analyticsCampaign = shareCareVideo["analytics_campaign"] as? String
            
            gAppShare.careVideo?.isRequired = shareCareVideo["isRequired"] as? Bool
        }
        
        if let shareCarePost = RemoteConfigManager.shared.getValueJson(fromKey: .shareCarePost) {
            gAppShare.carePost?.itunesProviderToken = shareCarePost["itunes_provider_token"] as? String
            gAppShare.carePost?.itunesCampaignToken = shareCarePost["itunes_campaign_token"] as? String
            
            gAppShare.carePost?.analyticsSource = shareCarePost["analytics_source"] as? String
            gAppShare.carePost?.analyticsMedium = shareCarePost["analytics_medium"] as? String
            gAppShare.carePost?.analyticsCampaign = shareCarePost["analytics_campaign"] as? String
            
            gAppShare.carePost?.isRequired = shareCarePost["isRequired"] as? Bool
        }
    }
}

// MARK: Version
class Version: NSObject {
    var minVersion: String?
    var latestVersion: String?
    var isReviewAppstore: Bool?
    
    var minVersionNumber: Int?
    var latestVersionNumber: Int?
    
    var systemVersionNumber: Int?
}

// MARK: Warning
class Warning: NSObject {
    var title: String?
    var content: String?
    var isRequired: Bool?
    var isEnable: Bool?
}

// MARK: AppSettings
class AppSettings: NSObject {
    var enableAgoraArea: Bool?
    var enableTwilioArea: Bool?
    var isRequiredVerifyPhone: Bool?
    
    var isHiddenHomeVR: Bool?
    var isHiddenHomeCare: Bool?
    var isHiddenHomeScales: Bool?
    var isHiddenHomeBP: Bool?
    var isHiddenHomeWatch: Bool?
    var isHiddenHomeHealth: Bool?
}
