////
////  OperationQueueManager.swift
////  1SK
////
////  Created by tuyenvx on 16/03/2021.
////
//
//import UIKit
//import Photos
//
//// MARK: - SKOperationQueue
//class SKOperationQueue {
//    private var maxConcurentCount = 1
//    private var qualityOfService: QualityOfService = .default
//
//    lazy var queue: OperationQueue = {
//        let queue = OperationQueue()
//        queue.qualityOfService = self.qualityOfService
//        queue.maxConcurrentOperationCount = self.maxConcurentCount
//        return queue
//    }()
//
//    init(maxConcurentCount: Int = 1, qualityOfService: QualityOfService = .default) {
//        self.maxConcurentCount = maxConcurentCount
//        self.qualityOfService = qualityOfService
//    }
//
//    func addOperations(_ operations: [Operation], waitUntilFinish: Bool = false, completion: (() -> Void)? = nil) {
//        self.queue.addOperations(operations, waitUntilFinished: waitUntilFinish)
//        if let `completion` = completion {
//            self.setCompletionBlock(completion)
//        }
//    }
//
//    func setCompletionBlock(_ completion: @escaping () -> Void) {
//        let blockOperation = BlockOperation(block: {
//            DispatchQueue.main.async {
//                completion()
//            }
//        })
//        for operation in self.queue.operations {
//            blockOperation.addDependency(operation)
//        }
//        self.queue.addOperation(blockOperation)
//    }
//
//    func cancelAllOperations() {
//        self.queue.cancelAllOperations()
//    }
//}
//
//// MARK: - AsyncOperation
//class FileUploadOperation: AsyncOperation {
//    private var fileUploadModel: FileUploadModel
//    private var profileID: Int
//    private var folderID: Int
//
//    private var completionHandler: ((Result<FileModel, APIError>) -> Void)?
//
//    override func main() {
//        if isCancelled {
//            self.finish()
//            return
//        }
//        let fileModel = FileModel(id: "", name: self.fileUploadModel.name, url: self.fileUploadModel.path, note: "")
//    }
//}
//
//// MARK: - AsyncOperation
//class ImageUploadOperation: AsyncOperation {
//    private var uploadService: UploadServiceProtocol?
//    private var asset: PHAsset
//    private var cachingImageManager: PHCachingImageManager
//    private var completionHandler: ((Result<BaseModel<FileUploadModel>, APIError>) -> Void)?
//
//    init(uploadService: UploadServiceProtocol?, asset: PHAsset, cachingImageManager: PHCachingImageManager, completion: ((Result<BaseModel<FileUploadModel>, APIError>) -> Void)?) {
//        self.uploadService = uploadService
//        self.asset = asset
//        self.cachingImageManager = cachingImageManager
//        self.completionHandler = completion
//    }
//
//    override func main() {
//        if isCancelled {
//            self.finish()
//            return
//        }
//
//        let option = PHImage.optionsPhotoImageRequest()
//        self.cachingImageManager.requestImage(for: self.asset, targetSize: PHImage.targetSize, contentMode: PHImage.contentMode, options: option) { [weak self] (image, _) in
//            guard let `image` = image else {
//                print("isCancelled: false")
//                return
//            }
//
//            print("isCancelled: true")
//            self?.uploadService?.uploadImage(image: image, completion: { [weak self] result in
//                self?.completionHandler?(result)
//                self?.finish()
//            })
//        }
//
////        let option = PHImageRequestOptions()
////        option.resizeMode = .exact
////        option.isSynchronous = true
////
////        self.cachingImageManager.requestImage(for: asset,
////                                              targetSize: CGSize(width: 2048, height: 2732),
////                                              contentMode: .aspectFill,
////                                              options: option) { [weak self] (image, _) in
////            guard let `image` = image else {
////                return
////            }
////            self?.uploadService?.uploadImage(image: image, completion: { [weak self] result in
////                self?.completionHandler?(result)
////                self?.finish()
////            })
////        }
//    }
//}
//
//// MARK: - Operation
//class AsyncOperation: Operation {
//    enum State: String {
//        case ready = "isReady"
//        case executing = "isExecuting"
//        case finished = "isFinished"
//    }
//
//    var state: State = .ready {
//        willSet {
//            self.willChangeValue(forKey: newValue.rawValue)
//            self.willChangeValue(forKey: self.state.rawValue)
//        }
//
//        didSet {
//            self.didChangeValue(forKey: oldValue.rawValue)
//            self.didChangeValue(forKey: self.state.rawValue)
//        }
//    }
//
//    override var isConcurrent: Bool {
//        return true
//    }
//
//    override var isReady: Bool {
//        return super.isReady && self.state == .ready
//    }
//
//    override var isExecuting: Bool {
//        return self.state == .executing
//    }
//
//    override var isFinished: Bool {
//        return self.state == .finished
//    }
//
//    override func cancel() {
//        super.cancel()
//        self.finish()
//    }
//
//    override func start() {
//        guard !isCancelled else {
//            self.finish()
//            return
//        }
//        if !self.isExecuting {
//            self.state = .executing
//        }
//        self.main()
//    }
//
//    func finish() {
//        if self.isExecuting {
//            self.state = .finished
//        }
//    }
//}
