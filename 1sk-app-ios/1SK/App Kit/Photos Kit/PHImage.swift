//
//  PHImage.swift
//  1SK
//
//  Created by vuongbachthu on 8/8/21.
//

import Foundation
import UIKit
import Photos

class PHImage: NSObject {
    static let targetSize = CGSize(width: 1280, height: 720)
    static let contentMode: PHImageContentMode = .aspectFit
    
    static func optionsPhotoImageRequest() -> PHImageRequestOptions {
        let options = PHImageRequestOptions()
        options.deliveryMode = .opportunistic
        options.isNetworkAccessAllowed = true
        options.resizeMode = .fast
        options.isSynchronous = false
        
        return options
    }
}
