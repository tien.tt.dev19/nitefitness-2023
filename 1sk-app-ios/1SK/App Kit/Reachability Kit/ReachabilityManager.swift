//
//  InternetConnection.swift
//  1SK
//
//  Created by tuyenvx on 23/02/2021.
//

import Foundation
import Reachability

class ReachabilityManager {
    static let shared = ReachabilityManager()
    private var reachability: Reachability?
    
    private init() {
        self.setupDefaults()
    }

    func isAvailable() -> Bool {
        switch self.reachability?.connection {
        case .cellular:
            return true
            
        case .wifi:
            return true
            
        case .unavailable:
            return false
            
        default:
            return false
        }
    }

    private func setupDefaults() {
        self.reachability = try? Reachability()
        self.reachability?.whenReachable = { _ in
            print("<reachability> whenReachable Available")
            gIsInternetConnectionAvailable = true
            NotificationCenter.default.post(name: .connectionAvailable, object: nil)
            
        }

        self.reachability?.whenUnreachable = { _ in
            print("<reachability> whenReachable Unavailable")
            gIsInternetConnectionAvailable = false
            NotificationCenter.default.post(name: .connectionUnavailable, object: nil)
        }
    }

    func setStartNotifier() {
        try? self.reachability?.startNotifier()
    }
}
