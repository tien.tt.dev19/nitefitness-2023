//
//  IndoorBikeManager.swift
//  1SK
//
//  Created by Thaad on 27/04/2022.
//

import Foundation

class IndoorBikeManager: NSObject {
    static let shared = IndoorBikeManager()
    private let realm: RealmManagerProtocol = RealmManager()
    
    func setAddLogSectionExercise(object: IndoorBikeModel) {
        self.realm.write(object)
    }
    
    func onSetEndTimeSection(endTimeSection: Int, objectSection: IndoorBikeModel?) {
        if let object: IndoorBikeModel = self.realm.object({$0.id == objectSection?.id}) {
            self.realm.update {
                object.end_time = endTimeSection
            }
        }
    }
    
    func onSetDeviceNameSection(deviceName: String?, objectSection: IndoorBikeModel?) {
        if let object: IndoorBikeModel = self.realm.object({$0.id == objectSection?.id}) {
            self.realm.update {
                object.device_name = deviceName ?? ""
            }
        }
    }
    
    func onSetEndTimeFile(endTimeFile: Int, objectSection: IndoorBikeModel?) {
        guard let object: IndoorBikeModel = self.realm.object({$0.id == objectSection?.id}) else {
            print("<Write Data> onSetEndTimeFile objectSection?.id = nil")
            return
        }
        guard let lastObj = object.log_path.last else {
            print("<Write Data> onSetEndTimeFile object.log_path.last = nil")
            return
        }
        guard lastObj.end_time == 0 else {
            print("<Write Data> onSetEndTimeFile lastObj.end_time != 0: end_time: ", lastObj.end_time ?? -1)
            return
        }
        print("<Write Data> onSetEndTimeFile endTimeFile true")
        guard let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileDirectory = directory.appendingPathComponent(Folder.FitLog)
        
        guard let oldFileName = lastObj.file_name else { return }
        let oldFilePath = fileDirectory.appendingPathComponent(oldFileName)
        
        let newFileName = String(format: "%@_%@.bin", oldFileName.replaceCharacter(target: ".bin", withString: ""), endTimeFile.asString)
        let newFilePath = fileDirectory.appendingPathComponent(newFileName)
        print("<Write Data> Write to path: ", newFilePath.path)
        
        do {
            try FileManager.default.moveItem(at: oldFilePath, to: newFilePath)

            self.realm.update {
                lastObj.file_name = newFileName
                lastObj.end_time = endTimeFile
            }
            
        } catch {
            print("<Write Data> Write to path error: ", error)
        }
    }
    
    func onRemoveFileLogData(objectSection: IndoorBikeModel) {
        for file in objectSection.log_path {
            if FileManager.default.fileExists(atPath: file.filePath) {
                // delete file
                do {
                    try FileManager.default.removeItem(atPath: file.filePath)

                    print("./UPLOAD: Delete file success - file_name:", file.file_name ?? "null")

                } catch {
                    print("./UPLOAD: Could not delete file, probably read-only filesystem - file_name:", file.file_name ?? "null")
                }

            } else {
                print("./UPLOAD: File not found - file_name:", file.file_name ?? "null")
            }
        }
        
        print("FileLogData upload id:", objectSection.id)
        print("FileLogData ")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.realm.remove(objectSection)
        }
    }
}
