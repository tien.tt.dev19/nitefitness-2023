//
//  UserDefaultsManager.swift
//  1SK
//
//  Created by Thaad on 15/03/2022.
//

import Foundation

protocol ObjectSavable {
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable
}

enum ObjectSavableError: String, LocalizedError {
    case unableToEncode = "<ObjectSavable> Unable to encode object into data"
    case noValue = "ObjectSavable> No data object found for the given key"
    case unableToDecode = "ObjectSavable> Unable to decode object into given type"
    
    var errorDescription: String? {
        rawValue
    }
}

extension UserDefaults: ObjectSavable {
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            set(data, forKey: forKey)
        } catch {
            throw ObjectSavableError.unableToEncode
        }
    }
    
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable {
        guard let data = data(forKey: forKey) else { throw ObjectSavableError.noValue }
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(type, from: data)
            return object
        } catch {
            throw ObjectSavableError.unableToDecode
        }
    }
}

extension UserDefaults {
    var cartProductSchema: [CartProductSchema] {
        get {
            print("<Schema> Get CartProductSchema")
            guard let data = UserDefaults.standard.data(forKey: "CartProductSchema") else { return [] }
            return (try? PropertyListDecoder().decode([CartProductSchema].self, from: data)) ?? []
        }
        set {
            print("<Schema> Set CartProductSchema")
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: "CartProductSchema")
        }
        
        //Usage:
        //UserDefaults.standard.domainSchemas = [.init(domain: "a", schema: "b"), .init(domain: "c", schema: "d")]
        //UserDefaults.standard.domainSchemas  // [{domain "a", schema "b"}, {domain "c", schema "d"}]
    }
    
    var LIST_DEVICE_CONNECT: [DeviceSchema] {
        get {
            print("<Schema> Get CartProductSchema")
            guard let data = UserDefaults.standard.data(forKey: "LIST_DEVICE_CONNECT") else { return [] }
            return (try? PropertyListDecoder().decode([DeviceSchema].self, from: data)) ?? []
        }
        set {
            print("<Schema> Set CartProductSchema")
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: "LIST_DEVICE_CONNECT")
        }
        
        //Usage:
        //UserDefaults.standard.domainSchemas = [.init(domain: "a", schema: "b"), .init(domain: "c", schema: "d")]
        //UserDefaults.standard.domainSchemas  // [{domain "a", schema "b"}, {domain "c", schema "d"}]
    }
    
}
