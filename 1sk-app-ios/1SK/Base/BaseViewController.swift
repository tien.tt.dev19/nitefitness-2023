//
//  BaseViewController.swift
//  1SK
//
//  Created by tuyenvx on 9/30/20.
//

import UIKit
import SnapKit
import Photos

class BaseViewController: UIViewController, UIGestureRecognizerDelegate {
    
    private var progressHub: UIActivityIndicatorView?
    private var viewProgressHub: UIView?
    var viewError: ErrorView?
    
    var isInteractivePopGestureEnable = true {
        didSet {
            self.setUpdateInteractivePopGesture()
        }
    }

    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setInitLeftBackButton()
        self.setInitProgressHub()
        self.setInitNotificationCenterNetWork()
    }
    
    deinit {
        print("BaseViewController deinit")
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setUpdateInteractivePopGesture()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        if #available(iOS 13, *) {
            return .darkContent
        }
        return .default
    }
    
    func setLeftBackButton(_ isInteractivePopGestureEnable: Bool = true) {
        self.setLeftBarButton(style: .back) { [weak self] _ in
            self?.navigationController?.popViewController(animated: true)
        }
        self.isInteractivePopGestureEnable = isInteractivePopGestureEnable
    }
}

// MARK: Navigation View
extension BaseViewController {
    func setNavagationBarHidden(_ viewControllers: UIViewController.Type...) {
        guard let baseNavigationController = navigationController as? BaseNavigationController else {
            return
        }
        baseNavigationController.setHiddenNavigationBarViewControllers(viewControllers)
    }
    
    private func setInitLeftBackButton() {
        if let navigation = self.navigationController, self != navigation.viewControllers.first {
            self.setLeftBackButton()
        }
    }
    
    private func setUpdateInteractivePopGesture() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.isInteractivePopGestureEnable
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
}

// MARK: - Alert View
extension BaseViewController {
    func showAlert(type: AlertType, delegate: AlertViewControllerDelegate? = nil, folderID: String = "", fileID: String = "") {
        let alertViewController = AlertViewController.loadFromNib()
        alertViewController.type = type
        alertViewController.folderID = folderID
        alertViewController.fileID = fileID
        alertViewController.delegate = delegate
        alertViewController.modalPresentationStyle = .overFullScreen
        self.present(alertViewController, animated: false, completion: nil)
    }
}

// MARK: - Internet Connection
extension BaseViewController {
    private func setInitNotificationCenterNetWork() {
        print("<reachability> BaseViewController setInitNotificationCenterNetWork")
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionAvailable), name: .connectionAvailable, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionUnavailable), name: .connectionUnavailable, object: nil)
    }

    @objc func onInternetConnectionAvailable() {
        print("<reachability> BaseViewController onInternetConnectionAvailable")
    }

    @objc func onInternetConnectionUnavailable() {
        print("<reachability> BaseViewController onInternetConnectionUnavailable")
        
        if gIsShowAlertNetworkView == false {
            gIsShowAlertNetworkView = true
            
            let controller = AlertNetworkViewController()
            controller.modalPresentationStyle = .custom
            UIApplication.shared.visibleViewController?.present(controller, animated: false)
        }
    }
}

// MARK: - Loading
extension BaseViewController {
    private func setInitProgressHub() {
        self.viewProgressHub = UIView()
        self.viewProgressHub!.backgroundColor = .clear
        self.viewProgressHub?.alpha = 0
        self.viewProgressHub?.isHidden = true
        self.view.addSubview(self.viewProgressHub!)
        self.view.bringSubviewToFront(viewProgressHub!)
        self.viewProgressHub?.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        self.progressHub = UIActivityIndicatorView(style: .whiteLarge)
        
        let hudBGView = UIView()
        hudBGView.backgroundColor = R.color.black_a53()
        self.viewProgressHub?.addSubview(hudBGView)
        hudBGView.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(80)
        }
        hudBGView.cornerRadius = 8
        hudBGView.addSubview(self.progressHub!)
        self.progressHub?.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.height.equalTo(40)
        }
    }

    func showProgressHud(offsetTop: CGFloat = 0, offsetBottom: CGFloat = 0, position: ProgressPosition = .full, backgroundColor: UIColor = .clear) {
        self.viewProgressHub?.backgroundColor = backgroundColor
        self.view.bringSubviewToFront(self.viewProgressHub!)
        let navigationBarHeight = navigationController?.navigationBar.height ?? 0
        let tabbarHeight = tabBarController?.tabBar.height ?? 0
        var statusBarHeight = UIApplication.shared.statusBarFrame.height
        if #available(iOS 13, *) {
            statusBarHeight = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        }
        var topMargin: CGFloat = 0
        var bottomMargin: CGFloat = 0
        switch position {
        case .full:
            topMargin = offsetTop
            bottomMargin = offsetBottom
        case .underStatusBar:
            topMargin = statusBarHeight + offsetTop
            bottomMargin = offsetBottom
        case .aboveTabbar:
            topMargin = offsetTop
            bottomMargin = tabbarHeight + offsetBottom
        case .underNavigationBar:
            topMargin = statusBarHeight + navigationBarHeight + offsetTop
            bottomMargin = offsetBottom
        case .aboveTabbarAndUnderNavigationBar:
            topMargin = statusBarHeight + navigationBarHeight + offsetTop
            bottomMargin = offsetBottom + tabbarHeight
        case .aboveTabbarAndUnderStatusBar:
            topMargin = statusBarHeight + offsetTop
            bottomMargin = offsetBottom + tabbarHeight
        }

        self.viewProgressHub?.snp.remakeConstraints { (make) in
            make.top.equalToSuperview().offset(topMargin)
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview().offset(-bottomMargin)
        }
        self.progressHub?.startAnimating()
        self.viewProgressHub?.isHidden = false
            self.viewProgressHub?.alpha = 1
            self.view.layoutIfNeeded()
    }

    func hideProgressHud() {
        self.progressHub?.stopAnimating()
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.viewProgressHub?.alpha = 0
        }, completion: { _ in
            self.viewProgressHub?.isHidden = true
        })
    }
}

// MARK: - Error View
extension BaseViewController {
    func showErrorView(with content: String?,
                       delegate: ErrorViewDelegate,
                       position: ProgressPosition = .full,
                       offsetTop: CGFloat = 0,
                       offsetBottom: CGFloat = 0) {
        if let `errorView` = self.viewError {
            errorView.removeFromSuperview()
            self.viewError = nil
        }
        let navigationBarHeight = navigationController?.navigationBar.height ?? 0
        let tabbarHeight = tabBarController?.tabBar.height ?? 0
        var statusBarHeight = UIApplication.shared.statusBarFrame.height
        if #available(iOS 13, *) {
            statusBarHeight = UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        }
        var topMargin: CGFloat = 0
        var bottomMargin: CGFloat = 0
        switch position {
        case .full:
            topMargin = offsetTop
            bottomMargin = offsetBottom
        case .underStatusBar:
            topMargin = statusBarHeight + offsetTop
            bottomMargin = offsetBottom
        case .aboveTabbar:
            topMargin = offsetTop
            bottomMargin = tabbarHeight + offsetBottom
        case .underNavigationBar:
            topMargin = statusBarHeight + navigationBarHeight + offsetTop
            bottomMargin = offsetBottom
        case .aboveTabbarAndUnderNavigationBar:
            topMargin = statusBarHeight + navigationBarHeight + offsetTop
            bottomMargin = offsetBottom + tabbarHeight
        case .aboveTabbarAndUnderStatusBar:
            topMargin = statusBarHeight + offsetTop
            bottomMargin = offsetBottom + tabbarHeight
        }

        self.viewError = ErrorView(message: content ?? "Không thể tải dữ liệu")
        self.view.addSubview(self.viewError!)
        self.viewError?.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.top.equalToSuperview().offset(topMargin)
            make.bottom.equalToSuperview().offset(-bottomMargin)
        }
        self.viewError?.delegate = delegate
        self.view.bringSubviewToFront(self.viewError!)
    }

    func hideErrorView() {
        self.viewError?.removeFromSuperview()
        self.viewError = nil
    }
}

// MARK: - UIGestureRecognizerDelegate
extension BaseViewController {
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer else {
            return true
        }
        return navigationController?.viewControllers.count ?? 0 > 1
    }
}
