//
//  Common.swift
//  1SK
//
//  Created by vuongbachthu on 8/8/21.
//

import Foundation
import UIKit
import SocketIO
import Realm

var gVersion = Version()
var gWarning = Warning()
var gAppLinks = AppLinks()
var gAppShare = AppShare()
var gAppSettings = AppSettings()
var gConfigs = [ConfigModel]()

var gUniversalLink: URL?
var gPushNotiModel: PushNotiModel?
var gStravaURL: URL?
var gUserIdReviewAppStore = 182 //1sk.elcom@gmail.com
var gTimeSystem: Int?
var gBadgeCare = 0
var gTimeDurationEndCall = 1800

var gSocketCare: SocketIOClient?
var gSocketTimer: SocketIOClient?
var gSocketFitness: SocketIOClient?
var gSocketID: SocketIOClient?

var gUser: UserModel?
var gRecentWorkouts: [WorkoutModel]?
var gDeviceToken: String?
//var gFCMToken: String?
var gTabBarController: UITabBarController?
var gPhoneHotline: String?

var gIsInternetConnectionAvailable = false
var gIsShowMarketingPopup = false
var gIsShowAlertNetworkView = false

var gListCartProduct: [CartProductSchema] = []
var gListCountryModel: [CountryModel] = []

var gRealm: RealmManagerProtocol = RealmManager()

class NotiApp: NSObject {
    static var isUpdatePost = false
}
