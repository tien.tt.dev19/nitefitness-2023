//
//  Constant.swift
//  1SK
//
//  Created by tuyenvx on 11/01/2021.
//

import UIKit

struct Constant {
    struct Number {
        static let animationTime = 0.3
        static let roundCornerRadius: CGFloat = 30
    }

    struct Screen {
        static var width = min(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
        static var height = max(UIScreen.main.bounds.width, UIScreen.main.bounds.height)
    }
}

struct SKDevice {
    struct Name {
        static let CF_398 = "1SK-SmartScale68" // "1SK-SMARTSCALE68"
        static let CF_539 = "Smart scale CF539" // "SMART SCALE CF539"
        static let CF_516 = "Smart scale CF516" // "SMART SCALE CF516"
        
        static let DBP_6292B = "DBP-6292B"
        static let DBP_6277B = "DBP-6277B" // "DBP-6277b"
        
        static let RS_1949LB = "RS1949LB"
        static let RS_2047LB = "RS2047LB"
    }
    
    struct NameDisplay {
        static let CF_398 = "Cân CF398"
        static let CF_539 = "Cân CF539"
        static let CF_516 = "Cân CF516BLE"
        
        static let DBP_6292B = "DBP-6292B"
        static let DBP_6277B = "DBP-6277B"
        
        static let RS_1949LB = "RS1949LB"
        static let RS_2047LB = "RS2047LB"
    }
    
    struct Code {
        static let CF_398 = "CF398"
        static let CF_539 = "CF539"
        static let CF_516 = "CF516"
        
        static let DBP_6292B = "DBP-6292B"
        static let DBP_6277B = "DBP-6277B"
        
        static let RS_1949LB = "RS1949LB"
        static let RS_2047LB = "RS2047LB"
    }
}

struct Folder {
    static let FitWorkout = "/wrokout"
    static let FitLog = "fit/log"
}

class DataConfig: NSObject {
    static let shared = DataConfig()
    
    func getRateComments(type: RateType?) -> [RateComment] {
        var rateComments: [RateComment] = []
        
        switch type {
        case .one:
            rateComments = [
                RateComment.init(comment: "Chất lượng sản phẩm rất kém"),
                RateComment.init(comment: "Không đáng tiền"),
                RateComment.init(comment: "Giao hàng quá chậm"),
                RateComment.init(comment: "Chất lượng phục vụ rất tệ")
            ]

        case .two:
            rateComments = [
                RateComment.init(comment: "Chất lượng sản phẩm kém"),
                RateComment.init(comment: "Giá cả không hợp lý"),
                RateComment.init(comment: "Giao hàng chậm"),
                RateComment.init(comment: "Chất lượng phục vụ kém")
            ]

        case .three:
            rateComments = [
                RateComment.init(comment: "Sản phẩm tạm được"),
                RateComment.init(comment: "Giá cả chấp nhận được"),
                RateComment.init(comment: "Giao hàng chưa nhanh"),
                RateComment.init(comment: "Chất lượng phục vụ tạm ổn")
            ]

        case .four:
            rateComments = [
                RateComment.init(comment: "Sản phẩm phù hợp"),
                RateComment.init(comment: "Giá cả hợp lý"),
                RateComment.init(comment: "Giao hàng nhanh"),
                RateComment.init(comment: "Chất lượng phục vụ tốt")
            ]

        case .five:
            rateComments = [
                RateComment.init(comment: "Sản phẩm dùng rất tốt"),
                RateComment.init(comment: "Khuyên dùng"),
                RateComment.init(comment: "Giá cả ưu đãi"),
                RateComment.init(comment: "Chất lượng phục vụ tuyệt vời"),
                RateComment.init(comment: "Giao hàng rất nhanh")
            ]

        default:
            break
        }
        
        return rateComments
    }
}
