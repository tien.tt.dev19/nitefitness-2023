//
//  Key.swift
//  1SK
//
//  Created by vuongbachthu on 9/9/21.
//

import Foundation

struct Key {
    static let accessToken = "accessToken"
    static let userAgent = "userAgent"
    static let encrytionKey = "com.elcom.OneSK.EncrytionKey"
    
}
