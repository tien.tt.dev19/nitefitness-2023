//
//  NotificationName.swift
//  1SK
//
//  Created by tuyenvx on 01/03/2021.
//

import Foundation

extension Notification.Name {
    static let applicationWillEnterForeground = Notification.Name("applicationWillEnterForeground")
    static let applicationDidEnterBackground = Notification.Name("applicationDidEnterBackground")
    static let applicationOpenDeepLinks = Notification.Name("applicationOpenDeepLinks")
    static let applicationStravaUrlCallBack = Notification.Name("applicationStravaUrlCallBack")
    static let applicationOSNotificationOpenedBlock = Notification.Name("applicationOSNotificationOpenedBlock")
    
    static let connectionAvailable = Notification.Name("ConnectionAvailable")
    static let connectionUnavailable = Notification.Name("ConnectionNotAvailable")
    static let unreadNotificationCountChanged = Notification.Name("UnreadNotificationCountChanged")
    static let showFitnessHomePage = Notification.Name("showFitnessHomePage")
    static let tokenExpire = Notification.Name("TokenExpire")
    static let deletePost = Notification.Name("DeletePost")
    static let didCreateNewComment = Notification.Name("DidCreateNewComment")
    static let didLikePost = Notification.Name("DidLikePost")
    static let didDeleteComment = Notification.Name("DidDeleteComment")
    
    static let didUpdatePost = Notification.Name("DidUpdatePost")
    static let onActionPlayControl = Notification.Name("onActionPlayControl")
    static let onActionLoading = Notification.Name("onActionLoading")
    
//    static let FCMToken = Notification.Name("FCMToken")
    
    static let DID_CHANGE_PROFILE = Notification.Name("DID_CHANGE_PROFILE")
}
