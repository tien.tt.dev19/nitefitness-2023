//
//  ActivityType.swift
//  1SK
//
//  Created by tuyenvx on 01/06/2021.
//

import Foundation
import UIKit

enum ActivityType: Int, Codable, CaseIterable {
    case aerobics = 1
    case pilates = 2
    case swim = 3
    case gym = 4
    case run = 5
    case walk = 6
    case dance = 7
    case yoga = 8
    case cycling = 9
    case meditation = 10
    case rockClimb = 11
    case synthetic = 12
    case other = 13

    var subjectId: Int {
        switch self {
        case .aerobics:
            return 2
            
        case .pilates:
            return 3
            
        case .swim:
            return 4
            
        case .gym:
            return 1
            
        case .run:
            return 5
            
        case .walk:
            return 6
            
        case .dance:
            return 8
            
        case .yoga:
            return 13
            
        case .cycling:
            return 9
            
        case .meditation:
            return 10
            
        case .rockClimb:
            return 11
            
        case .synthetic:
            return 12
            
        case .other:
            return 7
        }
    }
    
    var name: String {
        switch self {
        case .run:
            return "Chạy"
        case .swim:
            return "Bơi"
        case .yoga:
            return "Yoga"
        case .gym:
            return "Gym"
        case .cycling:
            return "Đạp xe"
        case .walk:
            return "Đi bộ"
        case .rockClimb:
            return "Leo núi"
        case .meditation:
            return "Thiền"
        case .pilates:
            return "Pilates"
        case .aerobics:
            return "Aerobics"
        case .dance:
            return "Nhảy"
        case .synthetic:
            return "Tổng hợp"
        case .other:
            return "Khác"
        }
    }

    var iconGray: UIImage? {
        switch self {
        case .aerobics:
            return R.image.ic_aerobics()
        case .pilates:
            return R.image.ic_pilates()
        case .swim:
            return R.image.ic_swim()
        case .gym:
            return R.image.ic_gym()
        case .run:
            return R.image.ic_run()
        case .walk:
            return R.image.ic_walk()
        case .dance:
            return R.image.ic_dance()
        case .yoga:
            return R.image.ic_yoga()
        case .cycling:
            return R.image.ic_cycling()
        case .meditation:
            return R.image.ic_meditation()
        case .rockClimb:
            return R.image.ic_rock_climb()
        case .synthetic:
            return UIImage(named: "")
        case .other:
            return UIImage(named: "")
        }
    }
    
    var iconBlue: UIImage? {
        switch self {
        case .aerobics:
            return R.image.ic_aerobics_blue()
        case .pilates:
            return R.image.ic_pilates_blue()
        case .swim:
            return R.image.ic_swim_blue()
        case .gym:
            return R.image.ic_gym_blue()
        case .run:
            return R.image.ic_run_blue()
        case .walk:
            return R.image.ic_walk_blue()
        case .dance:
            return R.image.ic_dance_blue()
        case .yoga:
            return R.image.ic_yoga_blue()
        case .cycling:
            return R.image.ic_cycling_blue()
        case .meditation:
            return R.image.ic_meditation_blue()
        case .rockClimb:
            return R.image.ic_rock_climb_blue()
        case .synthetic:
            return UIImage(named: "logo")
        case .other:
            return UIImage(named: "logo")
        }
    }

    var type: Int {
        let items = ActivityType.allCases
        return items.firstIndex(of: self)! + 1
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        let rawValue = try container.decode(String.self, forKey: .type)
        guard let activity = ActivityType(rawValue: Int(rawValue)!) else {
            fatalError("Can't Decode activity")
        }
        self = activity
        
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        let rawValue = try values.decode(Int.self, forKey: .type)

//        guard let activity = ActivityType(rawValue: rawValue) else {
//            fatalError("Can't Decode activity")
//        }
//        self = activity
    }

    init?(type: Int) {
        if type > 0 && type <= ActivityType.allCases.count {
            self = ActivityType.allCases[type - 1]
        } else {
            return nil
        }
    }

    init?(typeString: String) {
        guard let type = Int(typeString) else {
            return nil
        }
        self.init(type: type)
    }

    enum CodingKeys: String, CodingKey {
        case id, name, type
    }
}
