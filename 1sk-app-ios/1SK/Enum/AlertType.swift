//
//  AlertType.swift
//  1SK
//
//  Created by tuyenvx on 26/02/2021.
//

import UIKit

enum AlertType {
    case cameraUsage
    case photoUsage
    case deleteFile
    case cancelOrder
    case deleteFolder
    case deletePost(Int)
    case updateApp
    case internetConnection
    case deleteComment(Int)
    case logout
    
    case deleteFriend
    case unfollow(String)

    var bottomViewHeight: CGFloat {
        switch self {
        case .cameraUsage:
            return 236
            
        case .photoUsage:
            return 236
            
        case .deleteFile, .deleteFolder, .deletePost, .deleteComment, .deleteFriend, .unfollow:
            return 212
            
        case .updateApp:
            return 236
            
        case .internetConnection:
            return 107
        case .cancelOrder:
            return 107
        case .logout:
            return 224
        }
    }

    var image: UIImage? {
        switch self {
        case .cameraUsage:
            return R.image.ic_alert_camera()
        case .photoUsage:
            return R.image.ic_alert_photo()
        case .deleteFile, .deleteFolder, .deletePost, .deleteComment, .deleteFriend:
            return R.image.ic_alert_delete()
        case .updateApp:
            return R.image.ic_alert_update()
        case .internetConnection:
            return R.image.ic_wifi()
        case .logout:
            return R.image.ic_logout()
        case .unfollow:
            return R.image.ic_unfollow()
        case .cancelOrder:
            return UIImage()
        }
    }

    var cancelButtonTitle: String {
        switch self {
        case .cameraUsage:
            return "Hủy"
        case .photoUsage:
            return "Hủy"
        case .deleteFile, .deleteFolder, .deletePost, .deleteComment, .deleteFriend, .unfollow:
            return "Không"
        case .updateApp:
            return "Để sau"
        case .internetConnection:
            return "Đóng"
        case .logout:
            return "Đóng"
        case .cancelOrder:
            return "Trở lại"
        }
    }

    var okButtonTitle: String {
        switch self {
        case .cameraUsage:
            return "Đồng ý"
        case .photoUsage:
            return "Đồng ý"
        case .deleteFile, .deleteFolder, .deletePost, .deleteComment:
            return "Có, xóa"
        case .updateApp:
            return "Cập nhật"
        case .internetConnection:
            return "Thử lại"
        case .logout:
            return "Đăng xuất"
        case .deleteFriend:
            return "Bỏ theo dõi"
        case .unfollow:
            return "Bỏ theo dõi"
        case .cancelOrder:
            return "Đồng ý"
        }
    }

    var content: String {
        switch self {
        case .cameraUsage:
            return "Bạn cần cho phép truy cập máy ảnh để sử sụng tính năng này."
        case .photoUsage:
            return "Bạn cần cho phép truy cập Album ảnh để sử dụng tính năng này."
        case .deleteFile:
            return "Bạn có chắc chắn muốn xóa tệp này?"
        case .deleteFolder:
            return "Bạn có chắc chắn muốn xóa thư mục này?"
        case .deletePost:
            return "Bạn có chắc chắn muốn xóa bài viết này?"
        case .deleteComment:
            return "Bạn có chắc chắn muốn xoá bình luận này?"
        case .updateApp:
            return "Ứng dụng đã có phiên bản mới. Bạn vui lòng cập nhật để có trải nghiệm tốt nhất!"
        case .internetConnection:
            return "Không có kết nối mạng.Vui lòng kiểm tra kết nối mạng của bạn."
        case .logout:
            return "Bạn có chắc chắn muốn đăng xuất khỏi tài khoản trên thiết bị này?"
            
        case .deleteFriend:
            return "Bạn có chắc chắn muốn xóa người này khỏi danh sách người theo dõi bạn?"
        case .unfollow:
            return "Bạn có chắc chắn muốn bỏ theo dõi người này?"
        case .cancelOrder:
            return "Bạn có chắc chắn muốn hủy đơn?"
        }
    }
}

extension AlertType: Equatable {
    
}
