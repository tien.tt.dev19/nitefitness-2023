//
//  AppointmentStatus.swift
//  1SK
//
//  Created by vuongbachthu on 10/1/21.
//

import Foundation

enum AppointmentStatus: Int {
    case paying
    case confirming
    case confirmed
    case cancel
    case finish
    case complete
    case failure
    
    var id: Int {
        switch self {
        case .paying:
            return 1
        case .confirming:
            return 2
        case .confirmed:
            return 3
        case .cancel:
            return 4
        case .finish:
            return 6
        case .complete:
            return 5
        case .failure:
            return 7
        }
    }

    var name: String {
        switch self {
        case .paying:
            return "Đang xác nhận thanh toán"
        case .confirming:
            return "Đang xác nhận lịch hẹn"
        case .confirmed:
            return "Đã xác nhận lịch hẹn"
        case .cancel:
            return "Đã hủy"
        case .finish:
            return "Tư vấn thành công"
        case .complete:
            return "Hoàn thành"
        case .failure:
            return "Tư vấn không thành công"
        }
    }
}
