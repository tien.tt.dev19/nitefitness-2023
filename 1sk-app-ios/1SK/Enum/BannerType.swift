//
//  BannerType.swift
//  1SK
//
//  Created by tuyenvx on 19/01/2021.
//

import Foundation

enum BannerType: String, Codable {
    case image
    case video
    case home
}
