//
//  BaseEnum.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//

import Foundation

enum PositionToast {
    case top
    case center
    case bottom
}
