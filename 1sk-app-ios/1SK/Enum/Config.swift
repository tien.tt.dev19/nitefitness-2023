//
//  Config.swift
//  1SK
//
//  Created by tuyenvx on 26/02/2021.
//

import Foundation

enum Config: String {
    case baseURL
    case uploadURL

    var value: String {
        guard let infoDict = Bundle.main.infoDictionary else {
            return ""
        }
        return infoDict[rawValue] as? String ?? ""
    }
}
