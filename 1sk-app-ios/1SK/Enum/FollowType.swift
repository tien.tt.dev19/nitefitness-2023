//
//  FollowType.swift
//  1SK
//
//  Created by vuongbachthu on 9/10/21.
//

import Foundation

enum FollowType: String {
    case ALL // Tất cả
    case FOLLOWING // Người dùng đi follow người khác
    case FOLLOWED // Người dùng được follow bởi người khác
}
