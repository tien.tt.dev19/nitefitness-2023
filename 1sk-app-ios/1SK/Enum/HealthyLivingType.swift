//
//  HealthyLivingType.swift
//  1SK
//
//  Created by tuyenvx on 21/01/2021.
//

import Foundation

enum HealthyLivingType: String, CaseIterable {
    case healthLiving = "health"
    case knowledge = "knowledge"
    case nutrition = "nutrition"
    case training = "training"
    case device = "equipment"

    var title: String {
        switch self {
        case .healthLiving:
            return "Sống khoẻ"
        case .knowledge:
            return "Kiến thức sức khoẻ"
        case .nutrition:
            return "Dinh dưỡng"
        case .training:
            return "Tập luyện"
        case .device:
            return "Thiết bị chăm sóc sức khoẻ"
        }
    }

    static func getMenuItems() -> [HealthyLivingType] {
        return [.knowledge, .nutrition, .training, .device]
    }
}
