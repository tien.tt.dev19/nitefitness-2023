//
//  HealthProfileDetailsState.swift
//  1SK
//
//  Created by tuyenvx on 05/02/2021.
//

import Foundation

enum StateViewScreen {
    case add
    case view
    case new
    case edit
    case normal
}
