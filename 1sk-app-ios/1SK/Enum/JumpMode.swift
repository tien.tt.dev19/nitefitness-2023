//
//  JumpRopeModes.swift
//  1SKConnect
//
//  Created by Valerian on 24/05/2022.
//

import Foundation
import Kingfisher

enum JumpMode: Int {
    case freedom = 0
    case timing = 1
    case countdown = 2
    
    var name: String {
        switch self {
        case .freedom:
            return "Nhảy tự do"
            
        case .timing:
            return "Nhảy hẹn giờ"
            
        case .countdown:
            return "Nhảy đếm ngược"
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .freedom:
            return UIImage(named: "ic_jump_rope_free_style")
            
        case .timing:
            return UIImage(named: "ic_jump_rope_timing_mode")
            
        case .countdown:
            return UIImage(named: "ic_jump_rope_count_mode")
        }
    }
}

// MARK: OptionsTimingMode
enum OptionsJumpTiming: Int {
    case _30s
    case _1m
    case _5m
    case _10m
    case _others
    
    var name: String {
        switch self {
        case ._30s:
            return "30s"
        case ._1m:
            return "1 phút"
        case ._5m:
            return "5 phút"
        case ._10m:
            return "10 phút"
        case ._others:
            return "Tùy chỉnh"
        }
    }

    var value: Int {
        switch self {
        case ._30s:
            return 30
        case ._1m:
            return 60
        case ._5m:
            return 300
        case ._10m:
            return 600
        case ._others:
            return -1
        }
    }
}

// MARK: OptionsCountMode
enum OptionsJumpCount: Int {
    case _50
    case _100
    case _500
    case _1000
    case _others
    
    var name: String {
        switch self {
        case ._50:
            return "50"
        case ._100:
            return "100"
        case ._500:
            return "500"
        case ._1000:
            return "1.000"
        case ._others:
            return "Tùy chỉnh"
        }
    }
    
    var value: Int {
        switch self {
        case ._50:
            return 50
        case ._100:
            return 100
        case ._500:
            return 500
        case ._1000:
            return 1000
        case ._others:
            return -1
        }
    }
}

// MARK: ResultDataTypes
enum ResultDataTypes: Int {
    case calo
    case jump_time
    case freq_count
    case freq_most_jump
    case freq_avg_jump
    case most_jump 
    
    var name: String {
        switch self {
        case .calo:
            return "Calo tiêu thụ (kcal)"
        case .jump_time:
            return "Thời gian nhảy"
        case .freq_count:
            return "Số lần nhảy"
        case .freq_most_jump:
            return "Nhịp độ nhanh nhất (rpm)"
        case .freq_avg_jump:
            return "Nhịp độ trung bình (rpm)"
        case .most_jump:
            return "Số vòng nhảy nhiều nhất"
        }
    }
}
