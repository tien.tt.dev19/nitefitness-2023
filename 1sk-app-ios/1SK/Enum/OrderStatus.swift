//
//  OrderStatus.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//

import Foundation

enum OrderStatus: String {
    case all
    case pending
    case processing
    case completed
    case canceled
    
    var id: String {
        switch self {
        case .all:
            return ""
            
        case .pending:
            return "pending"
            
        case .processing:
            return "processing"
            
        case .completed:
            return "completed"
            
        case .canceled:
            return "canceled"
        }
    }
    
    var name: String {
        switch self {
        case .all:
            return "Tất cả"
            
        case .pending:
            return "Chờ xác nhận"
            
        case .processing:
            return "Đang giao"
            
        case .completed:
            return "Đã giao"
            
        case .canceled:
            return "Đã hủy"
        }
    }
}

