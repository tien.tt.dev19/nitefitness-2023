//
//  PHR.swift
//  1SK
//
//  Created by vuongbachthu on 10/11/21.
//

import Foundation

enum PHR {
    case weight
    case height
    case blood
    case pressure
    case spo2
    case sugar
    
    var name: String {
        switch self {
        case .weight:
            return "weight"
        case .height:
            return "height"
        case .blood:
            return "blood_type"
        case .pressure:
            return "blood_pressure"
        case .spo2:
            return "spo2"
        case .sugar:
            return "blood_sugar_level"
        }
    }
    
    var unit: String {
        switch self {
        case .weight:
            return "kg"
        case .height:
            return "cm"
        case .blood:
            return "none"
        case .pressure:
            return "mmHg"
        case .spo2:
            return "%"
        case .sugar:
            return "mg/dl"
        }
    }
}
