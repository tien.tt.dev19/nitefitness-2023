//
//  PaymentMethod.swift
//  1SK
//
//  Created by Thaad on 16/03/2022.
//

import Foundation

enum PaymentMethod: String {
    case PaymentOnDelivery = "cod"
    case PaymentTransfer = "bank_transfer"
}
