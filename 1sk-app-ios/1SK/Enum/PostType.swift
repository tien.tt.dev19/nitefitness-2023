//
//  PostType.swift
//  1SK
//
//  Created by tuyenvx on 04/06/2021.
//

import Foundation

enum PostType: Int, Codable {
    case post = 1
    case photo
    case activity
}
