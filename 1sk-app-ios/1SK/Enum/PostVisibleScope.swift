//
//  AudienceScope.swift
//  1SK
//
//  Created by tuyenvx on 02/06/2021.
//

import Foundation
import UIKit

enum PostVisibleScope: Int, CaseIterable, Codable {
    case publicPost = 1
    case friend = 2
    case onlyMe = 3

    var image: UIImage? {
        switch self {
        case .publicPost:
            return R.image.ic_public_1()
        case .friend:
            return R.image.ic_friend_1()
        case .onlyMe:
            return R.image.ic_lock_1()
        }
    }

    var newFeedImage: UIImage? {
        switch self {
        case .publicPost:
            return R.image.ic_public()
        case .friend:
            return R.image.ic_friend()
        case .onlyMe:
            return R.image.ic_lock()
        }
    }

    var name: String {
        switch self {
        case .publicPost:
            return "Công khai"
        case .friend:
            return "Bạn bè"
        case .onlyMe:
            return "Chỉ mình tôi"
        }
    }

    var description: String {
        switch self {
        case .publicPost:
            return "Người dùng trên 1SK"
        case .friend:
            return "Những người đang theo dõi tôi"
        case .onlyMe:
            return "Chỉ mình tôi được xem"
        }
    }
}
