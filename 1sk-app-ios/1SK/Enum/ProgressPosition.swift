//
//  ProgressPosition.swift
//  1SK
//
//  Created by vuongbachthu on 11/18/21.
//

import Foundation

enum ProgressPosition {
    case full
    case underStatusBar
    case underNavigationBar
    case aboveTabbar
    case aboveTabbarAndUnderNavigationBar
    case aboveTabbarAndUnderStatusBar
}
