//
//  RateType.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//

import Foundation

enum RateType {
    case one
    case two
    case three
    case four
    case five
    
    var value: Double {
        switch self {
        case .one:
            return 1.0
        case .two:
            return 2.0
        case .three:
            return 3.0
        case .four:
            return 4.0
        case .five:
            return 5.0
        }
    }
    
    var placeholderText: String {
        switch self {
        case .one:
            return "Hãy chia sẻ vì sao sản phẩm này không tốt nhé!"

        case .two:
            return "Hãy chia sẻ vì sao bạn không thích sản phẩm này nhé!"

        case .three:
            return "Hãy chia sẻ những điều bạn chưa thực sự thích ở sản phẩm này nhé!"

        case .four:
            return "Hãy chia sẻ những điều bạn thấy hài lòng ở sản phẩm này nhé!"

        case .five:
            return "Hãy chia sẻ những điều bạn yêu thích với sản phẩm này nhé!"
        }
    }
}

// MARK: RateComment
class RateComment: Codable {
    var isSelected: Bool?
    var comment: String?
    
    init(comment: String) {
        self.comment = comment
        self.isSelected = false
    }
}
