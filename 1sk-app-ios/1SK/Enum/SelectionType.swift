//
//  HealthProfileEnum.swift
//  1SK
//
//  Created by tuyenvx on 05/02/2021.
//

import Foundation

enum SelectionType {
    case profile
    case name
    case birthday
    case relationship
    case bloodGroup
    case weightActivity
    case videoResolution
    case videoSegment
    case subject
    case level
    case type
}
