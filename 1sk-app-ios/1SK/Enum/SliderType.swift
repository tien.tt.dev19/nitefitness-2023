//
//  SliderType.swift
//  1SK
//
//  Created by Thaad on 14/03/2022.
//

import Foundation

enum SliderType: String, Codable {
    case HomeStore = "mobile-home-banner"
    case HomeApp = "slider-trang-chu"
    case Consult = "mobile-consult-banner"
}
