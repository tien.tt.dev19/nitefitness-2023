//
//  TimeFilter.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//

import Foundation

enum TimeFilter {
    case day
    case week
    case month
    
    var value: Int {
        switch self {
        case .day:
            return 1
            
        case .week:
            return 2
            
        case .month:
            return 3
        }
    }
}
