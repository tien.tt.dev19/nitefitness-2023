//
//  TimeType.swift
//  1SK
//
//  Created by Thaad on 25/10/2022.
//

import Foundation

enum TimeType {
    case day
    case week
    case month
    case year
    
    var value: String {
        switch self {
        case .day:
            return "day"
        case .week:
            return "week"
        case .month:
            return "month"
        case .year:
            return "year"
        }
    }
}
