//
//  TypeBannerStore.swift
//  1SK
//
//  Created by Thaad on 15/03/2022.
//

import Foundation

enum TypeBannerStore: String {
    case link
    case product
}
