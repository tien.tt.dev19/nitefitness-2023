//
//  TypeCategoryStore.swift
//  1SK
//
//  Created by Thaad on 14/03/2022.
//

import Foundation
import UIKit

enum TypeCategoryStore {
    case DEVICE_FIT
    case DEVICE_CARE
    case NUTRITION
    case ACCESSORY
    
    var id: Int {
        switch self {
        case .DEVICE_FIT:
            return 45
            
        case .DEVICE_CARE:
            return 35
            
        case .NUTRITION:
            return 36
            
        case .ACCESSORY:
            return 37
        }
    }
    
    var name: String {
        switch self {
        case .DEVICE_FIT:
            return "Thiết bị\ntập luyện"
            
        case .DEVICE_CARE:
            return "Chăm sóc\nsức khỏe"
            
        case .NUTRITION:
            return "Dinh dưỡng"
            
        case .ACCESSORY:
            return "Phụ kiện\ntrang phục"
        }
    }
    
    var image: UIImage? {
        switch self {
        case .DEVICE_FIT:
            return R.image.ic_category_device_fit()
            
        case .DEVICE_CARE:
            return R.image.ic_category_device_care()
            
        case .NUTRITION:
            return R.image.ic_category_nutrition()
            
        case .ACCESSORY:
            return R.image.ic_category_accessory()
        }
    }
}
