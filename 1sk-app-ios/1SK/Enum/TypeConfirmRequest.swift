//
//  TypeConfirmRequest.swift
//  1SK
//
//  Created by Thaad on 10/05/2022.
//

import Foundation

enum TypeConfirmRequest: String {
    case accept
    case deny
}
