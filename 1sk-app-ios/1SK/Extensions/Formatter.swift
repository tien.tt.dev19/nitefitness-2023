//
//  Formatter.swift
//  1SK
//
//  Created by Elcom Corp on 20/10/2021.
//

import Foundation

extension Formatter {
    static let number = NumberFormatter()

    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.groupingSeparator = "."
        return formatter
    }()
}
