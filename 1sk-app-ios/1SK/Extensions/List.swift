//
//  List.swift
//  1SK
//
//  Created by TrungDN on 13/04/2022.
//

import Foundation
import RealmSwift

extension List {
    var array: Array<Element> {
        return Array(self)
    }
}
