//
//  Locale.swift
//  1SK
//
//  Created by Elcom Corp on 20/10/2021.
//

import Foundation

extension Locale {
    static let englishUS: Locale = .init(identifier: "en_US")
    static let frenchFR: Locale = .init(identifier: "fr_FR")
    static let portugueseBR: Locale = .init(identifier: "pt_BR")
}
