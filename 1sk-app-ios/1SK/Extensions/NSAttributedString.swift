//
//  NSAttributedString.swift
//  1SK
//
//  Created by Be More on 21/03/2022.
//

import Foundation
import UIKit

extension NSAttributedString {
    static func strikeThroughStyle(with text: String, strikeColor: UIColor) -> NSAttributedString {
        let attribtues = [
            NSAttributedString.Key.strikethroughStyle: NSNumber(value: NSUnderlineStyle.single.rawValue),
            NSAttributedString.Key.strikethroughColor: strikeColor
        ]
        
        let attr = NSAttributedString(string: text, attributes: attribtues as [NSAttributedString.Key: Any])
        return attr
    }
}
