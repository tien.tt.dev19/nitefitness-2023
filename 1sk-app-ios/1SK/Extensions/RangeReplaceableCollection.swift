//
//  RangeReplaceableCollection.swift
//  1SK
//
//  Created by Tiến Trần on 22/07/2022.
//

import Foundation

extension RangeReplaceableCollection {
    func intersection<S: Sequence>(_ sequence: S) -> Self where S.Element == Element, Element: Hashable {
        var set = Set(sequence)
        return filter { !set.insert($0).inserted }
    }
}
