//
//  TextViewBackward.swift
//  1SK
//
//  Created by vuongbachthu on 7/21/21.
//

import Foundation
import UIKit

protocol TextFieldBackwardDelegate: AnyObject {
    func textFieldDidDelete(textField: UITextField)
}

class TextFieldBackward: UITextField {
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: Methods
    
    weak var delegateBackward: TextFieldBackwardDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        self.delegateBackward?.textFieldDidDelete(textField: self)
    }
    
}
