//
//  Time+Extension.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//

import Foundation

struct StopWatch {

    var totalSeconds: Int

    var years: Int {
        return totalSeconds / 31536000
    }

    var days: Int {
        return (totalSeconds % 31536000) / 86400
    }

    var hours: Int {
        return (totalSeconds % 86400) / 3600
    }

    var minutes: Int {
        return (totalSeconds % 3600) / 60
    }

    var seconds: Int {
        return totalSeconds % 60
    }

    //simplified to what OP wanted
    var hoursMinutesAndSeconds: (hours: Int, minutes: Int, seconds: Int) {
        return (hours, minutes, seconds)
    }
    
//    let watch = StopWatch(totalSeconds: 27005 + 31536000 + 86400)
//    print(watch.years) // Prints 1
//    print(watch.days) // Prints 1
//    print(watch.hours) // Prints 7
//    print(watch.minutes) // Prints 30
//    print(watch.seconds) // Prints 5
//    print(watch.hoursMinutesAndSeconds) // Prints (7, 30, 5)
}

extension StopWatch {
    var simpleTimeString: String {
        let hoursText = timeText(from: hours)
        let minutesText = timeText(from: minutes)
        let secondsText = timeText(from: seconds)
        return "\(hoursText):\(minutesText):\(secondsText)"
    }

    private func timeText(from number: Int) -> String {
        return number < 10 ? "0\(number)" : "\(number)"
    }
}
