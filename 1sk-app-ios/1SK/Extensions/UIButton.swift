//
//  UIButton+Extension.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//

import Foundation
import UIKit

extension UIButton {
    func loadingIndicator(_ show: Bool) {
        let tag = 808404
        if show {
            self.isEnabled = false
            self.alpha = 0.5
            let indicator = UIActivityIndicatorView()
            indicator.color = .white
            let buttonHeight = self.bounds.size.height
            let buttonWidth = self.bounds.size.width
            
            indicator.center = CGPoint(x: buttonWidth/3, y: buttonHeight/2)
            indicator.tag = tag
            self.addSubview(indicator)
            indicator.startAnimating()
        } else {
            self.isEnabled = true
            self.alpha = 1.0
            if let indicator = self.viewWithTag(tag) as? UIActivityIndicatorView {
                indicator.stopAnimating()
                indicator.removeFromSuperview()
            }
        }
    }
}

class UIButtonRoundBlue: UIButton {
    override func awakeFromNib() {
        self.cornerRadius = self.frame.height / 2
        
        self.backgroundColor = R.color.mainColor()
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitle(self.title(for: .normal), for: .normal)
    }
}

class UIButtonRoundBlueLight: UIButton {
    override func awakeFromNib() {
        self.cornerRadius = self.frame.height / 2
        
        self.backgroundColor = R.color.color_blue_1()
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitle(self.title(for: .normal), for: .normal)
    }
}

class UIButtonRoundGray: UIButton {
    override func awakeFromNib() {
        self.cornerRadius = self.frame.height / 2
        
        self.backgroundColor = R.color.grayBgButton()
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        self.setTitleColor(UIColor.white, for: .normal)
        self.setTitle(self.title(for: .normal), for: .normal)
    }
}

class UIButtonRoundWhile: UIButton {
    override func awakeFromNib() {
        self.cornerRadius = self.frame.height / 2
        
        self.backgroundColor = .white
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        self.setTitleColor(R.color.mainColor(), for: .normal)
        self.setTitle(self.title(for: .normal), for: .normal)
    }
}

class UIButtonCheckBox: UIButton {
    override func awakeFromNib() {
        self.setImage(UIImage(named: "ic_checkbox"), for: .normal)
        self.setImage(UIImage(named: "ic_checkbox_selected"), for: .selected)
    }
}

class UIButtonShadow: UIButton {
    override func awakeFromNib() {
        self.layer.shadowColor = UIColor.gray.withAlphaComponent(0.7).cgColor
        self.layer.shadowOpacity = 0.7
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.clipsToBounds = false
    }
}

class UIButtonRadiusFill: UIButton {
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        self.backgroundColor = R.color.mainColor()
        self.cornerRadius = 12
        self.setTitleColor(UIColor.white, for: .normal)
    }
    
    override var isHighlighted: Bool {
         willSet(newValue) {
             if newValue {
                 self.backgroundColor = UIColor.init(hex: "058B8D")
                 self.setTitleColor(.white, for: .normal)
                 
             } else {
                 self.backgroundColor = R.color.mainColor()
             }
         }
     }
}

class UIButtonWithNoBorder: UIButton {
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 14.0)
        self.backgroundColor = .white
        self.setTitleColor(UIColor(hex: "676767"), for: .normal)
    }
    
    override var isSelected: Bool {
         willSet(newValue) {
             if newValue {
                 self.setTitleColor(UIColor(hex: "00C2C5"), for: .normal)
             } else {
                 self.setTitleColor(UIColor(hex: "676767"), for: .normal)
             }
         }
     }
}

class UIButtonRadiusBorder: UIButton {
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        self.backgroundColor = R.color.color_bg_green_light()
        self.cornerRadius = 12
        self.borderColor = R.color.mainColor()
        self.borderWidth = 1
        
        self.setTitleColor(R.color.mainColor(), for: .normal)
    }
    
    override var isHighlighted: Bool {
         willSet(newValue) {
             if newValue {
                 self.backgroundColor = UIColor.init(hex: "00C2C5")
                 self.setTitleColor(.white, for: .normal)
                 
             } else {
                 self.backgroundColor = R.color.color_bg_green_light()
                 self.setTitleColor(R.color.mainColor(), for: .normal)
             }
         }
     }
    
    
}

class UIButtonBorderRadiusWhile: UIButton {
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
        self.cornerRadius = 12
        self.borderColor = R.color.mainColor()
        self.borderWidth = 1
        
        self.setTitleColor(R.color.mainColor(), for: .normal)
    }
}

class UIButton_0: UIButton {
    @IBInspectable
    var btn_0_Radius: CGFloat {
        get {
            return self.cornerRadius
        }
        set {
            self.cornerRadius = newValue
        }
    }
    
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        self.backgroundColor = R.color.mainColor()
        self.cornerRadius = self.btn_0_Radius
        
        self.setTitleColor(.white, for: .normal)
    }
}

class UIButton_1: UIButton {
    @IBInspectable
    var btn_1_Radius: CGFloat {
        get {
            return self.cornerRadius
        }
        set {
            self.cornerRadius = newValue
        }
    }
    
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        self.backgroundColor = R.color.color_bg_green_light()
        self.cornerRadius = self.btn_1_Radius
        //self.borderColor = R.color.mainColor()
        //self.borderWidth = 1
        
        self.setTitleColor(R.color.mainColor(), for: .normal)
    }
}

class UIButton_2: UIButton {
    @IBInspectable
    var btn_1_Radius: CGFloat {
        get {
            return self.cornerRadius
        }
        set {
            self.cornerRadius = newValue
        }
    }
    
    override func awakeFromNib() {
        self.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        self.backgroundColor = R.color.color_bg_green_light()
        self.cornerRadius = self.btn_1_Radius
        
        self.setTitleColor(R.color.mainColor(), for: .normal)
    }
}
