//
//  UICollectionViewCell+Extension.swift
//  1SK
//
//  Created by tuyenvx on 21/01/2021.
//

import UIKit
import SkeletonView

extension UICollectionViewCell {
    func showSkeleton() {
        contentView.showAnimatedGradientSkeleton()
    }

    func hideSkeleton() {
        self.contentView.hideSkeleton()
    }
}
