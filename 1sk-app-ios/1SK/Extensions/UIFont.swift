////
////  UIFont.swift
////  1SK
////
////  Created by Thaad on 02/04/2022.
////
//
//import Foundation
//import UIKit
////import FontBlaster
//
//struct AppFontName {
//    static let regular = "BeVietnamPro-Regular"
//    static let bold = "BeVietnamPro-Bold"
//    static let lightAlt = "BeVietnamPro-Light"
//}
//
////Customise font
//extension UIFontDescriptor.AttributeName {
//    static let nsctFontUIUsage = UIFontDescriptor.AttributeName(rawValue: "NSCTFontUIUsageAttribute")
//}
//
//extension UIFont {
//    @objc class func mySystemFont(ofSize size: CGFloat) -> UIFont {
//        print("<Font> mySystemFont size: ", size)
//        return R.font.beVietnamProRegular(size: size)! //UIFont(name: AppFontName.regular, size: size)!
//    }
//
//    @objc class func myBoldSystemFont(ofSize size: CGFloat) -> UIFont {
//        print("<Font> myBoldSystemFont size: ", size)
//          return UIFont(name: AppFontName.bold, size: size)!
//    }
//
//    @objc class func myItalicSystemFont(ofSize size: CGFloat) -> UIFont {
//        print("<Font> myItalicSystemFont size: ", size)
//          return UIFont(name: AppFontName.lightAlt, size: size)!
//    }
//
//    @objc convenience init(myCoder aDecoder: NSCoder) {
//        guard
//            let fontDescriptor = aDecoder.decodeObject(forKey: "UIFontDescriptor") as? UIFontDescriptor,
//            let fontAttribute = fontDescriptor.fontAttributes[.nsctFontUIUsage] as? String else {
//            self.init(myCoder: aDecoder)
//            return
//        }
//
//        print("<Font> aDecoder fontDescriptor: ", fontDescriptor)
//
//        var fontName = ""
//
//        switch fontAttribute {
//        case "CTFontRegularUsage":
//            fontName = AppFontName.regular
//
//        case "CTFontEmphasizedUsage", "CTFontBoldUsage":
//            fontName = AppFontName.bold
//
//        case "CTFontObliqueUsage":
//            fontName = AppFontName.lightAlt
//
//        default:
//            fontName = AppFontName.regular
//        }
//
//        self.init(name: fontName, size: fontDescriptor.pointSize)!
//    }
//
//    class func setInitFontBlaster() {
//        //FontBlaster.blast()
//
//        //FontBlaster.blast() { (fonts) in
//          //print("<Font> name: ", fonts) // fonts is an array of Strings containing font names
//        //}
//    }
//
//    class func overrideInitialize() {
//        guard self == UIFont.self else { return }
//
//        if let systemFontMethod = class_getClassMethod(self, #selector(systemFont(ofSize:))),
//            let mySystemFontMethod = class_getClassMethod(self, #selector(mySystemFont(ofSize:))) {
//            method_exchangeImplementations(systemFontMethod, mySystemFontMethod)
//        }
//
//        if let boldSystemFontMethod = class_getClassMethod(self, #selector(boldSystemFont(ofSize:))),
//            let myBoldSystemFontMethod = class_getClassMethod(self, #selector(myBoldSystemFont(ofSize:))) {
//            method_exchangeImplementations(boldSystemFontMethod, myBoldSystemFontMethod)
//        }
//
//        if let italicSystemFontMethod = class_getClassMethod(self, #selector(italicSystemFont(ofSize:))),
//            let myItalicSystemFontMethod = class_getClassMethod(self, #selector(myItalicSystemFont(ofSize:))) {
//            method_exchangeImplementations(italicSystemFontMethod, myItalicSystemFontMethod)
//        }
//
//        if let initCoderMethod = class_getInstanceMethod(self, #selector(UIFontDescriptor.init(coder:))),
//            // Trick to get over the lack of UIFont.init(coder:))
//            let myInitCoderMethod = class_getInstanceMethod(self, #selector(UIFont.init(myCoder:))) {
//            method_exchangeImplementations(initCoderMethod, myInitCoderMethod)
//        }
//    }
//
//    /// Use: add init() to AppDelegate
//    //    override init() {
//    //        super.init()
//    //        UIFont.overrideInitialize()
//    //    }
//}
