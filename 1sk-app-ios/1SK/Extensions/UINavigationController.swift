//
//  UINavigationController+Extension.swift
//  1SK
//
//  Created by tuyenvx on 9/30/20.
//

import UIKit

extension UINavigationController {
    func changeBackgroundColor(to color: UIColor?) {
        self.navigationBar.barTintColor = color
    }

    func changeTintColor(to color: UIColor?) {
        self.navigationBar.tintColor = color
    }

    func changeBarTintColor(to color: UIColor?) {
        self.navigationBar.barTintColor = color
    }

    func changeTitleColor(to color: UIColor) {
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: color]
    }

    func setSeperatorLineHidden(_ isHidden: Bool) {
        self.navigationBar.setValue(isHidden, forKey: "hidesShadow")
    }
}

extension UINavigationController {
    enum ViewControllerPosition { case first, last }
    enum ViewControllersGroupPosition { case first, last, all }

    func removeController(_ position: ViewControllerPosition, animated: Bool = true, where closure: (UIViewController) -> Bool) {
        var index: Int?
        
        switch position {
        case .first:
            index = viewControllers.firstIndex(where: closure)
        case .last:
            index = viewControllers.lastIndex(where: closure)
        }
        if let index = index { removeControllers(animated: animated, in: Range(index...index)) }
    }

    func removeControllers(_ position: ViewControllersGroupPosition, animated: Bool = true, where closure: (UIViewController) -> Bool) {
        var range: Range<Int>?
        switch position {
        case .first:
            range = viewControllers.firstRange(where: closure)
            
        case .last:
            guard let _range = viewControllers.reversed().firstRange(where: closure) else { return }
            let count = viewControllers.count - 1
            range = .init(uncheckedBounds: (lower: count - _range.min()!, upper: count - _range.max()!))
            
        case .all:
            let viewControllers = self.viewControllers.filter { !closure($0) }
            setViewControllers(viewControllers, animated: animated)
            return
        }
        
        if let range = range { removeControllers(animated: animated, in: range) }
    }

    func removeControllers(animated: Bool = true, in range: Range<Int>) {
        var viewControllers = self.viewControllers
        viewControllers.removeSubrange(range)
        setViewControllers(viewControllers, animated: animated)
    }

    func removeControllers(animated: Bool = true, in range: ClosedRange<Int>) {
        removeControllers(animated: animated, in: Range(range))
    }
}
