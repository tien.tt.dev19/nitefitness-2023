//
//  UIStaclView.swift
//  1SK
//
//  Created by Tiến Trần on 22/07/2022.
//

import Foundation

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.cornerRadius = 16
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
