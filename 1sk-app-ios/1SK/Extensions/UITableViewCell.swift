//
//  UITableViewCell+Extension.swift
//  1SK
//
//  Created by tuyenvx on 11/06/2021.
//

import Foundation
import UIKit

extension UITableViewCell {
    func showSkeleton() {
        contentView.showAnimatedGradientSkeleton()
    }

    func hideSkeleton() {
        self.contentView.hideSkeleton()
    }
}

extension UITableViewHeaderFooterView {
    func showSkeleton() {
        contentView.showAnimatedGradientSkeleton()
    }

    func hideSkeleton() {
        self.contentView.hideSkeleton()
    }
}
