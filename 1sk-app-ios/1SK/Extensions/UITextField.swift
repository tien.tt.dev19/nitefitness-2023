//
//  UITextField.swift
//  1SK
//
//  Created by Thaad on 31/03/2022.
//

import Foundation
import UIKit

protocol UITextFieldBackwardDelegate {
    func textFieldDidDeleteChar(textField: UITextField)
}

class UITextFieldBackward: UITextField {
    // MARK: Life cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: Methods
    
    var delegateBackward: UITextFieldBackwardDelegate?
    
    override func deleteBackward() {
        super.deleteBackward()
        self.delegateBackward?.textFieldDidDeleteChar(textField: self)
    }
    
}
