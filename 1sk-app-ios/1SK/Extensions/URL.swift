//
//  NSURL.swift
//  1SK
//
//  Created by vuongbachthu on 11/5/21.
//

import Foundation

extension URL {
    func params() -> [String: Any]? {
        var params: [String: Any]?
        let components = URLComponents(url: self, resolvingAgainstBaseURL: false)!
        if let queryItems = components.queryItems {
            params = [:]
            for item in queryItems {
                params?[item.name] = item.value!
            }
        }
        return params
    }
}

extension NSURL {
    func resolveWithCompletionHandler(completion: @escaping (URL) -> Void) {
        let originalURL = self
        var req = URLRequest(url: originalURL as URL)
        req.httpMethod = "HEAD"

        URLSession.shared.dataTask(with: req) { body, response, error in
            completion(response?.url ?? originalURL as URL)
        }.resume()
    }
}
