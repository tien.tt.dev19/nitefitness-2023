////
////  AccountRealm.swift
////  1SK
////
////  Created by Thaad on 15/09/2022.
////
//
//import Foundation
//import RealmSwift
//
//class AccountRealm: Object {
//    @Persisted var key: String = ""
//    @Persisted var id: Int?
//    @Persisted var token: String?
//    @Persisted var name: String?
//    @Persisted var phone: String?
//    @Persisted var email: String?
//    @Persisted var avatar: String?
//    @Persisted var type: String?
//    @Persisted var is_current: Bool?
//
//    override init() {
//        super.init()
//        self.key = UUID().uuidString
//    }
//
//    override class func primaryKey() -> String? {
//        return "key"
//    }
//}
