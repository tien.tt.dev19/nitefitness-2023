//
//  LoginRequestModel.swift
//  1SK
//
//  Created by Thaad on 10/05/2022.
//

import Foundation

class LoginRequestModel: Codable {
    var token: String?
    var agent: AgentLoginRequest?
    var expiresAt: String?
    
    enum CodingKeys: String, CodingKey {
        case token
        case agent
        case expiresAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.token = try? container.decode(String.self, forKey: .token)
        self.agent = try? container.decode(AgentLoginRequest.self, forKey: .agent)
        self.expiresAt = try? container.decode(String.self, forKey: .expiresAt)
    }
}

class AgentLoginRequest: Codable {
    var deviceName, osName, osVersion, browserName: String?
    var browserVersion: String?
    
    enum CodingKeys: String, CodingKey {
        case deviceName
        case osName
        case osVersion
        case browserName
        case browserVersion
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.deviceName = try? container.decode(String.self, forKey: .deviceName)
        self.osName = try? container.decode(String.self, forKey: .osName)
        self.osVersion = try? container.decode(String.self, forKey: .osVersion)
        self.browserName = try? container.decode(String.self, forKey: .browserName)
        self.browserVersion = try? container.decode(String.self, forKey: .browserVersion)
    }
}
