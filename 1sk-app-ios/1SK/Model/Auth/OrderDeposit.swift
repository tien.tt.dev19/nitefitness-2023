//
//  OrderDeposit.swift
//  1SK
//
//  Created by vuongbachthu on 10/1/21.
//

import Foundation

class OrderDeposit: NSObject {
    var productName: String?
    var price: Int?
    var discount: Int?
    var salePrice: Int?
    var amount: Int?
    var productCode: String?
}
