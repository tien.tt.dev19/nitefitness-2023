//
//  ProfileRealm.swift
//  1SK
//
//  Created by Thaad on 22/09/2022.
//

import Foundation
import RealmSwift

class ProfileRealm: Object {
    @Persisted var key: String = ""
    @Persisted var id: Int?
    @Persisted var accessToken: String?
    @Persisted var fullName: String?
    @Persisted var birthday: String?
    @Persisted var avatar: String?
    @Persisted var gender: Gender?
    @Persisted var height: Double?
    @Persisted var weight: Double?
    @Persisted var blood: String?
    @Persisted var type: TypeDevice?
    
    override init() {
        super.init()
        self.key = UUID().uuidString
    }

    override class func primaryKey() -> String? {
        return "key"
    }
    
    var age: Int? {
        guard let birthday = self.birthday?.toDate(.dmySlash) else {
            return nil
        }
        return Date().year - birthday.year
    }
}
