//
//  AppointmentHistoryModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/12/21.
//

import Foundation

class AppointmentHistoryModel: Codable {
    var id: Int?
    var doctorFullName: String?
    var doctorAvatar: String?
    var date, startAt: Int?
    var diagnosis, prescription: String?
    var createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case doctorFullName
        case doctorAvatar
        case date
        case startAt
        case diagnosis, prescription
        case createdAt
        case updatedAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.doctorFullName = try? container.decode(String.self, forKey: .doctorFullName)
        self.doctorAvatar = try? container.decode(String.self, forKey: .doctorAvatar)
        self.date = try? container.decode(Int.self, forKey: .date)
        self.startAt = try? container.decode(Int.self, forKey: .startAt)
        self.diagnosis = try? container.decode(String.self, forKey: .diagnosis)
        self.prescription = try? container.decode(String.self, forKey: .prescription)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}
