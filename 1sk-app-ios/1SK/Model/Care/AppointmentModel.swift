//
//  AppointmentModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/1/21.
//

import Foundation

class AppointmentModel: Codable {
    var id: Int?
    var code: String?
    var status: AppointmentStatusModel?
    var patientName, patientBirthYear, patientGender, patientPhone: String?
    var patientSymptom, createdAt, updatedAt: String?
    var doctor: DoctorModel?
    var bank: BankModel?
    var doctorAvailableTime: AvailableTimeModel?
    
    var paymentPeriod: PaymentPeriodModel?
    var cancellationReason: String?
    var diagnosis: String?
    var prescription: String?
    var nextAppointment: Int?
    
    var price: Int?
    var discount: Int?
    var salePrice: Int?
    
    var voucher: VoucherModel?
    var meta: Meta?
    
    enum CodingKeys: String, CodingKey {
        case id, code, status
        case patientName
        case patientBirthYear
        case patientGender
        case patientPhone
        case patientSymptom
        case createdAt
        case updatedAt
        case doctor
        case bank
        case doctorAvailableTime
        case diagnosis, prescription
        case nextAppointment
        case cancellationReason
        case voucher
        case price, discount, salePrice
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.code = try? container.decode(String.self, forKey: .code)
        self.status = try? container.decode(AppointmentStatusModel.self, forKey: .status)
        self.patientName = try? container.decode(String.self, forKey: .patientName)
        self.patientBirthYear = try? container.decode(String.self, forKey: .patientBirthYear)
        self.patientGender = try? container.decode(String.self, forKey: .patientGender)
        self.patientPhone = try? container.decode(String.self, forKey: .patientPhone)
        self.patientSymptom = try? container.decode(String.self, forKey: .patientSymptom)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.doctor = try? container.decode(DoctorModel.self, forKey: .doctor)
        self.bank = try? container.decode(BankModel.self, forKey: .bank)
        self.doctorAvailableTime = try? container.decode(AvailableTimeModel.self, forKey: .doctorAvailableTime)
        self.diagnosis = try? container.decode(String.self, forKey: .diagnosis)
        self.prescription = try? container.decode(String.self, forKey: .prescription)
        self.nextAppointment = try? container.decode(Int.self, forKey: .nextAppointment)
        self.cancellationReason = try? container.decode(String.self, forKey: .cancellationReason)
        
        self.price = try? container.decode(Int.self, forKey: .price)
        self.discount = try? container.decode(Int.self, forKey: .discount)
        self.salePrice = try? container.decode(Int.self, forKey: .salePrice)
    }
    
    init() {
        //
    }
}

class AppointmentStatusModel: Codable {
    var id: Int?
    var name: String?
    var isFlagEnd: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case isFlagEnd
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.isFlagEnd = try? container.decode(Int.self, forKey: .isFlagEnd)
    }
    
    init() {
        //
    }
}

class AvailableTimeModel: Codable {
    var id, date, startAt, endAt: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, date
        case startAt
        case endAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.date = try? container.decode(Int.self, forKey: .date)
        self.startAt = try? container.decode(Int.self, forKey: .startAt)
        self.endAt = try? container.decode(Int.self, forKey: .endAt)
    }
}

//MARK: PaymentPeriodModel
class PaymentPeriodModel: Codable {
    var appointmentPaymentPeriod: Int?
    
    enum CodingKeys: String, CodingKey {
        case appointmentPaymentPeriod
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.appointmentPaymentPeriod = try? container.decode(Int.self, forKey: .appointmentPaymentPeriod)
    }
}
