//
//  BankModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/1/21.
//

import Foundation

// MARK: BankModel
class BankModel: Codable {
    var id: Int?
    var accountNumber, accountName, bankName, bankBranchName: String?
    var createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case accountNumber
        case accountName
        case bankName
        case bankBranchName
        case createdAt
        case updatedAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.accountNumber = try? container.decode(String.self, forKey: .accountNumber)
        self.accountName = try? container.decode(String.self, forKey: .accountName)
        self.bankName = try? container.decode(String.self, forKey: .bankName)
        self.bankBranchName = try? container.decode(String.self, forKey: .bankBranchName)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

// MARK: BankStoreModel
class BankStoreModel: Codable {
    var id: Int?
    var nameBank, nameBankShort, userReceive, account: String?
    var branch: String?
    var codeOrder: CodeOrderBank?
    
    enum CodingKeys: String, CodingKey {
        case id
        case nameBank
        case nameBankShort
        case userReceive
        case account, branch
        case codeOrder
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.nameBank = try? container.decode(String.self, forKey: .nameBank)
        self.nameBankShort = try? container.decode(String.self, forKey: .nameBankShort)
        self.userReceive = try? container.decode(String.self, forKey: .userReceive)
        self.account = try? container.decode(String.self, forKey: .account)
        self.branch = try? container.decode(String.self, forKey: .branch)
        self.codeOrder = try? container.decode(CodeOrderBank.self, forKey: .codeOrder)
    }
}

class CodeOrderBank: Codable {
    var code: String?

    enum CodingKeys: String, CodingKey {
        case code
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.code = try? container.decode(String.self, forKey: .code)
    }
}
