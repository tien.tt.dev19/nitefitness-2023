//
//  BookingCare.swift
//  1SK
//
//  Created by vuongbachthu on 9/30/21.
//

import Foundation

class BookingCare: Codable {
    var timeId: Int?
    
    var doctor: DoctorModel?
    
    var date: String?
    var time: String?
    
    var name: String?
    var birthday: String?
    var gender: Gender?
    var phone: String?
    var reason: String?
    
    var priceFee: Int?
    var priceVoucher: Int?
    var priceTotal: Int?
    
    var voucher: VoucherModel?
}
