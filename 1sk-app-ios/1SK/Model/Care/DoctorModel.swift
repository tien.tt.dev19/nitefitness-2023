//
//  DoctorModel.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import Foundation

// MARK: DoctorModel
class DoctorModel: Codable {
    var id: Int?
    var email, username: String?
    var basicInformation: BasicInformationModel?
    var cost: CostModel?
    var advanceInformation: AdvanceInformationModel?
    var specialist: [SpecialistModel]?
    var academicTitle: AcademicTitle?
    
    var voucher: VoucherModel?
    
    enum CodingKeys: String, CodingKey {
        case id, email, username
        case basicInformation
        case cost
        case advanceInformation
        case specialist
        case academicTitle
        case voucher
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.email = try? container.decode(String.self, forKey: .email)
        self.username = try? container.decode(String.self, forKey: .username)
        self.basicInformation = try? container.decode(BasicInformationModel.self, forKey: .basicInformation)
        self.cost = try? container.decode(CostModel.self, forKey: .cost)
        self.advanceInformation = try? container.decode(AdvanceInformationModel.self, forKey: .advanceInformation)
        self.specialist = try? container.decode([SpecialistModel].self, forKey: .specialist)
        self.academicTitle = try? container.decode(AcademicTitle.self, forKey: .academicTitle)
    }
}

// MARK: BasicInformationModel
class BasicInformationModel: Codable {
    var id: Int?
    var uuid, username, email: String?
    var mobile: String?
    var fullName: String?
    var skype, facebook, avatar, address: String?
    var birthday, gender, fbId, ggId: String?
    var activatedAt, emailVerifiedAt, mobileVerifiedAt, setPasswordAt: String?
    var phoneNumber, avatarUpdatedAt: String?
    var createdAt, updatedAt: String?
    var deletedAt: String?
    
    var canAccess: [CanAccessModel]?
    var lastDeviceRequest: LastDeviceRequestModel?
    
    enum CodingKeys: String, CodingKey {
        case id, uuid, username, email, mobile
        case fullName
        case skype, facebook, avatar, address, birthday, gender
        case fbId, ggId
        case activatedAt, emailVerifiedAt, mobileVerifiedAt, avatarUpdatedAt
        case setPasswordAt
        case phoneNumber
        case createdAt, updatedAt, deletedAt
        case canAccess, lastDeviceRequest
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.uuid = try? container.decode(String.self, forKey: .uuid)
        self.username = try? container.decode(String.self, forKey: .username)
        self.email = try? container.decode(String.self, forKey: .email)
        self.mobile = try? container.decode(String.self, forKey: .mobile)
        self.fullName = try? container.decode(String.self, forKey: .fullName)
        self.skype = try? container.decode(String.self, forKey: .skype)
        self.facebook = try? container.decode(String.self, forKey: .facebook)
        self.avatar = try? container.decode(String.self, forKey: .avatar)
        self.address = try? container.decode(String.self, forKey: .address)
        self.birthday = try? container.decode(String.self, forKey: .birthday)
        self.gender = try? container.decode(String.self, forKey: .gender)
        self.fbId = try? container.decode(String.self, forKey: .fbId)
        self.ggId = try? container.decode(String.self, forKey: .ggId)
        self.activatedAt = try? container.decode(String.self, forKey: .activatedAt)
        self.emailVerifiedAt = try? container.decode(String.self, forKey: .emailVerifiedAt)
        self.mobileVerifiedAt = try? container.decode(String.self, forKey: .mobileVerifiedAt)
        self.setPasswordAt = try? container.decode(String.self, forKey: .setPasswordAt)
        self.phoneNumber = try? container.decode(String.self, forKey: .phoneNumber)
        self.avatarUpdatedAt = try? container.decode(String.self, forKey: .avatarUpdatedAt)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.deletedAt = try? container.decode(String.self, forKey: .deletedAt)
        self.canAccess = try? container.decode([CanAccessModel].self, forKey: .canAccess)
        self.lastDeviceRequest = try? container.decode(LastDeviceRequestModel.self, forKey: .lastDeviceRequest)
    }
}

// MARK: CanAccessModel
class CanAccessModel: Codable {
    var name, code: String?
    var description: String?
    
    enum CodingKeys: String, CodingKey {
        case name, code
        case description
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try? container.decode(String.self, forKey: .name)
        self.code = try? container.decode(String.self, forKey: .code)
        self.description = try? container.decode(String.self, forKey: .description)
    }
}

// MARK: CostModel
class CostModel: Codable {
    var id, customerId, costPerHour: Int?
    var createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case customerId
        case costPerHour
        case createdAt
        case updatedAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.customerId = try? container.decode(Int.self, forKey: .customerId)
        self.costPerHour = try? container.decode(Int.self, forKey: .costPerHour)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

// MARK: AdvanceInformationModel
class AdvanceInformationModel: Codable {
    var id, customerId: Int?
    var officeAddress, createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case customerId
        case officeAddress
        case createdAt
        case updatedAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.customerId = try? container.decode(Int.self, forKey: .customerId)
        self.officeAddress = try? container.decode(String.self, forKey: .officeAddress)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

class AcademicTitle: Codable {
    var id: Int?
    var name, shortName: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case shortName
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.shortName = try? container.decode(String.self, forKey: .shortName)
    }
}
