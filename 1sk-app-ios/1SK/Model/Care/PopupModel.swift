//
//  PopupModel.swift
//  1SK
//
//  Created by Thaad on 09/02/2022.
//

import Foundation

enum PopupType: String {
    case None = "link_bat_ky"
    case Video = "video"
    case Blog = "bai_viet"
    case Doctor = "ho_so_bac_si"
    case Booking = "dat_lich"
}

class PopupModel: Codable {
    var id: Int?
    var name: String?
    var imageUrl: String?
    var destinationUrl: String?
    var objectId: Int?
    var frequency, position: Int?
    var startAt, endAt: String?
    var isPublished, isSent: Int?
    var createdAt, updatedAt: String?
    
    var type: String?
    var typeObj: PopupType?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case imageUrl
        case destinationUrl
        case type
        case objectId
        case frequency, position
        case startAt
        case endAt
        case isPublished
        case isSent
        case createdAt
        case updatedAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.imageUrl = try? container.decode(String.self, forKey: .imageUrl)
        self.destinationUrl = try? container.decode(String.self, forKey: .destinationUrl)
        self.objectId = try? container.decode(Int.self, forKey: .objectId)
        self.frequency = try? container.decode(Int.self, forKey: .frequency)
        self.position = try? container.decode(Int.self, forKey: .position)
        self.startAt = try? container.decode(String.self, forKey: .startAt)
        self.endAt = try? container.decode(String.self, forKey: .endAt)
        self.isPublished = try? container.decode(Int.self, forKey: .isPublished)
        self.isSent = try? container.decode(Int.self, forKey: .isSent)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        
        self.type = try? container.decode(String.self, forKey: .type)
        switch self.type {
        case PopupType.None.rawValue:
            self.typeObj = .None
            
        case PopupType.Video.rawValue:
            self.typeObj = .Video
            
        case PopupType.Blog.rawValue:
            self.typeObj = .Blog
            
        case PopupType.Doctor.rawValue:
            self.typeObj = .Doctor
            
        case PopupType.Booking.rawValue:
            self.typeObj = .Booking
            
        default:
            break
        }
    }
    
    init() {
        //
    }
    
    init(data: [String: Any]) {
        if let object = data["data"] as? [String: Any] {
//            let popup = PopupModel()
            
            self.id = object["id"] as? Int
            self.name = object["name"] as? String
            self.imageUrl = object["image_url"] as? String
            self.destinationUrl = object["destination_url"] as? String
            self.objectId = object["object_id"] as? Int
            self.frequency = object["frequency"] as? Int
            self.position = object["position"] as? Int
            self.startAt = object["start_at"] as? String
            self.endAt = object["end_at"] as? String
            self.isPublished = object["is_published"] as? Int
            self.isSent = object["is_sent"] as? Int
            self.createdAt = object["created_at"] as? String
            self.updatedAt = object["updated_at"] as? String
            
            self.type = object["type"] as? String
            switch self.type {
            case PopupType.None.rawValue:
                self.typeObj = .None
                
            case PopupType.Video.rawValue:
                self.typeObj = .Video
                
            case PopupType.Blog.rawValue:
                self.typeObj = .Blog
                
            case PopupType.Doctor.rawValue:
                self.typeObj = .Doctor
                
            case PopupType.Booking.rawValue:
                self.typeObj = .Booking
                
            default:
                break
            }
            
        }
    }
}
