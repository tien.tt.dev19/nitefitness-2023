//
//  SocketAppointmentModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/12/21.
//

import Foundation

class SocketAppointmentModel: Codable {
    var message: String?
    var appointment: AppointmentModel?
    var socket: String?
    
    init(data: [String: Any]) {
        self.message = data["message"] as? String
        
        if let appointmentObj = data["appointment"] as? [String: Any] {
            let appointment = AppointmentModel()
            appointment.id = appointmentObj["id"] as? Int
            appointment.code = appointmentObj["code"] as? String
            appointment.patientName = appointmentObj["patient_name"] as? String
            appointment.patientBirthYear = appointmentObj["patient_birth_year"] as? String
            appointment.patientGender = appointmentObj["patient_gender"] as? String
            appointment.patientPhone = appointmentObj["patient_phone"] as? String
            appointment.patientSymptom = appointmentObj["patient_symptom"] as? String
            appointment.cancellationReason = appointmentObj["cancellation_reason"] as? String
            appointment.diagnosis = appointmentObj["diagnosis"] as? String
            appointment.prescription = appointmentObj["prescription"] as? String
            
            if let statusObj = appointmentObj["status"] as? [String: Any] {
                let status = AppointmentStatusModel()
                status.id = statusObj["id"] as? Int
                status.name = statusObj["name"] as? String
                status.isFlagEnd = statusObj["is_flag_end"] as? Int
                
                appointment.status = status
            }
            
            self.appointment = appointment
        }
    }
    
//    init(from data: [String: Any]) {
//        if let paraData = try? JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.prettyPrinted) {
//            let paraJson = NSString(data: paraData, encoding: String.Encoding.utf8.rawValue)
//
//            let jsonRes = try? JSONDecoder().decode(SocketDataModel.self, from: paraData)
//
//            print("SocketDataModel paraJson")
//
//        }
//
//        print("SocketDataModel init")
//    }
}
