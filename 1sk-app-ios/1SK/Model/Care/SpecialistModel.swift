//
//  SpecialistModel.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import Foundation

class SpecialistModel: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var createdAt, updatedAt: String?
    var customerId, specialistId: Int?
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case description
        case createdAt
        case updatedAt
        case customerId
        case specialistId
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.customerId = try? container.decode(Int.self, forKey: .customerId)
        self.specialistId = try? container.decode(Int.self, forKey: .specialistId)
    }
}
