//
//  TagsModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/15/21.
//

import Foundation

class TagsModel: Codable {
    var id: Int?
    var name: String?
    var order, status: Int?
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, name, order, status
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.order = try? container.decode(Int.self, forKey: .order)
        self.status = try? container.decode(Int.self, forKey: .status)
        self.isSelected = false
    }
    
    init() {
        //
    }
}
