//
//  UserTag.swift
//  1SK
//
//  Created by tung97 on 2021/12/21.
//

import Foundation

class UserTag: Codable {
    var id: Int?
    var key: String?
    var value: String?
    var created_at: String?
    var update_at: String?
    
    enum CodingKeys: String, CodingKey {
        case id, key, value, created_at, update_at
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.key = try? container.decode(String.self, forKey: .key)
        self.value = try? container.decode(String.self, forKey: .value)
        self.created_at = try? container.decode(String.self, forKey: .created_at)
        self.update_at = try? container.decode(String.self, forKey: .update_at)
    }
    
    init() {
        //
    }
}
