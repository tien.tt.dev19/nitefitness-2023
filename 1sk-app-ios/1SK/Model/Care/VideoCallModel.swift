//
//  VideoCallModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/5/21.
//

import Foundation

class VideoCallModel: Codable {
    var uid: Int?
    var meetingId: String?
    var appId, channelName, token: String?
    var expiredAt: Int?
    var appointment: AppointmentModel?
    var code: Int?
    var message: String?
    var meta: Meta?
    
    var type: Int?
    var typeVideoCall: TypeVideoCall?
    
    enum CodingKeys: String, CodingKey {
        case uid
        case appId
        case channelName
        case token
        case expiredAt
        case appointment
        case meetingId
        case type
    }
    
    init() {
        //
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.uid = try? container.decode(Int.self, forKey: .uid)
        self.meetingId = try? container.decode(String.self, forKey: .meetingId)
        self.appId = try? container.decode(String.self, forKey: .appId)
        self.channelName = try? container.decode(String.self, forKey: .channelName)
        self.token = try? container.decode(String.self, forKey: .token)
        self.expiredAt = try? container.decode(Int.self, forKey: .expiredAt)
        self.appointment = try? container.decode(AppointmentModel.self, forKey: .appointment)
        
        self.type = try? container.decode(Int.self, forKey: .type)
        switch self.type {
        case TypeVideoCall.agora.rawValue:
            self.typeVideoCall = .agora

        case TypeVideoCall.videosdk.rawValue:
            self.typeVideoCall = .videosdk

        case TypeVideoCall.twilio.rawValue:
            self.typeVideoCall = .twilio
            
        default:
            break
        }
    }
}

// MARK: VideoCallType
enum TypeVideoCall: Int {
    case agora = 1
    case videosdk = 2
    case twilio = 3
}
