//
//  VoucherModel.swift
//  1SK
//
//  Created by vuongbachthu on 12/13/21.
//

import Foundation

class VoucherModel: Codable {
    var id: Int?
    var code, name: String?
    var description: String?
    var discountAmount: Int?
    var isFixed: Bool?
    var used, maxUses, maxUserUses: Int?
    var period, startAt, expiresAt: String?
    var updatedAt, createdAt: Int?
    var doctorIds, specialistIds: [Int]?
    
    enum CodingKeys: String, CodingKey {
        case id, code, name
        case description
        case discountAmount
        case isFixed
        case used
        case maxUses
        case maxUserUses
        case period
        case startAt
        case expiresAt
        case updatedAt
        case createdAt
        case doctorIds
        case specialistIds
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.code = try? container.decode(String.self, forKey: .code)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.discountAmount = try? container.decode(Int.self, forKey: .discountAmount)
        self.isFixed = try? container.decode(Bool.self, forKey: .isFixed)
        self.used = try? container.decode(Int.self, forKey: .used)
        self.maxUses = try? container.decode(Int.self, forKey: .maxUses)
        self.maxUserUses = try? container.decode(Int.self, forKey: .maxUserUses)
        self.period = try? container.decode(String.self, forKey: .period)
        self.startAt = try? container.decode(String.self, forKey: .startAt)
        self.expiresAt = try? container.decode(String.self, forKey: .expiresAt)
        self.updatedAt = try? container.decode(Int.self, forKey: .updatedAt)
        self.createdAt = try? container.decode(Int.self, forKey: .createdAt)
        self.doctorIds = try? container.decode([Int].self, forKey: .doctorIds)
        self.specialistIds = try? container.decode([Int].self, forKey: .specialistIds)
    }
}
