//
//  WeekdayModel.swift
//  1SK
//
//  Created by vuongbachthu on 9/29/21.
//

import Foundation

enum WeekdayState {
    case selected
    case disable
    case enable
}

class SectionWeekdayModel: NSObject {
    var section: Int = 0
    var listWeekdayModel: [WeekdayModel] = []
    var isExpand: Bool = false
}

class WeekdayModel: Codable {
    var date: Int?
    var slotTimes: [SlotTimesModel]?
    
    var isExpand: Bool?
    var isState: WeekdayState?
    var isToday: Bool?
    
    enum CodingKeys: String, CodingKey {
        case date
        case slotTimes
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.date = try? container.decode(Int.self, forKey: .date)
        self.slotTimes = try? container.decode([SlotTimesModel].self, forKey: .slotTimes)
    }
    
    init() {
        //
    }
}

class SlotTimesModel: Codable {
    var id: Int?
    var startAt, endAt: Int?
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id
        case startAt
        case endAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.startAt = try? container.decode(Int.self, forKey: .startAt)
        self.endAt = try? container.decode(Int.self, forKey: .endAt)
    }
}
