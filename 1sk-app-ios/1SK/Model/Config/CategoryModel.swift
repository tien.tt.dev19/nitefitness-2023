//
//  CategoryModel.swift
//  1SK
//
//  Created by Thaad on 05/01/2022.
//

import Foundation

class CategoryParentModel: Codable {
    var id: Int?
    var name: String?
    var parentId, type: Int?
    var children: [CategoryChildrenModel]?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case parentId
        case type
        case children
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.parentId = try? container.decode(Int.self, forKey: .parentId)
        self.type = try? container.decode(Int.self, forKey: .type)
        self.children = try? container.decode([CategoryChildrenModel].self, forKey: .children)
    }
}

class CategoryChildrenModel: Codable {
    var id: Int?
    var name: String?
    var parentId, type, order, status: Int?
    var deletedAt: String?
    var createdAt: String?
    var updatedAt: String?
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case parentId
        case type, order, status
        case deletedAt, createdAt, updatedAt
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.parentId = try? container.decode(Int.self, forKey: .parentId)
        self.type = try? container.decode(Int.self, forKey: .type)
        self.order = try? container.decode(Int.self, forKey: .order)
        self.status = try? container.decode(Int.self, forKey: .status)
        self.deletedAt = try? container.decode(String.self, forKey: .deletedAt)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}
