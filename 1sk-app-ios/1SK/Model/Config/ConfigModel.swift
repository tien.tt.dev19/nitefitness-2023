//
//  ConfigModel.swift
//  1SK
//
//  Created by Thaad on 14/09/2022.
//

import Foundation

class ConfigModel: Codable {
    var id: Int?
    var key, value: String?
    var status: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, key, value, status
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.key = try? container.decode(String.self, forKey: .key)
        self.value = try? container.decode(String.self, forKey: .value)
        self.status = try? container.decode(Int.self, forKey: .status)
    }
}

