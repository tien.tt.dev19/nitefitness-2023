//
//  BloodPressureModel.swift
//  1SK
//
//  Created by Thaad on 05/10/2022.
//

import Foundation

class BloodPressureModel: Codable {
    var systolic, diastolic: Int?
    var heartRate, group: Int?
    var dateTime: Int?
    var status: Int?
    var statusDescription: String?
    var index: BloodPressure?
    var date: Date?
    
    var device: DeviceRealm?
    
    enum CodingKeys: String, CodingKey {
        case systolic, diastolic
        case heartRate
        case dateTime
        case group, status
        case statusDescription
    }
    
    init() { }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.systolic = try? container.decode(Int.self, forKey: .systolic)
        self.diastolic = try? container.decode(Int.self, forKey: .diastolic)
        self.heartRate = try? container.decode(Int.self, forKey: .heartRate)
        self.dateTime = try? container.decode(Int.self, forKey: .dateTime)
        self.group = try? container.decode(Int.self, forKey: .group)
        
        self.status = try? container.decode(Int.self, forKey: .status)
        self.statusDescription = try? container.decode(String.self, forKey: .statusDescription)
        
        self.date = Date(timeIntervalSince1970: Double(self.dateTime ?? 0))
        
        if let sys = self.systolic, let dia = self.diastolic {
            self.index = BloodPressure.init(sys: sys, dia: dia)
        }
    }
}
