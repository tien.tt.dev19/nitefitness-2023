//
//  BpAnalysisModel.swift
//  1SK
//
//  Created by Thaad on 24/10/2022.
//

import Foundation

class BpAnalysisModel: Codable {
    var minSystolic, maxSystolic: Int?
    var minDiastolic, maxDiastolic: Int?
    var minHeartRate, maxHeartRate: Int?
    var time: Int?
    var date: Date?
    
    enum CodingKeys: String, CodingKey {
        case minSystolic
        case maxSystolic
        case minDiastolic
        case maxDiastolic
        case minHeartRate
        case maxHeartRate
        case time
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.minSystolic = try? container.decode(Int.self, forKey: .minSystolic)
        self.maxSystolic = try? container.decode(Int.self, forKey: .maxSystolic)
        self.minDiastolic = try? container.decode(Int.self, forKey: .minDiastolic)
        self.maxDiastolic = try? container.decode(Int.self, forKey: .maxDiastolic)
        self.minHeartRate = try? container.decode(Int.self, forKey: .minHeartRate)
        self.maxHeartRate = try? container.decode(Int.self, forKey: .maxHeartRate)
        self.time = try? container.decode(Int.self, forKey: .time)
        
        self.date = Date(timeIntervalSince1970: Double(self.time ?? 0))
    }
}
