//
//  DeviceRealm.swift
//  1SK
//
//  Created by Thaad on 14/09/2022.
//

import Foundation
import RealmSwift

// MARK: DeviceRealm
class DeviceRealm: Object {
    @Persisted var key: String?
    @Persisted var uuid: String?
    @Persisted var mac: String?
    @Persisted var name: String?
    @Persisted var name_display: String?
    @Persisted var code: String?
    @Persisted var type: TypeDevice?
    @Persisted var state: StateDevice?
    
    var profile: ProfileRealm?
    var icon: UIImage?
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    override init() {
        super.init()
        self.key = UUID().uuidString
    }
    
    init(type: TypeDevice) {
        self.type = type
    }
    
    init(name: String, mac: String, deviceType: TypeDevice, image: UIImage? = nil) {
        super.init()
        self.name = name
        self.mac = mac
        self.type = deviceType
    }
    
    func toDictionary() -> [String: Any] {
        var json = [String: Any]()
        json["name"] = self.name
        json["mac_address"] = self.mac
        json["device_display_name"] = self.name_display
        return json
    }
}

enum TypeDevice: Int, PersistableEnum {
    case OTHER = 0
    case SCALES = 1
    case SPO2 = 2
    case BP = 3
    case WATCH = 4
    case JUMP_ROPE = 5
    case BIKE = 6
}

enum StateDevice: Int, PersistableEnum {
    case unknown = 0
    case connected = 1
    case disconnect = 2
}
