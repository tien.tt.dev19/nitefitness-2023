//
//  ICSkipDataRealm.swift
//  1SKConnect
//
//  Created by Thaad on 18/06/2022.
//

import Foundation
import RealmSwift
import ICDeviceManager

// MARK: ICSkipDataRealm
class ICSkipDataRealm: Object {
    @Persisted var key: String?
    @Persisted var user_id: Int?
    @Persisted var is_stabilized: Bool?
    @Persisted var time: Int?
    @Persisted var mode: Int?
    @Persisted var setting: Int?
    @Persisted var elapsed_time: Int?
    @Persisted var actual_time: Int?
    @Persisted var skip_count: Int?
    @Persisted var avg_freq: Int?
    @Persisted var fastest_freq: Int?
    @Persisted var calories_burned: Double?
    @Persisted var fat_burn_efficiency: Double?
    @Persisted var freq_count: Int?
    @Persisted var most_jump: Int?
    @Persisted var device: DeviceRealm?
    @Persisted var freqs = List<ICSkipFreqDataRealm>()
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    override init() {
        super.init()
        self.key = UUID().uuidString
    }
    
    init(icSkipData: ICSkipData) {
        super.init()
        
        self.key = UUID().uuidString
        self.user_id = gUser?.id
        self.is_stabilized = icSkipData.isStabilized
        self.time = icSkipData.time.intValue
        self.mode = icSkipData.mode.rawValue.intValue
        self.setting = icSkipData.setting.intValue
        self.elapsed_time = icSkipData.elapsed_time.intValue
        self.actual_time = icSkipData.actual_time.intValue
        self.skip_count = icSkipData.skip_count.intValue
        self.avg_freq = icSkipData.avg_freq.intValue
        self.fastest_freq = icSkipData.fastest_freq.intValue
        self.calories_burned = icSkipData.calories_burned
        self.fat_burn_efficiency = icSkipData.fat_burn_efficiency
        self.freq_count = icSkipData.freq_count.intValue
        self.most_jump = icSkipData.most_jump.intValue
        self.freqs.append(objectsIn: icSkipData.freqs.map { return ICSkipFreqDataRealm(freq: $0) })
    }
    
    func toDictionary() -> [String: Any] {
        var object = [String: Any]()
        
        object["is_stable"] = self.is_stabilized ?? false
        object["time"] = self.time ?? 0
        object["mode"] = self.mode ?? 0
        object["setting"] = self.setting ?? 0
        object["elapsed_time"] = self.elapsed_time ?? 0
        object["actual_time"] = self.actual_time ?? 0
        object["skip_count"] = self.skip_count ?? 0
        object["avg_freq"] = self.avg_freq ?? 0
        object["fastest_freq"] = self.fastest_freq ?? 0
        object["calories_burned"] = self.calories_burned ?? 0
        object["fat_burn_efficiency"] = self.fat_burn_efficiency ?? 0
        object["freq_count"] = self.freq_count ?? 0
        object["most_jump"] = self.most_jump ?? 0
        object["device"] = self.device?.toDictionary()
        
        var freqs: [[String: Any]] = []
        for freq in self.freqs {
            freqs.append(freq.toDictionary())
        }
        object["freqs"] = freqs
        
        return object
    }
}

// MARK: ICSkipFreqDataRealm
class ICSkipFreqDataRealm: Object {
    @Persisted var key: String?
    @Persisted var user_id: Int?
    @Persisted var duration: Int?
    @Persisted var skip_count: Int?
    
    override class func primaryKey() -> String? {
        return "key"
    }
    
    override init() {
        super.init()
        self.key = UUID().uuidString
    }
    
    init(freq: ICSkipFreqData) {
        super.init()
        
        self.key = UUID().uuidString
        self.user_id = gUser?.id
        self.duration = Int(freq.duration)
        self.skip_count = Int(freq.skip_count)
    }
    
    func toDictionary() -> [String: Any] {
        var object = [String: Any]()
        
        object["duration"] = self.duration
        object["skip_count"] = self.skip_count
        
        return object
    }
}
