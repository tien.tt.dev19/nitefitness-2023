//
//  UserAchievementModel.swift
//  1SKConnect
//
//  Created by Thaad on 20/06/2022.
//

import Foundation

class UserAchievementModel: Codable {
    var totalCount, totalTime, jumpTimes: Int?
    var kcal: Double?
    
    enum CodingKeys: String, CodingKey {
        case totalCount
        case totalTime
        case kcal
        case jumpTimes
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.totalCount = try? container.decode(Int.self, forKey: .totalCount)
        self.totalTime = try? container.decode(Int.self, forKey: .totalTime)
        self.jumpTimes = try? container.decode(Int.self, forKey: .jumpTimes)
        self.kcal = try? container.decode(Double.self, forKey: .kcal)
    }
}
