//
//  UserRankingModel.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//

import Foundation

class UserRankingModel: Codable {
    var customerId: Int?
    var totalElapsedTime: String?
    var fullName, totalCount: String?
    var rank: Int?

    enum CodingKeys: String, CodingKey {
        case customerId
        case fullName 
        case totalCount
        case totalElapsedTime
        case rank
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.customerId = try? container.decode(Int.self, forKey: .customerId)
        self.fullName = try? container.decode(String.self, forKey: .fullName)
        self.totalCount = try? container.decode(String.self, forKey: .totalCount)
        self.totalElapsedTime = try? container.decode(String.self, forKey: .totalElapsedTime)
        self.rank = try? container.decode(Int.self, forKey: .rank)
    }
}
