//
//  UserStatisticModel.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//

import Foundation

// MARK: UserStatisticModel
class UserStatisticModel: Codable {
    var totalCount, totalTime: Int?
    var highestScore: HighestScore?
    var topCount: Int?
    
    enum CodingKeys: String, CodingKey {
        case totalCount
        case totalTime
        case highestScore
        case topCount
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.totalCount = try? container.decode(Int.self, forKey: .totalCount)
        self.totalTime = try? container.decode(Int.self, forKey: .totalTime)
        self.highestScore = try? container.decode(HighestScore.self, forKey: .highestScore)
        self.topCount = try? container.decode(Int.self, forKey: .topCount)
    }
}

// MARK: HighestScore
class HighestScore: Codable {
    var skipCount, elapsedTime: Int?
    
    enum CodingKeys: String, CodingKey {
        case skipCount
        case elapsedTime
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.skipCount = try? container.decode(Int.self, forKey: .skipCount)
        self.elapsedTime = try? container.decode(Int.self, forKey: .elapsedTime)
    }
}
