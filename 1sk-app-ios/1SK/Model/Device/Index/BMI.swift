//
//  BMI.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: BMI
enum BMI {
    case thin(Double)
    case normal(Double)
    case overWeight(Double)
    case fat_1(Double)
    case fat_2(Double)

    init(value: Double) {
        if value < 18.5 {
            self = .thin(value)
            
        } else if value < 25 {
            self = .normal(value)
            
        } else if value < 30 {
            self = .overWeight(value)
            
        } else if value < 35 {
            self = .fat_1(value)
            
        } else {
            self = .fat_2(value)
        }
    }
    
//    init(value: Double, ratingList: [Double]) {
//        if value < ratingList[0] { // 18.5
//            self = .thin(value)
//
//        } else if value < ratingList[1] { // 25
//            self = .normal(value)
//
//        } else if value < ratingList[2] { // 30
//            self = .overWeight(value)
//
//        } else if value < ratingList[3] { // 35
//            self = .fat_1(value)
//
//        } else {
//            self = .fat_2(value)
//        }
//    }
    
    init(status: String, value: Double) {
        switch status {
        case StatusIndex.THIN.rawValue:
            self = .thin(value)
            
        case StatusIndex.NORMAL.rawValue:
            self = .normal(value)
            
        case StatusIndex.OVER_WEIGHT.rawValue:
            self = .overWeight(value)
            
        case StatusIndex.FAT_1.rawValue:
            self = .fat_1(value)
            
        case StatusIndex.FAT_2.rawValue, "FAT_3":
            self = .fat_2(value)
            
        default:
            self = .normal(value)
        }
    }
}

// MARK: IndexProtocol
extension BMI: IndexProtocol {
    var code: String? {
        return "BMI"
    }

    var name: String? {
        return "BMI"
    }

    var title: String {
        return "Chỉ số BMI"
    }
    
    var status: String? {
        switch self {
        case .thin:
            return StatusIndex.THIN.rawValue
        case .normal:
            return StatusIndex.NORMAL.rawValue
        case .overWeight:
            return StatusIndex.OVER_WEIGHT.rawValue
        case .fat_1:
            return StatusIndex.FAT_1.rawValue
        case .fat_2:
            return StatusIndex.FAT_2.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .thin:
            return "Gầy"
        case .normal:
            return "Tiêu chuẩn"
        case .overWeight:
            return "Thừa cân"
        case .fat_1:
            return "Béo phì độ 1"
        case .fat_2:
            return "Béo phì độ 2"
        }
    }

    var color: UIColor? {
        switch self {
        case .thin:
            return R.color.thin()
        case .normal:
            return R.color.standard()
        case .overWeight:
            return R.color.overweight()
        case .fat_1:
            return R.color.overweight1()
        case .fat_2:
            return R.color.overweight2()
        }
    }
    
    var unit: String {
        return UnitMeasure.none.desciption
    }
    
    var minValue: Double {
        return 13.5
    }
    
    var maxValue: Double {
        return 40
    }

    var ratingsValue: [Double] {
        return [18.5, 25, 30, 35]
    }
    
    var ratingsStatus: [String] {
        return ["Gầy", "Tiêu chuẩn", "Thừa cân", "Béo phì độ 1", "Béo phì độ 2"]
    }
    
    var desc: String {
        return "Chỉ số BMI chuẩn được tính dựa trên chiều cao và cân nặng, áp dụng cho nam và nữ trưởng thành, nhằm xác định tình trạng cơ thể đang ở mức ổn định, suy dinh dưỡng hay béo phì. BMI lý tưởng của người Việt Nam (cả nam và nữ) là từ 18,5 đến 22,9. Người có BMI trong khoảng này nhưng cơ thể nhiều chất béo được xác định là “béo phì ẩn”"
    }
}
