//
//  BMR.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: BMR
enum BMR {
    case low(Double, Gender, Int)
    case optimal(Double, Gender, Int)
    
    init(value: Double, rating: Double, gender: Gender, age: Int) {
        if value < rating {
            self = .low(value, gender, age)
            
        } else {
            self = .optimal(value, gender, age)
        }
    }
    
    init(status: String, value: Double, gender: Gender, age: Int) {
        switch status {
        case StatusIndex.LOW.rawValue:
            self = .low(value, gender, age)
            
        case StatusIndex.OPTIMAL.rawValue:
            self = .optimal(value, gender, age)
            
        default:
            self = .optimal(value, gender, age)
        }
    }
}

// MARK: IndexProtocol
extension BMR: IndexProtocol {
    var code: String? {
        return "BMR"
    }
    
    var name: String? {
        return "BMR"
    }

    var title: String {
         return "Chỉ số BMR"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .optimal:
            return StatusIndex.OPTIMAL.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .optimal:
            return "Tối ưu"
        }
    }

    var color: UIColor? {
        switch self {
        case .low:
            return R.color.thin()
        case .optimal:
            return R.color.standard()
        }
    }
    
    var unit: String {
        return UnitMeasure.kcal.desciption
    }
    
    var minValue: Double {
        return 0
    }
    
    var maxValue: Double {
        guard let values = self.value as? (Double, Gender, Int) else {
            return 0
        }
        let gender = values.1
        let age = values.2
        
        if age < 30 {
            return (gender == .male ? Double(1520) : Double(1110)) * 2
            
        } else if age < 40 {
            return (gender == .male ? Double(1530) : Double(1150)) * 2
            
        } else if age < 70 {
            return (gender == .male ? Double(1400) : Double(1100)) * 2
            
        } else {
            return (gender == .male ? Double(1290) : Double(1020)) * 2
        }
    }

    var ratingsValue: [Double] {
        guard let values = self.value as? (Double, Gender, Int) else {
            return []
        }
        let gender = values.1
        let age = values.2
        
        if age < 30 {
            return gender == .male ? [1520] : [1110]
            
        } else if age < 40 {
            return gender == .male ? [1530] : [1150]
            
        } else if age < 70 {
            return gender == .male ? [1400] : [1100]
            
        } else {
            return gender == .male ? [1290] : [1020]
        }
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Tối ưu"]
    }
    
    var desc: String {
        return "Là tỉ lệ trao đổi chất cơ bản cho biết mức năng lượng (calo) tối thiếu cơ thể cần trong 24 giờ để đảm bảo hoạt động bình thường của các cơ quan (bao gồm cả trạng thái nghỉ ngơi). Chỉ số được xác định bằng năng lượng tiêu thụ bởi khối cơ bắp. Những người có BMR thường không dễ để tăng cân. Sử dụng nhiều thịt, cá và các sản phẩm từ sữa kết hợp tập thể dục có thể giúp bạn tăng khối lượng cơ"
    }
}
