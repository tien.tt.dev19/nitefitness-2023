//
//  BloodPressure.swift
//  1SKConnect
//
//  Created by admin on 22/11/2021.
//

import Foundation

enum BloodPressure: IndexProtocol {
    case LOW
    case NORMAL
    case HIGH
    case HIGH_1
    case HIGH_2

    init(sys: Int, dia: Int) {
        if dia == 0 {
            self = .NORMAL
        }
        
        switch dia {
        case 0...60:
            switch sys {
            case 0...90:
                self = .LOW
            case 91...120:
                self = .NORMAL
            case 121...140:
                self = .HIGH
            case 141...160:
                self = .HIGH_1
            default:
                self = .HIGH_2
            }
            
        case 61...80:
            switch sys {
            case 40...120:
                self = .NORMAL
            case 121...140:
                self = .HIGH
            case 141...160:
                self = .HIGH_1
            default:
                self = .HIGH_2
            }
            
        case 81...90:
            switch sys {
            case 40...140:
                self = .HIGH
            case 141...160:
                self = .HIGH_1
            default:
                self = .HIGH_2
            }
            
        case 91...100:
            switch sys {
            case 40...160:
                self = .HIGH_1
            default:
                self = .HIGH_2
            }
        default:
            self = .HIGH_2
        }
    }
    
    var code: String? {
        return "BP"
    }
    
    var name: String? {
        return "Huyết áp"
    }
    
    var title: String { return "" }
    
    var status: String? {
        switch self {
        case .LOW:
            return "LOW"
        case .NORMAL:
            return "NORMAL"
        case .HIGH:
            return "HIGH"
        case .HIGH_1:
            return "HIGH1"
        case .HIGH_2:
            return "HIGH2"
        }
    }
    
    var status_VN: String? {
        switch self {
        case .LOW:
            return "Huyết áp thấp"
        case .NORMAL:
            return "Huyết áp bình thường"
        case .HIGH:
            return "Tiền tăng huyết áp"
        case .HIGH_1:
            return "Tăng huyết áp độ 1"
        case .HIGH_2:
            return "Tăng huyết áp độ 2"
        }
    }
    
    var value: Int {
        switch self {
        case .LOW:
            return 1
        case .NORMAL:
            return 2
        case .HIGH:
            return 3
        case .HIGH_1:
            return 4
        case .HIGH_2:
            return 5
        }
    }
    
    var desc: String {
        switch self {
        case .LOW:
            return "HA Thấp"
        case .NORMAL:
            return "Bình thường"
        case .HIGH:
            return "Tiền tăng HA"
        case .HIGH_1:
            return "Tăng HA độ 1"
        case .HIGH_2:
            return "Tăng HA độ 2"
        }
    }
    
    var color: UIColor? {
        switch self {
        case .LOW:
            return R.color.biolightLow()
        case .NORMAL:
            return R.color.biolightNormal()
        case .HIGH:
            return R.color.biolightPre()
        case .HIGH_1:
            return R.color.biolightHigh1()
        case .HIGH_2:
            return R.color.biolightHigh2()
        }
    }
    
    var icon: UIImage? {
        switch self {
        case .LOW:
            return UIImage(named: "ic_nearest_bp_low")
        case .NORMAL:
            return UIImage(named: "ic_nearest_bp_normal")
        case .HIGH:
            return UIImage(named: "ic_nearest_bp_high")
        case .HIGH_1:
            return UIImage(named: "ic_nearest_bp_high1")
        case .HIGH_2:
            return UIImage(named: "ic_nearest_bp_high2")
        }
    }
    
    var unit: String { return "" }
    var maxValue: Double { return 0 }
    var minValue: Double { return 0 }
    var ratingsValue: [Double] { return [] }
    var ratingsStatus: [String] { return [] }
}
