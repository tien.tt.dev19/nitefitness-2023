//
//  BodyStandard.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

struct BodyStandard: IndexProtocol {
    init(value: Double) {
        self.value = value
    }
    
    var value: Double
    
    var code: String? {
        return "BODY_STANDARD"
    }
    
    var name: String? {
        return "Cân nặng tiêu chuẩn (kg)"
    }
    
    var title: String {
        return "Cân nặng tiêu chuẩn"
    }
    
    var status: String? {
        return "Cân nặng tiêu chuẩn"
    }
    
    var status_VN: String? {
        return ""
    }
    
    var color: UIColor? {
        return .clear
    }

    var unit: String {
        return UnitMeasure.weight.desciption
    }
    
    var minValue: Double {
        return 0
    }

    var maxValue: Double {
        return 0
    }

    var ratingsValue: [Double] {
        return []
    }

    var ratingsStatus: [String] {
        return []
    }
    
    var desc: String {
        return "Là khối lượng tương ứng phù hợp với chiều cao của mỗi người. Duy trì trọng lượng chuẩn giúp tránh một số vấn đề sức khỏe nghiêm trọng như: béo phì, tăng huyết áp, tiểu đường, suy dinh dưỡng..."
    }
}
