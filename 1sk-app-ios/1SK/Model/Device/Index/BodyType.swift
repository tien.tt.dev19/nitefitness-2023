//
//  BodyType.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation
import RealmSwift

// MARK: BodyType
enum BodyType: Int, CaseIterable, RealmEnum, PersistableEnum {
    case thin
    case standardMuscle
    case lThinMuscle
    
    case lackofexercise
    case standard
    case muscular
    
    case lFatMuscle
    case obesFat
    case muscleFat
    
    case none
    
    // Fat: LOW HEALTHY HIGH
    // Muscle: LOW NORMAL GOOD

    init(fatStatus: String, muscleStatus: String) {
        switch (fatStatus, muscleStatus) {
        case (StatusIndex.LOW.rawValue, StatusIndex.LOW.rawValue):
            self = .thin // Gầy
        case (StatusIndex.LOW.rawValue, StatusIndex.NORMAL.rawValue):
            self = .standardMuscle // Vận động viên điền kinh
        case (StatusIndex.LOW.rawValue, StatusIndex.GOOD.rawValue):
            self = .lThinMuscle // Vận động viên thể hình
            
        case (StatusIndex.HEALTHY.rawValue, StatusIndex.LOW.rawValue):
            self = .lackofexercise // Thiếu vận động
        case (StatusIndex.HEALTHY.rawValue, StatusIndex.NORMAL.rawValue):
            self = .standard // Cân đối
        case (StatusIndex.HEALTHY.rawValue, StatusIndex.GOOD.rawValue):
            self = .muscular // Cơ bắp
            
        case (StatusIndex.HIGH.rawValue, StatusIndex.LOW.rawValue):
            self = .lFatMuscle // Béo ẩn
        case (StatusIndex.HIGH.rawValue, StatusIndex.NORMAL.rawValue):
            self = .obesFat // Béo phì
        case (StatusIndex.HIGH.rawValue, StatusIndex.GOOD.rawValue):
            self = .muscleFat // Béo chắc
            
        default:
            self = .none
        }
    }
    
    init(status: String) {
        switch status {
        case StatusIndex.THIN.rawValue:
            self = .thin
            
        case StatusIndex.THIN_MUSCLE.rawValue:
            self = .lThinMuscle
            
        case StatusIndex.MUSCULAR.rawValue:
            self = .muscular
            
        case StatusIndex.LACK_OF_EXERCISE.rawValue:
            self = .lackofexercise
            
        case StatusIndex.STANDARD.rawValue:
            self = .standard
            
        case StatusIndex.STANDARD_MUSCLE.rawValue:
            self = .standardMuscle
            
        case StatusIndex.OBES_FAT.rawValue:
            self = .obesFat
            
        case StatusIndex.FAT_MUSCLE.rawValue:
            self = .lFatMuscle
            
        case StatusIndex.MUSCLE_FAT.rawValue:
            self = .muscleFat
            
        default:
            self = .none
        }
    }
    
}

// MARK: IndexProtocol
extension BodyType: IndexProtocol {
    var value: Any? {
        return self.rawValue
    }
    
    var code: String? {
        return "BODY_TYPE"
    }
    var name: String? {
        return "Vóc dáng"
    }
    
    var title: String {
        return "Vóc dáng"
    }
    
    var status: String? {
        switch self {
        case .thin:
            return StatusIndex.THIN.rawValue
        case .lThinMuscle:
            return StatusIndex.THIN_MUSCLE.rawValue
        case .muscular:
            return StatusIndex.MUSCULAR.rawValue
            
        case .lackofexercise:
            return StatusIndex.LACK_OF_EXERCISE.rawValue
        case .standard:
            return StatusIndex.STANDARD.rawValue
        case .standardMuscle:
            return StatusIndex.STANDARD_MUSCLE.rawValue
            
        case .obesFat:
            return StatusIndex.OBES_FAT.rawValue
        case .lFatMuscle:
            return StatusIndex.FAT_MUSCLE.rawValue
        case .muscleFat:
            return StatusIndex.MUSCLE_FAT.rawValue
            
        case .none:
            return "NONE"
        }
    }
    
    var status_VN: String? {
        switch self {
        case .thin:
            return "Gầy"
        case .lThinMuscle:
            return "Vận động viên thể hình"
        case .muscular:
            return "Cơ bắp"
        case .lackofexercise:
            return "Thiếu vận động"
        case .standard:
            return "Cân đối"
        case .standardMuscle:
            return "Vận động viên điền kinh"
        case .obesFat:
            return "Béo phì"
        case .lFatMuscle:
            return "Béo ẩn"
        case .muscleFat:
            return "Béo chắc"
        case .none:
            return "Không xác định"
        }
    }

    var color: UIColor? {
        return .clear
    }

    var unit: String {
        return UnitMeasure.none.desciption
    }
    
    var minValue: Double {
        return 0
    }

    var maxValue: Double {
        return 0
    }

    var ratingsValue: [Double] {
        return []
    }

    var ratingsStatus: [String] {
        return []
    }

    var desc: String {
        return "Chỉ số này phân loại vóc dáng người dùng dựa vào tỷ lệ Lượng mỡ và Lượng cơ của bạn. \nKhi bạn hoạt động và tăng cường tập luyện, lượng mỡ cơ thể sẽ giảm, chỉ số vóc dáng cũng sẽ thay đổi rõ rệt. Ngay cả khi cân nặng của bạn không đổi, sự thay đổi về tỷ lệ lượng cơ và lượng mỡ sẽ giúp bạn khỏe mạnh hơn và giảm thiểu nguy cơ dẫn tới các vấn đề về sức khỏe."
    }
}
