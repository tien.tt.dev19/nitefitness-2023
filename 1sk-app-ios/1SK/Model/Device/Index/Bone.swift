//
//  Bone.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: Bone
enum Bone {
    case low(Double, Gender)
    case normal(Double, Gender)
    case good(Double, Gender)
    
    init(value: Double, ratingList: [Double], gender: Gender) {
        if value < ratingList[0] {
            self = .low(value, gender)
            
        } else if value < ratingList[1] {
            self = .normal(value, gender)
            
        } else {
            self = .good(value, gender)
        }
    }
    
    init(status: String, value: Double, gender: Gender) {
        switch status {
        case StatusIndex.LOW.rawValue:
            self = .low(value, gender)
            
        case StatusIndex.NORMAL.rawValue:
            self = .normal(value, gender)
            
        case StatusIndex.GOOD.rawValue, "HIGHT", "HIGH", "Tốt":
            self = .good(value, gender)
            
        default:
            self = .normal(value, gender)
        }
    }

}

// MARK: IndexProtocol
extension Bone: IndexProtocol {
    var code: String? {
        return "BONE_MASS"
    }
    var name: String? {
        return "Xương (kg)"
    }

    var title: String {
        return "Khối lượng xương"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .normal:
            return StatusIndex.NORMAL.rawValue
        case .good:
            return StatusIndex.GOOD.rawValue
        }
    }

    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .normal:
            return "Tiêu chuẩn"
        case .good:
            return "Tốt"
        }
    }

    var color: UIColor? {
        switch self {
        case .low:
            return R.color.thin()
        case .normal:
            return R.color.standard()
        case .good:
            return R.color.good()
        }
    }
    
    var unit: String {
        return UnitMeasure.weight.desciption
    }
    
    var minValue: Double {
        guard let values = self.value as? (Double, Gender) else {
            return 0
        }
        let gender = values.1
        
        return gender == .male ? 2.5 : 1.8
    }

    var maxValue: Double {
        guard let values = self.value as? (Double, Gender) else {
            return 0
        }
        let gender = values.1
        
        return gender == .male ? 3.2 : 2.5
    }
    
    var ratingsValue: [Double] {
        guard let values = self.value as? (Double, Gender) else {
            return []
        }
        let gender = values.1
        
        return gender == .male ? [2.9] : [2.2]
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Tiêu chuẩn", "Tốt"]
    }
    
    var desc: String {
        return "Chỉ số thể hiện khối lượng xương trong cơ thể (xương khoáng, canxi và các khoáng chất khác). Đo khối lượng xương thường xuyên sẽ chẩn đoán tình trạng loãng xương của cơ thể. Lượng xương có quan hệ tỷ lệ thuận với lượng cơ, vì vậy, tập luyện thể thao, chế độ dinh dưỡng cân bằng và duy trì cân nặng ổn định giúp xây dựng hệ xương chắc khỏe hơn."
    }
}
