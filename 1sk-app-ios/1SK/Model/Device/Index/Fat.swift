//
//  Fat.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: Fat
enum Fat {
    case low(Double, Gender, Int)
    case healthy(Double, Gender, Int)
    case high(Double, Gender, Int)
    
    init(value: Double, ratingList: [Double], gender: Gender, age: Int) {
        if value < ratingList[0] {
            self = .low(value, gender, age)
            
        } else if value < ratingList[1] { // 7
            self = .healthy(value, gender, age)
            
        } else if value < ratingList[2] { // 18
            self = .healthy(value, gender, age)
            
        } else if value < ratingList[3] { // 28
            self = .high(value, gender, age)
            
        } else {
            self = .high(value, gender, age)
        }
    }

    init(status: String, value: Double, gender: Gender, age: Int) {
        switch status {
        case StatusIndex.LOW.rawValue, "THIN":
            self = .low(value, gender, age)
            
        case StatusIndex.HEALTHY.rawValue, "NORMAL":
            self = .healthy(value, gender, age)
            
        case StatusIndex.HIGH.rawValue, "FAT":
            self = .high(value, gender, age)
            
        default:
            self = .healthy(value, gender, age)
        }
    }
}

// MARK: IndexProtocol
extension Fat: IndexProtocol {
    var code: String? {
        return "FAT"
    }

    var name: String? {
        return "Mỡ (%)"
    }

    var title: String {
        return "Chất béo cơ thể"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .healthy:
            return StatusIndex.HEALTHY.rawValue
        case .high:
            return StatusIndex.HIGH.rawValue
        }
    }

    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .healthy:
            return "Khỏe mạnh"
        case .high:
            return "Cao"
        }
    }

    var color: UIColor? {
        switch self {
        case .low:
            return R.color.overweight()
        case .healthy:
            return R.color.standard()
        case .high:
            return R.color.overweight2()
        }
    }

    var unit: String {
        return UnitMeasure.percentage.desciption
    }
    
    var minValue: Double {
        guard let values = self.value as? (Double, Gender, Int) else {
            return 0
        }
        let gender = values.1
        let age = values.2
        
        if age <= 39 {
            return gender == .male ? 0 : 10
        } else if age >= 40 && age <= 59 {
            return gender == .male ? 0 : 13
        } else {
            return gender == .male ? 2 : 13
        }
    }

    var maxValue: Double {
        guard let values = self.value as? (Double, Gender, Int) else {
            return 0
        }
        let gender = values.1
        let age = values.2
        
        if age <= 39 {
            return gender == .male ? 30 : 43
        } else if age >= 40 && age <= 59 {
            return gender == .male ? 31 : 43
        } else {
            return gender == .male ? 35 : 46
        }
    }

    var ratingsValue: [Double] {
        guard let values = self.value as? (Double, Gender, Int) else {
            return []
        }
        let gender = values.1
        let age = values.2
        
        if age <= 39 {
            return gender == .male ? [8, 19] : [21, 32]
        } else if age >= 40 && age <= 59 {
            return gender == .male ? [11, 21] : [23, 33]
        } else {
            return gender == .male ? [13, 24] : [24, 35]
        }
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Khỏe mạnh", "Cao"]
    }
    
    var desc: String {
        return "Chỉ số thể hiện phần trăm khối lượng chất béo so với khối tượng toàn bộ cơ thể, giúp bạn nhanh chóng xác định lượng mỡ thừa, đặc biệt là mỡ xấu đang tích tụ. Chỉ số này đánh giá tình trạng sức khỏe cũng như vóc dáng cho nam giới và phụ nữ để xây dựng kế hoạch tập luyện và dinh dưỡng phù hợp.\nChỉ số này thấp cơ thể có thể bị rối loạn nội tiết \nChỉ số cao dẫn đến béo phì\nGiữ chỉ số này ở ngưỡng tiêu chuẩn với kế hoạch tập luyện và dinh dưỡng phù hợp cho bạn sức khỏe hoàn hảo, thân hình săn chắc, hấp dẫn"
    }
}
