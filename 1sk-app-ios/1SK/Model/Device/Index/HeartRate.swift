//
//  HeartRate.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: HeartRate
enum HeartRate {
    case low(Int)
    case normal(Int)
    case high(Int)
    
    init(value: Int, age: Int) {
        if age < 1 {
            if value > 140 {
                self = .high(value)
            } else if value < 80 {
                self = .low(value)
            } else {
                self = .normal(value)
            }
            
        } else if age >= 1 && age <= 2 {
            if value > 130 {
                self = .high(value)
            } else if value < 80 {
                self = .low(value)
            } else {
                self = .normal(value)
            }
            
        } else if age > 2 && age <= 6 {
            if value > 120 {
                self = .high(value)
            } else if value < 75 {
                self = .low(value)
            } else {
                self = .normal(value)
            }
            
        } else if age > 6 && age <= 12 {
            if value > 110 {
                self = .high(value)
            } else if value < 75 {
                self = .low(value)
            } else {
                self = .normal(value)
            }
            
        } else if age > 12 && age <= 17 {
            if value > 105 {
                self = .high(value)
            } else if value < 75 {
                self = .low(value)
            } else {
                self = .normal(value)
            }
            
        } else {
            if value > 90 {
                self = .high(value)
            } else if value < 50 {
                self = .low(value)
            } else {
                self = .normal(value)
            }
        }
    }
    
    init(status: String, value: Int) {
        switch status {
        case StatusIndex.LOW.rawValue, "Thấp":
            self = .low(value)
            
        case StatusIndex.NORMAL.rawValue, "Bình thường":
            self = .normal(value)
            
        case StatusIndex.HIGH.rawValue, "HIGHT", "Cao":
            self = .high(value)
            
        default:
            self = .normal(value)
        }
    }
}

// MARK: IndexProtocol
extension HeartRate: IndexProtocol {
    var code: String? {
        return "HEART_RATE"
    }
    
    var name: String? {
        return "Nhịp tim"
    }
    
    var title: String {
        return "Nhịp tim"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .normal:
            return StatusIndex.NORMAL.rawValue
        case .high:
            return StatusIndex.HIGH.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .normal:
            return "Bình thường"
        case .high:
            return "Cao"
        }
    }
    
    var color: UIColor? {
        switch self {
        case .low:
            return UIColor.init(hex: "3EAEFF")
        case .normal:
            return UIColor.init(hex: "52C41A")
        case .high:
            return UIColor.init(hex: "EB5757")
        }
    }
    
    var unit: String {
        return "bpm"
    }
    
    var minValue: Double {
        return 0
    }
    
    var maxValue: Double {
        return 0
    }
    
    var ratingsValue: [Double] {
        return [0]
    }
    
    var ratingsStatus: [String] {
        return [""]
    }
    
    var desc: String {
        return ""
    }
}
