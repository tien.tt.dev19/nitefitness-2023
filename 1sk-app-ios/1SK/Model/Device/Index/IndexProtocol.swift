//
//  IndexProtocol.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//

import Foundation

protocol IndexProtocol {
    var value: Any? { get }
    
    var code: String? { get }
    var name: String? { get }
    var title: String { get }
    var status: String? { get }
    var status_VN: String? { get }
    var color: UIColor? { get }
    var unit: String { get }
    
    var minValue: Double { get }
    var maxValue: Double { get }
    
    var ratingsValue: [Double] { get }
    var ratingsStatus: [String] { get }
    
    var desc: String { get }
}

extension IndexProtocol {
    var value: Any? {
        return Mirror(reflecting: self).children.first?.value
    }
}
