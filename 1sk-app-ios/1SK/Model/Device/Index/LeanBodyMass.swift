//
//  LeanBodyMass.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: LeanBodyMass
struct LeanBodyMass: IndexProtocol {
    init(value: Double) {
        self.value = value
    }
    
    var value: Double
    
    var code: String? {
        return "LEAN_BODY_MASS"
    }
    
    var name: String? {
        return "Khối lượng cơ thể săn chắc (kg)"
    }
    
    var title: String {
        return "Khối lượng cơ thể săn chắc"
    }
    
    var status: String? {
        return ""
    }
    
    var status_VN: String? {
        return "Khối lượng cơ thể săn chắc"
    }
    
    var color: UIColor? {
        return .clear
    }
    
    var unit: String {
        return UnitMeasure.weight.desciption
    }
    
    var minValue: Double {
        return 0
    }

    var maxValue: Double {
        return 0
    }

    var ratingsValue: [Double] {
        return []
    }
    
    var ratingsStatus: [String] {
        return []
    }

    var desc: String {
        return "Là tổng khối lượng cơ thể trừ đi tất cả khối lượng mỡ, giúp bạn đánh giá được trạng thái cơ thể, từ đó có thể điều chỉnh để giảm cân, tăng cân hoặc cải thiện một số chỉ số quan trọng phù hợp với tình trạng cơ thể. Cách tính: Trọng lượng cơ thể x (1 - tỷ lệ % mỡ cơ thể)"
    }
}
