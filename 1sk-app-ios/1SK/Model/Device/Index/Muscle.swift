//
//  Muscle.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: Muscle
enum Muscle {
    case low(Double, Gender)
    case medium(Double, Gender)
    case good(Double, Gender)

    init(value: Double, ratingList: [Double], gender: Gender) {
        if value < ratingList[0] {
            self = .low(value, gender)
            
        } else if value < ratingList[1] {
            self = .medium(value, gender)
            
        } else {
            self = .good(value, gender)
        }
    }

    init(status: String, value: Double, gender: Gender) {
        switch status {
        case StatusIndex.LOW.rawValue:
            self = .low(value, gender)
            
        case StatusIndex.NORMAL.rawValue:
            self = .medium(value, gender)
            
        case StatusIndex.GOOD.rawValue, "HIGHT", "HIGH", "Tốt":
            self = .good(value, gender)
            
        default:
            self = .medium(value, gender)
        }
    }
    
    
}

// MARK: IndexProtocol
extension Muscle: IndexProtocol {
    var code: String? {
        return "MUSCLE_MASS"
    }

    var name: String? {
        return "Cơ (kg)"
    }

    var title: String {
        return "Chỉ số cơ"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .medium:
            return StatusIndex.NORMAL.rawValue
        case .good:
            return StatusIndex.GOOD.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .medium:
            return "Tiêu chuẩn"
        case .good:
            return "Tốt"
        }
    }
    
    var color: UIColor? {
        switch self {
        case .low:
            return R.color.thin()
        case .medium:
            return R.color.standard()
        case .good:
            return R.color.good()
        }
    }
    
    var unit: String {
        return UnitMeasure.weight.desciption
    }

    var minValue: Double {
        guard let values = self.value as? (Double, Gender) else {
            return 0
        }
        let gender = values.1
        
        return gender == .male ? 22 : 17
    }
    
    var maxValue: Double {
        guard let values = self.value as? (Double, Gender) else {
            return 0
        }
        let gender = values.1
        
        return gender == .male ? 70 : 50
    }

    var ratingsValue: [Double] {
        guard let values = self.value as? (Double, Gender) else {
            return []
        }
        let gender = values.1
        
        return gender == .male ? [38, 54] : [28, 39]
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Tiêu chuẩn", "Tốt"]
    }
    
    var desc: String {
        return "Khối lượng cơ bao gồm khối lượng các cơ xương, cơ trơn như cơ tim, cơ hệ tiêu hóa và nước chứa trong các tế bào cơ. Tăng cường khối lượng cơ sẽ giúp tỷ lệ trao đổi chất cơ bản BMR tăng lên, giúp giảm lượng mỡ thừa trong cơ thể và giảm cân một cách lành mạnh."
    }
}
