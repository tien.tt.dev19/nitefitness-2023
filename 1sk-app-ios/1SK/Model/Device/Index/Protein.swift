//
//  Protein.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: Protein
enum Protein {
    case low(Double)
    case normal(Double)
    case high(Double)

    init(value: Double, ratingList: [Double]) {
        if value < ratingList[0] {
            self = .low(value)
            
        } else if value < ratingList[1] {
            self = .normal(value)
            
        } else {
            self = .high(value)
        }
    }
    
    init(status: String, value: Double) {
        switch status {
        case StatusIndex.LOW.rawValue:
            self = .low(value)
            
        case StatusIndex.NORMAL.rawValue:
            self = .normal(value)
            
        case StatusIndex.HIGH.rawValue, "HIGHT":
            self = .high(value)
            
        default:
            self = .normal(value)
        }
    }
}

// MARK: IndexProtocol
extension Protein: IndexProtocol {
    var code: String? {
        return "PROTEIN"
    }

    var name: String? {
        return "Protein (%)"
    }

    var title: String {
        return "Protein"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .normal:
            return StatusIndex.NORMAL.rawValue
        case .high:
            return StatusIndex.HIGH.rawValue
        }
    }

    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .normal:
            return "Tiêu chuẩn"
        case .high:
            return "Cao"
        }
    }

    var color: UIColor? {
        switch self {
        case .low:
            return R.color.thin()
        case .normal:
            return R.color.standard()
        case .high:
            return R.color.high()
        }
    }
    
    var unit: String {
        return UnitMeasure.percentage.desciption
    }
    
    var minValue: Double {
        return 12
    }

    var maxValue: Double {
        return 24
    }

    var ratingsValue: [Double] {
        return [16, 20]
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Tiêu chuẩn", "Cao"]
    }
    
    var desc: String {
        return "Là thành phần dinh dưỡng quan trọng cấu tạo nên các bộ phận của cơ thể. Protein không chỉ giúp cơ thể khỏe mạnh và tăng cơ bắp mà còn tốt cho xương, sụn, máu. Thêm vào đó, protein giúp nuôi dưỡng da, tóc, móng tay và các cơ quan quan trọng trong cơ thể. Thiếu hụt Protein có thể dẫn đến suy giảm miễn dịch, thiếu máu, teo cơ.\nTỷ lệ Protein trong cơ thể khỏe mạnh nằm từ 16- 20%"
    }
}
