//
//  StatusCode.swift
//  1SKConnect
//
//  Created by Elcom Corp on 02/11/2021.
//

import Foundation

enum StatusIndex: String {
    case LOW
    case THIN
    case NORMAL
    case GOOD
    case OPTIMAL
    case HIGH
    case VERY_HIGH
    case OVER_WEIGHT
    case UNDER_WEIGHT
    case FAT
    case FAT_1
    case FAT_2
    case FAT_3
    case HEALTHY
    case DANGEROUS
    case VERY_DANGEROUS
    case LACK_OF_EXERCISE
    case MUSCULAR
    case OBES_FAT
    case FAT_MUSCLE
    case MUSCLE_FAT
    case THIN_MUSCLE
    case BODY_BUILDER
    case STANDARD
    case STANDARD_MUSCLE
    
    var vn_VN: String {
        switch self {
        case .LOW:
            return "Thấp"
        case .THIN:
            return "Gầy"
        case .NORMAL:
            return "Tiêu chuẩn"
        case .GOOD:
            return "Tốt"
        case .OPTIMAL:
            return "Tối ưu"
        case .HIGH:
            return "Cao"
        case .VERY_HIGH:
            return "Rất cao"
        case .OVER_WEIGHT:
            return "Thừa cân"
        case .UNDER_WEIGHT:
            return "Thiếu cân"
        case .FAT:
            return "Mỡ"
        case .FAT_1:
            return "Béo phì độ 1"
        case .FAT_2:
            return "Béo phì độ 2"
        case .FAT_3:
            return "Béo phì độ 3"
        case .HEALTHY:
            return "Khoẻ mạnh"
        case .DANGEROUS:
            return "Nguy hiểm"
        case .VERY_DANGEROUS:
            return "Rất nguy hiểm"
        case .LACK_OF_EXERCISE:
            return "Thiếu vận động"
        case .MUSCULAR:
            return "Cơ"
        case .OBES_FAT:
            return "Béo phì"
        case .FAT_MUSCLE:
            return "Béo ẩn"
        case .MUSCLE_FAT:
            return "Béo chắc"
        case .THIN_MUSCLE:
            return "Vận động viên thể hình"
        case .BODY_BUILDER:
            return "Thể dục"
        case .STANDARD:
            return "Cân đối"
        case .STANDARD_MUSCLE:
            return "Vận động viên điền kinh"
        }
    }
}
