//
//  SubcutaneousFat.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: SubcutaneousFat - Mỡ dưới da
enum SubcutaneousFat {
    case low(Double)
    case normal(Double)
    case high(Double)
    case veryHigh(Double)
    
    init(value: Double, ratingList: [Double]) {
        if value < ratingList[0] {
            self = .low(value)
            
        } else if value < ratingList[1] {
            self = .normal(value)
            
        } else if value < ratingList[2] {
            self = .high(value)
            
        } else {
            self = .veryHigh(value)
        }
    }
    
    init(status: String, value: Double) {
        switch status {
        case StatusIndex.LOW.rawValue:
            self = .low(value)
            
        case StatusIndex.NORMAL.rawValue:
            self = .normal(value)
            
        case StatusIndex.HIGH.rawValue, "HIGHT":
            self = .high(value)
            
        case StatusIndex.VERY_HIGH.rawValue, "VERY_HIGHT":
            self = .veryHigh(value)
            
        default:
            self = .veryHigh(value)
        }
    }
}

// MARK: IndexProtocol
extension SubcutaneousFat: IndexProtocol {
    var code: String? {
        return "SUBCUTANEOUS_FAT"
    }

    var name: String? {
        return "Mỡ dưới da (%)"
    }
    
    var title: String {
        return "Mỡ dưới da"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .normal:
            return StatusIndex.NORMAL.rawValue
        case .high:
            return StatusIndex.HIGH.rawValue
        case .veryHigh:
            return StatusIndex.VERY_HIGH.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .normal:
            return "Tiêu chuẩn"
        case .high:
            return "Cao"
        case .veryHigh:
            return "Rất cao"
        }
    }

    var color: UIColor? {
        switch self {
        case .low:
            return R.color.thin()
        case .normal:
            return R.color.standard()
        case .high:
            return R.color.overweight1()
        case .veryHigh:
            return R.color.overweight2()
        }
    }

    var unit: String {
        return UnitMeasure.percentage.desciption
    }
    
    var minValue: Double {
        return 0
    }

    var maxValue: Double {
        return 28
    }

    var ratingsValue: [Double] {
        return [8.6, 16.7, 20.7]
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Tiêu chuẩn", "Cao", "Rất cao"]
    }
    
    var desc: String {
        return "Là lượng mỡ nằm ngay bên dưới da giúp cơ thể dự trữ năng lượng và điều chỉnh thân nhiệt, tỷ lệ này khác nhau phụ thuộc vào gen di truyền cũng như là các yếu tố về lối sống như mức độ hoạt động thể chất và chế độ ăn uống của mỗi người. Mỡ dưới da có chức năng như 1 lớp đệm để bảo vệ cơ và xương khỏi tác động của các cú va chạm từ bên ngoài như ngã, va đập,..."
    }
}
