//
//  VFat.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: VisceralFatLevel - Mỡ nội tạng
enum VisceralFatLevel {
    case healthy(Double)
    case dangerous(Double)
    case veryDangerous(Double)
    
    init(value: Double, ratingList: [Double]) {
        if value < ratingList[0] { // 10
            self = .healthy(value)
            
        } else if value < ratingList[1] { // 15
            self = .dangerous(value)
            
        } else {
            self = .veryDangerous(value)
        }
    }
    
    init(status: String, value: Double) {
        switch status {
        case StatusIndex.HEALTHY.rawValue:
            self = .healthy(value)
            
        case StatusIndex.DANGEROUS.rawValue:
            self = .dangerous(value)
            
        case StatusIndex.VERY_DANGEROUS.rawValue:
            self = .veryDangerous(value)
            
        default:
            self = .dangerous(value)
        }
    }
    
}

// MARK: IndexProtocol
extension VisceralFatLevel: IndexProtocol {
    var code: String? {
        return "VISCERAL_FAT"
    }

    var name: String? {
        return "Mỡ nội tạng"
    }

    var title: String {
        return "Mỡ nội tạng"
    }
    
    var status: String? {
        switch self {
        case .healthy:
            return StatusIndex.HEALTHY.rawValue
        case .dangerous:
            return StatusIndex.DANGEROUS.rawValue
        case .veryDangerous:
            return StatusIndex.VERY_DANGEROUS.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .healthy:
            return "Khỏe mạnh"
        case .dangerous:
            return "Nguy hiểm"
        case .veryDangerous:
            return "Rất nguy hiểm"
        }
    }

    var color: UIColor? {
        switch self {
        case .healthy:
            return R.color.standard()
        case .dangerous:
            return R.color.overweight1()
        case .veryDangerous:
            return R.color.overweight2()
        }
    }

    var unit: String {
        return UnitMeasure.none.desciption
    }

    var minValue: Double {
        return 5
    }
    
    var maxValue: Double {
        return 20
    }

    var ratingsValue: [Double] {
        return [10, 15]
    }
    
    var ratingsStatus: [String] {
        return ["Khoẻ mạnh", "Nguy hiểm", "Rất nguy hiểm"]
    }
    
    var desc: String {
        return "Mỡ nội tạng là một loại chất béo trong cơ thể được lưu trữ trong khoang bụng. Mỡ nội tạng dư thừa làm tăng nguy cơ phát triển một số bệnh lý nghiêm trọng kéo dài. Thường xuyên theo dõi và đảm bảo chỉ số mỡ nội tạng nằm trong mức cho phép giúp bảo vệ và nâng cao sức khỏe hiệu quả."
    }
}
