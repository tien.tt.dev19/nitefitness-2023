//
//  Water.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

// MARK: Water
enum Water {
    case low(Double, Int)
    case normal(Double, Int)
    case good(Double, Int)

    init(value: Double, ratingList: [Double], age: Int) {
        if value < ratingList[0] {
            self = .low(value, age)
            
        } else if value < ratingList[1] {
            self = .normal(value, age)
            
        } else {
            self = .good(value, age)
        }
    }
    
    init(status: String, value: Double, age: Int) {
        switch status {
        case StatusIndex.LOW.rawValue:
            self = .low(value, age)
            
        case StatusIndex.NORMAL.rawValue:
            self = .normal(value, age)
            
        case StatusIndex.GOOD.rawValue, "HIGHT", "HIGH", "Tốt":
            self = .good(value, age)
            
        default:
            self = .normal(value, age)
        }
    }
}

// MARK: IndexProtocol
extension Water: IndexProtocol {
    var code: String? {
        return "BODY_WATER"
    }

    var name: String? {
        return "Nước (%)"
    }

    var title: String {
        return "Lượng nước cơ thể"
    }
    
    var status: String? {
        switch self {
        case .low:
            return StatusIndex.LOW.rawValue
        case .normal:
            return StatusIndex.NORMAL.rawValue
        case .good:
            return StatusIndex.GOOD.rawValue
        }
    }
    
    var status_VN: String? {
        switch self {
        case .low:
            return "Thấp"
        case .normal:
            return "Tiêu chuẩn"
        case .good:
            return "Tốt"
        }
    }

    var color: UIColor? {
        switch self {
        case .low:
            return R.color.thin()
        case .normal:
            return R.color.standard()
        case .good:
            return R.color.good()
        }
    }
    
    var unit: String {
        return UnitMeasure.percentage.desciption
    }
    
    var minValue: Double {
        guard let values = self.value as? (Double, Int) else {
            return 0
        }
        let age = values.1
        
        if age <= 18 {
            return 72
        } else if age >= 19 && age <= 45 {
            return 35
        } else {
            return 0
        }
    }

    var maxValue: Double {
        guard let values = self.value as? (Double, Int) else {
            return 0
        }
        let age = values.1
        
        if age <= 18 {
            return 81
            
        } else if age >= 19 && age <= 45 {
            return 80
            
        } else {
            return 75
        }
    }

    var ratingsValue: [Double] {
        guard let values = self.value as? (Double, Int) else {
            return []
        }
        let age = values.1
        
        if age <= 18 {
            return [75, 78]
            
        } else if age >= 19 && age <= 45 {
            return [50, 65]
            
        } else {
            return [25, 50]
        }
    }
    
    var ratingsStatus: [String] {
        return ["Thấp", "Tiêu chuẩn", "Tốt"]
    }
    
    var desc: String {
        return "Là tỷ lệ giữa toàn bộ lượng nước (bao gồm: nước trong máu, bạch huyết, dịch cơ thể,...) và trọng lượng cơ thể. Mất nước có thể gây ảnh hưởng tiêu cực đến sức khỏe con người. Theo dõi % lượng nước toàn phần tại một thời điểm nhất định trong ngày để đánh giá chính xác những thay đổi của cơ thể."
    }
}
