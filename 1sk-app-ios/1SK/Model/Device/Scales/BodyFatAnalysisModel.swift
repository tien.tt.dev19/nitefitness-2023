//
//  BodyFatAnalysisModel.swift
//  1SK
//
//  Created by Thaad on 28/10/2022.
//

import Foundation

class BodyFatAnalysisModel: Codable {
    var weight, muscleMass, boneMass, bodyWater: [BodyFatEntry]?
    var protein, fat, subcutaneousFat: [BodyFatEntry]?
    
    enum CodingKeys: String, CodingKey {
        case weight
        case bodyWater
        case protein, fat
        case subcutaneousFat
        case muscleMass
        case boneMass
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.weight = try? container.decode([BodyFatEntry].self, forKey: .weight)
        self.muscleMass = try? container.decode([BodyFatEntry].self, forKey: .muscleMass)
        self.boneMass = try? container.decode([BodyFatEntry].self, forKey: .boneMass)
        self.bodyWater = try? container.decode([BodyFatEntry].self, forKey: .bodyWater)
        self.protein = try? container.decode([BodyFatEntry].self, forKey: .protein)
        self.fat = try? container.decode([BodyFatEntry].self, forKey: .fat)
        self.subcutaneousFat = try? container.decode([BodyFatEntry].self, forKey: .subcutaneousFat)
        
        let count = self.weight?.count
        
        self.weight?.forEach({ value in
            value.unit = "Kg"
            
            if let time = value.time {
                switch count {
                case 24, 25:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    value.label = "\(count ?? 0)"
                }
            }
        })
        
        self.muscleMass?.forEach({ value in
            value.unit = "Kg"
            
            if let time = value.time {
                switch count {
                case 24, 25:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    value.label = "\(count ?? 0)"
                }
            }
        })
        
        self.boneMass?.forEach({ value in
            value.unit = "Kg"
            
            if let time = value.time {
                switch count {
                case 24, 25:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    value.label = "\(count ?? 0)"
                }
            }
        })
        
        self.bodyWater?.forEach({ value in
            value.unit = "%"
            
            if let time = value.time {
                switch count {
                case 24, 25:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    value.label = "\(count ?? 0)"
                }
            }
        })
        
        self.protein?.forEach({ value in
            value.unit = "%"
            
            if let time = value.time {
                switch count {
                case 24, 25:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    value.label = "\(count ?? 0)"
                }
            }
        })
        
        self.fat?.forEach({ value in
            value.unit = "%"
            
            if let time = value.time {
                switch count {
                case 24, 25:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    value.label = "\(count ?? 0)"
                }
            }
        })
        
        self.subcutaneousFat?.forEach({ value in
            value.unit = "%"
            
            if let time = value.time {
                switch count {
                case 24:
                    value.label = TimeInterval(time).toDate().toString(.h)
                case 7:
                    value.label = TimeInterval(time).toDate().toString(.eee)
                case 28, 29, 30, 31:
                    value.label = TimeInterval(time).toDate().toString(.dd)
                case 12:
                    value.label = TimeInterval(time).toDate().toString(.MM)
                default:
                    break
                }
            }
        })
        
        if count == 24 {
            let entry = BodyFatEntry.init(avg: 0, unit: "", label: "24")
            self.weight?.append(entry)
            self.muscleMass?.append(entry)
            self.boneMass?.append(entry)
            self.bodyWater?.append(entry)
            self.protein?.append(entry)
            self.fat?.append(entry)
            self.subcutaneousFat?.append(entry)
        }
    }
}

// MARK: BodyFatEntry
class BodyFatEntry: Codable {
    var avg: Double?
    var time: Int?
    
    var unit: String?
    var label: String?
    var timeString: String?
    
    enum CodingKeys: String, CodingKey {
        case avg, time
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.time = try? container.decode(Int.self, forKey: .time)
        self.avg = 0
        
        if let _avg = try? container.decode(Double.self, forKey: .avg) {
            self.avg = _avg.rounded(toPlaces: 2)
        }
        
        if let _time = self.time {
            self.timeString = TimeInterval(_time).toDate().toString(.dmySlash)
        }
    }
    
    init(avg: Double, unit: String, label: String) {
        self.avg = avg
        self.unit = unit
        self.label = label
    }
}

extension BodyFatEntry: Comparable {
    static func < (lhs: BodyFatEntry, rhs: BodyFatEntry) -> Bool {
        return lhs.avg ?? 0 < rhs.avg ?? 0
    }
    static func == (lhs: BodyFatEntry, rhs: BodyFatEntry) -> Bool {
        return lhs.avg ?? 0 == rhs.avg ?? 0
    }
}
