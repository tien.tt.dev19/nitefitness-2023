//
//  BodyFatChart.swift
//  1SKConnect
//
//  Created by Thaad on 30/07/2022.
//

import Foundation

// MARK: BodyFatChart
class BodyFatChart: NSObject {
    var valueAxisX: [String]?
    var weightPoints: [PointChart]?
    var musclePoints: [PointChart]?
    var bonePoints: [PointChart]?
    var waterPoints: [PointChart]?
    var proteinPoints: [PointChart]?
    var fatPoints: [PointChart]?
    var subcutaneousFatPoints: [PointChart]?
    
    var startTime: String?
    var endTime: String?
    
    override init() {
        //
    }
    
    init(state: TimeType?, listBodyFat: [BodyFatModel], startTime: String?, endTime: String?) {
        self.weightPoints = []
        self.musclePoints = []
        self.bonePoints = []
        self.waterPoints = []
        self.proteinPoints = []
        self.fatPoints = []
        self.subcutaneousFatPoints = []
        
        self.startTime = startTime
        self.endTime = endTime
        
        switch state {
        case .day:
            self.valueAxisX = ["00", "01", "03", "04", "05", "06", "07", "08",
                               "09", "10", "11", "12", "13", "14", "15", "16",
                               "17", "18", "19", "20", "21", "22", "23", "24"]
            
            for (index, hour) in self.valueAxisX!.enumerated() {
                let listFilter = listBodyFat.filter { $0.hour == hour}
                if listFilter.count > 0 {
                    print("setDataLineChart hour: \(hour)")
                    
                    let valueX = Double(index)
                    
                    let weightPoint = PointChart.init(type: .weight, valueX: valueX, listFilter: listFilter)
                    self.weightPoints?.append(weightPoint)
                    
                    let musclePoint = PointChart.init(type: .muscle, valueX: valueX, listFilter: listFilter)
                    self.musclePoints?.append(musclePoint)
                    
                    let bonePoint = PointChart.init(type: .bone, valueX: valueX, listFilter: listFilter)
                    self.bonePoints?.append(bonePoint)
                    
                    let waterPoint = PointChart.init(type: .water, valueX: valueX, listFilter: listFilter)
                    self.waterPoints?.append(waterPoint)
                    
                    let proteinPoint = PointChart.init(type: .protein, valueX: valueX, listFilter: listFilter)
                    self.proteinPoints?.append(proteinPoint)
                    
                    let fatPoint = PointChart.init(type: .fat, valueX: valueX, listFilter: listFilter)
                    self.fatPoints?.append(fatPoint)
                    
                    let subcutaneousFatPoint = PointChart.init(type: .subcutaneousFat, valueX: valueX, listFilter: listFilter)
                    self.subcutaneousFatPoints?.append(subcutaneousFatPoint)
                }
            }
            
        case .week:
            self.valueAxisX = ["Th 2", "Th 3", "Th 4", "Th 5", "Th 6", "Th 7", "CN"]
            for (index, dayWeek) in self.valueAxisX!.enumerated() {
                let listFilter = listBodyFat.filter { $0.dayWeek == dayWeek}
                print("setDataLineChart dayWeek: \(dayWeek) - listFilter.count: \(listFilter.count)")
                
                if listFilter.count > 0 {
                    print("setDataLineChart dayWeek: \(dayWeek)")
                    
                    let valueX = Double(index)
                    
                    let weightPoint = PointChart.init(type: .weight, valueX: valueX, listFilter: listFilter)
                    self.weightPoints?.append(weightPoint)
                    
                    let musclePoint = PointChart.init(type: .muscle, valueX: valueX, listFilter: listFilter)
                    self.musclePoints?.append(musclePoint)
                    
                    let bonePoint = PointChart.init(type: .bone, valueX: valueX, listFilter: listFilter)
                    self.bonePoints?.append(bonePoint)
                    
                    let waterPoint = PointChart.init(type: .water, valueX: valueX, listFilter: listFilter)
                    self.waterPoints?.append(waterPoint)
                    
                    let proteinPoint = PointChart.init(type: .protein, valueX: valueX, listFilter: listFilter)
                    self.proteinPoints?.append(proteinPoint)
                    
                    let fatPoint = PointChart.init(type: .fat, valueX: valueX, listFilter: listFilter)
                    self.fatPoints?.append(fatPoint)
                    
                    let subcutaneousFatPoint = PointChart.init(type: .subcutaneousFat, valueX: valueX, listFilter: listFilter)
                    self.subcutaneousFatPoints?.append(subcutaneousFatPoint)
                }
            }
            
        case .month:
            self.valueAxisX = ["01", "03", "04", "05", "06", "07", "08", "09", "10", "11",
                               "12", "13", "14", "15", "16", "17", "18", "19", "20", "21",
                               "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
            for (index, day) in self.valueAxisX!.enumerated() {
                let listFilter = listBodyFat.filter { $0.day == Int(day)}
                if listFilter.count > 0 {
                    print("setDataLineChart day: \(day)")
                    
                    let valueX = Double(index)
                    
                    let weightPoint = PointChart.init(type: .weight, valueX: valueX, listFilter: listFilter)
                    self.weightPoints?.append(weightPoint)
                    
                    let musclePoint = PointChart.init(type: .muscle, valueX: valueX, listFilter: listFilter)
                    self.musclePoints?.append(musclePoint)
                    
                    let bonePoint = PointChart.init(type: .bone, valueX: valueX, listFilter: listFilter)
                    self.bonePoints?.append(bonePoint)
                    
                    let waterPoint = PointChart.init(type: .water, valueX: valueX, listFilter: listFilter)
                    self.waterPoints?.append(waterPoint)
                    
                    let proteinPoint = PointChart.init(type: .protein, valueX: valueX, listFilter: listFilter)
                    self.proteinPoints?.append(proteinPoint)
                    
                    let fatPoint = PointChart.init(type: .fat, valueX: valueX, listFilter: listFilter)
                    self.fatPoints?.append(fatPoint)
                    
                    let subcutaneousFatPoint = PointChart.init(type: .subcutaneousFat, valueX: valueX, listFilter: listFilter)
                    self.subcutaneousFatPoints?.append(subcutaneousFatPoint)
                }
            }
            
        case .year:
            self.valueAxisX = ["01", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
            
            for (index, month) in self.valueAxisX!.enumerated() {
                let listFilter = listBodyFat.filter { $0.month == Int(month)}
                if listFilter.count > 0 {
                    print("setDataLineChart month: \(month)")
                    
                    let valueX = Double(index)
                    
                    let weightPoint = PointChart.init(type: .weight, valueX: valueX, listFilter: listFilter)
                    self.weightPoints?.append(weightPoint)
                    
                    let musclePoint = PointChart.init(type: .muscle, valueX: valueX, listFilter: listFilter)
                    self.musclePoints?.append(musclePoint)
                    
                    let bonePoint = PointChart.init(type: .bone, valueX: valueX, listFilter: listFilter)
                    self.bonePoints?.append(bonePoint)
                    
                    let waterPoint = PointChart.init(type: .water, valueX: valueX, listFilter: listFilter)
                    self.waterPoints?.append(waterPoint)
                    
                    let proteinPoint = PointChart.init(type: .protein, valueX: valueX, listFilter: listFilter)
                    self.proteinPoints?.append(proteinPoint)
                    
                    let fatPoint = PointChart.init(type: .fat, valueX: valueX, listFilter: listFilter)
                    self.fatPoints?.append(fatPoint)
                    
                    let subcutaneousFatPoint = PointChart.init(type: .subcutaneousFat, valueX: valueX, listFilter: listFilter)
                    self.subcutaneousFatPoints?.append(subcutaneousFatPoint)
                }
            }
        default:
            break
        }
    }
}

// MARK: PointChart
class PointChart: NSObject {
    var valueX: Double?
    var valueY: Double?
    var time: String?
    var unit: String?
    var status: String?
    var type: BodyFatType?
    
    override init() {
        //
    }
    
    init(type: BodyFatType?, valueX: Double?, listFilter: [BodyFatModel]) {
        self.type = type
        self.valueX = valueX
        self.time = listFilter.first?.date?.toString(.dmySlash)
        
        let bodyFat = listFilter.first
        
        switch type {
        case .weight:
            let sum = listFilter.map({$0.weight ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
//            self.status = BMI(value: average).status_VN
            self.unit = "Kg"
            
        case .muscle:
            let sum = listFilter.map({$0.muscleMass ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
//            self.status = Muscle.init(muscle: average).status
            self.unit = "Kg"
            
//            if let _status = bodyFat?.statusMuscle, let _value = bodyFat?.muscleMass {
//                self.status = Muscle.init(status: _status, value: _value).status
//            } else {
//                self.status = bodyFat?.statusMuscle
//            }
            
        case .bone:
            let sum = listFilter.map({$0.boneMass ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
            self.unit = "Kg"
            
//            if let _status = bodyFat?.statusBoneMass, let _value = bodyFat?.boneMass {
//                self.status = Bone.init(status: _status, value: _value).status
//            } else {
//                self.status = bodyFat?.statusBoneMass
//            }
            
        case .water:
            let sum = listFilter.map({$0.bodyWater ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
            self.unit = "%"
             
//            if let _value = bodyFat?.bodyWater {
//                self.status = Water.init(value: _value).status
//            } else {
//                self.status = bodyFat?.statusBodyWater
//            }
            
        case .protein:
            let sum = listFilter.map({$0.protein ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
            self.unit = "%"
             
//            if let _value = bodyFat?.protein {
//                self.status = Protein.init(value: _value).statusCode
//            } else {
//                self.status = bodyFat?.statusProtein
//            }
            
        case .fat:
            let sum = listFilter.map({$0.fat ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
            self.unit = ""
             
//            if let status = bodyFat?.statusFat, let value = bodyFat?.fat {
//                self.status = Bone.init(status: status, value: value).status
//            } else {
//                self.status = bodyFat?.statusBoneMass
//            }
            
        case .subcutaneousFat:
            let sum = listFilter.map({$0.subcutaneousFat ?? 0}).reduce(0, +)
            let average = sum / Double(listFilter.count)
            self.valueY = average.rounded(toPlaces: 2)
            self.unit = "%"
             
//            if let status = bodyFat?.statusSubcutaneousFat, let value = bodyFat?.subcutaneousFat {
//                self.status = Bone.init(status: status, value: value).status
//            } else {
//                self.status = bodyFat?.statusSubcutaneousFat
//            }
            
        case .none:
            break
        }
    }
}
