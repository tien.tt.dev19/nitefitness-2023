//
//  BodyFatModel.swift
//  1SKConnect
//
//  Created by Thaad on 28/07/2022.
//

import Foundation

class BodyFatModel: Codable {
    var id: String?
    var userId: Int?
    var device: String?
    var mac: String?
    var scaleType: String?
    
    var weight: Double?
    var statusWeight: String?
    
    var heartRate: Int?
    var statusHeartRate: String?
    
    var bmi: Double?
    var statusBmi: String?
    var bmiRating: [Double]?
    
    var bmr: Double?
    var statusBmr: String?
    var bmrRating: [Double]?
    
    var muscleMass: Double?
    var statusMuscle: String?
    
    var boneMass: Double?
    var statusBoneMass: String?
    
    var bodyWater: Double?
    var statusBodyWater: String?
    
    var protein: Double?
    var statusProtein: String?
    
    var fatLevel: Double?
    var statusFatLevel: String?
    
    var fat: Double?
    var statusFat: String?
    var fatRating: [Double]?
    
    var visceralFat: Double?
    var statusVisceralFat: String?
    
    var subcutaneousFat: Double?
    var statusSubcutaneousFat: String?
    
    var leanBodyMass: Double?
    var statusLeanBodyMass: String?
    
    var standartWeight: Double?
    var statusStandartWeight: String?
    
    var bodyType: String?
    
    var impedance: Int?
    
    var metaData: [String]?
    var createdAt: String?
    
    var date: Date?
    var hour: String?
    var day: Int?
    var dayWeek: String?
    var month: Int?
    var time: String?
    var timestamp: Int?
    
    var age: Int?
    var gender: Gender?
    var height: Double?
    
    var idealWeight: Double?
    var bodyStandard: Double?
    
    var bodyFatSDK: BodyFatSDK?
    
    enum CodingKeys: String, CodingKey {
        case id, device
        case userId
        case mac
        case scaleType
        case weight
        case statusWeight
        case bmi
        case statusBmi
        case bmr
        case statusBmr
        case muscleMass
        case statusMuscle
        case boneMass
        case statusBoneMass
        case bodyWater
        case statusBodyWater
        case protein
        case statusProtein
        case fatLevel
        case statusFatLevel
        case fat
        case statusFat
        case visceralFat
        case statusVisceralFat
        case subcutaneousFat
        case statusSubcutaneousFat
        case leanBodyMass
        case statusLeanBodyMass
        case standartWeight
        case statusStandartWeight
        case bodyType
        case metaData
        case createdAt
        case impedance
        case heartRate
        case statusHeartRate
        
        case age
        case gender
        case height
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(String.self, forKey: .id)
        self.device = try? container.decode(String.self, forKey: .device)
        self.userId = try? container.decode(Int.self, forKey: .userId)
        self.mac = try? container.decode(String.self, forKey: .mac)
        self.scaleType = try? container.decode(String.self, forKey: .scaleType)
        self.weight = try? container.decode(Double.self, forKey: .weight)
        self.statusWeight = try? container.decode(String.self, forKey: .statusWeight)
        self.bmi = try? container.decode(Double.self, forKey: .bmi)
        self.statusBmi = try? container.decode(String.self, forKey: .statusBmi)
        self.bmr = try? container.decode(Double.self, forKey: .bmr)
        self.statusBmr = try? container.decode(String.self, forKey: .statusBmr)
        self.muscleMass = try? container.decode(Double.self, forKey: .muscleMass)
        self.statusMuscle = try? container.decode(String.self, forKey: .statusMuscle)
        self.boneMass = try? container.decode(Double.self, forKey: .boneMass)
        self.statusBoneMass = try? container.decode(String.self, forKey: .statusBoneMass)
        self.bodyWater = try? container.decode(Double.self, forKey: .bodyWater)
        self.statusBodyWater = try? container.decode(String.self, forKey: .statusBodyWater)
        self.protein = try? container.decode(Double.self, forKey: .protein)
        self.statusProtein = try? container.decode(String.self, forKey: .statusProtein)
        self.fatLevel = try? container.decode(Double.self, forKey: .fatLevel)
        self.statusFatLevel = try? container.decode(String.self, forKey: .statusFatLevel)
        self.fat = try? container.decode(Double.self, forKey: .fat)
        self.statusFat = try? container.decode(String.self, forKey: .statusFat)
        self.visceralFat = try? container.decode(Double.self, forKey: .visceralFat)
        self.statusVisceralFat = try? container.decode(String.self, forKey: .statusVisceralFat)
        self.subcutaneousFat = try? container.decode(Double.self, forKey: .subcutaneousFat)
        self.statusSubcutaneousFat = try? container.decode(String.self, forKey: .statusSubcutaneousFat)
        self.leanBodyMass = try? container.decode(Double.self, forKey: .leanBodyMass)
        self.statusLeanBodyMass = try? container.decode(String.self, forKey: .statusLeanBodyMass)
        self.standartWeight = try? container.decode(Double.self, forKey: .standartWeight)
        self.statusStandartWeight = try? container.decode(String.self, forKey: .statusStandartWeight)
        self.bodyType = try? container.decode(String.self, forKey: .bodyType)
        self.metaData = try? container.decode([String].self, forKey: .metaData)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.impedance = try? container.decode(Int.self, forKey: .impedance)
        self.heartRate = try? container.decode(Int.self, forKey: .heartRate)
        self.statusHeartRate = try? container.decode(String.self, forKey: .statusHeartRate)
        
        self.age = try? container.decode(Int.self, forKey: .age)
        self.height = try? container.decode(Double.self, forKey: .height)
        
        let genderStr = try? container.decode(String.self, forKey: .gender)
        switch genderStr {
        case "male":
            self.gender = .male
            
        case "female":
            self.gender = .female
            
        default:
            break
        }
        
        if let time = self.createdAt {
            if let dateCreated = time.toDate(.ymdhms) {
                self.date = dateCreated
                self.hour = dateCreated.toString(.h)
                self.day = dateCreated.toInt(.dd)
                self.dayWeek = dateCreated.toString(.eee)
                self.month = dateCreated.toInt(.MM)
                self.time = dateCreated.toString(.dmySlash)
                self.timestamp = dateCreated.toTimestamp()
            }
        }
    }
}

// MARK: BodyFatSDK
extension BodyFatModel {
    func setBodyFatSDK(profile: ProfileRealm?) {
        // Add Param profile fixbug:
        // Main Thread Checker: UI API called on a background thread: -[UIView init]
        // PID: 469, TID: 9387, Thread name: (none), Queue name: org.alamofire.session.rootQueue.serializationQueue, QoS: 0
        guard
            let impedance = self.impedance,
            let weight = self.weight,
            let height = self.height ?? profile?.height,
            let age = self.age ?? profile?.age,
            let gender = self.gender ?? profile?.gender
        else {
            SKToast.shared.showToast(content: "Lỗi hồ sơ người dùng không đầy đủ")
            return
        }
        
        self.bodyFatSDK = BodyFatSDK(impedance: impedance, weight: Float(weight), height: height, age: age, gender: gender, isMeasure: nil)
        self.bodyFatSDK?.weight = self.weight
        self.bodyFatSDK?.impedance = self.impedance
        
        self.bodyFatSDK?.heartRate = self.heartRate
        if let index = self.heartRateIndex {
            self.bodyFatSDK?.heartRateIndex = index
        }

        self.bodyFatSDK?.bmi = self.bmi
        if let index = self.bmiIndex {
            self.bodyFatSDK?.bmiIndex = index
        }

        self.bodyFatSDK?.bmr = self.bmr
        if let index = self.bmrIndex {
            self.bodyFatSDK?.bmrIndex = index
        }

        self.bodyFatSDK?.muscleKg = self.muscleMass
        if let index = self.muscleIndex {
            self.bodyFatSDK?.muscleIndex = index
        }

        self.bodyFatSDK?.boneKg = self.boneMass
        if let index = self.boneIndex {
            self.bodyFatSDK?.boneIndex = index
        }

        self.bodyFatSDK?.waterPercentage = self.bodyWater
        if let index = self.waterIndex {
            self.bodyFatSDK?.waterIndex = index
        }

        self.bodyFatSDK?.proteinPercentage = self.protein
        if let index = self.proteinIndex {
            self.bodyFatSDK?.proteinIndex = index
        }

        self.bodyFatSDK?.fatPercentage = self.fat
        if let index = self.fatIndex {
            self.bodyFatSDK?.fatIndex = index
        }

        self.bodyFatSDK?.obesityFatLevel = self.fatLevel
        if let index = self.obesityFatLevelIndex {
            self.bodyFatSDK?.obesityFatLevelIndex = index
        }

        self.bodyFatSDK?.visceralFatLevel = self.visceralFat
        if let index = self.visceralFatLevelIndex {
            self.bodyFatSDK?.visceralFatLevelIndex = index
        }

        self.bodyFatSDK?.subcutaneousFat = self.subcutaneousFat
        if let index = self.subcutaneousFatIndex {
            self.bodyFatSDK?.subcutaneousFatIndex = index
        }

        self.bodyFatSDK?.leanBodyMass = self.leanBodyMass
        if let index = self.leanBodyMassIndex {
            self.bodyFatSDK?.leanBodyMassIndex = index
        }

        self.bodyFatSDK?.bodyStandard = self.bodyStandard
        if let index = self.bodyStandardIndex {
            self.bodyFatSDK?.bodyStandardIndex = index
        }
        
        if let bodyType = self.bodyType, bodyType.count > 0 {
            self.bodyFatSDK?.bodyType = BodyType(status: bodyType)
        }
        if let bodyTypeIndex = self.bodyTypeIndex {
            self.bodyFatSDK?.bodyTypeIndex = bodyTypeIndex
        }

        self.bodyFatSDK?.age = self.age
        self.bodyFatSDK?.createAt = self.date?.toString(.ymdhms)
        self.bodyFatSDK?.timestamp = self.date?.timeIntervalSince1970
    }
}

// MARK: IndexProtocol
extension BodyFatModel {
    
    // MARK: HeartRate
    private var heartRateIndex: IndexProtocol? {
        guard let value = self.heartRate, value > 0 else {
            return nil
        }
        if let status = self.statusHeartRate {
            return HeartRate(status: status, value: value)
        } else {
            guard let age = self.age, age > 0 else {
                return nil
            }
            return HeartRate(value: value, age: age)
        }
    }
    
    // MARK: BMI
    private var bmiIndex: IndexProtocol? {
        guard let status = self.statusBmi, let value = self.bmi, value > 0 else {
            return nil
        }
        return BMI(status: status, value: value)
    }
    
    // MARK: BMR
    private var bmrIndex: IndexProtocol? {
        guard
            let status = self.statusBmr,
            let value = self.bmr, value > 0,
            let gender = self.gender,
            let age = self.age
        else {
            return nil
        }
        return BMR(status: status, value: value, gender: gender, age: age)
    }
    
    // MARK: Muscle
    private var muscleIndex: IndexProtocol? {
        guard
            let status = self.statusMuscle,
            let value = self.muscleMass, value > 0,
            let gender = self.gender
        else {
            return nil
        }
        return Muscle(status: status, value: value, gender: gender)
    }
    
    // MARK: Bone
    private var boneIndex: IndexProtocol? {
        guard
            let status = self.statusBoneMass,
            let value = self.boneMass, value > 0,
            let gender = self.gender
        else {
            return nil
        }
        return Bone(status: status, value: value, gender: gender)
    }
    
    // MARK: Water
    private var waterIndex: IndexProtocol? {
        guard
            let status = self.statusBodyWater,
            let value = self.bodyWater, value > 0,
            let age = self.age
        else {
            return nil
        }
        return Water(status: status, value: value, age: age )
    }
    
    // MARK: Protein
    private var proteinIndex: IndexProtocol? {
        guard let status = self.statusProtein, let value = self.protein, value > 0 else {
            return nil
        }
        return Protein(status: status, value: value)
    }
    
    // MARK: Fat
    private var fatIndex: IndexProtocol? {
        guard
            let status = self.statusFat,
            let value = self.fat, value > 0,
            let gender = self.gender,
            let age = self.age
        else {
            return nil
        }
        return Fat(status: status, value: value, gender: gender, age: age)
    }
    
    // MARK: Obesity Fat Level
    private var obesityFatLevelIndex: IndexProtocol? {
        guard
            let status = self.statusFatLevel,
            let value = self.fatLevel, value > 0
        else {
            return nil
        }
        return ObesityFatLevel(status: status, value: value)
    }
    
    // MARK: Visceral Fat
    private var visceralFatLevelIndex: IndexProtocol? {
        guard
            let status = self.statusVisceralFat,
            let value = self.visceralFat, value > 0
        else {
            return nil
        }
        return VisceralFatLevel(status: status, value: value)
    }
    
    // MARK: Subcutaneous Fat
    private var subcutaneousFatIndex: IndexProtocol? {
        guard
            let status = self.statusSubcutaneousFat,
            let value = self.subcutaneousFat, value > 0
        else {
            return nil
        }
        return SubcutaneousFat(status: status, value: value)
    }
    
    // MARK: LeanBodyMass
    private var leanBodyMassIndex: IndexProtocol? {
        guard let value = self.leanBodyMass, value > 0 else {
            return nil
        }
        return LeanBodyMass(value: value)
    }
    
    // MARK: Body Standard
    private var bodyStandardIndex: IndexProtocol? {
        guard let value = self.standartWeight, value > 0 else {
            return nil
        }
        return BodyStandard(value: value)
    }
    
    // MARK: BodyType
    private var bodyTypeIndex: IndexProtocol? {
        if let fatStatus = self.fatIndex?.status, let muscleStatus = self.muscleIndex?.status {
            return BodyType(fatStatus: fatStatus, muscleStatus: muscleStatus)
            
        } else {
            guard let status = self.bodyType else {
                return nil
            }
            return BodyType(status: status)
        }
    }
    
}
