//
//  BodyFatSDK.swift
//  1SK
//
//  Created by Thaad on 08/12/2022.
//

import Foundation

class BodyFatSDK: NSObject {
    var key: String?
    var userId: Int?
    var gender: Gender?
    var age: Int?
    var height: Double?
    
    var errorType: THTBodyfatErrorType?
    
    var weight: Double?
    var impedance: Int?
    
    var heartRate: Int?
    var heartRateIndex: IndexProtocol?
    
    var bmi: Double?
    var bmiRatingList: [Double]?
    var bmiIndex: IndexProtocol?
    
    var bmr: Double?
    var bmrRatingList: [Double]?
    var bmrIndex: IndexProtocol?
    
    var musclePercentage: Double? // Cơ
    var muscleKg: Double?
    var muscleRatingList: [Double]?
    var muscleIndex: IndexProtocol?
    
    var boneKg: Double? // Xương
    var boneRatingList: [Double]?
    var boneIndex: IndexProtocol?
    
    var waterPercentage: Double? // Nước
    var waterRatingList: [Double]?
    var waterIndex: IndexProtocol?
    
    var proteinPercentage: Double? // Protein
    var proteinRatingList: [Double]?
    var proteinIndex: IndexProtocol?
    
    var fatPercentage: Double? // Mỡ
    var fatRatingList: [Double]?
    var fatIndex: IndexProtocol?
    
    var obesityFatLevel: Double? // Mức độ béo phì
    var obesityFatLevelRatingList: [Double]?
    var obesityFatLevelIndex: IndexProtocol?
    
    var visceralFatLevel: Double? // mức mỡ nội tạng
    var visceralFatLevelRatingList: [Double]?
    var visceralFatLevelIndex: IndexProtocol?
    
    var subcutaneousFat: Double? // mỡ dưới da
    var subcutaneousFatRatingList: [Double]?
    var subcutaneousFatIndex: IndexProtocol?
    
    var leanBodyMass: Double? // Lean Body Mass - Khối lượng cơ thể săn chắc
    var leanBodyMassIndex: IndexProtocol?
    
    var bodyStandard: Double? // Cân nặng tiêu chuẩn
    var bodyStandardIndex: IndexProtocol?
    
    var bodyType: BodyType? // Vóc dáng
    var bodyTypeIndex: IndexProtocol?
    
    var idealWeight: Double?
    var idealWeightIndex: IndexProtocol?
    
    var bodyScore: Double?
    var bodyScoreIndex: IndexProtocol?
    
    var bodyAge: Double?
    var bodyAgeIndex: IndexProtocol?
    
    var createAt: String?
    var timestamp: Double?
    
    var indexs: [IndexProtocol]?
    
    // MARK: Init With HTBodyfat
    init(impedance: Int, weight: Float, height: Double, age: Int, gender: Gender, isMeasure: Bool?) {
        super.init()
        
        self.impedance = impedance
        self.weight = Double(weight)
        self.height = height
        self.age = age
        self.gender = gender
        
        let htBodyFat = HTBodyfat_NewSDK()
        
        var sex: THTSexType?
        switch gender {
        case .male:
            sex = THTSexType.male
        case .female:
            sex = THTSexType.female
        default:
            return
        }
        
        guard let _sex = sex else {
            SKToast.shared.showToast(content: "Error\n\n Giới tính không xác định")
            return
        }
        guard let _age = self.age else {
            SKToast.shared.showToast(content: "Error\n\n Tuổi không xác định")
            return
        }
//        print("BodyFatSDK profile impedance: \(impedance)")
//        print("BodyFatSDK profile weight: \(weight)")
//        print("BodyFatSDK profile height: \(height)")
//        print("BodyFatSDK profile age: \(_age)")
//        print("BodyFatSDK profile sex: \(gender.valueVi)")
//        print("BodyFatSDK profile ------------")
        
        let error = htBodyFat.getBodyfatWithweightKg(CGFloat(weight), heightCm: CGFloat(height), sex: _sex, age: _age, impedance: impedance)
        
        self.setBodyFatData(htBodyFat: htBodyFat, error: error, impedance: impedance)
        
        self.showError(error: error, isMeasure: isMeasure)
    }
    
    private func showError(error: THTBodyfatErrorType, isMeasure: Bool?) {
        if isMeasure == true {
            switch error {
            case .none:
                // Không có lỗi (có thể đọc được tất cả các thông số)
                //self.setBodyFatData(htBodyFat: htBodyFat, error: error, impedance: impedance)
                break
                
            case .impedance:
                // Trở kháng sai Khi trở kháng sai,
                // các thông số khác ngoài BMI/WeightWeightKg sẽ không được tính (viết 0)
                SKToast.shared.showToast(content: "Error\n\nTrở kháng sai Khi trở kháng sai\nCác thông số khác ngoài BMI sẽ không được tính")
                
                //self.setBodyFatData(htBodyFat: htBodyFat, error: error, impedance: impedance)
                
            case .age:
                // Tham số tuổi không chính xác,
                // cần phải từ 6 đến 99 tuổi (các tham số khác ngoài BMI/idealWeightKg không được tính)
                SKToast.shared.showToast(content: "Error\n\nTham số tuổi không chính xác \nCần phải từ 6 đến 99 tuổi (các tham số khác ngoài BMI không được tính)")
                
                //self.setBodyFatData(htBodyFat: htBodyFat, error: error, impedance: impedance)
                
            case .weight:
                // Thông số cân nặng sai, cần từ 10~200kg (không tính thông số sai)
                SKToast.shared.showToast(content: "Error\n\nThông số cân nặng sai \nCần từ 10~200kg (không thể tính các thông số khác)")
                //self.setBodyFatData(htBodyFat: htBodyFat, error: error, impedance: impedance)
                
            case .height:
                // Tham số chiều cao bị sai, cần từ 90~220cm (không tính tất cả các tham số)
                SKToast.shared.showToast(content: "Error\n\nTham số chiều cao bị sai \nCần từ 90~220cm (không tính tất cả các thông số khác)")
                //self.setBodyFatData(htBodyFat: htBodyFat, error: error, impedance: impedance)
                
            default:
                // Không xác định
                SKToast.shared.showToast(content: "Error\n\nCó lỗi xảy ra\nKhông xác định")
            }
        }
    }
    
    // MARK: Body Fat Data
    private func setBodyFatData(htBodyFat: HTBodyfat_NewSDK, error: THTBodyfatErrorType, impedance: Int) {
        self.key = UUID().uuidString
        self.createAt = Date().toString(.ymdhms)
        self.timestamp = Date().timeIntervalSince1970
        
        self.errorType = error
        
        self.bmi = htBodyFat.thtBMI.doubleValue
        self.bmiRatingList = [18.5, 25, 30, 35]
        //self.bmiRatingList = self.getRatingList(of: htBodyFat.thtBMIRatingList)
        //self.bmiRatingList?.append(35)
        
        self.obesityFatLevel = htBodyFat.thtBMI.doubleValue
        self.obesityFatLevelRatingList = self.getRatingList(of: htBodyFat.thtBMIRatingList)
        
        self.bmr = Double(htBodyFat.thtBMR)
        self.bmrRatingList = self.getRatingList(of: htBodyFat.thtBMRRatingList)
        
        if error == .none {
            self.musclePercentage = htBodyFat.thtMusclePercentage.doubleValue
            self.muscleKg = htBodyFat.thtMuscleKg.doubleValue
            self.muscleRatingList = self.getRatingList(of: htBodyFat.thtMuscleRatingList)
            
            self.boneKg = htBodyFat.thtBoneKg.doubleValue
            self.boneRatingList = self.getRatingList(of: htBodyFat.thtBoneRatingList)
            
            self.waterPercentage = htBodyFat.thtWaterPercentage.doubleValue
            self.waterRatingList = self.getRatingList(of: htBodyFat.thtWaterRatingList)
            
            self.proteinPercentage = htBodyFat.thtproteinPercentage.doubleValue
            self.proteinRatingList = self.getRatingList(of: htBodyFat.thtproteinRatingList)
            
            self.fatPercentage = htBodyFat.thtBodyfatPercentage.doubleValue
            self.fatRatingList = self.getRatingList(of: htBodyFat.thtBodyfatRatingList)
            
            //self.obesityFatLevel = htBodyFat.thtBMI.doubleValue
            //self.obesityFatLevelRatingList = self.getRatingList(of: htBodyFat.thtBMIRatingList)
            
            self.visceralFatLevel = htBodyFat.thtVFAL.doubleValue
            self.visceralFatLevelRatingList = self.getRatingList(of: htBodyFat.thtVFALRatingList)
            
            self.subcutaneousFat = htBodyFat.thtBodySubcutaneousFat.doubleValue
            switch htBodyFat.thtSex {
            case .male:
                self.subcutaneousFatRatingList = [8.6, 16.7, 20.7]
                
            case .female:
                self.subcutaneousFatRatingList = [18.5, 26.7, 30.8]
                
            default:
                break
            }
            
            self.leanBodyMass = htBodyFat.thtBodyLBW.doubleValue
            self.bodyAge = htBodyFat.thtBodyAge.doubleValue
            self.bodyScore = htBodyFat.thtBodyScore.doubleValue
            self.idealWeight = htBodyFat.thtIdealWeightKg.doubleValue
            self.bodyStandard = htBodyFat.thtBodystandard.doubleValue
            
            //self.bodyType = BodyType(rawValue: htBodyFat.thtBodyType.rawValue)
        }
        
        self.setIndexBMI()
        self.setIndexBMR()
        self.setIndexMuscle()
        
        self.setIndexBone()
        self.setIndexWater()
        self.setIndexProtein()
        
        self.setIndexFat()
        self.setIndexObesityFatLevel()
        self.setIndexVisceralFatLevel()
        
        self.setIndexSubcutaneousFat()
        self.setIndexLeanBodyMass()
        self.setIndexBodyStandard()
        
        self.setIndexBodyType()
        
        self.setListIndex()
    }
    
    // MARK: Rating List
    private func getRatingList(of ratingList: [AnyHashable: Any]?) -> [Double]? {
        let array = ratingList?.compactMap({ (dictionary) -> Double? in
            guard let value = dictionary.value as? Double else {
                return nil
            }
            return value
            
        }).sorted()
        return array
    }
}

extension BodyFatSDK {
    // MARK: BMI
    private func setIndexBMI() {
        guard let bmi = self.bmi, bmi > 0 else {
            return
        }
        self.bmiIndex = BMI(value: bmi)
    }
    
//    private func setIndexBMI() {
//        guard
//            let bmi = self.bmi, bmi > 0,
//            let ratingList = self.bmiRatingList, ratingList.count == 4 else {
//            return
//        }
//        self.bmiIndex = BMI(value: bmi, ratingList: ratingList)
//    }
    
    // MARK: BMR
    private func setIndexBMR() {
        guard
            let bmr = self.bmr, bmr > 0,
            let rating = self.bmrRatingList?.first,
            let gender = self.gender,
            let age = self.age
        else {
            return
        }
        self.bmrIndex = BMR(value: bmr, rating: rating, gender: gender, age: age)
    }
    
    // MARK: Muscle
    private func setIndexMuscle() {
        guard
            let muscle = self.muscleKg, muscle > 0,
            let ratingList = self.muscleRatingList, ratingList.count == 2,
            let gender = self.gender
        else {
            return
        }
        self.muscleIndex = Muscle(value: muscle, ratingList: ratingList, gender: gender)
    }
    
    // MARK: Bone
    private func setIndexBone() {
        guard
            let bone = self.boneKg, bone > 0,
            let ratingList = self.boneRatingList, ratingList.count == 2,
            let gender = self.gender
        else {
            return
        }
        self.boneIndex = Bone(value: bone, ratingList: ratingList, gender: gender)
    }
    
    // MARK: Water
    private func setIndexWater() {
        guard
            let water = self.waterPercentage, water > 0,
            let ratingList = self.waterRatingList, ratingList.count == 2,
            let age = self.age
        else {
            return
        }
        self.waterIndex = Water(value: water, ratingList: ratingList, age: age)
    }
    
    // MARK: Protein
    private func setIndexProtein() {
        guard
            let protein = self.proteinPercentage, protein > 0,
            let ratingList = self.proteinRatingList, ratingList.count == 2
        else {
            return
        }
        self.proteinIndex = Protein(value: protein, ratingList: ratingList)
    }
    
    // MARK: Fat
    private func setIndexFat() {
        guard
            let fat = self.fatPercentage, fat > 0,
            let ratingList = self.fatRatingList, ratingList.count == 4,
            let gender = self.gender,
            let age = self.age
        else {
            return
        }
        self.fatIndex = Fat(value: fat, ratingList: ratingList, gender: gender, age: age)
    }
    
    // MARK: Obesity Fat Level
    private func setIndexObesityFatLevel() {
        guard let obesityFatLevel = self.obesityFatLevel, obesityFatLevel > 0 else {
            return
        }
        self.obesityFatLevelIndex = ObesityFatLevel(value: obesityFatLevel)
    }
    
//    private func setIndexObesityFatLevel() {
//        guard
//            let obesityFatLevel = self.obesityFatLevel, obesityFatLevel > 0,
//            let ratingList = self.obesityFatLevelRatingList, ratingList.count == 3
//        else {
//            return
//        }
//        self.obesityFatLevelIndex = ObesityFatLevel(value: obesityFatLevel, ratingList: ratingList)
//    }
    
    // MARK: Visceral Fat Level
    private func setIndexVisceralFatLevel() {
        guard
            let visceralFatLevel = self.visceralFatLevel, visceralFatLevel > 0,
            let ratingList = self.visceralFatLevelRatingList, ratingList.count == 2
        else {
            return
        }
        self.visceralFatLevelIndex = VisceralFatLevel(value: visceralFatLevel, ratingList: ratingList)
    }
    
    // MARK: Subcutaneous Fat
    private func setIndexSubcutaneousFat() {
        guard
            let subcutaneousFat = self.subcutaneousFat, subcutaneousFat > 0,
            let ratingList = self.subcutaneousFatRatingList, ratingList.count == 3
        else {
            return
        }
        self.subcutaneousFatIndex = SubcutaneousFat(value: subcutaneousFat, ratingList: ratingList)
    }
    
    // MARK: LeanBodyMass
    private func setIndexLeanBodyMass() {
        guard
            let lbm = self.leanBodyMass, lbm > 0
        else {
            return
        }
        self.leanBodyMassIndex = LeanBodyMass(value: lbm)
    }
    
    // MARK: Body Standard
    private func setIndexBodyStandard() {
        guard
            let bodyStandard = self.bodyStandard
        else {
            return
        }
        self.bodyStandardIndex = BodyStandard(value: bodyStandard)
    }
    
    // MARK: Body Type
    private func setIndexBodyType() {
        guard
            let fatStatus = self.fatIndex?.status,
            let muscleStatus = self.muscleIndex?.status
        else {
            return
        }
        let bodyType = BodyType(fatStatus: fatStatus, muscleStatus: muscleStatus)
        self.bodyTypeIndex = bodyType
        self.bodyType = bodyType
    }
    
    // MARK: List Index
    func setListIndex() {
        var listIndex: [IndexProtocol] = []
        
        if let index = self.bmiIndex {
            listIndex.append(index)
        }
        if let index = self.bmrIndex {
            listIndex.append(index)
        }
        if let index = self.muscleIndex {
            listIndex.append(index)
        }
        if let index = self.boneIndex {
            listIndex.append(index)
        }
        if let index = self.waterIndex {
            listIndex.append(index)
        }
        if let index = self.proteinIndex {
            listIndex.append(index)
        }
        if let index = self.fatIndex {
            listIndex.append(index)
        }
        if let index = self.obesityFatLevelIndex {
            listIndex.append(index)
        }
        if let index = self.visceralFatLevelIndex {
            listIndex.append(index)
        }
        if let index = self.subcutaneousFatIndex {
            listIndex.append(index)
        }
        if let index = self.leanBodyMassIndex {
            listIndex.append(index)
        }
        if let index = self.bodyStandardIndex {
            listIndex.append(index)
        }
        if let index = self.bodyTypeIndex {
            listIndex.append(index)
        }
        
        self.indexs = listIndex
    }
}

// MARK: Parse Dictionary
extension BodyFatSDK {
    func getDictionary(device: DeviceRealm?) -> [String: Any] {
        var object = [String: Any]()

        if device?.name == "Smart scale CF516BLE" {
            object["device"] = SKDevice.Name.CF_516
        } else {
            object["device"] = device?.name ?? ""
        }
        object["device_display_name"] = device?.name_display
        
        object["user_id"] = self.userId ?? 0
        object["mac"] = device?.mac ?? ""
        object["scale_type"] = device?.code ?? "CF"
        object["created_at"] = self.createAt ?? ""

        object["impedance"] = self.impedance ?? 0
        object["weight"] = self.weight ?? 0
        object["status_weight"] = self.bmiIndex?.status ?? ""

        object["heart_rate"] = self.heartRate ?? 0
        object["status_heart_rate"] = self.heartRateIndex?.status ?? ""
        
        object["bmi"] = self.bmi ?? 0
        object["status_bmi"] = self.bmiIndex?.status ?? ""

        object["bmr"] = self.bmr ?? 0
        object["status_bmr"] = self.bmrIndex?.status ?? ""

        object["muscle_mass"] = self.muscleKg ?? 0
        object["status_muscle"] = self.muscleIndex?.status ?? ""

        object["bone_mass"] = self.boneKg ?? 0
        object["status_bone_mass"] = self.boneIndex?.status ?? ""

        object["body_water"] = self.waterPercentage ?? 0
        object["status_body_water"] = self.waterIndex?.status ?? ""

        object["protein"] = self.proteinPercentage ?? 0
        object["status_protein"] = self.proteinIndex?.status ?? ""
        
        object["fat"] = self.fatPercentage ?? 0
        object["status_fat"] = self.fatIndex?.status ?? ""

        object["fat_level"] = self.obesityFatLevel ?? 0
        object["status_fat_level"] = self.obesityFatLevelIndex?.status ?? ""

        object["visceral_fat"] = self.visceralFatLevel ?? 0
        object["status_visceral_fat"] = self.visceralFatLevelIndex?.status ?? ""

        object["subcutaneous_fat"] = self.subcutaneousFat ?? 0
        object["status_subcutaneous_fat"] = self.subcutaneousFatIndex?.status ?? ""

        object["lean_body_mass"] = self.leanBodyMass ?? 0
        object["status_lean_body_mass"] = "" // Không có trạng thái
        
        object["standart_weight"] = self.bodyStandard ?? 0
        object["status_standart_weight"] = "" // Không có trạng thái

        object["body_type"] = self.bodyType?.status ?? ""
        object["meta_data"] = []
        
        return object
    }
}
