//
//  BodyFatType.swift
//  1SKConnect
//
//  Created by Thaad on 28/07/2022.
//

import Foundation

enum BodyFatType: Int {
    case weight
    case muscle
    case bone
    case water
    case protein
    case fat
    case subcutaneousFat

    var id: Int {
        switch self {
        case .weight:
            return 0
            
        case .muscle:
            return 1
            
        case .bone:
            return 2
            
        case .water:
            return 3
            
        case .protein:
            return 4
            
        case .fat:
            return 5
            
        case .subcutaneousFat:
            return 6
        }
    }
    
    var title: String {
        switch self {
        case .weight:
            return "Cân nặng"
            
        case .muscle:
            return "Cơ"
            
        case .bone:
            return "Xương"
            
        case .water:
            return "Nước"
            
        case .protein:
            return "Protein"
            
        case .fat:
            return "Mỡ"
            
        case .subcutaneousFat:
            return "Mỡ dưới da"
        }
    }
}
