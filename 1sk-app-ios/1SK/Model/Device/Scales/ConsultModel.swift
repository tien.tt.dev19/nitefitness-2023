//
//  ConsultModel.swift
//  1SK
//
//  Created by Thaad on 24/11/2022.
//

import Foundation

class ConsultModel: Codable {
    var content: String?
    var status: Int?

    enum CodingKeys: String, CodingKey {
        case content
        case status
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.content = try? container.decode(String.self, forKey: .content)
        self.status = try? container.decode(Int.self, forKey: .status)
    }
}
