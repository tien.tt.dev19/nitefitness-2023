//
//  DateTime.swift
//  1SKConnect
//
//  Created by Thaad on 31/07/2022.
//

import Foundation

class DateTimeMenu: NSObject {
    var startDate: Date?
    var endDate: Date?
    
    var startTime: String?
    var endTime: String?
    
    var listBodyFat: [BodyFatModel]?
    var bodyFatChart: BodyFatChart?
    
    override init() {
        //
    }
    
    init(startDate: Date?, endDate: Date?) {
        self.startDate = startDate
        self.endDate = endDate
        
        self.startTime = startDate?.toString(.ymd)
        self.endTime = endDate?.toString(.ymd)
    }
}
