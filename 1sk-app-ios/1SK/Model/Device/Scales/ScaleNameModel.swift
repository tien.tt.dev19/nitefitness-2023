//
//  ScaleNameModel.swift
//  1SK
//
//  Created by Thaad on 06/12/2022.
//

import Foundation

struct DeviceName {
    var id: Int?
    var name: String?
    var display: String?
}
