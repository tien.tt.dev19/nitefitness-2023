//
//  ActivitySubjectModel.swift
//  1SK
//
//  Created by Thaad on 23/12/2022.
//

import Foundation

class ActivitySubjectModel: Codable, Equatable {
    var id: Int?
    var name, image, slug, description: String?
    var attributes: [AttributeModel]?
    var isSelected: Bool?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
        self.slug = try? container.decode(String.self, forKey: .slug)
        self.attributes = try? container.decode([AttributeModel].self, forKey: .attributes)
    }
    
    init(id: Int, name: String) {
        self.id = id
        self.name = name
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case description
        case image, slug, attributes
    }
    
    static func == (lhs: ActivitySubjectModel, rhs: ActivitySubjectModel) -> Bool {
        return lhs.id == rhs.id
    }
}
