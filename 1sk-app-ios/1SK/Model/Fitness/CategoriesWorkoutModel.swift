//
//  CategoriesWorkoutModel.swift
//  1SK
//
//  Created by vuongbachthu on 12/8/21.
//

import Foundation

class CategoriesWorkoutModel: Codable {
    var id: Int?
    var name, description: String?
    var image, cover: String?
    var order, status, color: String?
    var target: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case description
        case image, cover, order, status, color, target
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
        self.cover = try? container.decode(String.self, forKey: .cover)
        self.order = try? container.decode(String.self, forKey: .order)
        self.status = try? container.decode(String.self, forKey: .status)
        self.color = try? container.decode(String.self, forKey: .color)
        self.target = try? container.decode(Int.self, forKey: .target)
    }
}
