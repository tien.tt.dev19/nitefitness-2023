//
//  EquipmentModel.swift
//  1SK
//
//  Created by TrungDN on 20/04/2022.
//

import Foundation

class EquipmentModel: Codable {
    var id: Int?
    var code: String?
    var name: String?
    var description: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case code
        case name
        case description
        case image
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.code = try? container.decode(String.self, forKey: .code)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
    }
}
