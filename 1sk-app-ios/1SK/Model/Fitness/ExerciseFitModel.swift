//
//  ExerciseFitModel.swift
//  1SK
//
//  Created by Thaad on 22/04/2022.
//

import Foundation

class ExerciseFitModel: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var image: String?
    var video: String?
    var videoTime: Int?
    var kcal: Int?
    var createdAt: Int?
    var publishedAt: String?
    var level: LevelExercise?
    var subject: [SubjectExercise]?
    var information: [InformationExercise]?
    var workout: WorkoutExercise?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case image
        case video
        case videoTime
        case kcal
        case level
        case createdAt
        case publishedAt
        case subject
        case information
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
        self.video = try? container.decode(String.self, forKey: .video)
        self.videoTime = try? container.decode(Int.self, forKey: .videoTime)
        self.kcal = try? container.decode(Int.self, forKey: .kcal)
        self.createdAt = try? container.decode(Int.self, forKey: .createdAt)
        self.publishedAt = try? container.decode(String.self, forKey: .publishedAt)
        self.level = try? container.decode(LevelExercise.self, forKey: .level)
        self.subject = try? container.decode([SubjectExercise].self, forKey: .subject)
        self.information = try? container.decode([InformationExercise].self, forKey: .information)
    }
}

// MARK: LevelExercise
class LevelExercise: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case image
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
    }
}

// MARK: SubjectExercise
class SubjectExercise: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case image
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
    }
}

// MARK: InformationExercise
class InformationExercise: Codable {
    var id: Int?
    var minute: Int?
    var title: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case minute
        case title
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.minute = try? container.decode(Int.self, forKey: .minute)
        self.title = try? container.decode(String.self, forKey: .title)
    }
}

// MARK: WorkoutExercise
class WorkoutExercise: Codable {
    var instantaneousSpeed: Double? // TỐC ĐỘ
    var averageSpeed: Double? // TỐC ĐỘ TRUNG BÌNH
    var instantaneousCadence: Double? // NHỊP ĐỘ
    var averageCadence: Double? // NHỊP ĐỘ TRUNG BÌNH
    var totalDistance: Double? // TỔNG QUÃNG ĐƯỜNG
    var resistanceLevel: Int16? // KHÁNG LỰC
    var instantaneousPower: Double? // LỰC ĐẠP TỨC THỜI
    var averagePower: Double? // LỰC ĐẠP TRUNG BÌNH
    var heartRate: Double? // NHỊP TIM
    var energy: Double? // CALO TIÊU HAO
    var metabolicEquivalent: Double? // TRAO ĐỔI CHẤT TƯƠNG ĐƯƠNG
    var time: Double? // MACHINE TIME
    
    var sumDistance: Double?
    var sumCalories: Double?
    
    init() {
        //
    }
}
