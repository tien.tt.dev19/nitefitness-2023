//
//  Exsercise.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//

import Foundation

struct Exsercise {
    var id: Int?
    var name: String?
    var image: String?
    var movement: String?
    var typeMovement: Int?
    var video: String?
    var typeVideo: Int?
    var time: Int?
    var order: Int?
    var typeDisplay: Int?
    var desc: String?
    var status: Int?
    var subject: Subject?
    var level: Level?
    var type: Type?
    var tools: [Tool]?
    var filters: [Filter]?
    var createdAt: String?
    
    init() {
        self.subject = Subject()
        self.level = Level()
        self.type = Type()
        self.tools = [Tool]()
        self.filters = [Filter]()
    }
}

struct Subject {
    var id: String?
    var name: String?
    var createdBy: String?
    var option: String?
}

struct Level {
    var id: String?
    var name: String?
    var createdBy: String?
    var option: String?
}

struct Type {
    var id: String?
    var name: String?
    var createdBy: String?
    var option: String?
}

struct Tool {
    var id: String?
    var name: String?
    var createdBy: String?
}

struct Filter {
    var keyword: String?
    var order: String?
    var sort: String?
    var limit: Int?
    var page: Int?
    var subject: [String] = []
    var level: [String] = []
    var type: [String] = []
    var tool: [String] = []
    var filter: [String] = []
}

struct FilterValues {
    var type: String?
    var name: String?
    var values: [Values]?
}

struct Values {
    var id: String?
    var name: String?
}
