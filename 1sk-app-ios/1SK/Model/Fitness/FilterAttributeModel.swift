//
//  FilterAttributeModel.swift
//  1SK
//
//  Created by vuongbachthu on 7/20/21.
//

import Foundation

enum FilterType: String {
    case type
    case tool
    case filter
    case subject
    case levels
    
    var description: String {
        switch self {
        case .type:
            return "Loại động tác"
        case .tool:
            return "Dụng cụ"
        case .filter:
            return "Huấn luyện viên"
        case .subject:
            return "Bộ môn"
        case .levels:
            return "Cấp độ"
        }
    }
}

class FilterAttributeModel: Codable {
    //    var id: String?
    var type: String?
    var name: String?
    var values: [ValuesModel]?
    
    convenience init() {
        self.init(name: "", data: [])
    }
    
    init(name: String, data: [TypeModel]) {
        self.name = name
        //        self.values = data.map
    }
    
    init(types data: [TypeModel]?) {
        let filterType = FilterType.type
        self.name = filterType.description
        self.type = filterType.rawValue
        self.values = data?.map { return ValuesModel(id: $0.id, name: $0.name, isSelected: false) } ?? []
    }
    
    init(subjects data: [SubjectModel]?) {
        let filterType = FilterType.subject
        self.name = filterType.description
        self.type = filterType.rawValue
        self.values = data?.map { return ValuesModel(id: $0.id, name: $0.name, isSelected: false) } ?? []
    }
    
    init(tools data: [ToolModel]?) {
        let filterType = FilterType.tool
        self.name = filterType.description
        self.type = filterType.rawValue
        self.values = data?.map { return ValuesModel(id: $0.id, name: $0.name, isSelected: false) } ?? []
    }
    
    init(levels data: [LevelModel]?) {
        let filterType = FilterType.levels
        self.name = filterType.description
        self.type = filterType.rawValue
        self.values = data?.map { return ValuesModel(id: $0.id, name: $0.name, isSelected: false) } ?? []
    }
}

class ValuesModel: Codable {
    var id: Int?
    var name: String?
    var isSelected: Bool?
    
    convenience init() {
        self.init(id: -1, name: "", isSelected: false)
    }
    
    init(id: Int, name: String, isSelected: Bool) {
        self.id = id
        self.name = name
        self.isSelected = isSelected
    }
}


class FilterDataValue: Codable {
    
    enum CodingKeys: String, CodingKey {
        case types
        case subjects
        case tools
        case levels
    }
    
    var types: [TypeModel]?
    var subjects: [SubjectModel]?
    var tools: [ToolModel]?
    var levels: [LevelModel]?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        types = try container.decodeIfPresent([TypeModel].self, forKey: .types)
        subjects = try container.decodeIfPresent([SubjectModel].self, forKey: .subjects)
        tools = try container.decodeIfPresent([ToolModel].self, forKey: .tools)
        levels = try container.decodeIfPresent([LevelModel].self, forKey: .levels)
    }
}
