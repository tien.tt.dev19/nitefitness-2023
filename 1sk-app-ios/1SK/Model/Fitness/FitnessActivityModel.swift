//
//  FitnessActivityModel.swift
//  1SK
//
//  Created by vuongbachthu on 7/20/21.
//

import Foundation

class FitnessActivityModel: Codable {
    enum CodingKeys: String, CodingKey {
      case caloriesBurned
      case numberActivity
      case activitySubjectId
      case totalTime
      case name
      case type
    }

    var caloriesBurned: Int?
    var numberActivity: Int?
    var activitySubjectId: Int?
    var totalTime: String?
    var name: String?
    var type: Int?
    
    var activityType: ActivityType? {
        return ActivityType(rawValue: self.type ?? 0)
    }
    
    init() {
        
    }

    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      caloriesBurned = try container.decodeIfPresent(Int.self, forKey: .caloriesBurned)
      numberActivity = try container.decodeIfPresent(Int.self, forKey: .numberActivity)
      activitySubjectId = try container.decodeIfPresent(Int.self, forKey: .activitySubjectId)
      totalTime = try container.decodeIfPresent(String.self, forKey: .totalTime)
      name = try container.decodeIfPresent(String.self, forKey: .name)
      type = try container.decodeIfPresent(Int.self, forKey: .type)
    }
}
