//
//  IndoorBikeListData.swift
//  1SK
//
//  Created by TrungDN on 22/04/2022.
//

import Foundation

class IndoorBikeListData: Codable {
    var resistanceLevel: Int?
    var instantaneousSpeed: Double?
    var distance: Int?
    var calo: Int?
    var averagePower: Int?
    var instantaneousPower: Int?
    
    enum CodingKeys: String, CodingKey {
        case resistanceLevel
        case instantaneousSpeed
        case distance
        case calo
        case averagePower
        case instantaneousPower
    }
    
    init (resistanceLevel: Int?, instantaneousSpeed: Double?, distance: Int?, calo: Int?, averagePower: Int?, instantaneousPower: Int?) {
        self.resistanceLevel = resistanceLevel
        self.instantaneousSpeed = instantaneousSpeed
        self.distance = distance
        self.calo = calo
        self.averagePower = averagePower
        self.instantaneousPower = instantaneousPower
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        resistanceLevel = try container.decodeIfPresent(Int.self, forKey: .resistanceLevel)
        instantaneousSpeed = try container.decodeIfPresent(Double.self, forKey: .instantaneousSpeed)
        distance = try container.decodeIfPresent(Int.self, forKey: .distance)
        calo = try container.decodeIfPresent(Int.self, forKey: .calo)
        averagePower = try container.decodeIfPresent(Int.self, forKey: .averagePower)
        instantaneousPower = try container.decodeIfPresent(Int.self, forKey: .instantaneousPower)
    }
    
}
