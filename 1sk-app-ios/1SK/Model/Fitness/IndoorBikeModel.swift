//
//  IndoorBikeModel.swift
//  1SK
//
//  Created by Thaad on 04/05/2022.
//

import Foundation
import RealmSwift

class IndoorBikeModel: Object {
    @Persisted var id: String = ""
    @Persisted var device_name: String?
    @Persisted var device_type: String?
    @Persisted var start_time: Int?
    @Persisted var end_time: Int?
    @Persisted var is_synchronized: Bool?
    @Persisted var log_path: List<FileLogPathModel>
    
    override init() {
        super.init()
        self.id = UUID().uuidString
        self.log_path = List<FileLogPathModel>()
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}

// MARK: FileLogPathModel
class FileLogPathModel: Object {
    @Persisted var id: String = ""
    @Persisted var file_name: String?
    @Persisted var start_time: Int?
    @Persisted var end_time: Int?
    
    var filePath: String {
        guard let _fileName = self.file_name else {
            return ""
        }
        let docmentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        let fileDirectory = docmentURL!.appendingPathComponent(Folder.FitLog)
        let filePath = fileDirectory.appendingPathComponent(_fileName)
        return filePath.path
    }
    
    override init() {
        super.init()
        self.id = UUID().uuidString
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
