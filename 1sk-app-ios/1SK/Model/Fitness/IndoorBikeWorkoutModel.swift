//
//  IndoorBikeWorkoutModel.swift
//  1SK
//
//  Created by TrungDN on 20/04/2022.
//

import Foundation

class IndoorBikeWorkoutModel: Codable {
    enum CodingKeys: String, CodingKey {
        case equipment
        case startTime
        case customerId
        case endTime
        case clientId
        case id
        case dataPath
    }
    
    var equipment: EquipmentModel?
    var startTime: String?
    var customerId: Int?
    var endTime: String?
    var clientId: String?
    var id: Int?
    var dataPath: String?
    
    init(clientId: String, startTime: String, endTime: String, dataPath: String) {
        self.clientId = clientId
        self.startTime = startTime
        self.endTime = endTime
        self.dataPath = dataPath
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        equipment = try container.decodeIfPresent(EquipmentModel.self, forKey: .equipment)
        startTime = try container.decodeIfPresent(String.self, forKey: .startTime)
        customerId = try container.decodeIfPresent(Int.self, forKey: .customerId)
        endTime = try container.decodeIfPresent(String.self, forKey: .endTime)
        clientId = try container.decodeIfPresent(String.self, forKey: .clientId)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        dataPath = try container.decodeIfPresent(String.self, forKey: .dataPath)
    }
}
