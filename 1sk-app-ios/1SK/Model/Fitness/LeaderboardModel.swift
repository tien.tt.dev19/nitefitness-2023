//
//  LeaderboardModel.swift
//  1SK
//
//  Created by TrungDN on 18/07/2022.
//

import Foundation


class LeaderboardModel: Codable {
    var customerId: Int?
    var fullName: String?
    var avatar: String?
    var totalDistance: Float?
    var rank: Int?
    var unit: String?
    var goal: GoalLeaderboard?

    enum CodingKeys: String, CodingKey {
        case customerId
        case fullName
        case avatar
        case totalDistance
        case rank, unit
        case goal
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.fullName = try container.decodeIfPresent(String.self, forKey: .fullName)
        self.avatar = try container.decodeIfPresent(String.self, forKey: .avatar)
        self.rank = try container.decodeIfPresent(Int.self, forKey: .rank)
        self.totalDistance = try container.decodeIfPresent(Float.self, forKey: .totalDistance)
        self.customerId = try container.decodeIfPresent(Int.self, forKey: .customerId)
        self.unit = try container.decodeIfPresent(String.self, forKey: .unit)
        self.goal = try container.decodeIfPresent(GoalLeaderboard.self, forKey: .goal)
    }
}

class GoalLeaderboard: Codable {
    var id, goal: Int?
    var unit: String?
    
    enum CodingKeys: String, CodingKey {
        case id, goal, unit
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.goal = try? container.decode(Int.self, forKey: .goal)
        self.unit = try? container.decode(String.self, forKey: .unit)
    }
}
