//
//  ActivityTypeModel.swift
//  1SK
//
//  Created by vuongbachthu on 8/2/21.
//

import Foundation

class ActivityTypeModel: Codable {

    var order: Int?
    var measure: String?
    var status: Int?
    var id: Int?
    var type: Int?
    var activitySubjectId: Int?
    var name: String?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        order = try container.decodeIfPresent(Int.self, forKey: .order)
        measure = try container.decodeIfPresent(String.self, forKey: .measure)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        type = try container.decodeIfPresent(Int.self, forKey: .type)
        activitySubjectId = try container.decodeIfPresent(Int.self, forKey: .activitySubjectId)
        name = try container.decodeIfPresent(String.self, forKey: .name)
    }
    
    enum CodingKeys: String, CodingKey {
      case order
      case measure
      case status
      case id
      case type
      case activitySubjectId
      case name
    }
}
