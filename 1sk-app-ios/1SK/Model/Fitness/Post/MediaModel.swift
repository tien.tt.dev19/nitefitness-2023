//
//  MediaModel.swift
//  1SK
//
//  Created by tuyenvx on 04/06/2021.
//

import Foundation

//class MediaModel: Codable {
//    var id: String
//    var postId: String
//    var title: String
//    var link: String
//    var content: String
//    var order: Int
//    var type: Int
//    var createAt: String?
//    var updateAt: String?
//
//    init(title: String, link: String, content: String, order: Int, type: Int) {
//        self.id = ""
//        self.postId = ""
//        self.title = title
//        self.link = link
//        self.content = content
//        self.order = order
//        self.type = type
//    }
//
//    var param: [String: Any] {
//        return [
//            SKKey.title: title,
//            SKKey.link: link,
//            SKKey.content: content,
//            SKKey.order: order,
//            SKKey.type: type
//        ]
//    }
//}

extension MediaModel: Equatable {
    static func == (lhs: MediaModel, rhs: MediaModel) -> Bool {
        return lhs.id == rhs.id
    }
}
