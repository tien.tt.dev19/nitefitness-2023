//
//  PostModel.swift
//  1SK
//
//  Created by tuyenvx on 04/06/2021.
//

import Foundation

//class PostModel: Codable {
//    var id: String?
//    var userId: String?
//    var title: String?
//    var content: String?
//    var permissionId: PostVisibleScope?
//    var status: Int?
//    var type: Int?
//    var activityId: String?
//    var typeSubject: String?
//    var location: String?
//    var totalLike: Int?
//    var totalComment: Int?
//    var media: [MediaModel]?
//    var infos: [PostInfoModel]?
//    var createdAt: String?
//    var updatedAt: String?
//    var userName: String?
//    var userAvartar: String?
//    var isLike: Bool?
//
//    var activityType: ActivityType? {
//        guard let `typeSubject` = typeSubject else {
//            return nil
//        }
//        return ActivityType(typeString: typeSubject)
//    }
//
//    var param: [String: Any] {
//        var param: [String: Any] = [
//            SKKey.content: content ?? "",
//            SKKey.permissionId: permissionId?.rawValue ?? "",
//            SKKey.status: status ?? "",
//            SKKey.type: type ?? "",
//            SKKey.location: location ?? "",
//            SKKey.infos: infos!.map({ $0.param }),
//            SKKey.media: media!.map({ $0.param })
//        ]
//        if let `activityId` = activityId {
//            param[SKKey.activityId] = activityId
//        }
//
//        if let `typeSubject` = typeSubject {
//            param[SKKey.typeSubject] = typeSubject
//        }
//        return param
//    }
//
//    var indexPath: IndexPath?
//}


// MARK: - Post
class PostModel: Codable {
    var id: Int?
    var title, content: String?
    var permissionId: PostVisibleScope?
    var status, type: Int?
    var activityId: Int?
    var typeSubject, location: String?
    var totalComment, totalLike: Int?
    var isLike: Bool?
    var createdAt, updatedAt: String?
    var userId: Int?
    var userName, avatar: String?

    var infos: [PostInfoModel]?
    var media: [MediaModel]?
    
    var fullName: String?
    
    var activityType: ActivityType? {
        guard let `typeSubject` = self.typeSubject else {
            return nil
        }
        return ActivityType(typeString: typeSubject)
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.userId = try? container.decode(Int.self, forKey: .userId)
        self.title = try? container.decode(String.self, forKey: .title)
        self.content = try? container.decode(String.self, forKey: .content)
        
        self.status = try? container.decode(Int.self, forKey: .status)
        self.type = try? container.decode(Int.self, forKey: .type)
        self.activityId = try? container.decode(Int.self, forKey: .activityId)
        self.typeSubject = try? container.decode(String.self, forKey: .typeSubject)
        self.location = try? container.decode(String.self, forKey: .location)
        
        self.totalLike = try? container.decode(Int.self, forKey: .totalLike)
        
        self.totalComment = try? container.decode(Int.self, forKey: .totalComment)
        
        self.isLike = try? container.decode(Bool.self, forKey: .isLike)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.userName = try? container.decode(String.self, forKey: .userName)
        self.avatar = try? container.decode(String.self, forKey: .avatar)
        self.fullName = try? container.decode(String.self, forKey: .fullName)

        self.infos = try? container.decode([PostInfoModel].self, forKey: .infos)
        self.media = try? container.decode([MediaModel].self, forKey: .media)
        
        self.permissionId = try? container.decode(PostVisibleScope.self, forKey: .permissionId)
        
        
    }

    enum CodingKeys: String, CodingKey {
        case id
        case userId
        case title, content
        case permissionId
        case status, type
        case activityId
        case typeSubject
        case location
        case totalLike
        case totalComment
        case isLike
        case infos, media
        case createdAt
        case updatedAt
        case userName = "user_name"
        case avatar
        case fullName
    }

//    var indexPath: IndexPath?
}

// MARK: - PostInfos
class PostInfoModel: Codable {
    var id, name, value: String?
    var order: Int?

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(String.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.value = try? container.decode(String.self, forKey: .value)
        
        if let order = try? container.decode(String.self, forKey: .order) {
            self.order = Int(order)
        } else {
            self.order = 0
        }
    }

    enum CodingKeys: String, CodingKey {
        case id, name, value, order
    }

    /// Extention
    init(name: String, value: String, order: Int) {
        self.id = ""
        self.name = name
        self.value = value
        self.order = order
    }

    var param: [String: Any] {
        return [
            SKKey.name: name ?? "",
            SKKey.value: value ?? "",
            SKKey.order: order ?? ""
        ]
    }
}

// MARK: - PostMedia
class MediaModel: Codable {
    var id, postId, title, link, content: String?
    var order, type: Int?

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(String.self, forKey: .id)
        self.postId = try? container.decode(String.self, forKey: .postId)
        self.title = try? container.decode(String.self, forKey: .title)
        self.link = try? container.decode(String.self, forKey: .link)
        self.content = try? container.decode(String.self, forKey: .content)

        self.order = try? container.decode(Int.self, forKey: .order)
        self.type = try? container.decode(Int.self, forKey: .type)
    }

    enum CodingKeys: String, CodingKey {
        case id, postId, title, link, content
        case order, type
    }

    /// Extention
    init(title: String, link: String, content: String, order: Int, type: Int) {
        self.id = ""
        self.postId = ""
        self.title = title
        self.link = link
        self.content = content
        self.order = order
        self.type = type
    }

    var param: [String: Any] {
        return [
            SKKey.title: title ?? "",
            SKKey.link: link ?? "",
            SKKey.content: content ?? "",
            SKKey.order: order ?? "",
            SKKey.type: type ?? ""
        ]
    }
}
