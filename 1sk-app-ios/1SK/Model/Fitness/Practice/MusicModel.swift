//
//  MusicModel.swift
//  1SK
//
//  Created by vuongbachthu on 7/22/21.
//

import Foundation

class MusicModel: Codable {
    var id: Int?
    var created_by: String?
    var title: String?
    var link: String?
    var status: Int?
    var time: Int?
    var path: String?
}
