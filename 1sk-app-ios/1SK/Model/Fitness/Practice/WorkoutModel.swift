//
//  WorkoutModel.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//

import Foundation

class WorkoutModel: Codable {
    
    var userId: Int?
    var name: String?
    var timeBreak: Int?
    var isTemplate: Int?
    var status: Int?
    var id: Int?
    var details: [WorkoutDetailModel]?
    var time: Int?
    var userName: String?
    var finish: Int?
    var userAvatar: String?
    var fullName: String?
    var isBookmark: Int?
    var timeStart: String?
    var timePractice: String?
    var rate: Int?
    var music: MusicModel?
    
    var subject: SubjectModel?
    var level: LevelModel?
    var type: TypeModel?
    
    enum CodingKeys: String, CodingKey {
        case userName
        case details
        case subject
        case name
        case finish
        case level
        case isTemplate
        case id
        case type
        case userId
        case time
        case status
        case timeBreak
        case isBookmark
        case music
        case fullName
        case userAvatar
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try container.decodeIfPresent(TypeModel.self, forKey: .type)
        self.userId = try container.decodeIfPresent(Int.self, forKey: .userId)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.timeBreak = try container.decodeIfPresent(Int.self, forKey: .timeBreak)
        self.isTemplate = try container.decodeIfPresent(Int.self, forKey: .isTemplate)
        self.subject = try container.decodeIfPresent(SubjectModel.self, forKey: .subject)
        self.level = try container.decodeIfPresent(LevelModel.self, forKey: .level)
        self.status = try container.decodeIfPresent(Int.self, forKey: .status)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.details = try container.decodeIfPresent([WorkoutDetailModel].self, forKey: .details)
        self.time = try container.decodeIfPresent(Int.self, forKey: .time)
        self.userName = try container.decodeIfPresent(String.self, forKey: .userName)
        self.finish = try container.decodeIfPresent(Int.self, forKey: .finish)
        self.music = try container.decodeIfPresent(MusicModel.self, forKey: .music)
        self.isBookmark = try container.decodeIfPresent(Int.self, forKey: .isBookmark)
        self.fullName = try container.decodeIfPresent(String.self, forKey: .fullName)
        self.userAvatar = try container.decodeIfPresent(String.self, forKey: .userAvatar)
    }

    func getDetailsContent() -> String {
        var detailsTitle = ""
        detailsTitle.append(time?.doubleValue.getTimeString() ?? "")
        detailsTitle.append(" • ")
        detailsTitle.append("\(self.details?.count ?? 0) động tác")
        return detailsTitle
    }
}

class WorkoutDetailModel: Codable {
    var order: Int?
    var exercise: ExerciseModel?
    var time: Int?
    var id: Int?
    
    var subject: SubjectModel?
    var level: LevelModel?
    var type: TypeModel?
    
    enum CodingKeys: String, CodingKey {
        case order
        case exercise
        case time
        case id
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.order = try? container.decodeIfPresent(Int.self, forKey: .order)
        self.exercise = try? container.decodeIfPresent(ExerciseModel.self, forKey: .exercise)
        self.time = try? container.decodeIfPresent(Int.self, forKey: .time)
        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
    }
}
