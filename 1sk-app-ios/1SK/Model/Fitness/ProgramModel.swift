//
//  ProgramModel.swift
//  1SK
//
//  Created by tuyenvx on 18/01/2021.
//

import Foundation

class ProgramModel: Codable, Equatable {
    var id: String = ""
    var image: String = ""
    var name: String = ""
    var slug: String = ""
    var createdAt: String = ""

    static func == (lhs: ProgramModel, rhs: ProgramModel) -> Bool {
        return lhs.id == rhs.id
    }
}
