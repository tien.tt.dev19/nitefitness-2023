//
//  VisualRaceModel.swift
//  1SK
//
//  Created by Valerian on 24/06/2022.
//

import Foundation

class RaceModel: Codable {
    var id: Int?
    var name: String?
    var unitGoal, thumbnail: String?
    var startAt, finishAt, registerExpireAt, status: String?
    var location, information: String?
    var sport: SportRace?
    var goals: [GoalRace]?
    var awards: [AwardRace]?
    var totalParticipants: Int?
    var statusOfHappening: Int?
    var isRegistered: Bool?
    var customerGoal: CustomerGoalModel?
    var slug: String?
    var value: Int?
    var banners: [RaceBannersModel]?
    var stravaTracking: StravaTrackingModel?
    var limit: Int?
    var award: String?
    var rule: String?
    var utilities: String?
    var imageEbib: String?
    var imageUnfinishedMedal: String?
    var imageFinishMedal: String?
    var imageEcert: String?
    var lastPublishedAt: String?
    
    var typeRace: TypeRaceFilter? {
        return TypeRaceFilter(rawValue: self.statusOfHappening ?? 0)
    }

    enum CodingKeys: String, CodingKey {
        case id, name
        case unitGoal
        case thumbnail
        case sport
        case startAt
        case finishAt
        case registerExpireAt
        case statusOfHappening
        case status, location, information, goals, awards
        case totalParticipants
        case isRegistered
        case customerGoal
        case slug
        case value
        case banners
        case stravaTracking
        case limit
        case award
        case rule
        case utilities
        case imageEbib
        case imageUnfinishedMedal
        case imageFinishMedal
        case imageEcert
        case lastPublishedAt
    }
    
    init() { }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try? container.decode(Int.self, forKey: .id)
        self.isRegistered = try? container.decode(Bool.self, forKey: .isRegistered)
        self.statusOfHappening = try? container.decode(Int.self, forKey: .statusOfHappening)
        self.name = try? container.decode(String.self, forKey: .name)
        self.unitGoal = try? container.decode(String.self, forKey: .unitGoal)
        self.thumbnail = try? container.decode(String.self, forKey: .thumbnail)
        self.startAt = try? container.decode(String.self, forKey: .startAt)
        self.finishAt = try? container.decode(String.self, forKey: .finishAt)
        self.registerExpireAt = try? container.decode(String.self, forKey: .registerExpireAt)
        self.status = try? container.decode(String.self, forKey: .status)
        self.location = try? container.decode(String.self, forKey: .location)
        self.information = try? container.decode(String.self, forKey: .information)
        self.sport = try? container.decode(SportRace.self, forKey: .sport)
        self.goals = try? container.decode([GoalRace].self, forKey: .goals)
        self.awards = try? container.decode([AwardRace].self, forKey: .awards)
        self.totalParticipants = try? container.decode(Int.self, forKey: .totalParticipants)
        self.customerGoal = try container.decodeIfPresent(CustomerGoalModel.self, forKey: .customerGoal)
        self.slug = try container.decodeIfPresent(String.self, forKey: .slug)
        self.banners = try container.decodeIfPresent([RaceBannersModel].self, forKey: .banners)
        self.stravaTracking = try container.decodeIfPresent(StravaTrackingModel.self, forKey: .stravaTracking)
        self.value = try? container.decode(Int.self, forKey: .value)
        self.limit = try? container.decode(Int.self, forKey: .limit)
        self.award = try? container.decode(String.self, forKey: .award)
        self.rule = try? container.decode(String.self, forKey: .rule)
        self.utilities = try? container.decode(String.self, forKey: .utilities)
        self.imageEbib = try? container.decode(String.self, forKey: .imageEbib)
        self.imageUnfinishedMedal = try? container.decode(String.self, forKey: .imageUnfinishedMedal)
        self.imageFinishMedal = try? container.decode(String.self, forKey: .imageFinishMedal)
        self.imageEcert = try? container.decode(String.self, forKey: .imageEcert)
        self.lastPublishedAt = try? container.decode(String.self, forKey: .lastPublishedAt)
    }
}

//MARK: - GoalRace
class GoalRace: Codable {
    var id: Int?
    var goal, goalDescription: String?
    var vrVirtualRaceId: Int?
    var createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, goal
        case goalDescription = "description"
        case vrVirtualRaceId = "vr_virtual_race_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
    init() { }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.goal = try? container.decode(String.self, forKey: .goal)
        self.goalDescription = try? container.decode(String.self, forKey: .goalDescription)
        self.vrVirtualRaceId = try? container.decode(Int.self, forKey: .vrVirtualRaceId)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

//MARK: - AwardRace
class AwardRace: Codable {
    var id: Int?
    var image, name: String?
    var description: String?
    var createdAt, updatedAt: String?
    var vrVirtualRaceId: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, image, name
        case description
        case createdAt
        case updatedAt
        case vrVirtualRaceId
    }
    
    init() { }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.image = try? container.decode(String.self, forKey: .image)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.vrVirtualRaceId = try? container.decode(Int.self, forKey: .vrVirtualRaceId)
    }
}

//MARK: - SportRace
class SportRace: Codable {
    var name: String?
    var description: String?
    var unit: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case unit
    }
    
    init() { }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.unit = try? container.decode(String.self, forKey: .unit)
        self.description = try? container.decode(String.self, forKey: .description)
        self.name = try? container.decode(String.self, forKey: .name)
    }
}


//MARK: - StravaModel
class StravaModel: Codable {
    var id: Int?
    var name: String?
    var code: String?
    var info: StravaInfo?
    
    enum CodingKeys: String, CodingKey {
        case id
        case code
        case name
        case info
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try? container.decode(Int.self, forKey: .id)
        self.code = try? container.decode(String.self, forKey: .code)
        self.name = try? container.decode(String.self, forKey: .name)
        self.info = try container.decodeIfPresent(StravaInfo.self, forKey: .info)
    }
}

//MARK: - StravaInfo
class StravaInfo: Codable {
    
    enum CodingKeys: String, CodingKey {
        case id
        case profile
        case firstname
        case lastname
    }
    
    var id: Int?
    var profile: String?
    var firstname: String?
    var lastname: String?
    
    init (id: Int?, profile: String?, firstname: String?, lastname: String?) {
        self.id = id
        self.profile = profile
        self.firstname = firstname
        self.lastname = lastname
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        profile = try container.decodeIfPresent(String.self, forKey: .profile)
        firstname = try container.decodeIfPresent(String.self, forKey: .firstname)
        lastname = try container.decodeIfPresent(String.self, forKey: .lastname)
    }
}

//MARK: - CustomerGoalModel
class CustomerGoalModel: Codable {
    var id: Int?
    var vrVirtualRaceId: Int?
    var createdAt: String?
    var goal: String?
    var descriptionValue: String?
    var updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case vrVirtualRaceId
        case createdAt
        case goal
        case descriptionValue
        case updatedAt
    }
    
    init() { }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.vrVirtualRaceId = try container.decodeIfPresent(Int.self, forKey: .vrVirtualRaceId)
        self.createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt)
        self.goal = try container.decodeIfPresent(String.self, forKey: .goal)
        self.descriptionValue = try container.decodeIfPresent(String.self, forKey: .descriptionValue)
        self.updatedAt = try container.decodeIfPresent(String.self, forKey: .updatedAt)
    }
}

class RaceBannersModel: Codable {
    var image: String?
    var url: String?
    var id: Int?
    var vrId: Int?
    
    enum CodingKeys: String, CodingKey {
        case image
        case url
        case id
        case vrId
    }
    
    init() { }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.image = try container.decodeIfPresent(String.self, forKey: .image)
        self.url = try container.decodeIfPresent(String.self, forKey: .url)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.vrId = try container.decodeIfPresent(Int.self, forKey: .vrId)
    }
}

class StravaTrackingModel: Codable {
    var unit: String?
    var totalDistanceCustomer: Float?
    var totalDistance: Float?
    
    enum CodingKeys: String, CodingKey {
        case unit
        case totalDistanceCustomer
        case totalDistance
    }
    
    init() {
        //
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.unit = try container.decodeIfPresent(String.self, forKey: .unit)
        self.totalDistance = try container.decodeIfPresent(Float.self, forKey: .totalDistance)
        self.totalDistanceCustomer = try container.decodeIfPresent(Float.self, forKey: .totalDistanceCustomer)
    }

}
