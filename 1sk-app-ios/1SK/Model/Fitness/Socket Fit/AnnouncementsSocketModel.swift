//
//  AnnouncementsSocketModel.swift
//  1SK
//
//  Created by Tiến Trần on 23/09/2022.
//

import Foundation

class AnnouncementsSocketModel: Codable {
    var message: String?
    var data: Announcements?
    var socket: String?
    
    init(data: [String: Any]) {
        self.message = data["message"] as? String
        
        if let dataList = data["data"] as? [String: Any] {
            let temp = Announcements(data: dataList)
            self.data = temp
        }
        
        self.socket = data["socket"] as? String
    }
}

class Announcements: Codable {
    var id: Int?
    var title: String?
    var slug: String?
    var description: String?
    var content: String?
    var createdAt: Int?
    
    init(data: [String: Any]) {
        self.id = data["id"] as? Int
        self.title = data["title"] as? String
        self.slug = data["slug"] as? String
        self.description = data["description"] as? String
        self.content = data["content"] as? String
        self.createdAt = data["created_at"] as? Int
    }
}
