//
//  UserRegisterVRModel.swift
//  1SK
//
//  Created by Thaad on 12/09/2022.
//

import Foundation

class UserRegisterVRModel: Codable {
    var message: String?
    var race: RaceModel?
    var socket: String?
    
    init(data: [String: Any]) {
        self.message = data["message"] as? String
        
        if let dataObj = data["data"] as? [String: Any] {
            let race = RaceModel()
            race.id = dataObj["id"] as? Int
            race.isRegistered = dataObj["is_registered"] as? Bool
            race.statusOfHappening = dataObj["status_of_happening"] as? Int
            race.name = dataObj["name"] as? String
            race.unitGoal = dataObj["unit_goal"] as? String
            race.thumbnail = dataObj["thumbnail"] as? String
            race.startAt = dataObj["start_at"] as? String
            race.finishAt = dataObj["finish_at"] as? String
            race.registerExpireAt = dataObj["register_expire_at"] as? String
            race.status = dataObj["status"] as? String
            race.location = dataObj["location"] as? String
            race.information = dataObj["information"] as? String
            race.totalParticipants = dataObj["total_participants"] as? Int
            race.slug = dataObj["slug"] as? String
            race.value = dataObj["value"] as? Int
            race.limit = dataObj["limit"] as? Int
            race.award = dataObj["award"] as? String
            race.rule = dataObj["rule"] as? String
            race.utilities = dataObj["utilities"] as? String
            race.imageEbib = dataObj["image_ebib"] as? String
            race.imageUnfinishedMedal = dataObj["image_unfinished_medal"] as? String
            race.imageFinishMedal = dataObj["image_finish_medal"] as? String
            race.imageEcert = dataObj["image_ecert"] as? String
            race.lastPublishedAt = dataObj["last_published_at"] as? String
            
            if let sportObj = dataObj["sport"] as? [String: Any] {
                let sportRace = SportRace()
                sportRace.unit = sportObj["unit"] as? String
                sportRace.description = sportObj["description"] as? String
                sportRace.name = sportObj["name"] as? String
                
                self.race?.sport = sportRace
            }
            
            if let goalsList = dataObj["goals"] as? [[String: Any]] {
                self.race?.goals = []
                goalsList.forEach { goalObj in
                    let goalRace = GoalRace()
                    
                    goalRace.id = goalObj["id"] as? Int
                    goalRace.goal = goalObj["goal"] as? String
                    goalRace.goalDescription = goalObj["goal_description"] as? String
                    goalRace.vrVirtualRaceId = goalObj["vrVirtual_race_id"] as? Int
                    goalRace.createdAt = goalObj["created_at"] as? String
                    goalRace.updatedAt = goalObj["updated_at"] as? String
                    
                    self.race?.goals?.append(goalRace)
                }
            }
            
            if let awardsList = dataObj["awards"] as? [[String: Any]] {
                self.race?.awards = []
                awardsList.forEach { awardObj in
                    let awardRace = AwardRace()
                    
                    awardRace.id = awardObj["id"] as? Int
                    awardRace.image = awardObj["image"] as? String
                    awardRace.name = awardObj["name"] as? String
                    awardRace.description = awardObj["description"] as? String
                    awardRace.createdAt = awardObj["created_at"] as? String
                    awardRace.updatedAt = awardObj["updated_at"] as? String
                    awardRace.vrVirtualRaceId = awardObj["vrVirtual_race_id"] as? Int
                    
                    self.race?.awards?.append(awardRace)
                }
            }
            
            if let customerGoalObj = dataObj["customer_goal"] as? [String: Any] {
                let customerGoal = CustomerGoalModel()
                customerGoal.id = customerGoalObj["id"] as? Int
                customerGoal.vrVirtualRaceId = customerGoalObj["vrVirtual_race_id"] as? Int
                customerGoal.createdAt = customerGoalObj["created_at"] as? String
                customerGoal.goal = customerGoalObj["goal"] as? String
                customerGoal.descriptionValue = customerGoalObj["description_value"] as? String
                customerGoal.updatedAt = customerGoalObj["updated_at"] as? String
                
                self.race?.customerGoal = customerGoal
            }
            
            if let bannersList = dataObj["banners"] as? [[String: Any]] {
                self.race?.banners = []
                bannersList.forEach { bannerObj in
                    let raceBanner = RaceBannersModel()
                    
                    raceBanner.image = bannerObj["image"] as? String
                    raceBanner.url = bannerObj["url"] as? String
                    raceBanner.id = bannerObj["id"] as? Int
                    raceBanner.vrId = bannerObj["vr_id"] as? Int
                    
                    self.race?.banners?.append(raceBanner)
                }
            }
            
            if let stravaTrackingObj = dataObj["strava_tracking"] as? [String: Any] {
                let stravaTracking = StravaTrackingModel()
                stravaTracking.unit = stravaTrackingObj["unit"] as? String
                stravaTracking.totalDistance = stravaTrackingObj["total_distance"] as? Float
                stravaTracking.totalDistanceCustomer = stravaTrackingObj["total_distance_customer"] as? Float
                
                self.race?.stravaTracking = stravaTracking
            }
            
            self.race = race
        }
    }
}
