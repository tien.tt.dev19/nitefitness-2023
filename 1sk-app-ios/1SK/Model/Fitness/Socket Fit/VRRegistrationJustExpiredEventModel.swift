//
//  VRRegistrationJustExpiredEventModel.swift
//  1SK
//
//  Created by Thaad on 12/09/2022.
//

import Foundation

class VRRegistrationJustExpiredEventModel: Codable {
    var message: String?
    var listExpiredId: [Int]?
    var socket: String?
    
    init(data: [String: Any]) {
        self.message = data["message"] as? String
        
        if let dataList = data["data"] as? [Int] {
            self.listExpiredId = []
            dataList.forEach { id in
                self.listExpiredId?.append(id)
            }
        }
    }
}
