//
//  VRShouldUpdateRankingEventModel.swift
//  1SK
//
//  Created by Thaad on 12/09/2022.
//

import Foundation

class VRShouldUpdateRankingEventModel: Codable {
    var message: String?
    var listRankingId: [Int]?
    var socket: String?
    
    init(data: [String: Any]) {
        self.message = data["message"] as? String
        
        if let dataList = data["data"] as? [Int] {
            self.listRankingId = []
            dataList.forEach { id in
                self.listRankingId?.append(id)
            }
        }
    }
}
