//
//  StravaAthleteActivityModel.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//

import Foundation


//MARK: StravaAthleteActivityModel
class StravaAthleteActivityModel: Codable {
    var id, customerId, athleteId: Int?
    var name: String?
    var distance: Double?
    var movingTime, elapsedTime: Int?
    var totalElevationGain: Double?
    var type, sportType, startDate, startDateLocal: String?
    var timezone: String?
    var averageSpeed, maxSpeed: Double?
    var illegal: Int?
    var illegalLabel: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case customerId
        case athleteId
        case name, distance
        case movingTime
        case elapsedTime
        case totalElevationGain
        case type
        case sportType
        case startDate
        case startDateLocal
        case timezone
        case averageSpeed
        case maxSpeed
        case illegal, illegalLabel
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.customerId = try? container.decodeIfPresent(Int.self, forKey: .customerId)
        self.athleteId = try? container.decodeIfPresent(Int.self, forKey: .athleteId)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
        self.distance = try? container.decodeIfPresent(Double.self, forKey: .distance)
        self.movingTime = try? container.decodeIfPresent(Int.self, forKey: .movingTime)
        self.elapsedTime = try? container.decodeIfPresent(Int.self, forKey: .elapsedTime)
        self.totalElevationGain = try? container.decodeIfPresent(Double.self, forKey: .totalElevationGain)
        self.type = try? container.decodeIfPresent(String.self, forKey: .type)
        self.sportType = try? container.decodeIfPresent(String.self, forKey: .sportType)
        self.startDate = try? container.decodeIfPresent(String.self, forKey: .startDate)
        self.startDateLocal = try? container.decodeIfPresent(String.self, forKey: .startDateLocal)
        self.timezone = try? container.decodeIfPresent(String.self, forKey: .timezone)
        self.averageSpeed = try? container.decodeIfPresent(Double.self, forKey: .averageSpeed)
        self.maxSpeed = try? container.decodeIfPresent(Double.self, forKey: .maxSpeed)
        self.illegal = try? container.decodeIfPresent(Int.self, forKey: .illegal)
        self.illegalLabel = try? container.decodeIfPresent(String.self, forKey: .illegalLabel)
    }
}


//MARK: vr_sportModel
class VRSportModel: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var unit: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case unit
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
        self.description = try? container.decodeIfPresent(String.self, forKey: .description)
        self.unit = try? container.decodeIfPresent(String.self, forKey: .unit)
    }
}

// MARK: ActivitiesTotal
class ActivitiesTotal: Codable {
    var totalDistance: Double?
    var unitDistance, totalMovingTime, unitMovingTime: String?

    enum CodingKeys: String, CodingKey {
        case totalDistance
        case unitDistance
        case totalMovingTime
        case unitMovingTime
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.totalDistance = try? container.decodeIfPresent(Double.self, forKey: .totalDistance)
        self.unitDistance = try? container.decodeIfPresent(String.self, forKey: .unitDistance)
        self.totalMovingTime = try? container.decodeIfPresent(String.self, forKey: .totalMovingTime)
        self.unitMovingTime = try? container.decodeIfPresent(String.self, forKey: .unitMovingTime)
    }
}
