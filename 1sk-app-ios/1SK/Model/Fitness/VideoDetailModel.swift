//
//  VideoDetailModel.swift
//  1SK
//
//  Created by tuyenvx on 18/01/2021.
//

import Foundation

class VideoDetailModel: Codable {
    var id: Int?
    var attribute: AttributeModel?
    var value: AtrributeValueModel?
    
    func getTitle() -> String {
        let atributeName = attribute?.name ?? ""
        let valueName = value?.name ?? ""
        return "\(atributeName): \(valueName)"
    }

    func getAtrributedTitle() -> NSMutableAttributedString {
        let atributeName = NSAttributedString(string: "\(attribute?.name ?? ""): ",
                                              attributes: [NSAttributedString.Key.foregroundColor: R.color.subTitle()!])
        let valueName = NSAttributedString(string: value?.name ?? "",
                                           attributes: [NSAttributedString.Key.foregroundColor: R.color.darkText()!])
        let title = NSMutableAttributedString(attributedString: atributeName)
        title.append(valueName)
        return title
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case attribute
        case value
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.attribute = try? values.decode(AttributeModel.self, forKey: .attribute)
        self.value = try? values.decode(AtrributeValueModel.self, forKey: .value)
    }
}

class AttributeModel: Codable {
    var id: Int?
    var name: String?
    var values: [AtrributeValueModel]?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.values = try? container.decode([AtrributeValueModel].self, forKey: .values)
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case values
    }
}

class AtrributeValueModel: Codable {
    var id: Int?
    var name: String?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
    }
    
    enum CodingKeys: String, CodingKey {
        case id, name
    }
}
