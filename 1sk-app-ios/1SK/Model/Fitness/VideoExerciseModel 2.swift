//
//  VideoExerciseModel.swift
//  1SK
//
//  Created by Valerian on 24/06/2022.
//

import Foundation

//class VideoExerciseModel: Codable {
//    var id: Int?
//    var name, welcomeDescription, slug: String?
//    var image: String?
//    var trailer, duration, durationDisplay: String?
//    var categories: [CategoryModels]?
//    var link, urlM3U8: String?
//    var linkMp4: String?
//    var timeAction: Int?
//    var order: String?
//    var status, statusConvert: Int?
//    var source: String?
//    var isFeatured: Bool?
//
//    enum CodingKeys: String, CodingKey {
//        case id, name
//        case welcomeDescription = "description"
//        case slug, image, trailer, duration
//        case durationDisplay
//        case categories, link
//        case urlM3U8
//        case linkMp4
//        case timeAction
//        case order, status
//        case statusConvert
//        case source
//        case isFeatured
//    }
//
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        self.id = try? container.decode(Int.self, forKey: .id)
//        self.welcomeDescription = try? container.decode(String.self, forKey: .welcomeDescription)
//        self.name = try? container.decode(String.self, forKey: .name)
//        self.image = try? container.decode(String.self, forKey: .image)
//        self.slug = try? container.decode(String.self, forKey: .slug)
//        self.trailer = try? container.decode(String.self, forKey: .trailer)
//        self.duration = try? container.decode(String.self, forKey: .duration)
//        self.durationDisplay = try? container.decode(String.self, forKey: .durationDisplay)
//        self.categories = try? container.decode([CategoryModels].self, forKey: .categories)
//        self.link = try? container.decode(String.self, forKey: .link)
//        self.urlM3U8 = try? container.decode(String.self, forKey: .urlM3U8)
//        self.linkMp4 = try? container.decode(String.self, forKey: .linkMp4)
//        self.timeAction = try? container.decode(Int.self, forKey: .timeAction)
//        self.order = try? container.decode(String.self, forKey: .order)
//        self.status = try? container.decode(Int.self, forKey: .status)
//        self.statusConvert = try? container.decode(Int.self, forKey: .statusConvert)
//        self.source = try? container.decode(String.self, forKey: .source)
//        self.isFeatured = try? container.decode(Bool.self, forKey: .isFeatured)
//    }
//}
//
//MARK: - CategoryModels
class CategoryModels: Codable {
    var id: Int?
    var name, slug: String?
    var categoryDescription: String?
    var parentID, type, order, status: Int?
    var deletedAt: String?
    var createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name, slug
        case categoryDescription = "description"
        case parentID
        case type, order, status
        case deletedAt
        case createdAt
        case updatedAt
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.slug = try? container.decode(String.self, forKey: .slug)
        self.categoryDescription = try? container.decode(String.self, forKey: .categoryDescription)
        self.parentID = try? container.decode(Int.self, forKey: .parentID)
        self.type = try? container.decode(Int.self, forKey: .type)
        self.order = try? container.decode(Int.self, forKey: .order)
        self.status = try? container.decode(Int.self, forKey: .status)
        self.deletedAt = try? container.decode(String.self, forKey: .deletedAt)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

//MARK: - CategoryVideoExerciseModel
class CategoryVideoExerciseModel: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var image: String?
    var slug: String?
//    var attributes: []

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case image
        case slug
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.description = try? container.decode(String.self, forKey: .description)
        self.name = try? container.decode(String.self, forKey: .name)
        self.image = try? container.decode(String.self, forKey: .image)
        self.slug = try? container.decode(String.self, forKey: .slug)
    }
}

// MARK: - VideoExerciseModel
class VideoExerciseModel: Codable {

    var id: Int?
    var name: String?
    var description: String?
    var trailer: String?
    var trailerCdn: String?
    var url: String?
    var slug: String?
    var image: String?
    var categoryId: Int?
    var category: Category?
    var categories: [Categories]?
    var coachId: String?
    var coach: Coach?
    var linkShare: String?
    var link: String?
    var urlM3u8: String?
    var type: String?
//    var mimeType: Any?
    var size: Int?
    var duration: String?
//    var timeAction: Any?
    var details: [Details]?
    var status: Int?
    var position: [Position]?
    var statusConvert: Int?
//    var tags: [Any]?
    var createdBy: CreatedByModel?
//    var authors: Any?
    var history: History?
    var createdAt: String?
    var updatedAt: String?
    var source: String?
    var isFeatured: Bool?
    var segments: [SegmentModel]?
    var isLiked: Bool?
    var portals: [Portals]?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case trailer
        case trailerCdn
        case url
        case slug
        case image
        case categoryId 
        case category
        case categories
        case coachId
        case coach
        case linkShare
        case link
        case urlM3u8
        case type
//        case mimeType
        case size
        case duration
//        case timeAction
        case details
        case status
        case position
        case statusConvert
//        case tags
        case createdBy
//        case authors
        case createdAt
        case updatedAt
        case source
        case isFeatured
        case segments
        case isLiked
        case portals
        case history
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.trailer = try? container.decode(String.self, forKey: .trailer)
        self.trailerCdn = try? container.decode(String.self, forKey: .trailerCdn)
        self.url = try? container.decode(String.self, forKey: .url)
        self.slug = try? container.decode(String.self, forKey: .slug)
        self.image = try? container.decode(String.self, forKey: .image)
        self.categoryId = try? container.decode(Int.self, forKey: .categoryId)
        self.category = try? container.decode(Category.self, forKey: .category)
        self.categories = try? container.decode([Categories].self, forKey: .categories)
        self.coachId = try? container.decode(String.self, forKey: .coachId)
        self.coach = try? container.decode(Coach.self, forKey: .coach)
        self.linkShare = try? container.decode(String.self, forKey: .linkShare)
        self.link = try? container.decode(String.self, forKey: .link)
        self.urlM3u8 = try? container.decode(String.self, forKey: .urlM3u8)
        self.type = try? container.decode(String.self, forKey: .type)
//        mimeType = nil
        self.size = try? container.decode(Int.self, forKey: .size)
        self.duration = try? container.decode(String.self, forKey: .duration)
//        timeAction = nil
        self.details = try? container.decode([Details].self, forKey: .details)
        self.status = try? container.decode(Int.self, forKey: .status)
        self.position = try? container.decode([Position].self, forKey: .position)
        self.statusConvert = try? container.decode(Int.self, forKey: .statusConvert)
//        tags = []
        self.createdBy = try? container.decode(CreatedByModel.self, forKey: .createdBy)
//        authors = nil
        self.history = try? container.decode(History.self, forKey: .history)
        self.isLiked = try? container.decode(Bool.self, forKey: .isLiked)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.source = try? container.decode(String.self, forKey: .source)
        self.isFeatured = try? container.decode(Bool.self, forKey: .isFeatured)
        self.segments = try? container.decode([SegmentModel].self, forKey: .segments)
        self.portals = try? container.decode([Portals].self, forKey: .portals)
    }
}

//MARK: - Category Model
class Category: Codable {

    var id: Int?
    var name: String?
    var description: String?
    var image: String?
    var slug: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case image
        case slug
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
        self.slug = try? container.decode(String.self, forKey: .slug)
    }
}

//MARK: - Categories Model
class Categories: Codable {
    
    var id: Int?
    var name: String?
    var slug: String?
//    var description: Any
    var parentId: Int?
    var type: Int?
    var order: Int?
    var status: Int?
//    var deletedAt: Any
    var createdAt: String?
    var updatedAt: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case slug
//        case description
        case parentId
        case type
        case order
        case status
//        case deletedAt
        case createdAt
        case updatedAt
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.slug = try? container.decode(String.self, forKey: .slug)
//        description = nil
        self.parentId = try? container.decode(Int.self, forKey: .parentId)
        self.type = try? container.decode(Int.self, forKey: .type)
        self.order = try? container.decode(Int.self, forKey: .order)
        self.status = try? container.decode(Int.self, forKey: .status)
//        deletedAt = nil
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

//MARK: - Details Model
class Details: Codable {

    var id: Int?
    var attribute: Attribute?
    var value: Value?

    private enum CodingKeys: String, CodingKey {
        case id
        case attribute
        case value
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.attribute = try? values.decode(Attribute.self, forKey: .attribute)
        self.value = try? values.decode(Value.self, forKey: .value)
    }
}

//MARK: - Attribute Model
class Attribute: Codable {

    var id: Int?
    var name: String?
    var values: [ValuesVideoExerciseModel]?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case values
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.values = try? container.decode([ValuesVideoExerciseModel].self, forKey: .values)
    }
}

//MARK: - Values Model
class ValuesVideoExerciseModel: Codable {

    var id: Int?
    var name: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.name = try? values.decode(String.self, forKey: .name)
    }
}

//MARK: - Value Model
class Value: Codable {

    var id: Int?
    var name: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.name = try? values.decode(String.self, forKey: .name)
    }
}

//MARK: - Position Model
class Position: Codable {

    var id: Int?
    var name: String?
    var pivot: PivotVideoExerciseModel?

    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case pivot
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.name = try? values.decode(String.self, forKey: .name)
        self.pivot = try? values.decode(PivotVideoExerciseModel.self, forKey: .pivot)
    }
}

//MARK: - Pivot Model
class PivotVideoExerciseModel: Codable {

   var modelId: Int?
   var positionId: Int?
   var modelType: String?

    private enum CodingKeys: String, CodingKey {
        case modelId
        case positionId
        case modelType
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.modelId = try? values.decode(Int.self, forKey: .modelId)
        self.positionId = try? values.decode(Int.self, forKey: .positionId)
        self.modelType = try? values.decode(String.self, forKey: .modelType)
    }
}

//MARK: - CreatedBy Model
class CreatedByModel: Codable {

    var id: Int?
    var userId: Int?
    var email: String?
    var username: String?
    var information: String?
    var createdAt: String?
    var updatedAt: String?
//    var deletedAt: Any
    var fullName: String?

    private enum CodingKeys: String, CodingKey {
        case id
        case userId
        case email
        case username
        case information
        case createdAt
        case updatedAt
//        case deletedAt = "deleted_at"
        case fullName
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.userId = try? values.decode(Int.self, forKey: .userId)
        self.email = try? values.decode(String.self, forKey: .email)
        self.username = try? values.decode(String.self, forKey: .username)
        self.information = try? values.decode(String.self, forKey: .information)
        self.createdAt = try? values.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? values.decode(String.self, forKey: .updatedAt)
//        deletedAt = nil
        self.fullName = try? values.decode(String.self, forKey: .fullName)
    }
}

//MARK: - Portals Model
class Portals: Codable {
    
    var id: Int?
    var name: String?
    var parentId: Int?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case parentId
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.name = try? values.decode(String.self, forKey: .name)
        self.parentId = try? values.decode(Int.self, forKey: .parentId)
    }
}

//MARK: - Coach Model
class Coach: Codable {
    var id: Int?
    var userId: Int?
    var name: String?
    var slug: String?
    var avatar: String?
    var gender: String?
    var description: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case userId
        case name
        case slug
        case avatar
        case gender
        case description
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.userId = try? values.decode(Int.self, forKey: .userId)
        self.name = try? values.decode(String.self, forKey: .name)
        self.slug = try? values.decode(String.self, forKey: .slug)
        self.avatar = try? values.decode(String.self, forKey: .avatar)
        self.gender = try? values.decode(String.self, forKey: .gender)
        self.description = try? values.decode(String.self, forKey: .description)
    }
}

//MARK: - Segment Model
class SegmentModel: Codable {
    var id: Int?
    var title: String?
    var image: String?
    var startAt: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case title
        case image
        case startAt
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.title = try? values.decode(String.self, forKey: .title)
        self.image = try? values.decode(String.self, forKey: .image)
        self.startAt = try? values.decode(String.self, forKey: .startAt)
    }
}

//MARK: - History Model
class History: Codable {
    var id: Int?
    var videoId: Int?
    var userId: Int?
    var createdAt: String?
    var updatedAt: String?
    var lastSeenAt: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case videoId
        case userId
        case createdAt
        case updatedAt
        case lastSeenAt
    }
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? values.decode(Int.self, forKey: .id)
        self.videoId = try? values.decode(Int.self, forKey: .videoId)
        self.userId = try? values.decode(Int.self, forKey: .userId)
        self.createdAt = try? values.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? values.decode(String.self, forKey: .updatedAt)
        self.lastSeenAt = try? values.decode(String.self, forKey: .lastSeenAt)
    }
}
