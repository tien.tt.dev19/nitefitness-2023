//
//  VisualRaceModel.swift
//  1SK
//
//  Created by Valerian on 24/06/2022.
//

import Foundation

class VisualRaceModel: Codable {
    var id: Int?
    var name: String?
    var unitGoal, thumbnail: String?
    var startAt, finishAt, registerExpireAt, status: String?
    var location, information: String?
    var sport: SportModel?
    var goals: [GoalModel]?
    var awards: [AwardModel]?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case unitGoal = "unit_goal"
        case thumbnail
        case sport
        case startAt = "start_at"
        case finishAt = "finish_at"
        case registerExpireAt = "register_expire_at"
        case status, location, information, goals, awards
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.unitGoal = try? container.decode(String.self, forKey: .unitGoal)
        self.thumbnail = try? container.decode(String.self, forKey: .thumbnail)
        self.startAt = try? container.decode(String.self, forKey: .startAt)
        self.finishAt = try? container.decode(String.self, forKey: .finishAt)
        self.registerExpireAt = try? container.decode(String.self, forKey: .registerExpireAt)
        self.status = try? container.decode(String.self, forKey: .status)
        self.location = try? container.decode(String.self, forKey: .location)
        self.information = try? container.decode(String.self, forKey: .information)
        self.sport = try? container.decode(SportModel.self, forKey: .sport)
        self.goals = try? container.decode([GoalModel].self, forKey: .goals)
        self.awards = try? container.decode([AwardModel].self, forKey: .awards)
    }
}

//MARK: - GoalModel
class GoalModel: Codable {
    var id: Int?
    var goal, goalDescription: String?
    var vrVirtualRaceID: Int?
    var createdAt, updatedAt: String?
    
    enum CodingKeys: String, CodingKey {
        case id, goal
        case goalDescription = "description"
        case vrVirtualRaceID = "vr_virtual_race_id"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.goal = try? container.decode(String.self, forKey: .goal)
        self.goalDescription = try? container.decode(String.self, forKey: .goalDescription)
        self.vrVirtualRaceID = try? container.decode(Int.self, forKey: .vrVirtualRaceID)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
    }
}

//MARK: - AwardModel
class AwardModel: Codable {
    var id: Int?
    var image, name: String?
    var awardDescription: String?
    var createdAt, updatedAt: String?
    var vrVirtualRaceID: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, image, name
        case awardDescription = "description"
        case createdAt
        case updatedAt
        case vrVirtualRaceID
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.image = try? container.decode(String.self, forKey: .image)
        self.name = try? container.decode(String.self, forKey: .name)
        self.awardDescription = try? container.decode(String.self, forKey: .awardDescription)
        self.createdAt = try? container.decode(String.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(String.self, forKey: .updatedAt)
        self.vrVirtualRaceID = try? container.decode(Int.self, forKey: .vrVirtualRaceID)
    }
}

//MARK: - SportModel
class SportModel: Codable {
    var name: String?
    var description: String?
    var unit: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case description
        case unit
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.unit = try? container.decode(String.self, forKey: .unit)
        self.description = try? container.decode(String.self, forKey: .description)
        self.name = try? container.decode(String.self, forKey: .name)
    }
}
