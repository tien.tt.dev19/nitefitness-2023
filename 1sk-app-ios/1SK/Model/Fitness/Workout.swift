//
//  Workout.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//

import Foundation

struct Workout {
    var id: String?
    var userId: String?
    var userName: String?
    var userAvartar: String?
    var isTemplate: Int?
    var name: String?
    var time: Int?
    var timeBreak: Int?
    var status: Int?
    var finish: Int?
    var isBookmark: Int?
    var subject: Subject?
    var level: Level?
    var type: Type?
    var details: [WorkoutDetail]?
    var createdAt: String?

    init() {
        self.subject = Subject()
        self.level = Level()
        self.type = Type()
        self.details = [WorkoutDetail]()
    }
}

struct WorkoutDetail {
    var id: String?
    var excercise: Exsercise?
    var order: Int?
    var time: Int?
    var createdAt: String?
    
    init() {
        self.excercise = Exsercise()
    }
}
