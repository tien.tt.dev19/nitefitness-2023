//
//  ActivityUserModel.swift
//  1SK
//
//  Created by tuyenvx on 10/06/2021.
//

import Foundation

struct ActivityUserModel: Codable {
    var activityId: String?
}

class ActivityModel: Codable {
    var speed: String?
    var createdAt: String?
    var totalTime: String?
    var status: Int?
    var startTime: String?
    var userId: Int?
    var averageHeartRate: String?
    var name: String?
    var updatedAt: String?
    var wayLength: String?
    var id: Int?
    var type: Int?
    var caloriesBurned: String?
    var activitySubjectId: Int?
    
    enum CodingKeys: String, CodingKey {
        case speed
        case createdAt
        case totalTime
        case status
        case startTime
        case userId
        case averageHeartRate
        case name
        case updatedAt
        case wayLength
        case id
        case type
        case caloriesBurned
        case activitySubjectId
        case activitySubjectType
        case imageUrl
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        speed = try container.decodeIfPresent(String.self, forKey: .speed)
        createdAt = try container.decodeIfPresent(String.self, forKey: .createdAt)
        totalTime = try container.decodeIfPresent(String.self, forKey: .totalTime)
        status = try container.decodeIfPresent(Int.self, forKey: .status)
        startTime = try container.decodeIfPresent(String.self, forKey: .startTime)
        userId = try container.decodeIfPresent(Int.self, forKey: .userId)
        averageHeartRate = try container.decodeIfPresent(String.self, forKey: .averageHeartRate)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        updatedAt = try container.decodeIfPresent(String.self, forKey: .updatedAt)
        wayLength = try container.decodeIfPresent(String.self, forKey: .wayLength)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        type = try container.decodeIfPresent(Int.self, forKey: .type)
        caloriesBurned = try container.decodeIfPresent(String.self, forKey: .caloriesBurned)
        activitySubjectId = try container.decodeIfPresent(Int.self, forKey: .activitySubjectId)
        self.activitySubjectType = try container.decodeIfPresent(Int.self, forKey: .activitySubjectType)
        self.imageUrl = try container.decodeIfPresent([String].self, forKey: .activitySubjectType)
    }
    
    var activitySubjectType: Int?
    var address: String?
    var description: String?
    var imageUrl: [String]?
    
    var activityType: ActivityType? {
        if let type = self.activitySubjectType {
            return ActivityType(rawValue: type)
            
        } else if let type = self.type {
            return ActivityType(rawValue: type)
            
        } else {
            return nil
        }
    }
}
