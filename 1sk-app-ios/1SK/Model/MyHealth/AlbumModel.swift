//
//  AlbumModel.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//

import Foundation
import Photos

class AlbumModel {
    var album: PHAssetCollection?
    var imageFetchResult: PHFetchResult<PHAsset>?
    var name: String? {
        if let `album` = album {
            return album.localizedTitle
        } else {
            return "Tất cả ảnh"
        }
    }

    var count: Int {
        return imageFetchResult?.count ?? 0
    }

    init(album: PHAssetCollection?, fetchResult: PHFetchResult<PHAsset>? = nil) {
        self.album = album
        self.imageFetchResult = fetchResult
    }
}
