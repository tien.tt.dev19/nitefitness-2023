//
//  FileModel.swift
//  1SK
//
//  Created by tuyenvx on 15/01/2021.
//

import Foundation
//import RealmSwift

//class FileListModel: Object {
//    @objc dynamic var profileID: String = ""
//    @objc dynamic var folderID: String = ""
//    let files = List<FileModel>()
//
//    override class func primaryKey() -> String? {
//        return "folderID"
//    }
//
//    convenience init(profielID: String, folderID: String, files: [FileModel]) {
//        self.init()
//        self.profileID = profileID
//        self.folderID = folderID
//        self.files.append(objectsIn: files)
//    }
//}

class FileModel: Codable {
    var id: String = ""
    var name: String = ""
    var url: String = ""
    var note: String = ""

    convenience init(id: String, name: String, url: String, note: String) {
        self.init()
        self.id = id
        self.name = name
        self.url = url
        self.note = note
    }
}
