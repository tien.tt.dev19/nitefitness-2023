//
//  AnouncementModels.swift
//  1SK
//
//  Created by Tiến Trần on 22/09/2022.
//

import Foundation

// MARK: - AnnouncementModels
class AnnouncementModels: Codable {
    var id: Int?
    var title: String?
    var slug: String?
    var description: String?
    var content: String?
    var createdAt: Int?
    var isRead: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, title, slug
        case isRead
        case createdAt
        case description
        case content
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.title = try? container.decodeIfPresent(String.self, forKey: .title)
        self.slug = try? container.decodeIfPresent(String.self, forKey: .slug)
        self.description = try? container.decodeIfPresent(String.self, forKey: .description)
        self.content = try? container.decodeIfPresent(String.self, forKey: .content)
        self.createdAt = try? container.decodeIfPresent(Int.self, forKey: .createdAt)
        self.isRead = try? container.decodeIfPresent(Bool.self, forKey: .isRead)
    }
    
    init(socketMessage: AnnouncementsSocketModel?) {
        self.id = socketMessage?.data?.id
        self.title = socketMessage?.data?.title
        self.content = socketMessage?.data?.content
        self.description = socketMessage?.data?.description
        self.createdAt = socketMessage?.data?.createdAt
        self.slug = socketMessage?.data?.slug
        self.isRead = false
    }
}
