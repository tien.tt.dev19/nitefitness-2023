//
//  NotificationModel.swift
//  1SK
//
//  Created by tuyenvx on 25/02/2021.
//

import Foundation

class NotificationModel: Codable {
    var id: Int?
    var title, content: String?
    var isRead: Bool?
    var createdAt, updatedAt: Int?
    var avatar: String?
    
    var extends: ExtendsModel?
    
    enum CodingKeys: String, CodingKey {
        case id, title, content
        case isRead
        case createdAt
        case updatedAt
        case avatar
        case extends
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.title = try? container.decode(String.self, forKey: .title)
        self.content = try? container.decode(String.self, forKey: .content)
        self.isRead = try? container.decode(Bool.self, forKey: .isRead)
        self.createdAt = try? container.decode(Int.self, forKey: .createdAt)
        self.updatedAt = try? container.decode(Int.self, forKey: .updatedAt)
        self.avatar = try? container.decode(String.self, forKey: .avatar)
        
        self.extends = try? container.decode(ExtendsModel.self, forKey: .extends)
    }
}

class ExtendsModel: Codable {
    var appointmentId: Int?
    
    enum CodingKeys: String, CodingKey {
        case appointmentId
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.appointmentId = try? container.decode(Int.self, forKey: .appointmentId)
    }
}
