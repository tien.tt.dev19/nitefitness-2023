//
//  CartProductSchema.swift
//  1SK
//
//  Created by Thaad on 15/03/2022.
//

import Foundation

class CartProductSchema: Codable {
    var id: Int?
    var name: String?
    var image: String?
    var price: Int?
    var pricerOld: Int?
    var isSalePrice: Bool?
    var amount: Int?
    var isSelected: Bool?
    var attributes: [AttributeProduct]?
}

class AttributeProduct: Codable {
    var id: Int?
    var code: String?
    var title: String?
    var value: String?
}
