//
//  CountriesModel.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//

import Foundation

class CountryModel: Codable {
    var id: Int?
    var name: String?
    var code: String?
    var nationality: String?
    var states: [StateAddress]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, code, nationality, states
    }
    
    init() {
        // None
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.code = try? container.decode(String.self, forKey: .code)
        self.nationality = try? container.decode(String.self, forKey: .nationality)
        self.states = try? container.decode([StateAddress].self, forKey: .states)
    }
}

// MARK: StatesCountries
class StateAddress: Codable {
    var id: Int?
    var name: String?
    var isSelected: Bool?
    var cities: [CityAddress]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, cities
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.cities = try? container.decode([CityAddress].self, forKey: .cities)
    }
    
}

// MARK: CitiesCountries
class CityAddress: Codable {
    var id: Int?
    var name: String?
    var isSelected: Bool?
    var districts: [DistrictAddress]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, districts
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.districts = try? container.decode([DistrictAddress].self, forKey: .districts)
    }
    
}

// MARK: DistrictsCountries
class DistrictAddress: Codable {
    var id: Int?
    var name: String?
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, name
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
    }
    
}
