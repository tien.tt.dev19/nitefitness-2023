//
//  DeliveryModel.swift
//  1SK
//
//  Created by Thaad on 18/03/2022.
//

import Foundation

class DeliveryModel: Codable {
    var id: Int?
    var name: String?
    var email: String?
    var phone, address, type: String?
    var customerId, isDefault: Int?
    var country: CountryModel?
    var state: StateAddress?
    var city: CityAddress?
    var district: DistrictAddress?
    var customer: CustomerAddress?
    var typeDelivery: TypeDelivery?
    
    enum CodingKeys: String, CodingKey {
        case id, name, email, phone, address, type
        case customerId
        case isDefault
        case country, state, city, district, customer
    }
    
    init() {
        //
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.email = try? container.decode(String.self, forKey: .email)
        self.phone = try? container.decode(String.self, forKey: .phone)
        self.address = try? container.decode(String.self, forKey: .address)
        self.type = try? container.decode(String.self, forKey: .type)
        self.customerId = try? container.decode(Int.self, forKey: .customerId)
        self.isDefault = try? container.decode(Int.self, forKey: .isDefault)
        self.country = try? container.decode(CountryModel.self, forKey: .country)
        self.state = try? container.decode(StateAddress.self, forKey: .state)
        self.city = try? container.decode(CityAddress.self, forKey: .city)
        self.district = try? container.decode(DistrictAddress.self, forKey: .district)
        self.customer = try? container.decode(CustomerAddress.self, forKey: .customer)
        
        switch self.type {
        case TypeDelivery.Home.rawValue:
            self.typeDelivery = .Home
            
        case TypeDelivery.Company.rawValue:
            self.typeDelivery = .Company
            
        default:
            break
        }
        
    }
    
}

// MARK: CustomerAddress
class CustomerAddress: Codable {
    var id: Int?
    var name, avatar, email, phone: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, avatar, email, phone
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.avatar = try? container.decode(String.self, forKey: .avatar)
        self.email = try? container.decode(String.self, forKey: .email)
        self.phone = try? container.decode(String.self, forKey: .phone)
    }
}

// MARK: TypeDelivery
enum TypeDelivery: String {
    case Home = "home"
    case Company = "company"
}
