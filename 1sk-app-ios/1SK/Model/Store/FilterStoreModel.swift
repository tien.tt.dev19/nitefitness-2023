//
//  FilterStoreModel.swift
//  1SK
//
//  Created by Thaad on 04/04/2022.
//

import Foundation

enum TypeFilterStore: String {
    case text
    case price
}

class FilterStoreModel: Codable {
    var id: Int?
    var title: String?
    var displayLayout: String?
    var attributes: [AttributeFilter]?
    var isExpand: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case attributes
        case displayLayout
    }
    
    init() {
        //
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.title = try? container.decode(String.self, forKey: .title)
        self.displayLayout = try? container.decode(String.self, forKey: .displayLayout)
        self.attributes = try? container.decode([AttributeFilter].self, forKey: .attributes)
        self.isExpand = true
        
        if let listAttributes = self.attributes, listAttributes.count > 0 {
            for attr in listAttributes {
                switch self.displayLayout {
                case TypeFilterStore.text.rawValue:
                    attr.type = TypeFilterStore.text

                case TypeFilterStore.price.rawValue:
                    attr.type = TypeFilterStore.price

                default:
                    break
                }
            }
        }
    }
}

// MARK: - AttributeFilter
class AttributeFilter: Codable {
    var id: Int?
    var title: String?
    var fromPrice, toPrice: Int?
    var isSelected: Bool?
    var type: TypeFilterStore?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case fromPrice
        case toPrice
    }
    
    init() {
        //
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.title = try? container.decode(String.self, forKey: .title)
        self.fromPrice = try? container.decode(Int.self, forKey: .fromPrice)
        self.toPrice = try? container.decode(Int.self, forKey: .toPrice)
    }
}
