//
//  FlashSaleModel.swift
//  1SK
//
//  Created by Tiến Trần on 09/08/2022.
//

import Foundation


class FlashSaleModel: Codable {
    var id: Int?
    var name, startDate, startTime, endDate: String?
    var endTime: String?
    var products: [Product]?
    
    enum CodingKeys: String, CodingKey {
         case id, name
         case startDate
         case startTime
         case endDate
         case endTime
         case products
     }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
       
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.startDate = try? container.decode(String.self, forKey: .startDate)
        self.startTime = try? container.decode(String.self, forKey: .startTime)
        self.endDate = try? container.decode(String.self, forKey: .endDate)
        self.endTime = try? container.decode(String.self, forKey: .endTime)
        self.products = try? container.decode([Product].self, forKey: .products)
    }
}

//MARK: - Product Model
class Product: Codable {
    var id: Int?
    var originalProduct: Int?
    var name: String?
    var sku: String?
    var image: String?
    var description: String?
    var quantity: Int?
    var allowCheckoutWhenOutOfStock: Bool?
    var stockStatus: String?
    var price, priceWithTaxes, originalPrice, frontSalePrice: Int?
    var frontSalePriceWithTaxes: Int?
    var isSalePrice: Bool?
    var salePercentage, reviewsCount, reviewsAvg, sold: Int?
    var quantityFlashSale: Int?
    var images: [String]?
    var information: [Information]?
    var attributes: [ProductAttribute]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, sku, image
        case originalProduct
        case description
        case quantity
        case allowCheckoutWhenOutOfStock
        case stockStatus
        case price
        case priceWithTaxes
        case originalPrice
        case frontSalePrice
        case frontSalePriceWithTaxes
        case isSalePrice
        case salePercentage
        case reviewsCount
        case reviewsAvg
        case sold
        case quantityFlashSale
        case images, information, attributes
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.originalProduct = try? container.decode(Int.self, forKey: .originalProduct)
        self.name = try? container.decode(String.self, forKey: .name)
        self.sku = try? container.decode(String.self, forKey: .sku)
        self.image = try? container.decode(String.self, forKey: .image)
        self.description = try? container.decode(String.self, forKey: .description)
        self.quantity = try? container.decode(Int.self, forKey: .quantity)
        self.allowCheckoutWhenOutOfStock = try? container.decode(Bool.self, forKey: .allowCheckoutWhenOutOfStock)
        self.stockStatus = try? container.decode(String.self, forKey: .stockStatus)
        self.price = try? container.decode(Int.self, forKey: .price)
        self.priceWithTaxes = try? container.decode(Int.self, forKey: .priceWithTaxes)
        self.originalPrice = try? container.decode(Int.self, forKey: .originalPrice)
        self.frontSalePrice = try? container.decode(Int.self, forKey: .frontSalePrice)
        self.frontSalePriceWithTaxes =  try? container.decode(Int.self, forKey: .frontSalePriceWithTaxes)
        self.isSalePrice = try? container.decode(Bool.self, forKey: .isSalePrice)
        self.salePercentage = try? container.decode(Int.self, forKey: .salePercentage)
        self.reviewsCount = try? container.decode(Int.self, forKey: .reviewsCount)
        self.reviewsAvg = try? container.decode(Int.self, forKey: .reviewsAvg)
        self.sold = try? container.decode(Int.self, forKey: .sold)
        self.quantityFlashSale = try? container.decode(Int.self, forKey: .quantityFlashSale)
        self.images = try? container.decode([String].self, forKey: .images)
        self.information = try? container.decode([Information].self, forKey: .information)
        self.attributes = try? container.decode([ProductAttribute].self, forKey: .attributes)
    }
}



// MARK: - ProductAttribute
class ProductAttribute: Codable {
    var id: Int?
    var code, title, value: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case code
        case title
        case value
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.code = try? container.decode(String.self, forKey: .code)
        self.title = try? container.decode(String.self, forKey: .title)
        self.value = try? container.decode(String.self, forKey: .value)
    }
}


// MARK: - Information
class Information: Codable {
    var name, content: String?

    enum CodingKeys: String, CodingKey {
        case name
        case content
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try? container.decode(String.self, forKey: .name)
        self.content = try? container.decode(String.self, forKey: .content)
    }
}
