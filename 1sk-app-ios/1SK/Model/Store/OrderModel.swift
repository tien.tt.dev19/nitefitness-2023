//
//  OrderModel.swift
//  1SK
//
//  Created by Thaad on 21/03/2022.
//

import Foundation

class OrderModel: Codable {
    var id: Int?
    var code: String?
    var shippingOption: String?
    var shippingMethod: String?
    var amount, taxAmount, shippingAmount: Int?
    var couponCode: String?
    var discountAmount, subTotal: Int?
    var isConfirmed: Bool?
    var discountDescription: String?
    var isFinished: Bool?
    var paymentId: String?
    var status: String?
    var createdAt: Int?
    var items: [ItemOrder]?
    var address: DeliveryModel?
    var shipment: [ShipmentOrder]?
    var payment: [PaymentOrder]?
    var statusOrder: OrderStatus?
    
    enum CodingKeys: String, CodingKey {
        case id, code
        case shippingOption
        case shippingMethod
        case amount
        case taxAmount
        case shippingAmount
        case couponCode
        case discountAmount
        case subTotal
        case isConfirmed
        case discountDescription
        case isFinished
        case paymentId
        case status
        case createdAt
        case items, address
        case shipment, payment
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.code = try? container.decode(String.self, forKey: .code)
        self.shippingOption = try? container.decode(String.self, forKey: .shippingOption)
        self.shippingMethod = try? container.decode(String.self, forKey: .shippingMethod)
        self.amount = try? container.decode(Int.self, forKey: .amount)
        self.taxAmount = try? container.decode(Int.self, forKey: .taxAmount)
        self.shippingAmount = try? container.decode(Int.self, forKey: .shippingAmount)
        self.couponCode = try? container.decode(String.self, forKey: .couponCode)
        self.discountAmount = try? container.decode(Int.self, forKey: .discountAmount)
        self.subTotal = try? container.decode(Int.self, forKey: .subTotal)
        self.isConfirmed = try? container.decode(Bool.self, forKey: .isConfirmed)
        self.discountDescription = try? container.decode(String.self, forKey: .discountDescription)
        self.isFinished = try? container.decode(Bool.self, forKey: .isFinished)
        self.paymentId = try? container.decode(String.self, forKey: .paymentId)
        self.status = try? container.decode(String.self, forKey: .status)
        self.createdAt = try? container.decode(Int.self, forKey: .createdAt)
        self.items = try? container.decode([ItemOrder].self, forKey: .items)
        self.address = try? container.decode(DeliveryModel.self, forKey: .address)
        self.shipment = try? container.decode([ShipmentOrder].self, forKey: .shipment)
        self.payment = try? container.decode([PaymentOrder].self, forKey: .payment)
        
        if (self.items?.first(where: {$0.isReview == true})) != nil {
            self.items?.forEach({ item in
                item.isReview = true
            })
        }
        
        switch self.status {
        case OrderStatus.pending.rawValue:
            self.statusOrder = .pending
            
        case OrderStatus.processing.rawValue:
            self.statusOrder = .processing
            
        case OrderStatus.completed.rawValue:
            self.statusOrder = .completed
            
        case OrderStatus.canceled.rawValue:
            self.statusOrder = .canceled
            
        default:
            break
        }
    }
}

// MARK: ItemOrder
class ItemOrder: Codable {
    var id, productId: Int?
    var productName: String?
    var productImage: String?
    var quantity, price, taxAmount: Int?
    var isReview: Bool?
    var product: ProductOrderModel?
    var review: ReviewsProduct?
    
    var rateType: RateType?
    var rateComments: [RateComment]?
    var comment: String?
    
    var imageReviews: [UIImage]?
    var heightForCell: CGFloat?
    
    enum CodingKeys: String, CodingKey {
        case id
        case productId
        case productName
        case productImage
        case quantity, price
        case taxAmount
        case product
        case isReview
        case review
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.productId = try? container.decode(Int.self, forKey: .productId)
        self.productName = try? container.decode(String.self, forKey: .productName)
        self.productImage = try? container.decode(String.self, forKey: .productImage)
        self.quantity = try? container.decode(Int.self, forKey: .quantity)
        self.price = try? container.decode(Int.self, forKey: .price)
        self.taxAmount = try? container.decode(Int.self, forKey: .taxAmount)
        self.isReview = try? container.decode(Bool.self, forKey: .isReview)
        self.product = try? container.decode(ProductOrderModel.self, forKey: .product)
        self.review = try? container.decode(ReviewsProduct.self, forKey: .review)
        
        self.imageReviews = []
        if let image = R.image.img_bg_line_dashed() {
            self.imageReviews?.append(image)
        }
        self.heightForCell = 0
        
        switch self.review?.star {
        case 1:
            self.rateType = .one
        case 2:
            self.rateType = .two
        case 3:
            self.rateType = .three
        case 4:
            self.rateType = .four
        case 5:
            self.rateType = .five
        default:
            self.rateType = .five
        }
        
        self.rateComments = DataConfig.shared.getRateComments(type: self.rateType)
    }
}

// MARK: ProductOrder
class ProductOrderModel: Codable {
    var id: Int?
    var originalProduct: Int?
    var name, sku: String?
    var image: String?
    var description: String?
    var quantity: Int?
    var allowCheckoutWhenOutOfStock: Bool?
    var stockStatus: String?
    var price, priceWithTaxes, originalPrice, frontSalePrice: Int?
    var frontSalePriceWithTaxes: Int?
    var isSalePrice: Bool?
    var salePercentage, reviewsCount, reviewsAvg: Int?
    var images: [String]?
    var information: [ProductInfoOrder]?
    var reviews: [ReviewsProduct]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, sku, image
        case description
        case quantity
        case allowCheckoutWhenOutOfStock
        case stockStatus
        case price
        case priceWithTaxes
        case originalPrice
        case frontSalePrice
        case frontSalePriceWithTaxes
        case isSalePrice
        case salePercentage
        case reviewsCount
        case reviewsAvg
        case images, information
        case reviews
        case originalProduct
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.originalProduct = try? container.decode(Int.self, forKey: .originalProduct)
        self.name = try? container.decode(String.self, forKey: .name)
        self.sku = try? container.decode(String.self, forKey: .sku)
        self.image = try? container.decode(String.self, forKey: .image)
        self.description = try? container.decode(String.self, forKey: .description)
        self.quantity = try? container.decode(Int.self, forKey: .quantity)
        self.allowCheckoutWhenOutOfStock = try? container.decode(Bool.self, forKey: .allowCheckoutWhenOutOfStock)
        self.stockStatus = try? container.decode(String.self, forKey: .stockStatus)
        self.price = try? container.decode(Int.self, forKey: .price)
        self.priceWithTaxes = try? container.decode(Int.self, forKey: .priceWithTaxes)
        self.originalPrice = try? container.decode(Int.self, forKey: .originalPrice)
        self.frontSalePrice = try? container.decode(Int.self, forKey: .frontSalePrice)
        self.frontSalePriceWithTaxes = try? container.decode(Int.self, forKey: .frontSalePriceWithTaxes)
        self.isSalePrice = try? container.decode(Bool.self, forKey: .isSalePrice)
        self.salePercentage = try? container.decode(Int.self, forKey: .salePercentage)
        self.reviewsCount = try? container.decode(Int.self, forKey: .reviewsCount)
        self.reviewsAvg = try? container.decode(Int.self, forKey: .reviewsAvg)
        self.images = try? container.decode([String].self, forKey: .images)
        self.information = try? container.decode([ProductInfoOrder].self, forKey: .information)
        self.reviews = try? container.decode([ReviewsProduct].self, forKey: .reviews)
    }
}

// MARK: ProductInfoOrder
class ProductInfoOrder: Codable {
    var name, content: String?
    
    enum CodingKeys: String, CodingKey {
        case name, content
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try? container.decode(String.self, forKey: .name)
        self.content = try? container.decode(String.self, forKey: .content)
    }
}

// MARK: PaymentOrder
class PaymentOrder: Codable {
    var id: Int?
    var currency, chargeId, paymentChannel: String?
    var amount: Int?
    var paymentType: String?
    var refundedAmount, refundNote, description: String?
    var status: String?
    
    enum CodingKeys: String, CodingKey {
        case id, currency
        case chargeId
        case paymentChannel
        case amount
        case paymentType
        case refundedAmount
        case refundNote
        case description
        case status
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.currency = try? container.decode(String.self, forKey: .currency)
        self.chargeId = try? container.decode(String.self, forKey: .chargeId)
        self.paymentChannel = try? container.decode(String.self, forKey: .paymentChannel)
        self.amount = try? container.decode(Int.self, forKey: .amount)
        self.paymentType = try? container.decode(String.self, forKey: .paymentType)
        self.refundedAmount = try? container.decode(String.self, forKey: .refundedAmount)
        self.refundNote = try? container.decode(String.self, forKey: .refundNote)
        self.description = try? container.decode(String.self, forKey: .description)
        self.status = try? container.decode(String.self, forKey: .status)
    }
}

// MARK: ShipmentOrder
class ShipmentOrder: Codable {
    var id, weight, price, codAmount: Int?
    var codStatus, shippingCompanyName, trackingId, trackingLink: String?
    var crossCheckingStatus, note, estimateDateShipped, dateShipped: String?
    var status: String?
    
    enum CodingKeys: String, CodingKey {
        case id, weight, price
        case codAmount
        case codStatus
        case shippingCompanyName
        case trackingId
        case trackingLink
        case crossCheckingStatus
        case note
        case estimateDateShipped
        case dateShipped
        case status
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.weight = try? container.decode(Int.self, forKey: .weight)
        self.price = try? container.decode(Int.self, forKey: .price)
        self.codAmount = try? container.decode(Int.self, forKey: .codAmount)
        self.codStatus = try? container.decode(String.self, forKey: .codStatus)
        self.shippingCompanyName = try? container.decode(String.self, forKey: .shippingCompanyName)
        self.trackingId = try? container.decode(String.self, forKey: .trackingId)
        self.trackingLink = try? container.decode(String.self, forKey: .trackingLink)
        self.crossCheckingStatus = try? container.decode(String.self, forKey: .crossCheckingStatus)
        self.note = try? container.decode(String.self, forKey: .note)
        self.estimateDateShipped = try? container.decode(String.self, forKey: .estimateDateShipped)
        self.dateShipped = try? container.decode(String.self, forKey: .dateShipped)
        self.status = try? container.decode(String.self, forKey: .status)
    }
}

// MARK: ReviewsProduct
class ReviewsProduct: Codable {
    var id, star: Int?
    var subject, comment: String?
    var createdAt: Int?
    var images: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id, star, subject, comment
        case createdAt
        case images
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.star = try? container.decode(Int.self, forKey: .star)
        self.subject = try? container.decode(String.self, forKey: .subject)
        self.comment = try? container.decode(String.self, forKey: .comment)
        self.createdAt = try? container.decode(Int.self, forKey: .createdAt)
        self.images = try? container.decode([String].self, forKey: .images)
    }
}
