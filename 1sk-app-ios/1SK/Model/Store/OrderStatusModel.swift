//
//  OrderStatusModel.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//

import Foundation

class OrderStatusModel: Codable {
    var label, value: String?
    var ordersCount: Int?
    
    var status: OrderStatus?
    var isSelected: Bool?
    
    enum CodingKeys: String, CodingKey {
        case label, value
        case ordersCount
    }
    
    init(status: OrderStatus, isSelected: Bool) {
        self.status = status
        self.isSelected = isSelected
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.label = try? container.decode(String.self, forKey: .label)
        self.value = try? container.decode(String.self, forKey: .value)
        self.ordersCount = try? container.decode(Int.self, forKey: .ordersCount)
    }
}
