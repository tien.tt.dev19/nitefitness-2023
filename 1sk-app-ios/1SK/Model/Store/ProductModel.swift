//
//  ProductModel.swift
//  1SK
//
//  Created by Thaad on 14/03/2022.
//

import Foundation

class ProductModel: Codable {
    var id: Int?
    var originalProduct: Int?
    var name: String?
    var sku: String?
    var image: String?
    var description: String?
    var quantity: Int?
    var allowCheckoutWhenOutOfStock: Bool?
    var stockStatus: String?
    var price, priceWithTaxes, originalPrice, frontSalePrice: Int?
    var frontSalePriceWithTaxes: Int?
    var isSalePrice: Bool?
    var salePercentage, reviewsCount: Int?
    var reviewsAvg: Double?
    var content: String?
    var images: [String]?
    var sellStatus: String?
    var information: [InformationProduct]?
    var brand: BrandProduct?
    var unavailableAttributeId: [Int]?
    var imageChooseSize: String?
    var categories: [CategoriesProduct]?
    var attributes: [ProductAttribute]?
    var fullAttributes: [FullAttribute]?
    var flashSale: [FlashSaleTime]?
    
    enum CodingKeys: String, CodingKey {
        case id, originalProduct, name, sku, image
        case description
        case quantity
        case allowCheckoutWhenOutOfStock
        case stockStatus
        case price
        case priceWithTaxes
        case originalPrice
        case frontSalePrice
        case frontSalePriceWithTaxes
        case isSalePrice
        case salePercentage
        case reviewsCount
        case reviewsAvg
        case images
        case information, brand, categories
        case content
        case attributes
        case fullAttributes
        case sellStatus
        case imageChooseSize
        case flashSale
        case unavailableAttributeId
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.originalProduct = try? container.decode(Int.self, forKey: .originalProduct)
        self.name = try? container.decode(String.self, forKey: .name)
        self.sku = try? container.decode(String.self, forKey: .sku)
        self.image = try? container.decode(String.self, forKey: .image)
        self.description = try? container.decode(String.self, forKey: .description)
        self.quantity = try? container.decode(Int.self, forKey: .quantity)
        self.allowCheckoutWhenOutOfStock = try? container.decode(Bool.self, forKey: .allowCheckoutWhenOutOfStock)
        self.stockStatus = try? container.decode(String.self, forKey: .stockStatus)
        self.price = try? container.decode(Int.self, forKey: .price)
        self.priceWithTaxes = try? container.decode(Int.self, forKey: .priceWithTaxes)
        self.originalPrice = try? container.decode(Int.self, forKey: .originalPrice)
        self.frontSalePrice = try? container.decode(Int.self, forKey: .frontSalePrice)
        self.frontSalePriceWithTaxes = try? container.decode(Int.self, forKey: .frontSalePriceWithTaxes)
        self.isSalePrice = try? container.decode(Bool.self, forKey: .isSalePrice)
        self.salePercentage = try? container.decode(Int.self, forKey: .salePercentage)
        self.reviewsCount = try? container.decode(Int.self, forKey: .reviewsCount)
        self.reviewsAvg = try? container.decode(Double.self, forKey: .reviewsAvg)
        self.content = try? container.decode(String.self, forKey: .content)
        self.images = try? container.decode([String].self, forKey: .images)
        self.information = try? container.decode([InformationProduct].self, forKey: .information)
        self.brand = try? container.decode(BrandProduct.self, forKey: .brand)
        self.categories = try? container.decode([CategoriesProduct].self, forKey: .categories)
        self.attributes = try? container.decode([ProductAttribute].self, forKey: .attributes)
        self.fullAttributes = try? container.decode([FullAttribute].self, forKey: .fullAttributes)
        self.sellStatus = try? container.decode(String.self, forKey: .sellStatus)
        self.imageChooseSize = try? container.decode(String.self, forKey: .imageChooseSize)
        self.flashSale = try? container.decode([FlashSaleTime].self, forKey: .flashSale)
        self.unavailableAttributeId = try? container.decode([Int].self, forKey: .unavailableAttributeId)
    }
}

// MARK: InformationProduct
class InformationProduct: Codable {
    var name, content: String?

    enum CodingKeys: String, CodingKey {
        case name, content
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
        self.content = try? container.decodeIfPresent(String.self, forKey: .content)
    }
}

// MARK: CategoriesProduct
class CategoriesProduct: Codable {
    var id: Int?
    var logo: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case id, logo, name
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.logo = try? container.decodeIfPresent(String.self, forKey: .logo)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
    }
}

// MARK: BrandProduct
class BrandProduct: Codable {
    var id: Int?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case id, name
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
    }
}

//MARK: - FullAttribute
class FullAttribute: Codable {
    var id: Int?
    var title, displayLayout: String?
    var productAttributes: [Attribute]?
    var heightRow: CGFloat?
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case displayLayout
        case productAttributes
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.title = try? container.decodeIfPresent(String.self, forKey: .title)
        self.displayLayout = try? container.decodeIfPresent(String.self, forKey: .displayLayout)
        self.productAttributes = try? container.decodeIfPresent([Attribute].self, forKey: .productAttributes)
    }
}

//MARK: - Attribute
class Attribute: Codable {
    var id: Int?
    var title: String?
    var fromPrice, toPrice: Int?
    var isSelected = false
    
    enum CodingKeys: String, CodingKey {
        case id, title
        case fromPrice
        case toPrice
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.title = try? container.decodeIfPresent(String.self, forKey: .title)
        self.fromPrice = try? container.decodeIfPresent(Int.self, forKey: .fromPrice)
        self.toPrice = try? container.decodeIfPresent(Int.self, forKey: .toPrice)
    }
}

//MARK: - FlashSaleTime
class FlashSaleTime: Codable {
    var id: Int?
    var name: String?
    var startDate: String?
    var startTime: String?
    var endDate: String?
    var endTime: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case startDate
        case startTime
        case endDate
        case endTime
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
        self.startDate = try? container.decodeIfPresent(String.self, forKey: .startDate)
        self.startTime = try? container.decodeIfPresent(String.self, forKey: .startTime)
        self.endDate = try? container.decodeIfPresent(String.self, forKey: .endDate)
        self.endTime = try? container.decodeIfPresent(String.self, forKey: .endTime)
    }
}
