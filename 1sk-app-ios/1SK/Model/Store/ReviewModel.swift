//
//  ReviewModel.swift
//  1SK
//
//  Created by Thaad on 15/03/2022.
//

import Foundation

class ReviewModel: Codable {
    var id, star: Int?
    var subject, comment: String?
    var createdAt: Int?
    var customer: CustomerReview?
    
    enum CodingKeys: String, CodingKey {
        case id, star, subject, comment
        case createdAt, customer
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.star = try? container.decode(Int.self, forKey: .star)
        self.subject = try? container.decode(String.self, forKey: .subject)
        self.comment = try? container.decode(String.self, forKey: .comment)
        self.createdAt = try? container.decode(Int.self, forKey: .createdAt)
        self.customer = try? container.decode(CustomerReview.self, forKey: .customer)
    }
}

class CustomerReview: Codable {
    var id: Int?
    var name: String?
    var avatar: String?
    var email, phone: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, avatar, email, phone
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.avatar = try? container.decode(String.self, forKey: .avatar)
        self.email = try? container.decode(String.self, forKey: .email)
        self.phone = try? container.decode(String.self, forKey: .phone)
    }
}
