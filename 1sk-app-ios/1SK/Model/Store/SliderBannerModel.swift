//
//  SliderBannerModel.swift
//  1SK
//
//  Created by Thaad on 14/03/2022.
//

import Foundation

class SliderBannerModel: Codable {
    var id: Int?
    var name, key: String?
    var description: String?
    var items: [ItemBanner]?
    
    enum CodingKeys: String, CodingKey {
        case id, name, key
        case description
        case items
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
        self.key = try? container.decodeIfPresent(String.self, forKey: .key)
        self.description = try? container.decodeIfPresent(String.self, forKey: .description)
        self.items = try? container.decodeIfPresent([ItemBanner].self, forKey: .items)
    }
    
}


class ItemBanner: Codable {
    var id: Int?
    var title: String?
    var image: String?
    var type: String?
    var link, description: String?
    var productId: Int?
    var typeBanner: TypeBannerStore?

    enum CodingKeys: String, CodingKey {
        case id, title, image, type, link
        case productId
        case description
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.title = try container.decodeIfPresent(String.self, forKey: .title)
        self.image = try container.decodeIfPresent(String.self, forKey: .image)
        self.type = try container.decodeIfPresent(String.self, forKey: .type)
        self.link = try container.decodeIfPresent(String.self, forKey: .link)
        self.productId = try container.decodeIfPresent(Int.self, forKey: .productId)
        self.description = try container.decodeIfPresent(String.self, forKey: .description)
        
        switch type {
        case TypeBannerStore.link.rawValue:
            self.typeBanner = .link
            
        case TypeBannerStore.product.rawValue:
            self.typeBanner = .product
            
        default:
            break
        }
    }
}
