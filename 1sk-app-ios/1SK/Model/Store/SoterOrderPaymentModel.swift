//
//  SoterOrderPaymentModel.swift
//
//  Created by TrungDN on 21/03/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

class SoterOrderPaymentModel: Codable {
    var status: String?
    var currency: String?
    var id: Int?
    var chargeId: String?
    var paymentChannel: String?
    var amount: Float?
    var paymentType: String?
    
    enum CodingKeys: String, CodingKey {
        case status
        case currency
        case id
        case chargeId
        case paymentChannel
        case amount
        case paymentType
    }
    
    init (status: String?, currency: String?, id: Int?, chargeId: String?, paymentChannel: String?, amount: Float?, paymentType: String?) {
        self.status = status
        self.currency = currency
        self.id = id
        self.chargeId = chargeId
        self.paymentChannel = paymentChannel
        self.amount = amount
        self.paymentType = paymentType
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.status = try container.decodeIfPresent(String.self, forKey: .status)
        self.currency = try container.decodeIfPresent(String.self, forKey: .currency)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.chargeId = try container.decodeIfPresent(String.self, forKey: .chargeId)
        self.paymentChannel = try container.decodeIfPresent(String.self, forKey: .paymentChannel)
        self.amount = try container.decodeIfPresent(Float.self, forKey: .amount)
        self.paymentType = try container.decodeIfPresent(String.self, forKey: .paymentType)
    }
}
