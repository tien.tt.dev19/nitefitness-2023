//
//  StoreOrderAddressModel.swift
//
//  Created by TrungDN on 21/03/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

class StoreOrderAddressModel: Codable {
    var name: String?
    var address: String?
    var id: Int?
    var phone: String?
    var email: String?
    var state: StoreOrderSubAddressModel?
    var city: StoreOrderSubAddressModel?
    var district: StoreOrderSubAddressModel?
    
    enum CodingKeys: String, CodingKey {
        case name
        case address
        case id
        case phone
        case email
        case state
        case city
        case district
    }
    
    init (name: String?, address: String?, id: Int?, phone: String?) {
        self.name = name
        self.address = address
        self.id = id
        self.phone = phone
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.address = try container.decodeIfPresent(String.self, forKey: .address)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.phone = try container.decodeIfPresent(String.self, forKey: .phone)
        self.email = try container.decodeIfPresent(String.self, forKey: .email)
        self.district = try container.decodeIfPresent(StoreOrderSubAddressModel.self, forKey: .district)
        self.city = try container.decodeIfPresent(StoreOrderSubAddressModel.self, forKey: .city)
        self.state = try container.decodeIfPresent(StoreOrderSubAddressModel.self, forKey: .state)
    }
    
}
