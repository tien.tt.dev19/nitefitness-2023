//
//  StoreOrderInformationModel.swift
//
//  Created by TrungDN on 21/03/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoreOrderInformationModel: Codable {
    var content: String?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case content
        case name
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.content = try? container.decodeIfPresent(String.self, forKey: .content)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
    }
    
}
