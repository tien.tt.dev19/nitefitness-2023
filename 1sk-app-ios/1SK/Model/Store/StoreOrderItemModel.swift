//
//  StoreOrderItemModel.swift
//
//  Created by TrungDN on 21/03/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

class StoreOrderItemModel: Codable {
    var product: StoreOrderProductModel?
    var taxAmount: Int?
    var productName: String?
    var id: Int?
    var productId: Int?
    var productImage: String?
    var quantity: Int?
    var price: Int?

    enum CodingKeys: String, CodingKey {
        case taxAmount
        case productName
        case id
        case productId
        case productImage
        case quantity
        case price
        case product
    }

    init (taxAmount: Int?, productName: String?, id: Int?, productId: Int?, productImage: String?, quantity: Int?, price: Int?) {
        self.taxAmount = taxAmount
        self.productName = productName
        self.id = id
        self.productId = productId
        self.productImage = productImage
        self.quantity = quantity
        self.price = price
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.taxAmount = try container.decodeIfPresent(Int.self, forKey: .taxAmount)
        self.productName = try container.decodeIfPresent(String.self, forKey: .productName)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.productId = try container.decodeIfPresent(Int.self, forKey: .productId)
        self.productImage = try container.decodeIfPresent(String.self, forKey: .productImage)
        self.quantity = try container.decodeIfPresent(Int.self, forKey: .quantity)
        self.price = try container.decodeIfPresent(Int.self, forKey: .price)
        self.product = try container.decodeIfPresent(StoreOrderProductModel.self, forKey: .product)
    }

}
