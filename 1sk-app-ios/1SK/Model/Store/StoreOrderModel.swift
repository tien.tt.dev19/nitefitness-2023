//
//  StoreOrderModel.swift
//  1SK
//
//  Created by TrungDN on 21/03/2022.
//

import Foundation

class StoreOrderModel: Codable {
    
    enum CodingKeys: String, CodingKey {
        case shippingMethod
        case status
        case isConfirmed
        case isFinished
        case payment
        case address
        case subTotal
        case items
        case taxAmount
        case amount
        case id
        case shippingAmount
        case code
        case shipment
        case discountAmount
        case shippingOption
        case createdAt
        case paymentId
    }
    
    var shippingMethod: String?
    var status: String?
    var isConfirmed: Bool?
    var isFinished: Bool?
    var payment: SoterOrderPaymentModel?
    var address: StoreOrderAddressModel?
    var subTotal: Int?
    var items: [StoreOrderItemModel]?
    var taxAmount: Float?
    var amount: Int?
    var id: Int?
    var shippingAmount: Int?
    var code: String?
    var shipment: StoreOrderShipmentModel?
    var discountAmount: Int?
    var shippingOption: String?
    var createdAt: Int?
    var paymentId: Int?
    
    init (shippingMethod: String?, status: String?, isConfirmed: Bool?, isFinished: Bool?, payment: SoterOrderPaymentModel?, address: StoreOrderAddressModel?, subTotal: Int?, items: [StoreOrderItemModel]?, taxAmount: Float?, amount: Int?, id: Int?, shippingAmount: Int?, code: String?, shipment: StoreOrderShipmentModel?, discountAmount: Int?, shippingOption: String?, createdAt: Int?, paymentId: Int?) {
        self.shippingMethod = shippingMethod
        self.status = status
        self.isConfirmed = isConfirmed
        self.isFinished = isFinished
        self.payment = payment
        self.address = address
        self.subTotal = subTotal
        self.items = items
        self.taxAmount = taxAmount
        self.amount = amount
        self.id = id
        self.shippingAmount = shippingAmount
        self.code = code
        self.shipment = shipment
        self.discountAmount = discountAmount
        self.shippingOption = shippingOption
        self.createdAt = createdAt
        self.paymentId = paymentId
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        shippingMethod = try container.decodeIfPresent(String.self, forKey: .shippingMethod)
        status = try container.decodeIfPresent(String.self, forKey: .status)
        isConfirmed = try container.decodeIfPresent(Bool.self, forKey: .isConfirmed)
        isFinished = try container.decodeIfPresent(Bool.self, forKey: .isFinished)
        payment = try container.decodeIfPresent(SoterOrderPaymentModel.self, forKey: .payment)
        address = try container.decodeIfPresent(StoreOrderAddressModel.self, forKey: .address)
        subTotal = try container.decodeIfPresent(Int.self, forKey: .subTotal)
        items = try container.decodeIfPresent([StoreOrderItemModel].self, forKey: .items)
        taxAmount = try container.decodeIfPresent(Float.self, forKey: .taxAmount)
        amount = try container.decodeIfPresent(Int.self, forKey: .amount)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        shippingAmount = try container.decodeIfPresent(Int.self, forKey: .shippingAmount)
        code = try container.decodeIfPresent(String.self, forKey: .code)
        shipment = try container.decodeIfPresent(StoreOrderShipmentModel.self, forKey: .shipment)
        discountAmount = try container.decodeIfPresent(Int.self, forKey: .discountAmount)
        shippingOption = try container.decodeIfPresent(String.self, forKey: .shippingOption)
        createdAt = try container.decodeIfPresent(Int.self, forKey: .createdAt)
        paymentId = try container.decodeIfPresent(Int.self, forKey: .paymentId)
    }
}
