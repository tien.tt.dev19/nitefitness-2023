//
//  StoreOrderProductModel.swift
//
//  Created by TrungDN on 21/03/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

struct StoreOrderProductModel: Codable {
    var id: Int?
    var name: String?
    var quantity: Int?
    var information: [StoreOrderInformationModel]?
    var sku: String?
    var salePercentage: Int?
    var stockStatus: String?
    var isSalePrice: Bool?
    var frontSalePriceWithTaxes: Int?
    var image: String?
    var images: [String]?
    var allowCheckoutWhenOutOfStock: Bool?
    var reviewsCount: Int?
    var descriptionValue: String?
    var originalPrice: Int?
    var reviewsAvg: Int?
    var frontSalePrice: Int?
    var price: Int?
    var priceWithTaxes: Int?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case quantity
        case information
        case sku
        case salePercentage
        case stockStatus
        case isSalePrice
        case frontSalePriceWithTaxes
        case image
        case images
        case allowCheckoutWhenOutOfStock
        case reviewsCount
        case descriptionValue
        case originalPrice
        case reviewsAvg
        case frontSalePrice
        case price
        case priceWithTaxes
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.quantity = try container.decodeIfPresent(Int.self, forKey: .quantity)
        self.information = try container.decodeIfPresent([StoreOrderInformationModel].self, forKey: .information)
        self.sku = try container.decodeIfPresent(String.self, forKey: .sku)
        self.salePercentage = try container.decodeIfPresent(Int.self, forKey: .salePercentage)
        self.stockStatus = try container.decodeIfPresent(String.self, forKey: .stockStatus)
        self.isSalePrice = try container.decodeIfPresent(Bool.self, forKey: .isSalePrice)
        self.frontSalePriceWithTaxes = try container.decodeIfPresent(Int.self, forKey: .frontSalePriceWithTaxes)
        self.image = try container.decodeIfPresent(String.self, forKey: .image)
        self.images = try container.decodeIfPresent([String].self, forKey: .images)
        self.allowCheckoutWhenOutOfStock = try container.decodeIfPresent(Bool.self, forKey: .allowCheckoutWhenOutOfStock)
        self.reviewsCount = try container.decodeIfPresent(Int.self, forKey: .reviewsCount)
        self.descriptionValue = try container.decodeIfPresent(String.self, forKey: .descriptionValue)
        self.originalPrice = try container.decodeIfPresent(Int.self, forKey: .originalPrice)
        self.reviewsAvg = try container.decodeIfPresent(Int.self, forKey: .reviewsAvg)
        self.frontSalePrice = try container.decodeIfPresent(Int.self, forKey: .frontSalePrice)
        self.price = try container.decodeIfPresent(Int.self, forKey: .price)
        self.priceWithTaxes = try container.decodeIfPresent(Int.self, forKey: .priceWithTaxes)
    }
    
}
