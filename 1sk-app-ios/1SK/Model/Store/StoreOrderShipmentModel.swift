//
//  StoreOrderShipmentModel.swift
//
//  Created by TrungDN on 21/03/2022
//  Copyright (c) . All rights reserved.
//

import Foundation

class StoreOrderShipmentModel: Codable {
    var estimateDateShipped: String?
    var note: String?
    var codAmount: Int?
    var trackingId: String?
    var shippingCompanyName: String?
    var weight: Int?
    var trackingLink: String?
    var price: String?
    var codStatus: String?
    var id: Int?
    var status: String?
    var crossCheckingStatus: String?
    
    enum CodingKeys: String, CodingKey {
        case estimateDateShipped
        case note
        case codAmount
        case trackingId
        case shippingCompanyName
        case weight
        case trackingLink
        case price
        case codStatus
        case id
        case status
        case crossCheckingStatus
    }
    
    init (estimateDateShipped: String?, note: String?, codAmount: Int?, trackingId: String?, shippingCompanyName: String?, weight: Int?, trackingLink: String?, price: String?, codStatus: String?, id: Int?, status: String?, crossCheckingStatus: String?) {
        self.estimateDateShipped = estimateDateShipped
        self.note = note
        self.codAmount = codAmount
        self.trackingId = trackingId
        self.shippingCompanyName = shippingCompanyName
        self.weight = weight
        self.trackingLink = trackingLink
        self.price = price
        self.codStatus = codStatus
        self.id = id
        self.status = status
        self.crossCheckingStatus = crossCheckingStatus
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.estimateDateShipped = try container.decodeIfPresent(String.self, forKey: .estimateDateShipped)
        self.note = try container.decodeIfPresent(String.self, forKey: .note)
        self.codAmount = try container.decodeIfPresent(Int.self, forKey: .codAmount)
        self.trackingId = try container.decodeIfPresent(String.self, forKey: .trackingId)
        self.shippingCompanyName = try container.decodeIfPresent(String.self, forKey: .shippingCompanyName)
        self.weight = try container.decodeIfPresent(Int.self, forKey: .weight)
        self.trackingLink = try container.decodeIfPresent(String.self, forKey: .trackingLink)
        self.price = try container.decodeIfPresent(String.self, forKey: .price)
        self.codStatus = try container.decodeIfPresent(String.self, forKey: .codStatus)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id)
        self.status = try container.decodeIfPresent(String.self, forKey: .status)
        self.crossCheckingStatus = try container.decodeIfPresent(String.self, forKey: .crossCheckingStatus)
    }
    
}
