//
//  StoreOrderSubAddressModel.swift
//  1SK
//
//  Created by TrungDN on 22/03/2022.
//

import Foundation

class StoreOrderSubAddressModel: Codable {
    var id: Int?
    var name: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
    }
    
    init (id: Int?, name: String?) {
        self.id = id
        self.name = name
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try? container.decodeIfPresent(Int.self, forKey: .id)
        self.name = try? container.decodeIfPresent(String.self, forKey: .name)
    }
    
}
