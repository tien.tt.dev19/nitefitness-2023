//
//  StrategiesModel.swift
//  1SK
//
//  Created by Thaad on 11/10/2022.
//

import Foundation

class StrategiesModel: Codable {
    var title: String?
    var slug: String?
    var description: String?
    var image: String?
    var backgroundColor: String?

    enum CodingKeys: String, CodingKey {
        case title, slug
        case description
        case image
        case backgroundColor
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.title = try? container.decode(String.self, forKey: .title)
        self.slug = try? container.decode(String.self, forKey: .slug)
        self.description = try? container.decode(String.self, forKey: .description)
        self.image = try? container.decode(String.self, forKey: .image)
        self.backgroundColor = try? container.decode(String.self, forKey: .backgroundColor)
    }
}
