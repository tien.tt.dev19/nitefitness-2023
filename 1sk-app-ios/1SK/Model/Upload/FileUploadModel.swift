//
//  FileUploadModel.swift
//  1SK
//
//  Created by tuyenvx on 13/01/2021.
//

import Foundation

class FileUploadModel: Codable {
    enum CodingKeys: String, CodingKey {
        case name
        case path
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        path = try container.decodeIfPresent(String.self, forKey: .path) ?? ""
    }

    var name: String = ""
    var path: String = ""
    var fileType: String = ""
    var size: Int = 0
}
