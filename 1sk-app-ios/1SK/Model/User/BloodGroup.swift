//
//  BloodGroup.swift
//  1SK
//
//  Created by tuyenvx on 29/01/2021.
//

import Foundation

enum BloodGroup: String, Codable, CaseIterable {
    case OPositive = "O+"
    case ONegative = "O-"
    case APositive = "A+"
    case ANegative = "A-"
    case BPositive = "B+"
    case BNegative = "B-"
    case ABPositive = "AB+"
    case ABNegative = "AB-"
    case RhPositive = "Rh+"
    case RhNegative = "Rh-"

    var name: String {
        switch self {
        case .OPositive:
            return "O+"
        case .ONegative:
            return "O-"
        case .APositive:
            return "A+"
        case .ANegative:
            return "A-"
        case .BPositive:
            return "B+"
        case .BNegative:
            return "B-"
        case .ABPositive:
            return "AB+"
        case .ABNegative:
            return "AB-"
        case .RhPositive:
            return "Rh+"
        case .RhNegative:
            return "Rh-"
        }
    }
}
