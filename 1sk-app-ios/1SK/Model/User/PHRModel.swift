//
//  PHRModel.swift
//  1SK
//
//  Created by vuongbachthu on 10/10/21.
//

import Foundation

class PHRModel: Codable {
    var id: Int?
    var name, value, unit: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name, value, unit
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try? container.decode(Int.self, forKey: .id)
        self.name = try? container.decode(String.self, forKey: .name)
        self.value = try? container.decode(String.self, forKey: .value)
        self.unit = try? container.decode(String.self, forKey: .unit)
    }
    
    init() {
        //
    }
}
