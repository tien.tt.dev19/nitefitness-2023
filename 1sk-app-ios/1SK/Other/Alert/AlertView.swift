//
//  AlertView.swift
//  1SK
//
//  Created by tuyenvx on 26/02/2021.
//

import UIKit

protocol AlertViewDelegate: AnyObject {
    func cancelButtonDidTapped()
    func okButtonDidTapped()
}

class AlertView: UIView {
    private lazy var imageView = UIImageView()
    private lazy var contentLabel = UILabel()
    private lazy var cancelButton = UIButton()
    private lazy var okButton = UIButton()

    weak var delegate: AlertViewDelegate?

    private var type: AlertType = .cameraUsage

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupDefautls()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupDefautls()
    }

    convenience init(type: AlertType) {
        self.init(frame: .zero)
        self.type = type
        setupDefautls()
        updateByAlertType(type)
    }

    // MARK: - Setup
    private func setupDefautls() {
        setupConstraint()
        setupImageView()
        setupContentLabel()
        setupCancelButton()
        setupOKButton()
    }

    private func setupConstraint() {
        addSubview(imageView)
        imageView.contentMode = .scaleAspectFit
        imageView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            if type == .cancelOrder {
                make.height.equalTo(0)
            } else if type != .logout {
                make.width.height.equalTo(80)
            } else {
                make.width.equalTo(133)
                make.height.equalTo(106)
            }
            make.top.equalToSuperview().offset(type != .logout ? 22 : 8)
        }

        addSubview(contentLabel)
        contentLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(11)
            make.centerX.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
        }

        addSubview(cancelButton)
        cancelButton.snp.makeConstraints { (make) in
            make.top.equalTo(contentLabel.snp.bottom).offset(14)
            make.leading.equalToSuperview().offset(37)
            make.height.equalTo(36)
        }

        addSubview(okButton)
        okButton.snp.makeConstraints { (make) in
            make.top.bottom.width.equalTo(cancelButton)
            make.trailing.equalToSuperview().offset(-37)
            make.leading.equalTo(cancelButton.snp.trailing).offset(17)
            make.bottom.equalToSuperview().offset(-13).priority(250)
        }
    }

    private func setupImageView() {
        imageView.clipsToBounds = false
    }

    private func setupContentLabel() {
        contentLabel.numberOfLines = 0
        contentLabel.lineBreakMode = .byWordWrapping
        if self.type == .cancelOrder {
            contentLabel.font = R.font.robotoMedium(size: 16)
        } else {
            contentLabel.font = R.font.robotoRegular(size: 16)
        }
        contentLabel.textColor = R.color.darkText()
        contentLabel.textAlignment = .center
    }

    private func setupCancelButton() {
        cancelButton.backgroundColor = R.color.subTitle()
        cancelButton.setTitleColor(R.color.white(), for: .normal)
        cancelButton.cornerRadius = 18
        cancelButton.addTarget(self, action: #selector(onCancelButtonDidTapped), for: .touchUpInside)
    }

    private func setupOKButton() {
        okButton.backgroundColor = R.color.mainColor()
        okButton.setTitleColor(R.color.white(), for: .normal)
        okButton.cornerRadius = 18
        okButton.addTarget(self, action: #selector(onOkButtonDidTapped), for: .touchUpInside)
    }
    // MARK: - Action
    @objc func onCancelButtonDidTapped() {
        delegate?.cancelButtonDidTapped()
    }

    @objc func onOkButtonDidTapped() {
        delegate?.okButtonDidTapped()
    }

    func updateByAlertType(_ type: AlertType) {
        self.type = type
        imageView.image = type.image
        contentLabel.text = type.content
        cancelButton.setTitle(type.cancelButtonTitle, for: .normal)
        okButton.setTitle(type.okButtonTitle, for: .normal)
    }
}
