//
//  AlertViewController.swift
//  1SK
//
//  Created by tuyenvx on 26/02/2021.
//

import UIKit

protocol AlertViewControllerDelegate: AnyObject {
    func onAgreeAlertAction(_ type: AlertType)
    func onDismissAlertAction(_ type: AlertType)
}

class AlertViewController: BaseViewController {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var safeAreaViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!
    
    var type: AlertType = .cameraUsage
    var folderID: String = ""
    var fileID: String = ""
    weak var delegate: AlertViewControllerDelegate?

    var safeAreaBottom: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            return window.safeAreaInsets.bottom
        } else {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.bottom ?? 0
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.show()
    }
    
    @IBAction func backgroundDidTapped(_ sender: Any) {
        self.hide()
    }

    private func setupUI() {
        self.alertView.superview?.roundCorners(cornes: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 24)
        self.addAlertView()
        self.safeAreaViewHeightConstraint.constant = self.safeAreaBottom
        self.alertViewHeightConstraint.constant = self.type.bottomViewHeight
        self.bottomViewBottomConstraint.constant = -self.type.bottomViewHeight - self.safeAreaBottom
    }

    private func addAlertView() {
        let alertView = AlertView(type: type)
        alertView.delegate = self
        self.alertView.addSubview(alertView)
        alertView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }

    func show() {
        self.bottomViewBottomConstraint.constant = 0
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in

        })
    }

    func hide() {
        self.alertViewHeightConstraint.constant = -self.type.bottomViewHeight - self.safeAreaBottom
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: nil)
        })
    }

    private func openAppSetting() {
        guard let settingURL = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settingURL) else {
            return
        }
        UIApplication.shared.open(settingURL, options: [:], completionHandler: nil)
    }
}

// MARK: - AlertViewDelegate
extension AlertViewController: AlertViewDelegate {
    func cancelButtonDidTapped() {
        self.delegate?.onDismissAlertAction(self.type)
        self.hide()
    }

    func okButtonDidTapped() {
        switch self.type {
        case .cameraUsage:
            self.openAppSetting()
        case .photoUsage:
            self.openAppSetting()
        default:
            self.delegate?.onAgreeAlertAction(self.type)
        }
        
        if self.type != .updateApp {
            self.hide()
        }
    }
}
