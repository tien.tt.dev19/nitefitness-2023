//
//  PhotoKeyboardToolbar.swift
//  1SK
//
//  Created by tuyenvx on 03/06/2021.
//

import Foundation
import UIKit

protocol PhotoKeyboardToolbarDelegate: AnyObject {
    func onButtonPhotoDidTapped()
}

class PhotoKeyboardToolbar: UIView {
    private lazy var photoButton = createPhotoButton()
    weak var delegate: PhotoKeyboardToolbarDelegate?

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupDefaults()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: - Setup
    private func setupDefaults() {
        let topLine = UIView()
        topLine.backgroundColor = UIColor(hex: "E7ECF0")
        addSubview(topLine)
        topLine.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.height.equalTo(1)
        }

        addSubview(photoButton)
        photoButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview()
            make.leading.equalToSuperview().offset(16)
            make.width.equalTo(30)
        }
    }

    private func createPhotoButton() -> UIButton {
        let button = UIButton()
        button.setImage(R.image.ic_photo(), for: .normal)
        button.addTarget(self, action: #selector(buttonPhotoDidTapped), for: .touchUpInside)
        return button
    }
    // MARK: - Action
    @objc func buttonPhotoDidTapped() {
        delegate?.onButtonPhotoDidTapped()
    }
}
