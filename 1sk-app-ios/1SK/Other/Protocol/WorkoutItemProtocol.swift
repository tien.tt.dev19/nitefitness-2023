//
//  WorkoutItemProtocol.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//

import Foundation

protocol WorkoutItemProtocol {
    var id: Int {get set}
    var name: String {get set}
}
