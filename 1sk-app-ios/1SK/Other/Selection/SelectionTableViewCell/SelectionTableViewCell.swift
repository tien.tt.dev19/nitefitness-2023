//
//  SelectionTableViewCell.swift
//  SK365
//
//  Created by tuyenvx on 6/2/20.
//  Copyright © 2020 Elcom. All rights reserved.
//

import UIKit

class SelectionTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var checkImageViewLeadingConstant: NSLayoutConstraint!
    @IBOutlet weak var checkImageViewWidthConstant: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectedBackgroundView?.backgroundColor = R.color.background()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        contentView.backgroundColor = selected ? R.color.background() : R.color.white()
    }

    func config(title: String, type: SelectionType, isCheck: Bool = false) {
        let hasCheckImage = type == .profile || type == .videoResolution
        titleLabel.text = title
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        titleLabel.textColor = R.color.darkText()
        checkImageView.isHidden = !isCheck
        checkImageViewWidthConstant.constant = !hasCheckImage ? 0 : 12
        checkImageViewLeadingConstant.constant = !hasCheckImage ? 6 : 18
        titleLabel.textAlignment = !hasCheckImage ? .center : .left
    }
}
