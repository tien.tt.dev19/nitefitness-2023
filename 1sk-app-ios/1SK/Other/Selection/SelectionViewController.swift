//
//  SelectionViewController.swift
//  SK365
//
//  Created by tuyenvx on 6/2/20.
//  Copyright © 2020 Elcom. All rights reserved.
//

import UIKit

protocol SelectionViewControllerDelegate: AnyObject {
    func didSelectItem(of type: SelectionType, at index: Int)
    func didDismissView()
}

class SelectionViewController: BaseViewController {
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var selectionTableView: UITableView!
    @IBOutlet weak var cancleButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return [.portrait, .landscape]
    }

//    var profileList: ProfileListModel?
//    var model: ProfileModel?
//    var user: UserObject?
    var type: SelectionType = .relationship
    var selectedIndex: Int?
    var currentVideoResolution: VideoResolution = .auto

    private var contents: [String] {
        switch type {
//        case .profile:
//            return profileList?.profiles.map({return $0.name ?? ""}) ?? []
        case .bloodGroup:
            return BloodGroup.allCases.map { return $0.name }
//        case .relationship:
//            var relationships = Relationship.allCases.map { return $0.getName() }
//            if model?.relation.value != .yourSelf { // Can't change relation ship when it is your self
//                relationships.removeFirst()
//                return relationships
//            } else {
//                return [relationships.first ?? ""]
//            }
//        case .medicalHistoryType:
//            return MedicalHistoryType.allCases.map { return $0.name }
//        case .geneticDiseasesSource:
//            return GeneticDiseasesSource.allCases.map({ return $0.name })
        case .weightActivity:
            return ActivityIndex.allCases.map({ return $0.name })
        case .videoResolution:
            return VideoResolution.allCases.map({ return $0.name })
        default:
            return []
        }
    }

    weak var delegate: SelectionViewControllerDelegate?
    private let cellHeight: CGFloat = 42

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        setNavigationTitle()
        setupSelectionTableView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        show()
    }

    private func setupDefaults() {
        titleLabel.superview?.roundCorners(cornes: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 25)
        var safeAreaBottom: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            safeAreaBottom = window.safeAreaInsets.bottom
        } else {
            let window = UIApplication.shared.keyWindow
            safeAreaBottom = window?.safeAreaInsets.bottom ?? 0
        }

        tableViewBottomConstraint.constant = -213 - safeAreaBottom
        cancleButton.isHidden = type == .profile || type == .videoResolution
        confirmButton.isHidden = type == .profile || type == .videoResolution

        selectionTableView.isScrollEnabled = type != .videoResolution
    }

    private func setNavigationTitle() {
        switch type {
        case .profile:
            titleLabel.text = "Chọn hồ sơ sức khỏe"
        case .relationship:
            titleLabel.text = "Mối quan hệ"
        case .bloodGroup:
            titleLabel.text = "Nhóm máu"
        case .videoResolution:
            titleLabel.text = "Chọn chất lượng"
        default:
            return
        }
    }

    private func setupSelectionTableView() {
        selectionTableView.registerNib(ofType: SelectionTableViewCell.self)
        selectionTableView.dataSource = self
        selectionTableView.delegate = self
        selectionTableView.reloadData()
        selectionTableView.estimatedRowHeight = 56
    }

    @IBAction func buttonCloseDidTapped(_ sender: Any) {
        self.delegate?.didDismissView()
        hide()
    }

    @IBAction func buttonConfirmDidTapped(_ sender: Any) {
        if let `selectedIndex` = selectedIndex {
            delegate?.didSelectItem(of: type, at: selectedIndex)
            hide()
        }
    }

    @IBAction func backgroundDidTapped(_ sender: Any) {
        self.delegate?.didDismissView()
        hide()
    }

    func show() {
        tableViewBottomConstraint.constant = 0
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in

        })
    }

    func hide() {
        var safeAreaBottom: CGFloat = 0
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            safeAreaBottom = window.safeAreaInsets.bottom
        } else {
            let window = UIApplication.shared.keyWindow
            safeAreaBottom = window?.safeAreaInsets.bottom ?? 0
        }

        tableViewBottomConstraint.constant = -213 - safeAreaBottom
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
// MARK: - UITableViewDataSource
extension SelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contents.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectionCell = tableView.dequeueCell(ofType: SelectionTableViewCell.self, for: indexPath)
        var isCheck = false
//        if type == .profile && profileList?.profiles[indexPath.row].id == profileList?.selectedID {
//            isCheck = true
//        }
        if type == .videoResolution, VideoResolution.allCases[indexPath.row] == currentVideoResolution {
            isCheck = true
        }
        selectionCell.config(title: contents[indexPath.row], type: type, isCheck: isCheck)
        return selectionCell
    }
}
// MARK: - UITableView Delegate
extension SelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if type != .profile && type != .videoResolution {
            if type != .relationship {
                selectedIndex = indexPath.row
//            } else if model?.relation.value != .yourSelf {
//                selectedIndex = indexPath.row + 1
            } else {
                selectedIndex = indexPath.row
            }
        } else {
            tableView.deselectRow(at: indexPath, animated: true)
            delegate?.didSelectItem(of: type, at: indexPath.row)
            hide()
        }
    }
}
