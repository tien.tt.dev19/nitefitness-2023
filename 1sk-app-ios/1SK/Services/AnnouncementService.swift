//
//  AnnouncementService.swift
//  1SK
//
//  Created by Tiến Trần on 22/09/2022.
//

import Foundation

protocol AnnouncementServiceProtocol {
    func getAnnouncementList(page: Int, per_page: Int, completion: @escaping ((Result<BaseModel<[AnnouncementModels]>, APIError>) -> Void))
    func setAnnouncementStatus(markType: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setSingleAnnouncementStatus(notiID: [Int], completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
}

//MARK: -AnnouncementService
class AnnouncementService: AnnouncementServiceProtocol {
    private let service = BaseService.shared
    
    func getAnnouncementList(page: Int, per_page: Int, completion: @escaping ((Result<BaseModel<[AnnouncementModels]>, APIError>) -> Void)) {
        let url = AuthServiceAPI.announcement.urlString
        let params: [String: Any] = [
            "page": page,
            "per_page": per_page
        ]
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setAnnouncementStatus(markType: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.markAsRead.urlString
        let params: [String: Any] = [
            "mark_type": markType
        ]
        self.service.POST(url: url, param: params, completion: completion)
    }
    
    func setSingleAnnouncementStatus(notiID: [Int], completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.markAsRead.urlString
        let params: [String: Any] = [
            "announcement_ids": notiID
        ]
        self.service.POST(url: url, param: params, completion: completion)
    }
}
