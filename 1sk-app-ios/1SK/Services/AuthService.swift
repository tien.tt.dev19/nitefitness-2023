//
//  AuthService.swift
//  1SK
//
//  Created by vuongbachthu on 9/20/21.
//

import Foundation
import Alamofire

protocol AuthServiceProtocol {
    func loginWithFacebook(accessToken: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void))
    func loginWithGoogle(accessToken: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void))
    func loginWithApple(accessToken: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void))
    func loginWith1SK(account: String, password: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void))
    
    func getUserInfo(completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void))
    func getUserInfo(accessToken: String?, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void))
    func setUpdateProfile(newUser: UserModel, image: UIImage?, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void))
    func setUpdateSubProfile(newUser: UserModel, image: UIImage?, accessToken: String?, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void))
    
    func setOrderDeposit(orderDeposit: OrderDeposit, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func getUserPHR(completion: @escaping ((Result<BaseModel<[PHRModel]>, APIError>) -> Void))
    
    func setValuePHR(value: Any, phr: PHR?, completion: @escaping ((Result<BaseModel<PHRModel>, APIError>) -> Void))
    
    func getInfoRequestLogin(token: String, completion: @escaping ((Result<BaseModel<LoginRequestModel>, APIError>) -> Void))
    func setConfirmRequestLogin(token: String, action: TypeConfirmRequest, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func setVerifyPhoneNumber(phone: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setVerifyCodeOtp(phone: String, codeOtp: String, state: StateAuthPhone, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setRegisterUser(codeOtp: String, fullname: String, email: String, phoneNumber: String, password: String, passwordConfirm: String, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void))
    func setLoginWithPhoneNumber(phone: String, password: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void))
    func setChangePassword(oldPassword: String, newPassword: String, reNewPassword: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setValidatePhoneNumber(phone: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setResetPassword(phoneNumber: String, otpCode: String, newPassword: String, reNewPassword: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setDeleteAccount(completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func setDeleteProfile(accessToken: String?, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func setSubAccount(newUser: UserModel, avatar: UIImage?, createdBy: Int, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void))
    func getSubAccounts(completion: @escaping ((Result<BaseModel<[AuthModel]>, APIError>) -> Void))
    func getConfig(completion: @escaping ((Result<BaseModel<[ConfigModel]>, APIError>) -> Void))
}

// MARK: AuthServiceProtocol
class AuthService: AuthServiceProtocol {
    private let service = BaseService.shared
    
    func loginWithFacebook(accessToken: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void)) {
        let param = [
            "token": accessToken
        ]
        let url = AuthServiceAPI.loginWithFacebook.urlString
        self.service.POST(url: url, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func loginWithGoogle(accessToken: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void)) {
        let param = [
            "token": accessToken
        ]
        let url = AuthServiceAPI.loginWithGoogle.urlString
        self.service.POST(url: url, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func loginWithApple(accessToken: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void)) {
        let param = [
            "authorization_code": accessToken
        ]
        let url = AuthServiceAPI.loginWithApple.urlString
        self.service.POST(url: url, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func loginWith1SK(account: String, password: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void)) {
        let param = [
            "account": account,
            "password": password
        ]
        let url = AuthServiceAPI.loginWithAccount.urlString
        self.service.POST(url: url, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getUserInfo(completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.customer_me.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func getUserInfo(accessToken: String?, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.customer_me.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: nil, headers: headers, completion: completion)
    }
    
    func setUpdateProfile(newUser: UserModel, image: UIImage?, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void)) {
        var params: [String: String] = [
            "full_name": newUser.fullName ?? "",
            "birthday": newUser.birthday ?? "",
            "phone_number": newUser.phoneNumber ?? "",
            "gender": "\(newUser.gender?.id ?? 2)"
        ]
        
        if let value = newUser.height {
            params.updateValue("\(value)", forKey: "height")
        }
        if let value = newUser.weight {
            params.updateValue("\(value)", forKey: "weight")
        }
        if let value = newUser.blood {
            params.updateValue("\(value)", forKey: "blood")
        }
        
        let url = AuthServiceAPI.customer_me.urlString
        var imageParam: [String: Data] = [:]
        if let `image` = image, let imageData = image.jpegData(compressionQuality: 0.3) {
            imageParam["avatar"] = imageData
        }
        self.service.UPLOAD(url: url, param: params, imageParam: imageParam, completion: completion)
    }
    
    func setUpdateSubProfile(newUser: UserModel, image: UIImage?, accessToken: String?, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void)) {
        var params: [String: String] = [
            "full_name": newUser.fullName ?? "",
            "birthday": newUser.birthday ?? "",
            //"phone_number": newUser.phoneNumber ?? "",
            "gender": "\(newUser.gender?.id ?? 2)"
        ]
        
        if let value = newUser.height {
            params.updateValue("\(value)", forKey: "height")
        }
        if let value = newUser.weight {
            params.updateValue("\(value)", forKey: "weight")
        }
        if let value = newUser.blood {
            params.updateValue("\(value)", forKey: "blood")
        }
        
        let url = AuthServiceAPI.customer_me.urlString
        var imageParam: [String: Data] = [:]
        if let `image` = image, let imageData = image.jpegData(compressionQuality: 0.3) {
            imageParam["avatar"] = imageData
        }
        
        let headers = self.service.headers(accessToken: accessToken)
        self.service.UPLOAD(url: url, param: params, imageParam: imageParam, headers: headers, completion: completion)
    }
    
    func setOrderDeposit(orderDeposit: OrderDeposit, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "product_name": orderDeposit.productName!,
            "price": orderDeposit.price!,
            "discount": orderDeposit.discount!,
            "sale_price": orderDeposit.salePrice!,
            "amount": orderDeposit.amount!,
            "product_code": orderDeposit.productCode!
        ]
        let urlRequest = AuthServiceAPI.orderWithDeposit.urlString
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getUserPHR(completion: @escaping ((Result<BaseModel<[PHRModel]>, APIError>) -> Void)) {
        let url = AuthServiceAPI.phrGet.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func setValuePHR(value: Any, phr: PHR?, completion: @escaping ((Result<BaseModel<PHRModel>, APIError>) -> Void)) {
        var params: [String: Any] = [:]
        
        switch phr {
        case .weight:
            params.updateValue("weight", forKey: "name")
            params.updateValue("kg", forKey: "unit")
            params.updateValue(value, forKey: "value")
            
        case .height:
            params.updateValue("height", forKey: "name")
            params.updateValue("cm", forKey: "unit")
            params.updateValue(value, forKey: "value")
            
        case .blood:
            params.updateValue("blood_type", forKey: "name")
            params.updateValue("none", forKey: "unit")
            params.updateValue(value, forKey: "value")
            
        case .pressure:
            params.updateValue("blood_pressure", forKey: "name")
            params.updateValue("mmHg", forKey: "unit")
            params.updateValue(value, forKey: "value")
            
        case .spo2:
            params.updateValue("spo2", forKey: "name")
            params.updateValue("%", forKey: "unit")
            params.updateValue(value, forKey: "value")
            
        case .sugar:
            params.updateValue("blood_sugar_level", forKey: "name")
            params.updateValue("mm/dl", forKey: "unit")
            params.updateValue(value, forKey: "value")
            
        default:
            break
        }
        
        let url = AuthServiceAPI.phrSet.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getInfoRequestLogin(token: String, completion: @escaping ((Result<BaseModel<LoginRequestModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.qrCodeRequest(token).urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func setConfirmRequestLogin(token: String, action: TypeConfirmRequest, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "token": token,
            "action": action.rawValue
        ]
        let url = AuthServiceAPI.qrCodeConfirm.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setVerifyPhoneNumber(phone: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "phone_number": phone
        ]
        let url = AuthServiceAPI.customerOtp.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setVerifyCodeOtp(phone: String, codeOtp: String, state: StateAuthPhone, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        var params: [String: Any] = [
            "phone_number": phone,
            "digit_code": codeOtp
        ]
        
        switch state {
        case .Register:
            break
        case .ForgotPassword:
            break
        case .Verify:
            params.updateValue(state.value, forKey: "type")
        case .None:
            break
        }
        
        let url = AuthServiceAPI.customerValidateOtp.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setRegisterUser(codeOtp: String, fullname: String, email: String, phoneNumber: String, password: String, passwordConfirm: String, completion: @escaping ((Result<BaseModel<UserModel>, APIError>) -> Void)) {
        var params: [String: Any] = [
            "digit_code": codeOtp,
            "full_name": fullname,
            "phone_number": phoneNumber,
            "password": password,
            "password_confirmation": passwordConfirm
        ]
        if email.count > 3 {
            params.updateValue(email, forKey: "email")
        }
        let url = AuthServiceAPI.customerRegister.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setLoginWithPhoneNumber(phone: String, password: String, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "account": phone,
            "password": password
        ]
        let url = AuthServiceAPI.customerLogin.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setChangePassword(oldPassword: String, newPassword: String, reNewPassword: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "old_password": oldPassword,
            "new_password": newPassword,
            "re_new_password": reNewPassword
        ]
        let url = AuthServiceAPI.customerChangePassword.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setValidatePhoneNumber(phone: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "phone_number": phone
        ]
        let url = AuthServiceAPI.customerValidatePhoneNumber.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setResetPassword(phoneNumber: String, otpCode: String, newPassword: String, reNewPassword: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "phone_number": phoneNumber,
            "digit_code": otpCode,
            "new_password": newPassword,
            "new_password_confirmation": reNewPassword
        ]
        let url = AuthServiceAPI.customerResetPassword.urlString
        self.service.POST(url: url, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setDeleteAccount(completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.customer_me_delete.urlString
        self.service.DELETE(url: url, param: nil, completion: completion)
    }
    
    func setDeleteProfile(accessToken: String?, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.customer_me_delete.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.DELETE(url: url, param: nil, headers: headers, completion: completion)
    }
    
    func setSubAccount(newUser: UserModel, avatar: UIImage?, createdBy: Int, completion: @escaping ((Result<BaseModel<AuthModel>, APIError>) -> Void)) {
        var params: [String: String] = [
            "full_name": newUser.fullName ?? "",
            "birthday": newUser.birthday ?? "",
            "gender": "\(newUser.gender?.id ?? 2)",
            "created_by": "\(createdBy)"
        ]
        
        if let value = newUser.height {
            params.updateValue("\(value)", forKey: "height")
        }
        if let value = newUser.weight {
            params.updateValue("\(value)", forKey: "weight")
        }
        if let value = newUser.blood {
            params.updateValue("\(value)", forKey: "blood")
        }
        
        let url = AuthServiceAPI.customer_sub_account.urlString
        var imageParam: [String: Data] = [:]
        if let image = avatar, let imageData = image.jpegData(compressionQuality: 0.3) {
            imageParam["avatar"] = imageData
        }
        self.service.UPLOAD(url: url, param: params, imageParam: imageParam, completion: completion)
    }
    
    func getSubAccounts(completion: @escaping ((Result<BaseModel<[AuthModel]>, APIError>) -> Void)) {
        let url = AuthServiceAPI.customer_sub_accounts.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func getConfig(completion: @escaping ((Result<BaseModel<[ConfigModel]>, APIError>) -> Void)) {
        let url = AuthServiceAPI.configs.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
}
