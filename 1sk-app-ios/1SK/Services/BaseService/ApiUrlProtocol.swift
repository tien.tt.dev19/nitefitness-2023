//
//  APIUrlProtocol.swift
//  1SK
//
//  Created by tuyenvx on 9/30/20.
//

import Foundation

// MARK: ApiUrlProtocol
protocol ApiUrlProtocol {
    var path: String { get }
    var fullURLString: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension ApiUrlProtocol {
    var fullURLString: String {
        return String(format: "%@/%@", BASE_URL, path)
    }

    var urlString: String {
        return String(format: "%@/%@", FITNESS_SERVICE_URL, self.path)
    }

    var url: URL! {
        return URL(string: self.fullURLString)
    }
}

// MARK: AuthServiceUrlProtocol
protocol AuthServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension AuthServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", AUTH_SERVICE_URL, self.path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: FitnessServiceUrlProtocol
protocol FitnessServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension FitnessServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", FITNESS_SERVICE_URL, path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: CareServiceUrlProtocol
protocol CareServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension CareServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", CARE_SERVICE_URL, path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: StoreServiceUrlProtocol
protocol StoreServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension StoreServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", STORE_SERVICE_URL, path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: FitServiceUrlProtocol
protocol FitServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension FitServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", FITNESS_SERVICE_URL, path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: ConnectServiceUrlProtocol
protocol ConnectServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension ConnectServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", CONNECT_SERVICE_URL, self.path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: WordPressServiceUrlProtocol
protocol WordPressServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension WordPressServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", WORDPRESS_SERVICE_URL, self.path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}

// MARK: JumpRopeServiceUrl
protocol JumpRopeServiceUrlProtocol {
    var path: String { get }
    var urlString: String { get }
    var url: URL! { get }
}

extension JumpRopeServiceUrlProtocol {
    var urlString: String {
        return String(format: "%@/%@", CONNECT_SERVICE_URL, self.path)
    }

    var url: URL! {
        return URL(string: self.urlString)
    }
}
