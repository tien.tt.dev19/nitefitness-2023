//
//  Error.swift
//  1SK
//
//  Created by Thaad on 28/02/2022.
//

import Foundation

extension Error {
    var errorCode: Int? {
        return (self as NSError).code
    }
}
