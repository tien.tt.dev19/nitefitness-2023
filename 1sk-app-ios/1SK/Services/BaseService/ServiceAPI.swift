//
//  ServiceAPI.swift
//  1SK
//
//  Created by tuyenvx on 11/01/2021.
//

import Foundation

// MARK: - AuthServiceAPI
enum AuthServiceAPI: AuthServiceUrlProtocol {
    case loginWithFacebook
    case loginWithGoogle
    case loginWithApple
    case loginWithAccount
    case customer_me
    case orderWithDeposit
    case phrGet
    case phrSet
    case qrCodeRequest(String?)
    case qrCodeConfirm
    case customerOtp
    case customerValidateOtp
    case customerLogin
    case customerRegister
    case customerChangePassword
    case customerValidatePhoneNumber
    case customerResetPassword
    case customer_me_delete
    case connectStrava
    case disconnectStrava
    case identify_providers_strava
    case me_identify_providers
    case customer_sub_account
    case customer_sub_accounts
    case announcement
    case markAsRead
    case configs
    
    var path: String {
        switch self {
        case .loginWithFacebook:
            return "customer/fb/callback"
        case .loginWithGoogle:
            return "customer/google/callback"
        case .loginWithApple:
            return "customer/apple/login"
        case .loginWithAccount:
            return "customer/login"
        case .customer_me:
            return "customer/me"
        case .orderWithDeposit:
            return "customer/order-with-deposit"
        case .phrGet:
            return "customer/phr/indexes"
        case .phrSet:
            return "customer/phr/index"
        case .qrCodeRequest(let id):
            return "customer/qrcode/request\(id.toURLPath())"
        case .qrCodeConfirm:
            return "customer/qrcode/confirm"
        case .customerOtp:
            return "customer/otp"
        case .customerValidateOtp:
            return "customer/validate-otp"
        case .customerLogin:
            return "customer/login"
        case .customerRegister:
            return "customer/register"
        case .customerChangePassword:
            return "customer/change-password"
        case .customerValidatePhoneNumber:
            return "customer/validate-phone-number"
        case .customerResetPassword:
            return "customer/reset-password"
        case .customer_me_delete:
            return "customer/me/delete"
        case .connectStrava:
            return "customer/strava/callback"
        case .disconnectStrava:
            return "customer/strava/disconnect"
        case .identify_providers_strava:
            return "customer/identify-providers"
        case .me_identify_providers:
            return "customer/me/identify-providers"
        case .customer_sub_account:
            return "customer/sub-account"
        case .customer_sub_accounts:
            return "customer/sub-accounts"
        case .announcement:
            return "announcements"
        case .markAsRead:
            return "announcement/mark-as-read"
        case .configs:
            return "configs"
        }
    }
}

// MARK: - CareServiceAPI
enum CareServiceAPI: CareServiceUrlProtocol {
    case specialists
    case doctors
    case doctor(String?)
    case doctorTime
    case appointment(String?)
    case listAppointments
    case appointmentToken
    case appointmentTokenRenew
    case hotline
    case paymentPeriod
    case announcements
    case announcementsUnread
    case announcement(String?)
    case announcementRead
    case appointmentVote
    case appointmentJoin
    case voucherShowByCode
    case voucherShowByCodeDoctor
    case generateHashUserID
    case userTags
    case getPopups
    case markAsRead
    case markAsReceived

    var path: String {
        switch self {
        case .specialists:
            return "doctor/specialists"
            
        case .doctors:
            return "doctors"
            
        case .doctor(let id):
            return "doctor\(id.toURLPath())"
            
        case .doctorTime:
            return "doctor/:id/available-times"
            
        case .appointment(let id):
            return "appointment\(id.toURLPath())"
            
        case .listAppointments:
            return "appointments"
            
        case .appointmentToken:
            return "appointment/:id/token"
            
        case .appointmentTokenRenew:
            return "appointment/:id/renew-token"
            
        case .hotline:
            return "hotline"
            
        case .paymentPeriod:
            return "appointment-payment-period"
            
        case .announcements:
            return "customer/announcements"
            
        case .announcementsUnread:
            return "customer/announcement/unread"
            
        case .announcement(let id):
            return "customer/announcement\(id.toURLPath())"
            
        case .announcementRead:
            return "customer/announcement/read"
            
        case .appointmentVote:
            return "appointment/:id/vote"
            
        case .appointmentJoin:
            return "appointment/:id/join"
            
        case .voucherShowByCode:
            return "voucher/show-by-code/:code"
            
        case .voucherShowByCodeDoctor:
            return "doctor/:doctorId/voucher/show-by-code/:code"
            
        case .generateHashUserID:
            return "customer/onesignal/generate-user-id-hash"
            
        case .userTags:
            return "customer/onesignal/get-customer-tags"
            
        case .getPopups:
            return "iam/popups"
            
        case .markAsRead:
            return "iam/popup/mark-as-read/:id"
            
        case .markAsReceived:
            return "iam/popup/mark-as-received"
        }
    }
}

// MARK: - UserServiceAPI
enum UserServiceAPI: ApiUrlProtocol {
    case loginWithGoogle
    case loginWithFacebook
    case loginWithApple
    case user(String?)

    var path: String {
        switch self {
        case .loginWithGoogle:
            return "user/social-login/google"
            
        case .loginWithFacebook:
            return "user/social-login/facebook"
            
        case .loginWithApple:
            return "user/social-login/apple"
            
        case .user(let id):
            return "user\(id.toURLPath())"
        }
    }
}

// MARK: - CareBlogServiceAPI
enum CareBlogServiceAPI: FitnessServiceUrlProtocol {
    case blogPosts
    case blogRelates
    case mediaVideos
    case mediaVideo(String?)
    case mediaVideoRelated

    var path: String {
        switch self {
        case .blogPosts:
            return "portal/blog/posts"
        case .blogRelates:
            return "portal/blog/relates"
        case .mediaVideos:
            return "media/videos"
        case .mediaVideo(let id):
            return "media/video\(id.toURLPath())"
        case .mediaVideoRelated:
            return "media/video/related"
        }
    }
}

// MARK: - BlogServiceAPI
enum BlogServiceAPI: ApiUrlProtocol {
    case blogService(String?)
    case updateStatus
    case blog(String?)
    case feature
    case findByBlogSlug
    case findByCategorySlug
    case findBlogByTag
    case blogTopic(String?)
    case blogByTopic
    case relate
    case archives

    var path: String {
        switch self {
        case .blogService(let id):
            return "blog/category\(id.toURLPath())"
        case .updateStatus:
            return "blog/category/updateStatus"
        case .blog(let id):
            return "blog\(id.toURLPath())"
        case .feature:
            return "blog/featured"
        case .findByBlogSlug:
            return "blog/show_slug"
        case .findByCategorySlug:
            return "blog/blogByCategory"
        case .findBlogByTag:
            return "blog/blogByTag"
        case .blogTopic(let id):
            return "blog/topic\(id.toURLPath())"
        case .blogByTopic:
            return "blog/blogByTopic"
        case .relate:
            return "blog/relatedBlog"
        case .archives:
            return "archives"
        }
    }
}

// MARK: - FitnessServiceAPI
enum PortalServiceAPI: FitnessServiceUrlProtocol {
    case blogService(String?)
    case updateStatus
    case blog
    case detailBolog(Int?)
    case feature
    case findByBlogSlug
    case findByCategorySlug
    case findBlogByTag
    case blogTopic(String?)
    case blogByTopic
    case relate
    case blogTag
    case blogUserHistory
    case blogs

    var path: String {
        switch self {
        case .blogService(let id):
            return "blog/category\(id.toURLPath())"
        case .updateStatus:
            return "blog/category/updateStatus"
        case .blog:
            return "portal/blog/posts"
        case .detailBolog(let id):
            let url: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "portal/blog/post\(url.toURLPath())"
        case .feature:
            return "blog/featured"
        case .findByBlogSlug:
            return "blog/show_slug"
        case .findByCategorySlug:
            return "blog/blogByCategory"
        case .findBlogByTag:
            return "blog/blogByTag"
        case .blogTopic(let id):
            return "blog/topic\(id.toURLPath())"
        case .blogByTopic:
            return "blog/blogByTopic"
        case .relate:
            return "portal/blog/relates"
        case .blogTag:
            return "portal/customer/tags"
        case .blogUserHistory:
            return "portal/customer/blog/blog-user-history"
        case .blogs:
            return "portal/blog/posts"
        }
    }
}

// MARK: - FitnessServiceAPI
enum FitnessServiceAPI: ApiUrlProtocol {
    case attribute
    case category
    case topic(String?)
    case coach(String?)
    case video(Int?)
    case relatedVideo
    case archives

    var path: String {
        switch self {
        case .attribute:
            return "media/attribute"
        case .category:
            return "media/category"
        case .topic(let id):
            return "fitness/topic\(id.toURLPath())"
        case .coach(let id):
            return "fitness/coach\(id.toURLPath())"
        case .video(let id):
            let url: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "fitness/video\(url.toURLPath())"
        case .relatedVideo:
            return "fitness/video/related"
        case .archives:
            return "archives"
        }
    }
}

enum MediaServiceAPI: FitnessServiceUrlProtocol {
    case attribute
    case category
    case topic(String?)
    case coach(String?)
    case video(Int?)
    case detailVideo(Int?)
    case relatedVideo
    case videoUserHistory
    case videos

    var path: String {
        switch self {
        case .attribute:
            return "media/attribute"
        case .category:
            return "media/category"
        case .topic(let id):
            return "media/topic\(id.toURLPath())"
        case .coach(let id):
            return "fitness/coach\(id.toURLPath())"
        case .video(let id):
            let url: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "media/videos\(url.toURLPath())"
        case .detailVideo(let id):
            let url: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "media/video\(url.toURLPath())"
        case .relatedVideo:
            return "media/video/related"
        case .videoUserHistory:
            return "media/video-user-history"
        case .videos:
            return "media/videos"
        }
    }
}

// MARK: - UploadServiceAPI
enum UploadServiceAPI: ApiUrlProtocol {
    case avatar
    case image

    var path: String {
        switch self {
        case .avatar:
            return "upload/user/avatar"
        case .image:
            return "upload/fitness/image"
        }
    }

    var fullURLString: String {
        return String(format: "%@/%@", UPLOAD_IMAGE_URL, path)
    }

    var url: URL! {
        return URL(string: fullURLString)
    }
}

enum NewUploadServiceAPI: FitnessServiceUrlProtocol {
    case avatar
    case image

    var path: String {
        switch self {
        case .avatar:
            return "upload/user/avatar"
        case .image:
            return "files/uploadFile"
        }
    }
}

// MARK: - MyHealthServiceAPI
enum MyHealthServiceAPI: ApiUrlProtocol {
    case profile(Int?)
    case folder(String?)
    case file(String?)
    case activitySubject
    case activityUser(String?)
    case activityUserDetail
    case updateActivityUser
    case followUser
    case unfollowedUserByMe
    case listUserByKeyword
    case listUserFollowingMe
    case listUserFollowedByMe

    var path: String {
        switch self {
        case .profile(let id):
            let url: String? = String(id ?? 0) == "0" ? nil : String(id ?? 0)
            return "myhealth/profile\(url.toURLPath())"
        case .folder(let id):
            return "myhealth/folder\(id.toURLPath())"
        case .file(let id):
            return "myhealth/file\(id.toURLPath())"
        case .activitySubject:
            return "myhealth/activitySubject"
        case .activityUser(let id):
            return "myhealth/activityUser\(id.toURLPath())"
        case .activityUserDetail:
            return "myhealth/activityUserDetail"
        case .updateActivityUser:
            return "myhealth/updateActivityUser"
        case .followUser:
            return "myhealth/followUser"
        case .unfollowedUserByMe:
            return "myhealth/unfollowedUserByMe"
        case .listUserByKeyword:
            return "myhealth/listUserByKeyword"
        case .listUserFollowingMe:
            return "myhealth/listUserFollowingMe"
        case .listUserFollowedByMe:
            return "myhealth/listUserFollowedByMe"
        }
    }
}

// MARK: - MyHealthServiceAPI
enum ActivitiesServiceAPI: FitnessServiceUrlProtocol {
    case profile(Int?)
    case folder(String?)
    case file(String?)
    case activitySubject
    case activitySubjects
    case activityUser(Int?)
    case activityUserDetail
    case updateActivityUser
    case followUser
    case unfollowedUserByMe
    case listUserByKeyword
    case listUserFollowingMe
    case listUserFollowedByMe
    
    case listActivites
    case activityLogs

    var path: String {
        switch self {
        case .profile(let id):
            let url: String? = String(id ?? 0) == "0" ? nil : String(id ?? 0)
            return "myhealth/profile\(url.toURLPath())"
        case .folder(let id):
            return "myhealth/folder\(id.toURLPath())"
        case .file(let id):
            return "myhealth/file\(id.toURLPath())"
        case .activitySubject:
            return "activity/activitySubject"
        case .activityUser(let id):
            let url: String? = String(id ?? 0) == "0" ? nil : String(id ?? 0)
            return "activity/log\(url.toURLPath())"
        case .activityUserDetail:
            return "activity/activityUser/activityUserDetail"
        case .updateActivityUser:
            return "activity/updateActivityUser"
        case .followUser:
            return "myhealth/followUser"
        case .unfollowedUserByMe:
            return "myhealth/unfollowedUserByMe"
        case .listUserByKeyword:
            return "myhealth/listUserByKeyword"
        case .listUserFollowingMe:
            return "myhealth/listUserFollowingMe"
        case .listUserFollowedByMe:
            return "myhealth/listUserFollowedByMe"
        case .listActivites:
            return "activity/activityUsers"
        case .activitySubjects:
            return "activity/activitySubjects"
        case .activityLogs:
            return "activity/logs"
        }
    }
}

// MARK: - ConfigServiceAPI
enum ConfigServiceAPI: FitnessServiceUrlProtocol {
    case pageTopic
    case banner
    case home
    case updateNotifyStatus
    case notify
    case notifyNumber
    case saveDeviceToken
    case pageHomeFitnessApp
    case pageHomeApp
    case settingApp
    case categories

    var path: String {
        switch self {
        case .pageTopic:
            return "config/pageTopic"
        case .banner:
            return "config/bannerSlide"
        case .home:
            return "config/pageTopicHome"
        case .updateNotifyStatus:
            return "config/updateStatusNotifyUser"
        case .notify:
            return "config/notify"
        case .notifyNumber:
            return "config/getNumberNotify"
        case .saveDeviceToken:
            return "config/saveDeviceToken"
        case .pageHomeFitnessApp:
            return "config/pageHomeFitnessApp"
        case .pageHomeApp:
            return "config/pageHomeApp"
        case .settingApp:
            return "config/getSettingApp"
        case .categories:
            return "config/categories"
        }
    }
}

// MARK: - NewConfigServiceAPI
enum NewConfigServiceAPI: FitnessServiceUrlProtocol {
    case settingApp
    case banner
    case slider
    
    var path: String {
        switch self {
        case .settingApp:
            return "config/getSettingApp"
        case .banner:
            return "config/banners"
        case .slider:
            return "config/sliders"
        }
    }
}

// MARK: - PostServiceAPI
enum PostServiceAPI: ApiUrlProtocol {
    case post(String?)
    case like
    case likeByPost
    case permissions

    var path: String {
        switch self {
        case .post(let id):
            return "post\(id.toURLPath())"
        case .like:
            return "post/like"
        case .likeByPost:
            return "post/likeByPost"
        case .permissions:
            return "post/permissions"
        }
    }
}

// MARK: - PostServiceAPI
enum SocialServiceAPI: FitnessServiceUrlProtocol {
    case post(Int?)
    case like
    case likeByPost
    case permissions
    case createPost(Int?)
    case listComment
    case createComment(Int?)

    var path: String {
        switch self {
        case .post(let id):
            let url: String? = String(id ?? 0) == "0" ? nil : String(id ?? 0)
            return "social/posts\(url.toURLPath())"
        case .like:
            return "social/post/like"
        case .likeByPost:
            return "post/likeByPost"
        case .permissions:
            return "post/permissions"
        case .createPost(let id):
            let url: String? = String(id ?? 0) == "0" ? nil : String(id ?? 0)
            return "social/post\(url.toURLPath())"
        case .listComment:
            return "social/comments"
        case .createComment(let id):
            let url: String? = String(id ?? 0) == "0" ? nil : String(id ?? 0)
            return "social/comment\(url.toURLPath())"
        }
    }
}

// MARK: - CommentServiceAPI
enum CommentServiceAPI: ApiUrlProtocol {
    case comment(String?)
    case commentByComment

    var path: String {
        switch self {
        case .comment(let id):
            return "comment_service\(id.toURLPath())"
        case .commentByComment:
            return "comment_service/getByComment"
        }
    }
}

// MARK: - ExserciseServiceAPI
enum ExserciseServiceAPI: ApiUrlProtocol {
    case type(String?)
    case subject(String?)
    case level(String?)
    case workout(Int?)
    case exsercise(String?)
    case filter(String?)
    case rate(String?)
    case bookmark(String?)
    case music(String?)
    case suggest(String?)
    case clone

    var path: String {
        switch self {
        case .type(let id):
            return "exercise/type\(id.toURLPath())"
            
        case .subject(let id):
            return "exercise/subject\(id.toURLPath())"
            
        case .level(let id):
            return "exercise/level\(id.toURLPath())"
            
        case .workout(let id):
            let id: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "exercise/workout\(id.toURLPath())"
            
        case .exsercise(let id):
            return "exercise\(id.toURLPath())"
            
        case .filter(let id):
            return "exercise/workout/allFilter\(id.toURLPath())"
            
        case .rate(let id):
            return "exercise/workout/rate\(id.toURLPath())"
            
        case .bookmark(let id):
            return "exercise/workout/bookmark\(id.toURLPath())"
            
        case .music(let id):
            return "exercise/music\(id.toURLPath())"
            
        case .suggest(let id):
            return "exercise/suggest\(id.toURLPath())"
            
        case .clone:
            return "exercise/workout/clone"
        }
    }
}

enum NewExserciseServiceAPI: FitnessServiceUrlProtocol {
    case exercise
    case music
    
    var path: String {
        switch self {
        case .exercise:
            return "exercises"
        case .music:
            return "musics"
        }
    }
}

// MARK: - WorkoutServiceAPI
enum WorkoutServiceAPI: FitnessServiceUrlProtocol {
    case createWorkout(Int?)
    case listWorkout
    case workout(Int?)
    case filterWorkout
    case bookMark
    case suggest
    case clone
    case rate
    case listCategory
    case categoryTarget
    case category(Int)
    case listRecentWorkout
    case statisticalWorkouts

    var path: String {
        switch self {
        case .createWorkout(let id):
            let id: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "workout\(id.toURLPath())"
        case .listWorkout:
            return "workouts"
        case .workout(let id):
            let id: String? = String(id ?? -1) == "-1" ? nil : String(id ?? -1)
            return "workout\(id.toURLPath())"
        case .filterWorkout:
            return "workout/filters"
        case .bookMark:
            return "workout/bookmark"
        case .suggest:
            return "workout/suggest"
        case .clone:
            return "workout/cloneWorkout"
        case .rate:
            return "workout/rate"
        case .listCategory:
            return "categories"
        case .categoryTarget:
            return "workout/recent-workout"
        case .category(let id):
            return "category/\(id)"
        case .listRecentWorkout:
            return "workout/recent-workouts"
        case .statisticalWorkouts:
            return "workout/statistical-workouts"
        }
    }
}

// MARK: - LevelServiceAPI
enum LevelServiceAPI: FitnessServiceUrlProtocol {
    case level(String?)
    
    var path: String {
        switch self {
        case .level(let id):
            return "level\(id.toURLPath())"
        }
    }
}

// MARK: - LevelServiceAPI
enum SubjectServiceAPI: FitnessServiceUrlProtocol {
    case subject(String?)
    
    var path: String {
        switch self {
        case .subject(let id):
            return "subject\(id.toURLPath())"
        }
    }
}

// MARK: - LevelServiceAPI
enum TyoeServiceAPI: FitnessServiceUrlProtocol {
    case type(String?)
    
    var path: String {
        switch self {
        case .type(let id):
            return "type\(id.toURLPath())"
        }
    }
}

// MARK: - StoreServiceAPI
enum StoreServiceAPI: StoreServiceUrlProtocol {
    case slider(String?)
    case products
    case product(String?)
    case productReviews
    case countries
    case filterAttribute
    case storeSearchHistory
    case storeSearchDeleteHistory
    case addressCreate
    case addressUpdate(String?)
    case defaultAddress
    case paymentMethods
    case orderWithId(String?)
    case order
    case orders
    case orderStatuses
    case productReview
    case reviews
    case flashSale
    case registerProduct
    case variationProduct(Int)
    case flashSaleProducts
    case sliders
    case productsHot
    case strategies
    
    var path: String {
        switch self {
        case .slider(let id):
            return "slider\(id.toURLPath())"
            
        case .products:
            return "products"
            
        case .product(let id):
            return "product\(id.toURLPath())"
            
        case .productReviews:
            return "product/:id/reviews"
            
        case .countries:
            return "countries"
            
        case .filterAttribute:
            return "product-attribute-sets"
            
        case .storeSearchHistory:
            return "search-histories"

        case .storeSearchDeleteHistory:
            return "search-history"
            
        case .addressCreate:
            return "address"
            
        case .addressUpdate(let id):
            return "address\(id.toURLPath())"
            
        case .defaultAddress:
            return "default-address"
            
        case .paymentMethods:
            return "payment-methods"
            
        case .order:
            return "order"
            
        case .orderWithId(let id):
            return "order\(id.toURLPath())"

        case .orders:
            return "orders"
            
        case .orderStatuses:
            return "order-statuses"
            
        case .productReview:
            return "product/:id/review"
            
        case .reviews:
            return "reviews"
            
        case .flashSale:
            return "flash-sale"
            
        case .registerProduct:
            return "register-product"
            
        case .variationProduct(let id):
            return "variation-product/\(id)"
            
        case .flashSaleProducts:
            return "flash-sale-products"
            
        case .sliders:
            return "sliders"
            
        case .productsHot:
            return "products-hot"
            
        case .strategies:
            return "strategies"
        }
    }
}

// MARK: - StoreServiceAPI
enum FitServiceAPI: FitServiceUrlProtocol {
    case workoutEquipments
    case exercises
    case exercise_detail
    case customerWorkout
    
    var path: String {
        switch self {
        case .workoutEquipments:
            return "workout-equipments"
        case .exercises:
            return "iot/exercises"
        case .exercise_detail:
            return "iot/exercise"
        case .customerWorkout:
            return "customer/workout"
        }
    }
}

//MARK: - VisualRaceServiceAPI
enum FitRaceServiceAPI: FitServiceUrlProtocol {
    case visualRaceList
    case virtualRaceDetail(Int)
    case ranking
    case stravaRanking
    case register
    case registered
    case vr_sport
    case athleteActivity
    case strava_activities_total

    var path: String {
        switch self {
        case .visualRaceList:
            return "virtual-races"
        case .virtualRaceDetail(let id):
            return "virtual-race/\(id)"
        case .ranking:
            return "virtual-race/ranking-statistic"
        case .register:
            return "virtual-race/register"
        case .registered:
            return "virtual-race/registered"
        case .stravaRanking:
            return "strava/activities/ranking"
        case .vr_sport:
            return "vr/sports"
        case .athleteActivity:
            return "strava/activities"
        case .strava_activities_total:
            return "strava/activities/total"
        }
    }
}
 
//MARK: - FitVideoServiceAPI
enum FitVideoServiceAPI: FitServiceUrlProtocol {
    case listVideoExercise
    case relatedVideoExercise
    case listCategoryVideoExercise
    case historyVideoExercise
    case likedVideoExercise
    case videoUserHistory
    case videoUserFavorite
    case removeFavoriteList(Int)
    case lastSeen
    case categories
    
    var path: String {
        switch self {
        case .listVideoExercise:
            return "media/videos"
        case .relatedVideoExercise:
            return "media/video/related"
        case .historyVideoExercise:
            return "media/video-user-history"
        case .likedVideoExercise:
            return "media/video-user-favourite"
        case .listCategoryVideoExercise:
            return "media/category"
        case .videoUserHistory:
            return "media/video-user-history"
        case .videoUserFavorite:
            return "media/video-user-favourite"
        case .removeFavoriteList(let id):
            return "media/video-user-favourite/\(id)"
        case .lastSeen:
            return "media/video-user-history/last-seen"
        case .categories:
            return "config/categories?slug=video-tap-luyen"
        }
    }
}

// MARK: - ConnectServiceAPI
enum ConnectServiceAPI: ConnectServiceUrlProtocol {
    case customer_me
    case customer_me_update
    case connect_device_name
    case connect_smart_scale
    case connect_smart_scales
    case connect_smart_scales_analysis
    case connect_blood_pressure
    case connect_blood_pressures
    case connect_blood_pressure_analysis
    case consult_smart_scales_content
    case connect_smart_scales_list_device_name
    case consult_blood_pressure_content
    case connect_blood_pressure_list_device_name
    
    var path: String {
        switch self {
        case .customer_me:
            return "customer/me"
        case .customer_me_update:
            return "customer/me/update"
        case .connect_device_name:
            return "connect/device-name"
        case .connect_smart_scale:
            return "connect/smart-scale"
        case .connect_smart_scales:
            return "connect/smart-scales"
        case .connect_smart_scales_analysis:
            return "connect/smart-scale/avg-analysis"
        case .connect_blood_pressure:
            return "connect/blood-pressure"
        case .connect_blood_pressures:
            return "connect/blood-pressures"
        case .connect_blood_pressure_analysis:
            return "connect/blood-pressure/min-max-analysis"
        case .consult_smart_scales_content:
            return "consult/smart-scales/content"
        case .connect_smart_scales_list_device_name:
            return "connect/smart-scales/list-device-name"
        case .consult_blood_pressure_content:
            return "consult/blood-pressure/content"
        case .connect_blood_pressure_list_device_name:
            return "connect/blood-pressure/list-device-name"
        }
    }
}

// MARK: - WordPressServiceAPI
enum WordPressServiceAPI: WordPressServiceUrlProtocol {
    case posts
    case postsId(Int)
    case popularPosts
    case postsByTag
    
    var path: String {
        switch self {
        case .posts:
            return "wp/v2/posts"
            
        case .postsId(let id):
            return "wp/v2/posts/\(id)"
            
        case .popularPosts:
            return "jnews/v1/popularPosts"
            
        case .postsByTag:
            return "jnews/v1/postsByTag"
        }
    }
}

// MARK: - JumpRopeServiceAPI
enum JumpRopeServiceAPI: JumpRopeServiceUrlProtocol {
    case get_record
    case store_record
    case user_statistic
    case ranking
    case user_achievement

    
    var path: String {
        switch self {
        case .get_record:
            return "jump-rope/get-record"
        case .store_record:
            return "jump-rope/store-record"
        case .user_statistic:
            return "jump-rope/user-statistic"
        case .ranking:
            return "jump-rope/ranking"
        case .user_achievement:
            return "jump-rope/user-achievement"
        }
    }
}
