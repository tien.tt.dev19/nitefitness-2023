//
//  BlogService.swift
//  1SK
//
//  Created by tuyenvx on 13/01/2021.
//

import Foundation
import Alamofire

protocol BlogServiceProtocol {
    func getListBlog(categoryId: Int, tagsId: String?, keywords: String?, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    func getBlogDetail(with id: Int, completion: @escaping ((Result<BaseModel<BlogModel>, APIError>) -> Void))
    func getBlogFeatured(type: String, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    func findBlogBySlug(slug: String, completion: @escaping ((Result<BaseModel<BlogModel>, APIError>) -> Void))
    func getListBlogByTag(slug: String, portal: String?, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    func getListRelatedBlog(blogId: Int, categories: [CategoriesVideo], completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    
    func setSeeBlog(blogId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func getListBlogUserHistory(completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    func getListBlogSearch(categoryId: Int, keywords: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    
    func getListVideos(positions: String?, portal: String?, tagId: Int, categoryId: Int, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    
    func getListBlogByTags(positions: String, portal: String, tagId: Int, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    
    func getTags(positions: String, portal: String, type: String, completion: @escaping ((Result<BaseModel<[TagsModel]>, APIError>) -> Void))
    func getBlogBySlug(slug: String, type: String, completion: @escaping ((Result<BaseModel<BlogModel>, APIError>) -> Void))
}

// MARK: BlogServiceProtocol
class BlogService: BlogServiceProtocol {
    
    let service = BaseService.shared

    func getListBlog(categoryId: Int, tagsId: String?, keywords: String?, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        var param: [String: Any] = [
            "category_id": categoryId,
            "page": page,
            "per_page": perPage
        ]
        if let `tagsId` = tagsId {
            param.updateValue(tagsId, forKey: "tag")
        }
        if let `keywords` = keywords {
            param.updateValue(keywords, forKey: "keywords")
        }
        
        self.service.GET(url: PortalServiceAPI.blog.urlString, param: param, completion: completion)
    }

    func getBlogDetail(with id: Int, completion: @escaping ((Result<BaseModel<BlogModel>, APIError>) -> Void)) {
        self.service.GET(url: PortalServiceAPI.detailBolog(id).urlString, param: nil, completion: completion)
    }

    func getBlogFeatured(type: String, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.type: type
        ]
        service.get(url: BlogServiceAPI.feature, param: param, completion: completion)
    }

    func findBlogBySlug(slug: String, completion: @escaping ((Result<BaseModel<BlogModel>, APIError>) -> Void)) {
        let param = [
            SKKey.slug: slug
        ]
        service.get(url: BlogServiceAPI.findByBlogSlug, param: param, completion: completion)
    }

    func getListBlogByTag(slug: String, portal: String?, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "tag": slug
        ]
//        if let `portal` = portal {
//            param.updateValue(portal, forKey: "portal")
//        }
        self.service.GET(url: PortalServiceAPI.blog.urlString, param: param, completion: completion)
    }

    func getListRelatedBlog(blogId: Int, categories: [CategoriesVideo], completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        
        var urlRequest = PortalServiceAPI.relate.urlString
        urlRequest.append(contentsOf: "?blog_id=\(blogId)")
        
        for category in categories {
            if let id = category.id {
                urlRequest.append(contentsOf: "&category_id[]=\(id)")
            }
        }
        
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func setSeeBlog(blogId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "post_id": blogId
        ]
        let urlRequest = PortalServiceAPI.blogUserHistory.urlString
        self.service.POST(url: urlRequest, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getListBlogUserHistory(completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "category_id": CategoryType.blogFit.rawValue
        ]
        let urlRequest = PortalServiceAPI.blogUserHistory.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
    
    func getListBlogSearch(categoryId: Int, keywords: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "category_id": categoryId,
            "keywords": keywords,
            "page": page,
            "per_page": perPage
        ]
        let urlRequest = PortalServiceAPI.blogs.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
    
    func getListVideos(positions: String?, portal: String?, tagId: Int, categoryId: Int, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        var params: [String: Any] = [
            "module": "care",
            "tag_id": "",
            "category_id": "",
            "page": page,
            "per_page": limit
        ]
        if tagId > 0 {
            params.updateValue(tagId, forKey: "tag_id")
        }
        if categoryId > 0 {
            params.updateValue(categoryId, forKey: "category_id")
        }
        if let `positions` = positions {
            params.updateValue(positions, forKey: "positions")
        }
        if let `portal` = portal {
            params.updateValue(portal, forKey: "portal")
        }
        
        
        let urlRequest = CareBlogServiceAPI.mediaVideos.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getListBlogByTags(positions: String, portal: String, tagId: Int, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        var params: [String: Any] = [
            "positions": positions,
            "portal": portal,
            "tag_id": "",
            "page": page,
            "per_page": limit
        ]
        if tagId > 0 {
            params.updateValue(tagId, forKey: "tag_id")
        }
        let urlRequest = CareBlogServiceAPI.blogPosts.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getTags(positions: String, portal: String, type: String, completion: @escaping ((Result<BaseModel<[TagsModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "positions": positions,
            "portal": portal,
            "type": type
        ]
        let urlRequest = PortalServiceAPI.blogTag.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getBlogBySlug(slug: String, type: String, completion: @escaping ((Result<BaseModel<BlogModel>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "slug": slug,
            "type": type
        ]
        
        let urlRequest = PortalServiceAPI.blog.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
}
