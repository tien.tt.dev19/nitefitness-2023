//
//  CareService.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import Foundation
import Alamofire

protocol CareServiceProtocol {
    func getSpecialistList(completion: @escaping ((Result<BaseModel<[SpecialistModel]>, APIError>) -> Void))
    func getDoctorList(voucherCode: String?, specialistId: Int, startDate: String, endDate: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[DoctorModel]>, APIError>) -> Void))
    
    func getDoctorTime(doctorId: Int, startDate: String, endDate: String, completion: @escaping ((Result<BaseModel<[WeekdayModel]>, APIError>) -> Void))
    
    func setConfirmBooking(bookingCare: BookingCare, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void))
    
    func getListAppointments(isComing: Bool, isPats: Bool, page: Int, pageLimit: Int, orderByTime: String?, listStatus: [AppointmentStatus], completion: @escaping ((Result<BaseModel<[AppointmentModel]>, APIError>) -> Void))
    
    func getUserTags(completion: @escaping ((Result<BaseModel<[UserTag]>, APIError>) -> Void))
    
    func setCancelAppointment(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void))
    
    func setFinishAppointment(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void))
    
    func getTokenAppointmentRoom(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<VideoCallModel>, APIError>) -> Void))
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<VideoCallModel>, APIError>) -> Void))
    
    func getHotlinePhoneNumber(completion: @escaping ((Result<BaseModel<HotlineModel>, APIError>) -> Void))
    
    func getPaymentPeriodTime(completion: @escaping ((Result<BaseModel<PaymentPeriodModel>, APIError>) -> Void))
    
    func getListNotification(page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[NotificationModel]>, APIError>) -> Void))
    
    func getListNotificationUnread(page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[NotificationModel]>, APIError>) -> Void))
    
    func getNotificationDetail(announcementId: Int, completion: @escaping ((Result<BaseModel<NotificationModel>, APIError>) -> Void))
    
    func setNotificationRead(announcementId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func setRateVideoCall(appointmentId: Int, rateDoctor: Int, rateAppointment: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func setJoinRoomVideoCall(appointmentId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func getAppointmentDetail(appointmentId: Int, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void))
    
    func getVoucherDetail(voucherCode: String, completion: @escaping ((Result<BaseModel<VoucherModel>, APIError>) -> Void))
    
    func getVoucherDetailDoctor(doctorId: Int, voucherCode: String, completion: @escaping ((Result<BaseModel<VoucherModel>, APIError>) -> Void))
    
    func generateHasUserID(completion: @escaping ((Result<BaseModel<String>, APIError>) -> Void))
    
    func getDoctorDetailById(doctorId: Int, completion: @escaping ((Result<BaseModel<DoctorModel>, APIError>) -> Void))
}

// MARK: CareServiceProtocol
class CareService: CareServiceProtocol {
    
    private let service = BaseService.shared
    
    func getSpecialistList(completion: @escaping ((Result<BaseModel<[SpecialistModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [:]
        let urlRequest = CareServiceAPI.specialists.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getDoctorList(voucherCode: String?, specialistId: Int, startDate: String, endDate: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[DoctorModel]>, APIError>) -> Void)) {
        var params: [String: Any] = [
            "specialist_id": specialistId,
            "start_available_date": startDate,
            "end_available_date": endDate,
            "page": page,
            "per_page": perPage
        ]
        
        if let `voucherCode` = voucherCode {
            params.updateValue(voucherCode, forKey: "voucher_code")
        }
        let urlRequest = CareServiceAPI.doctors.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getDoctorTime(doctorId: Int, startDate: String, endDate: String, completion: @escaping ((Result<BaseModel<[WeekdayModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "start_date": startDate,
            "end_date": endDate,
            "group_by": "date"
        ]
        let urlRequest = CareServiceAPI.doctorTime.urlString.replaceCharacter(target: ":id", withString: "\(doctorId)")
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func setConfirmBooking(bookingCare: BookingCare, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void)) {
        var params: [String: Any] = [
            "include": "bank,doctor,doctor_available_time",
            "doctor_available_time_id": bookingCare.timeId!,
            "patient_name": bookingCare.name!,
            "patient_birth_year": bookingCare.birthday!,
            "patient_gender": bookingCare.gender!.valueVi,
            "patient_phone": bookingCare.phone!,
            "patient_symptom": bookingCare.reason!
        ]
        if let voucherCode = bookingCare.voucher?.code {
            params.updateValue(voucherCode, forKey: "voucher_code")
        }
        
        let urlRequest = CareServiceAPI.appointment(nil).urlString
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getListAppointments(isComing: Bool, isPats: Bool, page: Int, pageLimit: Int, orderByTime: String?, listStatus: [AppointmentStatus], completion: @escaping ((Result<BaseModel<[AppointmentModel]>, APIError>) -> Void)) {
        
        let params: [String: Any] = [:]
        var urlRequest = CareServiceAPI.listAppointments.urlString
        
        urlRequest.append(contentsOf: "?include=customer,doctor,bank,doctor_available_time")
        urlRequest.append(contentsOf: "&page=\(page)")
        urlRequest.append(contentsOf: "&per_page=\(pageLimit)")
        
        if let `orderByTime` = orderByTime {
            urlRequest.append(contentsOf: "&order_by_time=\(orderByTime)")
        }
        
        if isComing == true {
            urlRequest.append(contentsOf: "&coming=\(isComing)")
        }
        if isPats == true {
            urlRequest.append(contentsOf: "&past=\(isPats)")
        }
        
        for status in listStatus {
            urlRequest.append(contentsOf: "&appointment_status_id[]=\(status.id)")
        }
        
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func generateHasUserID(completion: @escaping ((Result<BaseModel<String>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.generateHashUserID.urlString
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func getUserTags(completion: @escaping ((Result<BaseModel<UserTag>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.userTags.urlString
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func setCancelAppointment(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "patient_name": appointment.patientName!,
            "patient_birth_year": appointment.patientBirthYear!,
            "patient_gender": appointment.patientGender!,
            "patient_phone": appointment.patientPhone!,
            "patient_symptom": appointment.patientSymptom!,
            "appointment_status_id": AppointmentStatus.cancel.id
        ]
        let urlRequest = CareServiceAPI.appointment(String(appointment.id!)).urlString
        self.service.PUT(url: urlRequest, param: params, encoding: JSONEncoding.default, completion: completion)
    }
    
    func setFinishAppointment(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "appointment_status_id": AppointmentStatus.finish.id
        ]
        let urlRequest = CareServiceAPI.appointment(String(appointment.id!)).urlString
        self.service.PUT(url: urlRequest, param: params, encoding: JSONEncoding.default, completion: completion)
    }
    
    func getTokenAppointmentRoom(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<VideoCallModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "include": "appointment,appointment.doctor"
        ]
        let urlRequest = CareServiceAPI.appointmentToken.urlString.replaceCharacter(target: ":id", withString: "\(appointment.id!)")
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel, completion: @escaping ((Result<BaseModel<VideoCallModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "include": "appointment,appointment.doctor"
        ]
        let urlRequest = CareServiceAPI.appointmentTokenRenew.urlString.replaceCharacter(target: ":id", withString: "\(appointment.id!)")
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getHotlinePhoneNumber(completion: @escaping ((Result<BaseModel<HotlineModel>, APIError>) -> Void)) {
        let params: [String: Any] = [:]
        let urlRequest = CareServiceAPI.hotline.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getPaymentPeriodTime(completion: @escaping ((Result<BaseModel<PaymentPeriodModel>, APIError>) -> Void)) {
        let params: [String: Any] = [:]
        let urlRequest = CareServiceAPI.paymentPeriod.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getListNotification(page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[NotificationModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "page": page,
            "per_page": limit
        ]
        let urlRequest = CareServiceAPI.announcements.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getListNotificationUnread(page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[NotificationModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "page": page,
            "per_page": limit
        ]
        let urlRequest = CareServiceAPI.announcementsUnread.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getNotificationDetail(announcementId: Int, completion: @escaping ((Result<BaseModel<NotificationModel>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.announcement(String(announcementId)).urlString
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func setNotificationRead(announcementId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "announcement_id": announcementId
        ]
        let urlRequest = CareServiceAPI.announcementRead.urlString
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setRateVideoCall(appointmentId: Int, rateDoctor: Int, rateAppointment: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "vote_doctor": rateDoctor,
            "vote_appointment": rateAppointment
        ]
        let urlRequest = CareServiceAPI.appointmentVote.urlString.replaceCharacter(target: ":id", withString: "\(appointmentId)")
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setJoinRoomVideoCall(appointmentId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.appointmentJoin.urlString.replaceCharacter(target: ":id", withString: "\(appointmentId)")
        self.service.POST(url: urlRequest, param: nil, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getAppointmentDetail(appointmentId: Int, completion: @escaping ((Result<BaseModel<AppointmentModel>, APIError>) -> Void)) {
        var urlRequest = CareServiceAPI.appointment(String(appointmentId)).urlString
        
        let include = "?include=customer,doctor,bank,doctor_available_time"
        urlRequest.append(contentsOf: include)
        
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func getVoucherDetail(voucherCode: String, completion: @escaping ((Result<BaseModel<VoucherModel>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.voucherShowByCode.urlString.replaceCharacter(target: ":code", withString: voucherCode)
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func getVoucherDetailDoctor(doctorId: Int, voucherCode: String, completion: @escaping ((Result<BaseModel<VoucherModel>, APIError>) -> Void)) {
        var urlRequest = CareServiceAPI.voucherShowByCodeDoctor.urlString
        urlRequest = urlRequest.replaceCharacter(target: ":doctorId", withString: "\(doctorId)")
        urlRequest = urlRequest.replaceCharacter(target: ":code", withString: voucherCode)
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func getUserTags(completion: @escaping ((Result<BaseModel<[UserTag]>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.userTags.urlString
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func getDoctorDetailById(doctorId: Int, completion: @escaping ((Result<BaseModel<DoctorModel>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.doctor("\(doctorId)").urlString
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
}
