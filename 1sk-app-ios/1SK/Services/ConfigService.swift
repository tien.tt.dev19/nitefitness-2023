//
//  ConfigService.swift
//  1SK
//
//  Created by tuyenvx on 18/01/2021.
//

import Foundation
import Alamofire

enum ApiType: String {
    case fitness
    case home
    case care
}

protocol ConfigServiceProtocol {
    func getListNotification(of userID: String,
                             page: Int,
                             limit: Int,
                             completion: @escaping ((Result<BaseModel<[NotificationModel]>, APIError>) -> Void))
    func updateNotificationStatus(of notificationId: String, completion: @escaping ((Result<BaseModel<NotificationModel>, APIError>) -> Void))
    func saveDeviceToken(_ token: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func getHomeFitnessWorkoutApp(type: ApiType, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[WorkoutModel]>, APIError>) -> Void))
    func getHomeFitnessVideoApp(type: ApiType, page: Int, catId: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    func getHomeFitnessBannerApp(completion: @escaping ((Result<BaseModel<[BannersModel]>, APIError>) -> Void))
    
    func getPageHomeBannersApp(completion: @escaping ((Result<BaseModel<[BannersModel]>, APIError>) -> Void))
    func getPageHomeBlogsApp(type: ApiType, tagId: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    func getHomeWorkoutApp(type: ApiType, completion: @escaping ((Result<BaseModel<[WorkoutModel]>, APIError>) -> Void))
    func getListBlogs(type: ApiType, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void))
    func getRecentWorkout(completion: @escaping ((Result<BaseModel<[WorkoutModel]>, APIError>) -> Void))
    
    func getListCategoriesChildren(categoryId: Int, completion: @escaping ((Result<BaseModel<[CategoryParentModel]>, APIError>) -> Void))
}

// MARK: ConfigServiceProtocol
class ConfigService: ConfigServiceProtocol {
    func getListCategoriesChildren(categoryId: Int, completion: @escaping ((Result<BaseModel<[CategoryParentModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "category_id[]": categoryId
        ]
        let urlRequest = ConfigServiceAPI.categories.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }

    private let service = BaseService.shared

    func getListNotification(of userID: String,
                             page: Int,
                             limit: Int,
                             completion: @escaping ((Result<BaseModel<[NotificationModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.userID: userID,
            SKKey.page: page,
            SKKey.limit: limit
        ]
        service.GET(url: ConfigServiceAPI.notify.urlString, param: param, completion: completion)
    }

    func updateNotificationStatus(of notificationId: String, completion: @escaping ((Result<BaseModel<NotificationModel>, APIError>) -> Void)) {
        let param = [
            SKKey.notifyID: notificationId
        ]
        service.POST(url: ConfigServiceAPI.updateNotifyStatus.urlString, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }

    func saveDeviceToken(_ token: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let param = [
            SKKey.deviceToken: token,
            SKKey.deviceType: "IOS"
        ]
        service.POST(url: ConfigServiceAPI.saveDeviceToken.urlString, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }

    func getHomeFitnessBannerApp(completion: @escaping ((Result<BaseModel<[BannersModel]>, APIError>) -> Void)) {
        service.GET(url: NewConfigServiceAPI.banner.urlString, param: ["page": "fitness"], completion: completion)
    }
    
    func getPageHomeBannersApp(completion: @escaping ((Result<BaseModel<[BannersModel]>, APIError>) -> Void)) {
        service.GET(url: NewConfigServiceAPI.slider.urlString, param: ["page": "home"], completion: completion)
    }
    
    func getHomeFitnessWorkoutApp(type: ApiType, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.page: page,
            SKKey.perpage: limit,
            "is_template": 1,
            "positions": type.rawValue
        ]
        self.service.GET(url: "\(WorkoutServiceAPI.listWorkout.urlString)", param: param, completion: completion)
    }
    
    func getListBlogs(type: ApiType, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.page: 1,
            SKKey.perpage: 10,
            "positions": type.rawValue
        ]
        service.GET(url: PortalServiceAPI.blog.urlString, param: param, completion: completion)
    }
    
    func getHomeWorkoutApp(type: ApiType, completion: @escaping ((Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.page: 1,
            SKKey.perpage: 2,
            "is_template": 1,
            "positions": type.rawValue
        ]
        let urlRequest = WorkoutServiceAPI.listWorkout.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
    
    func getHomeFitnessVideoApp(type: ApiType, page: Int, catId: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        var param: [String: Any] = [
            SKKey.page: page,
            SKKey.perpage: 10,
            "positions": type.rawValue
        ]
        
        if catId != -1 {
            param["category_id"] = catId
        }
        
        self.service.GET(url: MediaServiceAPI.video(nil).urlString, param: param, completion: completion)
    }
    
    func getRecentWorkout(completion: @escaping ((Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.page: 1,
            SKKey.perpage: 3
        ]
        self.service.GET(url: WorkoutServiceAPI.listRecentWorkout.urlString, param: param, completion: completion)
    }
    
    func getPageHomeBlogsApp(type: ApiType, tagId: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[BlogModel]>, APIError>) -> Void)) {
        var param: [String: Any] = [
            SKKey.page: 1,
            SKKey.perpage: perPage,
            "positions": type.rawValue
        ]
        if tagId != -1 {
            param["tag_id"] = tagId
        }
        
        service.GET(url: PortalServiceAPI.blog.urlString, param: param, completion: completion)
    }

}
