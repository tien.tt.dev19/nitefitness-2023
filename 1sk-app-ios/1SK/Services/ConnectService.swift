//
//  ConnectService.swift
//  1SK
//
//  Created by Thaad on 14/09/2022.
//

import Foundation
import Alamofire

protocol ConnectServiceProtocol {
    // MARK: BF Device
    func getDataScalesAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<BodyFatAnalysisModel>, APIError>) -> Void))
    func getDataScalesHistory(deviceName: String?, userId: Int, fromDate: String, toDate: String, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BodyFatModel]>, APIError>) -> Void))
    func getDataScalesRecent(userId: Int, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BodyFatModel]>, APIError>) -> Void))
    func setDataScalesRecord(listBodyFat: [BodyFatSDK], device: DeviceRealm?, accessToken: String?, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func getConsultIndexScales(index: String, status: String, gender: Gender, age: Int, weight: Double, completion: @escaping ((Result<BaseModel<ConsultModel>, APIError>) -> Void))
    func getListScalesName(accessToken: String?, completion: @escaping ((Result<BaseModel<[String]>, APIError>) -> Void))
    
    // MARK: BP Device
    func setDataBloodPressure(listBloodPressure: [BloodPressureModel], accessToken: String?, completion: @escaping ((Result<BaseModel<[BloodPressureModel]>, APIError>) -> Void))
    func getDataBloodPressure(deviceName: String?, userId: Int, fromDate: Int, toDate: Int, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BloodPressureModel]>, APIError>) -> Void))
    func getDataBloodPressureRecent(userId: Int, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BloodPressureModel]>, APIError>) -> Void))
    func getDataBloodPressureAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BpAnalysisModel]>, APIError>) -> Void))
    func getConsultIndexBp(valueCode: String, completion: @escaping ((Result<BaseModel<ConsultModel>, APIError>) -> Void))
    func getListBpName(accessToken: String?, completion: @escaping ((Result<BaseModel<[String]>, APIError>) -> Void))
}

// MARK: AuthServiceProtocol
class ConnectService: ConnectServiceProtocol {
    private let service = BaseService.shared
    
    // MARK: BF Device
    func getDataScalesAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<BodyFatAnalysisModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "time_type": type.value,
            "timestamp": time,
            "device_name": deviceName ?? ""
        ]
        let url = ConnectServiceAPI.connect_smart_scales_analysis.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func getDataScalesHistory(deviceName: String?, userId: Int, fromDate: String, toDate: String, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BodyFatModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "user_id": userId,
            "fromDate": fromDate,
            "toDate": toDate,
            "page": page,
            "perPage": perPage,
            "limit": perPage,
            "order_by": "created_at",
            "order_type": "DESC",
            "device": deviceName ?? ""
        ]
        
        let url = ConnectServiceAPI.connect_smart_scales.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func getDataScalesRecent(userId: Int, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BodyFatModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "user_id": userId,
            "page": page,
            "perPage": perPage,
            "limit": perPage,
            "order_by": "created_at",
            "order_type": "DESC",
            "device": ""
        ]
        
        let url = ConnectServiceAPI.connect_smart_scales.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func setDataScalesRecord(listBodyFat: [BodyFatSDK], device: DeviceRealm?, accessToken: String?, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        var listSmartScale: [[String: Any]] = []
        listBodyFat.forEach { bodyFat in
            let object = bodyFat.getDictionary(device: device)
            listSmartScale.append(object)
        }

        let params: [String: Any] = ["smart_scale": listSmartScale]

        let url = ConnectServiceAPI.connect_smart_scale.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.POST(url: url, param: params, headers: headers, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getConsultIndexScales(index: String, status: String, gender: Gender, age: Int, weight: Double, completion: @escaping ((Result<BaseModel<ConsultModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "index": index,
            "value_name": status,
            "gender": gender.param,
            "age": age,
            "weight": weight
        ]
        let url = ConnectServiceAPI.consult_smart_scales_content.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getListScalesName(accessToken: String?, completion: @escaping ((Result<BaseModel<[String]>, APIError>) -> Void)) {
        let url = ConnectServiceAPI.connect_smart_scales_list_device_name.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: nil, headers: headers, completion: completion)
    }
    
    // MARK: BP Device
    func setDataBloodPressure(listBloodPressure: [BloodPressureModel], accessToken: String?, completion: @escaping ((Result<BaseModel<[BloodPressureModel]>, APIError>) -> Void)) {
        var listData: [[String: Any]] = []
        
        listBloodPressure.forEach { bp in
            var data = [String: Any]()
            
            data["mac_address"] = bp.device?.mac ?? ""
            data["device_name"] = bp.device?.name ?? ""
            data["device_name_display"] = bp.device?.name_display ?? ""
            
            data["systolic"] = bp.systolic ?? 0
            data["diastolic"] = bp.diastolic ?? 0
            data["heart_rate"] = bp.heartRate ?? 0
            data["date_time"] = bp.dateTime ?? 0
            data["group"] = bp.group ?? 0
            data["status"] = bp.index?.value ?? 0
            data["status_description"] = bp.index?.desc ?? ""
            
            listData.append(data)
        }
        let params: [String: Any] = ["blood_pressures": listData]
        
        let url = ConnectServiceAPI.connect_blood_pressure.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.POST(url: url, param: params, headers: headers, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getDataBloodPressure(deviceName: String?, userId: Int, fromDate: Int, toDate: Int, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BloodPressureModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "device_name": deviceName ?? "",
            "fromDate": fromDate,
            "toDate": toDate,
            "page": page,
            "per_page": perPage
        ]
        let url = ConnectServiceAPI.connect_blood_pressures.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func getDataBloodPressureRecent(userId: Int, page: Int, perPage: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BloodPressureModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "device_name": "",
            "page": page,
            "per_page": perPage,
            "order_by": "created_at",
            "order_type": "DESC"
        ]
        let url = ConnectServiceAPI.connect_blood_pressures.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func getDataBloodPressureAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?, completion: @escaping ((Result<BaseModel<[BpAnalysisModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "device_name": deviceName ?? "",
            "time_type": type.value,
            "timestamp": time
        ]
        let url = ConnectServiceAPI.connect_blood_pressure_analysis.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func getConsultIndexBp(valueCode: String, completion: @escaping ((Result<BaseModel<ConsultModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "value_code": valueCode
        ]
        let url = ConnectServiceAPI.consult_blood_pressure_content.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getListBpName(accessToken: String?, completion: @escaping ((Result<BaseModel<[String]>, APIError>) -> Void)) {
        let url = ConnectServiceAPI.connect_blood_pressure_list_device_name.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: nil, headers: headers, completion: completion)
    }
}
