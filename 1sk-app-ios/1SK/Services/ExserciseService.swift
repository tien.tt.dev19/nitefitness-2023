//
//  ExserciseService.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//

import Foundation
import Alamofire

protocol ExerciseServiceProtocol {
    func getListSubject(completion: @escaping (Result<BaseModel<[SubjectModel]>, APIError>) -> Void)
    func getListType(completion: @escaping (Result<BaseModel<[TypeModel]>, APIError>) -> Void)
    func getListLevel(completion: @escaping (Result<BaseModel<[LevelModel]>, APIError>) -> Void)
    func createWorkout(name: String, time: Int, subjectID: Int, levelID: Int, typeID: Int, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void)
    func getListWorkout(userID: Int, page: Int, limit: Int, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)
    func getListWorkoutLibrary(filter: Filter, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)
    func updateWorkout(workout: WorkoutModel, detailAdd: WorkoutDetail?, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void)
    func deleteWorkout(workoutId: Int, completion: @escaping (Result<BaseModel<EmptyModel>, APIError>) -> Void)
    
    
    func getListExsercise(filter: Filter, completion: @escaping (Result<BaseModel<[ExerciseModel]>, APIError>) -> Void)
    
    func getFilterValueExerciseWorkour(completion: @escaping (Result<BaseModel<FilterDataValue>, APIError>) -> Void)
    
    func finishWorkoutSaveActivityUser(workout: WorkoutModel, completion: @escaping ((Result<BaseModel<ActivityUserModel>, APIError>) -> Void))
    
    func workoutRate(workout: WorkoutModel, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func bookmark(workoutId: Int, completion: @escaping ((Result<BaseModel<WorkoutModel>, APIError>) -> Void))
    func music(completion: @escaping ((Result<BaseModel<[MusicModel]>, APIError>) -> Void))
    
    func getWorkoutById(workoutId: Int, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void)
    
    func getListWorkoutSuggest(completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)
    
    func setCloneWorkout(workoutId: Int, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void)
    
    func getListWorkoutCategories(completion: @escaping (Result<BaseModel<[CategoriesWorkoutModel]>, APIError>) -> Void)
    func getCategoriesBanner(completion: @escaping ((Result<BaseModel<[BannersModel]>, APIError>) -> Void))
    
    func saveCategory(with id: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    
    func getListCategoriesWorkout(completion: @escaping (Result<BaseModel<[CategoriesWorkoutModel]>, APIError>) -> Void)
    
    func getListWorkoutCategoriesPurpose(category: CategoriesWorkoutModel, page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)
    
    func getListWorkoutSearch(keywords: String, page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void)
}

// MARK: ExerciseServiceProtocol
class ExerciseService: ExerciseServiceProtocol {
    
    
    
    let service = BaseService.shared

    func setCloneWorkout(workoutId: Int, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void) {
        let param = [
            "workout_id": workoutId
        ]
        self.service.POST(url: WorkoutServiceAPI.clone.urlString, param: param, encoding: JSONEncoding.default, completion: completion)
    }

    func getListWorkoutSuggest(completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void) {
        let param: [String: Any] = [String: Any]()
        self.service.GET(url: WorkoutServiceAPI.suggest.urlString, param: param, completion: completion)
    }

    func music(completion: @escaping ((Result<BaseModel<[MusicModel]>, APIError>) -> Void)) {
        let param = [
            SKKey.page: 1,
            SKKey.perpage: 100
        ]
        self.service.GET(url: NewExserciseServiceAPI.music.urlString, param: param, completion: completion)
    }

    func getListSubject(completion: @escaping (Result<BaseModel<[SubjectModel]>, APIError>) -> Void) {
        let param = [
            SKKey.page: 1,
            SKKey.perpage: 1000
        ]
        service.GET(url: SubjectServiceAPI.subject(nil).urlString, param: param, completion: completion)
    }

    func getListType(completion: @escaping (Result<BaseModel<[TypeModel]>, APIError>) -> Void) {
        let param = [
            SKKey.page: 1,
            SKKey.perpage: 1000
        ]
        service.GET(url: TyoeServiceAPI.type(nil).urlString, param: param, completion: completion)
    }

    func getListLevel(completion: @escaping (Result<BaseModel<[LevelModel]>, APIError>) -> Void) {
        let param = [
            SKKey.page: 1,
            SKKey.perpage: 1000
        ]
        service.GET(url: LevelServiceAPI.level(nil).urlString, param: param, completion: completion)
    }

    func createWorkout(name: String, time: Int, subjectID: Int, levelID: Int, typeID: Int, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void) {
        let param: [String: Any] = [
            SKKey.name: name,
            SKKey.time: time,
            SKKey.status: 1,
            SKKey.subjectID: subjectID,
            SKKey.levelID: levelID,
            SKKey.typeID: typeID
        ]
        service.POST(url: WorkoutServiceAPI.createWorkout(nil).urlString, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }

    func getListWorkout(userID: Int, page: Int, limit: Int, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void) {
        let param: [String: Any] = [
            SKKey.userID: userID,
            SKKey.page: page,
            SKKey.perpage: 15
        ]
        service.GET(url: WorkoutServiceAPI.listWorkout.urlString, param: param, completion: completion)
    }
    
    func updateWorkout(workout: WorkoutModel, detailAdd: WorkoutDetail?, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void) {
        var details: [[String: Any]] = []
        if let workoutDetails = workout.details {
            for detail in workoutDetails {
                if let exercise = detail.exercise {
                    var obj: [String: Any] = [
                        "exercise_id": exercise.id ?? "",
                        "time": detail.time ?? 0,
                        "order": detail.order ?? 0
                    ]
                    if detail.id ?? 0 > 0 {
                        obj.updateValue(detail.id ?? 0, forKey: "id")
                    }
                    details.append(obj)
                }
            }
        }
        if let detail = detailAdd {
            let objAdd: [String: Any] = [
                "exercise_id": detail.excercise!.id!,
                "time": detail.excercise!.time!,
                "order": detail.order!
            ]
            details.append(objAdd)
        }
        let param: [String: Any] = [
            SKKey.name: workout.name ?? "",
            SKKey.time: workout.time ?? "",
            SKKey.status: workout.status ?? "",
            SKKey.subjectID: workout.subject!.id,
            SKKey.levelID: workout.level!.id,
            SKKey.typeID: workout.type!.id,
            SKKey.details: details
        ]
        service.PUT(url: WorkoutServiceAPI.createWorkout(workout.id ?? -1).urlString, param: param, encoding: JSONEncoding.default, completion: completion)
    }

    func deleteWorkout(workoutId: Int, completion: @escaping (Result<BaseModel<EmptyModel>, APIError>) -> Void) {
        let param: [String: Any] = [:]
        service.DELETE(url: WorkoutServiceAPI.createWorkout(workoutId).urlString, param: param, completion: completion)
    }

    func getListExsercise(filter: Filter, completion: @escaping (Result<BaseModel<[ExerciseModel]>, APIError>) -> Void) {
        var paramString = "?keywords=\(filter.keyword ?? "")&per_page=\(filter.limit!)&page=\(filter.page!)"
        for tool in filter.tool {
            paramString.append("&tools[]=\(tool)")
        }
        for subject in filter.subject {
            paramString.append("&subjects[]=\(subject)")
        }
        for filter in filter.filter {
            paramString.append("&filters[]=\(filter)")
        }
        for type in filter.type {
            paramString.append("&types[]=\(type)")
        }
        service.GET(url: "\(NewExserciseServiceAPI.exercise.urlString)\(paramString)", param: [String: Any](), completion: completion)
    }

    func getFilterValueExerciseWorkour(completion: @escaping (Result<BaseModel<FilterDataValue>, APIError>) -> Void) {
        let param: [String: Any] = [:]
        self.service.GET(url: WorkoutServiceAPI.filterWorkout.urlString, param: param, completion: completion)
    }

    func finishWorkoutSaveActivityUser(workout: WorkoutModel, completion: @escaping ((Result<BaseModel<ActivityUserModel>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "type": workout.subject!.type!,
            "activity_subject_id": workout.subject!.id,
            "way_length": 0,
            "speed": 0,
            "total_time": workout.timePractice!,
            "start_time": workout.timeStart!,
            "address": "",
            "average_heart_rate": 0,
            "calories_burned": 0,
            "description": workout.name ?? "",
            "image_url": [],
            "permission": 1
        ]
        self.service.POST(url: ActivitiesServiceAPI.activityUser(nil).urlString, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }

    func workoutRate(workout: WorkoutModel, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {

        var histories: [[String: Any]] = []
        if let details = workout.details {
            for item in details {
                let exercise: [String: Any] = [
                    "workout_exercise_id": item.exercise!.id!,
                    "time": item.time ?? 0,
                    "status": item.exercise!.status!
                ]
                histories.append(exercise)
            }
        }

        let param: [String: Any] = [
            "workout_id": workout.id ?? -1,
            "rate": workout.rate!,
            "histories": histories
        ]
        
        self.service.POST(url: WorkoutServiceAPI.rate.urlString, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getListWorkoutLibrary(filter: Filter, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void) {
        var paramString = "?keywords=\(filter.keyword ?? "")&per_page=\(filter.limit!)&page=\(filter.page!)&is_template=1"
        for tool in filter.tool {
            paramString.append("&tools[]=\(tool)")
        }
        for subject in filter.subject {
            paramString.append("&subjects[]=\(subject)")
        }
        for filter in filter.filter {
            paramString.append("&filters[]=\(filter)")
        }
        for type in filter.type {
            paramString.append("&types[]=\(type)")
        }

        let urlRequest = "\(WorkoutServiceAPI.listWorkout.urlString)\(paramString)"
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func bookmark(workoutId: Int, completion: @escaping ((Result<BaseModel<WorkoutModel>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "workout_id": workoutId
        ]
        self.service.POST(url: WorkoutServiceAPI.bookMark.urlString, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getWorkoutById(workoutId: Int, completion: @escaping (Result<BaseModel<WorkoutModel>, APIError>) -> Void) {
        let param: [String: Any] = [:]
        self.service.GET(url: WorkoutServiceAPI.workout(workoutId).urlString, param: param, completion: completion)
    }
    
    func saveCategory(with id: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let param = ["workout_id": id]
        self.service.POST(url: WorkoutServiceAPI.categoryTarget.urlString, param: param, encoding: JSONEncoding.default, completion: completion)
    }
    
    func getCategoriesBanner(completion: @escaping ((Result<BaseModel<[BannersModel]>, APIError>) -> Void)) {
        self.service.GET(url: NewConfigServiceAPI.banner.urlString, param: ["page": "target"], completion: completion)
    }
    
    func getListWorkoutCategories(completion: @escaping (Result<BaseModel<[CategoriesWorkoutModel]>, APIError>) -> Void) {
        let params: [String: Any] = [
            "order": ""
        ]
        let urlRequest = WorkoutServiceAPI.listCategory.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getListCategoriesWorkout(completion: @escaping (Result<BaseModel<[CategoriesWorkoutModel]>, APIError>) -> Void) {
        self.service.GET(url: WorkoutServiceAPI.listCategory.urlString, param: nil, completion: completion)
    }
    
    func getListWorkoutCategoriesPurpose(category: CategoriesWorkoutModel, page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void) {
        let params: [String: Any] = [
            "order": "created_at",
            "sort": "DESC",
            "user_id": 0,
            "keywords": "",
            "subjects[]": "",
            "is_template": 1,
            "per_page": perPage,
            "page": page,
            "positions": "",
            "categories[]": category.id!
        ]
        let urlRequest = WorkoutServiceAPI.listWorkout.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
    
    func getListWorkoutSearch(keywords: String, page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[WorkoutModel]>, APIError>) -> Void) {
        let params: [String: Any] = [
            "order": "created_at",
            "sort": "DESC",
            "user_id": 0,
            "positions": "",
            "subjects[]": "",
            "is_template": 1,
            "categories[]": 0,
            "per_page": perPage,
            "page": page,
            "keywords": keywords
        ]
        let urlRequest = WorkoutServiceAPI.listWorkout.urlString
        self.service.GET(url: urlRequest, param: params, completion: completion)
    }
}
