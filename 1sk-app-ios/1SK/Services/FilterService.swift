//
//  FilterService.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//

import Foundation
import Alamofire

protocol FilterServiceProtocol {
    func getFilterValueExerciseWorkour(completion: @escaping (Result<BaseModel<[FilterModel]>, APIError>) -> Void)
    
}

class FilterService: FilterServiceProtocol {
    let service = BaseService.shared
    func getFilterValueExerciseWorkour(completion: @escaping (Result<BaseModel<[FilterModel]>, APIError>) -> Void) {
        let param = [
            SKKey.page: 1,
            SKKey.limit: 1000
        ]
        self.service.get(url: ExserciseServiceAPI.filter(nil), param: param, completion: completion)
    }
    
}
