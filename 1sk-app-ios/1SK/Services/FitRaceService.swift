//
//  VisualRaceService.swift
//  1SK
//
//  Created by Valerian on 24/06/2022.
//

import Foundation
import Alamofire

protocol FitRaceSeviceProtocol: AnyObject {
    func onGetListVisualRace(page: Int, perPage: Int, filterType: TypeRaceFilter, completion: @escaping ((Result<BaseModel<[RaceModel]>, APIError>) -> Void))
    func onGetVirtualRaceDetail(id: Int, completion: @escaping ((Result<BaseModel<RaceModel>, APIError>) -> Void))
    func onGetVirtualRaceDetail(by slug: String, completion: @escaping ((Result<BaseModel<RaceModel>, APIError>) -> Void))
    func onGetVirtualRaceLeaderboard(raceId: Int, customerId: Int?, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[LeaderboardModel]>, APIError>) -> Void))
    func onConnectStravaCode(code: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func onDisConnectStrava(completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func onRegister(register: RegisterParam, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func onIdentifyProviders(completion: @escaping ((Result<BaseModel<[StravaModel]>, APIError>) -> Void))
    func onGetMeIdentifyProvider(completion: @escaping ((Result<BaseModel<[StravaModel]>, APIError>) -> Void))
    func onGetAthleteActivities(per_page: Int, vr_sport_id: Int, before: String, after: String, completion: @escaping ((Result<BaseModel<[StravaAthleteActivityModel]>, APIError>) -> Void))
    func onGetListVisualRaceRegistered(page: Int, perPage: Int, filterType: TypeRaceFilter, completion: @escaping ((Result<BaseModel<[RaceModel]>, APIError>) -> Void))
    func onGetVRSports(completion: @escaping ((Result<BaseModel<[VRSportModel]>, APIError>) -> Void))
    func onGetStravaActivitiesTotal(vrSportId: Int, timeBefore: String, timeAfter: String, completion: @escaping ((Result<BaseModel<ActivitiesTotal>, APIError>) -> Void))
}

//MARK: - VisualRaceSeviceProtocol
class FitRaceService: FitRaceSeviceProtocol {
    private let service = BaseService.shared

    func onGetListVisualRace(page: Int, perPage: Int, filterType: TypeRaceFilter, completion: @escaping ((Result<BaseModel<[RaceModel]>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.visualRaceList.urlString

        var param: [String: Any] = [
            SKKey.page: page,
            SKKey.perpage: perPage
        ]

        if filterType != .registered {
            param["state_of_happening"] = filterType.state
        }

        self.service.GET(url: url, param: param, completion: completion)
    }
    
    func onGetListVisualRaceRegistered(page: Int, perPage: Int, filterType: TypeRaceFilter, completion: @escaping ((Result<BaseModel<[RaceModel]>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.registered.urlString
        
        var param: [String: Any] = [
            SKKey.page: page,
            SKKey.perpage: perPage
        ]
        if filterType == .all {
            param.updateValue("last_published_at", forKey: "order_by")
            param.updateValue("DESC", forKey: "order_type")
        }
        
        self.service.GET(url: url, param: param, completion: completion)
    }
    
    func onRegister(register: RegisterParam, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.register.urlString
        self.service.POST(url: url, param: register.toDictionary(), completion: completion)
    }
    
    func onGetVirtualRaceDetail(id: Int, completion: @escaping ((Result<BaseModel<RaceModel>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.virtualRaceDetail(id).urlString
        let param: [String: Any] = [
            "include": "awards,goals,banners"
        ]
        self.service.GET(url: url, param: param, completion: completion)
    }
    
    func onGetVirtualRaceDetail(by slug: String, completion: @escaping ((Result<BaseModel<RaceModel>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.visualRaceList.urlString

        let param: [String: Any] = [
            "include": "awards,goals,banners",
            "slug": slug
        ]

        self.service.GET(url: url, param: param, completion: completion)
    }
    
    func onGetVirtualRaceLeaderboard(raceId: Int, customerId: Int?, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[LeaderboardModel]>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.stravaRanking.urlString

        var param: [String: Any] = [
            "virtual_race_id": raceId,
            "page": page,
            "per_page": perPage
        ]
        
        if let _customerId = customerId {
            param.updateValue(_customerId, forKey: "customer_id")
        }

        self.service.GET(url: url, param: param, completion: completion)
    }
    
    func onConnectStravaCode(code: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.connectStrava.urlString
        
        let param: [String: Any] = [
            "code": code
        ]
        
        self.service.POST(url: url, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func onDisConnectStrava(completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let url = AuthServiceAPI.disconnectStrava.urlString
        self.service.POST(url: url, param: nil, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func onIdentifyProviders(completion: @escaping ((Result<BaseModel<[StravaModel]>, APIError>) -> Void)) {
        let url = AuthServiceAPI.identify_providers_strava.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func onGetMeIdentifyProvider(completion: @escaping ((Result<BaseModel<[StravaModel]>, APIError>) -> Void)) {
        let url = AuthServiceAPI.me_identify_providers.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func onGetAthleteActivities(per_page: Int, vr_sport_id: Int, before: String, after: String, completion: @escaping ((Result<BaseModel<[StravaAthleteActivityModel]>, APIError>) -> Void)) {
        
        let url = FitRaceServiceAPI.athleteActivity.urlString

        let param: [String: Any] = [
            "per_page": per_page,
            "vr_sport_id": vr_sport_id,
            "before": before,
            "after": after
        ]
        
        self.service.GET(url: url, param: param, completion: completion)
    }
    
    func onGetVRSports(completion: @escaping ((Result<BaseModel<[VRSportModel]>, APIError>) -> Void)) {
        let url = FitRaceServiceAPI.vr_sport.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func onGetStravaActivitiesTotal(vrSportId: Int, timeBefore: String, timeAfter: String, completion: @escaping ((Result<BaseModel<ActivitiesTotal>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "vr_sport_id": vrSportId,
            "before": timeBefore,
            "after": timeAfter
        ]
        let url = FitRaceServiceAPI.strava_activities_total.urlString
        self.service.GET(url: url, param: param, completion: completion)
    }
}
