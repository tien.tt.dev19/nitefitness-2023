//
//  FitService.swift
//  1SK
//
//  Created by Valerian on 19/04/2022.
//

import Foundation
import Alamofire

protocol FitServiceProtocol {
    func getListWorkoutEquipments(perPage: Int, completion: @escaping ((Result<BaseModel<[EquipmentModel]>, APIError>) -> Void))
    func getListExercises(perPage: Int, equipmentId: Int, completion: @escaping ((Result<BaseModel<[ExerciseFitModel]>, APIError>) -> Void))
    func getExerciseDetail(with id: Int, completion: @escaping ((Result<BaseModel<ExerciseFitModel>, APIError>) -> Void))
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], completion: @escaping ((Result<BaseModel<[IndoorBikeListData]>, APIError>) -> Void))
}

// MARK: FitServiceProtocol
class FitService: FitServiceProtocol {
    private let service = BaseService.shared

    func getListWorkoutEquipments(perPage: Int, completion: @escaping ((Result<BaseModel<[EquipmentModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "per_page": perPage
        ]
        let url = FitServiceAPI.workoutEquipments.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getListExercises(perPage: Int, equipmentId: Int, completion: @escaping ((Result<BaseModel<[ExerciseFitModel]>, APIError>) -> Void)) {
        
        let params: [String: Any] = [
            "per_page": perPage,
            "equipment_id": equipmentId,
            "order": "desc"
        ]
        
        let url = FitServiceAPI.exercises.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getExerciseDetail(with id: Int, completion: @escaping ((Result<BaseModel<ExerciseFitModel>, APIError>) -> Void)) {
        let url = FitServiceAPI.exercise_detail.urlString + "/\(id)"
        self.service.GET(url: url, param: nil, completion: completion)
        
    }
    
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], completion: @escaping ((Result<BaseModel<[IndoorBikeListData]>, APIError>) -> Void)) {
        let url = FitServiceAPI.customerWorkout.urlString
        
        let params: [String: String] = [
            "client_id": sectionId,
            "start_time": startTimeSection.asString,
            "end_time": endTimeSection.asString,
            "workout_equipment_id": "1" // IndoorBike Type
        ]
        
        print("./UPLOAD: setUploadLogFileBikeWokout - sectionId:", sectionId)
        
        var listData: [[String: Any]] = []
        for path in filePaths {
            let fileName = (path as NSString).lastPathComponent
            
            if let binaryData = FileHelper.shared.getBinaryFileContent(for: path) {
                let data: [String: Data] = [fileName: binaryData]
                listData.append(data)
            }
            
            print("./UPLOAD: setUploadLogFileBikeWokout - path:", path)
        }
        
        self.service.UPLOAD_BINARY_FILE(url: url, paramsText: params, listData: listData, completion: completion)
    }
}
