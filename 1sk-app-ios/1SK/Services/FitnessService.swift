//
//  FitnessService.swift
//  1SK
//
//  Created by tuyenvx on 13/01/2021.
//

import Foundation
import Alamofire

protocol FitnessServiceProtocol {
    func getFilterAttribute(completion: @escaping ((Result<BaseModel<FilterDataValue>, APIError>) -> Void))
    func getListCategory(completion: @escaping ((Result<BaseModel<[ActivitySubjectModel]>, APIError>) -> Void))
    func getListCoach(completion: @escaping ((Result<BaseModel<[CoachModel]>, APIError>) -> Void))
    func getListVideo(page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    func getListVideoByCategory(with id: Int, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    func getListRelatedVideo(with id: Int, categories: [CategoriesVideo], completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    func getVideoDetails(with slug: String, completion: @escaping ((Result<BaseModel<VideoModel>, APIError>) -> Void))
    func getListVideo(categoryId: Int, tagsId: String?, keywords: String?, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    
    func getVideoDetailByID(with videoId: Int, completion: @escaping ((Result<BaseModel<VideoModel>, APIError>) -> Void))
    
    func setSeeVideo(videoId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func getListVideoUserHistory(completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    
    func getListVideosSearch(categoryId: Int, keywords: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    func getListVideosTags(categoryId: Int, tag: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void))
    func getVideoBySlug(slug: String, type: String, completion: @escaping ((Result<BaseModel<VideoModel>, APIError>) -> Void))
}

// MARK: FitnessServiceProtocol
class FitnessService: FitnessServiceProtocol {
    
    let service = BaseService.shared

    func getFilterAttribute(completion: @escaping ((Result<BaseModel<FilterDataValue>, APIError>) -> Void)) {
        service.GET(url: MediaServiceAPI.attribute.urlString, param: nil, completion: completion)
    }

    func getListCategory(completion: @escaping ((Result<BaseModel<[ActivitySubjectModel]>, APIError>) -> Void)) {
        let urlRequest = MediaServiceAPI.category.urlString
        self.service.GET(url: urlRequest, param: [String: Any](), completion: completion)
    }

    func getListCoach(completion: @escaping ((Result<BaseModel<[CoachModel]>, APIError>) -> Void)) {
        service.get(url: FitnessServiceAPI.coach(nil), param: nil, completion: completion)
    }

    func getListVideo(page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        let param: [String: Int] = [
            SKKey.page: page,
            SKKey.limit: limit
        ]
        service.get(url: FitnessServiceAPI.video(nil), param: param, completion: completion)
    }

    func getListVideoByCategory(with id: Int, page: Int, limit: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            SKKey.categoryID: id,
            SKKey.page: page,
            SKKey.perpage: limit
        ]
        self.service.GET(url: MediaServiceAPI.video(nil).urlString, param: param, completion: completion)
    }

    func getListVideo(categoryId: Int, tagsId: String?, keywords: String?, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        
        var param: [String: Any] = [
            "category_id": categoryId,
            "page": page,
            "per_page": perPage
        ]
        if let `tagsId` = tagsId {
            param.updateValue(tagsId, forKey: "tag")
        }
        if let `keywords` = keywords {
            param.updateValue(keywords, forKey: "keywords")
        }
        
        let urlRequest = MediaServiceAPI.video(nil).urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }

    func getListRelatedVideo(with id: Int, categories: [CategoriesVideo], completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        var urlRequest = MediaServiceAPI.relatedVideo.urlString
        urlRequest.append(contentsOf: "?video_id=\(id)")
        urlRequest.append(contentsOf: "&type=related")
        
        for category in categories {
            if let id = category.id {
                urlRequest.append(contentsOf: "&category_id[]=\(id)")
            }
        }
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }

    func getVideoDetails(with slug: String, completion: @escaping ((Result<BaseModel<VideoModel>, APIError>) -> Void)) {
        let param = [
            SKKey.slug: slug
        ]
        self.service.GET(url: MediaServiceAPI.video(nil).urlString, param: param, completion: completion)
    }
    
    func getVideoDetailByID(with videoId: Int, completion: @escaping ((Result<BaseModel<VideoModel>, APIError>) -> Void)) {
        let param: [String: Any] = [:]
        self.service.GET(url: MediaServiceAPI.detailVideo(videoId).urlString, param: param, completion: completion)
    }
    
    func setSeeVideo(videoId: Int, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "video_id": videoId
        ]
        let urlRequest = MediaServiceAPI.videoUserHistory.urlString
        self.service.POST(url: urlRequest, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getListVideoUserHistory(completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "category_id": CategoryType.videoFit.rawValue
        ]
        let urlRequest = MediaServiceAPI.videoUserHistory.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
    
    func getListVideosSearch(categoryId: Int, keywords: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "category_id": categoryId,
            "keywords": keywords,
            "page": page,
            "per_page": perPage
        ]
        let urlRequest = MediaServiceAPI.videos.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
    
    func getListVideosTags(categoryId: Int, tag: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[VideoModel]>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "category_id": categoryId,
            "tag": tag,
            "page": page,
            "per_page": perPage
        ]
        let urlRequest = MediaServiceAPI.videos.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
    
    func getVideoBySlug(slug: String, type: String, completion: @escaping ((Result<BaseModel<VideoModel>, APIError>) -> Void)) {
        let param: [String: Any] = [
            "slug": slug,
            "type": type
        ]
        
        let urlRequest = MediaServiceAPI.videos.urlString
        self.service.GET(url: urlRequest, param: param, completion: completion)
    }
}

// MARK: Dictionary
extension Dictionary {
    static func += (lhs: inout Self, rhs: Self) {
        lhs.merge(rhs) { _, new in new }
    }
    static func +=<S: Sequence>(lhs: inout Self, rhs: S) where S.Element == (Key, Value) {
        lhs.merge(rhs) { _, new in new }
    }
}
