//
//  JumpRopeService.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//

import Foundation
import Alamofire

// MARK: JumpRopeServiceProtocol
protocol JumpRopeServiceProtocol {
    func setSyncDataICSkipJumpRope(accessToken: String?, icSkipDataRealm: ICSkipDataRealm, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
    func getUserStatistic(accessToken: String?, completion: @escaping ((Result<BaseModel<UserStatisticModel>, APIError>) -> Void))
    func getUserRanking(accessToken: String?, timeFilter: TimeFilter, completion: @escaping ((Result<BaseModel<[UserRankingModel]>, APIError>) -> Void))
    func getUserAchievementToday(accessToken: String?, completion: @escaping ((Result<BaseModel<UserAchievementModel>, APIError>) -> Void))
}

// MARK: JumpRopeService
class JumpRopeService: JumpRopeServiceProtocol {
    private let service = BaseService.shared
    
    func setSyncDataICSkipJumpRope(accessToken: String?, icSkipDataRealm: ICSkipDataRealm, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let params = icSkipDataRealm.toDictionary()
        let url = JumpRopeServiceAPI.store_record.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.POST(url: url, param: params, headers: headers, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getUserStatistic(accessToken: String?, completion: @escaping ((Result<BaseModel<UserStatisticModel>, APIError>) -> Void)) {
        let url = JumpRopeServiceAPI.user_statistic.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: nil, headers: headers, completion: completion)
    }
    
    func getUserRanking(accessToken: String?, timeFilter: TimeFilter, completion: @escaping ((Result<BaseModel<[UserRankingModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "order_by_time": timeFilter.value // 1-today, 2-week, 3-month
        ]
        
        let url = JumpRopeServiceAPI.ranking.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: params, headers: headers, completion: completion)
    }
    
    func getUserAchievementToday(accessToken: String?, completion: @escaping ((Result<BaseModel<UserAchievementModel>, APIError>) -> Void)) {
        let url = JumpRopeServiceAPI.user_achievement.urlString
        let headers = self.service.headers(accessToken: accessToken)
        self.service.GET(url: url, param: nil, headers: headers, completion: completion)
    }
}
