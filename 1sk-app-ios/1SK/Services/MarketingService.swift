//
//  MarketingService.swift
//  1SK
//
//  Created by Thaad on 09/02/2022.
//

import Foundation
import Alamofire

protocol MarketingServiceProtocol {
    func getPopupList(completion: @escaping ((Result<BaseModel<[PopupModel]>, APIError>) -> Void))
    func setPopupReadById(popupId: Int, completion: @escaping ((Result<BaseModel<PopupModel>, APIError>) -> Void))
    func setPopupReadByListId(listPopup: [PopupModel], completion: @escaping ((Result<BaseModel<PopupModel>, APIError>) -> Void))
}

// MARK: MarketingServiceProtocol
class MarketingService: MarketingServiceProtocol {
    private let service = BaseService.shared
    
    func getPopupList(completion: @escaping ((Result<BaseModel<[PopupModel]>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.getPopups.urlString
        self.service.GET(url: urlRequest, param: nil, completion: completion)
    }
    
    func setPopupReadById(popupId: Int, completion: @escaping ((Result<BaseModel<PopupModel>, APIError>) -> Void)) {
        let urlRequest = CareServiceAPI.markAsRead.urlString.replaceCharacter(target: ":id", withString: "\(popupId)")
        self.service.PUT(url: urlRequest, param: nil, encoding: JSONEncoding.default, completion: completion)
    }
    
    func setPopupReadByListId(listPopup: [PopupModel], completion: @escaping ((Result<BaseModel<PopupModel>, APIError>) -> Void)) {
        var listId: [Int] = []
        for item in listPopup {
            if let id = item.id {
                listId.append(id)
            }
            
//            listId = "\(listId),\(item.id ?? 0)"
        }
//        listId.removeFirst()
        
        let param = [
            "popup_ids": listId
        ]
        let url = CareServiceAPI.markAsReceived.urlString
        self.service.POST(url: url, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
}
