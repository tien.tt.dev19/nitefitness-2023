//
//  StoreService.swift
//  1SK
//
//  Created by Thaad on 14/03/2022.
//

import Foundation
import Alamofire

protocol StoreServiceProtocol {
    func getSliderBanner(typeSlider: SliderType, completion: @escaping ((Result<BaseModel<SliderBannerModel>, APIError>) -> Void))
    func getListProductSuggest(page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ProductModel]>, APIError>) -> Void))
    func getListProductFeatured(type: TypeCategoryStore, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ProductModel]>, APIError>) -> Void))
    func getProductDetail(id: Int, completion: @escaping ((Result<BaseModel<ProductModel>, APIError>) -> Void))
    func getListReviewProduct(id: Int, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ReviewModel]>, APIError>) -> Void))
    func getListCountries(completion: @escaping ((Result<BaseModel<[CountryModel]>, APIError>) -> Void))
    func getFilterAttribute(completion: @escaping ((Result<BaseModel<[FilterStoreModel]>, APIError>) -> Void))
    func getListProduct(category: TypeCategoryStore, filters: [FilterStoreModel], page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[ProductModel]>, APIError>) -> Void)
    func getListProduct(by keyword: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ProductModel]>, APIError>) -> Void))
    func setCreateDeliveryAddress(delivery: DeliveryModel, completion: @escaping ((Result<BaseModel<DeliveryModel>, APIError>) -> Void))
    func setUpdateDeliveryAddress(delivery: DeliveryModel, completion: @escaping ((Result<BaseModel<DeliveryModel>, APIError>) -> Void))
    func getDeliveryAddress(completion: @escaping ((Result<BaseModel<DeliveryModel>, APIError>) -> Void))
    func getBankStoreInfo(completion: @escaping ((Result<BaseModel<[BankStoreModel]>, APIError>) -> Void))
    func getListHistorySeach(completion: @escaping (Result<BaseModel<[String]>, APIError>) -> Void)
    func setDeleteSearchHistory(by keyword: String, completion: @escaping (Result<BaseModel<EmptyModel>, APIError>) -> Void)
    func setCreateOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], orderCode: String, paymentMethod: String?, couponCode: String?, desc: String?, completion: @escaping ((Result<BaseModel<OrderModel>, APIError>) -> Void))
    func setVoucherOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], paymentMethod: String?, couponCode: String?, desc: String?, completion: @escaping ((Result<BaseModel<OrderModel>, APIError>) -> Void))
    func getListOrder(with status: OrderStatus, page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[OrderModel]>, APIError>) -> Void)
    func getOrderDetail(orderId: Int, completion: @escaping (Result<BaseModel<OrderModel>, APIError>) -> Void)
    func setCancelOrder(orderId: Int, completion: @escaping (Result<BaseModel<OrderModel>, APIError>) -> Void)
    func getListOrderStatus(completion: @escaping (Result<BaseModel<[OrderStatusModel]>, APIError>) -> Void)
    func setReviewProduct(itemOrder: ItemOrder, orderId: Int?, completion: @escaping ((Result<BaseModel<ItemOrder>, APIError>) -> Void))
    func getMyReviewProduct(listProductId: [Int], orderId: Int?, completion: @escaping (Result<BaseModel<[ProductOrderModel]>, APIError>) -> Void)
    func getFlashSaleProducts(completion: @escaping (Result<BaseModel<[FlashSaleModel]>, APIError>) -> Void)
    func postRegisterProduct(name: String, phone: String, email: String, productId: String, completion: @escaping (Result<BaseModel<EmptyModel>, APIError>) -> Void)
    func getVariationProduct(id: Int, attributeIDs: [Int], completion: @escaping (Result<BaseModel<ProductModel>, APIError>) -> Void)
    func getAllFlashSaleProduct(page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[Product]>, APIError>) -> Void))
    func getHotProduct(completion: @escaping ((Result<BaseModel<[Product]>, APIError>) -> Void))
    func getStrategiesProduct(completion: @escaping ((Result<BaseModel<[StrategiesModel]>, APIError>) -> Void))
}

// MARK: StoreServiceProtocol
class StoreService: StoreServiceProtocol {
    private let service = BaseService.shared
    
    func getSliderBanner(typeSlider: SliderType, completion: @escaping ((Result<BaseModel<SliderBannerModel>, APIError>) -> Void)) {
        let url = StoreServiceAPI.slider(typeSlider.rawValue).urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func getListProductSuggest(page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ProductModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "suggest": "true",
            "page": page,
            "per_page": perPage
        ]
        
        let url = StoreServiceAPI.products.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getListProductFeatured(type: TypeCategoryStore, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ProductModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "featured": "true",
            "category_id": type.id,
            "page": page,
            "per_page": perPage
        ]
        
        let url = StoreServiceAPI.products.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getProductDetail(id: Int, completion: @escaping ((Result<BaseModel<ProductModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "include": "promotions,full_attributes,flash_sale"
        ]
        let url = StoreServiceAPI.product("\(id)").urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getListReviewProduct(id: Int, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ReviewModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "include": "customer",
            "page": page,
            "per_page": perPage
        ]
        let url = StoreServiceAPI.productReviews.urlString.replaceCharacter(target: ":id", withString: "\(id)")
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getListCountries(completion: @escaping ((Result<BaseModel<[CountryModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "include": "states.cities.districts"
        ]
        let url = StoreServiceAPI.countries.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }

    func getFilterAttribute(completion: @escaping ((Result<BaseModel<[FilterStoreModel]>, APIError>) -> Void)) {
        let url = StoreServiceAPI.filterAttribute.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func getListProduct(category: TypeCategoryStore, filters: [FilterStoreModel], page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[ProductModel]>, APIError>) -> Void) {
        
        var params = ""
        params.append("?category_id=\(category.id)")
        params.append("&page=\(page)")
        params.append("&per_page=\(perPage)")
        
        for filter in filters {
            if let attributes = filter.attributes {
                for attr in attributes {
                    if attr.isSelected == true {
                        switch attr.type {
                        case .text:
                            if let id = attr.id {
                                params.append("&attribute_ids[]=\(id)")
                            }
                            
                        case .price:
                            if let toPrice = attr.toPrice, let fromPrice = attr.fromPrice {
                                params.append("&price_range[]=\(fromPrice),\(toPrice)")
                            }
                            
                        default:
                            if let id = attr.id {
                                params.append("&attribute_ids[]=\(id)")
                            }
                        }
                    }
                }
            }
        }
        
        let url = StoreServiceAPI.products.urlString + params
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func getListHistorySeach(completion: @escaping (Result<BaseModel<[String]>, APIError>) -> Void) {
        let params: [String: Any] = [
            "order[created_at]": "desc",
            "per_page": 5,
            "page": 1
        ]
        
        let url = StoreServiceAPI.storeSearchHistory.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setDeleteSearchHistory(by keyword: String, completion: @escaping (Result<BaseModel<EmptyModel>, APIError>) -> Void) {
        let url = StoreServiceAPI.storeSearchDeleteHistory.urlString
        
        let params: [String: Any] = [
            "keywords": keyword
        ]
        
        self.service.DELETE(url: url, param: params, completion: completion)
    }
    
    func getListProduct(by keyword: String, page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[ProductModel]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "keywords": keyword,
            "page": page,
            "per_page": perPage
        ]
        
        let url = StoreServiceAPI.products.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setCreateDeliveryAddress(delivery: DeliveryModel, completion: @escaping ((Result<BaseModel<DeliveryModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "name": delivery.name ?? "",
            "phone": delivery.phone ?? "",
            "email": delivery.email ?? "",
            "country": delivery.country?.id ?? 0,
            "state": delivery.state?.id ?? 0,
            "city": delivery.city?.id ?? 0,
            "district": delivery.district?.id ?? 0,
            "address": delivery.address ?? "",
            "type": delivery.typeDelivery?.rawValue ?? TypeDelivery.Home.rawValue,
            "is_default": 1
        ]
        
        let urlRequest = StoreServiceAPI.addressCreate.urlString
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setUpdateDeliveryAddress(delivery: DeliveryModel, completion: @escaping ((Result<BaseModel<DeliveryModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "name": delivery.name ?? "",
            "phone": delivery.phone ?? "",
            "email": delivery.email ?? "",
            "country": delivery.country?.id ?? 0,
            "state": delivery.state?.id ?? 0,
            "city": delivery.city?.id ?? 0,
            "district": delivery.district?.id ?? 0,
            "address": delivery.address ?? "",
            "type": delivery.typeDelivery?.rawValue ?? TypeDelivery.Home.rawValue,
            "is_default": 1
        ]
        
        let urlRequest = StoreServiceAPI.addressUpdate("\(delivery.id!)").urlString
        self.service.PUT(url: urlRequest, param: params, encoding: JSONEncoding.default, completion: completion)
    }
    
    func getDeliveryAddress(completion: @escaping ((Result<BaseModel<DeliveryModel>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "include": "customer"
        ]
        let url = StoreServiceAPI.defaultAddress.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getBankStoreInfo(completion: @escaping ((Result<BaseModel<[BankStoreModel]>, APIError>) -> Void)) {
        let url = StoreServiceAPI.paymentMethods.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
    
    func setCreateOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], orderCode: String, paymentMethod: String?, couponCode: String?, desc: String?, completion: @escaping ((Result<BaseModel<OrderModel>, APIError>) -> Void)) {
        
        let address: [String: Any] = [
            "name": delivery.name ?? "",
            "phone": delivery.phone ?? "",
            "email": delivery.email ?? "",
            "country": delivery.country?.id ?? 0,
            "state": delivery.state?.id ?? 0,
            "city": delivery.city?.id ?? 0,
            "district": delivery.district?.id ?? 0,
            "address": delivery.address ?? "",
            "type": delivery.typeDelivery?.rawValue ?? "",
            "is_default": 1
        ]
        
        var items: [[String: Any]] = []
        for product in listProduct {
            let item: [String: Any] = [
                "product_id": product.id ?? 0,
                "quantity": product.amount ?? 0
            ]
            
            items.append(item)
        }
        
        let params: [String: Any] = [
            "address": address,
            "items": items,
            "payment_method": paymentMethod ?? "",
            "coupon_code": couponCode ?? "",
            "description": desc ?? "",
            "code": orderCode
        ]
        
        let urlRequest = StoreServiceAPI.order.urlString
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func setVoucherOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], paymentMethod: String?, couponCode: String?, desc: String?, completion: @escaping ((Result<BaseModel<OrderModel>, APIError>) -> Void)) {
        
        let address: [String: Any] = [
            "name": delivery.name ?? "",
            "phone": delivery.phone ?? "",
            "email": delivery.email ?? "",
            "country": delivery.country?.id ?? 0,
            "state": delivery.state?.id ?? 0,
            "city": delivery.city?.id ?? 0,
            "district": delivery.district?.id ?? 0,
            "address": delivery.address ?? "",
            "type": delivery.typeDelivery?.rawValue ?? ""
        ]
        
        var items: [[String: Any]] = []
        for product in listProduct {
            let item: [String: Any] = [
                "product_id": product.id ?? 0,
                "quantity": product.amount ?? 0
            ]
            
            items.append(item)
        }
        
        let params: [String: Any] = [
            "address": address,
            "items": items,
            "payment_method": paymentMethod ?? "",
            "coupon_code": couponCode ?? "",
            "description": desc ?? "",
            "apply_coupon": true
        ]
        
        let urlRequest = StoreServiceAPI.order.urlString
        self.service.POST(url: urlRequest, param: params, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func getListOrder(with status: OrderStatus, page: Int, perPage: Int, completion: @escaping (Result<BaseModel<[OrderModel]>, APIError>) -> Void) {
        var params: [String: Any] = [
            "page": page,
            "per_page": perPage,
            "include": "items.product,payment,address,shipment",
            "order[updated_at]": "desc"
        ]
        if status != .all {
            params.updateValue(status.rawValue, forKey: "status")
        }
        let url = StoreServiceAPI.orders.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getOrderDetail(orderId: Int, completion: @escaping (Result<BaseModel<OrderModel>, APIError>) -> Void) {
        let params: [String: Any] = [
            "include": "items.product,payment,address,shipment"
        ]
        let url = StoreServiceAPI.orderWithId("\(orderId)").urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setCancelOrder(orderId: Int, completion: @escaping (Result<BaseModel<OrderModel>, APIError>) -> Void) {
        let params: [String: Any] = [
            "status": OrderStatus.canceled.rawValue
        ]
        let url = StoreServiceAPI.orderWithId("\(orderId)").urlString
        self.service.PUT(url: url, param: params, completion: completion)
    }
    
    func getListOrderStatus(completion: @escaping (Result<BaseModel<[OrderStatusModel]>, APIError>) -> Void) {
        let params: [String: Any] = [
            "include": "orders_count"
        ]
        let url = StoreServiceAPI.orderStatuses.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func setReviewProduct(itemOrder: ItemOrder, orderId: Int?, completion: @escaping ((Result<BaseModel<ItemOrder>, APIError>) -> Void)) {
        // Value Params
        var rateComment = ""
        if let listRateComment = itemOrder.rateComments, listRateComment.count > 0 {
            for obj in listRateComment {
                if obj.isSelected == true {
                    if rateComment == "" {
                        rateComment = "\(obj.comment ?? "")."
                    } else {
                        rateComment = "\(rateComment) \(obj.comment ?? "")."
                    }
                }
            }
        }
        var comment = itemOrder.comment ?? ""
        if comment == "" {
            comment = "\(rateComment)"
        } else {
            comment = "\(itemOrder.comment ?? ""). \n\(rateComment)"
        }
        
        let param: [String: String] = [
            "order_id": orderId?.asString ?? "0",
            "star": "\(itemOrder.rateType?.value ?? RateType.five.value)",
            "subject": "Đánh giá",
            "comment": comment
        ]
        
        // Image Param
        var images: [[String: Data]] = []
        var listImage: [UIImage] = []
        
        if let imageReviews = itemOrder.imageReviews {
            for image in imageReviews {
                listImage.append(image)
            }
            listImage.removeLast()
            
            for image in listImage {
                if let imageData = image.jpegData(compressionQuality: 1) {
                    let imageObj: [String: Data] = ["image": imageData]
                    images.append(imageObj)
                }
            }
        }
        
        let productId = itemOrder.product?.originalProduct ?? 0
        
        let url = StoreServiceAPI.productReview.urlString.replaceCharacter(target: ":id", withString: "\(productId)")
        self.service.UPLOAD_IMAGES(url: url, params: param, images: images, completion: completion)
    }
    
    func getMyReviewProduct(listProductId: [Int], orderId: Int?, completion: @escaping (Result<BaseModel<[ProductOrderModel]>, APIError>) -> Void) {
        let params: [String: Any] = [
            "product_ids[]": listProductId,
            "order_id": orderId ?? 0,
            "include": "reviews"
        ]
        let url = StoreServiceAPI.reviews.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getFlashSaleProducts(completion: @escaping (Result<BaseModel<[FlashSaleModel]>, APIError>) -> Void) {
        let url = StoreServiceAPI.flashSale.urlString
        let params: [String: Any] = [
            "include": "products"
        ]
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func postRegisterProduct(name: String, phone: String, email: String, productId: String, completion: @escaping (Result<BaseModel<EmptyModel>, APIError>) -> Void) {
        let url = StoreServiceAPI.registerProduct.urlString
        let params: [String: Any] = [
              "name": name,
              "phone": phone,
              "email": email,
              "product_id": productId
        ]
        self.service.POST(url: url, param: params, completion: completion)
    }
    
    func getVariationProduct(id: Int, attributeIDs: [Int], completion: @escaping (Result<BaseModel<ProductModel>, APIError>) -> Void) {
        let url = StoreServiceAPI.variationProduct(id).urlString
        let params: [String: Any] = [
              "include": "full_attributes",
              "attributes": attributeIDs
        ]
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getAllFlashSaleProduct(page: Int, perPage: Int, completion: @escaping ((Result<BaseModel<[Product]>, APIError>) -> Void)) {
        let url = StoreServiceAPI.flashSaleProducts.urlString
        let params: [String: Any] = [
            "page": page,
            "per_page": perPage
        ]
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getHotProduct(completion: @escaping ((Result<BaseModel<[Product]>, APIError>) -> Void)) {
        let params: [String: Any] = [
            "page": 1,
            "per_page": 4
        ]
        let url = StoreServiceAPI.productsHot.urlString
        self.service.GET(url: url, param: params, completion: completion)
    }
    
    func getStrategiesProduct(completion: @escaping ((Result<BaseModel<[StrategiesModel]>, APIError>) -> Void)) {
        let url = StoreServiceAPI.strategies.urlString
        self.service.GET(url: url, param: nil, completion: completion)
    }
}
