//
//  UploadService.swift
//  1SK
//
//  Created by tuyenvx on 23/02/2021.
//

import UIKit

protocol UploadServiceProtocol {
    func uploadAvatar(image: UIImage?, completion: @escaping ((Result<FileUploadModel, APIError>) -> Void))
    func uploadImage(image: UIImage?, completion: @escaping ((Result<BaseModel<FileUploadModel>, APIError>) -> Void))
}

// MARK: UploadServiceProtocol
class UploadService: UploadServiceProtocol {
    let service = BaseService.shared

    func uploadAvatar(image: UIImage?, completion: @escaping ((Result<FileUploadModel, APIError>) -> Void)) {
        var imageParam: [String: Data] = [:]
        if let `image` = image, let imageData = image.jpegData(compressionQuality: 1) {
            imageParam[SKKey.file] = imageData
        }
        self.service.upload(url: UploadServiceAPI.avatar, param: [:], imageParam: imageParam, completion: completion)
    }

    func uploadImage(image: UIImage?, completion: @escaping ((Result<BaseModel<FileUploadModel>, APIError>) -> Void)) {
        var imageParam: [String: Data] = [:]
        if let `image` = image, let imageData = image.jpegData(compressionQuality: 1) {
            imageParam[SKKey.file] = imageData
        }
        self.service.UPLOAD(url: NewUploadServiceAPI.image.urlString, param: ["folder": "images"], imageParam: imageParam, completion: completion)
    }
}
