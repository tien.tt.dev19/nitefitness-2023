//
//  VideoExerciseService.swift
//  1SK
//
//  Created by Valerian on 24/06/2022.
//

import Foundation
import Alamofire

protocol VideoExerciseSeviceProtocol: AnyObject {
    func onGetCategoryVideoExercise(completion: @escaping ((Result<BaseModel<[CategoryVideoExerciseModel]>, APIError>) -> Void))
    func onGetRelatedVideoExercise(videoID: Int, categoryID: Int, completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void))
    func onGetListVideoExercise(page: Int?,
                                per_page: Int?,
                                popular: Bool?,
                                isFeatured: Int?,
                                coach_id: Int?,
                                slug: Int?,
                                category_id: Int?,
                                column: String?,
                                sort: String?,
                                completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void))
    func onGetVideoUserHistory(completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void))
    func onGetVideoUserFavorite(completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void))
    func onRemoveFavoriteList(with id: Int, completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void))
    func onAddToFavoriteList(with id: Int, completion: @escaping ((Result<BaseModel<VideoExerciseModel>, APIError>) -> Void))
    func onGetVideoByCatepory(categoryID: Int, completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void))
    func onSaveLastSeen(with videoID: String, at lastTime: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void))
}

//MARK: - VideoExerciseSeviceProtocol
class VideoExerciseService: VideoExerciseSeviceProtocol {
    private let service = BaseService.shared
    
    func onGetCategoryVideoExercise(completion: @escaping ((Result<BaseModel<[CategoryVideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.listCategoryVideoExercise.urlString
        self.service.GET(url: urlPath, param: nil, completion: completion)
    }
    
    func onGetListVideoExercise(page: Int?,
                                per_page: Int?,
                                popular: Bool?,
                                isFeatured: Int?,
                                coach_id: Int?,
                                slug: Int?,
                                category_id: Int?,
                                column: String?,
                                sort: String?,
                                completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.listVideoExercise.urlString
        let param: [String: Any] = [
            "page": page ?? 1,
            "per_page": per_page ?? 10,
            "popular": popular ?? true,
            "is_featured": isFeatured ?? 1,
            "coach_id": coach_id ?? "",
            "slug": slug ?? "",
            "column": column ?? "created_at",
            "category_id": category_id ?? "",
            "sort": sort ?? "DESC"
        ]
        self.service.GET(url: urlPath, param: param, completion: completion)
    }
    
    func onGetVideoByCatepory(categoryID: Int, completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.listVideoExercise.urlString
        let param: [String: Any] = [
            "page": 1,
            "per_page": 1000,
//            "popular": popular ?? true,
//            "is_featured": isFeatured ?? 1,
//            "coach_id": coach_id ?? "",
//            "slug": slug ?? "",
            "column": "created_at",
            "category_id": categoryID,
            "sort": "DESC"
        ]
        self.service.GET(url: urlPath, param: param, completion: completion)
    }
    
    
    func onGetRelatedVideoExercise(videoID: Int, categoryID: Int, completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.relatedVideoExercise.urlString
        let param: [String: Any] = [
            "video_id": videoID,
            "category_id": categoryID,
            "type": "related"
        ]
        self.service.GET(url: urlPath, param: param, completion: completion)
    }
    
    func onGetVideoUserHistory(completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.videoUserHistory.urlString
        self.service.GET(url: urlPath, param: nil, completion: completion)
    }
    
    func onGetVideoUserFavorite(completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.videoUserFavorite.urlString
        self.service.GET(url: urlPath, param: nil, completion: completion)
    }
    
    func onRemoveFavoriteList(with id: Int, completion: @escaping ((Result<BaseModel<[VideoExerciseModel]>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.removeFavoriteList(id).urlString
        self.service.DELETE(url: urlPath, param: nil, completion: completion)
    }
    
    func onAddToFavoriteList(with id: Int, completion: @escaping ((Result<BaseModel<VideoExerciseModel>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.videoUserFavorite.urlString
        let param: [String: Any] = [
            "video_id": id
        ]
        self.service.POST(url: urlPath, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func onSaveLastSeen(with videoID: String, at lastTime: String, completion: @escaping ((Result<BaseModel<EmptyModel>, APIError>) -> Void)) {
        let urlPath = VideoExerciseAPI.lastSeen.urlString
        let param: [String: Any] = [
            "video_id": videoID,
            "last_seen_at": lastTime
        ]
        self.service.POST(url: urlPath, param: param, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
}
