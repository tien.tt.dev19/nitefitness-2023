//
//  VisualRaceService.swift
//  1SK
//
//  Created by Valerian on 24/06/2022.
//

import Foundation
import Alamofire

protocol VisualRaceSeviceProtocol: AnyObject {
    func onGetListVisualRace(completion: @escaping ((Result<BaseModel<[VisualRaceModel]>, APIError>) -> Void))
}

//MARK: - VisualRaceSeviceProtocol
class VisualRaceService: VisualRaceSeviceProtocol {
    private let service = BaseService.shared

    func onGetListVisualRace(completion: @escaping ((Result<BaseModel<[VisualRaceModel]>, APIError>) -> Void)) {
        let urlPath = VisualRaceServiceAPI.visualRaceList.urlString
        self.service.GET(url: urlPath, param: nil, completion: completion)
    }
}
