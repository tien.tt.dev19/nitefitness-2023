//
//  SocketHelperCare.swift
//  1SK
//
//  Created by vuongbachthu on 10/11/21.
//

import Foundation
import SocketIO

enum EventSocketCare: String {
    case AppointmentCreatedEvent = "Modules\\v1_0\\Schedule\\Events\\AppointmentCreatedEvent"
    case AppointmentStatusChangedEvent = "Modules\\v1_0\\Schedule\\Events\\AppointmentStatusChangedEvent"
    case AppointmentShowButtonJoinEvent = "Modules\\v1_0\\Schedule\\Events\\AppointmentShowButtonJoinEvent"
    case AppointmentEndedEvent = "Modules\\v1_0\\Schedule\\Events\\AppointmentEndedEvent"
    case PopupLiveReadyToSendEvent = "Modules\\v1_0\\InAppMessaging\\Events\\PopupLiveReadyToSendEvent"
}

extension Notification.Name {
    static let AppointmentCreatedEvent = Notification.Name("AppointmentCreatedEvent")
    static let AppointmentStatusChangedEvent = Notification.Name("AppointmentStatusChangedEvent")
    static let AppointmentShowButtonJoinEvent = Notification.Name("AppointmentShowButtonJoinEvent")
    static let AppointmentEndedEvent = Notification.Name("AppointmentEndedEvent")
    
    static let PopupLiveReadyToSendEvent = Notification.Name("PopupLiveReadyToSendEvent")
}

// MARK: Init Socket Helper
class SocketHelperCare: NSObject {
    static let share = SocketHelperCare()
    var manager: SocketManager?
    var socket: SocketIOClient!
    
    /// Socket Client Event
    var authenticate: Bool?
    var reconnectAttempt: String?
    var connected: String?
    var disconnect: String?
    var reconnect: String?
    var existed: String?

    private override init() {
        super.init()
    }
    
    func initConnectionSocket() {
        print("socket.on care:  URL: ", SOCKET_CARE_URL)
        self.manager = SocketManager(socketURL: URL(string: SOCKET_CARE_URL)!, config: [.version(.two), .log(false), .compress, .forceNew(true), .connectParams([:])])

        gSocketCare = self.manager?.defaultSocket
        
        self.onHandleEventSystem()
        self.connectionSocket()
    }
    
    func connectionSocket() {
        gSocketCare?.connect()
    }

    func disconnectSocket() {
        gSocketCare?.disconnect()
    }

    func removeAllHandlersSocket() {
        gSocketCare?.removeAllHandlers()
    }
}

//MARK: Handle Emit Auth
extension SocketHelperCare {
    func authSocketChannel() {
        guard let tokenAuth = KeyChainManager.shared.accessToken, let userId = gUser?.id else {
            return
        }
        
        let headers: [String: Any] = ["Authorization": "Bearer \(tokenAuth)"]
        let auth: [String: Any] = ["headers": headers]
        
        let channelUser: [String: Any] = ["channel": "private-customer-\(userId)", "auth": auth]
        gSocketCare?.emit("subscribe", channelUser) {
            print("socket.emit care: subscribe channel: private-customer-\(userId): ")
            self.onHandleEvent1SK()
        }
        
        let channelLivePopup: [String: Any] = ["channel": "live-popup", "auth": auth]
        gSocketCare?.emit("subscribe", channelLivePopup) {
            print("socket.emit care: subscribe channel: live-popup:", channelLivePopup)
        }
    }
}

//MARK: Handle Socket System
extension SocketHelperCare {
    func onHandleEventSystem() {
        gSocketCare?.on(clientEvent: .connect) {data, ack in
            print("socket.on care: socket connected: ID:", gSocketCare?.manager?.engine?.sid ?? "nil")
            self.authSocketChannel()
        }

        gSocketCare?.on(clientEvent: .disconnect) {data, ack in
            print("socket.on care: disconnect")
            self.authenticate = false
        }

        gSocketCare?.on(clientEvent: .reconnect) {data, ack in
            print("socket.on care: reconnect")
        }

        gSocketCare?.on(clientEvent: .reconnectAttempt) {data, ack in
            print("socket.on care: reconnectAttempt")
        }

        gSocketCare?.on(clientEvent: .websocketUpgrade) {data, ack in
            print("socket.on care: websocketUpgrade")
        }
        
        gSocketCare?.on(clientEvent: .statusChange) {data, ack in
            print("socket.on care: statusChange: \((data.first as AnyObject).debugDescription ?? "Null")")
        }

        gSocketCare?.on(clientEvent: .error) {data, ack in
            let dataFirst =  data.first as? String ?? "Null"
            print("socket.on care: error: ", dataFirst.debugDescription)
            self.authenticate = false
        }

        gSocketCare?.on(clientEvent: .ping) {data, ack in
            print("socket.on care: ping")
        }

        gSocketCare?.on(clientEvent: .pong) {data, ack in
            print("socket.on care: pong")
        }
    }
}

// MARK: Handle Event 1SK
extension SocketHelperCare {
    
    /************************ On Socket Custom ************************/
    
    func onHandleEvent1SK() {
        print("socket.on care: onHandleEvent1SK")
        
        gSocketCare?.on(EventSocketCare.AppointmentCreatedEvent.rawValue) { (data, ack) in
            print("socket.on care: AppointmentCreatedEvent")
            
            guard let objSecond = data[1] as? [String: Any] else {
                return
            }
            let data = SocketAppointmentModel(data: objSecond)
            guard let appointment = data.appointment else {
                return
            }
            
            NotificationCenter.default.post(name: .AppointmentCreatedEvent, object: appointment)
        }
        
        gSocketCare?.on(EventSocketCare.AppointmentStatusChangedEvent.rawValue) { (data, ack) in
            print("socket.on care: AppointmentStatusChangedEvent")
            
            guard let objSecond = data[1] as? [String: Any] else {
                return
            }
            let data = SocketAppointmentModel(data: objSecond)
            guard let appointment = data.appointment else {
                return
            }
            
            print("socket.on care: AppointmentStatusChangedEvent Continue")
            
            gBadgeCare += 1
            //UIApplication.shared.applicationIconBadgeNumber = gBadgeCare
            LocalNotification.shared.createLocalNotification(title: "Lịch hẹn tư vấn", subtitle: nil, body: appointment.status?.name, badge: false)
            
            NotificationCenter.default.post(name: .AppointmentStatusChangedEvent, object: appointment)
        }
        
        gSocketCare?.on(EventSocketCare.AppointmentShowButtonJoinEvent.rawValue) { (data, ack) in
            print("socket.on care: AppointmentShowButtonJoinEvent")
            guard let objSecond = data[1] as? [String: Any] else {
                return
            }
            let data = SocketAppointmentModel(data: objSecond)
            guard let appointment = data.appointment else {
                return
            }
            NotificationCenter.default.post(name: .AppointmentShowButtonJoinEvent, object: appointment)
        }
        
        gSocketCare?.on(EventSocketCare.PopupLiveReadyToSendEvent.rawValue) { (data, ack) in
            print("socket.on care: PopupLiveReadyToSendEvent")
            guard let objSecond = data[1] as? [String: Any] else {
                return
            }
            let model = PopupModel(data: objSecond)
            NotificationCenter.default.post(name: .PopupLiveReadyToSendEvent, object: model)
        }
    }
}

// MARK: Handle Emit 1SK
extension SocketHelperCare {
    func emitUserJoinedRoom(appointmentId: Int) {
        guard let tokenAuth = KeyChainManager.shared.accessToken, let userId = gUser?.id else {
            return
        }
        
        let auth: [String: Any] = ["headers": ["Authorization": "Bearer \(tokenAuth)"]]
        
        let data: [String: Any] = ["appointmentId": "private-customer-\(userId)", "auth": auth]
        
        gSocketCare?.emit("subscribe", data) {
            print("socket.emit care: subscribe channel: private-customer-\(userId): ")
            self.onHandleEvent1SK()
        }
    }
}
