//
//  SocketHelperFitness.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//

import Foundation
import SocketIO

enum EventSocketFitness: String {
    case UserRegisterVR = "Modules\\v1_1\\VirtualRace\\Events\\UserRegisterVR"
    case VRRegistrationJustExpiredEvent = "Modules\\v1_1\\VirtualRace\\Events\\VRRegistrationJustExpiredEvent"
    case VRShouldUpdateRankingEvent = "Modules\\v1_1\\VirtualRace\\Events\\VRShouldUpdateRankingEvent"
}

extension Notification.Name {
    static let UserRegisterVR = Notification.Name("UserRegisterVR")
    static let VRRegistrationJustExpiredEvent = Notification.Name("VRRegistrationJustExpiredEvent")
    static let VRShouldUpdateRankingEvent = Notification.Name("VRShouldUpdateRankingEvent")
}

class SocketHelperFitness: NSObject {
    static let share = SocketHelperFitness()
    var manager: SocketManager?
    var socket: SocketIOClient!
    
    /// Socket Client Event
    var authenticate: Bool?
    var reconnectAttempt: String?
    var connected: String?
    var disconnect: String?
    var reconnect: String?
    var existed: String?

    private override init() {
        super.init()
    }
    
    func initConnectionSocket() {
        guard let tokenAuth = KeyChainManager.shared.accessToken else {
            return
        }
        let auth: [String: String] = ["Authorization": "Bearer \(tokenAuth)"]
        print("socket.on fitness: URL: ", SOCKET_FITNESS_URL)
        print("socket.on fitness: auth: ", auth)
        
        self.manager = SocketManager(socketURL: URL(string: SOCKET_FITNESS_URL)!, config: [.version(.three), .log(false), .compress, .forceNew(true), .extraHeaders(auth), .connectParams([:])])

        gSocketFitness = self.manager?.defaultSocket
        
        self.onHandleEventSystem()
        self.connectionSocket()
    }
    
    func connectionSocket() {
        gSocketFitness?.connect()
    }

    func disconnectSocket() {
        gSocketFitness?.disconnect()
    }

    func removeAllHandlersSocket() {
        gSocketFitness?.removeAllHandlers()
    }
}

//MARK: Handle Socket System
extension SocketHelperFitness {
    func onHandleEventSystem() {
        /************************ On Socket System *************************/
        
        gSocketFitness?.on(clientEvent: .connect) {data, ack in
            print("socket.on fitness: socket connected: ID:", gSocketFitness?.manager?.engine?.sid ?? "nil")
            self.authSocketChannel()
        }

        gSocketFitness?.on(clientEvent: .disconnect) {data, ack in
            print("socket.on fitness: disconnect")
            self.authenticate = false
        }

        gSocketFitness?.on(clientEvent: .reconnect) {data, ack in
            print("socket.on fitness: reconnect")
        }

        gSocketFitness?.on(clientEvent: .reconnectAttempt) {data, ack in
            print("socket.on fitness: reconnectAttempt")
        }

        gSocketFitness?.on(clientEvent: .websocketUpgrade) {data, ack in
            print("socket.on fitness: websocketUpgrade")
        }
        
        gSocketFitness?.on(clientEvent: .statusChange) {data, ack in
            print("socket.on fitness: statusChange: \((data.first as AnyObject).debugDescription ?? "Null")")
        }

        gSocketFitness?.on(clientEvent: .error) {data, ack in
            let dataFirst =  data.first as? String ?? "Null"
            print("socket.on fitness: error: ", dataFirst)
            self.authenticate = false
        }

        gSocketFitness?.on(clientEvent: .ping) {data, ack in
            print("socket.on fitness: ping")
        }

        gSocketFitness?.on(clientEvent: .pong) {data, ack in
            print("socket.on fitness: pong")
        }
    }
}

// MARK: Handle Event 1SK
extension SocketHelperFitness {
    func onHandleEvent1SK() {
        /************************ On Socket Custom ************************/
        
        gSocketFitness?.on(EventSocketFitness.UserRegisterVR.rawValue) { (data, ack) in
            print("socket.on fitness: UserRegisterVR data:", data)

            guard let dataObj = data[1] as? [String: Any] else {
                return
            }
            let model = UserRegisterVRModel(data: dataObj)
            guard let race = model.race else {
                return
            }

            print("socket.on fitness: UserRegisterVR UserRegisterVRModel race.id:", race.id ?? -1)
            NotificationCenter.default.post(name: .UserRegisterVR, object: race)
        }
        
        gSocketFitness?.on(EventSocketFitness.VRRegistrationJustExpiredEvent.rawValue) { (data, ack) in
            print("socket.on fitness: VRRegistrationJustExpiredEvent data:", data)

            guard let dataObj = data[1] as? [String: Any] else {
                return
            }
            let model = VRRegistrationJustExpiredEventModel(data: dataObj)
            guard let listExpiredId = model.listExpiredId else {
                return
            }
            print("socket.on fitness: VRRegistrationJustExpiredEvent listExpiredId.count:", model)
            NotificationCenter.default.post(name: .VRRegistrationJustExpiredEvent, object: listExpiredId)
        }
        
        gSocketFitness?.on(EventSocketFitness.VRShouldUpdateRankingEvent.rawValue) { (data, ack) in
            print("socket.on fitness: VRShouldUpdateRankingEvent data:", data)
            
            guard let dataObj = data[1] as? [String: Any] else {
                return
            }
            let model = VRShouldUpdateRankingEventModel(data: dataObj)
            guard let listRankingId = model.listRankingId else {
                return
            }
            print("socket.on fitness: VRShouldUpdateRankingEvent listRankingId.count:", listRankingId.count)
            NotificationCenter.default.post(name: .VRShouldUpdateRankingEvent, object: listRankingId)
        }
        //gSocketFitness?.off(EventSocketFitness.VRShouldUpdateRankingEvent.rawValue)
    }
}

//MARK: Emit Event Socket
extension SocketHelperFitness {
    func authSocketChannel() {
        guard let tokenAuth = KeyChainManager.shared.accessToken, let userId = gUser?.id else { return }
        let headers: [String: Any] = ["Authorization": "Bearer \(tokenAuth)"]
        let auth: [String: Any] = ["headers": headers]

        let channelUser: [String: Any] = ["channel": "private-customer-\(userId)", "auth": auth]
        gSocketFitness?.emit("subscribe", channelUser) {
            print("socket.emit fitness: subscribe channel: private-customer-\(userId): ")
            self.onHandleEvent1SK()
        }
        
        let channelVirtualRace: [String: Any] = ["channel": "virtual-race", "auth": auth]
        gSocketFitness?.emit("subscribe", channelVirtualRace) {
            print("socket.emit fitness: subscribe channel: virtual-race:", channelVirtualRace)
        }
    }
    
}
