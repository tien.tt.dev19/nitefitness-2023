//
//  SocketHelperID.swift
//  1SK
//
//  Created by Thaad on 12/09/2022.
//

import Foundation
import SocketIO

enum EventSocketID: String {
    case ConnectStravaSuccessEvent = "Modules\\AccountLinking\\Events\\ConnectStravaSuccessEvent"
    case DisconnectStravaSuccessEvent = "Modules\\AccountLinking\\Events\\DisconnectStravaSuccessEvent"
    case NewAnnouncementToAllEvent = "Modules\\Announcement\\Events\\NewAnnouncementToAllEvent"
    case NewAnnouncementToSingleUserEvent = "Modules\\Announcement\\Events\\NewAnnouncementToSingleUserEvent"
}

extension Notification.Name {
    static let ConnectStravaSuccessEvent = Notification.Name("ConnectStravaSuccessEvent")
    static let NewAnnouncementToAllEvent = Notification.Name("NewAnnouncementToAllEvent")
    static let NewAnnouncementToSingleUserEvent = Notification.Name("NewAnnouncementToSingleUserEvent")
}

class SocketHelperID: NSObject {
    static let share = SocketHelperID()
    var manager: SocketManager?
    var socket: SocketIOClient!
    
    /// Socket Client Event
    var authenticate: Bool?
    var reconnectAttempt: String?
    var connected: String?
    var disconnect: String?
    var reconnect: String?
    var existed: String?

    private override init() {
        super.init()
    }
    
    func initConnectionSocket() {
        guard let tokenAuth = KeyChainManager.shared.accessToken else {
            return
        }
        let auth: [String: String] = ["Authorization": "Bearer \(tokenAuth)"]
        print("socket.on ID: URL: ", SOCKET_ID_URL)
        print("socket.on ID: auth: ", auth)
        
        self.manager = SocketManager(socketURL: URL(string: SOCKET_ID_URL)!, config: [.version(.three), .log(false), .compress, .forceNew(true), .extraHeaders(auth), .connectParams([:])])
    

        gSocketID = self.manager?.defaultSocket
        
        self.onHandleEventSystem()
        self.connectionSocket()
    }
    
    func connectionSocket() {
        gSocketID?.connect()
    }

    func disconnectSocket() {
        gSocketID?.disconnect()
    }

    func removeAllHandlersSocket() {
        gSocketID?.removeAllHandlers()
    }
}

//MARK: On Event Socket
extension SocketHelperID {
    func onHandleEventSystem() {
        /************************ On Socket System *************************/
        
        gSocketID?.on(clientEvent: .connect) {data, ack in
            print("socket.on ID: socket connected: ID:", gSocketID?.manager?.engine?.sid ?? "nil")
            self.authSocketChannel()
        }

        gSocketID?.on(clientEvent: .disconnect) {data, ack in
            print("socket.on ID: disconnect")
            self.authenticate = false
        }

        gSocketID?.on(clientEvent: .reconnect) {data, ack in
            print("socket.on ID: reconnect")
        }

        gSocketID?.on(clientEvent: .reconnectAttempt) {data, ack in
            print("socket.on ID: reconnectAttempt")
        }

        gSocketID?.on(clientEvent: .websocketUpgrade) {data, ack in
            print("socket.on ID: websocketUpgrade")
        }
        
        gSocketID?.on(clientEvent: .statusChange) {data, ack in
            print("socket.on ID: statusChange: \((data.first as AnyObject).debugDescription ?? "Null")")
        }

        gSocketID?.on(clientEvent: .error) {data, ack in
            let dataFirst =  data.first as? String ?? "Null"
            print("socket.on ID: error: ", dataFirst)
            self.authenticate = false
        }

        gSocketID?.on(clientEvent: .ping) {data, ack in
            print("socket.on ID: ping")
        }

        gSocketID?.on(clientEvent: .pong) {data, ack in
            print("socket.on ID: pong")
        }
    }
}

// MARK: Handle Event 1SK
extension SocketHelperID {
    func onHandleEvent1SK() {
        /************************ On Socket Custom ************************/
        
        gSocketID?.on(EventSocketID.ConnectStravaSuccessEvent.rawValue) { (data, ack) in
            print("socket.on ID: ConnectStravaSuccessEvent data:", data)
            //NotificationCenter.default.post(name: .ConnectStravaSuccessEvent, object: listRankingId)
        }
        
        gSocketID?.on(EventSocketID.NewAnnouncementToSingleUserEvent.rawValue) { (data, ack) in
            print("socket.on ID: onHandleNewAnnouncementToSingleUserEvent1SK data:", data)
            if let model = data[1] as? [String: Any] {
                let data = AnnouncementsSocketModel(data: model)
                gTabBarController?.tabBar.addBadge(index: 3)
                NotificationCenter.default.post(name: .NewAnnouncementToSingleUserEvent, object: data)
            }
        }
        
        gSocketID?.on(EventSocketID.NewAnnouncementToAllEvent.rawValue) { (data, ack) in
            print("socket.on ID: onHandleAllAnnouncementEvent1SK data:", data)
            if let model = data[1] as? [String: Any] {
                let data = AnnouncementsSocketModel(data: model)
                gTabBarController?.tabBar.addBadge(index: 3)
                NotificationCenter.default.post(name: .NewAnnouncementToAllEvent, object: data)
            }
        }
    }
}

//MARK: Emit Event Socket
extension SocketHelperID {
    func authSocketChannel() {
        guard let tokenAuth = KeyChainManager.shared.accessToken, let userId = gUser?.id else { return }
        let headers: [String: Any] = ["Authorization": "Bearer \(tokenAuth)"]
        let auth: [String: Any] = ["headers": headers]

        let channelUser: [String: Any] = ["channel": "private-customer-\(userId)", "auth": auth]
        gSocketID?.emit("subscribe", channelUser) {
            print("socket.emit ID: subscribe channel: private-customer-\(userId): ")
            self.onHandleEvent1SK()
        }

        let channelVirtualRace: [String: Any] = ["channel": "virtual-race", "auth": auth]
        gSocketID?.emit("subscribe", channelVirtualRace) {
            print("socket.emit ID: subscribe channel: virtual-race:", channelVirtualRace)
        }
        
        let channelSingleAnnouncement: [String: Any] = ["channel": "customer-\(userId)", "auth": auth]
        gSocketID?.emit("subscribe", channelSingleAnnouncement) {
            print("socket.emit ID: subscribe channel: customer-\(userId):", channelSingleAnnouncement)
        }
        
        let channelAllAnnouncements: [String: Any] = ["channel": "announcements", "auth": auth]
        gSocketID?.emit("subscribe", channelAllAnnouncements) {
            print("socket.emit ID: subscribe channel: announcements:", channelAllAnnouncements)
        }
    }
}
