//
//  SocketHelperTimer.swift
//  1SK
//
//  Created by Thaad on 29/03/2022.
//

import Foundation
import SocketIO

enum EventTracking: String {
    case user_joined_room // Vào phòng
    case user_leaved_room // Rời phòng
    
    case user_disabled_camera // Tắt Camera
    case user_enabled_camera // Bật Camera
    
    case user_disabled_mic // Tắt Mic
    case user_enabled_mic // Bật Mic
    
    case user_focused // Bật App/Bật web
    case user_lost_focused // Tắt App/Tắt Web
    
    case user_error // Gặp lỗi (Gửi mã lỗi vào data)
}

class ErrorTracking: NSObject {
    var errorCode: Int?
    var errorMessage: String?
}

// MARK: Init Socket Helper Timer
class SocketHelperTimer: NSObject {
    static let share = SocketHelperTimer()
    var manager: SocketManager?
    var socket: SocketIOClient!
    
    /// Socket Client Event
    var authenticate: Bool?
    var reconnectAttempt: String?
    var connected: String?
    var disconnect: String?
    var reconnect: String?
    var existed: String?

    private override init() {
        super.init()
    }
    
    func initConnectionSocket() {
        guard let tokenAuth = KeyChainManager.shared.accessToken else {
            return
        }
        let auth: [String: String] = ["Authorization": "Bearer \(tokenAuth)"]
        print("socket.on timer: URL: ", SOCKET_TIMER_URL)
        print("socket.on timer: auth: ", auth)
        
        self.manager = SocketManager(socketURL: URL(string: SOCKET_TIMER_URL)!, config: [.version(.three), .log(false), .compress, .forceNew(true), .extraHeaders(auth), .connectParams([:])])
        gSocketTimer = self.manager?.defaultSocket
        
        self.onHandleEventSystem()
        self.connectionSocket()
    }
    
    func connectionSocket() {
        gSocketTimer?.connect()
    }

    func disconnectSocket() {
        gSocketTimer?.disconnect()
    }

    func removeAllHandlersSocket() {
        gSocketTimer?.removeAllHandlers()
    }
}

//MARK: Handle Socket System
extension SocketHelperTimer {
    func onHandleEventSystem() {
        gSocketTimer?.on(clientEvent: .connect) {data, ack in
            print("socket.on timer: socket connected: ID:", gSocketTimer?.manager?.engine?.sid ?? "nil")
        }

        gSocketTimer?.on(clientEvent: .disconnect) {data, ack in
            print("socket.on timer: disconnect")
            self.authenticate = false
        }

        gSocketTimer?.on(clientEvent: .reconnect) {data, ack in
            print("socket.on timer: reconnect")
        }

        gSocketTimer?.on(clientEvent: .reconnectAttempt) {data, ack in
            print("socket.on timer: reconnectAttempt")
        }

        gSocketTimer?.on(clientEvent: .websocketUpgrade) {data, ack in
            print("socket.on timer: websocketUpgrade")
        }
        
        gSocketTimer?.on(clientEvent: .statusChange) {data, ack in
            print("socket.on timer: statusChange: \((data.first as AnyObject).debugDescription ?? "Null")")
        }

        gSocketTimer?.on(clientEvent: .error) {data, ack in
            let dataFirst =  data.first as? String ?? "Null"
            print("socket.on timer: error: ", dataFirst.debugDescription)
            self.authenticate = false
        }

        gSocketTimer?.on(clientEvent: .ping) {data, ack in
            print("socket.on timer: ping")
        }

        gSocketTimer?.on(clientEvent: .pong) {data, ack in
            print("socket.on timer: pong")
        }
    }
}

//MARK: Handle Emit Auth
extension SocketHelperTimer {
    func emitCallTimerEventTracking(event: EventTracking, appointmentId: Int?, error: APIError?) {
        var data: [String: Any] = [:]
        if let code = error?.errorCode {
            data.updateValue(code, forKey: "errorCode")
        }
        if let message = error?.message {
            data.updateValue(message, forKey: "errorMessage")
        }
        
        let params: [String: Any] = [
            "appointmentId": appointmentId ?? 0,
            "event": event.rawValue,
            "data": data
        ]
        
        gSocketTimer?.emit("tracking", params) {
            print("socket.emit timer: tracking: \(params)")
        }
    }
}
