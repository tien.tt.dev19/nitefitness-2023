//
//  AppDelegate.swift
//  1SK
//
//  Created by tuyenvx on 9/30/20.
//

import UIKit
import GoogleSignIn
import Firebase
import FirebaseAnalytics
import FirebaseCrashlytics
import FirebaseDynamicLinks
import FBSDKCoreKit
import IQKeyboardManagerSwift
import AVFAudio
import AppsFlyerLib
import OneSignal
import RealmSwift

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.portrait
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //UIFont.setInitFontBlaster()
        self.setInitReachabilityManager()
        self.setInitConfigGoogleFirebase()
        self.setInitUNUserNotificationCenter()
        self.setInitNotificationOneSignal(launchOptions: launchOptions)
        self.setInitAppsFlyer()
        self.setInitConfigRealm()
        self.setInitConfigIQKeyboard()
        self.setEnableSoundWhenModeSilent()
        
        self.setInitRootViewController()
        
        // Open Deep Link
        if let url = launchOptions?[.url] as? URL, let annotation = launchOptions?[.annotation] {
            return self.application(application, open: url, sourceApplication: launchOptions?[.sourceApplication] as? String, annotation: annotation)
        }
        
        return true
    }
    
//    override init() {
//        super.init()
        //UIFont.overrideInitialize()
//    }
    
    // MARK: ViewController
    func setInitRootViewController() {
        let iOSVersion = ProcessInfo().operatingSystemVersion.majorVersion
        if iOSVersion < 13 { // iOS version < 13.0.0
            // Facebook
            let frame = UIScreen.main.bounds
            let size = CGSize(width: min(frame.width, frame.height), height: max(frame.width, frame.height))
            window = UIWindow(frame: CGRect(origin: .zero, size: size))
            window?.rootViewController = SplashRouter.setupModule()
            window?.makeKeyAndVisible()
        }
    }
    
    func setInitReachabilityManager() {
        ReachabilityManager.shared.setStartNotifier()
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return self.orientationLock
    }

    // MARK: - RemoteNotification
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
        
        let deviceTokenHex = deviceToken.hexStringToken
        gDeviceToken = deviceTokenHex
        
        print("<AppDelegate> deviceToken: ", deviceTokenHex)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        //self.alertDefault(title: "Remote Notifications With Error", message: error.localizedDescription)
        print("Failed to register: \(error.localizedDescription)")
    }
    
    //MARK: - Foreground - Open App
    func applicationWillEnterForeground(_ application: UIApplication) {
        NotificationCenter.default.post(name: .applicationWillEnterForeground, object: nil)
        
//        AnalyticsHelper.setEvent(with: .APP_OPEN_NEXT)
//        if let id = gUser?.id, id > 0  {
//            AnalyticsHelper.setEvent(with: .APP_OPEN_LOGGED)
//        } else {
//            AnalyticsHelper.setEvent(with: .APP_OPEN_NONE)
//        }
    }
    
    //MARK: - Background - Off App
    func applicationDidEnterBackground(_ application: UIApplication) {
        NotificationCenter.default.post(name: .applicationDidEnterBackground, object: nil)
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        AppsFlyerLib.shared().start()
    }

    // MARK: - UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    // MARK: Open Deeplinks
    // Report Push Notification attribution data for re-engagements
    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        GIDSignIn.sharedInstance.handle(url)
        
        ApplicationDelegate.shared.application(application, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        
        print("<AppDelegate> options url: ", url)
        
        if url.absoluteString.contains("myapp://1sk.vn?code") {
            print("<Strava redirect url> \(url)")
            
            gStravaURL = url
            
            NotificationCenter.default.post(name: .applicationStravaUrlCallBack, object: nil)
        }
        
        return true
    }
    
    // Open URI-scheme for iOS 9 and above
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        print("<AppDelegate> Deeplinks options iOS 9 and above")
        return true
    }
    
    // Reports app open from deep link for iOS 10 or later
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        print("<AppDelegate> continue userActivity")
        guard let url = userActivity.webpageURL else {
            return false
        }
        print("<AppDelegate> Dynamic Links: ", url)
        
        NSURL(string: "\(url)")!.resolveWithCompletionHandler {
            
            gUniversalLink = $0
            
            print("<AppDelegate> Universal Link \(String(describing: gUniversalLink)))")
            
            let path = gUniversalLink?.path
            print("<AppDelegate> Universal Link path \(path ?? "nil")")
            
            let params = gUniversalLink?.params() ?? [:]
            print("<AppDelegate> Universal Link params \(params)")
            
            NotificationCenter.default.post(name: .applicationOpenDeepLinks, object: nil)
        }
        return true
    }
    
    // Open URI-scheme for iOS 8 and below
    private func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        print("<AppDelegate> Deeplinks userActivity dynamiclink iOS 8 and below")
        return true
    }
}

// MARK: - IQKeyboard
extension AppDelegate {
    private func setInitConfigIQKeyboard() {
        // IQKeyboard manager  
        IQKeyboardManager.shared.enable = true
    }
}

// MARK: - Realm DB
extension AppDelegate {
    private func setInitConfigRealm() {
        if APP_ENV == .DEV {
            //print("<Realm> setInitConfigRealm .DEV")
            let configuration = Realm.Configuration(
                // Set the new schema version. This must be greater than the previously used
                // version (if you've never set a schema version before, the version is 0).
                schemaVersion: 1,
                // Set the block which will be called automatically when opening a Realm with
                // a schema version lower than the one set above
                migrationBlock: { migration, oldSchemaVersion in
                    // We haven’t migrated anything yet, so oldSchemaVersion == 0
                    print("<Realm> setInitConfigRealm migration: \(migration) - oldSchemaVersion: \(oldSchemaVersion)")
                    if oldSchemaVersion < 1 {
                        // Nothing to do!
                        // Realm will automatically detect new properties and removed properties
                        // And will update the schema on disk automatically
                    }
                })

            // Tell Realm to use this new configuration object for the default Realm
            Realm.Configuration.defaultConfiguration = configuration
            
        } else {
            //print("<Realm> setInitConfigRealm .PRO")
            let encryptionKeyData = KeyChainManager.shared.encryptionKey
            
            // Migrate pre-existing (not encrypted) realm DB to a new encrypted one
            // Fixbug version 1.3.6
            // Check if the user has the unencrypted Realm
            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
            let fileManager = FileManager.default
            
            let path_default = "\(documentDirectory)/default.realm"
            let path_default_lock = "\(documentDirectory)/default.realm.lock"
            let path_default_management = "\(documentDirectory)/default.realm.management"
            
            let path_1sk_db = "\(documentDirectory)/1sk_db.realm"
            
            let isDefaultDbExsist = fileManager.fileExists(atPath: path_default)
            let is1skDbExsist = fileManager.fileExists(atPath: path_1sk_db)
            
            if isDefaultDbExsist && !is1skDbExsist {
                let unencryptedRealm = try? Realm(configuration: Realm.Configuration(schemaVersion: 1))
                try? unencryptedRealm?.writeCopy(toFile: URL(fileURLWithPath: path_1sk_db), encryptionKey: encryptionKeyData)
                print("<Realm> setInitConfigRealm .PRO writeCopy")
                try? fileManager.removeItem(at: URL(fileURLWithPath: path_default))
                try? fileManager.removeItem(at: URL(fileURLWithPath: path_default_lock))
                try? fileManager.removeItem(at: URL(fileURLWithPath: path_default_management))
                print("<Realm> setInitConfigRealm .PRO removeItem default")
            }
            
            let configuration = Realm.Configuration(
                fileURL: URL(fileURLWithPath: path_1sk_db),
                // Set the new schema version. This must be greater than the previously used
                // version (if you've never set a schema version before, the version is 0).
                encryptionKey: encryptionKeyData,
                schemaVersion: 1,
                // Set the block which will be called automatically when opening a Realm with
                // a schema version lower than the one set above
                migrationBlock: { migration, oldSchemaVersion in
                    // We haven’t migrated anything yet, so oldSchemaVersion == 0
                    print("<Realm> setInitConfigRealm migration: \(migration) - oldSchemaVersion: \(oldSchemaVersion)")
                    if oldSchemaVersion < 1 {
                        // Nothing to do!
                        // Realm will automatically detect new properties and removed properties
                        // And will update the schema on disk automatically
                    }
                })

            Realm.Configuration.defaultConfiguration = configuration
            
            print("<Realm> setInitConfigRealm .PRO defaultConfiguration success")
        }
    }
}

// MARK: - Firebase
extension AppDelegate {
    private func setInitConfigGoogleFirebase() {
        FirebaseApp.configure()
        
        // Google Sign in
        //GIDSignIn.sharedInstance.clientID = FirebaseApp.app()?.options.clientID
        
        if APP_ENV == .DEV {
            Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(false)
        } else {
            Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)
        }
        
        RemoteConfigManager.shared.initRemoteConfig()
    }
}

// MARK: MessagingDelegate
//extension AppDelegate: MessagingDelegate {
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//        gFCMToken = fcmToken
//
//        let dataDict: [String: Any] = ["token": fcmToken ?? ""]
//        NotificationCenter.default.post(name: .FCMToken, object: nil, userInfo: dataDict)
//        // TTODO:: If necessary send token to application server.
//        // Note: This callback is fired at each app startup and whenever a new token is generated.
//
//        print("<AppDelegate> fcmToken: \(String(describing: fcmToken ?? ""))")
//    }
//}

// MARK: - AppUtility Orientation
extension AppDelegate {
    struct AppUtility {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }

        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation: UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
            UINavigationController.attemptRotationToDeviceOrientation()
        }
    }
}

//MARK: OneSignal
extension AppDelegate {
    func setInitUNUserNotificationCenter() {
        // Set RemoteNotifications
        if #available(iOS 10.0, *) {
            // iOS 10 or later
            // For iOS 10 display notification (sent via APNS)
            //UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
            UIApplication.shared.registerForRemoteNotifications()

        } else {
            // iOS 9 support - Given for reference. This demo app supports iOS 13 and above
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
        
        //UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func setInitNotificationOneSignal(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        let ONESIGNAL_ID = "672effc9-feb0-436e-a3e1-ea31a8d1d404"
        if APP_ENV == .DEV {
            // Remove this method to stop OneSignal Debugging
            OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        }
        
        // OneSignal initialization
        OneSignal.initWithLaunchOptions(launchOptions)
        OneSignal.setAppId(ONESIGNAL_ID)

        self.setOSNotificationOpenedBlock()
        
        // promptForPushNotifications will show the native iOS notification permission prompt.
        // We recommend removing the following code and instead using an In-App Message to prompt for notification permission (See step 8)
        //OneSignal.promptForPushNotifications(userResponse: { accepted in
          //print("<OneSignal> User accepted notifications: \(accepted)")
        //})
    }
    
    func setOSNotificationOpenedBlock() {
        let osNotificationOpenedBlock: OSNotificationOpenedBlock = { result in
            let notification: OSNotification = result.notification

            if let additionalData = notification.additionalData as? [String: Any] {
                print("<OneSignal> additionalData: ", additionalData)
                let object = PushNotiModel.init(object: additionalData)
                gPushNotiModel = object
                NotificationCenter.default.post(name: .applicationOSNotificationOpenedBlock, object: object)
            }
        }
        OneSignal.setNotificationOpenedHandler(osNotificationOpenedBlock)
    }
}

//MARK: UNUserNotificationCenterDelegate
//extension AppDelegate: UNUserNotificationCenterDelegate {
//    func setInitUNUserNotificationCenter() {
//        // Set RemoteNotifications
//        if #available(iOS 10.0, *) {
//            // iOS 10 or later
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {_, _ in })
//            UIApplication.shared.registerForRemoteNotifications()
//
//        } else {
//            // iOS 9 support - Given for reference. This demo app supports iOS 13 and above
//            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
//            UIApplication.shared.registerForRemoteNotifications()
//        }
//
//        //UIApplication.shared.applicationIconBadgeNumber = 0
//    }
//
//    // UNUserNotificationCenterDelegate
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        print("AppDelegate didReceive response")
//        self.onProcessPushNotification(userInfo: response.notification.request.content.userInfo)
//    }
//
//    // UNUserNotificationCenterDelegate
//    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        print("AppDelegate willPresent notification")
//
//        // Show Push Notication when App open
//        completionHandler( [.alert, .badge, .sound])
//        //completionHandler( [.alert, .badge])
//
//        // Just sound Push Notification when App open
//        // create a sound ID, in this case its the SMSReceived sound.
//        //let systemSoundID: SystemSoundID = 1007
//        // to play sound
//        //AudioServicesPlayAlertSound(systemSoundID)
//
//        self.onProcessPushNotification(userInfo: notification.request.content.userInfo)
//    }
//
//    // UNUserNotificationCenterDelegate
//    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
//        print("AppDelegate openSettingsFor notification")
//    }
//
//    func onProcessPushNotification(userInfo: [AnyHashable: Any]) {
//        //print("AppDelegate onProcessPushNotificationRoot: ", userInfo as? [String: Any])
//
//        if let aps = userInfo["aps"] as? [String: Any] {
//            print("AppDelegate onProcessPushNotification: ", aps)
//        }
//    }
//
//    // MARK: Handle Event Notification
//    func setHandleEventNotification(action: String, sevice: Int, state: UIApplication.State, userInfo: [AnyHashable: Any]) {
//
//    }
//
//    func alertDefault(title: String, message: String) {
//        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
//        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
//    }
//}

// MARK: Mode Device Silent
extension AppDelegate {
    func setEnableSoundWhenModeSilent() {
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
    }
}

// MARK: AppsFlyerLibDelegate
extension AppDelegate: AppsFlyerLibDelegate {
    func setInitAppsFlyer() {
        AppsFlyerLib.shared().appsFlyerDevKey = "sj2MQGdMT7p2APwVEE9dBn" //"<AF_DEV_KEY>"
        AppsFlyerLib.shared().appleAppID = "id1554801349" //"<APPLE_APP_ID>"
        AppsFlyerLib.shared().delegate = self
        AppsFlyerLib.shared().isDebug = false
        AppsFlyerLib.shared().waitForATTUserAuthorization(timeoutInterval: 60)
        
        // SceneDelegate support
        NotificationCenter.default.addObserver(self, selector: #selector(self.onSendLaunch), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        AppsFlyerLib.shared().start(completionHandler: { (dictionary, error) in
            if error != nil {
                print("<AppsFlyer> completionHandler error:", error ?? "")
                return
                
            } else {
                print("<AppsFlyer> completionHandler success:", dictionary ?? "")
                return
            }
        })
    }
    
    // SceneDelegate support - start AppsFlyer SDK
    @objc func onSendLaunch() {
        AppsFlyerLib.shared().start()
    }
    
    // Handle Organic/Non-organic installation
    func onConversionDataSuccess(_ installData: [AnyHashable: Any]) {
        Log.println("<AppsFlyer> onConversionDataSuccess", installData)
        
        if let status = installData["af_status"] as? String {
            if status == "Non-organic" {
                if let sourceID = installData["media_source"],
                   let campaign = installData["campaign"] {
                    Log.println("<AppsFlyer> onConversionDataSuccess: This is a Non-Organic install. Media source: \(sourceID)  Campaign: \(campaign)")
                }
            } else {
                Log.println("<AppsFlyer> onConversionDataSuccess: This is an organic install.")
            }
            
            if let is_first_launch = installData["is_first_launch"] as? Bool,
               is_first_launch {
                Log.println("<AppsFlyer> onConversionDataSuccess: First Launch")
                
            } else {
                Log.println("<AppsFlyer> onConversionDataSuccess: Not First Launch")
            }
        }
        
        // Get conversion data
        guard let first_launch_flag = installData["is_first_launch"] as? Int else {
            Log.println("<AppsFlyer> onConversionDataSuccess: first_launch_flag: nil")
            return
        }
        
        guard let status = installData["af_status"] as? String else {
            Log.println("<AppsFlyer> onConversionDataSuccess: status: nil")
            return
        }
        
        if first_launch_flag == 1 {
            if status == "Non-organic" {
                if let media_source = installData["media_source"], let campaign = installData["campaign"] {
                    Log.println("<AppsFlyer> This is a Non-Organic install. Media source: \(media_source) Campaign: \(campaign)")
                }
            } else {
                Log.println("<AppsFlyer> This is an organic install.")
            }
        } else {
            Log.println("<AppsFlyer> Not First Launch")
        }
    }

    func onConversionDataFail(_ error: Error) {
        Log.println("<AppsFlyer> onConversionDataFail:", error.localizedDescription)
    }

    //Handle Deep Link
    func onAppOpenAttribution(_ attributionData: [AnyHashable: Any]) {
        //Handle Deep Link Data
        Log.println("<AppsFlyer> onAppOpenAttribution data:", attributionData)
    }

    func onAppOpenAttributionFailure(_ error: Error) {
        Log.println("<AppsFlyer> onAppOpenAttributionFailure: ", error.localizedDescription)
    }
    
}
