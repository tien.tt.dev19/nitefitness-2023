//
//  SceneDelegate.swift
//  1SK
//
//  Created by tuyenvx on 9/30/20.
//

import UIKit
import FBSDKCoreKit
import GoogleSignIn
import FirebaseDynamicLinks
import AVFAudio

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    
    var window: UIWindow?
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        self.setEnableSoundWhenModeSilent()
        self.setInitRootViewController(scene: scene)
        
        // Open DeepLink
        if let userActivity = connectionOptions.userActivities.first {
            self.scene(scene, continue: userActivity)
            
        } else {
            self.scene(scene, openURLContexts: connectionOptions.urlContexts)
        }
        
        //UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "BeVietnamPro"))
    }
    
    func setInitRootViewController(scene: UIScene) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let frame = windowScene.coordinateSpace.bounds
        let size = CGSize(width: min(frame.width, frame.height), height: max(frame.width, frame.height))
        window = UIWindow(frame: CGRect(origin: .zero, size: size))
        window?.windowScene = windowScene
        window?.rootViewController = SplashRouter.setupModule()
        window?.makeKeyAndVisible()
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else { return }
        GIDSignIn.sharedInstance.handle(url)
        ApplicationDelegate.shared.application(UIApplication.shared, open: url, sourceApplication: nil, annotation: [UIApplication.OpenURLOptionsKey.annotation])
        
        print("<AppDelegate> openURLContexts: ", url)
        
        if url.absoluteString.contains("myapp://1sk.vn?code") {
            print("<Strava redirect url> \(url)")
            
            gStravaURL = url
            
            NotificationCenter.default.post(name: .applicationStravaUrlCallBack, object: nil)
        } else {
            NSURL(string: "\(url)")!.resolveWithCompletionHandler {
                gUniversalLink = $0
            }
        }
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }
    
    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }
    
    func sceneWillEnterForeground(_ scene: UIScene) {
        NotificationCenter.default.post(name: .applicationWillEnterForeground, object: nil)
    }
    
    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        NotificationCenter.default.post(name: .applicationDidEnterBackground, object: nil)
    }
    
    func scene(_ scene: UIScene, continue userActivity: NSUserActivity) {
        guard let url = userActivity.webpageURL else {
            return
        }
        print("<AppDelegate> Dynamic Links: ", url)
        
        NSURL(string: "\(url)")!.resolveWithCompletionHandler {
            gUniversalLink = $0
            
            print("<AppDelegate> Universal Link \(String(describing: gUniversalLink)))")
            
            let path = gUniversalLink?.path
            print("<AppDelegate> Universal Link path \(path ?? "nil")")
            
            let params = gUniversalLink?.params() ?? [:]
            print("<AppDelegate> Universal Link params \(params)")
            
            NotificationCenter.default.post(name: .applicationOpenDeepLinks, object: nil)
        }
    }
}

// MARK: Mode Device Silent
@available(iOS 13.0, *)
extension SceneDelegate {
    func setEnableSoundWhenModeSilent() {
        try? AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
    }
}
