//
//  AlertConfirmSuccessViewController.swift
//  1SK
//
//  Created by Thaad on 18/05/2022.
//

import UIKit

protocol AlertConfirmSuccessViewDelegate: AnyObject {
    func onDidDismissAlertConfirmSuccessView()
}

class AlertConfirmSuccessViewController: UIViewController {
    weak var delegate: AlertConfirmSuccessViewDelegate?
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    var image = R.image.ic_register_success()
    var titleAlert = "Thành công"
    var contentAlert = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lbl_Title.text = self.titleAlert
        self.img_Image.image = self.image
        
        if self.contentAlert.count > 0 {
            self.lbl_Content.text = self.contentAlert
            self.lbl_Content.isHidden = false
        } else {
            self.lbl_Content.isHidden = true
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.delegate?.onDidDismissAlertConfirmSuccessView()
            self.hiddenAnimate()
        }
    }
}

// MARK: - Init Animation
extension AlertConfirmSuccessViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
