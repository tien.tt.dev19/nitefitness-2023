//
//  AlertDatePickerViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/18/21.
//

import UIKit

protocol AlertDatePickerViewDelegate: AnyObject {
    func onAlertDatePickerCancelAction()
    func onAlertDatePickerAgreeAction(day: String)
}

class AlertDatePickerViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var picker_Date: UIDatePicker!
    
    weak var delegate: AlertDatePickerViewDelegate?
    var alertTitle = ""
    var dateSelect = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitLayoutView()
        self.setInitGestureView()
        self.setInitUIDatePicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    func setInitLayoutView() {
        self.lbl_Title.text = self.alertTitle
    }
    
    func setInitUIDatePicker() {
        if #available(iOS 13.4, *) {
            self.picker_Date.preferredDatePickerStyle = .wheels
        } else {
            self.picker_Date.setValue(true, forKey: "highlightsToday")
        }
        self.picker_Date.datePickerMode = .date
        self.picker_Date.minimumDate = Calendar.current.date(byAdding: .year, value: -122, to: Date().noon)!
        self.picker_Date.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date().noon)!
        
        if let date = Utils.shared.getDateByString(dateStr: self.dateSelect) {
            self.picker_Date.date = date
        } else {
            if let date = Utils.shared.getDateByString(dateStr: "01/01/1985") {
                self.picker_Date.date = date
            }
        }
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertDatePickerCancelAction()
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.dateSelect = dateFormatter.string(from: self.picker_Date.date)
        
        self.delegate?.onAlertDatePickerAgreeAction(day: self.dateSelect)
        self.setAnimationHidden()
    }

}

// MARK: - Animation
extension AlertDatePickerViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertDatePickerViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}
