//
//  AlertMoreDropdownViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/7/21.
//

import UIKit

protocol AlertMoreDropdownViewDelegate: AnyObject {
    func onDidSelectItemMore(item: ItemMoreDropdown)
}

class AlertMoreDropdownViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var constraint_height_ContentView: NSLayoutConstraint!
    
    weak var delegate: AlertMoreDropdownViewDelegate?
    
    var listItemMoreDropdown: [ItemMoreDropdown] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.setInitUITableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
}

// MARK: - Gesture View
extension AlertMoreDropdownViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertMoreDropdownViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.5
            self.constraint_height_ContentView.constant = CGFloat(self.listItemMoreDropdown.count * 40)
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            //
        }
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.3) {
            self.view_Bg.alpha = 0.0
            self.constraint_height_ContentView.constant = 0
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension AlertMoreDropdownViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewItemMore.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listItemMoreDropdown.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewItemMore.self, for: indexPath)
        let item = self.listItemMoreDropdown[indexPath.row]
        cell.config(item: item)
        return cell
    }
}

// MARK: - UITableViewDataSource
extension AlertMoreDropdownViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.listItemMoreDropdown[indexPath.row]
        self.delegate?.onDidSelectItemMore(item: item)
        
        self.setAnimationHidden()
    }
}
