//
//  CellTableViewItemMore.swift
//  1SK
//
//  Created by vuongbachthu on 10/7/21.
//

import UIKit

class CellTableViewItemMore: UITableViewCell {

    @IBOutlet weak var lbl_ItemName: UILabel!
    @IBOutlet weak var view_LineItem: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(item: ItemMoreDropdown) {
        self.lbl_ItemName.text = item.name
        self.view_LineItem.isHidden = item.isHiddenLine ?? false
    }
}
