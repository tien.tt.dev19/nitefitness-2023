//
//  ItemMoreDropdown.swift
//  1SK
//
//  Created by vuongbachthu on 10/7/21.
//

import Foundation

class ItemMoreDropdown: NSObject {
    var index: Int?
    var name: String?
    var isHiddenLine: Bool?
    
    override init() {
        //
    }
    
    init(index: Int, name: String, isHiddenLine: Bool) {
        self.index = index
        self.name = name
        self.isHiddenLine = isHiddenLine
    }
}
