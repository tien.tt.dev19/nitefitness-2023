//
//  AlertPickerViewViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/18/21.
//

import UIKit

class ItemPickerView: NSObject {
    var id: Int?
    var name: String?
    var isSelected: Bool?
}

protocol AlertPickerViewDelegate: AnyObject {
    func onAlertCancelAction()
    func onAlertAgreeAction(item: ItemPickerView)
}

class AlertPickerViewViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var picker_View: UIPickerView!
    
    weak var delegate: AlertPickerViewDelegate?
    
    var alertTitle = ""
    var listItemPickerView: [ItemPickerView] = []
    var itemSelected: ItemPickerView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitLayoutView()
        self.setInitGestureView()
        self.setInitUIPickerView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
        self.setSelectRow()
    }
    
    func setInitLayoutView() {
        self.lbl_Title.text = self.alertTitle
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertAgreeAction(item: self.itemSelected!)
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertPickerViewViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertPickerViewViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}

//MARK: UIPickerViewDataSource
extension AlertPickerViewViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func setInitUIPickerView() {
        self.picker_View.dataSource = self
        self.picker_View.delegate = self
    }
    
    func setSelectRow() {
        if let index = self.listItemPickerView.firstIndex(where: {$0.isSelected == true}) {
            self.picker_View.selectRow(index, inComponent: 0, animated: true)
            self.itemSelected = self.listItemPickerView[index]
            
        } else {
            self.itemSelected = self.listItemPickerView[0]
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.listItemPickerView.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.listItemPickerView[row].name ?? "Item \(row)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.itemSelected = self.listItemPickerView[row]
    }
}
