//
//  AlertPopupConfirmTitle_2_ViewController.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//

import UIKit

protocol AlertPopupConfirmTitle_2_ViewDelegate: AnyObject {
    func onAlertPopupConfirm_2_CancelAction(object: Any?)
    func onAlertPopupConfirm_2_AgreeAction(object: Any?)
}

class AlertPopupConfirmTitle_2_ViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Agree: UIButton!
    
    weak var delegate: AlertPopupConfirmTitle_2_ViewDelegate?
    var object: Any?
    var isTapBgDismiss = true
    
    var lblTitle: String?
    var lblContent: String?
    var btnTitleCancel: String?
    var btnTitleAgree: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    func setupUI() {
        if let text = self.lblTitle {
            self.lbl_Title.text = text
        }
        if let text = self.lblContent {
            self.lbl_Content.text = text
        }
        
        if let text = self.btnTitleCancel {
            self.btn_Cancel.setTitle(text, for: .normal)
        }
        if let text = self.btnTitleAgree {
            self.btn_Agree.setTitle(text, for: .normal)
        }
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertPopupConfirm_2_CancelAction(object: self.object)
        self.hiddenAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertPopupConfirm_2_AgreeAction(object: self.object)
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension AlertPopupConfirmTitle_2_ViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertPopupConfirmTitle_2_ViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        guard self.isTapBgDismiss else {
            return
        }
        self.delegate?.onAlertPopupConfirm_2_CancelAction(object: self.object)
        self.hiddenAnimate()
    }
}
