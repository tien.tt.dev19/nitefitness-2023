//
//  AlertPopupConfirmTitleLandscapeViewController.swift
//  1SK
//
//  Created by Thaad on 19/04/2022.
//

import UIKit

protocol AlertPopupConfirmTitleLandscapeViewDelegate: AnyObject {
    func onAlertPopupConfirmCancelAction(object: Any?, tag: Int)
    func onAlertPopupConfirmAgreeAction(object: Any?, tag: Int)
}

class AlertPopupConfirmTitleLandscapeViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Agree: UIButton!
    
    weak var delegate: AlertPopupConfirmTitleLandscapeViewDelegate?
    var object: Any?
    var isTapBgDismiss = true
    
    var lblTitle: String?
    var lblContent: String?
    var btnTitleCancel: String?
    var btnTitleAgree: String?
    
    var tag = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    func setupUI() {
        if let text = self.lblTitle {
            self.lbl_Title.text = text
            self.lbl_Title.isHidden = false
            
        } else {
            self.lbl_Title.isHidden = true
        }
        
        if let text = self.lblContent {
            self.lbl_Content.text = text
            self.lbl_Content.isHidden = false
            
        } else {
            self.lbl_Content.isHidden = true
        }
        
        if let text = self.btnTitleCancel {
            self.btn_Cancel.setTitle(text, for: .normal)
        }
        if let text = self.btnTitleAgree {
            self.btn_Agree.setTitle(text, for: .normal)
        }
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertPopupConfirmCancelAction(object: self.object, tag: self.tag)
        self.hiddenAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertPopupConfirmAgreeAction(object: self.object, tag: self.tag)
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension AlertPopupConfirmTitleLandscapeViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertPopupConfirmTitleLandscapeViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        guard self.isTapBgDismiss else {
            return
        }
        self.delegate?.onAlertPopupConfirmCancelAction(object: self.object, tag: self.tag)
        self.hiddenAnimate()
    }
}
