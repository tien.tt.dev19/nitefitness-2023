//
//  AlertConfirmIconViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//

import UIKit

protocol AlertSheetConfirmIconViewDelegate: AnyObject {
    func onAlertConfirmCancelAction()
    func onAlertConfirmAgreeAction()
}

class AlertSheetConfirmIconViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Agree: UIButton!
    
    weak var delegate: AlertSheetConfirmIconViewDelegate?
    
    var alertImage: UIImage?
    var alertContent: String?
    var btnTitleCancel: String?
    var btnTitleAgree: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.onSetData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    func onSetData() {
        self.lbl_Content.text = self.alertContent
        self.img_Image.image = self.alertImage
        
        if let title = self.btnTitleCancel {
            self.btn_Cancel.setTitle(title, for: .normal)
        }
        if let title = self.btnTitleAgree {
            self.btn_Agree.setTitle(title, for: .normal)
        }
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertConfirmCancelAction()
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertConfirmAgreeAction()
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertSheetConfirmIconViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertSheetConfirmIconViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}
