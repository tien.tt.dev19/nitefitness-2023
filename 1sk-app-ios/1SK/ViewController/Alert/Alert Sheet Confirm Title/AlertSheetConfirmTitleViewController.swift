//
//  AlertSheetConfirmTitleViewController.swift
//  1SK
//
//  Created by Thaad on 31/03/2022.
//

import UIKit

protocol AlertSheetConfirmTitleViewDelegate: AnyObject {
    func onAlertConfirmCancelAction()
    func onAlertConfirmAgreeAction()
}

class AlertSheetConfirmTitleViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var btn_Cancel: UIButton!
    @IBOutlet weak var btn_Agree: UIButton!
    
    weak var delegate: AlertSheetConfirmTitleViewDelegate?
    
    var lblTitle: String?
    var lblContent: String?
    var btnTitleCancel: String?
    var btnTitleAgree: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    func setupUI() {
        self.lbl_Title.text = self.lblTitle
        self.lbl_Content.text = self.lblContent
        self.btn_Cancel.setTitle(self.btnTitleCancel, for: .normal)
        self.btn_Agree.setTitle(self.btnTitleAgree, for: .normal)
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertConfirmCancelAction()
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertConfirmAgreeAction()
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertSheetConfirmTitleViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn) {
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ContentView.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertSheetConfirmTitleViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}
