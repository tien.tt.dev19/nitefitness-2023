//
//  VideoCallViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/4/21.
//

import UIKit
import AgoraRtcKit
import Toast_Swift

protocol AgoraVideoCallViewDelegate: AnyObject {
    func onEndCallAction(videoCall: VideoCallModel?)
}

class AgoraVideoCallViewController: UIViewController {
    
    @IBOutlet weak var view_Remote: UIView!
    @IBOutlet weak var view_Local: UIView!
    
    @IBOutlet weak var view_LocalMask: UIView!
    @IBOutlet weak var view_RemoteMask: UIView!
    
    @IBOutlet weak var img_AvatarDoctor: UIImageView!
    @IBOutlet weak var img_AvatarUser: UIImageView!
    
    @IBOutlet weak var btn_SwitchCamera: UIButton!
    @IBOutlet weak var btn_MuteCamera: UIButton!
    @IBOutlet weak var btn_EndCall: UIButton!
    @IBOutlet weak var btn_MuteVoice: UIButton!
    
    @IBOutlet weak var view_Warning: UIView!
    @IBOutlet weak var btn_Warning: UIButton!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_UserJoin: UILabel!
    @IBOutlet weak var lbl_TimeCountdown: UILabel!
    @IBOutlet weak var lbl_StatusAgora: UILabel!
    
    weak var delegate: AgoraVideoCallViewDelegate?
    let careService = CareService()
    
    var agoraKit: AgoraRtcEngineKit?
    var videoCall: VideoCallModel?
    
    var appId = ""
    var channelId = ""
    var tokenAgora = ""
    var uidAgora: UInt?
    var remoteId: UInt?
    
    var timerCallCountdow = Timer()
    var timeCallDuration: TimeInterval = 0
    var timeDurationWaiting: TimeInterval = 0
    
    var timeDurationEndCall: TimeInterval = 1800
    
    var isFinishAppointment = false
    var isFinishVideoCall = false
    
    private var isVideoRemoteMuted = false
    
    private var userJoin = 1 {
        didSet {
            self.lbl_UserJoin.text = userJoin.asString
        }
    }
    
    private var isSwitchCamera = true {
        didSet {
            self.agoraKit?.switchCamera()
        }
    }
    
    private var isSpeakerPhone = true {
        didSet {
            guard oldValue != isSpeakerPhone else {
                return
            }
            
            // switch playout audio route
            self.agoraKit?.setEnableSpeakerphone(isSpeakerPhone)
        }
    }
    
    private var isVideoLocalMuted = false {
        didSet {
            self.agoraKit?.enableLocalVideo(isVideoLocalMuted)
        }
    }
    
    private var isAudioMuted = false {
        didSet {
            guard oldValue != isAudioMuted else {
                return
            }
            // mute local audio
            self.agoraKit?.muteLocalAudioStream(isAudioMuted)
        }
    }
    
    
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setEnvironmentData()
        self.setInitLayoutView()
        self.setInitUIApplication()
        
        self.setInitTimerCall()
        self.setInitHandleSocketEvent()
        self.setObserverConnection()
        self.setJoinRoomVideoCall()
        
        self.setInitAgoraKit()
        
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_focused, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.setLeaveChannel()
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    func setEnvironmentData() {
        if APP_ENV == .DEV {
            self.timeDurationEndCall = 900
        } else {
            self.timeDurationEndCall = 1800
        }
    }
    
    func setInitLayoutView() {
        self.uidAgora = UInt(self.videoCall?.uid ?? 0)
        self.appId = self.videoCall?.appId ?? ""
        self.channelId = self.videoCall?.channelName ?? ""
        self.tokenAgora = self.videoCall?.token ?? ""
        
        self.btn_MuteCamera.isSelected = true
        self.btn_MuteVoice.isSelected = true
        
        self.btn_Warning.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let specialist = self.videoCall?.appointment?.doctor?.specialist?.first
        self.lbl_Title.text = "Tư vấn \(specialist?.name?.lowercased() ?? "chuyên Khoa") qua video"
        self.setRemoteMask(isHidden: false, status: "Bác sĩ chưa xuất hiện. Vui lòng chờ trong giây lát hoặc gọi Tổng đài: \(gPhoneHotline ?? "")")
    }
    
    func setJoinRoomVideoCall() {
        if let id = self.videoCall?.appointment?.id {
            self.careService.setJoinRoomVideoCall(appointmentId: id) { result in
                switch result.unwrapSuccessModel() {
                case .success:
                    break
                    
                case .failure(let error):
                    debugPrint(error)
                    SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_error, appointmentId: self.videoCall?.appointment?.id, error: error)
                }
                
                print("setJoinRoomVideoCall")
            }
        }
    }
    
    func setRemoteMask(isHidden: Bool, status: String) {
        self.lbl_StatusAgora.text = status
        self.view_RemoteMask.isHidden = isHidden
    }

    @IBAction func onMinisizeAction(_ sender: Any) {
        //
    }
    
    @IBAction func onSwitchCameraAction(_ sender: Any) {
        self.isSwitchCamera.toggle()
    }
    
    @IBAction func onMuteCameraAction(_ sender: Any) {
        self.btn_MuteCamera.isSelected = !self.btn_MuteCamera.isSelected
        self.isVideoLocalMuted = self.btn_MuteCamera.isSelected
        
        if self.btn_MuteCamera.isSelected == true {
            UIView.animate(withDuration: 1.0) {
                self.view_LocalMask.alpha = 0.0

            } completion: { _ in
                self.view_LocalMask.isHidden = true
            }

            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_enabled_camera, appointmentId: self.videoCall?.appointment?.id, error: nil)
            
        } else {
            self.view_LocalMask.alpha = 0.0
            self.view_LocalMask.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_LocalMask.alpha = 1.0
            }
            
            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_disabled_camera, appointmentId: self.videoCall?.appointment?.id, error: nil)
        }
    }
    
    @IBAction func onMuteVoiceAction(_ sender: Any) {
        self.btn_MuteVoice.isSelected = !self.btn_MuteVoice.isSelected
        self.agoraKit?.enableLocalAudio(self.btn_MuteVoice.isSelected)
        
        if self.btn_MuteVoice.isSelected == true {
            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_enabled_mic, appointmentId: self.videoCall?.appointment?.id, error: nil)
        } else {
            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_disabled_mic, appointmentId: self.videoCall?.appointment?.id, error: nil)
        }
    }
    
    @IBAction func onEndCallAction(_ sender: Any) {
        if self.timeCallDuration > 0 {
            if self.timeCallDuration <= self.timeDurationEndCall {
                let controller = AlertConfirmEndCallViewController()
                controller.view.frame = UIScreen.main.bounds
                controller.delegate = self
                controller.showInView(self, animated: true)
                
            } else {
                self.dismiss(animated: true)
            }
            
        } else {
            self.onSetFinishAppointment()
        }
    }
}

// MARK: - Timer Call
extension AgoraVideoCallViewController {
    func setInitTimerCall() {
        let expiredAt = self.videoCall?.appointment?.doctorAvailableTime?.endAt ?? 00
        self.timeCallDuration = Double(expiredAt) - Double(gTimeSystem ?? 0)
        self.timeDurationWaiting = self.timeCallDuration - self.timeDurationEndCall
        
        self.timerCallCountdow = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimeCountdown), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimeCountdown() {
        if self.timeCallDuration > 0 {
            self.timeCallDuration -= 1
            self.timeDurationWaiting -= 1
            
            if self.timeCallDuration <= self.timeDurationEndCall {
                self.view_Warning.isHidden = true
                self.lbl_TimeCountdown.isHidden = false
                self.lbl_TimeCountdown.text = self.timeCallDuration.asTimeFormat
                
            } else {
                self.view_Warning.isHidden = true
                self.lbl_TimeCountdown.isHidden = true
                
//                self.view_Warning.isHidden = false
//                self.btn_Warning.setTitle("Chưa đến giờ tư vấn", for: .normal)
//                self.lbl_TimeCountdown.text = "00:00" //self.timeDurationWaiting.asTimeFormat
            }
            
            if self.timeCallDuration < 60 && self.view_Warning.isHidden == true {
                self.view_Warning.isHidden = false
            }
            
        } else {
            self.timeCallDuration -= 1
            self.view_Warning.isHidden = false
            self.lbl_TimeCountdown.isHidden = false
            self.btn_Warning.setTitle("Thời gian tư vấn đã hết", for: .normal)
            self.lbl_TimeCountdown.text = "00:00" //self.timeCallDuration.asTimeFormat //
            
            if self.timeCallDuration < -290 {
                self.onStopTimerCallCountdown()
                self.onSetFinishAppointment()
            }
        }
        
//        print("<AgoraKit> updateTimeCountdown \(self.lbl_TimeCountdown.text ?? "00:00") ")
    }
    
    func onStopTimerCallCountdown() {
        self.timerCallCountdow.invalidate()
        print("onStopTimerCallCountdown")
    }
}

// MARK: - AgoraRtcEngineKit
private extension AgoraVideoCallViewController {
    func setInitAgoraKit() {
        if gAppSettings.enableAgoraArea == true {
            // Specify the region for connection as North America.
            let config = AgoraRtcEngineConfig()
            config.appId = self.appId
            config.areaCode = AgoraAreaCode.AS.rawValue
            self.agoraKit = AgoraRtcEngineKit.sharedEngine(with: config, delegate: self)
            print("<AgoraKit> AgoraAreaCode: AS")
            
        } else {
            // Pass in your App ID here
            self.agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: self.appId, delegate: self)
            print("<AgoraKit> AgoraAreaCode: Auto")
        }
        
        print("<AgoraKit> setInitAgoraKit appId:", self.appId)
        print("<AgoraKit> setInitAgoraKit channelId:", self.channelId)
        print("<AgoraKit> setInitAgoraKit tokenAgora:", self.tokenAgora)
        print("<AgoraKit> setInitAgoraKit uidAgora:", self.uidAgora ?? 0)
        
        // Video is disabled by default. You need to call enableVideo to start a video stream.
        self.agoraKit?.enableVideo()
        self.agoraKit?.setEnableSpeakerphone(true)
        
        self.agoraKit?.setVideoEncoderConfiguration(AgoraVideoEncoderConfiguration.init(size: CGSize(width: 1280, height: 720), frameRate: AgoraVideoFrameRate.fps30, bitrate: 1710, orientationMode: .fixedPortrait))
        
        // Create a videoCanvas to render the local video
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = self.uidAgora!
        videoCanvas.renderMode = .hidden
        videoCanvas.view = self.view_Local
        self.agoraKit?.setupLocalVideo(videoCanvas)

        // Join the channel with a token. Pass in your token and channel name here
        self.agoraKit?.joinChannel(byToken: self.tokenAgora, channelId: self.channelId, info: nil, uid: self.uidAgora!, joinSuccess: { (channel, uid, elapsed) in
            
            print("<AgoraKit> joinChannel: channel = \(channel), uid = \(uid)")
            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_joined_room, appointmentId: self.videoCall?.appointment?.id, error: nil)
        })
        
        let avatar = self.videoCall?.appointment?.doctor?.basicInformation?.avatar ?? ""
        self.img_AvatarDoctor.setImageWith(imageUrl: avatar, placeHolder: R.image.default_avatar_2())
        self.img_AvatarUser.setImageWith(imageUrl: gUser?.avatar ?? "", placeHolder: R.image.default_avatar_2())
    }
}

// MARK: - AgoraRtcEngineDelegate
extension AgoraVideoCallViewController: AgoraRtcEngineDelegate {
    // This callback is triggered when a remote user joins the channel
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        if uid != self.remoteId {
            self.remoteId = uid
            let videoCanvas = AgoraRtcVideoCanvas()
            videoCanvas.uid = uid
            videoCanvas.renderMode = .hidden
            videoCanvas.view = self.view_Remote
            self.agoraKit?.setupRemoteVideo(videoCanvas)
            
            
            self.setRemoteMask(isHidden: false, status: "")
            self.userJoin = 2
        }
        
        print("<AgoraKit> didJoinedOfUid uid:", uid)
    }
    
    /// Occurs when the connection between the SDK and the server is interrupted.
    func rtcEngineConnectionDidInterrupted(_ engine: AgoraRtcEngineKit) {
        print("<AgoraKit> rtcEngineConnectionDidInterrupted ")
        //self.setRemoteMask(isHidden: false, status: "Đang kết nối cuộc gọi...")
    }
    
    /// Occurs when the SDK cannot reconnect to Agora’s edge server 10 seconds after its connection to the server is interrupted.
    func rtcEngineConnectionDidLost(_ engine: AgoraRtcEngineKit) {
        print("<AgoraKit> rtcEngineConnectionDidLost ")
        if let endAt = self.videoCall?.appointment?.doctorAvailableTime?.endAt {
            if gTimeSystem ?? 0 < endAt {
                self.setRemoteMask(isHidden: false, status: "Mất kết nối cuộc gọi.\nVui lòng kiểm tra lại kết nối mạng của bạn")
            }
        }
    }
    
    /// Reports an error during SDK runtime.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("<AgoraKit> rtcEngine Occur error: \(errorCode.rawValue)")
        
        let error = APIError.init(message: "Agora - error during SDK runtime ", statusCode: errorCode.rawValue)
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_error, appointmentId: self.videoCall?.appointment?.id, error: error)
    }
    
    //MARK: first local
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        print("<AgoraKit> firstLocalVideoFrameWith")
    }
    
    //MARK: first remote
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        if uid == self.remoteId {
            if self.userJoin == 1 {
                self.setRemoteMask(isHidden: !self.isVideoRemoteMuted, status: "")
            }
            
            self.userJoin = 2
        }
        
        print("<AgoraKit> firstRemoteVideoDecodedOfUid uid:", uid)
    }
    
    //MARK: video muted
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted: Bool, byUid uid: UInt) {
        if uid == self.remoteId {
            self.isVideoRemoteMuted = muted
            self.setRemoteMask(isHidden: !muted, status: "")
        }
        
        print("<AgoraKit> didVideoMuted video muted:", muted)
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didLocalVideoEnabled enabled: Bool, byUid uid: UInt) {
        if uid == self.remoteId {
            self.isVideoRemoteMuted = !enabled
            self.setRemoteMask(isHidden: enabled, status: "")
        }
        
        print("<AgoraKit> didLocalVideoEnabled video muted:", enabled)
    }
    
    //MARK: user offline
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        if uid == self.remoteId {
            self.userJoin = 1
            self.setRemoteMask(isHidden: false, status: "Kết nối của Bác sĩ không ổn định.\nVui lòng chờ trong giây lát")
        }
        print("<AgoraKit> didOfflineOfUid uid:", uid)
    }
    
    //MARK: remote stat
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStats stats: AgoraRtcRemoteVideoStats) {
        //print("<AgoraKit> remoteVideoStats remote stats")
    }
    
    //MARK: audio mixing
    func rtcEngineLocalAudioMixingDidFinish(_ engine: AgoraRtcEngineKit) {
        print("<AgoraKit> rtcEngineLocalAudioMixingDidFinish audio mixing")
    }
    
    //MARK: did banned
    func rtcEngineConnectionDidBanned(_ engine: AgoraRtcEngineKit) {
        print("<AgoraKit> rtcEngineConnectionDidBanned")
        SKToast.shared.showToast(content: "Đã kết nối cuộc gọi bằng thiết bị khác")
        self.dismiss(animated: true)
    }
    
    func setLeaveChannel() {
        self.onStopTimerCallCountdown()
        self.agoraKit?.leaveChannel(nil)
        AgoraRtcEngineKit.destroy()
        print("<AgoraKit> leaveChannel Done")
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_leaved_room, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
    
    func alert(string: String) {
        guard !string.isEmpty else {
            return
        }
        SKToast.shared.showToast(content: string)
    }
}

// MARK: - Internet Connection
extension AgoraVideoCallViewController {
    func setObserverConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionAvailable), name: .connectionAvailable, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionNotAvailable), name: .connectionUnavailable, object: nil)
    }
    
    @objc func onInternetConnectionAvailable() {
        print("<AgoraKit>  onInternetConnectionAvailable")
        if userJoin == 1 {
            self.setRemoteMask(isHidden: false, status: "Bác sĩ chưa xuất hiện. Vui lòng chờ trong giây lát hoặc gọi Tổng đài: \(gPhoneHotline ?? "")")
        } else {
            self.setRemoteMask(isHidden: true, status: "")
        }
    }
    
    @objc func onInternetConnectionNotAvailable() {
        self.setRemoteMask(isHidden: false, status: "Mất kết nối Internet.\nVui lòng kiểm tra lại kết nối của bạn")
        print("<AgoraKit>  onInternetConnectionNotAvailable")
    }
}

// MARK: - UIApplication
extension AgoraVideoCallViewController {
    func setInitUIApplication() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppWillEnterForeground), name: .applicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppDidEnterBackground), name: .applicationDidEnterBackground, object: nil)
        
    }
    
    @objc func onAppWillEnterForeground() {
        print("<AgoraKit> onAppWillEnterForeground")
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_focused, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
    
    @objc func onAppDidEnterBackground() {
        print("<AgoraKit> onAppDidEnterBackground")
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_lost_focused, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
}

// MARK: - Timer Call
extension AgoraVideoCallViewController: AlertConfirmEndCallViewDelegate {
    func onAlertAgreeEndCallAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.onSetFinishAppointment()
        }
    }
}

// MARK: Socket Handle
extension AgoraVideoCallViewController {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentStatusChangedEvent VideoCallViewController ")
        
        if self.isFinishVideoCall == true {
            print("onFinishVideoCall true setInitHandleSocketEvent")
            return
        }
        if self.isFinishAppointment == true {
            print("onFinishVideoCall isFinishAppointment true setInitHandleSocketEvent")
            return
        }
        
        guard let appointment = notification.object as? AppointmentModel else {
            return
        }
        guard appointment.id == self.videoCall?.appointment?.id else {
            return
        }
        guard let status = appointment.status else {
            return
        }
        guard let statusId = status.id else {
            return
        }
        print("socket.on care: AppointmentStatusChangedEvent VideoCallViewController statusId: ", statusId)
        
        switch statusId {
        case AppointmentStatus.finish.id:
            self.setRemoteMask(isHidden: false, status: "Bác sĩ đã kết thúc cuộc tư vấn")
            
            self.view.makeToast(" \nBác sĩ đã kết thúc cuộc tư vấn\n ", duration: 3.0, position: .center)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.onFinishVideoCall()
            }
            
        default:
            break
        }
    }
}

// MARK: API
extension AgoraVideoCallViewController {
    func onSetFinishAppointment() {
        if self.isFinishVideoCall == true {
            print("onFinishVideoCall true onSetFinishAppointment")
            return
        }
        if let appointment = self.videoCall?.appointment {
            self.isFinishAppointment = true
            self.careService.setFinishAppointment(appointment: appointment, completion: { [weak self] result in
                switch result.unwrapSuccessModel() {
                case .success:
                    break
                    
                case .failure(let error):
                    debugPrint(error)
                    
                    SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_error, appointmentId: self?.videoCall?.appointment?.id, error: error)
                }
                
                self?.onFinishVideoCall()
            })
        }
    }
    
    func onFinishVideoCall() {
        if self.isFinishVideoCall == true {
            print("onFinishVideoCall true")
            return
        }
        print("onFinishVideoCall false")
        
        self.isFinishVideoCall = true
        self.delegate?.onEndCallAction(videoCall: self.videoCall)
        self.dismiss(animated: true)
    }
}
