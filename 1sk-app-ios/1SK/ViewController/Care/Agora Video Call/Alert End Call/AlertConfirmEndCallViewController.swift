//
//  AlertConfirmEndCallViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/4/21.
//

import UIKit

protocol AlertConfirmEndCallViewDelegate: AnyObject {
    func onAlertAgreeEndCallAction()
}

class AlertConfirmEndCallViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    weak var delegate: AlertConfirmEndCallViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitLayoutView()
    }

    func setInitLayoutView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBg)))
    }
    
    @objc func onTouchBg() {
        self.removeAnimate()
    }

    @IBAction func onDismissAction(_ sender: Any) {
        self.removeAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertAgreeEndCallAction()
        self.removeAnimate()
    }

}

// MARK: - Animation
extension AlertConfirmEndCallViewController {
    func showInView(_ aView: UIViewController, animated: Bool) {
        aView.view.addSubview(self.view)
        aView.addChild(self)
        
        //
        
        if animated == true {
            self.showAnimate()
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                self.view.removeFromSuperview()
                //self.navigationController?.popViewController(animated: false)
            }
        })
    }
}
