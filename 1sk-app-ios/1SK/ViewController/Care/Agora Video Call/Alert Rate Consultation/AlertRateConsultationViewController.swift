//
//  AlertRateConsultationViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/4/21.
//

import UIKit
import Cosmos

class AlertRateConsultationViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var cosmos_Consultation: CosmosView!
    @IBOutlet weak var cosmos_VideoCall: CosmosView!
    
    var careService = CareService()
    var appointmentId: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitLayoutView()
    }

    func setInitLayoutView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBg)))
    }
    
    @objc func onTouchBg() {
        self.removeAnimate()
    }

    @IBAction func onDismissAction(_ sender: Any) {
        self.removeAnimate()
    }

    @IBAction func onAgreeAction(_ sender: Any) {
        self.setRateConsultation()
    }
}

// MARK: - API
extension AlertRateConsultationViewController {
    func setRateConsultation() {
        let rateDoctor = self.cosmos_Consultation.rating
        let rateAppointment = self.cosmos_VideoCall.rating
        
        self.careService.setRateVideoCall(appointmentId: self.appointmentId!, rateDoctor: Int(rateDoctor), rateAppointment: Int(rateAppointment)) { [weak self] result in
            self?.removeAnimate()
        }
    }
}

// MARK: - Animation
extension AlertRateConsultationViewController {
    func showInView(_ aView: UIViewController, animated: Bool) {
        aView.view.addSubview(self.view)
        aView.addChild(self)
        
        //
        
        if animated == true {
            self.showAnimate()
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                self.view.removeFromSuperview()
            }
        })
    }
}
