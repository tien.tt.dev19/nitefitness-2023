//
//  AlertRenewTokenCallViewController.swift
//  1SK
//
//  Created by Thaad on 31/12/2021.
//

import UIKit

protocol AlertRenewTokenCallViewDelegate: AnyObject {
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?)
}

class AlertRenewTokenCallViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    weak var delegate: AlertRenewTokenCallViewDelegate?
    
    var alertTitle: String?
    var alertContent: String?
    var appointment: AppointmentModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onDismissAction(_ sender: Any) {
        self.removeAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAgreeAlertRenewTokenCallAction(appointment: self.appointment)
        self.removeAnimate()
    }
}

// MARK: - Animation
extension AlertRenewTokenCallViewController {
    func showInView(_ aView: UIViewController, animated: Bool) {
        aView.view.addSubview(self.view)
        aView.addChild(self)
        
        if let title = self.alertTitle {
            self.lbl_Title.text = title
        }
        
        if let content = self.alertContent {
            self.lbl_Content.text = content
        }
        
        if animated == true {
            self.showAnimate()
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                self.view.removeFromSuperview()
            }
        })
    }
}
