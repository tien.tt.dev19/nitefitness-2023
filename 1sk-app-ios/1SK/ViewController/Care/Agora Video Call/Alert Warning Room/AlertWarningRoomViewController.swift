//
//  AlertWarningRoomViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//

import UIKit

class AlertWarningRoomViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    
    var alertTitle = ""
    var alertContent = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitLayoutView()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }

    func setInitLayoutView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBg)))
    }
    
    @objc func onTouchBg() {
        self.removeAnimate()
    }

    @IBAction func onDismissAction(_ sender: Any) {
        self.removeAnimate()
    }
    
}

// MARK: - Animation
extension AlertWarningRoomViewController {
    func showInView(_ aView: UIViewController, animated: Bool) {
        aView.view.addSubview(self.view)
        aView.addChild(self)
        
        self.lbl_Title.text = self.alertTitle
        self.lbl_Content.text = self.alertContent
        self.lbl_Phone.text = "Liên hệ hỗ trợ \(gPhoneHotline ?? "")"
        
        if animated == true {
            self.showAnimate()
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                self.view.removeFromSuperview()
                //self.navigationController?.popViewController(animated: false)
            }
        })
    }
}
