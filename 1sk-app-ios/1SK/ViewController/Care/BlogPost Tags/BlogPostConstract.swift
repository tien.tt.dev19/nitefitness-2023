//
//  
//  BlogPostConstract.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

// MARK: - View
protocol BlogPostViewProtocol: AnyObject {
    func onViewDidLoad()
    
    // UICollectionView
    func onReloadDataColl()
    
    // UITableView
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func onSetNotFoundView(isHidden: Bool)
    
    // Layout View
    func setTitleNav(title: String?)
    
}

// MARK: - Presenter
protocol BlogPostPresenterProtocol {
    func onViewDidLoad()
    
    // UICollectionView
    func numberOfRowsColl() -> Int
    func cellForRowColl(at indexPath: IndexPath) -> CategoryChildrenModel
    func onDidSelectRowColl(at indexPath: IndexPath, indexPathBefore: IndexPath)
    
    // UITableView
    func numberOfRow(in section: Int) -> Int
    func itemForRow(at indexPath: IndexPath) -> BlogModel?
    func onLoadMoreAction()
    func onDidSelectRowAt(indexPath: IndexPath)
}

// MARK: - Interactor Input
protocol BlogPostInteractorInputProtocol {
    func getListCategoryBlog(categoryId: Int)
    func getListBlog(categoryId: Int, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol BlogPostInteractorOutputProtocol: AnyObject {
    func onGetListCategoryBlogFinish(with result: Result<[CategoryParentModel], APIError>)
    func onGetListBlogFinished(with result: Result<[BlogModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol BlogPostRouterProtocol: BaseRouterProtocol {
    func gotoBlogDetailsViewController(blogModel: BlogModel)
}
