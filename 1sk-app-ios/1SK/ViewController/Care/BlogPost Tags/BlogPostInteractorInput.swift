//
//  
//  BlogPostInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class BlogPostInteractorInput {
    weak var output: BlogPostInteractorOutputProtocol?
    var blogService: BlogServiceProtocol?
    var configService: ConfigServiceProtocol?
}

// MARK: - BlogPostInteractorInputProtocol
extension BlogPostInteractorInput: BlogPostInteractorInputProtocol {
    func getListCategoryBlog(categoryId: Int) {
        self.configService?.getListCategoriesChildren(categoryId: categoryId, completion: { [weak self] result in
            self?.output?.onGetListCategoryBlogFinish(with: result.unwrapSuccessModel())
        })
    }
    
    func getListBlog(categoryId: Int, page: Int, perPage: Int) {
        self.blogService?.getListBlog(categoryId: categoryId, tagsId: nil, keywords: nil, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListBlogFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
}
