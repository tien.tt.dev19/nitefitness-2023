//
//  
//  BlogPostPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class BlogPostPresenter {

    weak var view: BlogPostViewProtocol?
    private var interactor: BlogPostInteractorInputProtocol
    private var router: BlogPostRouterProtocol

    init(interactor: BlogPostInteractorInputProtocol,
         router: BlogPostRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var categoryType: CategoryType = .none
    
    var listCategory: [CategoryChildrenModel] = []
    var listBlogs: [BlogModel] = []
    
    private var cateSelected: CategoryChildrenModel?
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 10
    private var isOutOfData = false
    
    private func getDataCategory() {
        self.interactor.getListCategoryBlog(categoryId: self.categoryType.rawValue)
    }
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        
        self.router.showHud()
        let categoryId = self.cateSelected?.id ?? self.categoryType.rawValue
        self.interactor.getListBlog(categoryId: categoryId, page: page, perPage: self.perPage)
    }
    
    func checkDataNotFound() {
        // Set Status View Data Not Found
        if self.listBlogs.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - BlogPostPresenterProtocol
extension BlogPostPresenter: BlogPostPresenterProtocol {
    func onViewDidLoad() {
        self.getDataCategory()
        self.getData(in: 1)
    }
    
    // UICollectionView
    func numberOfRowsColl() -> Int {
        return self.listCategory.count
    }
    
    func cellForRowColl(at indexPath: IndexPath) -> CategoryChildrenModel {
        return self.listCategory[indexPath.row]
    }
    
    func onDidSelectRowColl(at indexPath: IndexPath, indexPathBefore: IndexPath) {
        for (index, item) in self.listCategory.enumerated() {
            if index == indexPath.row {
                item.isSelected = true
                self.cateSelected = item
                
            } else {
                item.isSelected = false
            }
        }
        self.getData(in: 1)
    }
    
    // UITableView
    func numberOfRow(in section: Int) -> Int {
        return self.listBlogs.count
    }
    
    func itemForRow(at indexPath: IndexPath) -> BlogModel? {
        return self.listBlogs[indexPath.row]
    }
    
    func onLoadMoreAction() {
        if self.listBlogs.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            if self.listBlogs.count >= self.perPage {
                SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            }
            self.view?.onLoadingSuccess()
        }
    }
    
    func onDidSelectRowAt(indexPath: IndexPath) {
        let blog = self.listBlogs[indexPath.row]
        self.router.gotoBlogDetailsViewController(blogModel: blog)
    }
}

// MARK: - BlogPostInteractorOutput 
extension BlogPostPresenter: BlogPostInteractorOutputProtocol {
    func onGetListCategoryBlogFinish(with result: Result<[CategoryParentModel], APIError>) {
        switch result {
        case .success(let listModel):
            if let model = listModel.first, let listCate =  model.children {
                self.listCategory = listCate
                self.view?.setTitleNav(title: model.name)

                let all = CategoryChildrenModel.init(id: model.id ?? 0, name: "Tất cả")
                all.isSelected = true
                self.cateSelected = all
                self.listCategory.insert(all, at: 0)
                
                self.view?.onReloadDataColl()
                
            } else {
                break
            }

        case .failure:
            break
        }

        self.router.hideHud()
    }
    
    func onGetListBlogFinished(with result: Result<[BlogModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listBlogs = listModel
                self.view?.onReloadData()

            } else {
                for object in listModel {
                    self.listBlogs.append(object)
                    self.view?.onInsertRow(at: self.listBlogs.count - 1)
                }
            }
            self.total = total

            // Set Out Of Data
            if self.listBlogs.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
    
}
