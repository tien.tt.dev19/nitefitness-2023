//
//  
//  BlogPostRouter.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

enum ParentBlogPost {
    case HomeApp
    case HomeFitness
    case HomeCare
    case None
}

class BlogPostRouter: BaseRouter {
    static func setupModule(with categoryType: CategoryType) -> BlogPostViewController {
        let viewController = BlogPostViewController()
        let router = BlogPostRouter()
        let interactorInput = BlogPostInteractorInput()
        let presenter = BlogPostPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.categoryType = categoryType
        interactorInput.output = presenter
        interactorInput.blogService = BlogService()
        interactorInput.configService = ConfigService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BlogPostRouterProtocol
extension BlogPostRouter: BlogPostRouterProtocol {
    func gotoBlogDetailsViewController(blogModel: BlogModel) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: .blogFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
