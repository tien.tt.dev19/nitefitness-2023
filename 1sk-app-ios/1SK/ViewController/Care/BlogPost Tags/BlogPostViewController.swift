//
//  
//  BlogPostViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class BlogPostViewController: BaseViewController {

    var presenter: BlogPostPresenterProtocol!
    
    @IBOutlet weak var view_Tag: UIView!
    @IBOutlet weak var constraint_height_ViewTag: NSLayoutConstraint!
    
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    
    private var isLoading = false
    private var tagsIndexPath: IndexPath?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitUICollectionView()
        self.setInitUITableView()
    }
}

// MARK: - BlogPostViewProtocol
extension BlogPostViewController: BlogPostViewProtocol {
    func onViewDidLoad() {
        
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
    
    func onReloadDataColl() {
        self.coll_CollectionView.reloadData()
    }
    
    func setTitleNav(title: String?) {
        self.title = title ?? "Bài viết"
    }
}

// MARK: - UICollectionViewDataSource
extension BlogPostViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewTags.self)
        self.coll_CollectionView.dataSource = self
        
        self.tagsIndexPath = IndexPath(row: 0, section: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.numberOfRowsColl()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewTags.self, for: indexPath)
        cell.delegate = self
        cell.indexPath = indexPath
        let item = self.presenter.cellForRowColl(at: indexPath)
        cell.config(category: item)
        return cell
    }
}

// MARK: - CellCollectionViewTagsDelegate
extension BlogPostViewController: CellCollectionViewTagsDelegate {
    func onDidSelectCellTags(indexPath: IndexPath) {
        if let cell = self.coll_CollectionView.cellForItem(at: self.tagsIndexPath!) as? CellCollectionViewTags {
            cell.view_Bg.backgroundColor = R.color.bgCell()
            cell.lbl_Title.textColor = R.color.text_2()
        }
        self.tagsIndexPath = indexPath
        self.presenter.onDidSelectRowColl(at: indexPath, indexPathBefore: self.tagsIndexPath!)
    }
}

// MARK: - UITableViewDataSource
extension BlogPostViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewBlog.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRow(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewBlog.self, for: indexPath)
        cell.config(with: self.presenter.itemForRow(at: indexPath))
        return cell
    }
}

// MARK: - UITableViewDelegate
extension BlogPostViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRowAt(indexPath: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}
