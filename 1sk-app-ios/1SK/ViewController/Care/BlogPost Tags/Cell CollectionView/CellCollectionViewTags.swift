//
//  CellCollectionViewTags.swift
//  1SK
//
//  Created by vuongbachthu on 10/15/21.
//

import UIKit

protocol CellCollectionViewTagsDelegate: AnyObject {
    func onDidSelectCellTags(indexPath: IndexPath)
}

class CellCollectionViewTags: UICollectionViewCell {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    weak var delegate: CellCollectionViewTagsDelegate?
    var category: CategoryChildrenModel?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayoutView()
    }

    func setInitLayoutView() {
        self.view_Bg.cornerRadius = self.view_Bg.frame.height/2
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchSelectCell)))
    }
    
    @objc func onTouchSelectCell() {
        if self.category?.isSelected != true {
            self.category?.isSelected = true
            self.view_Bg.backgroundColor = R.color.mainColor()
            self.lbl_Title.textColor = .white
            
            self.delegate?.onDidSelectCellTags(indexPath: self.indexPath!)
        }
    }
    
    func config(category: CategoryChildrenModel) {
        self.category = category
        
        self.lbl_Title.text = category.name
        if category.isSelected == true {
            self.view_Bg.backgroundColor = R.color.mainColor()
            self.lbl_Title.textColor = .white
            
        } else {
            self.view_Bg.backgroundColor = R.color.bgCell()
            self.lbl_Title.textColor = R.color.text_2()
        }
    }
}
