//
//  
//  BlogVideoConstract.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

// MARK: - View
protocol BlogVideoViewProtocol: AnyObject {
    func onScrollToTopTableView()
    func onReloadDataTableView()
    func onReloadDataCollView(scrollToItem: Int)
    func onSetNotFoundView(isHidden: Bool)
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
    
    func onReloadDataColl()
    
    // Layout View
    func setTitleNav(title: String?)
}

// MARK: - Presenter
protocol BlogVideoPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    // UICollectionView
    func numberOfRowsColl() -> Int
    func cellForRowColl(at indexPath: IndexPath) -> CategoryChildrenModel
    func onDidSelectRowColl(at indexPath: IndexPath, indexPathBefore: IndexPath)
    
    // UITableView
    func numberOfRowsTableView() -> Int
    func cellForRowTableView(at indexPath: IndexPath) -> VideoModel
    func didSelectRowTableView(at indexPath: IndexPath)
    func onLoadMoreAction()
}

// MARK: - Interactor Input
protocol BlogVideoInteractorInputProtocol {
    func getListCategoryVideo(categoryId: Int)
    func getListVideo(categoryId: Int, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol BlogVideoInteractorOutputProtocol: AnyObject {
    func onGetListCategoryVideoFinish(with result: Result<[CategoryParentModel], APIError>)
    func onGetListVideoFinish(with result: Result<[VideoModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol BlogVideoRouterProtocol: BaseRouterProtocol {
    func showVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType?)
}
