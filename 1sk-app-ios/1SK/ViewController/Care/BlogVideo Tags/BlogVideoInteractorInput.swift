//
//  
//  BlogVideoInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class BlogVideoInteractorInput {
    weak var output: BlogVideoInteractorOutputProtocol?
    var fitnessService: FitnessServiceProtocol?
    var blogService: BlogServiceProtocol?
    var configService: ConfigServiceProtocol?
}

// MARK: - BlogVideoInteractorInputProtocol
extension BlogVideoInteractorInput: BlogVideoInteractorInputProtocol {
    func getListCategoryVideo(categoryId: Int) {
        self.configService?.getListCategoriesChildren(categoryId: categoryId, completion: { [weak self] result in
            self?.output?.onGetListCategoryVideoFinish(with: result.unwrapSuccessModel())
        })
    }
    
    func getListVideo(categoryId: Int, page: Int, perPage: Int) {
        self.fitnessService?.getListVideo(categoryId: categoryId, tagsId: nil, keywords: nil, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListVideoFinish(with: result.unwrapSuccessModel(), page: page, total: total)
        })
        
//        self.blogService?.getListVideos(positions: positions, portal: portal, tagId: tagId, categoryId: 0, page: page, limit: limit, completion: { [weak self] result in
//            let total = result.getTotal() ?? 0
//            self?.output?.onGetListVideoFinish(with: result.unwrapSuccessModel(), page: page, total: total)
//        })
    }
    
}
