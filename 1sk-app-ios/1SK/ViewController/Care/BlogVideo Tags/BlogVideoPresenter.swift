//
//  
//  BlogVideoPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class BlogVideoPresenter {

    weak var view: BlogVideoViewProtocol?
    private var interactor: BlogVideoInteractorInputProtocol
    private var router: BlogVideoRouterProtocol

    init(interactor: BlogVideoInteractorInputProtocol,
         router: BlogVideoRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var categoryType: CategoryType = .none
    
    private var listCategory: [CategoryChildrenModel] = []
    private var listVideos: [VideoModel] = []
    
    private var cateSelected: CategoryChildrenModel?

    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getDataCategory() {
        self.interactor.getListCategoryVideo(categoryId: self.categoryType.rawValue)
    }
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        
        self.router.showHud()
        let categoryId = self.cateSelected?.id ?? self.categoryType.rawValue
        self.interactor.getListVideo(categoryId: categoryId, page: page, perPage: self.limit)
    }
    
    func checkDataNotFound() {
        if self.listVideos.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - BlogVideoPresenterProtocol
extension BlogVideoPresenter: BlogVideoPresenterProtocol {
    func onViewDidLoad() {
        self.getDataCategory()
        self.getData(in: 1)
    }

    func onViewDidAppear() {
        //
    }
    
    // UICollectionView
    func numberOfRowsColl() -> Int {
        return self.listCategory.count
    }

    func cellForRowColl(at indexPath: IndexPath) -> CategoryChildrenModel {
        return self.listCategory[indexPath.row]
    }

    func onDidSelectRowColl(at indexPath: IndexPath, indexPathBefore: IndexPath) {
        for (index, item) in self.listCategory.enumerated() {
            if index == indexPath.row {
                item.isSelected = true
                self.cateSelected = item

            } else {
                item.isSelected = false
            }
        }
        self.getData(in: 1)
    }

    // UITableView
    func numberOfRowsTableView() -> Int {
        return self.listVideos.count
    }

    func cellForRowTableView(at indexPath: IndexPath) -> VideoModel {
        return self.listVideos[indexPath.row]
    }

    func didSelectRowTableView(at indexPath: IndexPath) {
        let video = self.listVideos[indexPath.row]
        self.router.showVideoDetailsViewController(with: video, categoryType: self.categoryType)
        
    }

    func onLoadMoreAction() {
        if self.listVideos.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            if self.listVideos.count >= self.limit {
                SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            }
            self.view?.onLoadingSuccess()
        }
    }
}

// MARK: - BlogVideoInteractorOutput 
extension BlogVideoPresenter: BlogVideoInteractorOutputProtocol {
    func onGetListCategoryVideoFinish(with result: Result<[CategoryParentModel], APIError>) {
        switch result {
        case .success(let listModel):
            if let model = listModel.first, let listCate =  model.children {
                self.listCategory = listCate
                self.view?.setTitleNav(title: model.name)

                let all = CategoryChildrenModel.init(id: model.id ?? 0, name: "Tất cả")
                all.isSelected = true
                self.cateSelected = all
                self.listCategory.insert(all, at: 0)
                
                self.view?.onReloadDataColl()
                
            } else {
                break
            }

        case .failure:
            break
        }

        self.router.hideHud()
    }
    
    func onGetListVideoFinish(with result: Result<[VideoModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            for item in listModel {
                item.categoryType = self.categoryType
            }
            
            if page <= 1 {
                self.listVideos = listModel
                self.view?.onReloadDataTableView()
                
            } else {
                for object in listModel {
                    self.listVideos.append(object)
                    self.view?.onInsertRow(at: self.listVideos.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listVideos.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            break
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
    
}
