//
//  
//  BlogVideoRouter.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

enum ParentBlogVideo {
    case HomeApp
    case HomeFitness
    case HomeCare
    case None
}

class BlogVideoRouter: BaseRouter {
    static func setupModule(with categoryType: CategoryType) -> BlogVideoViewController {
        let viewController = BlogVideoViewController()
        let router = BlogVideoRouter()
        let interactorInput = BlogVideoInteractorInput()
        let presenter = BlogVideoPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.categoryType = categoryType
        interactorInput.output = presenter
        interactorInput.fitnessService = FitnessService()
        interactorInput.blogService = BlogService()
        interactorInput.configService = ConfigService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BlogVideoCareRouterProtocol
extension BlogVideoRouter: BlogVideoRouterProtocol {
    func showVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType?) {
        let controller = PlayVideoRouter.setupModule(with: video, categoryType: categoryType)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }

    func shopHudUnderTop() {
        guard let `viewController` = viewController as? BaseViewController else {
            return
        }
        viewController.showProgressHud(offsetTop: 64, offsetBottom: 0)
    }
    
    func gotoFilterAttributeViewController(with listFilterAttribute: [FilterAttributeModel], delegate: FilterExercisePresenterDelegate?) {
        let controller = FilterExerciseRouter.setupModule(with: listFilterAttribute, delegate: delegate)
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
