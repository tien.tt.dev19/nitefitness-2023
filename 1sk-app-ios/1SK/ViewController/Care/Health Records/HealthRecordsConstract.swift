//
//  
//  HealthRecordsConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

// MARK: - View
protocol HealthRecordsViewProtocol: AnyObject {
    func onViewDidLoad()
    func setInitData(user: UserModel?)
}

// MARK: - Presenter
protocol HealthRecordsPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onEditUserInfoAction()
}

// MARK: - Interactor Input
protocol HealthRecordsInteractorInputProtocol {
    
}

// MARK: - Interactor Output
protocol HealthRecordsInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol HealthRecordsRouterProtocol: BaseRouterProtocol {
    func gotoAccountViewController(delegate: UpdateProfileViewDelegate?)
}
