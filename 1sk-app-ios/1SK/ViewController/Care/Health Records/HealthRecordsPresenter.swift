//
//  
//  HealthRecordsPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthRecordsPresenter {

    weak var view: HealthRecordsViewProtocol?
    private var interactor: HealthRecordsInteractorInputProtocol
    private var router: HealthRecordsRouterProtocol

    init(interactor: HealthRecordsInteractorInputProtocol,
         router: HealthRecordsRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    
}

// MARK: - HealthRecordsPresenterProtocol
extension HealthRecordsPresenter: HealthRecordsPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onViewDidLoad()
        self.view?.setInitData(user: gUser)
    }
    
    func onViewDidAppear() {
        self.view?.setInitData(user: gUser)
    }
    
    func onEditUserInfoAction() {
        self.router.gotoAccountViewController(delegate: self)
    }
}

// MARK: - HealthRecordsInteractorOutput 
extension HealthRecordsPresenter: HealthRecordsInteractorOutputProtocol {
    
}

// MARK: UpdateProfilePresenterDelegate
extension HealthRecordsPresenter: UpdateProfileViewDelegate {
    func onSaveUserInfoChangeSuccess() {
        self.view?.setInitData(user: gUser)
    }
}
