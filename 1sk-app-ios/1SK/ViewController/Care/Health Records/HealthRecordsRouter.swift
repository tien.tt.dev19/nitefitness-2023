//
//  
//  HealthRecordsRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthRecordsRouter: BaseRouter {
//    weak var viewController: HealthRecordsViewController?
    static func setupModule() -> HealthRecordsViewController {
        let viewController = HealthRecordsViewController()
        let router = HealthRecordsRouter()
        let interactorInput = HealthRecordsInteractorInput()
        let presenter = HealthRecordsPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - HealthRecordsRouterProtocol
extension HealthRecordsRouter: HealthRecordsRouterProtocol {
    
    func gotoAccountViewController(delegate: UpdateProfileViewDelegate?) {
        let controller = UpdateProfileRouter.setupModule(with: delegate)
        self.viewController?.present(controller, animated: true)
    }
}
