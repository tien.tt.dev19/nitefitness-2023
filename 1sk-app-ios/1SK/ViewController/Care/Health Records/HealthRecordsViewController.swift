//
//  
//  HealthRecordsViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthRecordsViewController: BaseViewController {

    var presenter: HealthRecordsPresenterProtocol!
    
    @IBOutlet weak var view_Container: UIView!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Gender: UILabel!
    @IBOutlet weak var lbl_Age: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    
    @IBOutlet weak var btn_PHR: UIButton!
    @IBOutlet weak var btn_History: UIButton!
    
    @IBOutlet weak var view_IndicatorPHR: UIView!
    @IBOutlet weak var view_IndicatorHistory: UIView!
    
    private lazy var measurementVC = MeasurementRouter.setupModule()
    private lazy var historyConsultingVC = HistoryConsultingRouter.setupModule()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitStateButtonView()
    }
    
    private func setInitStateButtonView() {
        self.btn_PHR.setTitleColor(R.color.color_Gray_Text(), for: .normal)
        self.btn_PHR.setTitleColor(R.color.color_Dark_Text(), for: .selected)
        self.btn_PHR.tintColor = .clear
        
        self.btn_History.setTitleColor(R.color.color_Gray_Text(), for: .normal)
        self.btn_History.setTitleColor(R.color.color_Dark_Text(), for: .selected)
        self.btn_History.tintColor = .clear
        
        self.view_IndicatorPHR.alpha = 1.0
        self.view_IndicatorHistory.alpha = 0.0
        
        self.btn_PHR.isSelected = true
        self.btn_History.isSelected = false
        self.btn_PHR.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        self.btn_History.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
    }
    
    func setStateSelectViewPHR() {
        self.btn_PHR.isSelected = true
        self.btn_History.isSelected = false
        self.btn_PHR.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        self.btn_History.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        
        UIView.animate(withDuration: 0.3) {
            self.view_IndicatorPHR.alpha = 1.0
            self.view_IndicatorHistory.alpha = 0.0
            self.view.layoutIfNeeded()
        }
    }
    
    func setStateSelectViewHistory() {
        self.btn_PHR.isSelected = false
        self.btn_History.isSelected = true
        self.btn_PHR.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        self.btn_History.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        
        UIView.animate(withDuration: 0.3) {
            self.view_IndicatorPHR.alpha = 0.0
            self.view_IndicatorHistory.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
    
    // MARK: - Action
    
    @IBAction func onBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onMeasurementAction(_ sender: Any) {
        self.setStateSelectViewPHR()
        self.addChildView(childViewController: self.measurementVC)
    }
    
    @IBAction func onHistoryConsultingAction(_ sender: Any) {
        self.setStateSelectViewHistory()
        self.addChildView(childViewController: self.historyConsultingVC)
    }
    
    @IBAction func onEditUserInfoAction(_ sender: Any) {
        self.presenter.onEditUserInfoAction()
    }
}

// MARK: - HealthRecordsViewProtocol
extension HealthRecordsViewController: HealthRecordsViewProtocol {
    func onViewDidLoad() {
        self.addChildView(childViewController: self.measurementVC)
    }
    
    func setInitData(user: UserModel?) {
        self.lbl_Name.text = user?.fullName
        self.lbl_Gender.text = user?.gender?.valueVi ?? "..."
        
        if let birthday = user?.birthday?.split(separator: "/") {
            let yearSub = "\(birthday.last ?? "")"
            if let year = Int(yearSub) {
                let currentYear = Date().year
                self.lbl_Age.text = "\(currentYear - year) tuổi"
            }
        }
        
        self.lbl_Phone.text = "Số điện thoại: \(user?.phoneNumber ?? "...")"
        
        self.img_Avatar.setImageWith(imageUrl: user?.avatar ?? "", placeHolder: R.image.default_avatar_2())
    }
}
