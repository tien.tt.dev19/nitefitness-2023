//
//  CellTableViewHistoryConsulting.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//

import UIKit

class CellTableViewHistoryConsulting: UITableViewCell {

    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_Service: UILabel!
    @IBOutlet weak var lbl_Doctor: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Diagnosis: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(appointment: AppointmentModel?) {
        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(appointment?.doctorAvailableTime?.startAt ?? 0))
        if let specialist = appointment?.doctor?.specialist?.first {
            self.lbl_Service.text = "Dịch vụ tư vấn \(specialist.name ?? "") qua video"
        }
        
        self.lbl_Doctor.text = "\(appointment?.doctor?.academicTitle?.shortName ?? ""). \(appointment?.doctor?.basicInformation?.fullName ?? "")"
        self.lbl_Time.text = "\(date.toString(Date.Format.dmySlash)) lúc \(date.toString(Date.Format.hm))"
        
        self.img_Avatar.setImageWith(imageUrl: appointment?.doctor?.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
        
        if let diagnosis = appointment?.diagnosis, diagnosis.count > 0 {
            self.lbl_Diagnosis.text = "Chẩn đoán: \(diagnosis)"
            
        } else {
            self.lbl_Diagnosis.text = "Chẩn đoán: Chưa có"
        }
    }
}
