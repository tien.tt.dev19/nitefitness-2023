//
//  
//  DetailConsultingConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

// MARK: - View
protocol DetailConsultingViewProtocol: AnyObject {
    func onSetDataInit(appointment: AppointmentModel?)
}

// MARK: - Presenter
protocol DetailConsultingPresenterProtocol {
    func onViewDidLoad()
}

// MARK: - Interactor Input
protocol DetailConsultingInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol DetailConsultingInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol DetailConsultingRouterProtocol {

}
