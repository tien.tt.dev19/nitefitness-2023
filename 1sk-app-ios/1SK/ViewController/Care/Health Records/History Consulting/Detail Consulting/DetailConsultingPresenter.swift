//
//  
//  DetailConsultingPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

class DetailConsultingPresenter {

    weak var view: DetailConsultingViewProtocol?
    private var interactor: DetailConsultingInteractorInputProtocol
    private var router: DetailConsultingRouterProtocol

    init(interactor: DetailConsultingInteractorInputProtocol,
         router: DetailConsultingRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var appointment: AppointmentModel?
}

// MARK: - DetailConsultingPresenterProtocol
extension DetailConsultingPresenter: DetailConsultingPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onSetDataInit(appointment: self.appointment)
    }
}

// MARK: - DetailConsultingInteractorOutput 
extension DetailConsultingPresenter: DetailConsultingInteractorOutputProtocol {

}
