//
//  
//  DetailConsultingRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

class DetailConsultingRouter {
    weak var viewController: DetailConsultingViewController?
    static func setupModule(with appointmentModel: AppointmentModel?) -> DetailConsultingViewController {
        let viewController = DetailConsultingViewController()
        let router = DetailConsultingRouter()
        let interactorInput = DetailConsultingInteractorInput()
        let presenter = DetailConsultingPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.appointment = appointmentModel
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - DetailConsultingRouterProtocol
extension DetailConsultingRouter: DetailConsultingRouterProtocol {
    
}
