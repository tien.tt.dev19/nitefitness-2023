//
//  
//  DetailConsultingViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

class DetailConsultingViewController: BaseViewController {

    var presenter: DetailConsultingPresenterProtocol!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_Service: UILabel!
    @IBOutlet weak var lbl_Doctor: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_Birthday: UILabel!
    @IBOutlet weak var lbl_Gender: UILabel!
    @IBOutlet weak var lbl_Reason: UILabel!
    @IBOutlet weak var lbl_Diagnosis: UILabel!
    @IBOutlet weak var lbl_Prescription: UILabel!
    
    @IBOutlet weak var stack_NextAppointment: UIStackView!
    @IBOutlet weak var lbl_NextAppointment: UILabel!
    
    @IBOutlet weak var lbl_Note: UILabel!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.title = "Chi tiết tư vấn"
    }
    
    // MARK: - Action
    
}

// MARK: - DetailConsultingViewProtocol
extension DetailConsultingViewController: DetailConsultingViewProtocol {
    func onSetDataInit(appointment: AppointmentModel?) {
        self.img_Avatar.setImageWith(imageUrl: appointment?.doctor?.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
        
        if let specialist = appointment?.doctor?.specialist?.first {
            self.lbl_Service.text = "Dịch vụ: Tư vấn \(specialist.name ?? "chuyên khóa") qua video"
        }
        
        self.lbl_Doctor.text = "\(appointment?.doctor?.academicTitle?.shortName ?? ""). \(appointment?.doctor?.basicInformation?.fullName ?? "")"
        
        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(appointment?.doctorAvailableTime?.startAt ?? 0))
        self.lbl_Time.text = "\(date.toString(Date.Format.hm)) ngày \(date.toString(Date.Format.dmySlash))"
        
        self.lbl_UserName.text = appointment?.patientName
        self.lbl_Birthday.text = appointment?.patientBirthYear
        self.lbl_Gender.text = appointment?.patientGender
        self.lbl_Reason.text = appointment?.patientSymptom
        
        if let diagnosis = appointment?.diagnosis, diagnosis.count > 0 {
            self.lbl_Diagnosis.text = diagnosis
            self.lbl_Diagnosis.textColor = R.color.color_Dark_Text()
        }
        if let prescription = appointment?.prescription, prescription.count > 0 {
            self.lbl_Prescription.text = appointment?.prescription
            self.lbl_Prescription.textColor = R.color.color_Dark_Text()
        }
        if let nextAppointment = appointment?.nextAppointment {
            let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(nextAppointment))
            let day = date.toString(Date.Format.dmySlash)
            
            self.lbl_NextAppointment.text = day
            self.lbl_NextAppointment.textColor = R.color.color_Dark_Text()
        }
        
        self.lbl_Note.text = "Lưu ý: Sau 30 phút kể từ khi cuộc tư vấn kết thúc, nếu bạn chưa nhận được Chẩn đoán của bác sĩ, hãy liên hệ Hotline của 1SK để khiếu nại: \(gPhoneHotline ?? ""). Xin cảm ơn!"
        switch appointment?.status?.id {
        case AppointmentStatus.complete.id:
            self.lbl_Note.isHidden = true
            
        case AppointmentStatus.finish.id:
            self.lbl_Note.isHidden = false
            
        default:
            self.lbl_Note.isHidden = true
        }
        
    }
}
