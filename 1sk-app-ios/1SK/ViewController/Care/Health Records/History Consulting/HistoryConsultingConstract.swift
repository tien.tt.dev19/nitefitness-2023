//
//  
//  HistoryConsultingConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

// MARK: - View
protocol HistoryConsultingViewProtocol: AnyObject {
    func onReloadData()
    
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func onSetNotFoundView(isHidden: Bool)
}

// MARK: - Presenter
protocol HistoryConsultingPresenterProtocol {
    func onViewDidLoad()
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onRefreshAction()
    func onLoadMoreAction()
    
    /// Value
    func getTotalItem() -> Int
}

// MARK: - Interactor Input
protocol HistoryConsultingInteractorInputProtocol {
    func getListAppointments(listStatus: [AppointmentStatus], page: Int, limit: Int)
}

// MARK: - Interactor Output
protocol HistoryConsultingInteractorOutputProtocol: AnyObject {
    func onGetListAppointmentsFinished(with result: Result<[AppointmentModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol HistoryConsultingRouterProtocol: BaseRouterProtocol {
    func gotoDetailConsultingVC(appointment: AppointmentModel)
}
