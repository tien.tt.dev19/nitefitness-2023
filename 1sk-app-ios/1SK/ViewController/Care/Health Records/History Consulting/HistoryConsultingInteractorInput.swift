//
//  
//  HistoryConsultingInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HistoryConsultingInteractorInput {
    weak var output: HistoryConsultingInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - HistoryConsultingInteractorInputProtocol
extension HistoryConsultingInteractorInput: HistoryConsultingInteractorInputProtocol {
    func getListAppointments(listStatus: [AppointmentStatus], page: Int, limit: Int) {
        self.careService?.getListAppointments(isComing: false,
                                              isPats: false,
                                              page: page,
                                              pageLimit: limit,
                                              orderByTime: "DESC",
                                              listStatus: listStatus,
                                              completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListAppointmentsFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
