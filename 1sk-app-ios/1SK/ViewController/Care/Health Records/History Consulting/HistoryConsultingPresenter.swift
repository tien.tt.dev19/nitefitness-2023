//
//  
//  HistoryConsultingPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HistoryConsultingPresenter {

    weak var view: HistoryConsultingViewProtocol?
    private var interactor: HistoryConsultingInteractorInputProtocol
    private var router: HistoryConsultingRouterProtocol

    init(interactor: HistoryConsultingInteractorInputProtocol,
         router: HistoryConsultingRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var listAppointment: [AppointmentModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        self.router.showHud()
        
        let listStatus = [AppointmentStatus.complete, AppointmentStatus.finish]
        self.interactor.getListAppointments(listStatus: listStatus, page: page, limit: self.limit)
        
    }
}

// MARK: - HistoryConsultingPresenterProtocol
extension HistoryConsultingPresenter: HistoryConsultingPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.listAppointment.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel? {
        return self.listAppointment[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        guard self.listAppointment.count > 0 else {
            return
        }
        let item = self.listAppointment[indexPath.row]
        self.router.gotoDetailConsultingVC(appointment: item)
    }
    
    func onRefreshAction() {
        self.getData(in: 1)
    }
    
    func onLoadMoreAction() {
        if self.listAppointment.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            if self.listAppointment.count >= self.limit {
                SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            }
            self.view?.onLoadingSuccess()
        }
    }
    
    /// Value
    func getTotalItem() -> Int {
        return self.total
    }
}

// MARK: - HistoryConsultingInteractorOutput 
extension HistoryConsultingPresenter: HistoryConsultingInteractorOutputProtocol {
    func onGetListAppointmentsFinished(with result: Result<[AppointmentModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let blogs):
            if page <= 1 {
                self.listAppointment = blogs
                self.view?.onReloadData()

            } else {
                for object in blogs {
                    self.listAppointment.append(object)
                    self.view?.onInsertRow(at: self.listAppointment.count - 1)
                }
            }
            self.total = total

            // Set Out Of Data
            if self.listAppointment.count >= self.total {
                self.isOutOfData = true
            }

            // Set Status View Data Not Found
            if self.listAppointment.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            if self.listAppointment.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.router.hideHud()
    }
}
