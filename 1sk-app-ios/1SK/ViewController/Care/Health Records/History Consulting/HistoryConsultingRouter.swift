//
//  
//  HistoryConsultingRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HistoryConsultingRouter: BaseRouter {
//    weak var viewController: HistoryConsultingViewController?
    static func setupModule() -> HistoryConsultingViewController {
        let viewController = HistoryConsultingViewController()
        let router = HistoryConsultingRouter()
        let interactorInput = HistoryConsultingInteractorInput()
        let presenter = HistoryConsultingPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - HistoryConsultingRouterProtocol
extension HistoryConsultingRouter: HistoryConsultingRouterProtocol {
    func gotoDetailConsultingVC(appointment: AppointmentModel) {
        let controller = DetailConsultingRouter.setupModule(with: appointment)
        self.viewController?.show(controller, sender: nil)
    }
}
