//
//  SectionHeaderConsulting.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//

import UIKit

class SectionHeaderConsulting: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_TitleSection: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func config(count: Int) {
        self.lbl_TitleSection.text = "Tổng có: \(count) tư vấn"
    }
}
