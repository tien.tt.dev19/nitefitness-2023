//
//  
//  MeasurementConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

// MARK: - View
protocol MeasurementViewProtocol: AnyObject {
    func onReloadData(listPHR: [PHRModel])
}

// MARK: - Presenter
protocol MeasurementPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func onDidChangeWeight(value: Int?)
    func onDidChangeHeight(value: Int?)
    func onDidChangeBloodGroup(value: String?)
    func onDidChangeBloodPressure(value: String?)
    func onDidChangeBloodSPO2(value: Int?)
    func onDidChangeBloodSugar(value: Int?)
}

// MARK: - Interactor Input
protocol MeasurementInteractorInputProtocol {
    func getUserPhr()
    
    func setValueWeight(value: Int)
    func setValueHeight(value: Int)
    func setValueBloodGroup(value: String)
    func setValueBloodPressure(value: String)
    func setValueBloodSPO2(value: Int)
    func setValueBloodSugar(value: Int)
}

// MARK: - Interactor Output
protocol MeasurementInteractorOutputProtocol: AnyObject {
    func onGetUserPhrFinished(with result: Result<[PHRModel], APIError>)
    
    func onSetValueWeightFinished(with result: Result<PHRModel, APIError>)
    func onSetValueHeightFinished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodGroupFinished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodPressureFinished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodSPO2Finished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodSugarFinished(with result: Result<PHRModel, APIError>)
}

// MARK: - Router
protocol MeasurementRouterProtocol: BaseRouterProtocol {
    
}
