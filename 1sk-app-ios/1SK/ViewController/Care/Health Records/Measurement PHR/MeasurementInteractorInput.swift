//
//  
//  MeasurementInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MeasurementInteractorInput {
    weak var output: MeasurementInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - MeasurementInteractorInputProtocol
extension MeasurementInteractorInput: MeasurementInteractorInputProtocol {
    func getUserPhr() {
        self.authService?.getUserPHR(completion: { [weak self] result in
            self?.output?.onGetUserPhrFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueWeight(value: Int) {
        self.authService?.setValuePHR(value: value, phr: .weight, completion: { [weak self] result in
            self?.output?.onSetValueWeightFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueHeight(value: Int) {
        self.authService?.setValuePHR(value: value, phr: .height, completion: { [weak self] result in
            self?.output?.onSetValueHeightFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodGroup(value: String) {
        self.authService?.setValuePHR(value: value, phr: .blood, completion: { [weak self] result in
            self?.output?.onSetValueBloodGroupFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodPressure(value: String) {
        self.authService?.setValuePHR(value: value, phr: .pressure, completion: { [weak self] result in
            self?.output?.onSetValueBloodPressureFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodSPO2(value: Int) {
        self.authService?.setValuePHR(value: value, phr: .spo2, completion: { [weak self] result in
            self?.output?.onSetValueBloodSPO2Finished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodSugar(value: Int) {
        self.authService?.setValuePHR(value: value, phr: .sugar, completion: { [weak self] result in
            self?.output?.onSetValueBloodSugarFinished(with: result.unwrapSuccessModel())
        })
    }
    
}
