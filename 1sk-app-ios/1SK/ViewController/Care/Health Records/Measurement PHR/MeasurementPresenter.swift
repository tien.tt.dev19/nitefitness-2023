//
//  
//  MeasurementPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MeasurementPresenter {

    weak var view: MeasurementViewProtocol?
    private var interactor: MeasurementInteractorInputProtocol
    private var router: MeasurementRouterProtocol

    init(interactor: MeasurementInteractorInputProtocol,
         router: MeasurementRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var litsPHRModel: [PHRModel] = []

    func getInitData() {
        self.interactor.getUserPhr()
    }
}

// MARK: - MeasurementPresenterProtocol
extension MeasurementPresenter: MeasurementPresenterProtocol {
    func onViewDidLoad() {
        self.getInitData()
    }
    
    func onViewDidAppear() {
        self.getInitData()
    }
    
    // PHR Action
    func onDidChangeWeight(value: Int?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueWeight(value: value)
    }
    
    func onDidChangeHeight(value: Int?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueHeight(value: value)
    }
    
    func onDidChangeBloodGroup(value: String?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodGroup(value: value)
    }
    
    func onDidChangeBloodPressure(value: String?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodPressure(value: value)
    }
    
    func onDidChangeBloodSPO2(value: Int?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodSPO2(value: value)
    }
    
    func onDidChangeBloodSugar(value: Int?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodSugar(value: value)
    }
}

// MARK: - MeasurementInteractorOutput 
extension MeasurementPresenter: MeasurementInteractorOutputProtocol {
    func onGetUserPhrFinished(with result: Result<[PHRModel], APIError>) {
        switch result {
        case .success(let listModel):
            self.litsPHRModel = listModel
            self.view?.onReloadData(listPHR: litsPHRModel)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueWeightFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số cân nặng")
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueHeightFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số chiều cao")
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodGroupFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số nhóm máu")
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodPressureFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số huyết áp")
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodSPO2Finished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số SPO2")
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodSugarFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số đường huyết")
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
}
