//
//  
//  MeasurementRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MeasurementRouter: BaseRouter {
//    weak var viewController: MeasurementViewController?
    static func setupModule() -> MeasurementViewController {
        let viewController = MeasurementViewController()
        let router = MeasurementRouter()
        let interactorInput = MeasurementInteractorInput()
        let presenter = MeasurementPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.authService = AuthService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - MeasurementRouterProtocol
extension MeasurementRouter: MeasurementRouterProtocol {
    
}
