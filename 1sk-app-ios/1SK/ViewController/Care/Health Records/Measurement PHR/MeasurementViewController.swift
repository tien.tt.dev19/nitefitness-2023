//
//  
//  MeasurementViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit
import IQKeyboardManagerSwift

class MeasurementViewController: BaseViewController {

    var presenter: MeasurementPresenterProtocol!
    
    @IBOutlet weak var tf_Weight: UITextField!
    @IBOutlet weak var tf_Height: UITextField!
    @IBOutlet weak var tf_Blood: UITextField!
    @IBOutlet weak var tf_Pressure_0: UITextField!
    @IBOutlet weak var tf_Pressure_1: UITextField!
    @IBOutlet weak var tf_SPO2: UITextField!
    @IBOutlet weak var tf_Sugar: UITextField!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitUITextField()
        self.setInitKeyboardToolBar()
    }
    
    // MARK: - Action
    
}

// MARK: - MeasurementViewProtocol
extension MeasurementViewController: MeasurementViewProtocol {
    func onReloadData(listPHR: [PHRModel]) {
        for item in listPHR {
            switch item.name {
            case PHR.weight.name:
                self.tf_Weight.text = item.value
                
            case PHR.height.name:
                self.tf_Height.text = item.value
                
            case PHR.blood.name:
                self.tf_Blood.text = item.value
                
            case PHR.pressure.name:
                if let pressures = item.value?.split(separator: "/") {
                    for (index, value) in pressures.enumerated() {
                        switch index {
                        case 0:
                            if value != "" {
                                self.tf_Pressure_0.text = String(value)
                            }
                        case 1:
                            if value != "" {
                                self.tf_Pressure_1.text = String(value)
                            }
                        default:
                            break
                        }
                    }
                }
                
            case PHR.spo2.name:
                self.tf_SPO2.text = item.value
                
            case PHR.sugar.name:
                self.tf_Sugar.text = item.value
                
            default:
                break
            }
        }
    }
}

// MARK: - UITextFieldDelegate
extension MeasurementViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Weight.delegate = self
        self.tf_Height.delegate = self
        self.tf_Blood.delegate = self
        self.tf_Pressure_0.delegate = self
        self.tf_Pressure_1.delegate = self
        self.tf_SPO2.delegate = self
        self.tf_Sugar.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tf_Blood {
            self.showAlertPickerViewAction()
            return false
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.tf_Weight:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Height:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Blood:
            // Picker View
            return true
            
        case self.tf_Pressure_0:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Pressure_1:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_SPO2:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Sugar:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        default:
            return true
        }
    }
}

// MARK: - AlertPickerViewDelegate
extension MeasurementViewController: AlertPickerViewDelegate {
    func showAlertPickerViewAction() {
        var listItem: [ItemPickerView] = []
        for (index, blood) in BloodGroup.allCases.enumerated() {
            let item = ItemPickerView()
            item.id = index
            item.name = blood.name
            if blood.name == self.tf_Blood.text {
                item.isSelected = true
            } else {
                item.isSelected = false
            }
            
            listItem.append(item)
        }
        
        let controller = AlertPickerViewViewController()
        controller.modalPresentationStyle = .custom
        controller.alertTitle = "Chọn nhóm máu"
        controller.delegate = self
        controller.listItemPickerView = listItem
        self.navigationController?.present(controller, animated: true)
    }
    
    func onAlertCancelAction() {
        //
    }
    
    func onAlertAgreeAction(item: ItemPickerView) {
        let blood = BloodGroup.allCases[item.id ?? 0]
        self.tf_Blood.text = blood.name
        self.presenter.onDidChangeBloodGroup(value: blood.name)
    }
}

// MARK: - InitKeyboardToolBar
extension MeasurementViewController {
    private func setInitKeyboardToolBar() {
        let weight = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneWeightAction))
        self.tf_Weight.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số cân nặng", rightBarButtonConfiguration: weight)
        
        let height = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneHeightAction))
        self.tf_Height.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số chiều cao", rightBarButtonConfiguration: height)
        
        let pressure_0 = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDonePressure_0_Action))
        self.tf_Pressure_0.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số huyết áp", rightBarButtonConfiguration: pressure_0)
        
        let pressure_1 = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDonePressure_1_Action))
        self.tf_Pressure_1.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số huyết áp", rightBarButtonConfiguration: pressure_1)
        
        let spo2 = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneSpo2Action))
        self.tf_SPO2.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số SPO2", rightBarButtonConfiguration: spo2)
        
        let sugar = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneSugarAction))
        self.tf_Sugar.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số đường huyết", rightBarButtonConfiguration: sugar)
    }
    
    @objc func onDoneWeightAction() {
        guard self.tf_Weight.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số cân nặng để tiếp tục")
            return
        }
        guard self.tf_Weight.text?.first != "0" && self.tf_Weight.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số cân nặng không đúng")
            return
        }
        guard let value = Int(self.tf_Weight.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số cân nặng không đúng")
            return
        }
        
        self.view.endEditing(true)
        self.presenter.onDidChangeHeight(value: value)
    }
    
    @objc func onDoneHeightAction() {
        guard self.tf_Height.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số chiều cao để tiếp tục")
            return
        }
        guard self.tf_Height.text?.first != "0" && self.tf_Height.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số chiều cao không đúng")
            return
        }
        guard let value = Int(self.tf_Height.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số chiều cao không đúng")
            return
        }
        
        self.view.endEditing(true)
        self.presenter.onDidChangeHeight(value: value)
    }
    
    @objc func onDonePressure_0_Action() {
        self.onDonePressureAction()
    }
    
    @objc func onDonePressure_1_Action() {
        self.onDonePressureAction()
    }
    
    @objc func onDoneSpo2Action() {
        guard self.tf_SPO2.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số SPO2 để tiếp tục")
            return
        }
        guard self.tf_SPO2.text?.first != "0" && self.tf_SPO2.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số SPO2 không đúng")
            return
        }
        guard let value = Int(self.tf_SPO2.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số SPO2 không đúng")
            return
        }
        
        self.view.endEditing(true)
        self.presenter.onDidChangeBloodSPO2(value: value)
    }
    
    @objc func onDoneSugarAction() {
        guard self.tf_Sugar.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số đường huyết để tiếp tục")
            return
        }
        guard self.tf_Sugar.text?.first != "0" && self.tf_Sugar.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số đường huyết không đúng")
            return
        }
        guard let value = Int(self.tf_Sugar.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số đường huyết không đúng")
            return
        }
        
        self.view.endEditing(true)
        self.presenter.onDidChangeBloodSugar(value: value)
    }
    
    func onDonePressureAction() {
        guard self.tf_Pressure_0.text?.count ?? 0 > 0 && self.tf_Pressure_1.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số huyết áp để tiếp tục")
            return
        }
        guard self.tf_Pressure_0.text?.first != "0" && self.tf_Pressure_1.text?.first != "0"  else {
            SKToast.shared.showToast(content: "Nhập chỉ số huyết áp không đúng")
            return
        }
        guard self.tf_Pressure_0.text?.count ?? 0 >= 2 && self.tf_Pressure_1.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số huyết áp không đúng")
            return
        }
        guard let value_0 = Int(self.tf_Pressure_0.text ?? ""), let value_1 = Int(self.tf_Pressure_1.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số huyết áp không đúng")
            return
        }
        
        let value = String(format: "%@/%@", "\(value_0)", "\(value_1)")
        self.view.endEditing(true)
        self.presenter.onDidChangeBloodPressure(value: value)
    }
}
