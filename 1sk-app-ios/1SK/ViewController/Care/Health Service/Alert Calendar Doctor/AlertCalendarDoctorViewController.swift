//
//  AlertCalendarDoctorViewController.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//

import UIKit

protocol AlertCalendarDoctorViewDelegate: AnyObject {
    func onAlertCalendarDoctorViewCancelAction()
    func onAlertCalendarDoctorViewAgreeAction(doctor: DoctorModel?)
}

class AlertCalendarDoctorViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_NameDoctor: UILabel!
    
    @IBOutlet weak var btn_Continue: UIButton!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    @IBOutlet weak var view_NoData: UIView!
    @IBOutlet weak var constraint_height_ContentView: NSLayoutConstraint!
    
    weak var delegate: AlertCalendarDoctorViewDelegate?
    
    var doctor: DoctorModel?
    var listWeekday: [WeekdayModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitLayoutView()
        self.setInitDataDoctor()
        self.setInitUITableView()
        self.setInitGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    func setInitLayoutView() {
        self.btn_Continue.setTitleColor(.white, for: .normal)
        self.btn_Continue.setTitleColor(.white, for: .selected)
        
        if self.listWeekday.count == 0 {
            self.view_NoData.isHidden = false
            self.btn_Continue.isHidden = true
            self.constraint_height_ContentView.constant = 311
        }
    }

    func setInitDataDoctor() {
        self.lbl_NameDoctor.text = "\(self.doctor?.academicTitle?.shortName ?? ""). \(self.doctor?.basicInformation?.fullName ?? "")"
        self.img_Avatar.setImageWith(imageUrl: self.doctor?.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
    }

    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertCalendarDoctorViewCancelAction()
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        guard self.btn_Continue.isSelected == true else {
            SKToast.shared.showToast(content: "Bạn vui lòng chọn giờ lịch hẹn để tiếp tục")
            return
        }
        
        for weekday in self.listWeekday {
            if let listSlotTimes = weekday.slotTimes {
                var isExpand = false
                for time in listSlotTimes {
                    if time.isSelected == true {
                        isExpand = true
                        break
                    }
                }
                weekday.isExpand = isExpand
            }
        }
        
        self.delegate?.onAlertCalendarDoctorViewAgreeAction(doctor: self.doctor)
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertCalendarDoctorViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertCalendarDoctorViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureBgAction)))
    }
    
    @objc func onTapGestureBgAction() {
        self.delegate?.onAlertCalendarDoctorViewCancelAction()
        self.setAnimationHidden()
    }
}

// MARK: - UITableViewDataSource
extension AlertCalendarDoctorViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        if #available(iOS 15.0, *) {
            self.tbv_TableView.sectionHeaderTopPadding = 0.0
            
        } else {
            // Fallback on earlier versions
        }
        self.tbv_TableView.sectionFooterHeight = 0.0
        
        self.tbv_TableView.registerNib(ofType: CellTableViewDateDoctor.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderCalendarDoctorView.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.listWeekday.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: SectionHeaderCalendarDoctorView.self)
        header.section = section
        header.delegate = self
        header.config(weekday: self.listWeekday[section])
        
        if section == (self.listWeekday.count - 1) && self.listWeekday[section].isExpand != true {
            header.setHiddenLineBottom(isHidden: true)

        } else {
            header.setHiddenLineBottom(isHidden: false)
        }
        
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isExpand = self.listWeekday[section].isExpand
        if isExpand == true {
            return 1
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let count = self.listWeekday[indexPath.section].slotTimes?.count ?? 0
        var row = count / 3
        let sum = count % 3
        if sum != 0 {
            row += 1
        }

        let height = 40 * row
        return CGFloat(height + 10)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewDateDoctor.self, for: indexPath)
        cell.delegate = self
        cell.weekday = self.listWeekday[indexPath.section]
        cell.onReloadData()
        return cell
    }
}

// MARK: - SectionHeaderCalendarDoctorViewDelegate
extension AlertCalendarDoctorViewController: SectionHeaderCalendarDoctorViewDelegate {
    func onSectionHeaderCalendarAction(section: Int, isExpand: Bool) {
        guard self.listWeekday.count > 1 else {
            return
        }
        
        for (index, item) in self.listWeekday.enumerated() {
            if item.isExpand == true {
                item.isExpand = false
                self.tbv_TableView.reloadSections(IndexSet(integer: index), with: .fade)
            }
        }
        
        self.listWeekday[section].isExpand = !isExpand
        self.tbv_TableView.reloadSections(IndexSet(integer: section), with: .fade)
        
        if self.listWeekday[section].isExpand == true {
            let indexPath = IndexPath(row: 0, section: section)
            self.tbv_TableView.scrollToRow(at: indexPath, at: .none, animated: true)
        }
    }
}

// MARK: CellTableViewDateDoctorDelegate
extension AlertCalendarDoctorViewController: CellTableViewDateDoctorDelegate {
    func onSelectSlotTimesAction(slotTime: SlotTimesModel) {
        for weekday in self.listWeekday {
            if let listSlotTimes = weekday.slotTimes {
                for time in listSlotTimes {
                    if time.id == slotTime.id && time.startAt == slotTime.startAt && time.endAt == slotTime.endAt {
                        time.isSelected = true
                        self.btn_Continue.backgroundColor = R.color.mainColor()
                        self.btn_Continue.isSelected = true
                        
                    } else {
                        time.isSelected = false
                    }
                }
            }
        }
        
        self.tbv_TableView.reloadData()
    }
}
