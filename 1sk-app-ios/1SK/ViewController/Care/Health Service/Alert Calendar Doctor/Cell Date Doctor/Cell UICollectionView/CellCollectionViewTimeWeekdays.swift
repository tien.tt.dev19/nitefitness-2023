//
//  CellCollectionViewTimeWeekdays.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import UIKit

protocol CellCollectionViewTimeWeekdaysDelegate: AnyObject {
    func onSelectSlotTimesAction(slotTime: SlotTimesModel)
}

class CellCollectionViewTimeWeekdays: UICollectionViewCell {

    @IBOutlet weak var view_SlotTimes: UIView!
    @IBOutlet weak var lbl_SlotTimes: UILabel!
    
    weak var delegate: CellCollectionViewTimeWeekdaysDelegate?
    var slotTime: SlotTimesModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitGesrureView()
    }

    func config(slotTimes: SlotTimesModel) {
        self.slotTime = slotTimes
        
        let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTimes.startAt ?? 0))
        let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTimes.endAt ?? 0))
        
        self.lbl_SlotTimes.text = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
        
        if slotTimes.isSelected == true {
            self.view_SlotTimes.backgroundColor = R.color.mainColor()
            self.lbl_SlotTimes.textColor = .white
            
        } else {
            self.view_SlotTimes.backgroundColor = R.color.line_3()
            self.lbl_SlotTimes.textColor = R.color.subTitle()
        }
    }
    
    func setInitGesrureView() {
        self.view_SlotTimes.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectSlotTimesAction)))
    }
    
    @objc func onSelectSlotTimesAction() {
        self.delegate?.onSelectSlotTimesAction(slotTime: self.slotTime!)
    }
}
