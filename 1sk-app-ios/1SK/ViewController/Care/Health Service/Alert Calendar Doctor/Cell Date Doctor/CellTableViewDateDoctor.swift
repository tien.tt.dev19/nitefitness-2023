//
//  CellTableViewDateDoctor.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//

import UIKit

protocol CellTableViewDateDoctorDelegate: AnyObject {
    func onSelectSlotTimesAction(slotTime: SlotTimesModel)
}

class CellTableViewDateDoctor: UITableViewCell {

    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    weak var delegate: CellTableViewDateDoctorDelegate?
    
    var weekday: WeekdayModel?
    
    private var listSlotTimes: [SlotTimesModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUICollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func onReloadData() {
        self.listSlotTimes = self.weekday?.slotTimes ?? []
        self.coll_CollectionView.reloadData()
    }
}

// MARK: UICollectionViewDataSource
extension CellTableViewDateDoctor: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewTimeWeekdays.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listSlotTimes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewTimeWeekdays.self, for: indexPath)
        cell.delegate = self
        cell.config(slotTimes: self.listSlotTimes[indexPath.row])
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CellTableViewDateDoctor: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.coll_CollectionView.width - 26) / 3
        return CGSize(width: width, height: 30)
    }
}

// MARK: UICollectionViewDelegate
extension CellTableViewDateDoctor: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: CellCollectionViewTimeWeekdaysDelegate
extension CellTableViewDateDoctor: CellCollectionViewTimeWeekdaysDelegate {
    func onSelectSlotTimesAction(slotTime: SlotTimesModel) {
        self.delegate?.onSelectSlotTimesAction(slotTime: slotTime)
    }
}
