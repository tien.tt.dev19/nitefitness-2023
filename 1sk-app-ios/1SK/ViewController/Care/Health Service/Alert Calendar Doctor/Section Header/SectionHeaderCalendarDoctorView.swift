//
//  SectionHeaderCalendarDoctorView.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//

import UIKit

protocol SectionHeaderCalendarDoctorViewDelegate: AnyObject {
    func onSectionHeaderCalendarAction(section: Int, isExpand: Bool)
}

class SectionHeaderCalendarDoctorView: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_Weekday: UILabel!
    @IBOutlet weak var lbl_Day: UILabel!
    
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var img_Expand: UIImageView!
    
    @IBOutlet weak var view_LineTop: UIView!
    @IBOutlet weak var view_LineBottom: UIView!
    
    weak var delegate: SectionHeaderCalendarDoctorViewDelegate?
    var section: Int?
    var weekday: WeekdayModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitGestureView()
    }

    func config(weekday: WeekdayModel) {
        self.weekday = weekday
        
        if let timeStamp = weekday.date {
            let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(timeStamp))
            self.lbl_Weekday.text = date.toString(Date.Format.eee) + ","
            self.lbl_Day.text = date.toString(Date.Format.dmySlash)
        }
        
        let isExpand = weekday.isExpand
        if isExpand == true {
            self.img_Expand.image = R.image.ic_arrow_expand()
        } else {
            self.img_Expand.image = R.image.ic_arrow_collapse()
        }
    }
    
    func setHiddenLineTop(isHidden: Bool) {
        self.view_LineTop.isHidden = isHidden
    }
    
    func setHiddenLineBottom(isHidden: Bool) {
        self.view_LineBottom.isHidden = isHidden
    }
}

extension SectionHeaderCalendarDoctorView {
    func setInitGestureView() {
        self.view_Content.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapBgAction)))
    }
    
    @objc func onTapBgAction() {
        guard let _section = self.section else {
            return
        }
        self.delegate?.onSectionHeaderCalendarAction(section: _section, isExpand: self.weekday?.isExpand ?? false)
    }
}
