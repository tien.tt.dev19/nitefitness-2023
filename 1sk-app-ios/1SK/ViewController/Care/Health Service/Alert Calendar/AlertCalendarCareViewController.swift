//
//  AlertCalendarCareViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/27/21.
//

import UIKit

protocol AlertCalendarCareViewDelegate: AnyObject {
    func onAlertCancelAction()
    func onAlertAgreeAction(doctor: DoctorModel?)
}

enum Weekday: Int {
    case day_0, day_1, day_2, day_3, day_4, day_5, day_6, none
}

class AlertCalendarCareViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_NameDoctor: UILabel!
    
    @IBOutlet weak var view_Section_0: UIView!
    @IBOutlet weak var view_Section_1: UIView!
    @IBOutlet weak var view_Section_2: UIView!
    
    @IBOutlet weak var view_Day_0: UIView!
    @IBOutlet weak var view_Day_1: UIView!
    @IBOutlet weak var view_Day_2: UIView!
    @IBOutlet weak var view_Day_3: UIView!
    @IBOutlet weak var view_Day_4: UIView!
    @IBOutlet weak var view_Day_5: UIView!
    @IBOutlet weak var view_Day_6: UIView!
    
    @IBOutlet weak var lbl_Weekday_0: UILabel!
    @IBOutlet weak var lbl_Weekday_1: UILabel!
    @IBOutlet weak var lbl_Weekday_2: UILabel!
    @IBOutlet weak var lbl_Weekday_3: UILabel!
    @IBOutlet weak var lbl_Weekday_4: UILabel!
    @IBOutlet weak var lbl_Weekday_5: UILabel!
    @IBOutlet weak var lbl_Weekday_6: UILabel!
    
    @IBOutlet weak var lbl_Day_0: UILabel!
    @IBOutlet weak var lbl_Day_1: UILabel!
    @IBOutlet weak var lbl_Day_2: UILabel!
    @IBOutlet weak var lbl_Day_3: UILabel!
    @IBOutlet weak var lbl_Day_4: UILabel!
    @IBOutlet weak var lbl_Day_5: UILabel!
    @IBOutlet weak var lbl_Day_6: UILabel!
    
    @IBOutlet weak var coll_CollectionView_0: UICollectionView!
    @IBOutlet weak var coll_CollectionView_1: UICollectionView!
    @IBOutlet weak var coll_CollectionView_2: UICollectionView!
    
    @IBOutlet weak var btn_Continue: UIButton!
    
    weak var delegate: AlertCalendarCareViewDelegate?
    
    var listWeekday: [WeekdayModel] = []
    var listSlotTimes: [SlotTimesModel] = []
    
    var weekdaySelected: Weekday = .none
    var isStateEdit = false
    
    var doctor: DoctorModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitUICollectionView()
        self.setInitGestureView()
        self.setDataViewCalendar(isInit: true)
        
        self.btn_Continue.setTitleColor(.white, for: .normal)
        self.btn_Continue.setTitleColor(.white, for: .selected)
        
        self.setDataDoctor()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    func setDataDoctor() {
        self.lbl_NameDoctor.text = "\(self.doctor?.academicTitle?.shortName ?? ""). \(self.doctor?.basicInformation?.fullName ?? "")"
        self.img_Avatar.setImageWith(imageUrl: self.doctor?.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertCancelAction()
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        var isDidSelectTime = false
        for weekday in self.listWeekday {
            if let listSlotTimes = weekday.slotTimes {
                for time in listSlotTimes {
                    if time.isSelected == true {
                        isDidSelectTime = true
                        break
                    }
                }
            }
        }
        
        guard isDidSelectTime == true && self.btn_Continue.isSelected == true else {
            SKToast.shared.showToast(content: "Bạn vui lòng chọn giờ lịch hẹn để tiếp tục")
            return
        }
        
        self.delegate?.onAlertAgreeAction(doctor: self.doctor)
        self.setAnimationHidden()
    }
}

// MARK: Layout View
extension AlertCalendarCareViewController {
    func setReloadViewCalendar() {
        self.listSlotTimes = self.listWeekday[self.weekdaySelected.rawValue].slotTimes ?? []
        for (index, weekday) in self.listWeekday.enumerated() {
            if weekday.isState != .disable {
                if index == self.weekdaySelected.rawValue {
                    weekday.isState = .selected
                    self.setSelectedSection(index: index)
                    
                } else {
                    weekday.isState = .enable
                }
            }
        }
        
        self.setDataViewCalendar(isInit: false)
    }
    
    func setDataViewCalendar(isInit: Bool) {
        for (index, weekday) in self.listWeekday.enumerated() {
            switch index {
            case 0:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_0
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_0
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_0,
                                    lblWeekday: self.lbl_Weekday_0,
                                    lblTime: self.lbl_Day_0)
                
            case 1:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_1
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_1
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_1,
                                    lblWeekday: self.lbl_Weekday_1,
                                    lblTime: self.lbl_Day_1)
                
            case 2:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_2
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_2
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_2,
                                    lblWeekday: self.lbl_Weekday_2,
                                    lblTime: self.lbl_Day_2)
                
            case 3:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_3
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_3
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_3,
                                    lblWeekday: self.lbl_Weekday_3,
                                    lblTime: self.lbl_Day_3)
                
            case 4:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_4
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_4
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_4,
                                    lblWeekday: self.lbl_Weekday_4,
                                    lblTime: self.lbl_Day_4)
                
            case 5:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_5
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_5
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_5,
                                    lblWeekday: self.lbl_Weekday_5,
                                    lblTime: self.lbl_Day_5)
                
            case 6:
                if isInit == true {
                    if self.isStateEdit == true {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_6
                        }
                    } else {
                        if weekday.isState == .selected {
                            self.weekdaySelected = .day_6
                        }
                    }
                }
                self.setWeekdayData(weekday: weekday,
                                    viewWeekday: self.view_Day_6,
                                    lblWeekday: self.lbl_Weekday_6,
                                    lblTime: self.lbl_Day_6)
                
            default:
                break
            }
            
            
            if isInit == true {
                if self.isStateEdit == true {
                    if weekday.isState == .selected {
                        self.listSlotTimes = self.listWeekday[self.weekdaySelected.rawValue].slotTimes ?? []
                        self.setSelectedSection(index: index)
                    }
                } else {
                    if weekday.isState == .selected {
                        self.listSlotTimes = self.listWeekday[self.weekdaySelected.rawValue].slotTimes ?? []
                        self.setSelectedSection(index: index)
                    }
                }
            }
            
        }
    }
    
    func setWeekdayData(weekday: WeekdayModel, viewWeekday: UIView, lblWeekday: UILabel, lblTime: UILabel) {
        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
        
        lblWeekday.text = date.toString(Date.Format.eee)
        lblTime.text = date.toString(Date.Format.dmySlash)
        
        if let state = weekday.isState {
            switch state {
            case WeekdayState.selected:
                viewWeekday.backgroundColor = R.color.mainColor()
                lblWeekday.textColor = .white
                lblTime.textColor = .white
                
            case WeekdayState.disable:
                viewWeekday.backgroundColor = .white
                lblTime.textColor = R.color.color_Gray_Text_Disable()
                
                if weekday.isToday == true {
                    lblWeekday.textColor = R.color.mainColor()
                } else {
                    lblWeekday.textColor = R.color.color_Gray_Text_Disable()
                }
                
            case WeekdayState.enable:
                viewWeekday.backgroundColor = .white
                lblTime.textColor = .darkGray
                
                if weekday.isToday == true {
                    lblWeekday.textColor = R.color.mainColor()
                } else {
                    lblWeekday.textColor = R.color.color_Dark_Text()
                }
            }
        }
    }
    
    func setSelectedSection(index: Int) {
        switch index {
        case 0, 1, 2:
            self.coll_CollectionView_0.reloadData()
            
            self.view_Section_0.isHidden = false
            self.view_Section_1.isHidden = true
            self.view_Section_2.isHidden = true
            
        case 3, 4, 5:
            self.coll_CollectionView_1.reloadData()
            
            self.view_Section_0.isHidden = true
            self.view_Section_1.isHidden = false
            self.view_Section_2.isHidden = true
            
        case 6, 7, 8:
            self.coll_CollectionView_2.reloadData()
            
            self.view_Section_0.isHidden = true
            self.view_Section_1.isHidden = true
            self.view_Section_2.isHidden = false
            
        default:
            break
        }
    }
}

// MARK: UICollectionViewDataSource
extension AlertCalendarCareViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView_0.registerNib(ofType: CellCollectionViewTimeWeekdays.self)
        self.coll_CollectionView_0.dataSource = self
        self.coll_CollectionView_0.delegate = self
        
        self.coll_CollectionView_1.registerNib(ofType: CellCollectionViewTimeWeekdays.self)
        self.coll_CollectionView_1.dataSource = self
        self.coll_CollectionView_1.delegate = self
        
        self.coll_CollectionView_2.registerNib(ofType: CellCollectionViewTimeWeekdays.self)
        self.coll_CollectionView_2.dataSource = self
        self.coll_CollectionView_2.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listSlotTimes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewTimeWeekdays.self, for: indexPath)
        cell.delegate = self
        cell.config(slotTimes: self.listSlotTimes[indexPath.row])
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension AlertCalendarCareViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch collectionView {
        case self.coll_CollectionView_0:
            let width = (self.coll_CollectionView_0.width - 26) / 3
            return CGSize(width: width, height: 30)
            
        case self.coll_CollectionView_1:
            let width = (self.coll_CollectionView_1.width - 26) / 3
            return CGSize(width: width, height: 30)
            
        case self.coll_CollectionView_2:
            let width = (self.coll_CollectionView_2.width - 26) / 3
            return CGSize(width: width, height: 30)
            
        default:
            return CGSize(width: 0, height: 0)
        }
    }
}

// MARK: UICollectionViewDelegate
extension AlertCalendarCareViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: UICollectionViewDelegate
extension AlertCalendarCareViewController: CellCollectionViewTimeWeekdaysDelegate {
    func onSelectSlotTimesAction(slotTime: SlotTimesModel) {
        for weekday in self.listWeekday {
            if let listSlotTimes = weekday.slotTimes {
                for time in listSlotTimes {
                    if time.id == slotTime.id && time.startAt == slotTime.startAt && time.endAt == slotTime.endAt {
                        time.isSelected = true
                        self.btn_Continue.backgroundColor = R.color.mainColor()
                        self.btn_Continue.isSelected = true
                        
                    } else {
                        time.isSelected = false
                    }
                }
            }
        }
        
        switch self.weekdaySelected {
        case .day_0, .day_1, .day_2:
            self.coll_CollectionView_0.reloadData()
            
        case .day_3, .day_4, .day_5:
            self.coll_CollectionView_1.reloadData()
            
        case .day_6:
            self.coll_CollectionView_2.reloadData()
            
        default:
            break
        }
        
    }
}

// MARK: - Animation
extension AlertCalendarCareViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertCalendarCareViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureBgAction)))
        
        self.view_Day_0.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapMondayAction)))
        self.view_Day_1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapTuesdayAction)))
        self.view_Day_2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapWednesdayAction)))
        
        self.view_Day_3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapThursdayAction)))
        self.view_Day_4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapFridayAction)))
        self.view_Day_5.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapSaturdayAction)))
        
        self.view_Day_6.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapSundayAction)))
    }
    
    @objc func onTapGestureBgAction() {
        self.delegate?.onAlertCancelAction()
        self.setAnimationHidden()
    }
    
    @objc func onTapMondayAction() {
        let weekday = self.listWeekday[Weekday.day_0.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_0
        self.setReloadViewCalendar()
    }
    
    @objc func onTapTuesdayAction() {
        let weekday = self.listWeekday[Weekday.day_1.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_1
        self.setReloadViewCalendar()
    }
    
    @objc func onTapWednesdayAction() {
        let weekday = self.listWeekday[Weekday.day_2.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_2
        self.setReloadViewCalendar()
    }
    
    @objc func onTapThursdayAction() {
        let weekday = self.listWeekday[Weekday.day_3.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_3
        self.setReloadViewCalendar()
    }
    
    @objc func onTapFridayAction() {
        let weekday = self.listWeekday[Weekday.day_4.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_4
        self.setReloadViewCalendar()
    }
    
    @objc func onTapSaturdayAction() {
        let weekday = self.listWeekday[Weekday.day_5.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_5
        self.setReloadViewCalendar()
    }
    
    @objc func onTapSundayAction() {
        let weekday = self.listWeekday[Weekday.day_6.rawValue]
        if weekday.isState == .disable {
            self.alertDoctorBusy(weekday: weekday)
            return
        }
        self.onClearDataSelectWeekday()
        self.weekdaySelected = .day_6
        self.setReloadViewCalendar()
    }
    
    func alertDoctorBusy(weekday: WeekdayModel) {
        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
        SKToast.shared.showToast(content: "\(date.dayOfWeek(format: "EEEE") ?? "")\n\(date.toString(Date.Format.dmySlash))\n\nBác sĩ đã kín lịch\nBạn vui lòng chọn thời gian khác")
    }
    
    func onClearDataSelectWeekday() {
        for weekday in self.listWeekday {
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if slotTime.isSelected == true {
                        slotTime.isSelected = false
                    }
                }
            }
        }
        
        self.btn_Continue.isSelected = false
        self.btn_Continue.backgroundColor = R.color.grayBgButton()
    }
}
