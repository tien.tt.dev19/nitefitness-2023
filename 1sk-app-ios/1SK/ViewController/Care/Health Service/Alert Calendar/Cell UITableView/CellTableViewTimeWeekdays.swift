//
//  CellTableViewTimeWeekdays.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import UIKit

class CellTableViewTimeWeekdays: UITableViewCell {

    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUICollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: UICollectionViewDataSource
extension CellTableViewTimeWeekdays: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewTimeWeekdays.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewTimeWeekdays.self, for: indexPath)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CellTableViewTimeWeekdays: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.coll_CollectionView.width - 26) / 3
        return CGSize(width: width, height: 30)
    }
}

// MARK: UICollectionViewDelegate
extension CellTableViewTimeWeekdays: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
    }
}
