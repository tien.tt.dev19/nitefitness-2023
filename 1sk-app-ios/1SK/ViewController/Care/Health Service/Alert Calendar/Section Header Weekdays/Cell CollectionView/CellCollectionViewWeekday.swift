//
//  CellCollectionViewWeekday.swift
//  1SK
//
//  Created by vuongbachthu on 9/30/21.
//

import UIKit

class CellCollectionViewWeekday: UICollectionViewCell {

    @IBOutlet weak var view_Weekday: UIView!
    
    @IBOutlet weak var lbl_Weekday: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    var weekday: WeekdayModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(weekday: WeekdayModel) {
        self.weekday = weekday
        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
        
        self.lbl_Weekday.text = date.dayOfWeek()
        self.lbl_Time.text = date.toString(Date.Format.dmySlash)
        
        if let state = weekday.isState {
            switch state {
            case WeekdayState.selected:
                self.view_Weekday.backgroundColor = R.color.mainColor()
                self.lbl_Weekday.textColor = .white
                self.lbl_Time.textColor = .white
                
            case WeekdayState.disable:
                self.view_Weekday.backgroundColor = .white
                self.lbl_Weekday.textColor = R.color.color_Gray_Text_Disable()
                self.lbl_Time.textColor = R.color.color_Gray_Text_Disable()
                
            case WeekdayState.enable:
                if weekday.isToday == true {
                    self.lbl_Weekday.textColor = R.color.mainColor()
                    self.lbl_Time.textColor = .darkGray
                    
                } else {
                    self.lbl_Weekday.textColor = R.color.color_Dark_Text()
                    self.lbl_Time.textColor = .darkGray
                }
            }
        }
    }
}
