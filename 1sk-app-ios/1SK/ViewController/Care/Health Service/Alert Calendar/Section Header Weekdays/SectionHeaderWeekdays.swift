//
//  SectionHeaderWeekdays.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import UIKit

protocol SectionHeaderWeekdaysDelegate: AnyObject {
    func onSelectWeekday(in section: Int, _ index: Int, _ weekday: WeekdayModel?)
}

class SectionHeaderWeekdays: UITableViewHeaderFooterView {
    
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    weak var delegate: SectionHeaderWeekdaysDelegate?
    
    var section = 0
    var sectionWeekday: SectionWeekdayModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitUICollectionView()
        
    }
    
    override func prepareForReuse() {
        self.sectionWeekday = nil
    }
}

// MARK: UICollectionViewDataSource
extension SectionHeaderWeekdays: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewWeekday.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sectionWeekday?.listWeekdayModel.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewWeekday.self, for: indexPath)
        cell.config(weekday: self.sectionWeekday?.listWeekdayModel[indexPath.row] ?? WeekdayModel())
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension SectionHeaderWeekdays: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (self.coll_CollectionView.width - 26) / 3
        return CGSize(width: width, height: 57)
    }
}

// MARK: UICollectionViewDelegate
extension SectionHeaderWeekdays: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let weekday = self.sectionWeekday!.listWeekdayModel[indexPath.row]
        if weekday.isState == .disable {
            return
        }
        
        self.delegate?.onSelectWeekday(in: self.section, indexPath.row, weekday)
    }
}
