//
//  AlertDoctorInfoViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/27/21.
//

import UIKit

class AlertDoctorInfoViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    
    @IBOutlet weak var lbl_Doctor: UILabel!
    @IBOutlet weak var lbl_Specialist: UILabel!
    @IBOutlet weak var lbl_WorkPlace: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitLayoutView()
    }

    func setInitLayoutView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBg)))
    }
    
    @objc func onTouchBg() {
        self.removeAnimate()
    }
    
    func config(doctor: DoctorModel) {
        self.lbl_Doctor.text = "\(doctor.academicTitle?.shortName ?? ""). \(doctor.basicInformation?.fullName ?? "")"
        
        if let specialist = doctor.specialist?.first {
            let atrSpecialist = NSMutableAttributedString(string: "Chuyên khoa: ", attributes: [.foregroundColor: R.color.color_Gray_Text()!])
            atrSpecialist.append(NSAttributedString(string: specialist.name ?? "", attributes: [.foregroundColor: R.color.color_Dark_Text()!]))
            self.lbl_Specialist.attributedText = atrSpecialist
        } else {
            self.lbl_Specialist.text = "Chuyên khoa: "
        }
        
        let officeAddress = doctor.advanceInformation?.officeAddress
        let atrWorkPlace = NSMutableAttributedString(string: "Nơi công tác: ", attributes: [.foregroundColor: R.color.color_Gray_Text()!])
        atrWorkPlace.append(NSAttributedString(string: officeAddress ?? "", attributes: [.foregroundColor: R.color.color_Dark_Text()!]))
        self.lbl_WorkPlace.attributedText = atrWorkPlace
        
        self.img_Avatar.setImageWith(imageUrl: doctor.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
    }

    @IBAction func onDismissAction(_ sender: Any) {
        self.removeAnimate()
    }
}

// MARK: - Animation
extension AlertDoctorInfoViewController {
    func showInView(_ aView: UIViewController, animated: Bool) {
        aView.view.addSubview(self.view)
        aView.addChild(self)
        
        
        
        if animated == true {
            self.showAnimate()
        }
    }
    
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                self.view.removeFromSuperview()
                //self.navigationController?.popViewController(animated: false)
            }
        })
    }
}
