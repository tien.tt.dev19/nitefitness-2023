//
//  AlertSpecialistViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import UIKit

protocol AlertSpecialistViewDelegate: AnyObject {
    func onSelectSpecialistAction(specialist: SpecialistModel)
}

class AlertSpecialistViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var constraint_height_TableView: NSLayoutConstraint!
    
    weak var delegate: AlertSpecialistViewDelegate?
    
    var listSpecialist: [SpecialistModel] = []
    private let heightForRow: CGFloat = 56
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.setInitUITableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
}

// MARK: - Animation
extension AlertSpecialistViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertSpecialistViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}

// MARK: - UITableViewDataSource
extension AlertSpecialistViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewAlertSpecialist.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.constraint_height_TableView.constant = CGFloat(self.listSpecialist.count) * self.heightForRow
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightForRow
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listSpecialist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewAlertSpecialist.self, for: indexPath)
        let specialist = self.listSpecialist[indexPath.row]
        cell.config(specialist: specialist)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AlertSpecialistViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath) as? CellTableViewAlertSpecialist
        cell?.lbl_Specialist.textColor = R.color.mainColor()
        tableView.reloadRows(at: [indexPath], with: .automatic)
        
        self.delegate?.onSelectSpecialistAction(specialist: self.listSpecialist[indexPath.row])
        
        self.setAnimationHidden()
    }
}
