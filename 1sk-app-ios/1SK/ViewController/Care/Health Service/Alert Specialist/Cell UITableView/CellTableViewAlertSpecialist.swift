//
//  CellTableViewAlertSpecialist.swift
//  1SK
//
//  Created by vuongbachthu on 9/28/21.
//

import UIKit

class CellTableViewAlertSpecialist: UITableViewCell {

    @IBOutlet weak var lbl_Specialist: UILabel!
    
    var specialist: SpecialistModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(specialist: SpecialistModel) {
        self.specialist = specialist
        self.lbl_Specialist.text = specialist.name
    }
}
