//
//  
//  AuthPhoneNumberConstract.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//
//

import UIKit

// MARK: - View
protocol AuthPhoneNumberViewProtocol: AnyObject {

}

// MARK: - Presenter
protocol AuthPhoneNumberPresenterProtocol {
    func onViewDidLoad()
}

// MARK: - Interactor Input
protocol AuthPhoneNumberInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol AuthPhoneNumberInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol AuthPhoneNumberRouterProtocol {

}
