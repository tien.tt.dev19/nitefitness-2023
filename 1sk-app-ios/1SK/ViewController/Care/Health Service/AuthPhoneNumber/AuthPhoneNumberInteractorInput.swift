//
//  
//  AuthPhoneNumberInteractorInput.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//
//

import UIKit

class AuthPhoneNumberInteractorInput {
    weak var output: AuthPhoneNumberInteractorOutputProtocol?
}

// MARK: - AuthPhoneNumberInteractorInputProtocol
extension AuthPhoneNumberInteractorInput: AuthPhoneNumberInteractorInputProtocol {

}
