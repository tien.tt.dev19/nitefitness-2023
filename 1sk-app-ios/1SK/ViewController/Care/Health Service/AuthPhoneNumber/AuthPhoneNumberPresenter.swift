//
//  
//  AuthPhoneNumberPresenter.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//
//

import UIKit

class AuthPhoneNumberPresenter {

    weak var view: AuthPhoneNumberViewProtocol?
    private var interactor: AuthPhoneNumberInteractorInputProtocol
    private var router: AuthPhoneNumberRouterProtocol

    init(interactor: AuthPhoneNumberInteractorInputProtocol,
         router: AuthPhoneNumberRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - AuthPhoneNumberPresenterProtocol
extension AuthPhoneNumberPresenter: AuthPhoneNumberPresenterProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - AuthPhoneNumberInteractorOutput 
extension AuthPhoneNumberPresenter: AuthPhoneNumberInteractorOutputProtocol {

}
