//
//  
//  AuthPhoneNumberRouter.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//
//

import UIKit

class AuthPhoneNumberRouter {
    weak var viewController: AuthPhoneNumberViewController?
    static func setupModule() -> AuthPhoneNumberViewController {
        let viewController = AuthPhoneNumberViewController()
        let router = AuthPhoneNumberRouter()
        let interactorInput = AuthPhoneNumberInteractorInput()
        let presenter = AuthPhoneNumberPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - AuthPhoneNumberRouterProtocol
extension AuthPhoneNumberRouter: AuthPhoneNumberRouterProtocol {
    
}
