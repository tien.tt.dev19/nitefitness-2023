//
//  
//  AuthPhoneNumberViewController.swift
//  1SK
//
//  Created by Thaad on 07/02/2022.
//
//

import UIKit

class AuthPhoneNumberViewController: BaseViewController {

    var presenter: AuthPhoneNumberPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {

    }
    
    // MARK: - Action
    
}

// MARK: - AuthPhoneNumberViewProtocol
extension AuthPhoneNumberViewController: AuthPhoneNumberViewProtocol {
    
}
