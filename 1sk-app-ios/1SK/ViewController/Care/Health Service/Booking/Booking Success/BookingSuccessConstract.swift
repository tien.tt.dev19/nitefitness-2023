//
//  
//  BookingSuccessConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/25/21.
//
//

import UIKit

// MARK: - View
protocol BookingSuccessViewProtocol: AnyObject {
    func setDataView(appointment: AppointmentModel?)
    func setPaymentPeriodTime(paymentPeriod: PaymentPeriodModel)
}

// MARK: - Presenter
protocol BookingSuccessPresenterProtocol {
    func onViewDidLoad()
    
    // Action
    func onDetailBookingAction()
    func onCloseAction()
    func onCopyBankNumberAction()
    func onCopyBankContentAction()
    
    // Value
    func getAppointment() -> AppointmentModel?
}

// MARK: - Interactor Input
protocol BookingSuccessInteractorInputProtocol {
    func getPaymentPeriodTime()
}

// MARK: - Interactor Output
protocol BookingSuccessInteractorOutputProtocol: AnyObject {
    func onGetPaymentPeriodTimeFinished(with result: Result<PaymentPeriodModel, APIError>)
}

// MARK: - Router
protocol BookingSuccessRouterProtocol: BaseRouterProtocol {
    
}
