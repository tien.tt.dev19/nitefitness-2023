//
//  
//  BookingSuccessInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/25/21.
//
//

import UIKit

class BookingSuccessInteractorInput {
    weak var output: BookingSuccessInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - BookingSuccessInteractorInputProtocol
extension BookingSuccessInteractorInput: BookingSuccessInteractorInputProtocol {
    func getPaymentPeriodTime() {
        self.careService?.getPaymentPeriodTime(completion: { [weak self] result in
            self?.output?.onGetPaymentPeriodTimeFinished(with: result.unwrapSuccessModel())
        })
    }
}
