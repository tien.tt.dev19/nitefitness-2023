//
//  
//  BookingSuccessPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/25/21.
//
//

import UIKit

protocol BookingSuccessPresenterDelegate: AnyObject {
    func onDetailBookingAction(appointment: AppointmentModel?)
    func onPopRootViewAction()
}

class BookingSuccessPresenter {

    weak var view: BookingSuccessViewProtocol?
    private var interactor: BookingSuccessInteractorInputProtocol
    private var router: BookingSuccessRouterProtocol

    init(interactor: BookingSuccessInteractorInputProtocol,
         router: BookingSuccessRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    weak var delegate: BookingSuccessPresenterDelegate?
    
    var appointment: AppointmentModel?

}

// MARK: - BookingSuccessPresenterProtocol
extension BookingSuccessPresenter: BookingSuccessPresenterProtocol {
    func onViewDidLoad() {
        self.view?.setDataView(appointment: self.appointment)
        self.interactor.getPaymentPeriodTime()
    }
    
    func onDetailBookingAction() {
        self.delegate?.onDetailBookingAction(appointment: self.appointment)
    }
    
    func onCloseAction() {
        self.delegate?.onPopRootViewAction()
    }
    
    func onCopyBankNumberAction() {
        if let bankAccountNumber = self.appointment?.bank?.accountNumber {
            let pasteboard = UIPasteboard.general
            pasteboard.string = bankAccountNumber.replaceCharacter(target: " ", withString: "")
            SKToast.shared.showToast(content: "Đã sao chép số tài khoản: \(pasteboard.string ?? "") \n\nMở app NGÂN HÀNG và dán vào mục nhập số tài khoản để tiếp tục")
        }
    }
    
    func onCopyBankContentAction() {
        if let transferContent = self.appointment?.code {
            let pasteboard = UIPasteboard.general
            pasteboard.string = "1SK CARE \(transferContent)"
            SKToast.shared.showToast(content: "Đã sao chép nội dung chuyển khoản: \(pasteboard.string ?? "") \n\nMở app NGÂN HÀNG và dán vào mục nội dung chuyển khoản để tiếp tục")
        }
    }
    
    func getAppointment() -> AppointmentModel? {
        return self.appointment
    }
}

// MARK: - BookingSuccessInteractorOutput 
extension BookingSuccessPresenter: BookingSuccessInteractorOutputProtocol {
    func onGetPaymentPeriodTimeFinished(with result: Result<PaymentPeriodModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.setPaymentPeriodTime(paymentPeriod: model)
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
