//
//  
//  BookingSuccessRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/25/21.
//
//

import UIKit

class BookingSuccessRouter: BaseRouter {
//    weak var viewController: BookingSuccessViewController?
    static func setupModule(width appointment: AppointmentModel, delegate: BookingSuccessPresenterDelegate?) -> BookingSuccessViewController {
        let viewController = BookingSuccessViewController()
        let router = BookingSuccessRouter()
        let interactorInput = BookingSuccessInteractorInput()
        let presenter = BookingSuccessPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.appointment = appointment
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BookingSuccessRouterProtocol
extension BookingSuccessRouter: BookingSuccessRouterProtocol {
    
}
