//
//  
//  BookingSuccessViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/25/21.
//
//

import UIKit

class BookingSuccessViewController: BaseViewController {

    var presenter: BookingSuccessPresenterProtocol!
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Service: UILabel!
    @IBOutlet weak var lbl_Doctor: UILabel!
    @IBOutlet weak var lbl_Code: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_PriceTotal: UILabel!
    @IBOutlet weak var lbl_PaymentMethod: UILabel!
    
    @IBOutlet weak var lbl_BankNumber: UILabel!
    @IBOutlet weak var lbl_BankName: UILabel!
    @IBOutlet weak var lbl_BankBranch: UILabel!
    @IBOutlet weak var lbl_BankHolderName: UILabel!
    @IBOutlet weak var lbl_BankContent: UILabel!
    
    @IBOutlet weak var lbl_Note: UILabel!
    
    @IBOutlet weak var stack_PaymentMethod: UIStackView!
    @IBOutlet weak var constraint_height_ContentView: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setShowAnimation()
    }
    
    // MARK: - Setup
    private func setupInit() {
        if gUser?.id == gUserIdReviewAppStore {
            self.stack_PaymentMethod.isHidden = true
            self.constraint_height_ContentView.constant = -250
            self.lbl_Note.isHidden = true
        }
    }
    
    // MARK: - Action
    
    @IBAction func onCloseAction(_ sender: Any) {
        self.setHiddenAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.presenter.onCloseAction()
        }
    }
    
    @IBAction func onDetailBookingAction(_ sender: Any) {
        self.setHiddenAnimation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.presenter.onDetailBookingAction()
        }
    }
    
    @IBAction func onCopyBankNumberAction(_ sender: Any) {
        self.presenter.onCopyBankNumberAction()
    }
    
    @IBAction func onCopyBankContentAction(_ sender: Any) {
        self.presenter.onCopyBankContentAction()
    }
}

// MARK: - BookingSuccessViewProtocol
extension BookingSuccessViewController: BookingSuccessViewProtocol {
    func setDataView(appointment: AppointmentModel?) {
        let specialist = appointment?.doctor?.specialist?.first
        self.lbl_Service.text = "Tư vấn \(specialist?.name ?? "chuyên khoa") qua video"
        
        self.lbl_Doctor.text = appointment?.doctor?.basicInformation?.fullName
        self.lbl_Code.text = appointment?.code
        
        if let availableTime = appointment?.doctorAvailableTime {
            let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.date ?? 0))
            let day = date.toString(Date.Format.eeedmySlash)
            
            let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.startAt ?? 0))
            let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.endAt ?? 0))
            let timeText = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
            self.lbl_Time.text = String(format: "%@ %@", timeText, day)
        }
        
        
        self.lbl_Status.text = appointment?.status?.name
        
//        if let costPerHour = appointment?.doctor?.cost?.costPerHour, costPerHour > 0 {
//            self.lbl_PriceTotal.text = "\(costPerHour.formatNumber())đ"
//
//        } else {
//            self.lbl_PriceTotal.text = "0đ"
//            self.stack_PaymentMethod.isHidden = true
//            self.constraint_height_ContentView.constant = -210
//        }
        
        if let pricerTotal = appointment?.salePrice, pricerTotal > 0 {
            self.lbl_PriceTotal.text = "\(pricerTotal.formatNumber())đ"
            
        } else {
            self.lbl_PriceTotal.text = "0đ"
            self.stack_PaymentMethod.isHidden = true
            self.constraint_height_ContentView.constant = -210
        }
        
        self.lbl_PaymentMethod.text = "Chuyển khoản"
        
        self.lbl_BankNumber.text = appointment?.bank?.accountNumber
        self.lbl_BankName.text = appointment?.bank?.bankName ?? ""
        self.lbl_BankBranch.text = appointment?.bank?.bankBranchName ?? ""
        self.lbl_BankHolderName.text = appointment?.bank?.accountName ?? ""
        self.lbl_BankContent.text = "Nội dung: 1SK CARE \(appointment?.code ?? "")"
        
        if gUser?.id == gUserIdReviewAppStore {
            self.lbl_Status.text = "Đang xác nhận lịch hẹn"
        }
    }
    
    func setPaymentPeriodTime(paymentPeriod: PaymentPeriodModel) {
        let appointment = self.presenter.getAppointment()
        if let costPerHour = appointment?.salePrice, costPerHour > 0 {
            self.lbl_Note.text = "(*) Lưu ý: Sau \(paymentPeriod.appointmentPaymentPeriod ?? 0) phút từ khi đặt lịch, nếu bạn chưa thanh toán, lịch hẹn sẽ tự động hủy"
        } else {
            self.lbl_Note.text = "(*) Lưu ý: Sau khi xác nhận, chúng tôi sẽ gửi thông báo cho bạn"
            self.lbl_Note.textColor = R.color.subTitle()
        }
        
//        if let costPerHour = appointment?.doctor?.cost?.costPerHour, costPerHour > 0 {
//            self.lbl_Note.text = "(*) Lưu ý: Sau \(paymentPeriod.appointmentPaymentPeriod ?? 0) phút từ khi đặt lịch, nếu bạn chưa thanh toán, lịch hẹn sẽ tự động hủy"
//        } else {
//            self.lbl_Note.text = "(*) Lưu ý: Sau khi xác nhận, chúng tôi sẽ gửi thông báo cho bạn"
//            self.lbl_Note.textColor = R.color.subTitle()
//        }
    }
}

// MARK: - Animation
extension BookingSuccessViewController {
    func setShowAnimation() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setHiddenAnimation() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Bg.height
            self.view.layoutIfNeeded()
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}
