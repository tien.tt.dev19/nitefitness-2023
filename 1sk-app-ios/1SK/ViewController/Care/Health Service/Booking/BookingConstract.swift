//
//  
//  BookingConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//
//

import UIKit

// MARK: - View
protocol BookingViewProtocol: AnyObject {
    func setDataBookingView(bookingCare: BookingCare)
    func setStateEditInfo(_ isEdit: Bool)
    func setStateEnableButtonConfirm(_ isEnable: Bool)
    
    func setShowAlertConfirmVoucher()
}

// MARK: - Presenter
protocol BookingPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    // Action
    func onConfirmBookingAction()
    func onEditTimeAction()
    func onSaveInfoAction()
    
    func onAlertConfirmCancelAction()
    func onAlertConfirmAgreeAction()
    
    // Value
    func onNameChanged(with name: String)
    func onBirthDayChanged(with birthday: String)
    func onGenderChanged(with gender: Gender)
    func onPhoneChanged(with phone: String)
    func onResonChanged(with reson: String)
}

// MARK: - Interactor Input
protocol BookingInteractorInputProtocol {
    func setConfirmBooking(bookingCare: BookingCare)
    func setUpdateUserInfo(newUser: UserModel)
}

// MARK: - Interactor Output
protocol BookingInteractorOutputProtocol: AnyObject {
    func onSetConfirmBookingFinished(with result: Result<AppointmentModel, APIError>, meta: Meta?)
    func onUpdateUserInfoFinished(with result: Result<UserModel, APIError>)
}

// MARK: - Router
protocol BookingRouterProtocol: BaseRouterProtocol {
    func popRootViewController()
    func popViewController()
    func showBookingSuccess(appointment: AppointmentModel, delegate: BookingSuccessPresenterDelegate?)
    func gotoDetailAppointmentVC(appointment: AppointmentModel?)
    func showAlertDateTimeBookingView(listWeekday: [WeekdayModel], delegate: AlertCalendarCareViewDelegate?)
    func showAlertCalendarDoctorView(doctor: DoctorModel?, listWeekday: [WeekdayModel], delegate: AlertCalendarDoctorViewDelegate?)
}
