//
//  
//  BookingInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//
//

import UIKit

class BookingInteractorInput {
    weak var output: BookingInteractorOutputProtocol?
    var careService: CareServiceProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - BookingInteractorInputProtocol
extension BookingInteractorInput: BookingInteractorInputProtocol {
    func setConfirmBooking(bookingCare: BookingCare) {
        self.careService?.setConfirmBooking(bookingCare: bookingCare, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onSetConfirmBookingFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
    
    func setUpdateUserInfo(newUser: UserModel) {
        self.authService?.setUpdateProfile(newUser: newUser, image: nil, completion: { [weak self] result in
            self?.output?.onUpdateUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
}
