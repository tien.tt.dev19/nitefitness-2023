//
//  
//  BookingPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//
//

import UIKit

protocol BookingPresenterDelegate: AnyObject {
    func onVoucherExpiredBackAction()
}

class BookingPresenter {

    weak var view: BookingViewProtocol?
    private var interactor: BookingInteractorInputProtocol
    private var router: BookingRouterProtocol
    weak var delegate: BookingPresenterDelegate?
    
    init(interactor: BookingInteractorInputProtocol,
         router: BookingRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var doctor: DoctorModel?
    var listWeekday: [WeekdayModel] = []
    var bookingCare = BookingCare()
    var appointment: AppointmentModel?
    
    var weekdaySelect: Int?
    var startAtSelect: Int?
    var endAtSelect: Int?
    
    func setDataTimeView() {
        self.bookingCare.doctor = self.doctor
        self.bookingCare.priceFee = self.doctor?.cost?.costPerHour ?? 0
        self.bookingCare.priceVoucher = 0
        self.bookingCare.priceTotal = self.bookingCare.priceFee! + self.bookingCare.priceVoucher!
        self.bookingCare.voucher = self.doctor?.voucher
        
        for weekday in self.listWeekday {
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if slotTime.isSelected == true {
                        
                        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
                        let dayOfWeek = date.dayOfWeek() ?? "nil"
                        let day = date.toString(Date.Format.dmySlash)
                        
                        let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.startAt ?? 0))
                        let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.endAt ?? 0))
                        let timeText = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
                        
                        print("onAgreeAction slotTime.id:", slotTime.id ?? "nil")
                        print("onAgreeAction dayOfWeek:", dayOfWeek)
                        print("onAgreeAction day:", day)
                        print("onAgreeAction slotTime:", timeText)
                        print("onAgreeAction =======================\n")
                        
                        self.bookingCare.timeId = slotTime.id
                        self.bookingCare.date = day
                        self.bookingCare.time = timeText
                        
                        self.weekdaySelect = weekday.date
                        self.startAtSelect = slotTime.startAt
                        self.endAtSelect = slotTime.endAt
                    }
                }
            }
        }
    }
    
    func setDataInfoView() {
        self.bookingCare.name = gUser?.fullName
        self.bookingCare.birthday = gUser?.birthday
        self.bookingCare.phone = gUser?.phoneNumber
        self.bookingCare.gender = gUser?.gender
        
        if APP_ENV == .DEV {
            //self.bookingCare.name = "" //gUser?.fullName
            //self.bookingCare.birthday = "" //gUser?.birthday ?? "08/07/1992"
            //self.bookingCare.phone = "" //gUser?.phoneNumber ?? "0972297198"
            //self.bookingCare.gender = nil //gUser?.gender ?? .female
            self.bookingCare.reason = "Đau tim, tức ngực, khó thở"
        }
        
        self.checkStateValidationData()
    }
    
    func checkStateValidationData() {
        var isEdit = false
        if self.bookingCare.name?.count ?? 0 < 2 {
            isEdit = true
        }
        if self.bookingCare.birthday?.count ?? 0 < 10 {
            isEdit = true
        }
        if self.bookingCare.phone?.count ?? 0 < 10 {
            isEdit = true
        }
        if self.bookingCare.gender == nil {
            isEdit = true
        }
        
        self.view?.setStateEditInfo(isEdit)
        
        if self.bookingCare.reason?.count ?? 0 < 2 {
            isEdit = true
        }
        
        if isEdit == false {
            self.view?.setStateEnableButtonConfirm(true)
        } else {
            self.view?.setStateEnableButtonConfirm(false)
        }
    }
    
    func onConfirmBookingSuccess(appointment: AppointmentModel) {
        self.appointment = appointment
        self.router.showBookingSuccess(appointment: appointment, delegate: self)
    }
}

// MARK: - BookingPresenterProtocol
extension BookingPresenter: BookingPresenterProtocol {
    func onViewDidLoad() {
        self.setDataTimeView()
        self.setDataInfoView()
        self.view?.setDataBookingView(bookingCare: self.bookingCare)
    }
    
    func onViewDidAppear() {
        //
    }
    
    // Action
    func onConfirmBookingAction() {
        if self.bookingCare.name?.count ?? 0 < 2 {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Họ Tên của bạn")
            return
        }
        if self.bookingCare.birthday?.count ?? 0 < 10 {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Ngày sinh của bạn")
            return
        }
        if self.bookingCare.phone?.count ?? 0 < 10 {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Số điện thoại của bạn")
            return
        }
        if self.bookingCare.gender == nil {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Giới tính của bạn")
            return
        }
        if self.bookingCare.reason?.count ?? 0 < 2 {
            SKToast.shared.showToast(content: "Vui lòng nhập đủ lý do khám bệnh của bạn")
            return
        }
        
        self.router.showHud()
        self.interactor.setConfirmBooking(bookingCare: self.bookingCare)
    }
    
    func onEditTimeAction() {
        self.router.showAlertCalendarDoctorView(doctor: self.doctor, listWeekday: self.listWeekday, delegate: self)
        
//        self.router.showAlertDateTimeBookingView(listWeekday: self.listWeekday, delegate: self)
    }
    
    func onSaveInfoAction() {
        if self.bookingCare.name?.count ?? 0 < 2 {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Họ Tên của bạn")
            return
        }
        if self.bookingCare.birthday?.count ?? 0 < 10 {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Ngày sinh của bạn")
            return
        }
        if self.bookingCare.gender == nil {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Giới tính của bạn")
            return
        }
        if self.bookingCare.phone?.count ?? 0 < 10 {
            SKToast.shared.showToast(content: "Vui lòng nhập đúng Số điện thoại của bạn")
            return
        }
        
        let newUser = UserModel()
        newUser.fullName = self.bookingCare.name
        newUser.birthday = self.bookingCare.birthday
        newUser.phoneNumber = self.bookingCare.phone
        newUser.gender = self.bookingCare.gender
        
        self.router.showHud()
        self.interactor.setUpdateUserInfo(newUser: newUser)
    }
    
    func onAlertConfirmCancelAction() {
        self.delegate?.onVoucherExpiredBackAction()
        self.router.popViewController()
    }
    
    func onAlertConfirmAgreeAction() {
        self.doctor?.voucher = nil
        self.onViewDidLoad()
        
//        self.setDataTimeView()
//        self.setDataInfoView()
//        self.view?.setDataBookingView(bookingCare: self.bookingCare)
    }
    
    // Value
    func onNameChanged(with name: String) {
        self.bookingCare.name = name
    }
    
    func onBirthDayChanged(with birthday: String) {
        self.bookingCare.birthday = birthday
    }
    
    func onGenderChanged(with gender: Gender) {
        self.bookingCare.gender = gender
    }
    
    func onPhoneChanged(with phone: String) {
        self.bookingCare.phone = phone
    }
    
    func onResonChanged(with reason: String) {
        self.bookingCare.reason = reason
        
        var isEdit = false
        if self.bookingCare.reason?.count ?? 0 < 2 {
            isEdit = true
        }
        
        if isEdit == false {
            self.view?.setStateEnableButtonConfirm(true)
        } else {
            self.view?.setStateEnableButtonConfirm(false)
        }
    }
}

// MARK: - BookingInteractorOutput 
extension BookingPresenter: BookingInteractorOutputProtocol {
    func onSetConfirmBookingFinished(with result: Result<AppointmentModel, APIError>, meta: Meta?) {
        switch meta?.code {
        case 10006:
            self.view?.setShowAlertConfirmVoucher()
            
        default:
            switch result {
            case .success(let appointmentModel):
                self.onConfirmBookingSuccess(appointment: appointmentModel)
                
            case .failure:
                SKToast.shared.showToast(content: "Khung giờ này hiện đã có người đặt.\nVui lòng chọn khung giờ khác")
            }
        }
        self.router.hideHud()
    }
    
    func onUpdateUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let userModel):
            gUser = userModel
            self.view?.setStateEditInfo(false)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
}

// MARK: - BookingSuccessPresenterDelegate
extension BookingPresenter: BookingSuccessPresenterDelegate {
    func onDetailBookingAction(appointment: AppointmentModel?) {
        self.router.gotoDetailAppointmentVC(appointment: appointment)
    }
    
    func onPopRootViewAction() {
        self.router.popRootViewController()
    }
}

// MARK: - AlertDateTimeBookingViewDelegate
extension BookingPresenter: AlertCalendarCareViewDelegate {
    func onAlertCancelAction() {
        for weekday in self.listWeekday {
            if weekday.isState == .selected {
                weekday.isState = .enable
            }
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if weekday.date == self.weekdaySelect && slotTime.startAt == self.startAtSelect && slotTime.endAt == self.endAtSelect {
                        slotTime.isSelected = true
                        weekday.isState = .selected
                        
                    } else {
                        slotTime.isSelected = false
                    }
                }
            }
        }
    }
    
    func onAlertAgreeAction(doctor: DoctorModel?) {
        self.setDataTimeView()
        self.view?.setDataBookingView(bookingCare: self.bookingCare)
    }
}

// MARK: AlertCalendarDoctorViewDelegate
extension BookingPresenter: AlertCalendarDoctorViewDelegate {
    func onAlertCalendarDoctorViewCancelAction() {
        for weekday in self.listWeekday {
            if weekday.isState == .selected {
                weekday.isState = .enable
            }
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if weekday.date == self.weekdaySelect && slotTime.startAt == self.startAtSelect && slotTime.endAt == self.endAtSelect {
                        slotTime.isSelected = true
                        weekday.isState = .selected
                        
                    } else {
                        slotTime.isSelected = false
                    }
                }
            }
        }
    }
    
    func onAlertCalendarDoctorViewAgreeAction(doctor: DoctorModel?) {
        self.setDataTimeView()
        self.view?.setDataBookingView(bookingCare: self.bookingCare)
    }
}
