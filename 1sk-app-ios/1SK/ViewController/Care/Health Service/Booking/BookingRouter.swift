//
//  
//  BookingRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//
//

import UIKit

class BookingRouter: BaseRouter {
//    weak var viewController: BookingViewController?
    static func setupModule(width doctor: DoctorModel, listWeekday: [WeekdayModel], delegate: BookingPresenterDelegate?) -> BookingViewController {
        let viewController = BookingViewController()
        let router = BookingRouter()
        let interactorInput = BookingInteractorInput()
        let presenter = BookingPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.doctor = doctor
        presenter.listWeekday = listWeekday
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        interactorInput.authService = AuthService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BookingRouterProtocol
extension BookingRouter: BookingRouterProtocol {
    func popRootViewController() {
        self.viewController?.navigationController?.popToRootViewController(animated: true)
    }
    
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showBookingSuccess(appointment: AppointmentModel, delegate: BookingSuccessPresenterDelegate?) {
        let controller = BookingSuccessRouter.setupModule(width: appointment, delegate: delegate)
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: true)
    }
    
    func gotoDetailAppointmentVC(appointment: AppointmentModel?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: nil, isFromBooking: true)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showAlertDateTimeBookingView(listWeekday: [WeekdayModel], delegate: AlertCalendarCareViewDelegate?) {
        let controller = AlertCalendarCareViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.listWeekday = listWeekday
        controller.isStateEdit = true
        self.viewController?.navigationController?.present(controller, animated: true)
    }
    
    func showAlertCalendarDoctorView(doctor: DoctorModel?, listWeekday: [WeekdayModel], delegate: AlertCalendarDoctorViewDelegate?) {
        let controller = AlertCalendarDoctorViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.doctor = doctor
        controller.listWeekday = listWeekday
        self.viewController?.navigationController?.present(controller, animated: true)
    }
}
