//
//  
//  BookingViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//
//

import UIKit
import DLRadioButton

class BookingViewController: BaseViewController {

    var presenter: BookingPresenterProtocol!
    
    @IBOutlet weak var view_Scroll: UIScrollView!
    
    @IBOutlet weak var btn_EditTime: UIButton!
    @IBOutlet weak var btn_EditInfo: UIButton!
    
    @IBOutlet weak var stack_InfoView: UIStackView!
    @IBOutlet weak var stack_InfoEdit: UIStackView!
    
    @IBOutlet weak var lbl_Doctor: UILabel!
    
    @IBOutlet weak var lbl_TitleTime: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    
    @IBOutlet weak var lbl_TitleInfo: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Birthday: UILabel!
    @IBOutlet weak var lbl_Gender: UILabel!
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    @IBOutlet weak var tf_Name: SKTextField!
    @IBOutlet weak var tf_Birthday: SKTextField!
    @IBOutlet weak var tf_PhoneNumber: SKTextField!
    
    @IBOutlet weak var btn_Male: DLRadioButton!
    @IBOutlet weak var btn_Female: DLRadioButton!
    
    @IBOutlet weak var lbl_TitleReason: UILabel!
    @IBOutlet weak var tv_Reason: UITextView!
    
    @IBOutlet weak var lbl_PriceFee: UILabel!
    
    @IBOutlet weak var view_Voucher: UIView!
    @IBOutlet weak var lbl_PriceVoucher: UILabel!
    
    @IBOutlet weak var lbl_PriceTotal: UILabel!
    
    @IBOutlet weak var view_Footer: UIView!
    
    @IBOutlet weak var btn_ConfirmBooking: UIButton!
    
    @IBOutlet weak var constraint_bottom_ViewScroll: NSLayoutConstraint!
    
    private var sk_tf_Name: UITextField {
        return self.tf_Name.titleTextField
    }

    private var sk_tf_Birthday: UITextField {
        return self.tf_Birthday.titleTextField
    }

    private var sk_tf_Phone: UITextField {
        return self.tf_PhoneNumber.titleTextField
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitNavigationBar()
        self.setInitUITextField()
        self.setInitUITextView()
        self.setInitRadioButtonGender()
        self.setHandleKeyboardEvent()
    }
    
    func setInitNavigationBar() {
        self.title = "Tư vấn bác sĩ qua video"
        self.view_Footer.addShadow(width: 0, height: -4, color: .black, radius: 4, opacity: 0.1)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Action
    @IBAction func onStepDoctorAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onEditTimeAction(_ sender: Any) {
        self.presenter.onEditTimeAction()
    }
    
    @IBAction func onEditInfoAction(_ sender: Any) {
        self.setStateEditInfo(true)
    }
    
    @IBAction func onSaveInfoAction(_ sender: Any) {
        self.view.endEditing(true)
        self.presenter.onSaveInfoAction()
    }
    
    @IBAction func onConfirmBookingAction(_ sender: Any) {
        self.presenter.onConfirmBookingAction()
        
//        self.showAlertConfirmActionView()
    }
}

// MARK: - BookingViewProtocol
extension BookingViewController: BookingViewProtocol {
    func setDataBookingView(bookingCare: BookingCare) {
        let doctor = bookingCare.doctor
        let atrDescInfo = NSMutableAttributedString(string: "Vui lòng điền các thông tin dưới đây để hoàn tất đặt lịch tư vấn với bác sĩ ", attributes: [.foregroundColor: R.color.color_Gray_Text()!])
        atrDescInfo.append(NSAttributedString(string: doctor?.basicInformation?.fullName ?? "", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)]))
        atrDescInfo.append(NSAttributedString(string: " chuyên khoa ", attributes: [.foregroundColor: R.color.color_Gray_Text()!]))
        atrDescInfo.append(NSAttributedString(string: doctor?.specialist?.first?.name ?? "", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_Doctor.attributedText = atrDescInfo
        
        let atrTitleTime = NSMutableAttributedString(string: "Thời gian tư vấn ", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)])
        atrTitleTime.append(NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_TitleTime.attributedText = atrTitleTime
        
        let atrTitleInfo = NSMutableAttributedString(string: "Thông tin bệnh nhân ", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)])
        atrTitleInfo.append(NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_TitleInfo.attributedText = atrTitleInfo
        
        let atrTitleReason = NSMutableAttributedString(string: "Lý do khám ", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)])
        atrTitleReason.append(NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_TitleReason.attributedText = atrTitleReason
        
        self.lbl_Date.text = bookingCare.date
        self.lbl_Time.text = bookingCare.time
        
        self.lbl_Name.text = bookingCare.name
        self.lbl_Birthday.text = bookingCare.birthday
        self.lbl_Gender.text = bookingCare.gender?.valueVi
        self.lbl_PhoneNumber.text = bookingCare.phone
        
        self.tv_Reason.text = bookingCare.reason
        
        if let priceTotal = bookingCare.priceTotal, priceTotal > 0 {
            self.lbl_PriceTotal.text = "\(priceTotal.formatNumber())đ"
        } else {
            self.lbl_PriceTotal.text = "0đ"
        }
        
        if let voucher = bookingCare.voucher {
            if let priceFee = bookingCare.priceFee, priceFee > 0 {
                self.lbl_PriceFee.text = "\(priceFee.formatNumber())đ"
                
                if voucher.isFixed == true {
                    self.lbl_PriceFee.text = "\(priceFee.formatNumber())đ"
                    
                    let priceVoucher = Int(voucher.discountAmount ?? 0)
                    self.lbl_PriceVoucher.text = "\(priceVoucher.formatNumber())đ"
                    
                    let priceTotal = priceFee - Int(voucher.discountAmount ?? 0)
                    if priceTotal > 0 {
                        self.lbl_PriceTotal.text = "\(priceTotal.formatNumber())đ"
                    } else {
                        self.lbl_PriceTotal.text = "0đ"
                    }
                    
                } else {
                    self.lbl_PriceVoucher.text = "\(voucher.discountAmount ?? 0)%"
                    
                    let priceVoucher = (priceFee / 100) * Int(voucher.discountAmount ?? 0)
                    let priceTotal = priceFee - priceVoucher
                    self.lbl_PriceTotal.text = "\(priceTotal.formatNumber())đ"
                }
                
            } else {
                self.lbl_PriceFee.text = "Miễn phí"
                
                if voucher.isFixed == true {
                    let priceVoucher = Int(voucher.discountAmount ?? 0)
                    self.lbl_PriceVoucher.text = "\(priceVoucher.formatNumber())đ"
                    
                } else {
                    self.lbl_PriceVoucher.text = "\(voucher.discountAmount ?? 0)%"
                }
            }
            
            self.view_Voucher.isHidden = false
            
        } else {
            if let priceFee = bookingCare.priceFee, priceFee > 0 {
                self.lbl_PriceFee.text = "\(priceFee.formatNumber())đ"
                
            } else {
                self.lbl_PriceFee.text = "Miễn phí"
                self.lbl_PriceVoucher.text = "0đ"
            }
            
            self.lbl_PriceVoucher.text = "0đ"
            self.view_Voucher.isHidden = true
        }
        
        
//        if let priceFee = bookingCare.priceFee, priceFee > 0 {
//            self.lbl_PriceFee.text = "\(priceFee.formatNumber())đ"
//
//            if let voucher = bookingCare.voucher {
//                if voucher.isFixed == true {
//                    let priceVoucher = Int(voucher.discountAmount ?? 0)
//                    self.lbl_PriceVoucher.text = "\(priceVoucher.formatNumber())đ"
//
//                    let priceTotal = priceFee - Int(voucher.discountAmount ?? 0)
//                    if priceTotal > 0 {
//                        self.lbl_PriceTotal.text = "\(priceTotal.formatNumber())đ"
//                    } else {
//                        self.lbl_PriceTotal.text = "0đ"
//                    }
//
//                } else {
//                    self.lbl_PriceVoucher.text = "\(voucher.discountAmount ?? 0)%"
//
//                    let priceVoucher = (priceFee / 100) * Int(voucher.discountAmount ?? 0)
//                    let priceTotal = priceFee - priceVoucher
//                    self.lbl_PriceTotal.text = "\(priceTotal.formatNumber())đ"
//                }
//
//                self.view_Voucher.isHidden = false
//
//            } else {
//                self.lbl_PriceVoucher.text = "0đ"
//                self.view_Voucher.isHidden = true
//            }
//
//        } else {
//            self.lbl_PriceFee.text = "Miễn phí"
//            self.lbl_PriceVoucher.text = "0đ"
//            self.view_Voucher.isHidden = true
//        }
        
        
        self.sk_tf_Name.text = bookingCare.name
        self.sk_tf_Birthday.text = bookingCare.birthday
        self.sk_tf_Phone.text = bookingCare.phone

        switch bookingCare.gender {
        case .male:
            self.btn_Male.isSelected = true
            self.btn_Female.isSelected = false
            
        case .female:
            self.btn_Male.isSelected = false
            self.btn_Female.isSelected = true
            
        default:
            break
        }
        
        for textField in [self.sk_tf_Name, self.sk_tf_Birthday, self.sk_tf_Phone] {
            self.updateTypeLabelHiddenState(of: textField)
        }
        
        if gUser?.id == gUserIdReviewAppStore {
            self.lbl_PriceFee.text = "Miễn phí"
            self.lbl_PriceTotal.text = "0đ"
        }
    }
    
    func setStateEditInfo(_ isEdit: Bool) {
        if isEdit {
            self.stack_InfoEdit.alpha = 0.0
            UIView.animate(withDuration: 0.5) {
                self.stack_InfoView.isHidden = true
                
            } completion: { _ in
                UIView.animate(withDuration: 0.5) {
                    self.stack_InfoEdit.isHidden = false
                    self.btn_EditInfo.isHidden = true
                    self.stack_InfoEdit.alpha = 1.0
                }
            }
        } else {
            
            UIView.animate(withDuration: 0.5) {
                self.stack_InfoEdit.isHidden = true
                self.btn_EditInfo.isHidden = false
                self.stack_InfoEdit.alpha = 0.0
                
            } completion: { _ in
                UIView.animate(withDuration: 0.5) {
                    self.stack_InfoView.isHidden = false
                }
            }
        }
        
    }
    
    func setStateEnableButtonConfirm(_ isEnable: Bool) {
        if isEnable == true {
            self.btn_ConfirmBooking.backgroundColor = R.color.mainColor()
        } else {
            self.btn_ConfirmBooking.backgroundColor = .lightGray
        }
    }
    
    func setShowAlertConfirmVoucher() {
        self.showAlertConfirmActionView()
    }
}

// MARK: - UITextFieldDelegate
extension BookingViewController: UITextFieldDelegate {
    private func setInitUITextField() {
        self.sk_tf_Name.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Birthday.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Phone.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        
        self.tf_Name.setContentType(.name)
        self.tf_Name.setKeyboardType(.alphabet)
        self.tf_Birthday.setKeyboardType(.default)
        self.tf_PhoneNumber.setContentType(.telephoneNumber)
        self.tf_PhoneNumber.setKeyboardType(.phonePad)
        
        self.sk_tf_Name.delegate = self
        self.sk_tf_Birthday.delegate = self
        self.sk_tf_Phone.delegate = self
        
        self.sk_tf_Name.returnKeyType = .done
        self.sk_tf_Phone.returnKeyType = .done

        self.tf_Name.setEnable(isEnable: true)
        self.tf_Name.updateTypeLabelHiddenState()
        
        self.tf_Birthday.setEnable(isEnable: true)
        self.tf_Birthday.updateTypeLabelHiddenState()
        
        self.tf_PhoneNumber.setEnable(isEnable: true)
        self.tf_PhoneNumber.updateTypeLabelHiddenState()
    }

    @objc func onTextFieldDidChange(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }
    
    private func updateTypeLabelHiddenState(of textField: UITextField) {
        switch textField {
        case self.sk_tf_Name:
            self.tf_Name.updateTypeLabelHiddenState()

        case self.sk_tf_Birthday:
            self.tf_Birthday.updateTypeLabelHiddenState()

        case self.sk_tf_Phone:
            self.tf_PhoneNumber.updateTypeLabelHiddenState()

        default:
            break
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.sk_tf_Birthday {
            self.showAlertCalendarDateViewAction()
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
        
        switch textField {
        case self.sk_tf_Name:
            self.lbl_Name.text = self.sk_tf_Name.text ?? ""
            self.presenter.onNameChanged(with: self.sk_tf_Name.text ?? "")

        case self.sk_tf_Phone:
            self.lbl_PhoneNumber.text = self.sk_tf_Phone.text ?? ""
            self.presenter.onPhoneChanged(with: self.sk_tf_Phone.text ?? "")

        default:
            break
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
}

// MARK: -  Radio Button
extension BookingViewController {
    private func setInitRadioButtonGender() {
        self.btn_Male.addTarget(self, action: #selector(self.onMaleAction), for: .touchUpInside)
        self.btn_Female.addTarget(self, action: #selector(self.onFemaleAction), for: .touchUpInside)
    }

    @objc private func onMaleAction() {
        self.btn_Male.isSelected = true
        self.btn_Female.isSelected = false
        
        self.lbl_Gender.text = Gender.female.valueVi
        self.presenter.onGenderChanged(with: .male)
    }

    @objc private func onFemaleAction() {
        self.btn_Male.isSelected = false
        self.btn_Female.isSelected = true
        
        self.lbl_Gender.text = Gender.female.valueVi
        self.presenter.onGenderChanged(with: .female)
    }
}

// MARK: -  UITextViewDelegate
extension BookingViewController: UITextViewDelegate {
    func setInitUITextView() {
        self.tv_Reason.delegate = self
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.presenter.onResonChanged(with: textView.text ?? "")
    }
}

// MARK: onHandleKeyboardEvent
extension BookingViewController {
    func setHandleKeyboardEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.view_Scroll.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBgAction)))
    }
    
    @objc func onTouchBgAction() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 1.0) {
                self.constraint_bottom_ViewScroll.constant = keyboardSize.height - 83.5
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 1.0) {
            self.constraint_bottom_ViewScroll.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - AlertCalendarDateViewDelegate
extension BookingViewController: AlertDatePickerViewDelegate {
    func showAlertCalendarDateViewAction() {
        self.view.endEditing(true)
        
        let controller = AlertDatePickerViewController()
        controller.modalPresentationStyle = .custom
        controller.alertTitle = "Chọn ngày sinh"
        controller.dateSelect = self.sk_tf_Birthday.text ?? ""
        controller.delegate = self
        
        self.navigationController?.present(controller, animated: true)
    }
    
    func onAlertDatePickerCancelAction() {
        //
    }
    
    func onAlertDatePickerAgreeAction(day: String) {
        self.sk_tf_Birthday.text = day
        self.lbl_Birthday.text = day
        self.tf_Birthday.updateTypeLabelHiddenState()
        self.presenter.onBirthDayChanged(with: day)
    }
}

// MARK: - AlertConfirmActionViewDelegate
extension BookingViewController: AlertSheetConfirmIconViewDelegate {
    func showAlertConfirmActionView() {
        let controller = AlertSheetConfirmIconViewController()
        controller.alertContent = "Mã voucher này đã hết hiệu lực"
        controller.alertImage = R.image.ic_calendar_cancel()
        controller.btnTitleCancel = "Quay lại"
        controller.btnTitleAgree = "Tiếp tục đặt lịch"
        
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        self.navigationController?.present(controller, animated: true)
    }
    
    func onAlertConfirmCancelAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.presenter.onAlertConfirmCancelAction()
        }
    }
    
    func onAlertConfirmAgreeAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
            self.presenter.onAlertConfirmAgreeAction()
        }
    }
}
