//
//  CellTableViewHealthService.swift
//  1SK
//
//  Created by vuongbachthu on 9/24/21.
//

import UIKit

protocol CellTableViewHealthServiceDelegate: AnyObject {
    func onCellAvatarAction(_ cell: UITableViewCell)
    func onCellContentAction(_ cell: UITableViewCell)
}

class CellTableViewHealthService: UITableViewCell {

    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var btn_DetailView: UIButton!
    @IBOutlet weak var stack_Content: UIStackView!
    
    @IBOutlet weak var lbl_Doctor: UILabel!
    @IBOutlet weak var lbl_Specialist: UILabel!
    @IBOutlet weak var lbl_WorkPlace: UILabel!
    @IBOutlet weak var btn_ConsultantFee: UIButton!
    
    weak var delegate: CellTableViewHealthServiceDelegate?
    var doctor: DoctorModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitGestureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onDetailViewAction(_ sender: Any) {
        self.delegate?.onCellAvatarAction(self)
    }
    
    
    func config(doctor: DoctorModel) {
        self.doctor = doctor
        
        self.lbl_Doctor.text = "\(doctor.academicTitle?.shortName ?? ""). \(doctor.basicInformation?.fullName ?? "")"
        
        if let specialist = doctor.specialist?.first {
            let atrSpecialist = NSMutableAttributedString(string: "Chuyên khoa: ", attributes: [.foregroundColor: R.color.color_Gray_Text()!])
            atrSpecialist.append(NSAttributedString(string: specialist.name ?? "", attributes: [.foregroundColor: R.color.color_Dark_Text()!]))
            self.lbl_Specialist.attributedText = atrSpecialist
        } else {
            self.lbl_Specialist.text = "Chuyên khoa: "
        }
        
        
        let officeAddress = doctor.advanceInformation?.officeAddress
        let atrWorkPlace = NSMutableAttributedString(string: "Nơi công tác: ", attributes: [.foregroundColor: R.color.color_Gray_Text()!])
        atrWorkPlace.append(NSAttributedString(string: officeAddress ?? "", attributes: [.foregroundColor: R.color.color_Dark_Text()!]))
        self.lbl_WorkPlace.attributedText = atrWorkPlace
        
        if let fee = doctor.cost?.costPerHour, fee > 0 {
            self.btn_ConsultantFee.setTitle("\(fee.formatNumber())đ", for: .normal)
        } else {
            self.btn_ConsultantFee.setTitle("Miễn phí", for: .normal)
        }
        
        self.img_Avatar.setImageWith(imageUrl: doctor.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
        
        if gUser?.id == gUserIdReviewAppStore {
            self.btn_ConsultantFee.setTitle("Miễn phí", for: .normal)
        }
    }
}

// MARK: - UITapGestureRecognizer
extension CellTableViewHealthService {
    func setInitGestureView() {
        self.img_Avatar.isUserInteractionEnabled = true
        self.img_Avatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onAvatarAction)))
        self.stack_Content.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onContentAction)))
    }
    
    @objc func onAvatarAction() {
        self.delegate?.onCellAvatarAction(self)
    }
    
    @objc func onContentAction() {
        self.delegate?.onCellContentAction(self)
    }
}
