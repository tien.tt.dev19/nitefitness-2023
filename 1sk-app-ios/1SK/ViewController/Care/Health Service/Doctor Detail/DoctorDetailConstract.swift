//
//  
//  DoctorDetailConstract.swift
//  1SK
//
//  Created by Thaad on 29/12/2021.
//
//

import UIKit

// MARK: - View
protocol DoctorDetailViewProtocol: AnyObject {
    func onSetDataView(doctor: DoctorModel?)
    
    func onVoucherLayout(voucher: VoucherModel?)
    func getVoucherCode() -> String?
    func onVoucherExpiredBackAction()
}

// MARK: - Presenter
protocol DoctorDetailPresenterProtocol {
    func onViewDidLoad()
    
    func onBackAction()
    func onShareAction(buttonShare: UIButton?)
    func onConfirmBookingAction()
    
    func onCheckVoucher(code: String)
    func onTextFieldShouldClear()
}

// MARK: - Interactor Input
protocol DoctorDetailInteractorInputProtocol {
    func getDoctorTime(doctor: DoctorModel?, startDate: String, endDate: String)
    func getVoucherDetailDoctor(doctorId: Int, voucherCode: String)
}

// MARK: - Interactor Output
protocol DoctorDetailInteractorOutputProtocol: AnyObject {
    func onGetDoctorTimeFinished(with result: Result<[WeekdayModel], APIError>, doctor: DoctorModel?)
    func onGetVoucherDetailFinished(with result: Result<VoucherModel, APIError>)
}

// MARK: - Router
protocol DoctorDetailRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
    func showAlertCalendarDoctorView(doctor: DoctorModel?, listWeekday: [WeekdayModel], delegate: AlertCalendarDoctorViewDelegate?)
    func gotoBookingViewController(doctor: DoctorModel, listWeekday: [WeekdayModel], delegate: BookingPresenterDelegate?)
}
