//
//  
//  DoctorDetailInteractorInput.swift
//  1SK
//
//  Created by Thaad on 29/12/2021.
//
//

import UIKit

class DoctorDetailInteractorInput {
    weak var output: DoctorDetailInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - DoctorDetailInteractorInputProtocol
extension DoctorDetailInteractorInput: DoctorDetailInteractorInputProtocol {
    func getDoctorTime(doctor: DoctorModel?, startDate: String, endDate: String) {
        guard let doctorId = doctor?.id else {
            return
        }
        self.careService?.getDoctorTime(doctorId: doctorId, startDate: startDate, endDate: endDate, completion: { [weak self] result in
            self?.output?.onGetDoctorTimeFinished(with: result.unwrapSuccessModel(), doctor: doctor)
        })
    }
    
    func getVoucherDetailDoctor(doctorId: Int, voucherCode: String) {
        self.careService?.getVoucherDetailDoctor(doctorId: doctorId, voucherCode: voucherCode, completion: { [weak self] result in
            self?.output?.onGetVoucherDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
