//
//  
//  DoctorDetailPresenter.swift
//  1SK
//
//  Created by Thaad on 29/12/2021.
//
//

import UIKit

protocol DoctorDetailPresenterDelegate: AnyObject {
    func onDetailConfirmBookingAction(doctor: DoctorModel?)
}

class DoctorDetailPresenter {

    weak var view: DoctorDetailViewProtocol?
    private var interactor: DoctorDetailInteractorInputProtocol
    private var router: DoctorDetailRouterProtocol
    weak var delegate: DoctorDetailPresenterDelegate?
    
    init(interactor: DoctorDetailInteractorInputProtocol,
         router: DoctorDetailRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var doctor: DoctorModel?
    var isFrom: String?
    
    var listWeekdayRoot: [WeekdayModel] = []
    var voucher: VoucherModel?
    
    private let startTime = Date().toString(Date.Format.ymdSlash)
    private let endTime = Date().futureWeekDay.toString(Date.Format.ymdSlash)
    
    func onGetDoctorTimeSuccess(listWeekday: [WeekdayModel], doctor: DoctorModel?) {
//        guard listWeekday.count > 0 else {
//            SKToast.shared.showToast(content: "Bác sĩ này hiện tại đang bận \nVui lòng chọn bác sĩ khác")
//            return
//        }
        
        self.listWeekdayRoot = listWeekday
        self.listWeekdayRoot.first?.isExpand = true
        self.router.showAlertCalendarDoctorView(doctor: doctor, listWeekday: self.listWeekdayRoot, delegate: self)
    }
    
    func onGetVoucherDetailSuccess(voucher: VoucherModel?) {
        self.voucher = voucher
        self.view?.onVoucherLayout(voucher: self.voucher)
        self.router.hideHud()
    }
    
    func onTextFieldShouldClear() {
        self.voucher = nil
    }
}

// MARK: - DoctorDetailPresenterProtocol
extension DoctorDetailPresenter: DoctorDetailPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onSetDataView(doctor: self.doctor)
    }
    
    func onBackAction() {
        self.router.popViewController()
    }
    
    func onShareAction(buttonShare: UIButton?) {
        guard let doctorId = self.doctor?.id else {
            SKToast.shared.showToast(content: "Không thể chia sẻ Bác sĩ này")
            return
        }
        
        self.router.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.doctor?.basicInformation?.fullName
        meta.socialMetaDescText = self.doctor?.basicInformation?.address
        meta.socialMetaImageURL = self.doctor?.basicInformation?.avatar
        
        let universalLink = "\(SHARE_URL)\(doctorId)\(ShareType.doctor.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                self.router.hideHud()
                
                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }
                
                guard let shareView = buttonShare else {
                    return
                }
                self.router.showShareViewController(with: [urlShare], sourceView: shareView)
                
                print("createLinkShareFitnessPost The short URL is: \(urlShare)")
            })
            
        } else {
            self.router.hideHud()
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }
    
    func onConfirmBookingAction() {
        self.router.showHud()
        self.interactor.getDoctorTime(doctor: doctor, startDate: self.startTime, endDate: self.endTime)
        
//        switch self.isFrom {
//        case "DoctorDetailViewController":
//            self.delegate?.onDetailConfirmBookingAction(doctor: self.doctor)
//            self.router.popViewController()
//
//        case "HomeAppLandingViewController":
//            self.delegate?.onDetailConfirmBookingAction(doctor: self.doctor)
//            self.router.popViewController()
//
//        case "CareViewController":
//            self.delegate?.onDetailConfirmBookingAction(doctor: self.doctor)
//            self.router.popViewController()
//
//        default:
//            break
//        }
    }
    
    func onCheckVoucher(code: String) {
        guard let doctorId = self.doctor?.id else {
            return
        }
        self.router.showHud()
        self.interactor.getVoucherDetailDoctor(doctorId: doctorId, voucherCode: code)
    }
}

// MARK: - DoctorDetailInteractorOutput 
extension DoctorDetailPresenter: DoctorDetailInteractorOutputProtocol {
    func onGetDoctorTimeFinished(with result: Result<[WeekdayModel], APIError>, doctor: DoctorModel?) {
        switch result {
        case .success(let listModel):
            self.onGetDoctorTimeSuccess(listWeekday: listModel, doctor: doctor)
            
        case .failure(let error):
            debugPrint(error)
            self.onGetDoctorTimeSuccess(listWeekday: [], doctor: doctor)
        }
        self.router.hideHud()
    }
    
    func onGetVoucherDetailFinished(with result: Result<VoucherModel, APIError>) {
        switch result {
        case .success(let model):
            self.onGetVoucherDetailSuccess(voucher: model)

        case .failure:
            self.router.hideHud()

            self.voucher = nil
            self.view?.onVoucherLayout(voucher: nil)
        }
    }
}

// MARK: AlertCalendarDoctorViewDelegate
extension DoctorDetailPresenter: AlertCalendarDoctorViewDelegate {
    func onAlertCalendarDoctorViewCancelAction() {
        //
    }
    
    func onAlertCalendarDoctorViewAgreeAction(doctor: DoctorModel?) {
        for weekday in self.listWeekdayRoot {
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if slotTime.isSelected == true {
                        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
                        print("onAgreeAction dayOfWeek:", date.dayOfWeek() ?? "nil")
                        print("onAgreeAction day:", date.toString(Date.Format.dmySlash))
                        
                        let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.startAt ?? 0))
                        let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.endAt ?? 0))
                        let text = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
                        print("onAgreeAction slotTime:", text)
                        
                        print("onAgreeAction =======================\n")
                    }
                }
            }
        }
        
        guard let mDoctor = doctor else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            mDoctor.voucher = self.voucher
            self.router.gotoBookingViewController(doctor: mDoctor, listWeekday: self.listWeekdayRoot, delegate: self)
        }
    }
}

// MARK: - BookingPresenterDelegate
extension DoctorDetailPresenter: BookingPresenterDelegate {
    func onVoucherExpiredBackAction() {
//        self.view?.onVoucherExpiredBackAction()
//        self.getData(in: 1, specialistId: 0)
    }
}
