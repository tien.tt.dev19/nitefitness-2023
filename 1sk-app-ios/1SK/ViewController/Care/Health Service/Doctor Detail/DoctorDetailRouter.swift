//
//  
//  DoctorDetailRouter.swift
//  1SK
//
//  Created by Thaad on 29/12/2021.
//
//

import UIKit

class DoctorDetailRouter: BaseRouter {
    static func setupModule(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?) -> DoctorDetailViewController {
        let viewController = DoctorDetailViewController()
        let router = DoctorDetailRouter()
        let interactorInput = DoctorDetailInteractorInput()
        let presenter = DoctorDetailPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.doctor = doctor
        presenter.isFrom = isFrom
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - DoctorDetailRouterProtocol
extension DoctorDetailRouter: DoctorDetailRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        let controller = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        controller.popoverPresentationController?.sourceView = sourceView
        controller.popoverPresentationController?.sourceRect = sourceView.bounds
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func showAlertCalendarDoctorView(doctor: DoctorModel?, listWeekday: [WeekdayModel], delegate: AlertCalendarDoctorViewDelegate?) {
        let controller = AlertCalendarDoctorViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.doctor = doctor
        controller.listWeekday = listWeekday
        self.viewController?.navigationController?.present(controller, animated: true)
    }
    
    func gotoBookingViewController(doctor: DoctorModel, listWeekday: [WeekdayModel], delegate: BookingPresenterDelegate?) {
        let controller = BookingRouter.setupModule(width: doctor, listWeekday: listWeekday, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
}
