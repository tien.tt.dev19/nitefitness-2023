//
//  
//  DoctorDetailViewController.swift
//  1SK
//
//  Created by Thaad on 29/12/2021.
//
//

import UIKit

class DoctorDetailViewController: BaseViewController {

    var presenter: DoctorDetailPresenterProtocol!
    
    @IBOutlet weak var btn_Share: UIButton!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_Doctor: UILabel!
    
    @IBOutlet weak var lbl_Specialist: UILabel!
    @IBOutlet weak var lbl_WorkPlace: UILabel!
    @IBOutlet weak var lbl_Position: UILabel!
    @IBOutlet weak var btn_ConsultantFee: UIButton!
    
    @IBOutlet weak var tf_Voucher: UITextField!
    @IBOutlet weak var lbl_VoucherWarning: UILabel!
    @IBOutlet weak var btn_VoucherStatus: UIButton!
    
    @IBOutlet weak var constraint_bottom_ViewScroll: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitUITextField()
        self.setInitGestureView()
        self.setInitHandleKeyboard()
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.presenter.onBackAction()
    }
    
    @IBAction func onShareAction(_ sender: Any) {
        self.presenter.onShareAction(buttonShare: self.btn_Share)
    }
    
    @IBAction func onCheckVoucherAction(_ sender: Any) {
        self.setCheckVoucher()
    }
    
    @IBAction func onConfirmBookingAction(_ sender: Any) {
        self.presenter.onConfirmBookingAction()
    }
}

// MARK: - DoctorDetailViewProtocol
extension DoctorDetailViewController: DoctorDetailViewProtocol {
    func onSetDataView(doctor: DoctorModel?) {
        self.lbl_Doctor.text = "\(doctor?.academicTitle?.shortName ?? ""). \(doctor?.basicInformation?.fullName ?? "")"
        self.lbl_Specialist.text = doctor?.specialist?.first?.name
        self.lbl_WorkPlace.text = doctor?.advanceInformation?.officeAddress
        self.lbl_Position.text = doctor?.academicTitle?.name
        
        self.img_Avatar.setImageWith(imageUrl: doctor?.basicInformation?.avatar ?? "", placeHolder: R.image.default_avatar_2())
        
        if let fee = doctor?.cost?.costPerHour, fee > 0 {
            self.btn_ConsultantFee.setTitle("\(fee.formatNumber())đ", for: .normal)
        } else {
            self.btn_ConsultantFee.setTitle("Miễn phí", for: .normal)
        }
        if gUser?.id == gUserIdReviewAppStore {
            self.btn_ConsultantFee.setTitle("Miễn phí", for: .normal)
        }
    }
    
    func onVoucherLayout(voucher: VoucherModel?) {
        guard let `voucher` = voucher else {
            self.btn_VoucherStatus.isSelected = false
            self.lbl_VoucherWarning.text = "Mã không hợp lệ"
            self.lbl_VoucherWarning.textColor = R.color.color_Text_Red_1()
            return
        }
        self.btn_VoucherStatus.isSelected = true
        if voucher.isFixed == true {
            let priceVoucher = voucher.discountAmount ?? 0
            self.lbl_VoucherWarning.text = "Mã Voucher trị giá \(priceVoucher.formatNumber())đ"
        } else {
            self.lbl_VoucherWarning.text = "Mã Voucher trị giá \(voucher.discountAmount ?? 0)%"
        }
        
        self.lbl_VoucherWarning.textColor = R.color.mainColor()
    }
    
    func getVoucherCode() -> String? {
        return self.tf_Voucher.text
    }
    
    func onVoucherExpiredBackAction() {
        self.btn_VoucherStatus.isSelected = false
        self.lbl_VoucherWarning.text = ""
        self.tf_Voucher.text = ""
    }
}

// MARK: - UITextFieldDelegate
extension DoctorDetailViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Voucher.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.btn_VoucherStatus.isSelected = false
        self.lbl_VoucherWarning.text = ""
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count ?? 0 <= 0 {
            self.presenter.onTextFieldShouldClear()
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        self.tf_Voucher.text = self.tf_Voucher.text?.uppercased() ?? ""
        
        if textField.text?.count ?? 0 == 6 {
            self.setCheckVoucher()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        let count = textField.text?.count ?? 0
        if count < 6 {
            return true
            
        } else {
            return false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.setCheckVoucher()
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        switch textField {
        case self.tf_Voucher:
            self.tf_Voucher.text = ""
        default:
            break
        }
        return true
    }
    
    func setCheckVoucher() {
        guard let code = self.tf_Voucher.text else {
            self.lbl_VoucherWarning.text = "Vui lòng nhập mã Voucher"
            self.lbl_VoucherWarning.textColor = R.color.color_Text_Red_1()
            return
        }
        guard code.count > 0 else {
            return
        }
        self.view.endEditing(true)
        self.presenter.onCheckVoucher(code: code)
    }
}

// MARK: - Gesture View
extension DoctorDetailViewController {
    func setInitGestureView() {
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureBgAction)))
    }
    
    @objc func onTapGestureBgAction() {
        self.view.endEditing(true)
    }
}

// MARK: onHandleKeyboardEvent
extension DoctorDetailViewController {
    func setInitHandleKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func onTouchBgAction() {
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 1.0) {
                self.constraint_bottom_ViewScroll.constant = keyboardSize.height - 56
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 1.0) {
            self.constraint_bottom_ViewScroll.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
