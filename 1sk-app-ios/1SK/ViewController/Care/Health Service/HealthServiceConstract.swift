//
//  
//  HealthServiceConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

// MARK: - View
protocol HealthServiceViewProtocol: AnyObject {
    func onReloadData()
    func setCountList(count: Int, specialistModel: SpecialistModel?)
    
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func onSetNotFoundView(isHidden: Bool)
    
    func onVoucherLayout(voucher: VoucherModel?)
    func onSelectSpecialistAction(specialist: SpecialistModel)
    
    func getVoucherCode() -> String?
    
    func onVoucherExpiredBackAction()
}

// MARK: - Presenter
protocol HealthServicePresenterProtocol {
    func onViewDidLoad()
    
    // Action
    func onBookingAction()
    func onSelectSpecialistAction(specialist: SpecialistModel)
    func onCheckVoucher(code: String)
    func onTextFieldShouldClear()
    func onDoctorDetailAction(at indexPath: IndexPath)
    
    
    // Return
    func getListSpecialist() -> [SpecialistModel]
    
    // UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> DoctorModel
    func onDidSelectRow(at indexPath: IndexPath)
    func onLoadMoreAction()
    
    // Geter
    func getVoucher() -> VoucherModel?
}

// MARK: - Interactor Input
protocol HealthServiceInteractorInputProtocol {
    func getSpecialist()
    func getDoctorList(voucherCode: String?, specialistId: Int, startDate: String, endDate: String, page: Int, perPage: Int)
    func getDoctorTime(doctor: DoctorModel?, startDate: String, endDate: String)
    func getVoucherDetail(voucherCode: String)
}

// MARK: - Interactor Output
protocol HealthServiceInteractorOutputProtocol: AnyObject {
    func onGetSpecialistFinished(with result: Result<[SpecialistModel], APIError>)
    func onGetDoctorListFinished(with result: Result<[DoctorModel], APIError>, page: Int, total: Int)
    func onGetDoctorTimeFinished(with result: Result<[WeekdayModel], APIError>, doctor: DoctorModel?)
    func onGetVoucherDetailFinished(with result: Result<VoucherModel, APIError>)
}

// MARK: - Router
protocol HealthServiceRouterProtocol: BaseRouterProtocol {
    func gotoBookingViewController(doctor: DoctorModel, listWeekday: [WeekdayModel], delegate: BookingPresenterDelegate?)
    func showAlertDateTimeBookingView(listWeekday: [WeekdayModel], doctor: DoctorModel?, delegate: AlertCalendarCareViewDelegate?)
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?)
    func showAlertCalendarDoctorView(doctor: DoctorModel?, listWeekday: [WeekdayModel], delegate: AlertCalendarDoctorViewDelegate?)
}
