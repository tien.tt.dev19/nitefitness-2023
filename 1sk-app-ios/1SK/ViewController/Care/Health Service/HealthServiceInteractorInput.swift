//
//  
//  HealthServiceInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthServiceInteractorInput {
    weak var output: HealthServiceInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - HealthServiceInteractorInputProtocol
extension HealthServiceInteractorInput: HealthServiceInteractorInputProtocol {
    func getDoctorTime(doctor: DoctorModel?, startDate: String, endDate: String) {
        guard let doctorId = doctor?.id else {
            return
        }
        self.careService?.getDoctorTime(doctorId: doctorId, startDate: startDate, endDate: endDate, completion: { [weak self] result in
            self?.output?.onGetDoctorTimeFinished(with: result.unwrapSuccessModel(), doctor: doctor)
        })
    }
    
    func getDoctorList(voucherCode: String?, specialistId: Int, startDate: String, endDate: String, page: Int, perPage: Int) {
        self.careService?.getDoctorList(voucherCode: voucherCode, specialistId: specialistId, startDate: startDate, endDate: endDate, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetDoctorListFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
    func getSpecialist() {
        self.careService?.getSpecialistList(completion: { [weak self] result in
            self?.output?.onGetSpecialistFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getVoucherDetail(voucherCode: String) {
        self.careService?.getVoucherDetail(voucherCode: voucherCode, completion: { [weak self] result in
            self?.output?.onGetVoucherDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
