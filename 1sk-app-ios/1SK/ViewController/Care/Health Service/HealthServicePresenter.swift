//
//  
//  HealthServicePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthServicePresenter {

    weak var view: HealthServiceViewProtocol?
    private var interactor: HealthServiceInteractorInputProtocol
    private var router: HealthServiceRouterProtocol

    init(interactor: HealthServiceInteractorInputProtocol,
         router: HealthServiceRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var doctor: DoctorModel?

    var listSpecialist: [SpecialistModel] = []
    var listDoctors: [DoctorModel] = []
    var listSectionWeekday: [SectionWeekdayModel] = []
    
    var listWeekdayRoot: [WeekdayModel] = []
    
    var specialistSelected: SpecialistModel?
    var voucher: VoucherModel?
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 15
    private var isOutOfData = false
    
    private let startTime = Date().toString(Date.Format.ymdSlash)
    private let endTime = Date().futureWeekDay.toString(Date.Format.ymdSlash)
    
    private func getData(in page: Int, specialistId: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        self.router.showHud()
        
        let voucherCode = self.view?.getVoucherCode()
        self.interactor.getDoctorList(voucherCode: voucherCode, specialistId: specialistId, startDate: self.startTime, endDate: self.endTime, page: page, perPage: self.limit)
    }
    
    func getInitData() {
        self.view?.setCountList(count: 0, specialistModel: self.specialistSelected)
        self.interactor.getSpecialist()
        
        self.getData(in: 1, specialistId: 0)
    }
    
    func onGetDoctorTimeSuccess(listWeekday: [WeekdayModel], doctor: DoctorModel?) {
//        guard listWeekday.count > 0 else {
//            SKToast.shared.showToast(content: "Bác sĩ này hiện tại đang bận \nVui lòng chọn bác sĩ khác")
//            self.getData(in: 1, specialistId: 0)
//            return
//        }
        
        self.listWeekdayRoot = listWeekday
        self.listWeekdayRoot.first?.isExpand = true
        self.router.showAlertCalendarDoctorView(doctor: doctor, listWeekday: self.listWeekdayRoot, delegate: self)
        
//        self.listWeekdayRoot.removeAll()
//
//        let currentWeekDates = Date().currentContinueWeekDates()
//        for item in currentWeekDates {
//            let weekday = WeekdayModel()
//            weekday.date = item.toTimestamp()
//
//            let day = item.toString(Date.Format.dmySlash)
//            let today = Date().toString(Date.Format.dmySlash)
//
//            for dateItem in listWeekday {
//                let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(dateItem.date ?? 0))
//                let dateStr = date.toString(Date.Format.dmySlash)
//
//                if dateStr == day {
//                    weekday.slotTimes = dateItem.slotTimes
//                }
//            }
//
//            if weekday.slotTimes?.count ?? 0 == 0 {
//                weekday.isState = .disable
//            } else {
//                weekday.isState = .enable
//            }
//
//            if day == today {
//                weekday.isToday = true
//            }
//
//            self.listWeekdayRoot.append(weekday)
//        }
//
//        for weekday in self.listWeekdayRoot {
//            if weekday.slotTimes?.count ?? 0 > 0 {
//                weekday.isState = .selected
//                break
//            }
//        }
//        self.router.showAlertDateTimeBookingView(listWeekday: self.listWeekdayRoot, doctor: doctor, delegate: self)
        
    }
    
    func onGetDoctorTimeAvailable(doctor: DoctorModel?) {
        self.router.showHud()
        self.interactor.getDoctorTime(doctor: doctor, startDate: self.startTime, endDate: self.endTime)
    }
    
    func onGetVoucherDetailSuccess(voucher: VoucherModel?) {
        self.voucher = voucher
        self.view?.onVoucherLayout(voucher: self.voucher)
        
        self.router.hideHud()
        
        let countSpecialist = self.voucher?.specialistIds?.count
        if countSpecialist == 1, let specialistId = voucher?.specialistIds?.first {
            if let specialist = self.listSpecialist.first(where: {$0.id == specialistId}) {
                self.view?.onSelectSpecialistAction(specialist: specialist)
            }
            
        } else {
            if let specialist = self.listSpecialist.first(where: {$0.id == 0}) {
                self.view?.onSelectSpecialistAction(specialist: specialist)
            }
        }
    }
}

// MARK: - HealthServicePresenterProtocol
extension HealthServicePresenter: HealthServicePresenterProtocol {
    func onViewDidLoad() {
        self.getInitData()
        if let mDoctor = self.doctor {
            self.onGetDoctorTimeAvailable(doctor: mDoctor)
        }
    }
    
    // Action
    func onBookingAction() {
        //
    }
    
    func onSelectSpecialistAction(specialist: SpecialistModel) {
        self.specialistSelected = specialist
        
        guard let specialistId = self.specialistSelected?.id else {
            return
        }
        self.listDoctors.removeAll()
        self.view?.onReloadData()
        self.page = 1
        self.getData(in: 1, specialistId: specialistId)
    }
    
    func onCheckVoucher(code: String) {
        self.router.showHud()
        self.interactor.getVoucherDetail(voucherCode: code)
    }
    
    func onTextFieldShouldClear() {
        self.voucher = nil
        if let specialist = self.listSpecialist.first(where: {$0.id == 0}) {
            self.view?.onSelectSpecialistAction(specialist: specialist)
        }
    }
    
    func onDoctorDetailAction(at indexPath: IndexPath) {
        let doctor = self.listDoctors[indexPath.row]
        self.router.gotoDoctorDetailVC(doctor: doctor, isFrom: "DoctorDetailViewController", delegate: self)
    }
    
    // Return
    func getListSpecialist() -> [SpecialistModel] {
        return self.listSpecialist
    }
    
    // UITableView
    func numberOfRows() -> Int {
        return self.listDoctors.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> DoctorModel {
        return self.listDoctors[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        let doctor = self.listDoctors[indexPath.row]
        self.onGetDoctorTimeAvailable(doctor: doctor)
    }
    
    func onLoadMoreAction() {
        if self.listDoctors.count >= self.limit && !self.isOutOfData {
            guard let specialistId = self.specialistSelected?.id else {
                return
            }
            self.getData(in: self.page + 1, specialistId: specialistId)
            
        } else {
            if self.listDoctors.count >= self.limit {
                SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            }
            self.view?.onLoadingSuccess()
        }
    }
    
    // Geter
    func getVoucher() -> VoucherModel? {
        return self.voucher
    }
}

// MARK: - HealthServiceInteractorOutput 
extension HealthServicePresenter: HealthServiceInteractorOutputProtocol {
    func onGetSpecialistFinished(with result: Result<[SpecialistModel], APIError>) {
        switch result {
        case .success(let listSpecialistModel):
            self.listSpecialist = listSpecialistModel
            
            let specialistAll = SpecialistModel()
            specialistAll.id = 0
            specialistAll.name = "Tất cả"
            self.listSpecialist.insert(specialistAll, at: 0)
            self.specialistSelected = specialistAll
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetDoctorListFinished(with result: Result<[DoctorModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listDoctors = listModel
                self.view?.onReloadData()

            } else {
                for object in listModel {
                    self.listDoctors.append(object)
                    self.view?.onInsertRow(at: self.listDoctors.count - 1)
                }
            }
            self.total = total
            self.view?.setCountList(count: total, specialistModel: self.specialistSelected)

            // Set Out Of Data
            if self.listDoctors.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        // Set Status View Data Not Found
        if self.listDoctors.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
        self.router.hideHud()
    }
    
    func onGetDoctorTimeFinished(with result: Result<[WeekdayModel], APIError>, doctor: DoctorModel?) {
        switch result {
        case .success(let listModel):
            self.onGetDoctorTimeSuccess(listWeekday: listModel, doctor: doctor)
            
        case .failure(let error):
            debugPrint(error)
            self.onGetDoctorTimeSuccess(listWeekday: [], doctor: doctor)
        }
        self.router.hideHud()
    }
    
    func onGetVoucherDetailFinished(with result: Result<VoucherModel, APIError>) {
        switch result {
        case .success(let model):
            self.onGetVoucherDetailSuccess(voucher: model)
            
        case .failure:
            self.router.hideHud()
            
            self.voucher = nil
            self.view?.onVoucherLayout(voucher: nil)
            
            /// Get Data Doctor
            guard let specialistId = self.specialistSelected?.id else {
                return
            }
            self.listDoctors.removeAll()
            self.view?.onReloadData()
            self.page = 1
            self.getData(in: 1, specialistId: specialistId)
            
        }
    }
}

// MARK: - AlertDateTimeBookingViewDelegate
extension HealthServicePresenter: AlertCalendarCareViewDelegate {
    func onAlertCancelAction() {
        //
    }
    
    func onAlertAgreeAction(doctor: DoctorModel?) {
        for weekday in self.listWeekdayRoot {
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if slotTime.isSelected == true {
                        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
                        print("onAgreeAction dayOfWeek:", date.dayOfWeek() ?? "nil")
                        print("onAgreeAction day:", date.toString(Date.Format.dmySlash))
                        
                        let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.startAt ?? 0))
                        let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.endAt ?? 0))
                        let text = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
                        print("onAgreeAction slotTime:", text)
                        
                        print("onAgreeAction =======================\n")
                    }
                }
            }
        }
        
        guard let mDoctor = doctor else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            mDoctor.voucher = self.voucher
            self.router.gotoBookingViewController(doctor: mDoctor, listWeekday: self.listWeekdayRoot, delegate: self)
        }
    }
}


// MARK: - BookingPresenterDelegate
extension HealthServicePresenter: BookingPresenterDelegate {
    func onVoucherExpiredBackAction() {
        self.view?.onVoucherExpiredBackAction()
        self.getData(in: 1, specialistId: 0)
    }
}

// MARK: - DoctorDetailPresenterDelegate
extension HealthServicePresenter: DoctorDetailPresenterDelegate {
    func onDetailConfirmBookingAction(doctor: DoctorModel?) {
        self.onGetDoctorTimeAvailable(doctor: doctor)
    }
}

// MARK: AlertCalendarDoctorViewDelegate
extension HealthServicePresenter: AlertCalendarDoctorViewDelegate {
    func onAlertCalendarDoctorViewCancelAction() {
        //
    }
    
    func onAlertCalendarDoctorViewAgreeAction(doctor: DoctorModel?) {
        for weekday in self.listWeekdayRoot {
            if let listSlotTimes = weekday.slotTimes {
                for slotTime in listSlotTimes {
                    if slotTime.isSelected == true {
                        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(weekday.date ?? 0))
                        print("onAgreeAction dayOfWeek:", date.dayOfWeek() ?? "nil")
                        print("onAgreeAction day:", date.toString(Date.Format.dmySlash))
                        
                        let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.startAt ?? 0))
                        let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(slotTime.endAt ?? 0))
                        let text = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
                        print("onAgreeAction slotTime:", text)
                        
                        print("onAgreeAction =======================\n")
                    }
                }
            }
        }
        
        guard let mDoctor = doctor else {
            return
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            mDoctor.voucher = self.voucher
            self.router.gotoBookingViewController(doctor: mDoctor, listWeekday: self.listWeekdayRoot, delegate: self)
        }
    }
}
