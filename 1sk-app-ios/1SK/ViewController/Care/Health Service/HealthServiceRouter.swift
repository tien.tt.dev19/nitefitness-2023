//
//  
//  HealthServiceRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthServiceRouter: BaseRouter {
//    weak var viewController: HealthServiceViewController?
    static func setupModule(doctor: DoctorModel?) -> HealthServiceViewController {
        let viewController = HealthServiceViewController()
        let router = HealthServiceRouter()
        let interactorInput = HealthServiceInteractorInput()
        let presenter = HealthServicePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.doctor = doctor
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - HealthServiceRouterProtocol
extension HealthServiceRouter: HealthServiceRouterProtocol {
    func showAlertDateTimeBookingView(listWeekday: [WeekdayModel], doctor: DoctorModel?, delegate: AlertCalendarCareViewDelegate?) {
        let controller = AlertCalendarCareViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.listWeekday = listWeekday
        controller.doctor = doctor
        self.viewController?.navigationController?.present(controller, animated: true)
    }
    
    func showAlertCalendarDoctorView(doctor: DoctorModel?, listWeekday: [WeekdayModel], delegate: AlertCalendarDoctorViewDelegate?) {
        let controller = AlertCalendarDoctorViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.doctor = doctor
        controller.listWeekday = listWeekday
        self.viewController?.navigationController?.present(controller, animated: true)
    }
    
    func gotoBookingViewController(doctor: DoctorModel, listWeekday: [WeekdayModel], delegate: BookingPresenterDelegate?) {
        let controller = BookingRouter.setupModule(width: doctor, listWeekday: listWeekday, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?) {
        let controller = DoctorDetailRouter.setupModule(doctor: doctor, isFrom: isFrom, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
}
