//
//  
//  HealthServiceViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class HealthServiceViewController: BaseViewController {

    var presenter: HealthServicePresenterProtocol!
    
    @IBOutlet weak var view_StepDoctor: UIView!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_Specialist: UIView!
    @IBOutlet weak var tf_Specialist: UITextField!
    
    @IBOutlet weak var tf_Voucher: UITextField!
    @IBOutlet weak var lbl_VoucherWarning: UILabel!
    @IBOutlet weak var btn_VoucherStatus: UIButton!
    
    @IBOutlet weak var lbl_Count: UILabel!
    @IBOutlet weak var stack_NoData_Workout: UIStackView!
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.title = "Tư vấn bác sĩ qua video"
        self.setInitUITextField()
        self.setInitUITableView()
        self.setInitGestureView()
    }
    
    
    // MARK: - Action
    @IBAction func onBookingAction(_ sender: Any) {
        self.presenter.onBookingAction()
    }
    
    @IBAction func onCheckVoucherAction(_ sender: Any) {
        self.setCheckVoucher()
    }
    
    @IBAction func onSpecialistDropDownAction(_ sender: Any) {
        self.onTapGestureSpecialistAction()
    }
    
}

// MARK: - HealthServiceViewProtocol
extension HealthServiceViewController: HealthServiceViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func setCountList(count: Int, specialistModel: SpecialistModel?) {
        guard let specialist = specialistModel else {
            self.lbl_Count.text = ""
            return
        }
        
        if specialist.id == 0 {
            self.lbl_Count.text = "Có \(count) bác sĩ chuyên khoa sẵn sàng"
        } else {
            self.lbl_Count.text = """
Có \(count) bác sĩ chuyên khoa "\(specialist.name ?? "")" sẵn sàng
"""
        }
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.stack_NoData_Workout.alpha = 0
            } completion: { _ in
                self.stack_NoData_Workout.isHidden = true
            }

        } else {
            self.stack_NoData_Workout.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.stack_NoData_Workout.alpha = 1
            }
        }
    }
    
    func onVoucherLayout(voucher: VoucherModel?) {
        guard let `voucher` = voucher else {
            self.btn_VoucherStatus.isSelected = false
            self.lbl_VoucherWarning.text = "Mã không hợp lệ"
            self.lbl_VoucherWarning.textColor = R.color.color_Text_Red_1()
            return
        }
        self.btn_VoucherStatus.isSelected = true
        if voucher.isFixed == true {
            let priceVoucher = voucher.discountAmount ?? 0
            self.lbl_VoucherWarning.text = "Mã Voucher trị giá \(priceVoucher.formatNumber())đ"
        } else {
            self.lbl_VoucherWarning.text = "Mã Voucher trị giá \(voucher.discountAmount ?? 0)%"
        }
        
        self.lbl_VoucherWarning.textColor = R.color.mainColor()
    }
    
    func getVoucherCode() -> String? {
        return self.tf_Voucher.text
    }
    
    func onVoucherExpiredBackAction() {
        self.btn_VoucherStatus.isSelected = false
        self.lbl_VoucherWarning.text = ""
        self.tf_Voucher.text = ""
    }
}

// MARK: - UITextFieldDelegate
extension HealthServiceViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Specialist.text = "Tất cả"
        self.tf_Specialist.delegate = self
        self.tf_Voucher.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case self.tf_Specialist:
            self.onTapGestureSpecialistAction()
            return false
            
        case self.tf_Voucher:
            self.btn_VoucherStatus.isSelected = false
            self.lbl_VoucherWarning.text = ""
            return true
            
        default:
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case self.tf_Voucher:
            if textField.text?.count ?? 0 <= 0 {
                self.presenter.onTextFieldShouldClear()
            }
            
        default:
            break
        }
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        switch textField {
        case self.tf_Voucher:
            self.tf_Voucher.text = self.tf_Voucher.text?.uppercased() ?? ""
            
            if textField.text?.count ?? 0 == 6 {
                self.setCheckVoucher()
            }
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.tf_Voucher:
            let count = textField.text?.count ?? 0
            if count < 6 {
                return true
                
            } else {
                return false
            }
            
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.tf_Voucher:
            self.setCheckVoucher()
            return true
            
        default:
            return true
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        switch textField {
        case self.tf_Voucher:
            self.tf_Voucher.text = ""
        default:
            break
        }
        return true
    }
    
    func setCheckVoucher() {
        guard let code = self.tf_Voucher.text else {
            self.lbl_VoucherWarning.text = "Vui lòng nhập mã Voucher"
            self.lbl_VoucherWarning.textColor = R.color.color_Text_Red_1()
            return
        }
        guard code.count > 0 else {
            return
        }
        self.view.endEditing(true)
        self.presenter.onCheckVoucher(code: code)
    }
}

// MARK: - AlertSpecialistViewDelegate
extension HealthServiceViewController: AlertSpecialistViewDelegate {
    func setShowAlertSpecialistView() {
        let controller = AlertSpecialistViewController()
        controller.modalPresentationStyle = .custom
        controller.listSpecialist = self.presenter.getListSpecialist()
        controller.delegate = self
        self.navigationController?.present(controller, animated: true)
        
        self.view.endEditing(true)
    }
    
    // AlertSpecialistViewDelegate
    func onSelectSpecialistAction(specialist: SpecialistModel) {
        self.tf_Specialist.text = specialist.name
        self.presenter.onSelectSpecialistAction(specialist: specialist)
    }
}

// MARK: - HealthServiceViewProtocol
extension HealthServiceViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewHealthService.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewHealthService.self, for: indexPath)
        cell.delegate = self
        cell.config(doctor: self.presenter.cellForRow(at: indexPath))
        return cell
    }
}

// MARK: UITableViewDelegate
extension HealthServiceViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}

// MARK: - CellTableViewHealthServiceDelegate
extension HealthServiceViewController: CellTableViewHealthServiceDelegate {
    func onCellAvatarAction(_ cell: UITableViewCell) {
        guard let indexPath = self.tbv_TableView.indexPath(for: cell) else {
            return
        }
        self.presenter.onDoctorDetailAction(at: indexPath)
        
//        let doctor = self.presenter.cellForRow(at: indexPath)
//        let controller = AlertDoctorInfoViewController()
//        controller.view.frame = UIScreen.main.bounds
//        controller.config(doctor: doctor)
//        controller.showInView(self.tabBarController!, animated: true)
    }
    
    func onCellContentAction(_ cell: UITableViewCell) {
        guard let indexPath = self.tbv_TableView.indexPath(for: cell) else {
            return
        }
        self.presenter.onDidSelectRow(at: indexPath)
    }
}

// MARK: - Gesture View
extension HealthServiceViewController {
    func setInitGestureView() {
        self.view_Specialist.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureSpecialistAction)))
        self.view_StepDoctor.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureBgAction)))
    }
    
    @objc func onTapGestureSpecialistAction() {
        self.setShowAlertSpecialistView()
        
//        if self.tf_Voucher.text?.count ?? 0 <= 0 {
//            self.setShowAlertSpecialistView()
//
//        } else {
//            SKToast.shared.showToast(content: "Bạn đang nhập mã Voucher, vui lòng xóa mã Voucher để tiếp tục với chức năng này")
//        }
    }
    
    @objc func onTapGestureBgAction() {
        self.view.endEditing(true)
    }
}
