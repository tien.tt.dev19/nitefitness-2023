//
//
//  CareConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//
//

import UIKit

// MARK: - View
protocol CareViewProtocol: AnyObject {
    func onReloadData()
    func onReloadData(section: Int)
    func onJoinRoomActive(videoCall: VideoCallModel?)
    func onUpdateBadgeNotification(count: Int)
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?)
}

// MARK: - Presenter
protocol CarePresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    /// Action
    func onNotificationAction()
    
    func onHealthRecordsAction()
    func onHealthServiceAction()
    func onMyAppointmentAction()
    func onDetailBookingAction(appointment: AppointmentModel?)
    func onJoinRoomCallAction(appointment: AppointmentModel)
    func onCallAction()
    func onRefreshAction()
    
    func onSeeMoreHeaderNormalAction(section: Int)
    func onDidSelectItemPost(blog: BlogModel)
    func onDidSelectItemVideo(videoCare: VideoModel)
    
    /// UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel
    func onDidSelectRow(at indexPath: IndexPath)
    
    /// PHR Action
    func onDidChangeWeight(value: String?)
    func onDidChangeHeight(value: String?)
    func onDidChangeBloodGroup(value: String?)
    func onDidChangeBloodPressure(value: String?)
    func onDidChangeBloodSPO2(value: String?)
    func onDidChangeBloodSugar(value: String?)
    
    /// Get Value
    func getListPHR() -> [PHRModel]
    func getListVideoCare() -> [VideoModel]
    func getListBlogPostCare() -> [BlogModel]
    func getTotalAppointment() -> Int
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?)
}

// MARK: - Interactor Input
protocol CareInteractorInputProtocol {
    func getListAppointment()
    func getTokenAppointmentRoom(appointment: AppointmentModel)
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel)
    func getHotlinePhoneNumber()
    func getUserPhr()
    
    func setValueWeight(value: String)
    func setValueHeight(value: String)
    func setValueBloodGroup(value: String)
    func setValueBloodPressure(value: String)
    func setValueBloodSPO2(value: String)
    func setValueBloodSugar(value: String)
    
    func getListBlogCare(categoryId: Int, page: Int, perPage: Int)
    func getListVideoCare(categoryId: Int, page: Int, perPage: Int)
    
    func getCountBadgeNotificationCare()
    
    func getDoctorDetailById(doctorId: Int)
}

// MARK: - Interactor Output
protocol CareInteractorOutputProtocol: AnyObject {
    func onGetListAppointmentFinished(with result: Result<[AppointmentModel], APIError>, total: Int?)
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel)
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?)
    
    func onGetHotlinePhoneNumberFinished(with result: Result<HotlineModel, APIError>)
    func onGetUserPhrFinished(with result: Result<[PHRModel], APIError>)
    
    func onSetValueWeightFinished(with result: Result<PHRModel, APIError>)
    func onSetValueHeightFinished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodGroupFinished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodPressureFinished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodSPO2Finished(with result: Result<PHRModel, APIError>)
    func onSetValueBloodSugarFinished(with result: Result<PHRModel, APIError>)
    
    func onGetListBlogPostFinished(with result: Result<[BlogModel], APIError>)
    func onGetListVideoCareFinished(with result: Result<[VideoModel], APIError>)
    
    func onGetCountBadgeNotificationCareFinished(with result: Result<[NotificationModel], APIError>, meta: Meta?)
    
    func onGetDoctorDetailByIdFinished(with result: Result<DoctorModel, APIError>)
}

// MARK: - Router
protocol CareRouterProtocol: BaseRouterProtocol {
    func gotoHealthRecordsVC()
    func gotoHealthServiceVC()
    func gotoMyAppointmentVC()
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func showCallTelephone(hotline: String)
    
    func gotoBlogVideoCareViewController()
    func gotoBlogPostCareViewController()
    
    func gotoBlogDetailsViewController(blogModel: BlogModel)
    func gotoVideoDetailsViewController(with video: VideoModel)
    
    func gotoNotificationCareViewController()
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?)
    
    func alertDefault(title: String, message: String)
}
