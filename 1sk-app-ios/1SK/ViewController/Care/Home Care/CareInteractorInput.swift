//
//  
//  CareInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//
//

import UIKit

class CareInteractorInput {
    weak var output: CareInteractorOutputProtocol?
    var careService: CareServiceProtocol?
    var authService: AuthServiceProtocol?
    var blogService: BlogServiceProtocol?
    var fitnessService: FitnessServiceProtocol?
}

// MARK: - CareInteractorInputProtocol
extension CareInteractorInput: CareInteractorInputProtocol {
    func getListAppointment() {
        let listStatus = [AppointmentStatus.paying, AppointmentStatus.confirming, AppointmentStatus.confirmed]
        self.careService?.getListAppointments(isComing: true,
                                              isPats: false,
                                              page: 1,
                                              pageLimit: 2,
                                              orderByTime: "ASC",
                                              listStatus: listStatus,
                                              completion: { [weak self] result in
            let total = result.getTotal()
            self?.output?.onGetListAppointmentFinished(with: result.unwrapSuccessModel(), total: total)
        })
    }
    
    func getTokenAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta, appointment: appointment)
        })
    }
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenRenewAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenRenewAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
    
    func getHotlinePhoneNumber() {
        self.careService?.getHotlinePhoneNumber(completion: { [weak self] result in
            self?.output?.onGetHotlinePhoneNumberFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getUserPhr() {
        self.authService?.getUserPHR(completion: { [weak self] result in
            self?.output?.onGetUserPhrFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueWeight(value: String) {
        self.authService?.setValuePHR(value: value, phr: .weight, completion: { [weak self] result in
            self?.output?.onSetValueWeightFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueHeight(value: String) {
        self.authService?.setValuePHR(value: value, phr: .height, completion: { [weak self] result in
            self?.output?.onSetValueHeightFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodGroup(value: String) {
        self.authService?.setValuePHR(value: value, phr: .blood, completion: { [weak self] result in
            self?.output?.onSetValueBloodGroupFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodPressure(value: String) {
        self.authService?.setValuePHR(value: value, phr: .pressure, completion: { [weak self] result in
            self?.output?.onSetValueBloodPressureFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodSPO2(value: String) {
        self.authService?.setValuePHR(value: value, phr: .spo2, completion: { [weak self] result in
            self?.output?.onSetValueBloodSPO2Finished(with: result.unwrapSuccessModel())
        })
    }
    
    func setValueBloodSugar(value: String) {
        self.authService?.setValuePHR(value: value, phr: .sugar, completion: { [weak self] result in
            self?.output?.onSetValueBloodSugarFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListBlogCare(categoryId: Int, page: Int, perPage: Int) {
        self.blogService?.getListBlog(categoryId: categoryId, tagsId: nil, keywords: nil, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListBlogPostFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListVideoCare(categoryId: Int, page: Int, perPage: Int) {
        self.fitnessService?.getListVideo(categoryId: categoryId, tagsId: nil, keywords: nil, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListVideoCareFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getCountBadgeNotificationCare() {
        self.careService?.getListNotificationUnread(page: 1, limit: 1, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetCountBadgeNotificationCareFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
    
    func getDoctorDetailById(doctorId: Int) {
        self.careService?.getDoctorDetailById(doctorId: doctorId, completion: { [weak self] result in
            self?.output?.onGetDoctorDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
}
