//
//
//  CarePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//
//

import UIKit

class CarePresenter {

    weak var view: CareViewProtocol?
    private var interactor: CareInteractorInputProtocol
    private var router: CareRouterProtocol

    init(interactor: CareInteractorInputProtocol,
         router: CareRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var listAppointment: [AppointmentModel] = []
    var litsPHRModel: [PHRModel] = []
    var listVideoCare: [VideoModel] = []
    var listBlogPostCare: [BlogModel] = []
    
    var totalAppointment = 0
    
    func getDataListAppointment() {
        self.router.showHud()
        self.interactor.getListAppointment()
    }
    
    func getDataListBlog() {
        self.interactor.getListBlogCare(categoryId: CategoryType.blogCare.rawValue, page: 1, perPage: 10)
        self.interactor.getListVideoCare(categoryId: CategoryType.videoCare.rawValue, page: 1, perPage: 10)
    }
}

// MARK: - CarePresenterProtocol
extension CarePresenter: CarePresenterProtocol {
    func onViewDidLoad() {
        self.getDataListAppointment()
        self.getDataListBlog()
        
        self.setInitHandleSocketEvent()
//        self.setInitUIApplication()
    }
    
    func onViewDidAppear() {
        self.view?.onUpdateBadgeNotification(count: gBadgeCare)
        self.interactor.getCountBadgeNotificationCare()
        self.interactor.getUserPhr()
    }
    
    //MARK: Action
    func onNotificationAction() {
        self.router.gotoNotificationCareViewController()
    }
    
    func onHealthRecordsAction() {
        self.router.gotoHealthRecordsVC()
    }
    
    func onHealthServiceAction() {
        self.router.gotoHealthServiceVC()
    }
    
    func onMyAppointmentAction() {
        self.router.gotoMyAppointmentVC()
    }
    
    func onDetailBookingAction(appointment: AppointmentModel?) {
        self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: self)
    }
    
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.router.showHud()
        self.interactor.getTokenAppointmentRoom(appointment: appointment)
    }
    
    func onCallAction() {
        self.router.hideHud()
        self.interactor.getHotlinePhoneNumber()
    }
    
    func onRefreshAction() {
        self.getDataListAppointment()
    }
    
    func onSeeMoreHeaderNormalAction(section: Int) {
        switch section {
        case 2:
            self.router.gotoHealthRecordsVC()
            
//        case 3:
//            self.router.gotoBlogPostCareViewController()
            
        case 3:
            self.router.gotoBlogVideoCareViewController()
            
        default:
            break
        }
    }
    
    func onDidSelectItemPost(blog: BlogModel) {
        self.router.gotoBlogDetailsViewController(blogModel: blog)
    }
    
    func onDidSelectItemVideo(videoCare: VideoModel) {
        self.router.gotoVideoDetailsViewController(with: videoCare)
    }
    
    //MARK: UITableView
    func numberOfRows() -> Int {
        return self.listAppointment.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel {
        return self.listAppointment[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        guard self.listAppointment.count > 0 else {
            return
        }
        let appointment = self.listAppointment[indexPath.row]
        self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: self)
    }
    
    //MARK: PHR Action
    func onDidChangeWeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" && value.count <= 3 else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.router.showHud()
        self.interactor.setValueWeight(value: value)
    }
    
    func onDidChangeHeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" && value.count <= 3 else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.router.showHud()
        self.interactor.setValueHeight(value: value)
    }
    
    func onDidChangeBloodGroup(value: String?) {
        guard let `value` = value else {
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodGroup(value: value)
    }
    
    func onDidChangeBloodPressure(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" && value.count <= 7 else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodPressure(value: value)
    }
    
    func onDidChangeBloodSPO2(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" && value.count <= 3 else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodSPO2(value: value)
    }
    
    func onDidChangeBloodSugar(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" && value.count <= 3 else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.router.showHud()
        self.interactor.setValueBloodSugar(value: value)
    }
    
    //MARK: Get Value
    func getListPHR() -> [PHRModel] {
        return self.litsPHRModel
    }
    
    func getListVideoCare() -> [VideoModel] {
        return self.listVideoCare
    }
    
    func getListBlogPostCare() -> [BlogModel] {
        return self.listBlogPostCare
    }
    
    func getTotalAppointment() -> Int {
        return self.totalAppointment
    }
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        guard let `appointment` = appointment else {
            return
        }
        self.router.showHud()
        self.interactor.getTokenRenewAppointmentRoom(appointment: appointment)
    }
}

// MARK: - CareInteractorOutput
extension CarePresenter: CareInteractorOutputProtocol {
    func onGetListAppointmentFinished(with result: Result<[AppointmentModel], APIError>, total: Int?) {
        switch result {
        case .success(let listModel):
            self.listAppointment = listModel
            self.totalAppointment = total ?? 0
            self.view?.onReloadData(section: 1)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel) {
        if meta?.code == 10002 {
            self.view?.onShowAlertRenewToken(meta: meta, appointment: appointment)
            
        } else {
            switch result {
            case .success(let model):
                let room = model
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
            case .failure(let error):
                let room = VideoCallModel()
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
                self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
            }
        }
        self.router.hideHud()
    }
    
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?) {
        switch result {
        case .success(let model):
            let room = model
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
        case .failure(let error):
            let room = VideoCallModel()
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
            self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
        }
        self.router.hideHud()
    }
    
    func onGetHotlinePhoneNumberFinished(with result: Result<HotlineModel, APIError>) {
        switch result {
        case .success(let model):
            self.router.showCallTelephone(hotline: model.hotline ?? "")
            
        case .failure(let error):
            SKToast.shared.showToast(content: "Lỗi số điện thoại Hotline")
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetUserPhrFinished(with result: Result<[PHRModel], APIError>) {
        switch result {
        case .success(let listModel):
            self.litsPHRModel = listModel
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetListBlogPostFinished(with result: Result<[BlogModel], APIError>) {
        switch result {
        case .success(let listModel):
            for item in listModel {
                item.categoryType = .blogCare
            }
            
            self.listBlogPostCare = listModel
            self.view?.onReloadData(section: 3)
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetListVideoCareFinished(with result: Result<[VideoModel], APIError>) {
        switch result {
        case .success(let listModel):
            self.listVideoCare = listModel
            self.view?.onReloadData(section: 4)
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetCountBadgeNotificationCareFinished(with result: Result<[NotificationModel], APIError>, meta: Meta?) {
        switch result {
        case .success:
            gBadgeCare = meta?.total ?? 0
            self.view?.onUpdateBadgeNotification(count: gBadgeCare)
//            UIApplication.shared.applicationIconBadgeNumber = gBadgeCare
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    // MARK: PHR Value
    func onSetValueWeightFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số cân nặng")
            
            if let index = self.litsPHRModel.firstIndex(where: {$0.name == PHR.weight.name}) {
                self.litsPHRModel[index] = model
                
            } else {
                self.litsPHRModel.append(model)
            }
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueHeightFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số chiều cao")
            
            if let index = self.litsPHRModel.firstIndex(where: {$0.name == PHR.height.name}) {
                self.litsPHRModel[index] = model
                
            } else {
                self.litsPHRModel.append(model)
            }
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodGroupFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số nhóm máu")
            
            if let index = self.litsPHRModel.firstIndex(where: {$0.name == PHR.blood.name}) {
                self.litsPHRModel[index] = model
                
            } else {
                self.litsPHRModel.append(model)
            }
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodPressureFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số huyết áp")
            
            if let index = self.litsPHRModel.firstIndex(where: {$0.name == PHR.pressure.name}) {
                self.litsPHRModel[index] = model
                
            } else {
                self.litsPHRModel.append(model)
            }
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodSPO2Finished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số SPO2")
            
            if let index = self.litsPHRModel.firstIndex(where: {$0.name == PHR.spo2.name}) {
                self.litsPHRModel[index] = model
                
            } else {
                self.litsPHRModel.append(model)
            }
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onSetValueBloodSugarFinished(with result: Result<PHRModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Đã cập nhật chỉ số đường huyết")
            
            if let index = self.litsPHRModel.firstIndex(where: {$0.name == PHR.sugar.name}) {
                self.litsPHRModel[index] = model
                
            } else {
                self.litsPHRModel.append(model)
            }
            self.view?.onReloadData(section: 2)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetDoctorDetailByIdFinished(with result: Result<DoctorModel, APIError>) {
        switch result {
        case .success(let model):
            self.setGoToDoctorDetailView(doctor: model)
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
}

// MARK: - DetailAppointmentPresenterDelegate
extension CarePresenter: DetailAppointmentPresenterDelegate {
    func onCancelAppointmentSuccess(appointment: AppointmentModel) {
        self.view?.onReloadData(section: 1)
    }
    
    func onAppointmentChangeStatus(appointment: AppointmentModel) {
        self.view?.onReloadData(section: 1)
    }
}

// MARK: - DoctorDetailPresenterDelegate
extension CarePresenter: DoctorDetailPresenterDelegate {
    func setGoToDoctorDetailView(doctor: DoctorModel?) {
        self.router.gotoDoctorDetailVC(doctor: doctor, isFrom: "CareViewController", delegate: self)
    }
    
    func onDetailConfirmBookingAction(doctor: DoctorModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.router.gotoHealthServiceVC()
        }
    }
}

// MARK: Socket Handle
extension CarePresenter {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentCreatedEvent), name: .AppointmentCreatedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentShowButtonJoinEvent), name: .AppointmentShowButtonJoinEvent, object: nil)
    }
    
    @objc func onAppointmentCreatedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentCreatedEvent CarePresenter")
        self.getDataListAppointment()
        
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentStatusChangedEvent CarePresenter")
        self.view?.onUpdateBadgeNotification(count: gBadgeCare)
        self.getDataListAppointment()
    }
    
    @objc func onAppointmentShowButtonJoinEvent(notification: NSNotification) {
        print("socket.on care: AppointmentShowButtonJoinEvent CarePresenter")
        
    }
}

// MARK: UIApplication - DeepLinks
//extension CarePresenter {
//    func setInitUIApplication() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleOpenDeepLinks), name: .applicationOpenDeepLinks, object: nil)
//    }
//
//    @objc func onHandleOpenDeepLinks() {
//        print("<AppDelegate> onOpenDeepLinks gUniversalLink: \(String(describing: gUniversalLink))")
//
//        if let universalLink = gUniversalLink {
//            if let params = universalLink.params() {
//                guard let service = params["position"] as? String, let share = params["share"] as? String, let id = params["id"] as? String else {
//                    print("<AppDelegate> onOpenDeepLinks: params nil")
//                    return
//                }
//
//                guard let shareId = Int(id) else {
//                    print("<AppDelegate> onOpenDeepLinks: shareId nil")
//                    return
//                }
//                print("<AppDelegate> onOpenDeepLinks: shareId: \(id)")
//
//                switch service {
//                case "care":
//                    switch share {
//                    case ShareType.doctor.rawValue:
//                        DispatchQueue.main.async {
//                            self.router.showHud()
//                            self.interactor.getDoctorDetailById(doctorId: shareId)
//                        }
//
//                    default:
//                        break
//                    }
//
//                default:
//                    break
//                }
//            }
//
//            gUniversalLink = nil
//        }
//    }
//}
