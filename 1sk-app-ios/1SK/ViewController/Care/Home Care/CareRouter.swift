//
//  
//  CareRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//
//

import UIKit

class CareRouter: BaseRouter {
//    weak var viewController: CareViewController?
    static func setupModule() -> CareViewController {
        let viewController = CareViewController()
        let router = CareRouter()
        let interactorInput = CareInteractorInput()
        let presenter = CarePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        interactorInput.authService = AuthService()
        interactorInput.blogService = BlogService()
        interactorInput.fitnessService = FitnessService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - CareRouterProtocol
extension CareRouter: CareRouterProtocol {
    func gotoHealthRecordsVC() {
        let controller = HealthRecordsRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoHealthServiceVC() {
        let controller = HealthServiceRouter.setupModule(doctor: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoMyAppointmentVC() {
        let controller = MyAppointmentRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showCallTelephone(hotline: String) {
        self.viewController?.onCallTelephone(phoneNumber: hotline)
    }
    
    func gotoBlogVideoCareViewController() {
        let controller = BlogVideoRouter.setupModule(with: .videoCare)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoBlogPostCareViewController() {
        let controller = BlogPostRouter.setupModule(with: .blogCare)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoBlogDetailsViewController(blogModel: BlogModel) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: .blogCare)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoVideoDetailsViewController(with video: VideoModel) {
        let controller = PlayVideoRouter.setupModule(with: video, categoryType: .videoCare)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoNotificationCareViewController() {
//        let controller = NotificationCareRouter.setupModule()
        let controller = NotificationRouter.setupModule(pushNotiModel: nil)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?) {
        let controller = DoctorDetailRouter.setupModule(doctor: doctor, isFrom: isFrom, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func alertDefault(title: String, message: String) {
        self.viewController?.alertDefault(title: title, message: message)
    }
}
