//
//
//  CareViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//
//

import UIKit
import Photos

class CareViewController: BaseViewController {

    var presenter: CarePresenterProtocol!
    
    @IBOutlet weak var view_Badge: UIView!
    @IBOutlet weak var lbl_Badge: EdgeInsetLabel!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var btn_Call: UIButtonShadow!
    
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.onViewDidAppear()
        
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitTableView()
        self.setInitRefreshControl()
        self.title = "Tư vấn"
    }
    
    // MARK: - Action
    @IBAction func onCallAction(_ sender: Any) {
        self.presenter.onCallAction()
    }
    
    @IBAction func onNotificationAction(_ sender: Any) {
        self.presenter.onNotificationAction()
    }
}

// MARK: - CareViewProtocol
extension CareViewController: CareViewProtocol {
    func onUpdateBadgeNotification(count: Int) {
        if count > 0 {
            if count <= 9 {
                self.lbl_Badge.text = count.asString
            } else {
                self.lbl_Badge.text = "9+"
            }
            self.view_Badge.isHidden = false
        } else {
            self.view_Badge.isHidden = true
        }
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onReloadData(section: Int) {
        switch section {
        case 1:
            let indexSet = IndexSet(integer: section)
            self.tbv_TableView.reloadSections(indexSet, with: .automatic)
            
        default:
            self.tbv_TableView.reloadData()
        }
    }
    
    func onJoinRoomActive(videoCall: VideoCallModel?) {
        if videoCall?.appId != nil && videoCall?.channelName != nil && videoCall?.token != nil {
            self.showJoinRoomVideoCallView(videoCall: videoCall!)
            
        } else {
            self.onShowAlertWarningAppointment(videoCall: videoCall)
        }
    }
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?) {
        self.showAlertRenewTokenCallViewController(title: "Cuộc gọi tư vấn", content: meta?.message ?? "Bạn đang tham gia tư vấn online bằng thiết bị khác", appointment: appointment, delegate: self)
    }
}

// MARK: AlertRenewTokenCallViewDelegate
extension CareViewController: AlertRenewTokenCallViewDelegate {
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        self.presenter.onAgreeAlertRenewTokenCallAction(appointment: appointment)
    }
}

// MARK: Refresh Control
extension CareViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension CareViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewAppointment.self)
//        self.tbv_TableView.registerNib(ofType: CellTableViewCareListBlog.self)
//        self.tbv_TableView.registerNib(ofType: CellTableViewCareListVideo.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewPhr.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewEmpty.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewNoData.self)
        
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderCareService.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderCareAppointment.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderNormalMore.self)
        
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 0:
            return 210
            
        default:
            return 24
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderCareService.self)
            header.delegate = self
            return header

        case 1:
            let count = self.presenter.getTotalAppointment()
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderCareAppointment.self)
            header.delegate = self
            header.setStateHeader(countAppointment: count)
            return header
            
        case 2:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderNormalMore.self)
            header.delegate = self
            header.section = section
            header.config(title: "Sổ sức khỏe của tôi", isMore: false)
            header.btn_SeeMore.setTitle("Xem thêm", for: .normal)
            return header
            
        default:
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            let count = self.presenter.numberOfRows()
            if count > 0 {
                return count
            } else {
                return 1
            }
            
        case 0, 2, 3:
            return 1
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
       
        case 0:
            let cell = tableView.dequeueCell(ofType: CellTableViewEmpty.self, for: indexPath)
            return cell
            
        case 1:
            let count = self.presenter.numberOfRows()
            if count > 0 {
                let cell = tableView.dequeueCell(ofType: CellTableViewAppointment.self, for: indexPath)
                cell.delegate = self
                cell.config(appointment: self.presenter.cellForRow(at: indexPath))
                return cell
            } else {
                let cell = tableView.dequeueCell(ofType: CellTableViewNoData.self, for: indexPath)
                return cell
            }
        case 2:
            let cell = tableView.dequeueCell(ofType: CellTableViewPhr.self, for: indexPath)
            cell.delegate = self
            cell.onSetStateData(listPHR: self.presenter.getListPHR())
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: - UITableViewDelegate
extension CareViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.section {
        case 1:
            self.presenter.onDidSelectRow(at: indexPath)
            
        default:
            break
        }
    }
}

// MARK: - SectionHeaderCareServiceDelegate
extension CareViewController: SectionHeaderCareServiceDelegate {
    func onHealthServiceAction() {
        self.presenter.onHealthServiceAction()
    }
}

// MARK: - SectionHeaderNormalMoreDelegate
extension CareViewController: SectionHeaderNormalMoreDelegate {
    func onSeeMoreHeaderNormalAction(section: Int) {
        self.presenter.onSeeMoreHeaderNormalAction(section: section)
    }
}

// MARK: - SectionHeaderCareAppointmentDelegate
extension CareViewController: SectionHeaderCareAppointmentDelegate {
    func onSeeMoreMyAppointment() {
        self.presenter.onMyAppointmentAction()
    }
}

// MARK: - CellTableViewPhrDelegate
extension CareViewController: CellTableViewPhrDelegate {
    func onSelectBloodGroupAction() {
        self.view.endEditing(true)
        self.showAlertPickerViewAction()
    }
    
    func onTextFieldDidEndEditing(_ phr: PHR, value: String?) {
        switch phr {
        case .weight:
            self.presenter.onDidChangeWeight(value: value)
            
        case .height:
            self.presenter.onDidChangeHeight(value: value)
            
        case .blood:
            self.presenter.onDidChangeBloodGroup(value: value)
            
        case .pressure:
            self.presenter.onDidChangeBloodPressure(value: value)
            
        case .spo2:
            self.presenter.onDidChangeBloodSPO2(value: value)
            
        case .sugar:
            self.presenter.onDidChangeBloodSugar(value: value)
            
        }
    }
}

// MARK: - CellTableViewAppointmentDelegate
extension CareViewController: CellTableViewAppointmentDelegate {
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.onCheckAuthorizationStatus(appointment: appointment)
    }
}

// MARK: - AVCaptureDevice
extension CareViewController {
    func onCheckAuthorizationStatus(appointment: AppointmentModel) {
        guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized &&  AVCaptureDevice.authorizationStatus(for: .audio) == .authorized else {
            self.onCheckAuthorizationVideo(appointment: appointment)
            return
        }
        self.presenter.onJoinRoomCallAction(appointment: appointment)
    }
    
    func onCheckAuthorizationVideo(appointment: AppointmentModel) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.onCheckAuthorizationAudio(appointment: appointment)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    self.onCheckAuthorizationAudio(appointment: appointment)
                    
                } else {
                    //access denied
                    if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Camera để có thể tiếp tục")
                        }
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
                    }
                }
            })
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
        }
    }
    
    func onCheckAuthorizationAudio(appointment: AppointmentModel) {
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
        case .authorized:
            DispatchQueue.main.async {
                self.presenter.onJoinRoomCallAction(appointment: appointment)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .audio) { granted in
                if granted == true {
                   // User granted
                    DispatchQueue.main.async {
                        self.presenter.onJoinRoomCallAction(appointment: appointment)
                    }
                    
                } else {
                    // User denied
                    if AVCaptureDevice.authorizationStatus(for: .audio) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Microphone để có thể tiếp tục")
                        }
                        
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
                        message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
                    }
                }
            }
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
        }
    }
}

// MARK: - AlertPickerViewDelegate
extension CareViewController: AlertPickerViewDelegate {
    func showAlertPickerViewAction() {
        let listPHR = self.presenter.getListPHR()
        
        if let bloodPHR = listPHR.first(where: {$0.name == PHR.blood.name}) {
            var listItem: [ItemPickerView] = []
            for (index, blood) in BloodGroup.allCases.enumerated() {
                let item = ItemPickerView()
                item.id = index
                item.name = blood.name
                if blood.name == bloodPHR.value {
                    item.isSelected = true
                } else {
                    item.isSelected = false
                }
                
                listItem.append(item)
            }
            
            let controller = AlertPickerViewViewController()
            controller.modalPresentationStyle = .custom
            controller.alertTitle = "Chọn nhóm máu"
            controller.delegate = self
            controller.listItemPickerView = listItem
            self.navigationController?.present(controller, animated: true)
            
        } else {
            var listItem: [ItemPickerView] = []
            for (index, blood) in BloodGroup.allCases.enumerated() {
                let item = ItemPickerView()
                item.id = index
                item.name = blood.name
                listItem.append(item)
            }
            
            let controller = AlertPickerViewViewController()
            controller.modalPresentationStyle = .custom
            controller.alertTitle = "Chọn nhóm máu"
            controller.delegate = self
            controller.listItemPickerView = listItem
            self.navigationController?.present(controller, animated: true)
        }
    }
    
    func onAlertCancelAction() {
        //
    }
    
    func onAlertAgreeAction(item: ItemPickerView) {
        let blood = BloodGroup.allCases[item.id ?? 0]
        self.presenter.onDidChangeBloodGroup(value: blood.name)
    }
}

// MARK: - AgoraVideoCallViewDelegate
extension CareViewController: AgoraVideoCallViewDelegate, TwilioVideoCallViewDelegate {
    func showJoinRoomVideoCallView(videoCall: VideoCallModel) {
        switch videoCall.typeVideoCall {
        case .agora:
            let controller = AgoraVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        case .twilio:
            let controller = TwilioVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        default:
            SKToast.shared.showToast(content: "Chức năng Video Call không hoạt động")
        }
    }
    
    func onEndCallAction(videoCall: VideoCallModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let id = videoCall?.appointment?.id {
                self.onShowAlertRateConsultationView(appointmentId: id)
            }
        }
    }
}
