//
//  CellTableViewPhr.swift
//  1SK
//
//  Created by vuongbachthu on 10/24/21.
//

import UIKit
import IQKeyboardManagerSwift

protocol CellTableViewPhrDelegate: AnyObject {
    func onSelectBloodGroupAction()
    func onTextFieldDidEndEditing(_ phr: PHR, value: String?)
}

class CellTableViewPhr: UITableViewCell {

    @IBOutlet weak var view_Weight: UIView!
    @IBOutlet weak var view_Height: UIView!
    @IBOutlet weak var view_Blood: UIView!
    @IBOutlet weak var view_Pressure: UIView!
    @IBOutlet weak var view_SPO2: UIView!
    @IBOutlet weak var view_Sugar: UIView!
    
    @IBOutlet weak var tf_Weight: UITextField!
    @IBOutlet weak var tf_Height: UITextField!
    @IBOutlet weak var tf_Blood: UITextField!
    @IBOutlet weak var tf_Pressure_0: UITextField!
    @IBOutlet weak var tf_Pressure_1: UITextField!
    @IBOutlet weak var tf_SPO2: UITextField!
    @IBOutlet weak var tf_Sugar: UITextField!
    
    weak var delegate: CellTableViewPhrDelegate?
    var litsPHRModel: [PHRModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.view_Weight.isHidden = true
        self.view_Height.isHidden = true
        self.view_Blood.isHidden = true
        self.view_Pressure.isHidden = true
        self.view_SPO2.isHidden = true
        self.view_Sugar.isHidden = true
        
        self.selectionStyle = .none
        
        self.setInitUITextField()
        self.setInitKeyboardToolBar()
        self.setDataPHRDefault()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.view_Weight.isHidden = true
        self.view_Height.isHidden = true
        self.view_Blood.isHidden = true
        self.view_Pressure.isHidden = true
        self.view_SPO2.isHidden = true
        self.view_Sugar.isHidden = true
    }
    
    func setDataPHRDefault() {
        let weight = PHRModel()
        weight.name = PHR.weight.name
        self.litsPHRModel.append(weight)
        
        let height = PHRModel()
        height.name = PHR.height.name
        self.litsPHRModel.append(height)
        
        let blood = PHRModel()
        blood.name = PHR.blood.name
        self.litsPHRModel.append(blood)
        
        let pressure = PHRModel()
        pressure.name = PHR.pressure.name
        self.litsPHRModel.append(pressure)
        
        let spo2 = PHRModel()
        spo2.name = PHR.spo2.name
        self.litsPHRModel.append(spo2)
        
        let sugar = PHRModel()
        sugar.name = PHR.sugar.name
        self.litsPHRModel.append(sugar)
        
    }
    
    func onSetStateData(listPHR: [PHRModel]) {
        for (index, item) in self.litsPHRModel.enumerated() {
            if let phr = listPHR.first(where: {$0.name == item.name}) {
                self.litsPHRModel[index] = phr
                
                switch item.name {
                case PHR.weight.name:
                    self.tf_Weight.text = phr.value
                    
                case PHR.height.name:
                    self.tf_Height.text = phr.value
                    
                case PHR.blood.name:
                    self.tf_Blood.text = phr.value
                    
                case PHR.pressure.name:
                    if let pressures = phr.value?.split(separator: "/") {
                        for (index, value) in pressures.enumerated() {
                            switch index {
                            case 0:
                                if value != "" {
                                    self.tf_Pressure_0.text = String(value)
                                }
                            case 1:
                                if value != "" {
                                    self.tf_Pressure_1.text = String(value)
                                }
                            default:
                                break
                            }
                        }
                    }
                    
                case PHR.spo2.name:
                    self.tf_SPO2.text = phr.value
                    
                case PHR.sugar.name:
                    self.tf_Sugar.text = phr.value
                    
                default:
                    break
                }
            }
        }
        
        switch listPHR.count {
        case 6:
            let pressure = listPHR.first(where: {$0.name == PHR.pressure.name})
            if pressure?.value?.count ?? 0 < 5 {
                self.view_Weight.isHidden = true
                self.view_Height.isHidden = true
                self.view_Blood.isHidden = false
                self.view_Pressure.isHidden = false
                self.view_SPO2.isHidden = true
                self.view_Sugar.isHidden = true
                
            } else {
                self.view_Weight.isHidden = false
                self.view_Height.isHidden = false
                self.view_Blood.isHidden = true
                self.view_Pressure.isHidden = true
                self.view_SPO2.isHidden = true
                self.view_Sugar.isHidden = true
            }
            
        case 5:
            self.setStateView(max: 1)
            self.view_Sugar.isHidden = false
            
        default:
            self.setStateView(max: 2)
        }
    }
    
    func setStateView(max: Int) {
        var isCountShow = 0
        for item in self.litsPHRModel {
            switch item.name {
            case PHR.weight.name:
                if item.id == nil {
                    self.view_Weight.isHidden = false
                    isCountShow += 1
                    print("setStateView weight")
                }
                
            case PHR.height.name:
                if item.id == nil {
                    self.view_Height.isHidden = false
                    isCountShow += 1
                    print("setStateView height")
                }
                
            case PHR.blood.name:
                if item.id == nil {
                    self.view_Blood.isHidden = false
                    isCountShow += 1
                    print("setStateView blood")
                }
                
            case PHR.pressure.name:
                if item.id == nil {
                    self.view_Pressure.isHidden = false
                    isCountShow += 1
                    print("setStateView pressure")
                    
                } else if item.value?.count ?? 0 < 5 {
                    self.view_Pressure.isHidden = false
                    isCountShow += 1
                    print("setStateView pressure")
                }
                
            case PHR.spo2.name:
                if item.id == nil {
                    self.view_SPO2.isHidden = false
                    isCountShow += 1
                    print("setStateView spo2")
                }
                
            case PHR.sugar.name:
                if item.id == nil {
                    self.view_Sugar.isHidden = false
                    isCountShow += 1
                    print("setStateView sugar")
                }
                
            default:
                break
            }
            
            if isCountShow == max {
                break
            }
        }
        
        print("setStateView ===================\n")
    }
}

// MARK: - UITextFieldDelegate
extension CellTableViewPhr: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Weight.delegate = self
        self.tf_Height.delegate = self
        self.tf_Blood.delegate = self
        self.tf_Pressure_0.delegate = self
        self.tf_Pressure_1.delegate = self
        self.tf_SPO2.delegate = self
        self.tf_Sugar.delegate = self
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == self.tf_Blood {
            self.delegate?.onSelectBloodGroupAction()
            return false
        }
        return true
    }
    
    func onValidatePHR(_ textField: UITextField, maxCount: Int) -> Int? {
        if textField.text?.count ?? 0 == 0 || textField.text?.count ?? 0 > maxCount {
            SKToast.shared.showToast(content: "Dữ liệu nhập không hợp lệ")
            return nil
        }
        if let value = Int(textField.text ?? "") {
            return value
        } else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không hợp lệ")
            return nil
        }
    }
    
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        switch textField {
//        case self.tf_Weight:
//            if self.onValidatePHR(textField, maxCount: 3) != nil {
//                self.delegate?.onTextFieldDidEndEditing(.weight, value: self.tf_Weight.text ?? "")
//            }
//
//        case self.tf_Height:
//            if self.onValidatePHR(textField, maxCount: 3) != nil {
//                self.delegate?.onTextFieldDidEndEditing(.height, value: self.tf_Height.text ?? "")
//            }
//
//        case self.tf_Blood:
//            // Picker View
//            break
//
//        case self.tf_Pressure_0:
//            if self.onValidatePHR(textField, maxCount: 3) != nil {
//                let text = String(format: "%@/%@", self.tf_Pressure_0.text ?? "", self.tf_Pressure_1.text ?? "")
//
//                if self.tf_Pressure_0.text?.count ?? 0 >= 2 && self.tf_Pressure_1.text?.count ?? 0 >= 2 {
//                    self.delegate?.onTextFieldDidEndEditing(.pressure, value: text)
//                }
//            }
//
//        case self.tf_Pressure_1:
//            if self.onValidatePHR(textField, maxCount: 3) != nil {
//                let text = String(format: "%@/%@", self.tf_Pressure_0.text ?? "", self.tf_Pressure_1.text ?? "")
//
//                if self.tf_Pressure_0.text?.count ?? 0 >= 2 && self.tf_Pressure_1.text?.count ?? 0 >= 2 {
//                    self.delegate?.onTextFieldDidEndEditing(.pressure, value: text)
//                }
//            }
//
//        case self.tf_SPO2:
//            if self.onValidatePHR(textField, maxCount: 3) != nil {
//                self.delegate?.onTextFieldDidEndEditing(.spo2, value: textField.text ?? "")
//            }
//
//        case self.tf_Sugar:
//            if self.onValidatePHR(textField, maxCount: 3) != nil {
//                self.delegate?.onTextFieldDidEndEditing(.sugar, value: textField.text ?? "")
//            }
//
//        default:
//            break
//        }
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.tf_Weight:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Height:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Blood:
            // Picker View
            return true
            
        case self.tf_Pressure_0:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Pressure_1:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_SPO2:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.tf_Sugar:
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        default:
            return true
        }
    }
}

// MARK: - InitKeyboardToolBar
extension CellTableViewPhr {
    private func setInitKeyboardToolBar() {
        let weight = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneWeightAction))
        self.tf_Weight.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số cân nặng", rightBarButtonConfiguration: weight)
        
        let height = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneHeightAction))
        self.tf_Height.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số chiều cao", rightBarButtonConfiguration: height)
        
        let pressure_0 = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDonePressure_0_Action))
        self.tf_Pressure_0.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số huyết áp", rightBarButtonConfiguration: pressure_0)
        
        let pressure_1 = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDonePressure_1_Action))
        self.tf_Pressure_1.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số huyết áp", rightBarButtonConfiguration: pressure_1)
        
        let spo2 = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneSpo2Action))
        self.tf_SPO2.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số SPO2", rightBarButtonConfiguration: spo2)
        
        let sugar = IQBarButtonItemConfiguration(title: "Lưu", action: #selector(self.onDoneSugarAction))
        self.tf_Sugar.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số đường huyết", rightBarButtonConfiguration: sugar)
    }
    
    @objc func onDoneWeightAction() {
        guard self.tf_Weight.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số cân nặng để tiếp tục")
            return
        }
        guard self.tf_Weight.text?.first != "0" && self.tf_Weight.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số cân nặng không đúng")
            return
        }
        guard let value = Int(self.tf_Weight.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số cân nặng không đúng")
            return
        }
        
        self.endEditing(true)
        self.delegate?.onTextFieldDidEndEditing(.weight, value: "\(value)")
    }
    
    @objc func onDoneHeightAction() {
        guard self.tf_Height.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số chiều cao để tiếp tục")
            return
        }
        guard self.tf_Height.text?.first != "0" && self.tf_Height.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số chiều cao không đúng")
            return
        }
        guard let value = Int(self.tf_Height.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số chiều cao không đúng")
            return
        }
        
        self.endEditing(true)
        self.delegate?.onTextFieldDidEndEditing(.height, value: "\(value)")
    }
    
    @objc func onDonePressure_0_Action() {
        self.onDonePressureAction()
    }
    
    @objc func onDonePressure_1_Action() {
        self.onDonePressureAction()
    }
    
    @objc func onDoneSpo2Action() {
        guard self.tf_SPO2.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số SPO2 để tiếp tục")
            return
        }
        guard self.tf_SPO2.text?.first != "0" && self.tf_SPO2.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số SPO2 không đúng")
            return
        }
        guard let value = Int(self.tf_SPO2.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số SPO2 không đúng")
            return
        }
        
        self.endEditing(true)
        self.delegate?.onTextFieldDidEndEditing(.spo2, value: "\(value)")
    }
    
    @objc func onDoneSugarAction() {
        guard self.tf_Sugar.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số đường huyết để tiếp tục")
            return
        }
        guard self.tf_Sugar.text?.first != "0" && self.tf_Sugar.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số đường huyết không đúng")
            return
        }
        guard let value = Int(self.tf_Sugar.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số đường huyết không đúng")
            return
        }
        
        self.endEditing(true)
        self.delegate?.onTextFieldDidEndEditing(.sugar, value: "\(value)")
    }
    
    func onDonePressureAction() {
        guard self.tf_Pressure_0.text?.count ?? 0 > 0 && self.tf_Pressure_1.text?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập chỉ số huyết áp để tiếp tục")
            return
        }
        guard self.tf_Pressure_0.text?.first != "0" && self.tf_Pressure_1.text?.first != "0"  else {
            SKToast.shared.showToast(content: "Nhập chỉ số huyết áp không đúng")
            return
        }
        guard self.tf_Pressure_0.text?.count ?? 0 >= 2 && self.tf_Pressure_1.text?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập chỉ số huyết áp không đúng")
            return
        }
        guard let value_0 = Int(self.tf_Pressure_0.text ?? ""), let value_1 = Int(self.tf_Pressure_1.text ?? "") else {
            SKToast.shared.showToast(content: "Nhập chỉ số huyết áp không đúng")
            return
        }
        
        let value = String(format: "%@/%@", "\(value_0)", "\(value_1)")
        self.endEditing(true)
        self.delegate?.onTextFieldDidEndEditing(.pressure, value: value)
    }
}
