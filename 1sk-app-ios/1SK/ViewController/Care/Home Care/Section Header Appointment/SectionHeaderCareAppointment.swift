//
//  SectionHeaderCareAppointment.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//

import UIKit

protocol SectionHeaderCareAppointmentDelegate: AnyObject {
    func onSeeMoreMyAppointment()
}

class SectionHeaderCareAppointment: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_TitleSection: UILabel!
    @IBOutlet weak var lbl_Count: UILabel!
    @IBOutlet weak var btn_More: UIButton!
    
    weak var delegate: SectionHeaderCareAppointmentDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setStateHeader(countAppointment: Int) {
        if countAppointment > 0 {
            self.lbl_Count.isHidden = false
//            self.btn_More.isHidden = false
            
            self.lbl_Count.text = countAppointment.asString
            
        } else {
            self.lbl_Count.isHidden = true
//            self.btn_More.isHidden = true
            
            self.lbl_Count.text = "0"
        }
    }

    @IBAction func onSeeMoreMyAppointment(_ sender: Any) {
        self.delegate?.onSeeMoreMyAppointment()
    }
}
