//
//  SectionHeaderNormalMore.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//

import UIKit

protocol SectionHeaderNormalMoreDelegate: AnyObject {
    func onSeeMoreHeaderNormalAction(section: Int)
}

class SectionHeaderNormalMore: UITableViewHeaderFooterView {
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_SeeMore: UIButton!
    
    weak var delegate: SectionHeaderNormalMoreDelegate?
    var section: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func config(title: String, isMore: Bool) {
        self.lbl_Title.text = title
        self.btn_SeeMore.isHidden = isMore
    }
    
    @IBAction func onSeeMoreAction(_ sender: Any) {
        self.delegate?.onSeeMoreHeaderNormalAction(section: self.section!)
    }
}
