//
//  SectionHeaderCareService.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//

import UIKit

protocol SectionHeaderCareServiceDelegate: AnyObject {
    func onHealthServiceAction()
}

class SectionHeaderCareService: UITableViewHeaderFooterView {

    weak var delegate: SectionHeaderCareServiceDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func onHealthServiceAction(_ sender: Any) {
        self.delegate?.onHealthServiceAction()
    }
    
}
