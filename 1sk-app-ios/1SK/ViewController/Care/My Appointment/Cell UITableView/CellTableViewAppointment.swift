//
//  CellTableViewAppointment.swift
//  1SK
//
//  Created by vuongbachthu on 9/22/21.
//

import UIKit

protocol CellTableViewAppointmentDelegate: AnyObject {
    func onJoinRoomCallAction(appointment: AppointmentModel)
}

class CellTableViewAppointment: UITableViewCell {

    @IBOutlet weak var lbl_Service: UILabel!
    @IBOutlet weak var lbl_Code: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var btn_Status: UIButton!
    
    @IBOutlet weak var view_JoinRoomCounseling: UIView!
    
    weak var delegate: CellTableViewAppointmentDelegate?
    var appointment: AppointmentModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        //
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(appointment: AppointmentModel) {
        self.appointment = appointment
        
        let specialist = appointment.doctor?.specialist?.first
        self.lbl_Service.text = "Tư vấn \(specialist?.name ?? "chuyên khoa") qua video"
        self.lbl_Code.text = appointment.code
        
        if let availableTime = appointment.doctorAvailableTime {
            let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.date ?? 0))
            let day = date.toString(Date.Format.eeedmySlash)
            
            let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.startAt ?? 0))
            let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.endAt ?? 0))
            let timeText = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
            self.lbl_Time.text = String(format: "%@ %@", timeText, day)
        }
        
        let status = appointment.status
        self.btn_Status.setTitle(status?.name, for: .normal)
        
        //print("config status id: \(status?.id ?? -1) - name: \(status?.name ?? "") - code: \(appointment.code ?? "")")
        
        switch status?.id {
        case AppointmentStatus.paying.id:
            self.btn_Status.backgroundColor = R.color.color_Bg_Orange()
            self.btn_Status.setTitleColor(R.color.color_Text_Orange_1(), for: .normal)
            self.view_JoinRoomCounseling.isHidden = true
            
            if gUser?.id == gUserIdReviewAppStore {
                self.btn_Status.setTitle("Đang xác nhận lịch hẹn", for: .normal)
            }
            
        case AppointmentStatus.confirming.id:
            self.btn_Status.backgroundColor = R.color.color_Bg_Orange()
            self.btn_Status.setTitleColor(R.color.color_Text_Orange_1(), for: .normal)
            self.view_JoinRoomCounseling.isHidden = true
            
        case AppointmentStatus.confirmed.id:
            self.btn_Status.backgroundColor = R.color.color_Bg_Green()
            self.btn_Status.setTitleColor(R.color.mainColor(), for: .normal)
            self.onUpdateStateButtonJoinRoom(appointment: appointment)
            
        case AppointmentStatus.cancel.id:
            self.btn_Status.backgroundColor = R.color.color_Bg_Red()
            self.btn_Status.setTitleColor(R.color.color_Text_Red_1(), for: .normal)
            self.view_JoinRoomCounseling.isHidden = true
            
        case AppointmentStatus.complete.id, AppointmentStatus.finish.id:
            self.btn_Status.setTitle("Tư vấn thành công", for: .normal)
            
            self.btn_Status.backgroundColor = R.color.color_Bg_Green()
            self.btn_Status.setTitleColor(R.color.mainColor(), for: .normal)
            self.view_JoinRoomCounseling.isHidden = true
            
        case AppointmentStatus.failure.id:
            self.btn_Status.backgroundColor = R.color.color_Bg_Red()
            self.btn_Status.setTitleColor(R.color.color_Text_Red_1(), for: .normal)
            self.view_JoinRoomCounseling.isHidden = true
            
        default:
            self.view_JoinRoomCounseling.isHidden = true
        }
    }
    
    private func onUpdateStateButtonJoinRoom(appointment: AppointmentModel?) {
        if let startAt = appointment?.doctorAvailableTime?.startAt {
            let currentAt = gTimeSystem ?? 0 //Utils.shared.getTimeSystemWidthStamp()
            let beforeAt = Int(startAt - gTimeDurationEndCall)
            if Int(currentAt) > beforeAt {
                self.view_JoinRoomCounseling.isHidden = false

            } else {
                self.view_JoinRoomCounseling.isHidden = true
            }
        } else {
            self.view_JoinRoomCounseling.isHidden = true
        }
    }
    
    @IBAction func onJoinRoomCallAction(_ sender: Any) {
        self.delegate?.onJoinRoomCallAction(appointment: self.appointment!)
    }
}
