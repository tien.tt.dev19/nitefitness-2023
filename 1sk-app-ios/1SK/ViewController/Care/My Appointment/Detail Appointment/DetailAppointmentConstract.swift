//
//  
//  DetailAppointmentConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

// MARK: - View
protocol DetailAppointmentViewProtocol: AnyObject {
    func setDataView(appointment: AppointmentModel?)
    func onUpdateStateButtonJoinRoom(appointment: AppointmentModel?)
    func onJoinRoomActive(videoCall: VideoCallModel?)
    func setPaymentPeriodTime(paymentPeriod: PaymentPeriodModel)
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?)
}

// MARK: - Presenter
protocol DetailAppointmentPresenterProtocol {
    func onViewDidLoad()
    
    //Action
    func onAlertAgreeAction()
    func onCopyBankNumberAction()
    func onCopyBankContentAction()
    func onJoinRoomCallAction()
    func onBackNavAction()
    
    //Value
    func getAppointmentStatusId() -> Int?
    func getIsFromBooking() -> Bool?
    func getAppointment() -> AppointmentModel?
    func getAppointmentPrice() -> Int?
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?)
}

// MARK: - Interactor Input
protocol DetailAppointmentInteractorInputProtocol {
    func setCancelAppointment(appointment: AppointmentModel)
    func getTokenAppointmentRoom(appointment: AppointmentModel)
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel)
    func getPaymentPeriodTime()
}

// MARK: - Interactor Output
protocol DetailAppointmentInteractorOutputProtocol: AnyObject {
    func onSetCancelAppointmentFinished(with result: Result<AppointmentModel, APIError>)
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?)
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?)
    func onGetPaymentPeriodTimeFinished(with result: Result<PaymentPeriodModel, APIError>)
}

// MARK: - Router
protocol DetailAppointmentRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func popRootViewController()
    func alertDefault(title: String, message: String)
}
