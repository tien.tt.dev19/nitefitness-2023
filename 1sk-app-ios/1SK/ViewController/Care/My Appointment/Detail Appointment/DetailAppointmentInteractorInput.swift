//
//  
//  DetailAppointmentInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

class DetailAppointmentInteractorInput {
    weak var output: DetailAppointmentInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - DetailAppointmentInteractorInputProtocol
extension DetailAppointmentInteractorInput: DetailAppointmentInteractorInputProtocol {
    func setCancelAppointment(appointment: AppointmentModel) {
        self.careService?.setCancelAppointment(appointment: appointment, completion: { [weak self] result in
            self?.output?.onSetCancelAppointmentFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getTokenAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenRenewAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenRenewAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
    
    func getPaymentPeriodTime() {
        self.careService?.getPaymentPeriodTime(completion: { [weak self] result in
            self?.output?.onGetPaymentPeriodTimeFinished(with: result.unwrapSuccessModel())
        })
    }
}
