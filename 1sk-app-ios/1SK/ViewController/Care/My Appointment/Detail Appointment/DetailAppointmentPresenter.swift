//
//  
//  DetailAppointmentPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

protocol DetailAppointmentPresenterDelegate: AnyObject {
    func onCancelAppointmentSuccess(appointment: AppointmentModel)
    func onAppointmentChangeStatus(appointment: AppointmentModel)
}

class DetailAppointmentPresenter {

    weak var view: DetailAppointmentViewProtocol?
    private var interactor: DetailAppointmentInteractorInputProtocol
    private var router: DetailAppointmentRouterProtocol
    weak var delegate: DetailAppointmentPresenterDelegate?
    
    init(interactor: DetailAppointmentInteractorInputProtocol,
         router: DetailAppointmentRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var isFromBooking: Bool?
    var appointment: AppointmentModel?

}

// MARK: - DetailAppointmentPresenterProtocol
extension DetailAppointmentPresenter: DetailAppointmentPresenterProtocol {
    func onViewDidLoad() {
        self.view?.setDataView(appointment: self.appointment)
        self.interactor.getPaymentPeriodTime()
        
        self.setInitHandleSocketEvent()
    }
    
    //Action
    func onAlertAgreeAction() {
        self.router.hideHud()
        self.interactor.setCancelAppointment(appointment: self.appointment!)
    }
    
    func onCopyBankNumberAction() {
        if let bankAccountNumber = self.appointment?.bank?.accountNumber {
            let pasteboard = UIPasteboard.general
            pasteboard.string = bankAccountNumber.replaceCharacter(target: " ", withString: "")
            SKToast.shared.showToast(content: "Đã sao chép số tài khoản: \(pasteboard.string ?? "") \n\nMở app NGÂN HÀNG và dán vào mục nhập số tài khoản để tiếp tục")
        }
    }
    
    func onCopyBankContentAction() {
        if let transferContent = self.appointment?.code {
            let pasteboard = UIPasteboard.general
            pasteboard.string = "1SK CARE \(transferContent)"
            SKToast.shared.showToast(content: "Đã sao chép nội dung chuyển khoản: \(pasteboard.string ?? "") \n\nMở app NGÂN HÀNG và dán vào mục nội dung chuyển khoản để tiếp tục")
        }
    }
    
    func onJoinRoomCallAction() {
        self.router.showHud()
        self.interactor.getTokenAppointmentRoom(appointment: self.appointment!)
    }
    
    func onBackNavAction() {
        if self.isFromBooking == true {
            self.router.popRootViewController()
        } else {
            self.router.popViewController()
        }
    }
    
    // Value
    func getAppointmentStatusId() -> Int? {
        return self.appointment?.status?.id
    }
    
    func getAppointmentPrice() -> Int? {
        return self.appointment?.doctor?.cost?.costPerHour
    }
    
    func getIsFromBooking() -> Bool? {
        return self.isFromBooking
    }
    
    func getAppointment() -> AppointmentModel? {
        return self.appointment
    }
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        guard let `appointment` = appointment else {
            return
        }
        self.router.showHud()
        self.interactor.getTokenRenewAppointmentRoom(appointment: appointment)
    }
}

// MARK: - DetailAppointmentInteractorOutput 
extension DetailAppointmentPresenter: DetailAppointmentInteractorOutputProtocol {
    func onSetCancelAppointmentFinished(with result: Result<AppointmentModel, APIError>) {
        switch result {
        case .success(let appointmentModel):
            self.appointment?.status = appointmentModel.status
            self.appointment?.cancellationReason = appointment?.cancellationReason
            self.view?.setDataView(appointment: self.appointment)
            SKToast.shared.showToast(content: "Đã hủy lịch thành công")
            self.delegate?.onCancelAppointmentSuccess(appointment: self.appointment!)
            
        case .failure:
            SKToast.shared.showToast(content: "Lịch hẹn này đã hết thời gian hủy")
        }
        self.router.hideHud()
    }
    
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?) {
        if meta?.code == 10002 {
            self.view?.onShowAlertRenewToken(meta: meta, appointment: appointment)
            
        } else {
            switch result {
            case .success(let model):
                let room = model
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
            case .failure(let error):
                let room = VideoCallModel()
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
                self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
            }
        }
        self.router.hideHud()
    }
    
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?) {
        switch result {
        case .success(let model):
            let room = model
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
        case .failure(let error):
            let room = VideoCallModel()
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
            self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
        }
        self.router.hideHud()
    }
    
    func onGetPaymentPeriodTimeFinished(with result: Result<PaymentPeriodModel, APIError>) {
        switch result {
        case .success(let model):
            self.appointment?.paymentPeriod = model
            self.view?.setPaymentPeriodTime(paymentPeriod: model)
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}

// MARK: SocketEvent
extension DetailAppointmentPresenter {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentShowButtonJoinEvent), name: .AppointmentShowButtonJoinEvent, object: nil)
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentStatusChangedEvent DetailAppointmentPresenter")
        guard let appointment = notification.object as? AppointmentModel else {
            return
        }
        guard let status = appointment.status else {
            return
        }
        self.appointment?.status = status
        if let reason = appointment.cancellationReason {
            self.appointment?.cancellationReason = reason
        }
        self.view?.setDataView(appointment: self.appointment)
        self.delegate?.onAppointmentChangeStatus(appointment: self.appointment!)
    }
    
    @objc func onAppointmentShowButtonJoinEvent(notification: NSNotification) {
        print("socket.on care: AppointmentShowButtonJoinEvent DetailAppointmentPresenter")
        self.view?.onUpdateStateButtonJoinRoom(appointment: self.appointment)
    }
}
