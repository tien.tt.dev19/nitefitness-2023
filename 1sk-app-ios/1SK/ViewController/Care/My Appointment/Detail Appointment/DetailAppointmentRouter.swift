//
//  
//  DetailAppointmentRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit

class DetailAppointmentRouter: BaseRouter {
//    weak var viewController: DetailAppointmentViewController?
    static func setupModule(width appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?, isFromBooking: Bool?) -> DetailAppointmentViewController {
        let viewController = DetailAppointmentViewController()
        let router = DetailAppointmentRouter()
        let interactorInput = DetailAppointmentInteractorInput()
        let presenter = DetailAppointmentPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.appointment = appointment
        presenter.delegate = delegate
        presenter.isFromBooking = isFromBooking
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - DetailAppointmentRouterProtocol
extension DetailAppointmentRouter: DetailAppointmentRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func popRootViewController() {
        self.viewController?.navigationController?.popToRootViewController(animated: true)
    }
    
    func alertDefault(title: String, message: String) {
        self.viewController?.alertDefault(title: title, message: message)
    }
}
