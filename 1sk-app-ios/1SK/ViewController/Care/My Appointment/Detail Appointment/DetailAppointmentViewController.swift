//
//  
//  DetailAppointmentViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/26/21.
//
//

import UIKit
import Photos

class DetailAppointmentViewController: BaseViewController {

    var presenter: DetailAppointmentPresenterProtocol!
    
    @IBOutlet weak var view_NavigationBar: UIView!
    @IBOutlet weak var lbl_NavigationTitle: UILabel!
    @IBOutlet weak var btn_NavigationItemMore: UIButton!
    
    @IBOutlet weak var lbl_Service: UILabel!
    @IBOutlet weak var lbl_Doctor: UILabel!
    @IBOutlet weak var lbl_Code: UILabel!
    
    @IBOutlet weak var lbl_TitleTime: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    
    @IBOutlet weak var lbl_TitleInfo: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Birthday: UILabel!
    @IBOutlet weak var lbl_Gender: UILabel!
    @IBOutlet weak var lbl_PhoneNumber: UILabel!
    
    @IBOutlet weak var lbl_TitleReason: UILabel!
    @IBOutlet weak var lbl_Reason: UILabel!
    @IBOutlet weak var lbl_Note: UILabel!
    
    @IBOutlet weak var view_CancelReason: UIView!
    @IBOutlet weak var lbl_CancelReason: UILabel!
    
    @IBOutlet weak var view_Status_Paying: UIStackView!
    @IBOutlet weak var view_Status_Confirming: UIView!
    @IBOutlet weak var view_Status_Confirmed: UIView!
    @IBOutlet weak var view_Status_Cancel: UIView!
    @IBOutlet weak var view_Status_Success: UIView!
    @IBOutlet weak var view_Status_NotSuccess: UIView!
    
    @IBOutlet weak var lbl_Status_Paying: UILabel!
    @IBOutlet weak var lbl_PriceTotal: UILabel!
    @IBOutlet weak var lbl_PaymentMethod: UILabel!
    
    @IBOutlet weak var lbl_BankNumber: UILabel!
    @IBOutlet weak var lbl_BankName: UILabel!
    @IBOutlet weak var lbl_BankBranch: UILabel!
    @IBOutlet weak var lbl_BankHolderName: UILabel!
    @IBOutlet weak var lbl_BankContent: UILabel!
    
    @IBOutlet weak var stack_PaymentMethod: UIStackView!
    
    @IBOutlet weak var lbl_Status_Confirming: UILabel!
    @IBOutlet weak var lbl_Status_Confirmed: UILabel!
    @IBOutlet weak var lbl_Status_Cancel: UILabel!
    @IBOutlet weak var lbl_Status_Success: UILabel!
    @IBOutlet weak var lbl_Status_NotSuccess: UILabel!
    
    @IBOutlet weak var lbl_Note_Confirming: UILabel!
    
    @IBOutlet weak var lbl_ReasonNotSuccess: UILabel!
    
    @IBOutlet weak var btn_JoinRoomCounseling: UIButtonRoundBlue!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let isFromBooking = self.presenter.getIsFromBooking()
        if isFromBooking == true {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        let isFromBooking = self.presenter.getIsFromBooking()
        if isFromBooking == true {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        }
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitNavigationBar()
        self.setInitLayoutView()
    }
    
    func setInitLayoutView() {
        if gUser?.id == gUserIdReviewAppStore {
            self.stack_PaymentMethod.isHidden = true
            self.lbl_Note.isHidden = true
        }
        
        let atrTitleTime = NSMutableAttributedString(string: "Thời gian tư vấn ", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)])
        atrTitleTime.append(NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_TitleTime.attributedText = atrTitleTime
        
        let atrTitleInfo = NSMutableAttributedString(string: "Thông tin bệnh nhân ", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)])
        atrTitleInfo.append(NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_TitleInfo.attributedText = atrTitleInfo
        
        let atrTitleReason = NSMutableAttributedString(string: "Lý do khám ", attributes: [.foregroundColor: R.color.color_Dark_Text()!, .font: UIFont.boldSystemFont(ofSize: 16)])
        atrTitleReason.append(NSAttributedString(string: "*", attributes: [.foregroundColor: UIColor.red, .font: UIFont.boldSystemFont(ofSize: 16)]))
        self.lbl_TitleReason.attributedText = atrTitleReason
    }
    
    // MARK: - Action
    @IBAction func onJoinRoomCallAction(_ sender: Any) {
        self.onCheckAuthorizationStatus()
    }
    
    @IBAction func onCopyBankNumberAction(_ sender: Any) {
        self.presenter.onCopyBankNumberAction()
    }
    
    @IBAction func onCopyBankContentAction(_ sender: Any) {
        self.presenter.onCopyBankContentAction()
    }
    
    @IBAction func onBackNavAction(_ sender: Any) {
        self.presenter.onBackNavAction()
    }
}

// MARK: - AVCaptureDevice
extension DetailAppointmentViewController {
    func onCheckAuthorizationStatus() {
        guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized &&  AVCaptureDevice.authorizationStatus(for: .audio) == .authorized else {
            self.onCheckAuthorizationVideo()
            return
        }
        self.presenter.onJoinRoomCallAction()
    }
    
    func onCheckAuthorizationVideo() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.onCheckAuthorizationAudio()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    self.onCheckAuthorizationAudio()
                    
                } else {
                    //access denied
                    if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Camera để có thể tiếp tục")
                        }
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
                    }
                }
            })
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
        }
    }
    
    func onCheckAuthorizationAudio() {
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
        case .authorized:
            DispatchQueue.main.async {
                self.presenter.onJoinRoomCallAction()
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .audio) { granted in
                if granted == true {
                   // User granted
                    DispatchQueue.main.async {
                        self.presenter.onJoinRoomCallAction()
                    }
                    
                } else {
                    // User denied
                    if AVCaptureDevice.authorizationStatus(for: .audio) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Microphone để có thể tiếp tục")
                        }
                        
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
                        message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
                    }
                }
            }
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
        }
    }
}

// MARK: - Navigation Bar
extension DetailAppointmentViewController {
    func setInitNavigationBar() {
        self.view_NavigationBar.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.lbl_NavigationTitle.text = "Chi tiết lịch hẹn"
        self.btn_NavigationItemMore.addTarget(self, action: #selector(self.onBarButtonItemMoreAction), for: .touchUpInside)
    }
    @objc func onBarButtonItemMoreAction() {
        let item = ItemMoreDropdown.init(index: 0, name: "Hủy lịch hẹn", isHiddenLine: true)
        let controller = AlertMoreDropdownViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.listItemMoreDropdown = [item]
        self.navigationController?.present(controller, animated: false)
    }
    
    func onCheckStatusButtonMore() {
        let statusId = self.presenter.getAppointmentStatusId()
        switch statusId {
        case AppointmentStatus.paying.id, AppointmentStatus.confirming.id:
            self.btn_NavigationItemMore.isHidden = false
            
        case AppointmentStatus.confirmed.id:
            let appointment = self.presenter.getAppointment()
            if let startAt = appointment?.doctorAvailableTime?.startAt {
                let currentAt = Utils.shared.getTimeSystemWidthStamp()
                let beforeAt = Int(startAt - gTimeDurationEndCall)
                
                if Int(currentAt) < beforeAt {
                    self.btn_NavigationItemMore.isHidden = false
                    
                } else {
                    self.btn_NavigationItemMore.isHidden = true
                }
            } else {
                self.btn_NavigationItemMore.isHidden = true
            }
            
        default:
            self.btn_NavigationItemMore.isHidden = true
        }
    }
}

// MARK: - AlertMoreDropdownViewDelegate
extension DetailAppointmentViewController: AlertMoreDropdownViewDelegate {
    func onDidSelectItemMore(item: ItemMoreDropdown) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.showAlertConfirmActionView()
        }
    }
}

// MARK: - AlertConfirmActionViewDelegate
extension DetailAppointmentViewController: AlertSheetConfirmIconViewDelegate {
    func showAlertConfirmActionView() {
        let controller = AlertSheetConfirmIconViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        let statusId = self.presenter.getAppointmentStatusId()
        switch statusId {
        case AppointmentStatus.paying.id:
            controller.alertContent = "Bạn có chắc chắn muốn hủy lịch hẹn tư vấn này?"
            controller.alertImage = R.image.ic_calendar_cancel()
            
        case AppointmentStatus.confirming.id, AppointmentStatus.confirmed.id:
            if let costPerHour = self.presenter.getAppointmentPrice(), costPerHour > 0 {
                controller.alertContent = "Hủy lịch hẹn sẽ không được hoàn phí tư vấn. Bạn có chắc chắn muốn hủy?"
            } else {
                controller.alertContent = "Bạn có chắc chắn muốn hủy lịch hẹn tư vấn này?"
            }
            controller.alertImage = R.image.ic_calendar_cancel()
            
        default:
            break
        }
        self.navigationController?.present(controller, animated: true)
    }
    
    func onAlertConfirmCancelAction() {
        //
    }
    
    func onAlertConfirmAgreeAction() {
        self.presenter.onAlertAgreeAction()
    }
}

// MARK: - DetailAppointmentViewProtocol
extension DetailAppointmentViewController: DetailAppointmentViewProtocol {
    func setDataView(appointment: AppointmentModel?) {
        let specialist = appointment?.doctor?.specialist?.first
        self.lbl_Service.text = "Tư vấn \(specialist?.name ?? "chuyên khoa") qua video"
        
        self.lbl_Doctor.text = appointment?.doctor?.basicInformation?.fullName
        self.lbl_Code.text = appointment?.code
        
        if let availableTime = appointment?.doctorAvailableTime {
            let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.date ?? 0))
            let startDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.startAt ?? 0))
            let endDate = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(availableTime.endAt ?? 0))
            self.lbl_Time.text = String(format: "%02i:%02i - %02i:%02i", startDate.hour, startDate.minute, endDate.hour, endDate.minute)
            self.lbl_Date.text = date.toString(Date.Format.dmySlash)
        }
        
        self.lbl_Name.text = appointment?.patientName
        self.lbl_Birthday.text = appointment?.patientBirthYear
        self.lbl_Gender.text = appointment?.patientGender
        self.lbl_PhoneNumber.text = appointment?.patientPhone
        
        self.lbl_Reason.text = appointment?.patientSymptom
        
        if let cancelReason = appointment?.cancellationReason {
            self.lbl_CancelReason.text = cancelReason
            self.view_CancelReason.isHidden = false
            
        } else {
            self.view_CancelReason.isHidden = true
        }
        
        let status = appointment?.status
        switch status?.id {
        case AppointmentStatus.paying.id:
            if let costPerHour = appointment?.doctor?.cost?.costPerHour, costPerHour > 0 {
                self.lbl_Status_Paying.text = status?.name
                self.view_Status_Paying.isHidden = false
                self.view_Status_Confirming.isHidden = true
                self.view_Status_Confirmed.isHidden = true
                self.view_Status_Cancel.isHidden = true
                self.view_Status_Success.isHidden = true
                self.view_Status_NotSuccess.isHidden = true

            } else {
                self.lbl_Status_Confirming.text = status?.name
                self.lbl_Note_Confirming.text = "Lịch hẹn đang chờ xác nhận"
                self.view_Status_Paying.isHidden = true
                self.view_Status_Confirming.isHidden = false
                self.view_Status_Confirmed.isHidden = true
                self.view_Status_Cancel.isHidden = true
                self.view_Status_Success.isHidden = true
                self.view_Status_NotSuccess.isHidden = true
            }
            
            if gUser?.id == gUserIdReviewAppStore {
                self.lbl_Status_Paying.text = "Đang xác nhận lịch hẹn"
            }
            
        case AppointmentStatus.confirming.id:
            self.lbl_Status_Confirming.text = status?.name
            self.view_Status_Paying.isHidden = true
            self.view_Status_Confirming.isHidden = false
            self.view_Status_Confirmed.isHidden = true
            self.view_Status_Cancel.isHidden = true
            self.view_Status_Success.isHidden = true
            self.view_Status_NotSuccess.isHidden = true
            
        case AppointmentStatus.confirmed.id:
            self.lbl_Status_Confirmed.text = status?.name
            self.view_Status_Paying.isHidden = true
            self.view_Status_Confirming.isHidden = true
            self.view_Status_Confirmed.isHidden = false
            self.view_Status_Cancel.isHidden = true
            self.view_Status_Success.isHidden = true
            self.view_Status_NotSuccess.isHidden = true
            
        case AppointmentStatus.cancel.id, AppointmentStatus.failure.id:
            self.lbl_Status_Cancel.text = status?.name
            self.view_Status_Paying.isHidden = true
            self.view_Status_Confirming.isHidden = true
            self.view_Status_Confirmed.isHidden = true
            self.view_Status_Cancel.isHidden = false
            self.view_Status_Success.isHidden = true
            self.view_Status_NotSuccess.isHidden = true
            
        case AppointmentStatus.complete.id, AppointmentStatus.finish.id:
            self.lbl_Status_Success.text = "Tư vấn thành công"
            self.view_Status_Paying.isHidden = true
            self.view_Status_Confirming.isHidden = true
            self.view_Status_Confirmed.isHidden = true
            self.view_Status_Cancel.isHidden = true
            self.view_Status_Success.isHidden = false
            self.view_Status_NotSuccess.isHidden = true
            
        default:
            self.view_Status_Paying.isHidden = true
            self.view_Status_Confirming.isHidden = true
            self.view_Status_Confirmed.isHidden = true
            self.view_Status_Cancel.isHidden = true
            self.view_Status_Success.isHidden = true
            self.view_Status_NotSuccess.isHidden = true
        }
        
//        if let costPerHour = appointment?.doctor?.cost?.costPerHour, costPerHour > 0 {
//            self.lbl_PriceTotal.text = "\(costPerHour.formatNumber())đ"
//        } else {
//            self.lbl_PriceTotal.text = "Miễn phí"
//            self.stack_PaymentMethod.isHidden = true
//            self.lbl_Note.isHidden = true
//        }
        
        if let priceTotal = appointment?.salePrice, priceTotal > 0 {
            self.lbl_PriceTotal.text = "\(priceTotal.formatNumber())đ"
            
        } else {
            self.lbl_PriceTotal.text = "Miễn phí"
            self.stack_PaymentMethod.isHidden = true
            self.lbl_Note.isHidden = true
        }
        
        self.lbl_PaymentMethod.text = "Chuyển khoản"
        
        self.lbl_BankNumber.text = appointment?.bank?.accountNumber
        self.lbl_BankName.text = appointment?.bank?.bankName ?? ""
        self.lbl_BankBranch.text = appointment?.bank?.bankBranchName ?? ""
        self.lbl_BankHolderName.text = appointment?.bank?.accountName ?? ""
        self.lbl_BankContent.text = "Nội dung: 1SK CARE \(appointment?.code ?? "")"
        
        self.onCheckStatusButtonMore()
        self.onUpdateStateButtonJoinRoom(appointment: appointment)
    }
    
    func onUpdateStateButtonJoinRoom(appointment: AppointmentModel?) {
        if let startAt = appointment?.doctorAvailableTime?.startAt {
            let currentAt = gTimeSystem ?? 0 //Utils.shared.getTimeSystemWidthStamp()
            let beforeAt = Int(startAt - gTimeDurationEndCall)

            print("onUpdateStateButtonJoinRoom startAt", Utils.shared.getTimeStringByTimeStamp(timeStamp: Double(startAt), timeFomat: Date.Format.hmdmy.rawValue))
            print("onUpdateStateButtonJoinRoom")

            print("onUpdateStateButtonJoinRoom currentAt", currentAt)
            print("onUpdateStateButtonJoinRoom currentAt", Utils.shared.getTimeStringByTimeStamp(timeStamp: Double(currentAt), timeFomat: Date.Format.hmdmy.rawValue))
            print("onUpdateStateButtonJoinRoom")

            print("onUpdateStateButtonJoinRoom beforeAt", Utils.shared.getTimeStringByTimeStamp(timeStamp: Double(beforeAt), timeFomat: Date.Format.hmdmy.rawValue))
            print("onUpdateStateButtonJoinRoom")

            if currentAt > beforeAt {
                print("onUpdateStateButtonJoinRoom startAt < beforeAt")
                self.btn_JoinRoomCounseling.isHidden = false

            } else {
                print("onUpdateStateButtonJoinRoom else")
                self.btn_JoinRoomCounseling.isHidden = true
            }
        } else {
            print("onUpdateStateButtonJoinRoom startAt nil")
            self.btn_JoinRoomCounseling.isHidden = true
        }
    }
    
    func setPaymentPeriodTime(paymentPeriod: PaymentPeriodModel) {
        let appointment = self.presenter.getAppointment()
        if let costPerHour = appointment?.doctor?.cost?.costPerHour, costPerHour > 0 {
            self.lbl_Note.text = "(*) Lưu ý: Sau \(paymentPeriod.appointmentPaymentPeriod ?? 0) phút từ khi đặt lịch, nếu bạn chưa thanh toán, lịch hẹn sẽ tự động hủy"
        } else {
            self.lbl_Note.text = "(*) Lưu ý: Sau khi xác nhận lịch hẹn, chúng tôi sẽ gửi thông báo cho bạn"
        }
    }
    
    func onJoinRoomActive(videoCall: VideoCallModel?) {
        if videoCall?.appId != nil && videoCall?.channelName != nil && videoCall?.token != nil {
            self.showJoinRoomVideoCallView(videoCall: videoCall!)
            
        } else {
            self.onShowAlertWarningAppointment(videoCall: videoCall)
        }
    }
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?) {
        self.showAlertRenewTokenCallViewController(title: "Cuộc gọi tư vấn", content: meta?.message ?? "Bạn đang tham gia tư vấn online bằng thiết bị khác", appointment: appointment, delegate: self)
    }
}

// MARK: AlertRenewTokenCallViewDelegate
extension DetailAppointmentViewController: AlertRenewTokenCallViewDelegate {
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        self.presenter.onAgreeAlertRenewTokenCallAction(appointment: appointment)
    }
}

// MARK: - VideoCallViewDelegate
extension DetailAppointmentViewController: AgoraVideoCallViewDelegate, TwilioVideoCallViewDelegate {
    func showJoinRoomVideoCallView(videoCall: VideoCallModel) {
        switch videoCall.typeVideoCall {
        case .agora:
            let controller = AgoraVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        case .twilio:
            let controller = TwilioVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        default:
            SKToast.shared.showToast(content: "Chức năng Video Call không hoạt động")
        }
    }
    
    func onEndCallAction(videoCall: VideoCallModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let id = videoCall?.appointment?.id {
                self.onShowAlertRateConsultationView(appointmentId: id)
            }
        }
    }
}
