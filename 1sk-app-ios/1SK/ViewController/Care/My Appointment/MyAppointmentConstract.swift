//
//  
//  MyAppointmentConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

// MARK: - View
protocol MyAppointmentViewProtocol: AnyObject {
    
}

// MARK: - Presenter
protocol MyAppointmentPresenterProtocol {
    func onViewDidLoad()
    
    // Action
    func onHistoryAction()
}

// MARK: - Interactor Input
protocol MyAppointmentInteractorInputProtocol {
    
}

// MARK: - Interactor Output
protocol MyAppointmentInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol MyAppointmentRouterProtocol: BaseRouterProtocol {
    func gotoHistoryConsulting()
}
