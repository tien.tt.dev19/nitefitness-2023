//
//  
//  MyAppointmentInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MyAppointmentInteractorInput {
    weak var output: MyAppointmentInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - MyAppointmentInteractorInputProtocol
extension MyAppointmentInteractorInput: MyAppointmentInteractorInputProtocol {
    
}
