//
//  
//  MyAppointmentPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MyAppointmentPresenter {

    weak var view: MyAppointmentViewProtocol?
    private var interactor: MyAppointmentInteractorInputProtocol
    private var router: MyAppointmentRouterProtocol

    init(interactor: MyAppointmentInteractorInputProtocol,
         router: MyAppointmentRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - MyAppointmentPresenterProtocol
extension MyAppointmentPresenter: MyAppointmentPresenterProtocol {
    func onViewDidLoad() {
        
    }
    
    // Action
    func onHistoryAction() {
        self.router.gotoHistoryConsulting()
    }
    
}

// MARK: - MyAppointmentInteractorOutput 
extension MyAppointmentPresenter: MyAppointmentInteractorOutputProtocol {
}
