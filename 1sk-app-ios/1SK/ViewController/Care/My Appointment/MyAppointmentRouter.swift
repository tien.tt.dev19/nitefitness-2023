//
//  
//  MyAppointmentRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MyAppointmentRouter: BaseRouter {
//    weak var viewController: MyAppointmentViewController?
    static func setupModule() -> MyAppointmentViewController {
        let viewController = MyAppointmentViewController()
        let router = MyAppointmentRouter()
        let interactorInput = MyAppointmentInteractorInput()
        let presenter = MyAppointmentPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - MyAppointmentRouterProtocol
extension MyAppointmentRouter: MyAppointmentRouterProtocol {
    func gotoHistoryConsulting() {
        let controller = HistoryConsultingRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
}
