//
//  
//  MyAppointmentViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/23/21.
//
//

import UIKit

class MyAppointmentViewController: BaseViewController {

    var presenter: MyAppointmentPresenterProtocol!
    
//    @IBOutlet weak var tbv_TableView: UITableView!
    
    
    @IBOutlet weak var view_Container: UIView!
    @IBOutlet weak var btn_PageUpcoming: UIButton!
    @IBOutlet weak var btn_PageHistory: UIButton!
    @IBOutlet weak var view_LinePageUpcoming: UIView!
    @IBOutlet weak var view_LinePageHistory: UIView!
    
    private lazy var pageUpcomingVC = PageUpcomingAppointmentRouter.setupModule()
    private lazy var pageHistoryVC = PageHistoryAppointmentRouter.setupModule()
    
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitNavigationBar()
//        self.setInitTableView()
//        self.setInitRefreshControl()
        
        self.setInitPageView()
    }
    
}

// MARK: - Navigation Bar
extension MyAppointmentViewController {
    func setInitNavigationBar() {
        self.title = "Lịch hẹn của tôi"
        let btnHistory = UIBarButtonItem(image: R.image.ic_history(), style: .plain, target: self, action: #selector(self.onHistoryAction))
        self.navigationItem.setRightBarButton(btnHistory, animated: true)
    }
    
    @objc func onHistoryAction() {
        self.presenter.onHistoryAction()
    }
}

// MARK: - Page Controller
extension MyAppointmentViewController {
    private func setInitPageView() {
        self.btn_PageUpcoming.setTitleColor(R.color.color_Gray_Text(), for: .normal)
        self.btn_PageUpcoming.setTitleColor(R.color.color_Dark_Text(), for: .selected)
        self.btn_PageUpcoming.tintColor = .clear
        
        self.btn_PageHistory.setTitleColor(R.color.color_Gray_Text(), for: .normal)
        self.btn_PageHistory.setTitleColor(R.color.color_Dark_Text(), for: .selected)
        self.btn_PageHistory.tintColor = .clear
        
        self.view_LinePageUpcoming.alpha = 1.0
        self.view_LinePageHistory.alpha = 0.0
        
        self.btn_PageUpcoming.isSelected = true
        self.btn_PageHistory.isSelected = false
        self.btn_PageUpcoming.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
        self.btn_PageHistory.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
        
        self.btn_PageUpcoming.addTarget(self, action: #selector(self.onSelectPage_0), for: .touchUpInside)
        self.btn_PageHistory.addTarget(self, action: #selector(self.onSelectPage_1), for: .touchUpInside)
        
        self.addChildView(childViewController: self.pageUpcomingVC)
    }
    
    @objc func onSelectPage_0() {
        self.setPageBarState(page: 0)
        self.addChildView(childViewController: self.pageUpcomingVC)
    }
    
    @objc func onSelectPage_1() {
        self.setPageBarState(page: 1)
        self.addChildView(childViewController: self.pageHistoryVC)
    }
    
    func setPageBarState(page: Int) {
        switch page {
        case 0:
            self.btn_PageUpcoming.isSelected = true
            self.btn_PageHistory.isSelected = false
            self.btn_PageUpcoming.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
            self.btn_PageHistory.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
            
            UIView.animate(withDuration: 0.3) {
                self.view_LinePageUpcoming.alpha = 1.0
                self.view_LinePageHistory.alpha = 0.0
                self.view.layoutIfNeeded()
            }
            
        case 1:
            self.btn_PageUpcoming.isSelected = false
            self.btn_PageHistory.isSelected = true
            self.btn_PageUpcoming.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
            self.btn_PageHistory.titleLabel?.font = UIFont.systemFont(ofSize: 16.0, weight: .semibold)
            
            UIView.animate(withDuration: 0.3) {
                self.view_LinePageUpcoming.alpha = 0.0
                self.view_LinePageHistory.alpha = 1.0
                self.view.layoutIfNeeded()
            }
            
        default:
            break
        }
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: - MyAppointmentViewProtocol
extension MyAppointmentViewController: MyAppointmentViewProtocol {
    
}
