//
//  
//  PageHistoryAppointmentConstract.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

// MARK: - View
protocol PageHistoryAppointmentViewProtocol: AnyObject {
    func onReloadData()
    func onReloadData(section: Int)
    func onJoinRoomActive(videoCall: VideoCallModel?)
    
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func onSetNotFoundView(isHidden: Bool)
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?)
}

// MARK: - Presenter
protocol PageHistoryAppointmentPresenterProtocol {
    func onViewDidLoad()
    
    // Action
    func onJoinRoomCallAction(appointment: AppointmentModel)
    func onRefreshAction()
    
    // UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onLoadMoreAction()
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?)
}

// MARK: - Interactor Input
protocol PageHistoryAppointmentInteractorInputProtocol {
    func getListAppointments(listStatus: [AppointmentStatus], page: Int, limit: Int)
    func getTokenAppointmentRoom(appointment: AppointmentModel)
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel)
}

// MARK: - Interactor Output
protocol PageHistoryAppointmentInteractorOutputProtocol: AnyObject {
    func onGetListAppointmentsFinished(with result: Result<[AppointmentModel], APIError>, page: Int, total: Int)
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel)
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?)
}

// MARK: - Router
protocol PageHistoryAppointmentRouterProtocol: BaseRouterProtocol {
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func gotoDetailConsultingVC(appointment: AppointmentModel)
}
