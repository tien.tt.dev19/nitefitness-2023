//
//  
//  PageHistoryAppointmentInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

class PageHistoryAppointmentInteractorInput {
    weak var output: PageHistoryAppointmentInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - PageHistoryAppointmentInteractorInputProtocol
extension PageHistoryAppointmentInteractorInput: PageHistoryAppointmentInteractorInputProtocol {
    func getListAppointments(listStatus: [AppointmentStatus], page: Int, limit: Int) {
        self.careService?.getListAppointments(isComing: false, isPats: true, page: page, pageLimit: limit, orderByTime: "DESC", listStatus: listStatus, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListAppointmentsFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
    func getTokenAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta, appointment: appointment)
        })
    }
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenRenewAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenRenewAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
}
