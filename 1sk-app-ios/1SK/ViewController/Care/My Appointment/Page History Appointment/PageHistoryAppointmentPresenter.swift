//
//  
//  PageHistoryAppointmentPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

class PageHistoryAppointmentPresenter {

    weak var view: PageHistoryAppointmentViewProtocol?
    private var interactor: PageHistoryAppointmentInteractorInputProtocol
    private var router: PageHistoryAppointmentRouterProtocol

    init(interactor: PageHistoryAppointmentInteractorInputProtocol,
         router: PageHistoryAppointmentRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var listAppointment: [AppointmentModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        self.router.showHud()
        
        let listStatus = [AppointmentStatus.cancel, AppointmentStatus.complete, AppointmentStatus.finish, AppointmentStatus.failure]
        self.interactor.getListAppointments(listStatus: listStatus, page: page, limit: self.limit)
        
    }
}

// MARK: - PageHistoryAppointmentPresenterProtocol
extension PageHistoryAppointmentPresenter: PageHistoryAppointmentPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
        self.setInitHandleSocketEvent()
    }
    
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.router.showHud()
        self.interactor.getTokenAppointmentRoom(appointment: appointment)
    }
    
    func onRefreshAction() {
        self.getData(in: 1)
    }
    
    // UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.listAppointment.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel? {
        return self.listAppointment[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        guard self.listAppointment.count > 0 else {
            return
        }
        let appointment = self.listAppointment[indexPath.row]
        switch appointment.status?.id {
        case AppointmentStatus.complete.id, AppointmentStatus.finish.id:
            self.router.gotoDetailConsultingVC(appointment: appointment)
            
        default:
            self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: self)
        }
    }
    
    func onLoadMoreAction() {
        if self.listAppointment.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            if self.listAppointment.count >= self.limit {
                SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            }
            self.view?.onLoadingSuccess()
        }
    }
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        guard let `appointment` = appointment else {
            return
        }
        self.router.showHud()
        self.interactor.getTokenRenewAppointmentRoom(appointment: appointment)
    }
}

// MARK: - PageHistoryAppointmentInteractorOutput 
extension PageHistoryAppointmentPresenter: PageHistoryAppointmentInteractorOutputProtocol {
    func onGetListAppointmentsFinished(with result: Result<[AppointmentModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let blogs):
            if page <= 1 {
                self.listAppointment = blogs
                self.view?.onReloadData()

            } else {
                for object in blogs {
                    self.listAppointment.append(object)
                    self.view?.onInsertRow(at: self.listAppointment.count - 1)
                }
            }
            self.total = total

            // Set Out Of Data
            if self.listAppointment.count >= self.total {
                self.isOutOfData = true
            }

            // Set Status View Data Not Found
            if self.listAppointment.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            if self.listAppointment.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.router.hideHud()
    }
    
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel) {
        if meta?.code == 10002 {
            self.view?.onShowAlertRenewToken(meta: meta, appointment: appointment)
            
        } else {
            switch result {
            case .success(let model):
                let room = model
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
            case .failure:
                let room = VideoCallModel()
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
            }
        }
        self.router.hideHud()
    }
    
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?) {
        switch result {
        case .success(let model):
            let room = model
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
        case .failure:
            let room = VideoCallModel()
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
        }
        self.router.hideHud()
    }
}

// MARK: - DetailAppointmentPresenterDelegate
extension PageHistoryAppointmentPresenter: DetailAppointmentPresenterDelegate {
    func onCancelAppointmentSuccess(appointment: AppointmentModel) {
        self.view?.onReloadData()
    }
    
    func onAppointmentChangeStatus(appointment: AppointmentModel) {
        self.view?.onReloadData()
    }
}

// MARK:
extension PageHistoryAppointmentPresenter {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentStatusChangedEvent PageUpcomingAppointmentPresenter")
        self.getData(in: 1)
    }
}
