//
//  
//  PageHistoryAppointmentRouter.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

class PageHistoryAppointmentRouter: BaseRouter {
//    weak var viewController: PageHistoryAppointmentViewController?
    static func setupModule() -> PageHistoryAppointmentViewController {
        let viewController = PageHistoryAppointmentViewController()
        let router = PageHistoryAppointmentRouter()
        let interactorInput = PageHistoryAppointmentInteractorInput()
        let presenter = PageHistoryAppointmentPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - PageHistoryAppointmentRouterProtocol
extension PageHistoryAppointmentRouter: PageHistoryAppointmentRouterProtocol {
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailConsultingVC(appointment: AppointmentModel) {
        let controller = DetailConsultingRouter.setupModule(with: appointment)
        self.viewController?.show(controller, sender: nil)
    }
}
