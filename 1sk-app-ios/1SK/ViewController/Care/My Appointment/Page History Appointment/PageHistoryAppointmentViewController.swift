//
//  
//  PageHistoryAppointmentViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit
import Photos

class PageHistoryAppointmentViewController: BaseViewController {

    var presenter: PageHistoryAppointmentPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    
    private var isLoading = false
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitTableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - PageHistoryAppointmentViewProtocol
extension PageHistoryAppointmentViewController: PageHistoryAppointmentViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onReloadData(section: Int) {
        let indexSet = IndexSet(integer: section)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
    
    func onJoinRoomActive(videoCall: VideoCallModel?) {
        if videoCall?.appId != nil && videoCall?.channelName != nil && videoCall?.token != nil {
            self.showJoinRoomVideoCallView(videoCall: videoCall!)
            
        } else {
            self.onShowAlertWarningAppointment(videoCall: videoCall)
        }
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?) {
        self.showAlertRenewTokenCallViewController(title: "Cuộc gọi tư vấn", content: meta?.message ?? "Bạn đang tham gia tư vấn online bằng thiết bị khác", appointment: appointment, delegate: self)
    }
}

// MARK: AlertRenewTokenCallViewDelegate
extension PageHistoryAppointmentViewController: AlertRenewTokenCallViewDelegate {
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        self.presenter.onAgreeAlertRenewTokenCallAction(appointment: appointment)
    }
}

// MARK: Refresh Control
extension PageHistoryAppointmentViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension PageHistoryAppointmentViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewAppointment.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewAppointment.self, for: indexPath)
        cell.delegate = self
        if let appointment = self.presenter.cellForRow(at: indexPath) {
            cell.config(appointment: appointment)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PageHistoryAppointmentViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRow(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}

// MARK: - CellTableViewAppointmentDelegate
extension PageHistoryAppointmentViewController: CellTableViewAppointmentDelegate {
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.onCheckAuthorizationStatus(appointment: appointment)
    }
}

// MARK: - AVCaptureDevice
extension PageHistoryAppointmentViewController {
    func onCheckAuthorizationStatus(appointment: AppointmentModel) {
        guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized &&  AVCaptureDevice.authorizationStatus(for: .audio) == .authorized else {
            self.onCheckAuthorizationVideo(appointment: appointment)
            return
        }
        self.presenter.onJoinRoomCallAction(appointment: appointment)
    }
    
    func onCheckAuthorizationVideo(appointment: AppointmentModel) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.onCheckAuthorizationAudio(appointment: appointment)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    self.onCheckAuthorizationAudio(appointment: appointment)
                    
                } else {
                    //access denied
                    if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Camera để có thể tiếp tục")
                        }
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
                    }
                }
            })
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
        }
    }
    
    func onCheckAuthorizationAudio(appointment: AppointmentModel) {
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
        case .authorized:
            DispatchQueue.main.async {
                self.presenter.onJoinRoomCallAction(appointment: appointment)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .audio) { granted in
                if granted == true {
                   // User granted
                    DispatchQueue.main.async {
                        self.presenter.onJoinRoomCallAction(appointment: appointment)
                    }
                    
                } else {
                    // User denied
                    if AVCaptureDevice.authorizationStatus(for: .audio) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Microphone để có thể tiếp tục")
                        }
                        
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
                        message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
                    }
                }
            }
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
        }
    }
}

// MARK: - VideoCallViewDelegate
extension PageHistoryAppointmentViewController: AgoraVideoCallViewDelegate, TwilioVideoCallViewDelegate {
    func showJoinRoomVideoCallView(videoCall: VideoCallModel) {
        switch videoCall.typeVideoCall {
        case .agora:
            let controller = AgoraVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        case .twilio:
            let controller = TwilioVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        default:
            SKToast.shared.showToast(content: "Chức năng Video Call không hoạt động")
        }
    }
    
    func onEndCallAction(videoCall: VideoCallModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let id = videoCall?.appointment?.id {
                self.onShowAlertRateConsultationView(appointmentId: id)
            }
        }
    }
}
