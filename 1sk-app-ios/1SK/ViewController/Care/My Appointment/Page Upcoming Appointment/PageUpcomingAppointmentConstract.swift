//
//  
//  PageUpcomingAppointmentConstract.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

// MARK: - View
protocol PageUpcomingAppointmentViewProtocol: AnyObject {
    func onReloadData()
    func onJoinRoomActive(videoCall: VideoCallModel?)
    
    func onSetNotFoundView(isHidden: Bool)
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?)
}

// MARK: - Presenter
protocol PageUpcomingAppointmentPresenterProtocol {
    func onViewDidLoad()
    
    // Action
    func onJoinRoomCallAction(appointment: AppointmentModel)
    func onRefreshAction()
    
    // UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel?
    func onDidSelectRow(at indexPath: IndexPath)
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?)
}

// MARK: - Interactor Input
protocol PageUpcomingAppointmentInteractorInputProtocol {
    func getListAppointments(listStatus: [AppointmentStatus])
    func getTokenAppointmentRoom(appointment: AppointmentModel)
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel)
}

// MARK: - Interactor Output
protocol PageUpcomingAppointmentInteractorOutputProtocol: AnyObject {
    func onGetListAppointmentsFinished(with result: Result<[AppointmentModel], APIError>)
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel)
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?)
}

// MARK: - Router
protocol PageUpcomingAppointmentRouterProtocol: BaseRouterProtocol {
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func alertDefault(title: String, message: String)
}
