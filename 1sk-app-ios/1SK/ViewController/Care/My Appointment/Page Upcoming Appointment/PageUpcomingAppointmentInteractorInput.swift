//
//  
//  PageUpcomingAppointmentInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

class PageUpcomingAppointmentInteractorInput {
    weak var output: PageUpcomingAppointmentInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - PageUpcomingAppointmentInteractorInputProtocol
extension PageUpcomingAppointmentInteractorInput: PageUpcomingAppointmentInteractorInputProtocol {
    func getListAppointments(listStatus: [AppointmentStatus]) {
        self.careService?.getListAppointments(isComing: true,
                                              isPats: false,
                                              page: 1,
                                              pageLimit: 100,
                                              orderByTime: "ASC",
                                              listStatus: listStatus,
                                              completion: { [weak self] result in
            self?.output?.onGetListAppointmentsFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getTokenAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta, appointment: appointment)
        })
    }
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenRenewAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenRenewAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
}
