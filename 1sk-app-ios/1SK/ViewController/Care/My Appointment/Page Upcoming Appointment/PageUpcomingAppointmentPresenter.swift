//
//  
//  PageUpcomingAppointmentPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

class PageUpcomingAppointmentPresenter {

    weak var view: PageUpcomingAppointmentViewProtocol?
    private var interactor: PageUpcomingAppointmentInteractorInputProtocol
    private var router: PageUpcomingAppointmentRouterProtocol

    init(interactor: PageUpcomingAppointmentInteractorInputProtocol,
         router: PageUpcomingAppointmentRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var listAppointmentToday: [AppointmentModel] = []
    var listAppointmentUpcoming: [AppointmentModel] = []
    
    func getDataAppointment() {
        self.router.showHud()
        let listStatus = [AppointmentStatus.paying, AppointmentStatus.confirming, AppointmentStatus.confirmed, AppointmentStatus.cancel, AppointmentStatus.failure]
        self.interactor.getListAppointments(listStatus: listStatus)
    }
    
    func checkDataNotFound() {
        // Set Status View Data Not Found
        if self.listAppointmentUpcoming.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - PageUpcomingAppointmentPresenterProtocol
extension PageUpcomingAppointmentPresenter: PageUpcomingAppointmentPresenterProtocol {
    func onViewDidLoad() {
        self.getDataAppointment()
        self.setInitHandleSocketEvent()
    }
    
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.router.showHud()
        self.interactor.getTokenAppointmentRoom(appointment: appointment)
    }
    
    func onRefreshAction() {
        self.getDataAppointment()
    }
    
    // UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.listAppointmentUpcoming.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel? {
        return self.listAppointmentUpcoming[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        let appointment = self.listAppointmentUpcoming[indexPath.row]
        self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: self)
    }
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        guard let `appointment` = appointment else {
            return
        }
        self.router.showHud()
        self.interactor.getTokenRenewAppointmentRoom(appointment: appointment)
    }
}

// MARK: - PageUpcomingAppointmentInteractorOutput 
extension PageUpcomingAppointmentPresenter: PageUpcomingAppointmentInteractorOutputProtocol {
    func onGetListAppointmentsFinished(with result: Result<[AppointmentModel], APIError>) {
        switch result {
        case .success(let listModel):
            self.listAppointmentUpcoming = listModel
            self.view?.onReloadData()
            
//            for item in self.listAppointmentUpcoming {
//                print("ID:  ", item.id ?? 0)
//            }
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
    
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel) {
        if meta?.code == 10002 {
            self.view?.onShowAlertRenewToken(meta: meta, appointment: appointment)
            
        } else {
            switch result {
            case .success(let model):
                let room = model
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
            case .failure(let error):
                let room = VideoCallModel()
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
                self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
            }
        }
        self.router.hideHud()
    }
    
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?) {
        switch result {
        case .success(let model):
            let room = model
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
        case .failure(let error):
            let room = VideoCallModel()
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
            self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
        }
        self.router.hideHud()
    }
}

// MARK: - DetailAppointmentPresenterDelegate
extension PageUpcomingAppointmentPresenter: DetailAppointmentPresenterDelegate {
    func onCancelAppointmentSuccess(appointment: AppointmentModel) {
        self.view?.onReloadData()
    }
    
    func onAppointmentChangeStatus(appointment: AppointmentModel) {
        self.view?.onReloadData()
    }
}

// MARK:
extension PageUpcomingAppointmentPresenter {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentShowButtonJoinEvent), name: .AppointmentShowButtonJoinEvent, object: nil)
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentStatusChangedEvent PageUpcomingAppointmentPresenter")
        self.getDataAppointment()
    }
    
    @objc func onAppointmentShowButtonJoinEvent(notification: NSNotification) {
        print("socket.on care: AppointmentShowButtonJoinEvent PageUpcomingAppointmentPresenter")
        self.getDataAppointment()
    }
}
