//
//  
//  PageUpcomingAppointmentRouter.swift
//  1SK
//
//  Created by vuongbachthu on 10/22/21.
//
//

import UIKit

class PageUpcomingAppointmentRouter: BaseRouter {
//    weak var viewController: PageUpcomingAppointmentViewController?
    static func setupModule() -> PageUpcomingAppointmentViewController {
        let viewController = PageUpcomingAppointmentViewController()
        let router = PageUpcomingAppointmentRouter()
        let interactorInput = PageUpcomingAppointmentInteractorInput()
        let presenter = PageUpcomingAppointmentPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - PageUpcomingAppointmentRouterProtocol
extension PageUpcomingAppointmentRouter: PageUpcomingAppointmentRouterProtocol {
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func alertDefault(title: String, message: String) {
        self.viewController?.alertDefault(title: title, message: message)
    }
}
