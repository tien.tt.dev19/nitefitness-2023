//
//  SectionHeaderAppointmentUpcoming.swift
//  1SK
//
//  Created by vuongbachthu on 9/25/21.
//

import UIKit

class SectionHeaderAppointmentUpcoming: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_TitleSection: UILabel!
    @IBOutlet weak var lbl_Count: UILabel!
    @IBOutlet weak var view_NotData: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setTitleSection(count: Int) {
        if count > 0 {
            self.lbl_Count.text = count.asString
            self.lbl_Count.isHidden = false
            
        } else {
            self.lbl_Count.text = "0"
            self.lbl_Count.isHidden = true
        }
    }
}
