//
//  CellTableViewNotification.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//

import UIKit

class CellTableViewNotification: UITableViewCell {

    @IBOutlet weak var view_Unread: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(notification: NotificationModel?) {
        self.lbl_Title.text = notification?.title
        self.lbl_Content.text = notification?.content
        
        let date = Utils.shared.getTimeDateByTimeStamp(timeStamp: Double(notification?.createdAt ?? 0))
        self.lbl_Time.text = date.toString(Date.Format.hmdmy)
        
        self.view_Unread.isHidden = notification?.isRead ?? false
    }
}
