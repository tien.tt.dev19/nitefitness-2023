//
//  
//  NotificationCareConstract.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

// MARK: - View
protocol NotificationCareViewProtocol: AnyObject {
    func onReloadData()
    func onSetNotFoundView(isHidden: Bool)
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol NotificationCarePresenterProtocol {
    func onViewDidLoad()
    
    // UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> NotificationModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onDidEndDisplayingRow(at indexPath: IndexPath)
    func onLoadMoreAction()
}

// MARK: - Interactor Input
protocol NotificationCareInteractorInputProtocol {
    func getListNotification(page: Int, limit: Int)
    func setNotificationRead(announcementId: Int)
}

// MARK: - Interactor Output
protocol NotificationCareInteractorOutputProtocol: AnyObject {
    func onGetListNotificationFinished(with result: Result<[NotificationModel], APIError>, page: Int, total: Int)
    func onSetNotificationReadFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - Router
protocol NotificationCareRouterProtocol: BaseRouterProtocol {

}
