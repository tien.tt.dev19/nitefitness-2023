//
//  
//  NotificationCareInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class NotificationCareInteractorInput {
    weak var output: NotificationCareInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - NotificationCareInteractorInputProtocol
extension NotificationCareInteractorInput: NotificationCareInteractorInputProtocol {
    func getListNotification(page: Int, limit: Int) {
        self.careService?.getListNotification(page: page, limit: limit, completion: { [weak self] result in
            self?.output?.onGetListNotificationFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
    
    func setNotificationRead(announcementId: Int) {
        self.careService?.setNotificationRead(announcementId: announcementId, completion: { [weak self] result in
            self?.output?.onSetNotificationReadFinished(with: result.unwrapSuccessModel())
        })
    }
}
