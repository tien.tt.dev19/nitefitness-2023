//
//  
//  NotificationCarePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class NotificationCarePresenter {

    weak var view: NotificationCareViewProtocol?
    private var interactor: NotificationCareInteractorInputProtocol
    private var router: NotificationCareRouterProtocol

    init(interactor: NotificationCareInteractorInputProtocol,
         router: NotificationCareRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var listNotification: [NotificationModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    func getData(in page: Int) {
        self.router.showHud()
        self.interactor.getListNotification(page: page, limit: self.limit)
    }
}

// MARK: - NotificationCarePresenterProtocol
extension NotificationCarePresenter: NotificationCarePresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    // UITableView
    func numberOfRows() -> Int {
        return self.listNotification.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> NotificationModel? {
        return self.listNotification[indexPath.row]
    }
    
    func onLoadMoreAction() {
        if self.listNotification.count >= self.limit && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        //
    }
    
    func onDidEndDisplayingRow(at indexPath: IndexPath) {
        let notification = self.listNotification[indexPath.row]
        guard notification.isRead == false else {
            return
        }
        if let announcementId = notification.id {
            self.interactor.setNotificationRead(announcementId: announcementId)
            notification.isRead = true
        }
    }
}

// MARK: - NotificationCareInteractorOutput 
extension NotificationCarePresenter: NotificationCareInteractorOutputProtocol {
    func onGetListNotificationFinished(with result: Result<[NotificationModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listNotification = listModel
                self.view?.onReloadData()
                
            } else {
                for object in listModel {
                    self.listNotification.append(object)
                    self.view?.onInsertRow(at: self.listNotification.count - 1)
                }
            }
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listNotification.count >= self.total {
                self.isOutOfData = true
            }
            
            // Set Status View Data Not Found
            if self.listNotification.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            self.router.hideErrorView()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            if self.listNotification.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.router.hideHud()
    }
    
    func onSetNotificationReadFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            break
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
}
