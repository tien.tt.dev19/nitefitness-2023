//
//  
//  NotificationCareRouter.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class NotificationCareRouter: BaseRouter {
//    weak var viewController: NotificationCareViewController?
    static func setupModule() -> NotificationCareViewController {
        let viewController = NotificationCareViewController()
        let router = NotificationCareRouter()
        let interactorInput = NotificationCareInteractorInput()
        let presenter = NotificationCarePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - NotificationCareRouterProtocol
extension NotificationCareRouter: NotificationCareRouterProtocol {
    
}
