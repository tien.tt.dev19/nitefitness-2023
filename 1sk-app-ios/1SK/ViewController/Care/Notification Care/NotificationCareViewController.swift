//
//  
//  NotificationCareViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/14/21.
//
//

import UIKit

class NotificationCareViewController: BaseViewController {

    var presenter: NotificationCarePresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.title = "Thông báo"
        self.setInitTableView()
    }
    
}

// MARK: - NotificationCareViewProtocol
extension NotificationCareViewController: NotificationCareViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension NotificationCareViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewNotification.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewNotification.self, for: indexPath)
        cell.config(notification: self.presenter.cellForRow(at: indexPath))
        return cell
    }
}

// MARK: - UITableViewDataSource
extension NotificationCareViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.presenter.onDidEndDisplayingRow(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}
