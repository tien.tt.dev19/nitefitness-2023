//
//  VideoSDKVideoCallViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/4/21.
//

import UIKit
import Toast_Swift
import TwilioVideo

protocol TwilioVideoCallViewDelegate: AnyObject {
    func onEndCallAction(videoCall: VideoCallModel?)
}

class TwilioVideoCallViewController: UIViewController {
    
    @IBOutlet weak var view_Local: VideoView!
    @IBOutlet weak var view_Remote: UIView!
    
    @IBOutlet weak var view_LocalMask: UIView!
    @IBOutlet weak var view_RemoteMask: UIView!
    
    @IBOutlet weak var img_AvatarDoctor: UIImageView!
    @IBOutlet weak var img_AvatarUser: UIImageView!
    
    @IBOutlet weak var btn_SwitchCamera: UIButton!
    @IBOutlet weak var btn_MuteCamera: UIButton!
    @IBOutlet weak var btn_EndCall: UIButton!
    @IBOutlet weak var btn_MuteVoice: UIButton!
    
    @IBOutlet weak var view_Warning: UIView!
    @IBOutlet weak var btn_Warning: UIButton!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_UserJoin: UILabel!
    @IBOutlet weak var lbl_TimeCountdown: UILabel!
    @IBOutlet weak var lbl_StatusAgora: UILabel!
    
    weak var delegate: TwilioVideoCallViewDelegate?
    let careService = CareService()
    var videoCall: VideoCallModel?
    var errorCodeIsReconnecting: Int?
    
    var localId: String?
    var remoteId: String?
    
    var timerCallCountdow = Timer()
    var timeCallDuration: TimeInterval = 0
    var timeDurationWaiting: TimeInterval = 0
    
    var timeDurationEndCall: TimeInterval = 1800
    
    var isFinishAppointment = false
    var isFinishVideoCall = false
    
    private var isVideoRemoteMuted = false
    
    // Video SDK components
    var room: Room?
    var camera: CameraSource?
    var localVideoTrack: TwilioVideo.LocalVideoTrack?
    var localAudioTrack: TwilioVideo.LocalAudioTrack?
    var remoteParticipant: RemoteParticipant?
    var remoteView: VideoView?
    
    private var userJoin = 1 {
        didSet {
            self.lbl_UserJoin.text = userJoin.asString
        }
    }
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isIdleTimerDisabled = true
        
        self.setEnvironmentData()
        self.setInitLayoutView()
        
        self.setInitTimerCall()
        self.setInitHandleSocketEvent()
        self.setObserverConnection()
        self.setInitUIApplication()
        self.setJoinRoomVideoCall()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.onJoinRoomRoom()
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_focused, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isIdleTimerDisabled = false
        self.setLeaveRoom()
    }
    
    func setEnvironmentData() {
        if APP_ENV == .DEV {
            self.timeDurationEndCall = 900
        } else {
            self.timeDurationEndCall = 1800
        }
    }
    
    func setInitLayoutView() {
        self.btn_MuteCamera.isSelected = true
        self.btn_MuteVoice.isSelected = true
        
        self.btn_Warning.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        
        let specialist = self.videoCall?.appointment?.doctor?.specialist?.first
        self.lbl_Title.text = "Tư vấn \(specialist?.name?.lowercased() ?? "chuyên Khoa") qua video"
        self.setRemoteMask(isHidden: false, status: "Bác sĩ chưa xuất hiện. Vui lòng chờ trong giây lát hoặc gọi Tổng đài: \(gPhoneHotline ?? "")")
        
        let avatar = self.videoCall?.appointment?.doctor?.basicInformation?.avatar ?? ""
        self.img_AvatarDoctor.setImageWith(imageUrl: avatar, placeHolder: R.image.default_avatar_2())
        self.img_AvatarUser.setImageWith(imageUrl: gUser?.avatar ?? "", placeHolder: R.image.default_avatar_2())
    }
    
    func setJoinRoomVideoCall() {
        if let id = self.videoCall?.appointment?.id {
            self.careService.setJoinRoomVideoCall(appointmentId: id) { result in
                print("setJoinRoomVideoCall")
                switch result.unwrapSuccessModel() {
                case .success:
                    break
                    
                case .failure(let error):
                    debugPrint(error)
                    self.setErrorLog(error: error)
                }
            }
        }
    }
    
    func setRemoteMask(isHidden: Bool, status: String) {
        self.lbl_StatusAgora.text = status
        self.view_RemoteMask.isHidden = isHidden
    }

    @IBAction func onMinisizeAction(_ sender: Any) {
        //
    }
    
    @IBAction func onSwitchCameraAction(_ sender: Any) {
        self.flipCamera()
    }
    
    @IBAction func onMuteCameraAction(_ sender: Any) {
        self.btn_MuteCamera.isSelected = !self.btn_MuteCamera.isSelected
        if self.btn_MuteCamera.isSelected == true {
            UIView.animate(withDuration: 1.0) {
                self.view_LocalMask.alpha = 0.0

            } completion: { _ in
                self.view_LocalMask.isHidden = true
            }
            
            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_enabled_camera, appointmentId: self.videoCall?.appointment?.id, error: nil)
            
        } else {
            self.view_LocalMask.alpha = 0.0
            self.view_LocalMask.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_LocalMask.alpha = 1.0
            }
            SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_disabled_camera, appointmentId: self.videoCall?.appointment?.id, error: nil)
        }
        
        self.localVideoTrack?.isEnabled = self.btn_MuteCamera.isSelected
    }
    
    @IBAction func onMuteVoiceAction(_ sender: Any) {
        if self.localAudioTrack != nil {
            self.localAudioTrack?.isEnabled = !(self.localAudioTrack?.isEnabled)!
            self.btn_MuteVoice.isSelected = self.localAudioTrack?.isEnabled ?? false
            
            if self.btn_MuteVoice.isSelected == true {
                SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_enabled_mic, appointmentId: self.videoCall?.appointment?.id, error: nil)
            } else {
                SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_disabled_mic, appointmentId: self.videoCall?.appointment?.id, error: nil)
            }
        }
    }
    
    @IBAction func onEndCallAction(_ sender: Any) {
        if self.timeCallDuration > 0 {
            if self.timeCallDuration <= self.timeDurationEndCall {
                let controller = AlertConfirmEndCallViewController()
                controller.view.frame = UIScreen.main.bounds
                controller.delegate = self
                controller.showInView(self, animated: true)

            } else {
                self.onDismissController()
            }

        } else {
            self.onSetFinishAppointment()
        }
    }
    
    func onDismissController() {
        self.dismiss(animated: true)
    }
}

// MARK: - Timer Call
extension TwilioVideoCallViewController {
    func setInitTimerCall() {
        let expiredAt = self.videoCall?.appointment?.doctorAvailableTime?.endAt ?? 00
        self.timeCallDuration = Double(expiredAt) - Double(gTimeSystem ?? 0)
        self.timeDurationWaiting = self.timeCallDuration - self.timeDurationEndCall
        
        self.timerCallCountdow = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimeCountdown), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimeCountdown() {
        if self.timeCallDuration > 0 {
            self.timeCallDuration -= 1
            self.timeDurationWaiting -= 1
            
            if self.timeCallDuration <= self.timeDurationEndCall {
                self.view_Warning.isHidden = true
                self.lbl_TimeCountdown.isHidden = false
                self.lbl_TimeCountdown.text = self.timeCallDuration.asTimeFormat
                
            } else {
                self.view_Warning.isHidden = true
                self.lbl_TimeCountdown.isHidden = true
                
//                self.view_Warning.isHidden = false
//                self.btn_Warning.setTitle("Chưa đến giờ tư vấn", for: .normal)
//                self.lbl_TimeCountdown.text = "00:00" //self.timeDurationWaiting.asTimeFormat
            }
            
            if self.timeCallDuration < 60 && self.view_Warning.isHidden == true {
                self.view_Warning.isHidden = false
            }
            
        } else {
            self.timeCallDuration -= 1
            self.view_Warning.isHidden = false
            self.lbl_TimeCountdown.isHidden = false
            self.btn_Warning.setTitle("Thời gian tư vấn đã hết", for: .normal)
            self.lbl_TimeCountdown.text = "00:00" //self.timeCallDuration.asTimeFormat //
            
            if self.timeCallDuration < -290 {
                self.onStopTimerCallCountdown()
                self.onSetFinishAppointment()
            }
        }
        
//        print("<AgoraKit> updateTimeCountdown \(self.lbl_TimeCountdown.text ?? "00:00") ")
    }
    
    func onStopTimerCallCountdown() {
        self.timerCallCountdow.invalidate()
        print("onStopTimerCallCountdown")
    }
}

// MARK: - Internet Connection
extension TwilioVideoCallViewController {
    func setObserverConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionAvailable), name: .connectionAvailable, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionNotAvailable), name: .connectionUnavailable, object: nil)
    }
    
    @objc func onInternetConnectionAvailable() {
        print("<Twilio> onInternetConnectionAvailable")
        if self.room == nil {
            self.onJoinRoomRoom()
            
        } else {
            self.setRemoteMask(isHidden: true, status: "")
        }
    }
    
    @objc func onInternetConnectionNotAvailable() {
        self.setRemoteMask(isHidden: false, status: "Mất kết nối Internet.\nVui lòng kiểm tra lại kết nối của bạn")
        print("<Twilio> onInternetConnectionNotAvailable")
    }
}

// MARK: - UIApplication
extension TwilioVideoCallViewController {
    func setInitUIApplication() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppWillEnterForeground), name: .applicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppDidEnterBackground), name: .applicationDidEnterBackground, object: nil)
        
    }
    
    @objc func onAppWillEnterForeground() {
        print("<Twilio> onAppWillEnterForeground")
        if let video = self.localVideoTrack {
            self.room?.localParticipant?.publishVideoTrack(video)
        }
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_focused, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
    
    @objc func onAppDidEnterBackground() {
        print("<Twilio> onAppDidEnterBackground")
        if let video = self.localVideoTrack {
            self.room?.localParticipant?.unpublishVideoTrack(video)
        }
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_lost_focused, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
}

// MARK: - Timer Call
extension TwilioVideoCallViewController: AlertConfirmEndCallViewDelegate {
    func onAlertAgreeEndCallAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.onSetFinishAppointment()
        }
    }
}

// MARK: Socket Handle
extension TwilioVideoCallViewController {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentStatusChangedEvent VideoCallViewController ")
        
        if self.isFinishVideoCall == true {
            print("onFinishVideoCall true setInitHandleSocketEvent")
            return
        }
        if self.isFinishAppointment == true {
            print("onFinishVideoCall isFinishAppointment true setInitHandleSocketEvent")
            return
        }
        
        guard let appointment = notification.object as? AppointmentModel else {
            return
        }
        guard appointment.id == self.videoCall?.appointment?.id else {
            return
        }
        guard let status = appointment.status else {
            return
        }
        guard let statusId = status.id else {
            return
        }
        print("socket.on care: AppointmentStatusChangedEvent VideoCallViewController statusId: ", statusId)
        
        switch statusId {
        case AppointmentStatus.finish.id:
            self.setRemoteMask(isHidden: false, status: "Bác sĩ đã kết thúc cuộc tư vấn")
            
            self.view.makeToast(" \nBác sĩ đã kết thúc cuộc tư vấn\n ", duration: 3.0, position: .center)
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.onFinishVideoCall()
            }
            
        default:
            break
        }
    }
}

// MARK: API
extension TwilioVideoCallViewController {
    func onSetFinishAppointment() {
        if self.isFinishVideoCall == true {
            print("onFinishVideoCall true onSetFinishAppointment")
            return
        }
        if let appointment = self.videoCall?.appointment {
            self.isFinishAppointment = true
            
            self.careService.setFinishAppointment(appointment: appointment, completion: { [weak self] result in
                self?.onFinishVideoCall()
                
                switch result.unwrapSuccessModel() {
                case .success:
                    break
                    
                case .failure(let error):
                    debugPrint(error)
                    self?.setErrorLog(error: error)
                }
            })
        }
    }
    
    func onFinishVideoCall() {
        if self.isFinishVideoCall == true {
            print("onFinishVideoCall true")
            return
        }
        print("onFinishVideoCall false")
        
        self.isFinishVideoCall = true
        self.delegate?.onEndCallAction(videoCall: self.videoCall)
        
        self.onDismissController()
    }
}

// MARK: Twilio Init
extension TwilioVideoCallViewController {
    func onJoinRoomRoom() {
        guard let roomName = self.videoCall?.channelName, let accessToken = self.videoCall?.token else {
            return
        }
        
        self.connectToARoom(roomName: roomName, accessToken: accessToken)
    }
    
    func connectToARoom(roomName: String, accessToken: String) {
        print("<Twilio> connectToARoom")
        
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = ConnectOptions(token: accessToken) { (builder) in
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [LocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [TwilioVideo.LocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = TwilioSettings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = TwilioSettings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = TwilioSettings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }

            // Use the preferred signaling region
            if let signalingRegion = TwilioSettings.shared.signalingRegion {
                builder.region = signalingRegion
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = roomName
        }
        
        // Connect to the Room using the options we provided.
        self.room = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
        
        print("<Twilio> connectToARoom Attempting to connect to room \(String(describing: roomName))")
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_joined_room, appointmentId: self.videoCall?.appointment?.id, error: nil)
        
        self.showRoomUI(inRoom: true)
    }
    
    // MARK:- Private
    func startPreview() {
        print("<Twilio> startPreview flipCamera")
        
//        if PlatformUtils.isSimulator {
//            return
//        }

        let frontCamera = CameraSource.captureDevice(position: .front)
        let backCamera = CameraSource.captureDevice(position: .back)

        if frontCamera != nil || backCamera != nil {
            let options = CameraSourceOptions { (builder) in
                if #available(iOS 13.0, *) {
                    // Track UIWindowScene events for the key window's scene.
                    // The example app disables multi-window support in the .plist (see UIApplicationSceneManifestKey).
                    builder.orientationTracker = UserInterfaceTracker(scene: UIApplication.shared.keyWindow!.windowScene!)
                }
            }
            
            // Preview our local camera track in the local video preview view.
            self.camera = CameraSource(options: options, delegate: self)
            self.localVideoTrack = TwilioVideo.LocalVideoTrack(source: self.camera!, enabled: true, name: "Camera")

            // Add renderer to video track for local preview
            self.localVideoTrack!.addRenderer(self.view_Local)
            print("<Twilio> Video track created")

            if frontCamera != nil && backCamera != nil {
                // We will flip camera on tap.
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.flipCamera))
                self.view_Local.addGestureRecognizer(tap)
            }

            self.camera!.startCapture(device: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
                if let error = error {
                    print("<Twilio> Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    self.setErrorLog(error: error)
                    
                } else {
                    self.view_Local.shouldMirror = (captureDevice.position == .front)
                }
            }
        } else {
            print("<Twilio> No front or back capture device found!")
        }
    }

    @objc func flipCamera() {
        print("<Twilio> flipCamera")
        var newDevice: AVCaptureDevice?

        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }

            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        print("<Twilio> flipCamera Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                        self.setErrorLog(error: error)
                        
                    } else {
                        self.view_Local.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
        }
    }

    func prepareLocalMedia() {
        // We will share local audio and video when we connect to the Room.
        print("<Twilio> prepareLocalMedia")
              
        // Create an audio track.
        if self.localAudioTrack == nil {
            self.localAudioTrack = LocalAudioTrack(options: nil, enabled: true, name: "Microphone")

            if self.localAudioTrack == nil {
                print("<Twilio> prepareLocalMedia Failed to create audio track")
            }
        }

        // Create a video track which captures from the camera.
        if self.localVideoTrack == nil {
            self.startPreview()
        }
   }
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        print("<Twilio> showRoomUI")
        
//        self.connectButton.isHidden = inRoom
//        self.roomTextField.isHidden = inRoom
//        self.roomLine.isHidden = inRoom
//        self.roomLabel.isHidden = inRoom
//        self.micButton.isHidden = !inRoom
//        self.disconnectButton.isHidden = !inRoom
//        self.navigationController?.setNavigationBarHidden(inRoom, animated: true)
//        UIApplication.shared.isIdleTimerDisabled = inRoom
//
        // Show / hide the automatic home indicator on modern iPhones.
        self.setNeedsUpdateOfHomeIndicatorAutoHidden()
    }

    func renderRemoteParticipant(participant: RemoteParticipant) -> Bool {
        print("<Twilio> renderRemoteParticipant")
        // This example renders the first subscribed RemoteVideoTrack from the RemoteParticipant.
        
        let videoPublications = participant.remoteVideoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.remoteTrack, publication.isTrackSubscribed {
                self.setupRemoteVideoView()
                subscribedVideoTrack.addRenderer(self.remoteView!)
                self.remoteParticipant = participant
                
                print("<Twilio> renderRemoteParticipant Camera isTrackEnabled: ", publication.isTrackEnabled)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    if publication.isTrackEnabled == true {
                        self.setRemoteMask(isHidden: true, status: "")
                    } else {
                        self.setRemoteMask(isHidden: false, status: "")
                    }
                    self.userJoin = 2
                }
                return true
            }
        }
        return false
    }

    func renderRemoteParticipants(participants: Array<RemoteParticipant>) {
        print("<Twilio> renderRemoteParticipants")
        for participant in participants {
            // Find the first renderable track.
            if participant.remoteVideoTracks.count > 0,
               self.renderRemoteParticipant(participant: participant) {
                break
            }
        }
    }

    func cleanupRemoteParticipant() {
        print("<Twilio> cleanupRemoteParticipant")
        if self.remoteParticipant != nil {
            self.remoteView?.removeFromSuperview()
            self.remoteView = nil
            self.remoteParticipant = nil
        }
    }
    
    func setupRemoteVideoView() {
        print("<Twilio> setupRemoteVideoView")
        
        // Creating `VideoView` programmatically
        self.remoteView = VideoView(frame: CGRect(x: 0, y: 0, width: self.view_Remote.width, height: self.view_Remote.height), delegate: self)
        
        // `VideoView` supports scaleToFill, scaleAspectFill and scaleAspectFit
        // scaleAspectFit is the default mode when you create `VideoView` programmatically.
        self.remoteView!.contentMode = .scaleAspectFill //.scaleAspectFit
        self.view_Remote.addSubview(self.remoteView!)
    }
    
    func setLeaveRoom() {
        print("<Twilio> Leave Room Done")
        
        self.room?.disconnect()
        self.room = nil
        
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_leaved_room, appointmentId: self.videoCall?.appointment?.id, error: nil)
    }
    
    func setErrorLog(error: Error?) {
        let _error = APIError.init(message: "Twilio - \(error?.localizedDescription ?? "error")", statusCode: error?.errorCode ?? 0)
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_error, appointmentId: self.videoCall?.appointment?.id, error: _error)
    }
    
    func setErrorLog(error: APIError?) {
        SocketHelperTimer.share.emitCallTimerEventTracking(event: .user_error, appointmentId: self.videoCall?.appointment?.id, error: error)
    }
}

// MARK: - RoomDelegate
extension TwilioVideoCallViewController: RoomDelegate {
    func roomDidConnect(room: Room) {
        print("<Twilio> roomDidConnect Connected to room \(room.name) as \(room.localParticipant?.identity ?? "")")

        // This example only renders 1 RemoteVideoTrack at a time. Listen for all events to decide which track to render.
        for remoteParticipant in room.remoteParticipants {
            remoteParticipant.delegate = self
        }
    }

    func roomDidDisconnect(room: Room, error: Error?) {
        print("<Twilio> roomDidDisconnect Disconnected from room \(room.name), error = \(String(describing: error))")
        
        self.cleanupRemoteParticipant()
        self.room = nil
        self.showRoomUI(inRoom: false)
        
        if error == nil {
            self.dismiss(animated: true)
            
        } else {
            //self.view.makeToast("error = \(String(describing: error))")
            self.setRemoteMask(isHidden: false, status: "Kết nối của bạn không ổn định.\nVui lòng kiểm tra lại kết nối mạng của bạn")
            
            if self.errorCodeIsReconnecting != nil {
                self.errorCodeIsReconnecting = nil
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.onJoinRoomRoom()
                }
            }
        }
        
        self.setErrorLog(error: error)
    }

    func roomDidFailToConnect(room: Room, error: Error) {
        print("<Twilio> roomDidFailToConnect Failed to connect to room with error = \(String(describing: error))")
        self.room = nil
        self.showRoomUI(inRoom: false)
    }

    func roomIsReconnecting(room: Room, error: Error) {
        print("<Twilio> roomIsReconnecting Reconnecting to room \(room.name), error = \(String(describing: error))")
        
        self.errorCodeIsReconnecting = error.errorCode
        self.userJoin = 1
        
        self.setErrorLog(error: error)
    }

    func roomDidReconnect(room: Room) {
        print("<Twilio> roomDidReconnect Reconnected to room \(room.name)")
//        self.userJoin = 2
    }

    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        // Listen for events from all Participants to decide which RemoteVideoTrack to render.
        participant.delegate = self
        
        print("<Twilio> Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }

    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        // Nothing to do in this example. Subscription events are used to add/remove renderers.
        print("<Twilio> participantDidDisconnect Room \(room.name), Participant \(participant.identity) disconnected")

        self.userJoin = 1
        self.setRemoteMask(isHidden: false, status: "Kết nối của Bác sĩ không ổn định.\nVui lòng chờ trong giây lát")
    }
}

// MARK: - RemoteParticipantDelegate
extension TwilioVideoCallViewController: RemoteParticipantDelegate {
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has offered to share the video Track.
        
        print("<Twilio> remoteParticipantDidPublishVideoTrack Participant \(participant.identity) published \(publication.trackName) video track")
    }

    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has stopped sharing the video Track.
        print("<Twilio> remoteParticipantDidUnpublishVideoTrack Participant \(participant.identity) unpublished \(publication.trackName) video track")
        
        self.setRemoteMask(isHidden: false, status: "")
    }

    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        // Remote Participant has offered to share the audio Track.

        print("<Twilio> remoteParticipantDidPublishAudioTrack Participant \(participant.identity) published \(publication.trackName) audio track")
    }

    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        // Remote Participant has stopped sharing the audio Track.

        print("<Twilio> remoteParticipantDidUnpublishAudioTrack Participant \(participant.identity) unpublished \(publication.trackName) audio track")
    }

    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // The LocalParticipant is subscribed to the RemoteParticipant's video Track. Frames will begin to arrive now.

        print("<Twilio> didSubscribeToVideoTrack Subscribed to \(publication.trackName) video track for Participant \(participant.identity)")

        if self.remoteParticipant == nil {
            _ = renderRemoteParticipant(participant: participant)
        }
    }
    
    func didUnsubscribeFromVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        print("<Twilio> didUnsubscribeFromVideoTrack Unsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")

        if self.remoteParticipant == participant {
            self.cleanupRemoteParticipant()

            // Find another Participant video to render, if possible.
            if var remainingParticipants = room?.remoteParticipants,
                let index = remainingParticipants.firstIndex(of: participant) {
                remainingParticipants.remove(at: index)
                self.renderRemoteParticipants(participants: remainingParticipants)
            }
        }
    }

    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        print("<Twilio> didSubscribeToAudioTrack Subscribed to \(publication.trackName) audio track for Participant \(participant.identity) - audioTrack: \(audioTrack.isEnabled)")
        
        self.userJoin = 2
        self.setRemoteMask(isHidden: false, status: "")
    }
    
    func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        
        print("<Twilio> didUnsubscribeFromAudioTrack Unsubscribed from \(publication.trackName) audio track for Participant \(participant.identity)")
    }

    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        print("<Twilio> remoteParticipantDidEnableVideoTrack Participant \(participant.identity) enabled \(publication.trackName) video track")
        
        self.setRemoteMask(isHidden: true, status: "")
    }

    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        print("<Twilio> remoteParticipantDidDisableVideoTrack Participant \(participant.identity) disabled \(publication.trackName) video track")
        
        self.setRemoteMask(isHidden: false, status: "")
    }

    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        print("<Twilio> remoteParticipantDidEnableAudioTrack Participant \(participant.identity) enabled \(publication.trackName) audio track")
    }

    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        print("<Twilio> remoteParticipantDidDisableAudioTrack Participant \(participant.identity) disabled \(publication.trackName) audio track")
    }

    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        print("<Twilio> didFailToSubscribeToAudioTrack FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
        self.setErrorLog(error: error)
    }

    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
        print("<Twilio> didFailToSubscribeToVideoTrack FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
        self.setErrorLog(error: error)
    }
}

// MARK: - VideoViewDelegate
extension TwilioVideoCallViewController: VideoViewDelegate {
    func videoViewDimensionsDidChange(view: VideoView, dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
        print("<Twilio> videoViewDimensionsDidChange")
    }
}

// MARK: - CameraSourceDelegate
extension TwilioVideoCallViewController: CameraSourceDelegate {
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        print("<Twilio> cameraSourceDidFail Camera source failed with error: \(error.localizedDescription)")
        self.setErrorLog(error: error)
    }
}
