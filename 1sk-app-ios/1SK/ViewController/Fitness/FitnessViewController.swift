//
//  
//  FitnessViewController.swift
//  1SK
//
//  Created by tuyenvx on 05/05/2021.
//
//

import UIKit

class FitnessViewController: BaseViewController {
    @IBOutlet weak var notificationNumberLabel: UILabel!

    var presenter: FitnessPresenterProtocol!
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        presenter.onViewDidLoad()
    }
    // MARK: - Setup
    private func setupDefaults() {

    }
    // MARK: - Action
    @IBAction func buttonNewFeedDidTapped(_ sender: Any) {
        presenter.onNewFeedButtonDidTapped()
    }

    @IBAction func buttonDiscoveryDidTapped(_ sender: Any) {
        presenter.onDiscoveryButtonDidTapped()
    }

    @IBAction func buttonPracticeDidTapped(_ sender: Any) {
        presenter.onPracticeButtonDidTapped()
    }
}

// MARK: - FitnessViewProtocol
extension FitnessViewController: FitnessViewProtocol {
    func updateNotificationView(with unreadCount: Int) {
        notificationNumberLabel.text = String(unreadCount)
        notificationNumberLabel.superview?.alpha = unreadCount != 0 ? 1 : 0
    }
}
