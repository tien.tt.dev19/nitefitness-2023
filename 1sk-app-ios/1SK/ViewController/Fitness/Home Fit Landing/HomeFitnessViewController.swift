//
//  
//  HomeFitnessLandingViewController.swift
//  1SK
//
//  Created by vuongbachthu on 11/9/21.
//
//

import UIKit
import WebKit

class HomeFitnessLandingViewController: BaseViewController {

    var presenter: HomeFitnessLandingPresenterProtocol!
    
    @IBOutlet weak var web_WebView: WKWebView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {

    }
    
    func setInitWKWebView(urlRequest: String) {
        self.web_WebView.scrollView.showsHorizontalScrollIndicator = false
        self.web_WebView.scrollView.pinchGestureRecognizer?.isEnabled = false
        self.web_WebView.navigationDelegate = self
        if let url = URL(string: urlRequest) {
            self.web_WebView.load(URLRequest(url: url))
        }
    }
    
    // MARK: - Action
    @IBAction func onFitnessAction(_ sender: Any) {
        self.presenter.onFitnessAction()
    }
    
}

// MARK: - HomeFitnessViewProtocol
extension HomeFitnessLandingViewController: HomeFitnessLandingViewProtocol {
    func onViewDidLoad(urlRequest: String) {
        self.presenter.onShowHub()
        self.setInitWKWebView(urlRequest: urlRequest)
    }
}

// MARK: - WKNavigationDelegate
extension HomeFitnessLandingViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.presenter.onHiddenHub()
    }
}
