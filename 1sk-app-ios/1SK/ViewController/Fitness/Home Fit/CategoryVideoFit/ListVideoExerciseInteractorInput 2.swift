//
//  
//  ListVideoExerciseInteractorInput.swift
//  1SK
//
//  Created by Valerian on 23/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ListVideoExerciseInteractorInputProtocol {
    func onGetListCategoryVideoExercise()
    func onGetListVideoExercise(with category_id: Int, inPage page: Int)
}

// MARK: - Interactor Output Protocol
protocol ListVideoExerciseInteractorOutputProtocol: AnyObject {
    func didGetListCategoryVideoExerciseFinished(with result: Result<[CategoryVideoExerciseModel], APIError>)
    func didGetListVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>, totalPages: Int)
}

// MARK: - ListVideoExercise InteractorInput
class ListVideoExerciseInteractorInput {
    weak var output: ListVideoExerciseInteractorOutputProtocol?
    var videoExerciseService: VideoExerciseSeviceProtocol?

}

// MARK: - ListVideoExercise InteractorInputProtocol
extension ListVideoExerciseInteractorInput: ListVideoExerciseInteractorInputProtocol {
    func onGetListCategoryVideoExercise() {
        self.videoExerciseService?.onGetCategoryVideoExercise(completion: { [weak self] result in
            self?.output?.didGetListCategoryVideoExerciseFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetListVideoExercise(with category_id: Int, inPage page: Int) {
        self.videoExerciseService?.onGetVideoByCatepory(categoryID: category_id, completion: { [weak self] result in
            let totalPages = result.getMeta()?.pagination?.totalPages
            self?.output?.didGetListVideoExerciseFinished(with: result.unwrapSuccessModel(), totalPages: totalPages!)
        })
    }
}
