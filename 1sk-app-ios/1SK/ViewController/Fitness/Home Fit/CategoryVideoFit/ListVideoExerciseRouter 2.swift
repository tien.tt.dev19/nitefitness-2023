//
//  
//  ListVideoExerciseRouter.swift
//  1SK
//
//  Created by Valerian on 23/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ListVideoExerciseRouterProtocol {
    func showSavedVideoExercise(type: SaveExerciseVideos)
    func showVideoExerciseDetail(with model: VideoExerciseModel)
}

// MARK: - ListVideoExercise Router
class ListVideoExerciseRouter {
    weak var viewController: ListVideoExerciseViewController?
    
    static func setupModule() -> ListVideoExerciseViewController {
        let viewController = ListVideoExerciseViewController()
        let router = ListVideoExerciseRouter()
        let interactorInput = ListVideoExerciseInteractorInput()
        let viewModel = ListVideoExerciseViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.videoExerciseService = VideoExerciseService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ListVideoExercise RouterProtocol
extension ListVideoExerciseRouter: ListVideoExerciseRouterProtocol {
    func showSavedVideoExercise(type: SaveExerciseVideos) {
        let controller = SavedExerciseVideosRouter.setupModule(type: type)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showVideoExerciseDetail(with model: VideoExerciseModel) {
        let controller = VideoExerciseDetailRouter.setupModule(with: model)
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
