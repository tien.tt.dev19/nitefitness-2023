//
//  
//  ListVideoExerciseViewController.swift
//  1SK
//
//  Created by Valerian on 23/06/2022.
//
//

import UIKit

enum CategoryExercises: Int {
    case Title
    case Yoga
    case Gym
    case BodyRhythm
    case BodyFight
    case BalanceFit
    case Erobics
    case Pilates
    case Zumba
    
    var toString: String {
        switch self {
        case .Title:
            return "Video bài tập"
        case .Yoga:
            return "Yoga"
        case .Gym:
            return "Gym"
        case .BodyRhythm:
            return "Body\nThythm"
        case .BodyFight:
            return "Body Fight"
        case .BalanceFit:
            return "Balance Fit"
        case .Erobics:
            return "Erobics"
        case .Pilates:
            return "Pilates"
        case .Zumba:
            return "Zumba"
        }
    }
}

enum SaveExerciseVideos: Int {
    case HistoryVideos
    case likedVideos
    
    var title: String {
        switch self {
        case .HistoryVideos:
            return "Video đã tập"
        case .likedVideos:
            return "Video yêu thích"
        }
    }
}

// MARK: - ViewProtocol
protocol ListVideoExerciseViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
//    UITableView
    func onReloadData()
    func onReloadVideoExercise()
}

// MARK: - ListVideoExercise ViewController
class ListVideoExerciseViewController: BaseViewController {
    var router: ListVideoExerciseRouterProtocol!
    var viewModel: ListVideoExerciseViewModelProtocol!
    
    @IBOutlet weak var tbv_CategoryExercise: UITableView!
    @IBOutlet weak var tbv_ListVideoExercise: UITableView!

    var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let indexPath = IndexPath(row: 0, section: 0)
        self.tbv_CategoryExercise.selectRow(at: indexPath, animated: true, scrollPosition: .none)
    }
    // MARK: - Init
    private func setupInit() {
        self.title = CategoryExercises.Title.toString
        self.setUpBarButton()
        self.setUpTableView()
    }
    
    func setUpBarButton() {
        let historyVideosButton = UIButton(type: .custom)
        historyVideosButton.setImage(R.image.ic_fit_history(), for: .normal)
        historyVideosButton.frame = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        historyVideosButton.addTarget(self, action: #selector(didTapHistoryVideosButton), for: .touchUpInside)
        
        let historyVideosBarButtonItem = UIBarButtonItem(customView: historyVideosButton)

        let likedVideosButton = UIButton(type: .custom)
        likedVideosButton.setImage(R.image.ic_fit_liked(), for: .normal)
        likedVideosButton.frame = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        likedVideosButton.addTarget(self, action: #selector(didTapLikedVideosButton), for: .touchUpInside)

        let likedVideosBarButtonItem = UIBarButtonItem(customView: likedVideosButton)
        
        self.navigationItem.rightBarButtonItems = [likedVideosBarButtonItem, historyVideosBarButtonItem]
    }
    
    // MARK: - Action
    @objc func didTapHistoryVideosButton(sender: AnyObject) {
        self.router.showSavedVideoExercise(type: .HistoryVideos)
    }
    
    @objc func didTapLikedVideosButton(sender: AnyObject) {
        self.router.showSavedVideoExercise(type: .likedVideos)
    }
}
// MARK: - UITableViewDelegate
extension ListVideoExerciseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tbv_CategoryExercise {
            self.viewModel.didSelectCategoryExerciseRow(at: indexPath)
            self.tbv_ListVideoExercise.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
        } else {
            self.router.showVideoExerciseDetail(with: self.viewModel.onGetAllVideoExercise()[indexPath.row])
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tbv_ListVideoExercise {
            if indexPath.row == self.viewModel.numberOfListVideoExercise() - 3, !isLoading {
                self.isLoading = true
                self.viewModel.onLoadMore()
            }
        } 
    }
}

// MARK: - UITableViewDataSource
extension ListVideoExerciseViewController: UITableViewDataSource {
    func setUpTableView() {
        self.tbv_CategoryExercise.registerNib(ofType: CellTableViewCategoryExercise.self)
        self.tbv_ListVideoExercise.registerNib(ofType: CellTableViewListVideoExercise.self)
        
        self.tbv_CategoryExercise.delegate = self
        self.tbv_CategoryExercise.separatorStyle = .none
        self.tbv_CategoryExercise.dataSource = self
        
        self.tbv_ListVideoExercise.delegate = self
        self.tbv_ListVideoExercise.separatorStyle = .none
        self.tbv_ListVideoExercise.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tbv_CategoryExercise {
            return self.viewModel.numberOfListCategoryVideoExercise()
        } else {
            return self.viewModel.numberOfListVideoExercise()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tbv_CategoryExercise {
            let cell = tableView.dequeueCell(ofType: CellTableViewCategoryExercise.self, for: indexPath)
            cell.model = self.viewModel.listCategoryExerciseCellForRow(at: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueCell(ofType: CellTableViewListVideoExercise.self, for: indexPath)
            cell.model = self.viewModel.listVideoExerciseCellForRow(at: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tbv_CategoryExercise {
            return self.tbv_CategoryExercise.frame.width
        } else {
            return 168
        }
    }
}

// MARK: - ListVideoExercise ViewProtocol
extension ListVideoExerciseViewController: ListVideoExerciseViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_CategoryExercise.reloadData()
    }
    
    func onReloadVideoExercise() {
        self.isLoading = false
        self.tbv_ListVideoExercise.reloadData()
    }
    
    func zoomInAnimateButton(button: UIButton) {
        let zoomInAndOut = CABasicAnimation(keyPath: "transform.scale")
        zoomInAndOut.fromValue = 1.0
        zoomInAndOut.toValue = 0.5
        zoomInAndOut.duration = 1.0
        zoomInAndOut.repeatCount = 5
        zoomInAndOut.autoreverses = true
        zoomInAndOut.speed = 2.0
        button.layer.add(zoomInAndOut, forKey: nil)
    }
}
