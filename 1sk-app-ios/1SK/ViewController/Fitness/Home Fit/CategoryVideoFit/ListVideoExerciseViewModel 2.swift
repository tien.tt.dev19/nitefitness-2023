//
//  
//  ListVideoExerciseViewModel.swift
//  1SK
//
//  Created by Valerian on 23/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ListVideoExerciseViewModelProtocol {
    func onViewDidLoad()
    func onLoadMore()
    //func onViewDidAppear()
    
    // UITableView
    func numberOfListCategoryVideoExercise() -> Int
    func numberOfListVideoExercise() -> Int
    
    func onGetAllVideoExercise() -> [VideoExerciseModel]
    func listVideoExerciseCellForRow(at indexPath: IndexPath) -> VideoExerciseModel?
    func listCategoryExerciseCellForRow(at indexPath: IndexPath) -> CategoryVideoExerciseModel?
    
    func didSelectCategoryExerciseRow(at indexPath: IndexPath)
}

// MARK: - ListVideoExercise ViewModel
class ListVideoExerciseViewModel {
    weak var view: ListVideoExerciseViewProtocol?
    private var interactor: ListVideoExerciseInteractorInputProtocol
    
    var listCategory = [CategoryVideoExerciseModel]()
    var listVideoExercise = [VideoExerciseModel]()
    
    private var totalPage = 0
    private var page = 1
    private var currentCategpryIndex: Int?
    private var isOutOfData = false

    init(interactor: ListVideoExerciseInteractorInputProtocol) {
        self.interactor = interactor
    }
}

// MARK: - ListVideoExercise ViewModelProtocol
extension ListVideoExerciseViewModel: ListVideoExerciseViewModelProtocol {
    func onViewDidLoad() {
        self.view?.showHud()
        self.interactor.onGetListCategoryVideoExercise()
    }
    
    func numberOfListCategoryVideoExercise() -> Int {
        return self.listCategory.count
    }
    
    func numberOfListVideoExercise() -> Int {
        return self.listVideoExercise.count
    }
    
    func listVideoExerciseCellForRow(at indexPath: IndexPath) -> VideoExerciseModel? {
        return self.listVideoExercise[indexPath.row]
    }
    
    func listCategoryExerciseCellForRow(at indexPath: IndexPath) -> CategoryVideoExerciseModel? {
        return self.listCategory[indexPath.row]
    }
    
    func didSelectCategoryExerciseRow(at indexPath: IndexPath) {
        self.view?.showHud()
        self.page = 1
        self.currentCategpryIndex = indexPath.row
        self.interactor.onGetListVideoExercise(with: self.listCategory[self.currentCategpryIndex!].id ?? -1, inPage: self.page)
    }
    
    func onLoadMore() {
        if self.page >= self.totalPage {
            self.isOutOfData = true
        }
        
        if !isOutOfData {
            self.view?.showHud()
            self.page += 1
            self.interactor.onGetListVideoExercise(with: self.listCategory[self.currentCategpryIndex!].id ?? -1, inPage: self.page)
        }
        
        self.isOutOfData = false
    }
    
    func onGetAllVideoExercise() -> [VideoExerciseModel] {
        return self.listVideoExercise
    }
}

// MARK: - ListVideoExercise InteractorOutputProtocol
extension ListVideoExerciseViewModel: ListVideoExerciseInteractorOutputProtocol {
    func didGetListVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>, totalPages: Int) {
        switch result {
        case .success(let model):
            self.totalPage = totalPages
            if self.page > 1 {
                self.listVideoExercise += model
            } else {
                self.listVideoExercise = model
            }
            self.view?.onReloadVideoExercise()
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func didGetListCategoryVideoExerciseFinished(with result: Result<[CategoryVideoExerciseModel], APIError>) {
        switch result {
        case .success(let model):
            self.listCategory = model
            self.interactor.onGetListVideoExercise(with: self.listCategory.first?.id ?? -1, inPage: self.page)
            self.view?.onReloadData()
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
