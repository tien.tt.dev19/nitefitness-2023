//
//  
//  VideoExerciseDetailInteractorInput.swift
//  1SK
//
//  Created by Valerian on 28/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol VideoExerciseDetailInteractorInputProtocol {
    func onAddVideoFavorite(with model: VideoExerciseModel)
    func onRemoveVideoFavorite(with model: VideoExerciseModel)
    func onGetRelatedVideo(with model: VideoExerciseModel)
    func onGetOnFavoriteVideoExercise()
    func onSaveLastSeen(with lastTime: String, videoID: Int)
}

// MARK: - Interactor Output Protocol
protocol VideoExerciseDetailInteractorOutputProtocol: AnyObject {
    func didAddVideoFavoriteFinished(with result: Result<VideoExerciseModel, APIError>)
    func didGetFavoriteVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>)
    func didRemoveVideoFavoriteFinished(with result: Result<[VideoExerciseModel], APIError>)
    func didGetRelatedVideoFinished(with result: Result<[VideoExerciseModel], APIError>)
    func didSaveLastSeenFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - VideoExerciseDetail InteractorInput
class VideoExerciseDetailInteractorInput {
    weak var output: VideoExerciseDetailInteractorOutputProtocol?
    var service: VideoExerciseSeviceProtocol?
}

// MARK: - VideoExerciseDetail InteractorInputProtocol
extension VideoExerciseDetailInteractorInput: VideoExerciseDetailInteractorInputProtocol {
    func onAddVideoFavorite(with model: VideoExerciseModel) {
        if let id = model.id {
            self.service?.onAddToFavoriteList(with: id, completion: { [weak self] result in
                self?.output?.didAddVideoFavoriteFinished(with: result.unwrapSuccessModel())
            })
        }
    }
    
    func onGetOnFavoriteVideoExercise() {
        self.service?.onGetVideoUserFavorite(completion: { [weak self] result in
            self?.output?.didGetFavoriteVideoExerciseFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func onRemoveVideoFavorite(with model: VideoExerciseModel) {
        if let id = model.id {
            self.service?.onRemoveFavoriteList(with: id, completion: { [weak self] result in
                self?.output?.didRemoveVideoFavoriteFinished(with: result.unwrapSuccessModel())
            })
        }
    }
    
    func onGetRelatedVideo(with model: VideoExerciseModel) {
        self.service?.onGetRelatedVideoExercise(videoID: model.id ?? -1, categoryID: model.categoryId ?? -1, completion: { [weak self] result in
            self?.output?.didGetRelatedVideoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func onSaveLastSeen(with lastTime: String, videoID: Int) {
        self.service?.onSaveLastSeen(with: "\(videoID)", at: lastTime, completion: { [weak self] result in
            self?.output?.didSaveLastSeenFinished(with: result.unwrapSuccessModel())
        })
    }
}
