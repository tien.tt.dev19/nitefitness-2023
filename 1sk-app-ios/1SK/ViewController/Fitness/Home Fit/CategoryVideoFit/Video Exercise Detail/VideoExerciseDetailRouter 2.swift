//
//  
//  VideoExerciseDetailRouter.swift
//  1SK
//
//  Created by Valerian on 28/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol VideoExerciseDetailRouterProtocol {
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
}

// MARK: - VideoExerciseDetail Router
class VideoExerciseDetailRouter {
    weak var viewController: VideoExerciseDetailViewController?
    
    static func setupModule(with model: VideoExerciseModel) -> VideoExerciseDetailViewController {
        let viewController = VideoExerciseDetailViewController()
        let router = VideoExerciseDetailRouter()
        let interactorInput = VideoExerciseDetailInteractorInput()
        let viewModel = VideoExerciseDetailViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.videoExercise = model
        interactorInput.output = viewModel
        interactorInput.service = VideoExerciseService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - VideoExerciseDetail RouterProtocol
extension VideoExerciseDetailRouter: VideoExerciseDetailRouterProtocol {
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        let controller = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        controller.popoverPresentationController?.sourceView = sourceView
        controller.popoverPresentationController?.sourceRect = sourceView.bounds
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
