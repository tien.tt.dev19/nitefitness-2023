//
//  
//  VideoExerciseDetailViewController.swift
//  1SK
//
//  Created by Valerian on 28/06/2022.
//
//

import UIKit
import AVFoundation

protocol VideoExerciseDetailViewDelegate: AnyObject {
    func onSendVideoSegment(segment: Int)
}

// MARK: - ViewProtocol
protocol VideoExerciseDetailViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func updateUI()
    func getShareView() -> UIView
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
    func onReloadDataVideo(with video: VideoExerciseModel?, categoryType: CategoryType?)
    func onReloadDataRelated()
    func setVideoResolution(_ resolution: VideoResolution)

    //UITableView
    //func onReloadData()
}

// MARK: - VideoExerciseDetail ViewController
class VideoExerciseDetailViewController: BaseViewController {
    var router: VideoExerciseDetailRouterProtocol!
    var viewModel: VideoExerciseDetailViewModelProtocol!
    weak var videoSegmentDelegate: VideoExerciseDetailViewDelegate?

    @IBOutlet weak var view_Player: UIView!
    @IBOutlet weak var view_Control: UIView!
    @IBOutlet weak var view_AVPlayerControl: UIView!
    @IBOutlet weak var lbl_VideoName: UILabel!
    @IBOutlet weak var lbl_VideoDescription: UILabel!
    @IBOutlet weak var btn_FavoriteVideo: UIButton!
    @IBOutlet weak var lbl_ExerciseTime: UILabel!
    @IBOutlet weak var lbl_ExerciseLevel: UILabel!
    @IBOutlet weak var lbl_ExerciseCategory: UILabel!
    @IBOutlet weak var lbl_ExerciseTool: UILabel!
    @IBOutlet weak var btn_shareButton: UIButton!
    @IBOutlet weak var coll_RelatedContent: UICollectionView!
    @IBOutlet weak var coll_Segments: UICollectionView!
    @IBOutlet weak var img_CoachAvater: UIImageView!
    @IBOutlet weak var lbl_CoachName: UILabel!
    @IBOutlet weak var lbl_CoachDescription: UILabel!
    @IBOutlet weak var constraint_HeightSegmentView: NSLayoutConstraint!
    @IBOutlet weak var constraint_HeightCoachView: NSLayoutConstraint!
    @IBOutlet weak var scroll_ScrollView: UIScrollView!
    
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var avControlView: AVPlayerControlView?
    var timerControl: Timer?
    var timeControlDuration: TimeInterval = 5
    var isPlayFullScreen = false
    var valueSliderChange: Float = 0
    var progressValue: CGFloat = 0
    var needHideLoad = true
    var segmentVideoTime: Double = 0
    var lastSeenTime: String = ""
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isPlayFullScreen {
            self.view_Player.layer.addSublayer(self.avPlayerLayer!)
            self.view_AVPlayerControl.addSubview(self.avControlView!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if self.isPlayFullScreen {
            self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: self.view_Player.width, height: self.view_Player.height)
            self.avControlView?.frame = CGRect(x: 0, y: 0, width: self.view_AVPlayerControl.width, height: self.view_AVPlayerControl.height)
            
            self.isPlayFullScreen = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.viewModel.onSaveLastSeen(with: self.lastSeenTime)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avPlayerLayer?.frame = self.view_Player.bounds
    }
    
    deinit {
        gIsShowMarketingPopup = false
    }
    
    // MARK: - Init
    private func setupInit() {
        gIsShowMarketingPopup = true
        self.setInitAVPlayerControlView()
        self.setInitAnimationViewControl()
        self.setIntCollectioView()
        
        guard let `video` = self.viewModel.onGetVideoExercise() else {
            return
        }
        self.configUI(with: video)
    }
    
    func setInitAVPlayerControlView() {
        self.avControlView = AVPlayerControlView.loadFromNib()
        self.avControlView?.delegate = self
        self.avControlView?.frame = CGRect(x: 0, y: 0, width: self.view_AVPlayerControl.width, height: self.view_AVPlayerControl.height)
        self.view_AVPlayerControl.addSubview(self.avControlView!)
        
    }
    
    func configUI(with data: VideoExerciseModel) {
        self.lbl_VideoName.text = data.name ?? ""
        self.lbl_VideoDescription.text = data.description ?? ""
        self.lbl_ExerciseTime.text = data.duration ?? ""
        
        if let data = data.details {
            if !data.isEmpty {
                self.lbl_ExerciseLevel.text = data[0].value?.name ?? ""
                self.lbl_ExerciseCategory.text = data[1].value?.name ?? ""
                self.lbl_ExerciseTool.text = data[2].value?.name ?? ""
            }
        }
        
        if let coachData = data.coach {
            self.img_CoachAvater.setImageWith(imageUrl: coachData.avatar ?? "")
            self.lbl_CoachName.text = coachData.name ?? ""
            self.lbl_CoachDescription.text = coachData.description ?? ""
            self.constraint_HeightCoachView.constant = 84
        } else {
            self.constraint_HeightCoachView.constant = 0
        }
    
        self.setInitAVPlayer(videoUrl: data.link ?? "")
        self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize.zero
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        self.setInvalidateTimeControl()
        self.setRemoveObserver()
    }
    
    @IBAction func onLikeAction(_ sender: UIButton) {
        if self.viewModel.onCheckVideoFavorite() {
            self.viewModel.onRemoveVideoFavorite(with: self.viewModel.onGetVideoExercise()!)
        } else {
            self.viewModel.onAddVideoFavorite(with: self.viewModel.onGetVideoExercise()!)
        }
    }
    
    @IBAction func onShareAction(_ sender: UIButton) {
        self.viewModel.onShareAction()
    }
    
    @IBAction func onFullScreenAction(_ sender: UIButton) {
        self.isPlayFullScreen = true
        
        let controller = PlayVideoFullViewController()
        controller.delegate = self
        controller.videoModel = self.viewModel.onGetVideoExercise()
        controller.avPlayer = self.avPlayer
        controller.avPlayerLayer = self.avPlayerLayer
        controller.avControlView = self.avControlView
        controller.resolution = VideoResolution.auto
        controller.segmentVideoTime = self.segmentVideoTime
        self.videoSegmentDelegate = controller as? VideoExerciseDetailViewDelegate
        
        self.navigationController?.show(controller, sender: nil)
        
        self.view_Control.alpha = 0
        self.view_Control.isHidden = true
    }
}

// MARK: - UICollectionViewDataSource
extension VideoExerciseDetailViewController: UICollectionViewDataSource {
    func setIntCollectioView() {
        self.coll_RelatedContent.registerNib(ofType: CellCollectionVideoExercise.self)
        self.coll_Segments.registerNib(ofType: CellCollectionVideoExercise.self)
        
        self.coll_RelatedContent.delegate = self
        self.coll_RelatedContent.dataSource = self
        self.coll_Segments.delegate = self
        self.coll_Segments.dataSource = self
        
        let layoutVideoExercise = UICollectionViewFlowLayout()
        layoutVideoExercise.scrollDirection = .horizontal
        layoutVideoExercise.minimumLineSpacing = 16
        layoutVideoExercise.minimumInteritemSpacing = 16
        layoutVideoExercise.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutVideoExercise.itemSize = CGSize(width: 200, height: self.coll_RelatedContent.frame.height)
        self.coll_RelatedContent.collectionViewLayout = layoutVideoExercise
        
        let layoutVideoExerciseSegment = UICollectionViewFlowLayout()
        layoutVideoExerciseSegment.scrollDirection = .horizontal
        layoutVideoExerciseSegment.minimumLineSpacing = 16
        layoutVideoExerciseSegment.minimumInteritemSpacing = 16
        layoutVideoExerciseSegment.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutVideoExerciseSegment.itemSize = CGSize(width: 164, height: self.coll_Segments.frame.height)
        self.coll_Segments.collectionViewLayout = layoutVideoExerciseSegment
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case coll_Segments:
            return self.viewModel.onGetVideoExercise()?.segments?.count ?? 0
        case coll_RelatedContent:
            return self.viewModel.numberOfItems()
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionVideoExercise.self, for: indexPath)
        switch collectionView {
        case coll_Segments:
            cell.segmetsVideoExercise = self.viewModel.onGetVideoExercise()?.segments?[indexPath.row]
            
        case coll_RelatedContent:
            cell.relatedVideoExercise = self.viewModel.cellForItem(at: indexPath)
            
        default:
            return cell
        }
        return cell
    }

}

// MARK: - UICollectionViewDelegate
extension VideoExerciseDetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case coll_Segments:
            if let videoSegment = self.viewModel.onGetVideoExercise()?.segments?[indexPath.row].startAt {
                let second = self.viewModel.onFormatTimeToInt(with: videoSegment)
                self.segmentVideoTime = Double(second)
                self.videoSegmentChanged(with: second)
                self.scroll_ScrollView.scrollsToTop = true
            }
            
        case coll_RelatedContent:
            self.viewModel.didSelectItem(at: indexPath)
            
        default:
            break
        }
    }
}

// MARK: - PlayVideoFullViewDelegate
extension VideoExerciseDetailViewController: PlayVideoFullViewDelegate {
    func setVideoSegment(second: Int) {
        self.videoSegmentChanged(with: second)
    }
    
    func setResolutionFullVideo(_ resolution: VideoResolution) {
        self.setVideoResolution(resolution)
    }
}

// MARK: - VideoExerciseDetail ViewProtocol
extension VideoExerciseDetailViewController: VideoExerciseDetailViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func getShareView() -> UIView {
        return self.btn_shareButton
    }
    
    func updateUI() {
        if self.viewModel.onCheckVideoFavorite() {
            self.btn_FavoriteVideo.setImage(R.image.ic_fit_likedVideo(), for: .normal)
        } else {
            self.btn_FavoriteVideo.setImage(R.image.ic_fit_likedButton(), for: .normal)
        }
    }
    
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        self.router.showShareViewController(with: shareItem, sourceView: sourceView)
    }
    
    func onReloadDataVideo(with video: VideoExerciseModel?, categoryType: CategoryType?) {
        guard let `video` = video else {
            return
        }
        self.configUI(with: video)
        self.hideHud()
    }
    
    func onReloadDataRelated() {
        
        if self.viewModel.onGetVideoExercise()?.segments?.count == 0 {
            self.constraint_HeightSegmentView.constant = 0
        } else {
            self.constraint_HeightSegmentView.constant = 110
        }
        
        self.coll_RelatedContent.reloadData()
        self.coll_Segments.reloadData()
        self.coll_RelatedContent.setContentOffset(.zero, animated: true)
    }
    
    func setVideoResolution(_ resolution: VideoResolution) {
        switch resolution {
        case .auto:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize.zero
            
        case ._1080:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1920, height: 1080)
            
        case ._720:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1280, height: 720)
            
        case ._480:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 854, height: 480)
            
        case ._360:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 640, height: 360)
        }
    }
}

// MARK: - AVPlayerControlViewDelegate
extension VideoExerciseDetailViewController: AVPlayerControlViewDelegate {
    func onPlayAction() {
        guard let avPlayer = self.avPlayer else {
            return
        }
        
        switch self.avControlView?.avState {
        case .readyToPlay:
            if avPlayer.isPlaying {
                self.onPause()
                
            } else {
                self.onPlay()
            }
            
        case .playedToTheEnd:
            self.onReplay()
            
        default:
            break
        }
    }
    
    func onSliderValueChangeAction(value: Float) {
        self.needHideLoad = true
        self.valueSliderChange = value
        
        self.avControlView?.avState = .loading
        self.avControlView?.ind_Loading.startAnimating()
        self.avControlView?.btn_Play.isHidden = true
        
        guard let duration = self.avPlayer?.currentItem?.asset.duration,
              let item = self.avPlayer?.currentItem else { return }
        
        let valueChanged = Double(self.avControlView?.slider_TimeTracking.value ?? 0) * duration.seconds
        let timeIntervalChanged = CMTime(seconds: valueChanged, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        self.segmentVideoTime = valueChanged
        
        self.videoSegmentDelegate?.onSendVideoSegment(segment: Int(self.segmentVideoTime))
        
        item.seek(to: timeIntervalChanged) { _ in
            if self.avPlayer?.isPlaying == true {
                self.avPlayer?.play()
            }
        }
    }
    
    func onSliderEventAction(event: SliderEvent) {
        switch event {
        case .began:
            self.avControlView?.avState = .seeking
            
        case .ended:
            self.setTimerViewControlHidden()
            
        default:
            break
        }
    }
}

// MARK: AVPlayer Video
extension VideoExerciseDetailViewController {
    func setInitAVPlayer(videoUrl: String) {
        print("videoUrl:", videoUrl)
        
        guard let url = URL(string: videoUrl) else {
            SKToast.shared.showToast(content: "Error video link: \(videoUrl)")
            return
        }
        
        if self.avPlayer == nil {
            self.preparelayVideoWithFisrt(url: url)
            
        } else {
            self.preparePlayVideoNext(url: url)
        }
    }
    
    func preparelayVideoWithFisrt(url: URL) {
        let avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer = AVPlayer(playerItem: avPlayerItem)
        
        self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
        self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: self.view_Player.width, height: self.view_Player.height)
        self.avPlayerLayer?.videoGravity = .resizeAspect
        self.view_Player.layer.addSublayer(self.avPlayerLayer!)
        
        self.onAddObserverCurrentItem()
        self.onAddObserverKVOPlayer()
        
        self.avPlayer?.play()
    }
    
    func preparePlayVideoNext(url: URL) {
        let avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer?.replaceCurrentItem(with: avPlayerItem)
        
        self.onAddObserverCurrentItem()
        self.onResetViewControl()
        self.setViewControlShow()
        
        self.avPlayer?.play()
    }
    
    func onResetViewControl() {
        self.avControlView?.avState = .loading
        self.avControlView?.ind_Loading.startAnimating()
        self.avControlView?.slider_TimeTracking.setValue(0, animated: true)
        self.avControlView?.btn_Play.isHidden = true
    }
    
}


// MARK: - Player Handler Control
extension VideoExerciseDetailViewController {
    func onPlay() {
        self.avPlayer?.play()
        self.avControlView?.btn_Play.setImage(R.image.ic_fit_pause(), for: .normal)
        NotificationCenter.default.post(name: .onActionPlayControl, object: nil)
        self.setViewControlShow()
    }
    
    func onPause() {
        self.avPlayer?.pause()
        self.avControlView?.btn_Play.setImage(R.image.ic_fit_play(), for: .normal)
    }
    
    func onReplay() {
        self.avPlayer?.seek(to: CMTime.zero)
        self.onPlay()
    }
}

// MARK: Timer Control
extension VideoExerciseDetailViewController {
    func setTimerViewControlHidden() {
        if self.timerControl == nil {
            self.timeControlDuration = 5
            self.timerControl = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimeControlDidChange), userInfo: nil, repeats: true)
            
        } else {
            self.timeControlDuration = 3
        }
    }
    
    @objc func onTimeControlDidChange() {
        if self.timeControlDuration > 1 {
            self.timeControlDuration -= 1
            
        } else {
            self.setInvalidateTimeControl()
            self.setViewControlHidden()
        }
    }
    
    func setInvalidateTimeControl() {
        self.timerControl?.invalidate()
        self.timerControl = nil
    }
}
// MARK: KVO Observer
extension VideoExerciseDetailViewController {
    func onAddObserverKVOPlayer() {
        self.avPlayer?.addObserver(self, forKeyPath: AVConstant.rate, options: [.old, .new], context: nil)
        
        self.onTimeObserver()
    }
    
    func setRemoveObserver() {
        self.avPlayer?.currentItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: nil)
        self.avPlayer?.currentItem?.removeObserver(self, forKeyPath: AVConstant.loadedTimeRangesKey, context: nil)
        self.avPlayer?.removeObserver(self, forKeyPath: AVConstant.rate, context: nil)
    }
    
    func onAddObserverCurrentItem() {
        self.avPlayer?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.initial, .new], context: nil)
        self.avPlayer?.currentItem?.addObserver(self, forKeyPath: AVConstant.loadedTimeRangesKey, options: [.initial, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        switch keyPath {
        case #keyPath(AVPlayerItem.status):
            var status: AVPlayerItem.Status = .unknown
            if let statusNumber = change?[.newKey] as? NSNumber,
                let newStatus = AVPlayerItem.Status(rawValue: statusNumber.intValue) {
                status = newStatus
            }
            
            switch status {
            case .readyToPlay:
                self.avControlView?.btn_Play.setImage(R.image.ic_fit_pause(), for: .normal)
                self.avControlView?.ind_Loading.stopAnimating()
                self.avControlView?.btn_Play.isHidden = false
                
                if self.avControlView?.avState == .seeking || self.avControlView?.avState == .loading {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.avControlView?.avState = .readyToPlay
                        self.valueSliderChange = 0
                        
                        print("observeValue readyToPlay 1.5")
                    }
                } else {
                    self.avControlView?.avState = .readyToPlay
                    print("observeValue readyToPlay")
                }
                
                self.setViewControlShow()
                
            case .failed:
                print("observeValue failed")
                
            case .unknown:
                print("observeValue unknown")
                
            @unknown default:
                fatalError()
            }
            
        case AVConstant.loadedTimeRangesKey:
            self.onLoadedTimeRangedChanged()
            
        case AVConstant.rate:
            if self.avPlayer?.rate == 1 {
                self.avControlView?.avRate = .isPlay
                
            } else {
                self.avControlView?.avRate = .isPause
            }
             
        default:
            break
        }
    }
    
    private func onLoadedTimeRangedChanged() {
        let duration = self.avPlayer?.currentItem?.asset.duration
        let durationSeconds = CMTimeGetSeconds(duration ?? .zero)
        
        let totalBuffer = self.avPlayer?.currentItem?.totalBuffer()
        self.progressValue = CGFloat(totalBuffer ?? 0) / CGFloat(durationSeconds)
        self.avControlView?.view_Progress.setProgress(Float(progressValue), animated: true)
    }
    
    func onTimeObserver() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC)) //DispatchQueue.main
        
        self.avPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: nil, using: { [weak self] _ in
            
            guard let currentItem = self?.avPlayer?.currentItem, let durationCMTimeFormat = self?.avPlayer?.currentItem?.asset.duration else {
                return
            }
            
            let duration = CMTimeGetSeconds(durationCMTimeFormat).rounded(toPlaces: 1)
            let currentTime = currentItem.currentTime().seconds.rounded(toPlaces: 1)
            
            if self?.avControlView?.avState != .seeking {
                if self?.valueSliderChange ?? 0 == 0 {
                    let sliderValue = Float(currentTime / Double(duration))
                    self?.avControlView?.slider_TimeTracking.setValue(sliderValue, animated: true)
                    
                    let sliderValuePlay = sliderValue.rounded(toPlaces: 3)
                    let sliderValueLoad = Float(self!.progressValue).rounded(toPlaces: 3)
                    
                    if sliderValuePlay == sliderValueLoad && sliderValueLoad > 0.0 {
                        self?.avControlView?.avState = .loading
                        self?.avControlView?.btn_Play.isHidden = true
                        self?.avControlView?.ind_Loading.startAnimating()
                        self?.setViewControlShow()
                        
                        NotificationCenter.default.post(name: .onActionLoading, object: nil)
//                        print("<AVPlayer> addTimeObserver: xxx ==")
                    }
                    
                } else {
                    self?.avControlView?.slider_TimeTracking.setValue(self?.valueSliderChange ?? 0, animated: true)
//                    print("<AVPlayer> addTimeObserver: valueSliderChange: ", self?.valueSliderChange ?? 0)
                    if self?.needHideLoad == true {
                        self?.avControlView?.avState = .readyToPlay
                        self?.avControlView?.ind_Loading.stopAnimating()
                        self?.avControlView?.btn_Play.isHidden = false
                        self?.setViewControlShow()
                        self?.needHideLoad = false
                    }
                }
            }
            
            let currentTimeFormat = AVUtils.formatDurationTime(time: Int(currentTime))
            let durationFormat = AVUtils.formatDurationTime(time: Int(duration))
            self?.lastSeenTime = currentTimeFormat

            self?.setTimePlayer(withDuration: durationFormat, currentTime: currentTimeFormat)
            
            // Played To The End
            if currentTimeFormat == durationFormat {
                self?.avControlView?.avState = .playedToTheEnd
                self?.avControlView?.btn_Play.setImage(R.image.ic_replay(), for: .normal)
                self?.avControlView?.ind_Loading.stopAnimating()
                self?.avControlView?.btn_Play.isHidden = false
                self?.setViewControlShow()
                print("<AVPlayer> addTimeObserver: Played To The End")
            }
            
            print("<AVPlayer> addTimeObserver: duration: ", duration)
            print("<AVPlayer> addTimeObserver: currentTime: ", currentTime)
            print("<AVPlayer> addTimeObserver: ========================\n")
        })
    }
    
    private func setTimePlayer(withDuration duration: String, currentTime: String) {
        self.avControlView?.lbl_TimePlayer.text = String(format: "%@/%@", currentTime, duration)
    }
}

// MARK: Animation View Control
extension VideoExerciseDetailViewController {
    func setInitAnimationViewControl() {
        self.view_Player.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapViewPlayerAction)))
        self.view_Control.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapViewControlAction)))
        
        self.setViewControlShow()
    }
    
    @objc func onTapViewPlayerAction() {
        self.setViewControlShow()
    }
    
    @objc func onTapViewControlAction() {
        self.timeControlDuration = 5
    }
    
    func setViewControlShow() {
        if self.view_Control.isHidden == true {
            self.view_Control.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_Control.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
        
        self.setTimerViewControlHidden()
    }
    
    func setViewControlHidden() {
        if self.avControlView?.avState == .readyToPlay && self.avControlView?.avRate == .isPlay {
            UIView.animate(withDuration: 1.0) {
                self.view_Control.alpha = 0
                self.view.layoutIfNeeded()

            } completion: { _ in
                self.view_Control.isHidden = true
            }
        }
    }
    
    func videoSegmentChanged(with valueChanged: Int) {
        
        guard let durationCMTimeFormat = self.avPlayer?.currentItem?.asset.duration else {
            return
        }
        
        let duration = CMTimeGetSeconds(durationCMTimeFormat).rounded(toPlaces: 1)
        let sliderValue = Float(Double(valueChanged) / Double(duration))
        self.avControlView?.slider_TimeTracking.setValue(sliderValue, animated: true)
        
        self.onSliderValueChangeAction(value: sliderValue)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.onPlay()
        }
    }
}
