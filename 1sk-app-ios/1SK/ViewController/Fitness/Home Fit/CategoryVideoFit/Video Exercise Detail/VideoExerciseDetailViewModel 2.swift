//
//  
//  VideoExerciseDetailViewModel.swift
//  1SK
//
//  Created by Valerian on 28/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol VideoExerciseDetailViewModelProtocol {
    func onViewDidLoad()
    func onGetListVideoFavorite() -> [VideoExerciseModel]?
    func onGetVideoExercise() -> VideoExerciseModel?
    func onAddVideoFavorite(with model: VideoExerciseModel)
    func onRemoveVideoFavorite(with model: VideoExerciseModel)
    func onShareAction()
    func onCheckVideoFavorite() -> Bool
    func onFormatTimeToInt(with startAt: String) -> Int
    func onSaveLastSeen(with lastTime: String)
    
    // UICollectionView
    func numberOfItems() -> Int
    func cellForItem(at indexPath: IndexPath) -> VideoExerciseModel?
    func didSelectItem(at indexPath: IndexPath)
}

// MARK: - VideoExerciseDetail ViewModel
class VideoExerciseDetailViewModel {
    weak var view: VideoExerciseDetailViewProtocol?
    private var interactor: VideoExerciseDetailInteractorInputProtocol
    
    var videoExercise: VideoExerciseModel?
    var listVideoFavorite: [VideoExerciseModel]?
    var listRelatedVideo: [VideoExerciseModel]?

    init(interactor: VideoExerciseDetailInteractorInputProtocol) {
        self.interactor = interactor
    }
}

// MARK: - VideoExerciseDetail ViewModelProtocol
extension VideoExerciseDetailViewModel: VideoExerciseDetailViewModelProtocol {
    func numberOfItems() -> Int {
        return self.listRelatedVideo?.count ?? 0
    }
    
    func cellForItem(at indexPath: IndexPath) -> VideoExerciseModel? {
        return self.listRelatedVideo?[indexPath.row]
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        self.view?.showHud()
        self.videoExercise = self.listRelatedVideo?[indexPath.row]
        self.interactor.onGetOnFavoriteVideoExercise()
        self.interactor.onGetRelatedVideo(with: self.videoExercise!)
        self.view?.onReloadDataVideo(with: self.videoExercise, categoryType: CategoryType.none)
    }
    
    func onViewDidLoad() {
        self.view?.showHud()
        self.interactor.onGetOnFavoriteVideoExercise()
        self.interactor.onGetRelatedVideo(with: self.videoExercise!)
    }
    
    func onGetVideoExercise() -> VideoExerciseModel? {
        return self.videoExercise
    }
    
    func onGetListVideoFavorite() -> [VideoExerciseModel]? {
        return self.listVideoFavorite
    }
    
    func onAddVideoFavorite(with model: VideoExerciseModel) {
        self.view?.showHud()
        self.interactor.onAddVideoFavorite(with: model)
    }
    
    func onRemoveVideoFavorite(with model: VideoExerciseModel) {
        self.view?.showHud()
        self.interactor.onRemoveVideoFavorite(with: model)
    }
    
    func onCheckVideoFavorite() -> Bool {
        if let listVideoFavorite = self.onGetListVideoFavorite() {
            let listVideoFavoriteID = listVideoFavorite.map({ item in item.id })
            if let videoID = self.onGetVideoExercise()?.id {
                if listVideoFavoriteID.contains(videoID) {
                    return true
                }
                return false
            }
        }
        return false
    }
    
    func onShareAction() {
        guard let slug = self.onGetVideoExercise()?.slug else {
            SKToast.shared.showToast(content: "Không thể chia sẻ video này")
            return
        }
        
        self.view?.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.onGetVideoExercise()?.name
        meta.socialMetaDescText = self.onGetVideoExercise()?.description
        meta.socialMetaImageURL = self.onGetVideoExercise()?.image
        
        let universalLink = "\(SHARE_URL)\(slug)\(ShareType.video.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                self.view?.hideHud()
                
                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }
                
                guard let shareView = self.view?.getShareView() else {
                    return
                }
                self.view?.showShareViewController(with: [urlShare], sourceView: shareView)
                
                print("createLinkShareFitnessPost The short URL is: \(urlShare)")
            })
            
        } else {
            self.view?.hideHud()
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }
    
    func onFormatTimeToInt(with startAt: String) -> Int {
        
        // tinh theo gio ???
        
        let abc = startAt.replacingOccurrences(of: ":", with: " ")
        
        if let range = abc.range(of: " ") {
            let firstPart = abc[abc.startIndex..<range.lowerBound]
            
            let minute = firstPart
            let minuteFormat = Int(String(minute)) ?? 0

            let secondString = abc.replacingOccurrences(of: firstPart, with: "")
            let secondFormat = secondString.replacingOccurrences(of: " ", with: "")

            let second = Int(secondFormat) ?? 0
            return (minuteFormat * 60) + second
        } else {
            return 0
        }
    }
    
    func onSaveLastSeen(with lastTime: String) {
        self.interactor.onSaveLastSeen(with: lastTime, videoID: self.videoExercise?.id ?? -1)
    }
}

// MARK: - VideoExerciseDetail InteractorOutputProtocol
extension VideoExerciseDetailViewModel: VideoExerciseDetailInteractorOutputProtocol {
    func didAddVideoFavoriteFinished(with result: Result<VideoExerciseModel, APIError>) {
        switch result {
        case .success(let model):
            print("Add \(model) to favorite successfully!!!")
        case .failure(let error):
            debugPrint(error)
        }
        self.interactor.onGetOnFavoriteVideoExercise()
        self.view?.hideHud()
    }
    
    func didGetFavoriteVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>) {
        switch result {
        case .success(let model):
            self.listVideoFavorite = model
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.updateUI()
        self.view?.hideHud()
    }
    
    func didRemoveVideoFavoriteFinished(with result: Result<[VideoExerciseModel], APIError>) {
        switch result {
        case .success(let model):
            print("remove \(model) successfully!!!")
        case .failure(let error):
            debugPrint(error)
        }
        self.interactor.onGetOnFavoriteVideoExercise()
    }
    
    func didGetRelatedVideoFinished(with result: Result<[VideoExerciseModel], APIError>) {
        switch result {
        case .success(let model):
            self.listRelatedVideo = model
            self.view?.onReloadDataRelated()
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func didSaveLastSeenFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            print("save last seen successfully")
        case .failure(let error):
            debugPrint(error)
        }
    }
}
