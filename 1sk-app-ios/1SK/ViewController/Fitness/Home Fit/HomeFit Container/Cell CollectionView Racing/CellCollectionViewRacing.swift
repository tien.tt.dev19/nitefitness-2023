//
//  CellCollectionViewRacing.swift
//  1SK
//
//  Created by Valerian on 23/06/2022.
//

import UIKit

class CellCollectionViewRacing: UICollectionViewCell {
    
    @IBOutlet weak var lbl_RacingName: UILabel!
    @IBOutlet weak var lbl_SubjectName: UILabel!
    @IBOutlet weak var img_Racing: UIImageView!
    @IBOutlet weak var lbl_NumberTotal: UILabel!
    
    var model: RaceModel? {
        didSet {
            self.lbl_RacingName.text = model?.name ?? ""
            self.lbl_SubjectName.text = model?.sport?.name?.uppercased() ?? ""
            self.lbl_NumberTotal.text = "\(model?.totalParticipants ?? 0) người"
            self.img_Racing.setImageWith(imageUrl: model?.thumbnail ?? "")
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
