//
//  
//  HomeFitContainerInteractorInput.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HomeFitContainerInteractorInputProtocol {
    func getVirtualRacing()
    func getFeaturedVideo(page: Int, perPage: Int)
    
    func getHistoryVideo(page: Int, perPage: Int)
    func getSuggestVideo(videoId: Int, categoryId: Int)
}

// MARK: - Interactor Output Protocol
protocol HomeFitContainerInteractorOutputProtocol: AnyObject {
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>)
    func onGetFeaturedVideoFinished(with result: Result<[VideoModel], APIError>)
    
    func onGetHistoryVideoFinished(with result: Result<[VideoModel], APIError>)
    func onGetSuggestVideoFinished(with result: Result<[VideoModel], APIError>)
}

// MARK: - HomeFitContainer InteractorInput
class HomeFitContainerInteractorInput {
    weak var output: HomeFitContainerInteractorOutputProtocol?
    
    var fitRaceSevice: FitRaceSeviceProtocol?
    var fitVideoSevice: FitVideoSeviceProtocol?
}

// MARK: - HomeFitContainer InteractorInputProtocol
extension HomeFitContainerInteractorInput: HomeFitContainerInteractorInputProtocol {
    func getVirtualRacing() {
        self.fitRaceSevice?.onGetListVisualRace(page: 1, perPage: 10, filterType: .opened, completion: { [weak self] result in
            self?.output?.onGetVirtualRacingFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getFeaturedVideo(page: Int, perPage: Int) {
        self.fitVideoSevice?.getVideoFeatured(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetFeaturedVideoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getHistoryVideo(page: Int, perPage: Int) {
        self.fitVideoSevice?.getVideoHistory(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetHistoryVideoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getSuggestVideo(videoId: Int, categoryId: Int) {
        self.fitVideoSevice?.getVideoSuggest(videoId: videoId, categoryId: categoryId, completion: { [weak self] result in
            self?.output?.onGetSuggestVideoFinished(with: result.unwrapSuccessModel())
        })
    }
}
