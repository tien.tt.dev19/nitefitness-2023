//
//  
//  HomeFitContainerRouter.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HomeFitContainerRouterProtocol {
    func showVideoFitDetailPlayerRouter(with model: VideoModel)
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter)
}

// MARK: - HomeFitContainer Router
class HomeFitContainerRouter {
    weak var viewController: HomeFitContainerViewController?
    
    static func setupModule() -> HomeFitContainerViewController {
        let viewController = HomeFitContainerViewController()
        let router = HomeFitContainerRouter()
        let interactorInput = HomeFitContainerInteractorInput()
        let viewModel = HomeFitContainerViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        interactorInput.fitVideoSevice = FitVideoService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HomeFitContainer RouterProtocol
extension HomeFitContainerRouter: HomeFitContainerRouterProtocol {
    func showVideoFitDetailPlayerRouter(with model: VideoModel) {
        let controller = VideoFitDetailPlayerRouter.setupModule(video: model)
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter) {
        let controller = DetailRacingRouter.setupModule(with: id, status: status)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
