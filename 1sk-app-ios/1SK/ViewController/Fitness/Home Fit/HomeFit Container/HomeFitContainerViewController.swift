//
//  
//  HomeFitContainerViewController.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol HomeFitContainerViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadDataRacing()
    func onReloadDataVideoFeatured()
    func onReloadDataVideoSuggest()
}

// MARK: - HomeFitContainer ViewController
class HomeFitContainerViewController: BaseViewController {
    var router: HomeFitContainerRouterProtocol!
    var viewModel: HomeFitContainerViewModelProtocol!
    
    @IBOutlet weak var view_Racing: UIView!
    @IBOutlet weak var view_VideoFeatured: UIView!
    @IBOutlet weak var view_VideoSuggest: UIView!
    
    @IBOutlet weak var coll_Racing: UICollectionView!
    @IBOutlet weak var coll_VideoFeatured: UICollectionView!
    @IBOutlet weak var coll_VideoSuggest: UICollectionView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewModel.onViewDidDisappear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUICollectionView()
    }
    
}

//MARK: - HomeFitContainer ViewProtocol
extension HomeFitContainerViewController: HomeFitContainerViewProtocol {
    func showHud() {
        self.showProgressHud()
    }

    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadDataRacing() {
        self.coll_Racing.reloadData()
        
        if self.viewModel.listRacing.count > 0 {
            self.view_Racing.isHidden = false
            
        } else {
            self.view_Racing.isHidden = true
        }
    }
    
    func onReloadDataVideoFeatured() {
        self.coll_VideoFeatured.reloadData()
        
        if self.viewModel.listVideoFeatured.count > 0 {
            self.view_VideoFeatured.isHidden = false
            
        } else {
            self.view_VideoFeatured.isHidden = true
        }
    }
    
    func onReloadDataVideoSuggest() {
        self.coll_VideoSuggest.reloadData()
        
        if self.viewModel.listVideoSuggest.count > 0 {
            self.view_VideoSuggest.isHidden = false
            
        } else {
            self.view_VideoSuggest.isHidden = true
        }
    }
}

//MARK: - UICollectionViewDataSource
extension HomeFitContainerViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_Racing.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_VideoFeatured.registerNib(ofType: CellCollectionViewVideoFit.self)
        self.coll_VideoSuggest.registerNib(ofType: CellCollectionViewVideoFit.self)

        self.coll_Racing.delegate = self
        self.coll_Racing.dataSource = self
        
        self.coll_VideoFeatured.delegate = self
        self.coll_VideoFeatured.dataSource = self
        
        self.coll_VideoSuggest.delegate = self
        self.coll_VideoSuggest.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.itemSize = CGSize(width: 270, height: self.coll_Racing.frame.height)
        self.coll_Racing.collectionViewLayout = layout
        
        let layoutVideoExercise = UICollectionViewFlowLayout()
        layoutVideoExercise.scrollDirection = .horizontal
        layoutVideoExercise.minimumLineSpacing = 16
        layoutVideoExercise.minimumInteritemSpacing = 16
        layoutVideoExercise.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutVideoExercise.itemSize = CGSize(width: 200, height: self.coll_VideoFeatured.frame.height)
        self.coll_VideoFeatured.collectionViewLayout = layoutVideoExercise
        
        let layoutRecommendVideoExercise = UICollectionViewFlowLayout()
        layoutRecommendVideoExercise.scrollDirection = .horizontal
        layoutRecommendVideoExercise.minimumLineSpacing = 16
        layoutRecommendVideoExercise.minimumInteritemSpacing = 16
        layoutRecommendVideoExercise.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutRecommendVideoExercise.itemSize = CGSize(width: 200, height: self.coll_VideoSuggest.frame.height)
        self.coll_VideoSuggest.collectionViewLayout = layoutRecommendVideoExercise
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.coll_Racing:
            return self.viewModel.numberOfItemsRacing()
            
        case self.coll_VideoFeatured:
            return self.viewModel.numberOfItemsVideoFeatured()
            
        case self.coll_VideoSuggest:
            return self.viewModel.numberOfItemsVideoSuggest()
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.coll_Racing:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.cellForRowRacing(at: indexPath)
            return cell

        case self.coll_VideoFeatured:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewVideoFit.self, for: indexPath)
            cell.videoFeatured = self.viewModel.cellForRowVideoFeatured(at: indexPath)
            return cell

        case self.coll_VideoSuggest:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewVideoFit.self, for: indexPath)
            cell.videoSuggest = self.viewModel.cellForRowVideoSuggest(at: indexPath)
            return cell

        default:
            return UICollectionViewCell()
        }
    }
}

//MARK: - UICollectionViewDelegate
extension HomeFitContainerViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.coll_Racing:
            let racing = self.viewModel.listRacing[indexPath.item]
            self.router.showDetailRacingRouter(with: racing.id ?? 0, status: .inprocess)
            
        case self.coll_VideoFeatured:
            self.router.showVideoFitDetailPlayerRouter(with: self.viewModel.listVideoFeatured[indexPath.row])
            
        case self.coll_VideoSuggest:
            self.router.showVideoFitDetailPlayerRouter(with: self.viewModel.listVideoSuggest[indexPath.row])
            
        default:
            break
        }
    }
}
