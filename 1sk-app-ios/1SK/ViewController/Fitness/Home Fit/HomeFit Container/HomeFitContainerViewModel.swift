//
//  
//  HomeFitContainerViewModel.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol HomeFitContainerViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onViewDidDisappear()
    
    func numberOfItemsRacing() -> Int
    func numberOfItemsVideoSuggest() -> Int
    func numberOfItemsVideoFeatured() -> Int
    
    func cellForRowRacing(at indexPath: IndexPath) -> RaceModel?
    func cellForRowVideoFeatured(at indexPath: IndexPath) -> VideoModel?
    func cellForRowVideoSuggest(at indexPath: IndexPath) -> VideoModel?
    
    // Value
    var listRacing: [RaceModel] { get set }
    var listVideoFeatured: [VideoModel] { get set }
    var listVideoSuggest: [VideoModel] { get set }
}

// MARK: - HomeFitContainer ViewModel
class HomeFitContainerViewModel {
    weak var view: HomeFitContainerViewProtocol?
    private var interactor: HomeFitContainerInteractorInputProtocol
    
    init(interactor: HomeFitContainerInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listRacing: [RaceModel] = []
    var listVideoFeatured: [VideoModel] = []
    var listVideoSuggest: [VideoModel] = []
    
    func getInitData() {
        self.view?.showHud()
        self.interactor.getVirtualRacing()
        self.interactor.getFeaturedVideo(page: 1, perPage: 10)
        self.interactor.getHistoryVideo(page: 1, perPage: 1)
    }
    
}

// MARK: - HomeFitContainer ViewModelProtocol
extension HomeFitContainerViewModel: HomeFitContainerViewModelProtocol {
    func onViewDidLoad() {
        self.getInitData()
    }
    
    func onViewDidAppear() {
        self.setInitObserverEvent()
    }
    
    func onViewDidDisappear() {
        self.setRemoveObserverEvent()
    }
    
    func numberOfItemsRacing() -> Int {
        return self.listRacing.count
    }
    
    func numberOfItemsVideoFeatured() -> Int {
        return self.listVideoFeatured.count
    }
    
    func numberOfItemsVideoSuggest() -> Int {
        return self.listVideoSuggest.count
    }

    func cellForRowRacing(at indexPath: IndexPath) -> RaceModel? {
        return self.listRacing[indexPath.row]
    }
    
    func cellForRowVideoFeatured(at indexPath: IndexPath) -> VideoModel? {
        return self.listVideoFeatured[indexPath.row]
    }
    
    func cellForRowVideoSuggest(at indexPath: IndexPath) -> VideoModel? {
        return self.listVideoSuggest[indexPath.row]
    }
    
}

// MARK: HandleSocketEvent
extension HomeFitContainerViewModel {
    func setInitObserverEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUserRegisterVRAction), name: .UserRegisterVR, object: nil)
    }
    
    func setRemoveObserverEvent() {
        NotificationCenter.default.removeObserver(self, name: .UserRegisterVR, object: nil)
    }
    
    @objc func onUserRegisterVRAction(notification: NSNotification) {
        guard let race = notification.object as? RaceModel else {
            return
        }
        guard self.listRacing.first(where: {$0.id == race.id}) != nil else {
            return
        }
        self.interactor.getVirtualRacing()
    }
}

// MARK: - HomeFitContainer InteractorOutputProtocol
extension HomeFitContainerViewModel: HomeFitContainerInteractorOutputProtocol {
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>) {
        switch result {
        case .success(let model):
            self.listRacing = model
            self.view?.onReloadDataRacing()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetFeaturedVideoFinished(with result: Result<[VideoModel], APIError>) {
        switch result {
        case .success(let model):
            self.listVideoFeatured = model
            self.view?.onReloadDataVideoFeatured()
            
        case .failure(let error):
            debugPrint(error)
        }
    }

    func onGetHistoryVideoFinished(with result: Result<[VideoModel], APIError>) {
        switch result {
        case .success(let model):
            if let videoId = model.first?.id, let categoryId = model.first?.category?.id {
                self.interactor.getSuggestVideo(videoId: videoId, categoryId: categoryId)
                
            } else {
                self.listVideoSuggest.removeAll()
                self.view?.onReloadDataVideoSuggest()
                self.view?.hideHud()
            }
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetSuggestVideoFinished(with result: Result<[VideoModel], APIError>) {
        switch result {
        case .success(let model):
            self.listVideoSuggest = model
            self.view?.onReloadDataVideoSuggest()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
