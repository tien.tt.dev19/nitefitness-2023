//
//  
//  VideoExerciseFitInteractorInput.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol VideoExerciseFitInteractorInputProtocol {
    func onGetVisualRacingList()
    func onGetRelatedVideoExercise(videoId: Int, categoryID: Int)
    func onGetHistoryVideoExercise()
    func onGetFeaturedVideoExercise(page: Int?,
                                    per_page: Int?,
                                    popular: Bool?,
                                    isFeatured: Int?,
                                    coach_id: Int?,
                                    slug: Int?,
                                    category_id: Int?,
                                    column: String?,
                                    sort: String?)
}

// MARK: - Interactor Output Protocol
protocol VideoExerciseFitInteractorOutputProtocol: AnyObject {
    func didGetVisualRacingListFinished(with result: Result<[VisualRaceModel], APIError>)
    func didGetRelatedVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>)
    func didGetFeaturedVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>, totalPages: Int)
    func didGetHistoryVideoExerciseFinished(with result: Result<[VideoExerciseModel], APIError>)
}

// MARK: - VideoExerciseFit InteractorInput
class VideoExerciseFitInteractorInput {
    weak var output: VideoExerciseFitInteractorOutputProtocol?
    
    var visualRacingService: VisualRaceSeviceProtocol?
    var videoExerciseService: VideoExerciseSeviceProtocol?
}

// MARK: - VideoExerciseFit InteractorInputProtocol
extension VideoExerciseFitInteractorInput: VideoExerciseFitInteractorInputProtocol {
    func onGetVisualRacingList() {
        self.visualRacingService?.onGetListVisualRace(completion: { [weak self] result in
            self?.output?.didGetVisualRacingListFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetFeaturedVideoExercise(page: Int?, per_page: Int?, popular: Bool?, isFeatured: Int?, coach_id: Int?, slug: Int?, category_id: Int?, column: String?, sort: String?) {
        self.videoExerciseService?.onGetListVideoExercise(page: page,
                                                          per_page: per_page,
                                                          popular: popular,
                                                          isFeatured: isFeatured,
                                                          coach_id: coach_id,
                                                          slug: slug,
                                                          category_id: category_id,
                                                          column: column,
                                                          sort: sort,
                                                          completion: { [weak self] result in
            let numberOfPage = result.getMeta()?.pagination?.totalPages
            self?.output?.didGetFeaturedVideoExerciseFinished(with: result.unwrapSuccessModel(), totalPages: numberOfPage!)
        })
    }
    
    func onGetRelatedVideoExercise(videoId: Int, categoryID: Int) {
        self.videoExerciseService?.onGetRelatedVideoExercise(videoID: videoId, categoryID: categoryID, completion: { [weak self] result in
            self?.output?.didGetRelatedVideoExerciseFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetHistoryVideoExercise() {
        self.videoExerciseService?.onGetVideoUserHistory(completion: { [weak self] result in
            self?.output?.didGetHistoryVideoExerciseFinished(with: result.unwrapSuccessModel())
        })
    }
}
