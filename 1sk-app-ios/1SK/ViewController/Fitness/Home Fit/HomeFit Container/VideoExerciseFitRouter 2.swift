//
//  
//  VideoExerciseFitRouter.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol VideoExerciseFitRouterProtocol {
    func showVideoExerciseDetail(with model: VideoExerciseModel)
}

// MARK: - VideoExerciseFit Router
class VideoExerciseFitRouter {
    weak var viewController: VideoExerciseFitViewController?
    
    static func setupModule() -> VideoExerciseFitViewController {
        let viewController = VideoExerciseFitViewController()
        let router = VideoExerciseFitRouter()
        let interactorInput = VideoExerciseFitInteractorInput()
        let viewModel = VideoExerciseFitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.visualRacingService = VisualRaceService()
        interactorInput.videoExerciseService = VideoExerciseService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - VideoExerciseFit RouterProtocol
extension VideoExerciseFitRouter: VideoExerciseFitRouterProtocol {
    func showVideoExerciseDetail(with model: VideoExerciseModel) {
        let controller = VideoExerciseDetailRouter.setupModule(with: model)
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
