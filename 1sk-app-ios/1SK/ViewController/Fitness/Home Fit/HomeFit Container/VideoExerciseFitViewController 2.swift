//
//  
//  VideoExerciseFitViewController.swift
//  1SK
//
//  Created by Valerian on 22/06/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol VideoExerciseFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
}

// MARK: - VideoExerciseFit ViewController
class VideoExerciseFitViewController: BaseViewController {
    var router: VideoExerciseFitRouterProtocol!
    var viewModel: VideoExerciseFitViewModelProtocol!
    
    @IBOutlet weak var coll_RacingList: UICollectionView!
    @IBOutlet weak var coll_VideoExercise: UICollectionView!
    @IBOutlet weak var coll_RecommendVideoExercise: UICollectionView!
    @IBOutlet weak var stack_MainView: UIStackView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setUpCollectionView()
    }
    
    // MARK: - Action
    
}

//MARK: - UICollectionViewDelegate
extension VideoExerciseFitViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case coll_RacingList:
            print("ada")
        case coll_VideoExercise:
            self.router.showVideoExerciseDetail(with: self.viewModel.onGetAllFeaturedVideos()[indexPath.row])
        case coll_RecommendVideoExercise:
            self.router.showVideoExerciseDetail(with: self.viewModel.onGetAllReleatedVideos()[indexPath.row])
        default:
            break
        }
    }
}
//MARK: - UICollectionViewDataSource
extension VideoExerciseFitViewController: UICollectionViewDataSource {
    func setUpCollectionView() {
        self.coll_RacingList.registerNib(ofType: CellCollectionRacingLists.self)
        self.coll_VideoExercise.registerNib(ofType: CellCollectionVideoExercise.self)
        self.coll_RecommendVideoExercise.registerNib(ofType: CellCollectionVideoExercise.self)

        self.coll_RacingList.delegate = self
        self.coll_RacingList.dataSource = self
        
        self.coll_VideoExercise.delegate = self
        self.coll_VideoExercise.dataSource = self
        
        self.coll_RecommendVideoExercise.delegate = self
        self.coll_RecommendVideoExercise.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.itemSize = CGSize(width: 270, height: self.coll_RacingList.frame.height)
        self.coll_RacingList.collectionViewLayout = layout
        
        let layoutVideoExercise = UICollectionViewFlowLayout()
        layoutVideoExercise.scrollDirection = .horizontal
        layoutVideoExercise.minimumLineSpacing = 16
        layoutVideoExercise.minimumInteritemSpacing = 16
        layoutVideoExercise.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutVideoExercise.itemSize = CGSize(width: 200, height: self.coll_VideoExercise.frame.height)
        self.coll_VideoExercise.collectionViewLayout = layoutVideoExercise
        
        let layoutRecommendVideoExercise = UICollectionViewFlowLayout()
        layoutRecommendVideoExercise.scrollDirection = .horizontal
        layoutRecommendVideoExercise.minimumLineSpacing = 16
        layoutRecommendVideoExercise.minimumInteritemSpacing = 16
        layoutRecommendVideoExercise.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutRecommendVideoExercise.itemSize = CGSize(width: 200, height: self.coll_RecommendVideoExercise.frame.height)
        self.coll_RecommendVideoExercise.collectionViewLayout = layoutRecommendVideoExercise
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case coll_RacingList:
            return self.viewModel.numberRacingLisOfRows()
            
        case coll_VideoExercise:
            return self.viewModel.numberOfFeaturedVideoExercise()
            
        case coll_RecommendVideoExercise:
            return self.viewModel.numberOfRelatedVideoExercise()
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case coll_RacingList:
            let cell = collectionView.dequeuCell(ofType: CellCollectionRacingLists.self, for: indexPath)
            cell.model = self.viewModel.cellForRacingListRow(at: indexPath)
            return cell

        case coll_VideoExercise:
            let cell = collectionView.dequeuCell(ofType: CellCollectionVideoExercise.self, for: indexPath)
            cell.featuredVideoExercise = self.viewModel.cellForFearturedVideo(at: indexPath)
            return cell

        case coll_RecommendVideoExercise:
            let cell = collectionView.dequeuCell(ofType: CellCollectionVideoExercise.self, for: indexPath)
            cell.relatedVideoExercise = self.viewModel.cellForRelatedListRow(at: indexPath)
            return cell

        default:
            return UICollectionViewCell()
        }
    }
}

 //MARK: - VideoExerciseFit ViewProtocol
extension VideoExerciseFitViewController: VideoExerciseFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }

    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.coll_RacingList.reloadData()
        self.coll_VideoExercise.reloadData()
        if self.viewModel.onGetAllReleatedVideos().count != 0 {
            self.stack_MainView.subviews[2].isHidden = false
            self.coll_RecommendVideoExercise.reloadData()
        } else {
            self.stack_MainView.subviews[2].isHidden = true
        }
    }
}
