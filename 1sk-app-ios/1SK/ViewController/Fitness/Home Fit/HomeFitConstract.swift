//
//  
//  HomeFitConstract.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

// MARK: - View
protocol HomeFitViewProtocol: AnyObject {
    func onUpdateBadgeNotification(count: Int)
    func onSetTabBarSelectedIndex(_ index: Int)
}

// MARK: - Presenter
protocol HomeFitPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func onSearchAction()
    func onNotificationAction()
    
    func onVideoTrainingAction()
    func onVirtualRaceAction()
    func onRunningAction()
    func onWalkingAction()
}

// MARK: - Interactor Input
protocol HomeFitInteractorInputProtocol {
    func getHotlinePhoneNumber()
    func getCountBadgeNotificationCare()
    func getUserTags()
    func generateHashID()
    
    func getBlogDetailById(blogId: Int)
    func getVideoDetailById(videoId: Int)
    func getDoctorDetailById(doctorId: Int)
    
    func getAppointmentDetail(id: Int)
    
    func getBlogDetailBySlug(slug: String, type: String)
    func getVideoDetailBySlug(slug: String, type: String)
    
    func getMarketingPopups()
    
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], objectSection: IndoorBikeModel)
    
    func getVirtualRaceDetailBySlug(slug: String)
}

// MARK: - Interactor Output
protocol HomeFitInteractorOutputProtocol: AnyObject {
    func onGetCountBadgeNotificationCareFinished(with result: Result<[NotificationModel], APIError>, meta: Meta?)
    func ongenerateHashIDFinished(with result: Result<String, APIError>)
    func onGetTagsFinished(with result: Result<[UserTag], APIError>)
    func onGetHotlinePhoneNumberFinished(with result: Result<HotlineModel, APIError>)

    func onGetBlogDetailByIdFinished(with result: Result<BlogModel, APIError>)
    func onGetVideoDetailByIdFinished(with result: Result<VideoModel, APIError>)
    func onGetDoctorDetailByIdFinished(with result: Result<DoctorModel, APIError>)
    
    func onGetAppointmentDetailFinished(with result: Result<AppointmentModel, APIError>)
    
    func onGetBlogDetailsBySlugFinished(with result: Result<BlogModel, APIError>)
    func onGetVideoDetailsBySlugFinished(with result: Result<VideoModel, APIError>)
    
    func onGetMarketingPopupsFinished(with result: Result<[PopupModel], APIError>)
    
    func onSetUploadLogFileBikeWokoutFinished(with result: Result<[IndoorBikeListData], APIError>, objectSection: IndoorBikeModel)

    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>)
}

// MARK: - Router
protocol HomeFitRouterProtocol: BaseRouterProtocol {
    func gotoSearchFit()
    func gotoNotificationViewController(pushNotiModel: PushNotiModel?)
    func showListVideoExerciseRouter()
    func showVirtualRaceFitRouter()
    func showRunningFitRouter()
    func showWalkingFitRouter()

    func gotoBlogDetailsViewController(blogModel: BlogModel, categoryType: CategoryType)
    func gotoVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType)

    func gotoCareViewController()

    func gotoDetailConsultingVC(appointment: AppointmentModel)

    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?)
    
    func gotoHealthServiceVC(doctor: DoctorModel?)
    
    func showPopupMKTViewController(listPopup: [PopupModel], delegate: PopupMKTViewDelegate?)
    
    func gotoDetailVirtualRaceDetailsViewController(raceModel: RaceModel)
    
    func showDetailRacingRouter(raceId: Int, status: TypeRaceFilter)
}
