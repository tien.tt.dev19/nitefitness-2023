//
//  
//  HomeFitInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

class HomeFitInteractorInput {
    weak var output: HomeFitInteractorOutputProtocol?
    
    var configService: ConfigServiceProtocol?
    
    var careService: CareServiceProtocol?
    
    var exerciseService: ExerciseServiceProtocol?
    
    var blogService: BlogServiceProtocol?
    var fitnessService: FitnessServiceProtocol?
    
    var marketingService: MarketingServiceProtocol?
    
    var fitService: FitServiceProtocol?
    
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - HomeFitInteractorInputProtocol
extension HomeFitInteractorInput: HomeFitInteractorInputProtocol {
    func getCountBadgeNotificationCare() {
        self.careService?.getListNotificationUnread(page: 1, limit: 1, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetCountBadgeNotificationCareFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
    
    func generateHashID() {
        self.careService?.generateHasUserID( completion: { [weak self] result in
            self?.output?.ongenerateHashIDFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getUserTags() {
        self.careService?.getUserTags( completion: { [weak self] result in
            self?.output?.onGetTagsFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getHotlinePhoneNumber() {
        self.careService?.getHotlinePhoneNumber(completion: { [weak self] result in
            self?.output?.onGetHotlinePhoneNumberFinished(with: result.unwrapSuccessModel())
        })
    }
        
    func getBlogDetailById(blogId: Int) {
        self.blogService?.getBlogDetail(with: blogId, completion: { [weak self] result in
            self?.output?.onGetBlogDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getVideoDetailById(videoId: Int) {
        self.fitnessService?.getVideoDetailByID(with: videoId, completion: { [weak self] result in
            self?.output?.onGetVideoDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getDoctorDetailById(doctorId: Int) {
        self.careService?.getDoctorDetailById(doctorId: doctorId, completion: { [weak self] result in
            self?.output?.onGetDoctorDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getAppointmentDetail(id: Int) {
        self.careService?.getAppointmentDetail(appointmentId: id, completion: { [weak self] result in
            self?.output?.onGetAppointmentDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getBlogDetailBySlug(slug: String, type: String) {
        self.blogService?.getBlogBySlug(slug: slug, type: type, completion: { [weak self] result in
            self?.output?.onGetBlogDetailsBySlugFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getVideoDetailBySlug(slug: String, type: String) {
        self.fitnessService?.getVideoBySlug(slug: slug, type: type, completion: { [weak self] result in
            self?.output?.onGetVideoDetailsBySlugFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getMarketingPopups() {
        self.marketingService?.getPopupList(completion: { [weak self] result in
            self?.output?.onGetMarketingPopupsFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], objectSection: IndoorBikeModel) {
        self.fitService?.setUploadLogFileBikeWokout(sectionId: sectionId, startTimeSection: startTimeSection, endTimeSection: endTimeSection, filePaths: filePaths, completion: { [weak self] result in
            self?.output?.onSetUploadLogFileBikeWokoutFinished(with: result.unwrapSuccessModel(), objectSection: objectSection)
        })
    }
    
    func getVirtualRaceDetailBySlug(slug: String) {
        self.fitRaceSevice?.onGetVirtualRaceDetail(by: slug, completion: { [weak self] result in
            self?.output?.onGetVirtualRaceDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
