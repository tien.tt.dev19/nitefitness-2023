//
//  
//  HomeFitPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit
import OneSignal

class HomeFitPresenter {

    weak var view: HomeFitViewProtocol?
    private var interactor: HomeFitInteractorInputProtocol
    private var router: HomeFitRouterProtocol
    
    var listTags: [UserTag] = []

    init(interactor: HomeFitInteractorInputProtocol,
         router: HomeFitRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    func getInitData() {
        self.interactor.getHotlinePhoneNumber()
        self.interactor.generateHashID()
        self.interactor.getUserTags()
        self.interactor.getCountBadgeNotificationCare()
    }
    
    func setSyncFitLogDataFile() {
        FileHelper.shared.printContentsOfDocumentDirectory(title: "./fit/log:", path: "/fit/log/")
        let realm: RealmManagerProtocol = RealmManager()
        let listSection: [IndoorBikeModel] = realm.objects()
        
        guard listSection.count > 0 else {
            FileHelper.shared.setRemoveDirectory(path: "/fit/log")
            return
        }
        
        for object in listSection {
            print("FileLogData id:", object.id)
            print("FileLogData device_name:", object.device_name ?? "null")
            print("FileLogData device_type:", object.device_type ?? "null")
            print("FileLogData start_time:", object.start_time ?? -1)
            print("FileLogData end_time:", object.end_time ?? -1)
            print("FileLogData -----------------------")
            
            for file in object.log_path {
                print("FileLogData                id:", file.id)
                print("FileLogData                file_name:", file.file_name ?? -1)
                print("FileLogData                end_time:", file.start_time ?? -1)
                print("FileLogData                end_time:", file.end_time ?? -1)
                print("FileLogData                -----------------------")
            }
            
            if let startTimeSection = object.start_time {
                let endTimeSection = object.end_time ?? 0

                let filePaths = object.log_path.array.map { return $0.filePath }
                let listFile = FileHelper.shared.getFileInDirectory(path: "/fit/log/")

                if filePaths.count > 0, listFile?.count ?? 0 > 0 {
                    self.interactor.setUploadLogFileBikeWokout(sectionId: object.id, startTimeSection: startTimeSection, endTimeSection: endTimeSection, filePaths: filePaths, objectSection: object)
                    
                } else {
                    print("FileLogData remove id:", object.id)
                    print("FileLogData ")
                    realm.remove(object)
                }
            }
        }
        
    }
}

// MARK: - HomeFitPresenterProtocol
extension HomeFitPresenter: HomeFitPresenterProtocol {
    func onViewDidLoad() {
        self.getInitData()
        self.setInitHandleSocketEvent()
        self.setInitUIApplicationOneSignal()
        self.setInitUIApplicationDeepLinks()
        self.setSyncFitLogDataFile()
        
        self.onHandleNotificationOpenBackground(pushNotiModel: gPushNotiModel)
    }
    
    func onViewDidAppear() {
        self.view?.onUpdateBadgeNotification(count: gBadgeCare)
        
        self.interactor.getCountBadgeNotificationCare()
        
        if APP_ENV == .PRO {
            self.interactor.getMarketingPopups()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.onHandleOpenDeepLinks()
        }
    }
    
    // Action
    func onSearchAction() {
        self.router.gotoSearchFit()
    }
    
    func onNotificationAction() {
        self.router.gotoNotificationViewController(pushNotiModel: nil)
    }
    
    func onVideoTrainingAction() {
        self.router.showListVideoExerciseRouter()
    }
    
    func onVirtualRaceAction() {
        self.router.showVirtualRaceFitRouter()
    }
    
    func onRunningAction() {
        self.router.showRunningFitRouter()
    }
    
    func onWalkingAction() {
        self.router.showWalkingFitRouter()
    }
    
}

// MARK: - HomeFitInteractorOutput 
extension HomeFitPresenter: HomeFitInteractorOutputProtocol {
    func onGetCountBadgeNotificationCareFinished(with result: Result<[NotificationModel], APIError>, meta: Meta?) {
        switch result {
        case .success:
            gBadgeCare = meta?.total ?? 0
            self.view?.onUpdateBadgeNotification(count: gBadgeCare)
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func ongenerateHashIDFinished(with result: Result<String, APIError>) {
        switch result {
        case .success(let hashID):
            if let userID = gUser?.id {
                let id = String(userID)
                
                OneSignal.setExternalUserId(id, withExternalIdAuthHashToken: hashID, withSuccess: { results in
                    // The results will contain push and email success statuses
                    print("<OneSignal> External user id update complete with results: ", results!.description)
                    // Push can be expected in almost every situation with a success status, but
                    // as a pre-caution its good to verify it exists
                    if let pushResults = results!["push"] {
                        print("<OneSignal> Set external user id push status: ", pushResults)
                    }
                    if let emailResults = results!["email"] {
                        print("<OneSignal> Set external user id email status: ", emailResults)
                    }
                    if let smsResults = results!["sms"] {
                        print("<OneSignal> Set external user id sms status: ", smsResults)
                    }
                    
                }, withFailure: {error in
                    print("<OneSignal> Set external user id done with error: " + error.debugDescription)
                })
            }
            
        case .failure:
            break
        }
    }
    
    func onGetTagsFinished(with result: Result<[UserTag], APIError>) {
        switch result {
        case .success(let listTag):
            self.listTags = listTag
            for tag in listTags {
                OneSignal.sendTag(tag.key!, value: tag.value!)
            }
            
        case .failure:
            break
        }
    }
    
    func onGetHotlinePhoneNumberFinished(with result: Result<HotlineModel, APIError>) {
        switch result {
        case .success(let model):
            gPhoneHotline = model.hotline ?? ""

        case .failure:
            break
        }
    }
    
    func onGetBlogDetailByIdFinished(with result: Result<BlogModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.blogFit.rawValue:
                    self.router.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogFit)
                    
                case CategoryType.blogCare.rawValue:
                    self.router.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
    
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>) {
        switch result {
        case .success(let model):
            self.router.gotoDetailVirtualRaceDetailsViewController(raceModel: model)
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.router.hideHud()
    }
    
    func onGetVideoDetailByIdFinished(with result: Result<VideoModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.videoFit.rawValue:
                    self.router.gotoVideoDetailsViewController(with: model, categoryType: .videoFit)
                    
                case CategoryType.videoCare.rawValue:
                    self.router.gotoVideoDetailsViewController(with: model, categoryType: .videoCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
    
    func onGetDoctorDetailByIdFinished(with result: Result<DoctorModel, APIError>) {
        switch result {
        case .success(let model):
            self.setGoToDoctorDetailView(doctor: model)
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
    
    func onGetAppointmentDetailFinished(with result: Result<AppointmentModel, APIError>) {
        switch result {
        case .success(let model):
            self.onGetAppointmentDetailSuccess(appointment: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetBlogDetailsBySlugFinished(with result: Result<BlogModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.blogFit.rawValue:
                    self.router.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogFit)
                    
                case CategoryType.blogCare.rawValue:
                    self.router.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
    
    func onGetVideoDetailsBySlugFinished(with result: Result<VideoModel, APIError>) {
        switch result {
        case .success(let model):
            switch model.videoType {
            case 1: // FIT
                self.router.gotoVideoDetailsViewController(with: model, categoryType: .videoFit)
                
            case 2: // CARE
                self.router.gotoVideoDetailsViewController(with: model, categoryType: .videoCare)
                
            default:
                break
            }
            
        case .failure:
            break
        }
        self.router.hideHud()

    }
    
    func onGetMarketingPopupsFinished(with result: Result<[PopupModel], APIError>) {
        switch result {
        case .success(let listModel):
            if listModel.count > 0 {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.router.showPopupMKTViewController(listPopup: listModel, delegate: self)
                }
            }
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
    
    func onSetUploadLogFileBikeWokoutFinished(with result: Result<[IndoorBikeListData], APIError>, objectSection: IndoorBikeModel) {
        switch result {
        case .success(let model):
            print("./UPLOAD: Finished true: ", model)
            
            IndoorBikeManager.shared.onRemoveFileLogData(objectSection: objectSection)
            
        case .failure(let error):
            print("./UPLOAD: Finished false: ", error)
        }
    }
}

// MARK: HandleSocketEvent
extension HomeFitPresenter {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPopupLiveReadyToSendEvent), name: .PopupLiveReadyToSendEvent, object: nil)
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        self.view?.onUpdateBadgeNotification(count: gBadgeCare)
    }
    
    @objc func onPopupLiveReadyToSendEvent(notification: NSNotification) {
        print("socket.on care: PopupLiveReadyToSendEvent HomePresenter")
        
        if let popup = notification.object as? PopupModel {
            self.view?.onSetTabBarSelectedIndex(0)
            var listModel: [PopupModel] = []
            listModel.append(popup)
            self.router.showPopupMKTViewController(listPopup: listModel, delegate: self)
        }
    }
}

// MARK: UIApplication - OneSignal
extension HomeFitPresenter {
    func setInitUIApplicationOneSignal() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleNotificationOpenForeground(_:)), name: .applicationOSNotificationOpenedBlock, object: nil)
    }
    
    @objc func onHandleNotificationOpenForeground(_ notification: Notification) {
        if let object = notification.object as? PushNotiModel {
            self.view?.onSetTabBarSelectedIndex(0)
            
            switch object.screen {
            case .appointment:
                guard let appointmentId = object.data?.id else { return }
                self.router.showHud()
                self.interactor.getAppointmentDetail(id: appointmentId)
                
            case .vr_detail:
                guard let raceId = object.data?.id else { return }
                self.router.showDetailRacingRouter(raceId: raceId, status: .registered)
                
            default:
                break
            }
        }
        gPushNotiModel = nil
    }
    
    func onHandleNotificationOpenBackground(pushNotiModel: PushNotiModel?) {
        if let object = pushNotiModel {
            switch object.screen {
            case .appointment:
                guard let appointmentId = object.data?.id else { return }
                self.router.showHud()
                self.interactor.getAppointmentDetail(id: appointmentId)
                
            case .vr_detail:
                guard let raceId = object.data?.id else { return }
                self.router.showDetailRacingRouter(raceId: raceId, status: .registered)
                
            default:
                break
            }
        }
        gPushNotiModel = nil
    }
    
    func onGetAppointmentDetailSuccess(appointment: AppointmentModel) {
        guard let statusId = appointment.status?.id else {
            return
        }
        switch statusId {
        case AppointmentStatus.complete.id, AppointmentStatus.finish.id:
            self.router.gotoDetailConsultingVC(appointment: appointment)
            
        default:
            self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: nil)
        }
    }
}

// MARK: UIApplication - DeepLinks
extension HomeFitPresenter {
    func setInitUIApplicationDeepLinks() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleOpenDeepLinks), name: .applicationOpenDeepLinks, object: nil)
    }
    
    @objc func onHandleOpenDeepLinks() {
        //print("<AppDelegate> onOpenDeepLinks gUniversalLink: \(String(describing: gUniversalLink))")
        
        if let universalLink = gUniversalLink {
            self.view?.onSetTabBarSelectedIndex(0)
            
            let path = universalLink.path.split(separator: "/")
            if path.count > 0 {
                var pathSlug = "\(path[0])"
                
                let indexType = pathSlug.index(pathSlug.endIndex, offsetBy: -2)
                let shareType = "\(pathSlug[indexType...])"
                
                let indexObject = pathSlug.index(pathSlug.endIndex, offsetBy: -3)
                let objectId = "\(pathSlug[...indexObject])"
                print("<AppDelegate> onOpenDeepLinks shareType ", shareType)
                print("<AppDelegate> onOpenDeepLinks objectId ", objectId)
                
                switch shareType {
                case ShareType.blog.rawValue:
                    if pathSlug.count > 0 {
                        pathSlug.removeLast()
                        pathSlug.removeLast()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.router.showHud()
                            self.interactor.getBlogDetailBySlug(slug: pathSlug, type: shareType)
                        }
                    }

                case ShareType.video.rawValue:
                    if pathSlug.count > 0 {
                        pathSlug.removeLast()
                        pathSlug.removeLast()
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.router.showHud()
                            self.interactor.getVideoDetailBySlug(slug: pathSlug, type: shareType)
                        }
                    }
                    
                case ShareType.doctor.rawValue:
                    if let doctorId = Int(objectId) {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            self.router.showHud()
                            self.interactor.getDoctorDetailById(doctorId: doctorId)
                        }
                    }

                case ShareType.workout.rawValue:
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        //self.router.showHud()
                        //self.interactor.getWorkoutDetailById(workoutId: shareId)
                    }

                case ShareType.virtualRace.rawValue:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.router.showHud()
                        self.interactor.getVirtualRaceDetailBySlug(slug: objectId)
                    }
                default:
                    break
                }
                
                gUniversalLink = nil
            }
            
        }
    }
}

// MARK: PopupMKTViewDelegate
extension HomeFitPresenter: PopupMKTViewDelegate {
    func onSelectPopupAction(popup: PopupModel) {
        switch popup.typeObj {
        case .None:
            self.router.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.router.hideHud()
                guard let url = URL(string: popup.destinationUrl ?? "") else { return }
                UIApplication.shared.open(url)
            }
            
        case .Video:
            guard let objectId = popup.objectId else {
                return
            }
            self.router.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.interactor.getVideoDetailById(videoId: objectId)
            }
            
        case .Blog:
            guard let objectId = popup.objectId else {
                return
            }
            self.router.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.interactor.getBlogDetailById(blogId: objectId)
            }
            
        case .Doctor:
            guard let objectId = popup.objectId else {
                return
            }
            self.router.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.interactor.getDoctorDetailById(doctorId: objectId)
            }
            
        case .Booking:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.router.gotoCareViewController()
            }
            
        default:
            break
        }
        
    }
}

// MARK: - DoctorDetailPresenterDelegate
extension HomeFitPresenter: DoctorDetailPresenterDelegate {
    func setGoToDoctorDetailView(doctor: DoctorModel?) {
        self.router.gotoDoctorDetailVC(doctor: doctor, isFrom: "HomeAppLandingViewController", delegate: self)
    }
    
    func onDetailConfirmBookingAction(doctor: DoctorModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.router.gotoHealthServiceVC(doctor: doctor)
        }
    }
}
