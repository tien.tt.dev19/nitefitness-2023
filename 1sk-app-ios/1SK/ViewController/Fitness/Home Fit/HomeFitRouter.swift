//
//
//  HomeFitRouter.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

class HomeFitRouter: BaseRouter {
    static func setupModule() -> HomeFitViewController {
        let viewController = HomeFitViewController()
        let router = HomeFitRouter()
        let interactorInput = HomeFitInteractorInput()
        let presenter = HomeFitPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        interactorInput.configService = ConfigService()
        interactorInput.exerciseService = ExerciseService()
        interactorInput.blogService = BlogService()
        interactorInput.fitnessService = FitnessService()
        interactorInput.marketingService = MarketingService()
        interactorInput.fitService = FitService()
        interactorInput.fitRaceSevice = FitRaceService()
        
        router.viewController = viewController
        return viewController
    }
}

// MARK: - HomeFitRouterProtocol
extension HomeFitRouter: HomeFitRouterProtocol {
    func gotoSearchFit() {
        let controller = SearchFitRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoNotificationViewController(pushNotiModel: PushNotiModel?) {
        let notificationVC = NotificationRouter.setupModule(pushNotiModel: pushNotiModel)
        self.viewController?.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    func showListVideoExerciseRouter() {
        let controller = CategoryVideoFitRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showVirtualRaceFitRouter() {
        let controller = VirtualRaceFitRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showRunningFitRouter() {
        let controller = RunningFitRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showWalkingFitRouter() {
        let controller = WalkingFitRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoBlogDetailsViewController(blogModel: BlogModel, categoryType: CategoryType) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: categoryType)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType) {
        switch categoryType {
        case .videoFit:
            let controller = VideoFitDetailPlayerRouter.setupModule(video: video)
            self.viewController?.navigationController?.show(controller, sender: nil)
            
        case .videoCare:
            let controller = PlayVideoRouter.setupModule(with: video, categoryType: categoryType)
            self.viewController?.navigationController?.pushViewController(controller, animated: true)
            
        default:
            break
        }
    }
    
    func gotoCareViewController() {
        let controller = HealthServiceRouter.setupModule(doctor: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailConsultingVC(appointment: AppointmentModel) {
        let controller = DetailConsultingRouter.setupModule(with: appointment)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?) {
        let controller = DoctorDetailRouter.setupModule(doctor: doctor, isFrom: isFrom, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoHealthServiceVC(doctor: DoctorModel?) {
        let controller = HealthServiceRouter.setupModule(doctor: doctor)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showPopupMKTViewController(listPopup: [PopupModel], delegate: PopupMKTViewDelegate?) {
        guard let tabBarController = self.viewController?.tabBarController else {
            return
        }

        if gIsShowMarketingPopup == false {
            let controller = PopupMKTViewController()
            controller.modalPresentationStyle = .custom
            controller.delegate = delegate
            controller.listPopup = listPopup
            tabBarController.present(controller, animated: false)
        }
    }
    
    func gotoDetailVirtualRaceDetailsViewController(raceModel: RaceModel) {
        if let id = raceModel.id {
            let viewController = DetailRacingRouter.setupModule(with: id, status: .registered)
            self.viewController?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func showDetailRacingRouter(raceId: Int, status: TypeRaceFilter) {
        let controller = DetailRacingRouter.setupModule(with: raceId, status: status)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
