//
//  
//  HomeFitViewController.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

class HomeFitViewController: BaseViewController {
    var presenter: HomeFitPresenterProtocol!
    
    /// Navigation Bar
    @IBOutlet weak var view_Badge: UIView!
    @IBOutlet weak var lbl_Badge: EdgeInsetLabel!
    
    @IBOutlet weak var view_Container: UIView!
    
    private lazy var videoExerciseFitRouter = HomeFitContainerRouter.setupModule()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.addChildView(childViewController: self.videoExerciseFitRouter)
    }
    
    // MARK: - Action
    
    @IBAction func onSearchAction(_ sender: Any) {
        self.presenter.onSearchAction()
    }
    
    @IBAction func onNotificationAction(_ sender: Any) {
        self.presenter.onNotificationAction()
    }
    
    // Tab Menu
    @IBAction func onVideoTrainingAction(_ sender: Any) {
        self.presenter.onVideoTrainingAction()
    }
    
    @IBAction func onVirtualRaceAction(_ sender: Any) {
        self.presenter.onVirtualRaceAction()
    }
    
    @IBAction func onRunningAction(_ sender: Any) {
        self.presenter.onRunningAction()
    }
    
    @IBAction func onWalkingAction(_ sender: Any) {
        self.presenter.onWalkingAction()
    }
}

// MARK: - Page Menu
extension HomeFitViewController {
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: - HomeFitViewProtocol
extension HomeFitViewController: HomeFitViewProtocol {
    func onUpdateBadgeNotification(count: Int) {
        self.lbl_Badge.text = count.asString
        if count > 0 {
            self.view_Badge.isHidden = false
        } else {
            self.view_Badge.isHidden = true
        }
    }
    
    func onSetTabBarSelectedIndex(_ index: Int) {
        DispatchQueue.main.async {
            self.tabBarController?.selectedIndex = index
        }
    }
}
