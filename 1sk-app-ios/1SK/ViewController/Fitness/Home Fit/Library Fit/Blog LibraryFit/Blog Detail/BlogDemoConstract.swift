//
//  
//  BlogDetailConstract.swift
//  1SK
//
//  Created by vuongbachthu on 8/23/21.
//
//

import UIKit

// MARK: - View
protocol BlogDetailViewProtocol: AnyObject {
    func setDataInit(with blog: BlogModel?)
    func onRealoadDataColl()
    func onRealoadDataTable()
}

// MARK: - Presenter
protocol BlogDetailPresenterProtocol {
    func onViewDidLoad()
    func onBackAction()
    func onShareAction(_ shareButton: UIButton)
    func onBlogListByTagViewController(_ blog: BlogModel)
    
    // UICollectionView
    func numberOfItemsInSectionColl() -> Int
    func cellForItemColl(at index: IndexPath) -> TagModel?
    func didSelectItemColl(at index: IndexPath)
    
    // UITableView
    func numberOfRowInSectionTable() -> Int
    func cellForRowTable(at index: IndexPath) -> BlogModel?
    func didSelectedItemTable(at index: IndexPath)
}

// MARK: - Interactor Input
protocol BlogDetailInteractorInputProtocol {
    func getRelatedBlogs(blogId: Int, categories: [CategoriesVideo])
    func getBlogDetails(slug: String)
    func getBlogDetails(id: Int)
    
    func setSeeBlog(blogId: Int)
    
}

// MARK: - Interactor Output
protocol BlogDetailInteractorOutputProtocol: AnyObject {
    func onGetRelatedBlogsFinish(with result: Result<[BlogModel], APIError>)
    func onGetBlogDetailsFinished(with result: Result<BlogModel, APIError>)
    
    func onSetSeeBlogsFinish(with result: Result<EmptyModel, APIError>)
    
}

// MARK: - Router
protocol BlogDetailRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
    func gotoBlogListByTagViewController(with tag: TagModel, categoryType: CategoryType?)
}
