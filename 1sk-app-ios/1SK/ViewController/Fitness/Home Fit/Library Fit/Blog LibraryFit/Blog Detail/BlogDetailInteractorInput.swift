//
//  
//  BlogDetailInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 8/23/21.
//
//

import UIKit

class BlogDetailInteractorInput {
    weak var output: BlogDetailInteractorOutputProtocol?
    var blogService: BlogServiceProtocol?
}

// MARK: - BlogDetailInteractorInputProtocol
extension BlogDetailInteractorInput: BlogDetailInteractorInputProtocol {
    func getRelatedBlogs(blogId: Int, categories: [CategoriesVideo]) {
        self.blogService?.getListRelatedBlog(blogId: blogId, categories: categories, completion: { [weak self] result in
            self?.output?.onGetRelatedBlogsFinish(with: result.unwrapSuccessModel())
        })
    }

    func getBlogDetails(slug: String) {
        self.blogService?.findBlogBySlug(slug: slug, completion: { [weak self] result in
            self?.output?.onGetBlogDetailsFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getBlogDetails(id: Int) {
        self.blogService?.getBlogDetail(with: id, completion: { [weak self] result in
            self?.output?.onGetBlogDetailsFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setSeeBlog(blogId: Int) {
        self.blogService?.setSeeBlog(blogId: blogId, completion: { [weak self] result in
            self?.output?.onSetSeeBlogsFinish(with: result.unwrapSuccessModel())
        })
    }
}
