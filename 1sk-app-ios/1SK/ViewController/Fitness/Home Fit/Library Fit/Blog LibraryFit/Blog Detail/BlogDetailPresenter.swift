//
//  
//  BlogDetailPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 8/23/21.
//
//

import UIKit

class BlogDetailPresenter {

    weak var view: BlogDetailViewProtocol?
    private var interactor: BlogDetailInteractorInputProtocol
    private var router: BlogDetailRouterProtocol

    init(interactor: BlogDetailInteractorInputProtocol,
         router: BlogDetailRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var categoryType: CategoryType?
    var blog: BlogModel?
    var relatedBlogs: [BlogModel] = []
    
    // MARK: - API
    private func getListRelated() {
        guard let blogId = self.blog?.id else {
            return
        }
        guard let categories = self.blog?.categories else {
            return
        }
        self.interactor.getRelatedBlogs(blogId: blogId, categories: categories)
    }

    private func getBlogDetails() {
        guard let blogId = self.blog?.id else {
            return
        }
        self.interactor.getBlogDetails(id: blogId)
        
        if self.blog?.categoryType == .blogFit {
            self.interactor.setSeeBlog(blogId: blogId)
        }
    }
    
    private func getInitDataDetail() {
        self.router.showHud()
        self.getBlogDetails()
        
    }
    
    private func updateWithNewBlog(_ newBlog: BlogModel?) {
        self.blog = newBlog
        self.relatedBlogs = []
        self.getInitDataDetail()
    }
}

// MARK: - BlogDetailPresenterProtocol
extension BlogDetailPresenter: BlogDetailPresenterProtocol {
    func onViewDidLoad() {
        self.getInitDataDetail()
    }
    
    func onBackAction() {
        self.router.popViewController()
    }
    
    func onShareAction(_ shareButton: UIButton) {
        guard let slug = self.blog?.friendlyUrl else {
            SKToast.shared.showToast(content: "Không thể chia sẻ bài viết này")
            return
        }
        
        self.router.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.blog?.title
        meta.socialMetaDescText = self.blog?.shortDescription
        meta.socialMetaImageURL = self.blog?.thumbImage
        
        let universalLink = "\(SHARE_URL)\(slug)\(ShareType.blog.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                self.router.hideHud()

                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }

                self.router.showShareViewController(with: [urlShare], sourceView: shareButton)
                print("createLinkShareFitnessPost The short URL is: \(urlShare)")
            })

        } else {
            self.router.hideHud()
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }
    
    func onBlogListByTagViewController(_ blog: BlogModel) {
        self.updateWithNewBlog(blog)
    }
    
    // MARK: - UICollectionView
    func numberOfItemsInSectionColl() -> Int {
        return self.blog?.tag?.count ?? 0
    }
    
    func cellForItemColl(at index: IndexPath) -> TagModel? {
        return self.blog?.tag?[index.row]
    }
    
    func didSelectItemColl(at index: IndexPath) {
        guard let tag = self.blog?.tag?[index.row] else {
            return
        }
        self.router.gotoBlogListByTagViewController(with: tag, categoryType: self.categoryType)
    }
    
    // MARK: - UITableView
    func numberOfRowInSectionTable() -> Int {
        return self.relatedBlogs.count
    }
    
    func cellForRowTable(at index: IndexPath) -> BlogModel? {
        return self.relatedBlogs[index.row]
    }
    
    func didSelectedItemTable(at index: IndexPath) {
        let selectedBlog = self.relatedBlogs[index.row]
        self.updateWithNewBlog(selectedBlog)
    }
}

// MARK: - BlogDetailInteractorOutput
extension BlogDetailPresenter: BlogDetailInteractorOutputProtocol {
    func onGetBlogDetailsFinished(with result: Result<BlogModel, APIError>) {
        switch result {
        case .success(let blog):
            self.blog = blog
            self.blog?.categoryType = self.categoryType
            
            self.view?.setDataInit(with: self.blog)
            self.view?.onRealoadDataColl()
            
            self.getListRelated()
            
        case .failure:
            SKToast.shared.showToast(content: "Không thể kết nối máy chủ")
        }
        self.view?.onRealoadDataTable()
        self.router.hideHud()
    }
    
    func onGetRelatedBlogsFinish(with result: Result<[BlogModel], APIError>) {
        switch result {
        case .success(let relatedBlogs):
            for item in relatedBlogs {
                switch self.categoryType {
                case .blogFit:
                    item.categoryType = .blogFit
                    
                case .blogCare:
                    item.categoryType = .blogCare
                    
                default:
                    break
                }
            }
            
            self.relatedBlogs = relatedBlogs
            self.view?.onRealoadDataTable()
            
        case .failure:
            SKToast.shared.showToast(content: "Không thể kết nối máy chủ")
        }
        
        self.router.hideHud()
    }
    
    func onSetSeeBlogsFinish(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            break
            
        case .failure:
            break
        }
    }
}
