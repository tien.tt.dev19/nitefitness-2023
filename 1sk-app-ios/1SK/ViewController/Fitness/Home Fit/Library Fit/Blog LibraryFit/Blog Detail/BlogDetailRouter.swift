//
//  
//  BlogDetailRouter.swift
//  1SK
//
//  Created by vuongbachthu on 8/23/21.
//
//

import UIKit

class BlogDetailRouter: BaseRouter {
    static func setupModule(blogModel: BlogModel, categoryType: CategoryType?) -> BlogDetailViewController {
        let viewController = BlogDetailViewController()
        viewController.hidesBottomBarWhenPushed = true
        let router = BlogDetailRouter()
        let interactorInput = BlogDetailInteractorInput()
        let presenter = BlogDetailPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.blog = blogModel
        presenter.categoryType = categoryType
        interactorInput.output = presenter
        interactorInput.blogService = BlogService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BlogDetailRouterProtocol
extension BlogDetailRouter: BlogDetailRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        let shareVC = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        shareVC.popoverPresentationController?.sourceView = sourceView
        shareVC.popoverPresentationController?.sourceRect = sourceView.bounds
        viewController?.present(shareVC, animated: true, completion: nil)
    }
    
    func gotoBlogListByTagViewController(with tag: TagModel, categoryType: CategoryType?) {
        guard let `viewController` = viewController as? BlogDetailViewController else {
            return
        }
        let blogListByTagVC = BlogListByTagRouter.setupModule(tag: tag, categoryType: categoryType, delegate: viewController)
        viewController.navigationController?.pushViewController(blogListByTagVC, animated: true)
    }
}
