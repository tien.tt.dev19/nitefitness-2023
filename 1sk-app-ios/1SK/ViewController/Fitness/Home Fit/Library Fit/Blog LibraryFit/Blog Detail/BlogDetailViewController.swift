//
//  
//  BlogDetailViewController.swift
//  1SK
//
//  Created by vuongbachthu on 8/23/21.
//
//

import UIKit
import WebKit

class BlogDetailViewController: BaseViewController {

    var presenter: BlogDetailPresenterProtocol!
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_AuthorName: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var img_IconAuthor: UIImageView!
    @IBOutlet weak var img_IconCalendar: UIImageView!
    @IBOutlet private weak var lbl_Source: UILabel!
    
    @IBOutlet weak var web_WebView: WKWebView!
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    @IBOutlet weak var constraint_height_Webview: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_CollectionView: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_TableView: NSLayoutConstraint!
    
    @IBOutlet weak var view_Scroll: UIScrollView!
    @IBOutlet weak var view_nav_Blur: UIView!
    @IBOutlet weak var view_nav_Fill: UIView!
    @IBOutlet weak var lbl_nav_Title: UILabel!
    
    @IBOutlet weak var btn_Share: UIButton!
    
    
    private var isCheckNavBar = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitNavigationBar()
        self.setInitScrollView()
        self.setInitCollectionView()
        self.setInitTableView()
        self.setInitWKWebView()
        
        self.view_Scroll.alpha = 0
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.presenter.onBackAction()
    }
    
    @IBAction func onShareAction(_ sender: Any) {
        self.presenter.onShareAction(self.btn_Share)
    }
}

// MARK: - BlogDetailViewProtocol
extension BlogDetailViewController: BlogDetailViewProtocol {
    func setDataInit(with blog: BlogModel?) {
        self.img_Image.setImageWith(imageUrl: blog?.thumbImage ?? "", placeHolder: R.image.img_default())
        self.lbl_nav_Title.text = blog?.title
        self.lbl_Title.text = blog?.title
        self.lbl_AuthorName.text = blog?.authors //blog?.createdBy?.fullName ?? ""
        
        let sourceAtributedString = NSMutableAttributedString(string: "Nguồn: ", attributes: [.foregroundColor: R.color.darkText()!])
        sourceAtributedString.append(NSAttributedString(string: blog?.source ?? "www.1sk.vn"))
        self.lbl_Source.attributedText = sourceAtributedString
        
        if let createdAt = blog?.createdAt {
            self.lbl_Time.text = Date.iso8601Formatter.date(from: createdAt)?.toString(.dmySlash)
        }
        
        self.view_Scroll.setContentOffset(.zero, animated: true)
        
        self.web_WebView.loadHTMLString(blog?.content?.fullHTMLString ?? "", baseURL: nil)
        
    }
    
    func onRealoadDataColl() {
        self.coll_CollectionView.reloadData()
    }
    
    func onRealoadDataTable() {
        self.tbv_TableView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            UIView.animate(withDuration: 0.5) {
                self.view_Scroll.alpha = 1
                self.view_nav_Fill.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - WKNavigationDelegate
extension BlogDetailViewController: WKNavigationDelegate {
    func setInitWKWebView() {
        self.web_WebView.navigationDelegate = self
        self.web_WebView.scrollView.isScrollEnabled = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.web_WebView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_WebView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if let `height` = height as? CGFloat {
                        self.constraint_height_Webview.constant = CGFloat(height + 20)
                    }
                })
            }
        })
    }
}

// MARK: - UICollectionViewDataSource
extension BlogDetailViewController: UICollectionViewDataSource {
    private func setInitCollectionView() {
        self.coll_CollectionView.registerNib(ofType: TagCollectionViewCell.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
        
        self.constraint_height_CollectionView.constant = 0
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 10
        layout.headerReferenceSize = CGSize(width: 16, height: 0)
        layout.footerReferenceSize = CGSize(width: 16, height: 0)
        layout.estimatedItemSize = CGSize(width: 50, height: 54)
        self.coll_CollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let count = self.presenter.numberOfItemsInSectionColl()
        
        if count > 0 {
            self.constraint_height_CollectionView.constant = 46
        } else {
            self.constraint_height_CollectionView.constant = 0
        }
        
        return count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: TagCollectionViewCell.self, for: indexPath)
        let item = presenter.cellForItemColl(at: indexPath)
        cell.config(with: item)
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension BlogDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.didSelectItemColl(at: indexPath)
    }
}

// MARK: - UITableView DataSource
extension BlogDetailViewController: UITableViewDataSource {
    private func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: BlogTableViewCell.self)
        self.tbv_TableView.delegate = self
        self.tbv_TableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.presenter.numberOfRowInSectionTable()
        self.constraint_height_TableView.constant = CGFloat(115 * count)
        return count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: BlogTableViewCell.self, for: indexPath)
        let item = self.presenter.cellForRowTable(at: indexPath)
        cell.config(with: item)
        return cell
    }
}

// MARK: - UITableView Delegate
extension BlogDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: 0.3) {
            self.view_Scroll.alpha = 0
            self.view_nav_Fill.alpha = 0
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.presenter.didSelectedItemTable(at: indexPath)
        }
    }
}

// MARK: - UIScrollViewDelegate
extension BlogDetailViewController: UIScrollViewDelegate {
    func setInitScrollView() {
        self.view_Scroll.delegate = self
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        self.setStateNavigationBar(contentOffset: contentOffset)
    }
}

// MARK: - Navigation Bar
extension BlogDetailViewController {
    func setInitNavigationBar() {
        self.view_nav_Fill.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.view_nav_Fill.isHidden = true
    }
    
    func setStateNavigationBar(contentOffset: CGFloat) {
        if self.isCheckNavBar {
            if contentOffset <= 0.0 {
                self.isCheckNavBar = false
                if self.view_nav_Fill.isHidden == false {
                    UIView.animate(withDuration: 0.5) {
                        self.view_nav_Fill.alpha = 0
                        self.view.layoutIfNeeded()
                        
                    } completion: { _ in
                        self.view_nav_Fill.isHidden = true
                        self.isCheckNavBar = true
                    }
                } else {
                    self.isCheckNavBar = true
                }
            }
            
            if contentOffset > 44.0 {
                self.isCheckNavBar = false
                if self.view_nav_Fill.isHidden == true {
                    
                    self.view_nav_Fill.isHidden = false
                    self.view_nav_Fill.alpha = 0
                    
                    UIView.animate(withDuration: 0.5) {
                        self.view_nav_Fill.alpha = 1
                        self.view.layoutIfNeeded()
                        
                    } completion: { _ in
                        self.isCheckNavBar = true
                    }
                } else {
                    self.isCheckNavBar = true
                }
            }
        }
    }
}

// MARK: - BlogListByTagPresenter Delegate
extension BlogDetailViewController: BlogListByTagPresenterDelegate {
    func didSelectedBlog(_ blog: BlogModel) {
        self.navigationController?.popViewController(animated: true)
        UIView.animate(withDuration: 0.3) {
            self.view_Scroll.alpha = 0
            self.view_nav_Fill.alpha = 0
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.presenter.onBlogListByTagViewController(blog)
        }
    }
}
