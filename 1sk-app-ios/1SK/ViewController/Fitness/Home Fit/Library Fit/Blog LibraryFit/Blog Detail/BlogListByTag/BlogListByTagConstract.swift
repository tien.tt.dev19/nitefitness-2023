//
//  
//  BlogListByTagConstract.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//
//

import UIKit

// MARK: View -
protocol BlogListByTagViewProtocol: AnyObject {
    func reloadData()
    func getVisibleIndexPath() -> [IndexPath]
    func updateNavigationWith(title: String)
}

// MARK: Interactor -
protocol BlogListByTagInteractorInputProtocol {
    func getListBlogByTag(with slug: String, portal: String?, page: Int, limit: Int)
}

protocol BlogListByTagInteractorOutputProtocol: AnyObject {
    func onGetListBlogByTagFinish(with result: Result<[BlogModel], APIError>, page: Int, total: Int)
}
// MARK: Presenter -
protocol BlogListByTagPresenterProtocol {
    func onViewDidLoad()
    func numberOfRowInSection() -> Int
    func itemForRow(at index: IndexPath) -> BlogModel?
    func onPrefetchItem(at indexs: [IndexPath])
    func onTableViewDidSelectedRow(at index: IndexPath)
}

// MARK: Router -
protocol BlogListByTagRouterProtocol: BaseRouterProtocol {

}
