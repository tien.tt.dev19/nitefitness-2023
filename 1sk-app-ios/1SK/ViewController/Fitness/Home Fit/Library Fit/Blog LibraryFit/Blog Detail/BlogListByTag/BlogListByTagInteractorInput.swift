//
//  
//  BlogListByTagInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//
//

import UIKit

class BlogListByTagInteractorInput {
    weak var output: BlogListByTagInteractorOutputProtocol?

    var blogService: BlogServiceProtocol?
}

// MARK: - BlogListByTagInteractorInput - BlogListByTagInteractorInputProtocol -
extension BlogListByTagInteractorInput: BlogListByTagInteractorInputProtocol {
    func getListBlogByTag(with slug: String, portal: String?, page: Int, limit: Int) {
        self.blogService?.getListBlogByTag(slug: slug, portal: portal, page: page, limit: limit, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListBlogByTagFinish(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
