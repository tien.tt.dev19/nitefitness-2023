//
//  
//  BlogListByTagPresenter.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//
//

import UIKit

protocol BlogListByTagPresenterDelegate: AnyObject {
    func didSelectedBlog(_ blog: BlogModel)
}

class BlogListByTagPresenter {

    weak var view: BlogListByTagViewProtocol?
    private var interactor: BlogListByTagInteractorInputProtocol
    private var router: BlogListByTagRouterProtocol

    var tag: TagModel?
    private var blogs: [BlogModel] = []
    
    // Loadmore
    private var totalItem = 0
    private var currentPage = 1
    private var isFetchingBlog = false
    private let limit = 10

    weak var delegate: BlogListByTagPresenterDelegate?

    init(interactor: BlogListByTagInteractorInputProtocol,
         router: BlogListByTagRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var categoryType: CategoryType?

    // MARK: - API
    private func loadItem(in page: Int) {
        if page == 1 {
            self.router.showHud()
        }
        self.isFetchingBlog = true
        
        switch self.categoryType {
        case .blogFit:
            self.interactor.getListBlogByTag(with: tag?.name ?? "", portal: "fit", page: page, limit: self.limit)
            
        case .blogCare:
            self.interactor.getListBlogByTag(with: tag?.name ?? "", portal: "care", page: page, limit: self.limit)
            
        default:
            self.interactor.getListBlogByTag(with: tag?.name ?? "", portal: nil, page: page, limit: self.limit)
        }
    }
}

// MARK: - extending BlogListByTagPresenter: BlogListByTagPresenterProtocol -
extension BlogListByTagPresenter: BlogListByTagPresenterProtocol {

    func onViewDidLoad() {
        view?.updateNavigationWith(title: tag?.name ?? "") 
        loadItem(in: 1)
    }

    func numberOfRowInSection() -> Int {
        return totalItem
    }

    func itemForRow(at index: IndexPath) -> BlogModel? {
        return  index.row < blogs.count ? blogs[index.row] : nil
    }

    func onPrefetchItem(at indexs: [IndexPath]) {
        indexs.forEach { indexPath in
            if needFetchData(indexPath: indexPath) && !isFetchingBlog {
                // Fetch more data
                loadItem(in: currentPage + 1)
            }
        }
    }

    func onTableViewDidSelectedRow(at index: IndexPath) {
        guard index.row < blogs.count else {
            return
        }
        delegate?.didSelectedBlog(blogs[index.row])
    }
}

// MARK: - BlogListByTagPresenter: BlogListByTagInteractorOutput -
extension BlogListByTagPresenter: BlogListByTagInteractorOutputProtocol {
    func onGetListBlogByTagFinish(with result: Result<[BlogModel], APIError>, page: Int, total: Int) {
        let isFirstLoad = page == 1
        switch result {
        case .success(let blogByTagModel):
            let blogs = blogByTagModel
            if !isFirstLoad {
                handleLoadmoreBlogs(blogs)
                
            } else {
                self.blogs = blogs
                totalItem = blogs.count
                view?.reloadData()
                router.hideErrorView()
            }
            currentPage = page
            
        case .failure:
            if isFirstLoad {
                router.showErrorView(with: nil, delegate: self)
            }
        }
        isFetchingBlog = false
        router.hideHud()
    }
}

// MARK: - Load more
extension BlogListByTagPresenter {
    private func handleLoadmoreBlogs(_ blogs: [BlogModel]) {
        self.blogs.append(contentsOf: blogs)
        let visibleIndexPaths = view?.getVisibleIndexPath()
        let startIndex = (currentPage - 1) * limit
        let endIndex = min(startIndex + limit, totalItem)
        view?.reloadData()
        
        // Continue fetch if needed
        if visibleIndexPaths?.contains(where: { $0.row >= endIndex }) ?? false && !isFetchingBlog {
            loadItem(in: currentPage + 1)
        }
    }
    private func needFetchData(indexPath: IndexPath) -> Bool {
        let numberOfItem = blogs.count
        return indexPath.row >= numberOfItem && indexPath.row < totalItem
    }
}

// MARK: - ErrorView Delegate
extension BlogListByTagPresenter: ErrorViewDelegate {
    func onRetryButtonDidTapped(_ errorView: UIView) {
        loadItem(in: 1)
    }
}
