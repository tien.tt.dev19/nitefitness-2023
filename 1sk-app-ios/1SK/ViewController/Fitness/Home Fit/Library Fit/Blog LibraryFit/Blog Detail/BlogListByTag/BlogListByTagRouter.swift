//
//  
//  BlogListByTagRouter.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//
//

import UIKit

class BlogListByTagRouter: BaseRouter {
    static func setupModule(tag: TagModel, categoryType: CategoryType?, delegate: BlogListByTagPresenterDelegate?) -> BlogListByTagViewController {
        let viewController = BlogListByTagViewController()
        let router = BlogListByTagRouter()
        let interactorInput = BlogListByTagInteractorInput()
        let presenter = BlogListByTagPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.tag = tag
        presenter.delegate = delegate
        presenter.categoryType = categoryType
        interactorInput.output = presenter
        interactorInput.blogService = BlogService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BlogListByTagRouter: BlogListByTagRouterProtocol -
extension BlogListByTagRouter: BlogListByTagRouterProtocol {

}
