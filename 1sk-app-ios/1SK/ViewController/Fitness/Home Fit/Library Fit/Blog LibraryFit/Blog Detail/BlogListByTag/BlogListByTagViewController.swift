//
//  
//  BlogListByTagViewController.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//
//

import UIKit

class BlogListByTagViewController: BaseViewController {
    var presenter: BlogListByTagPresenterProtocol!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setupTableView()
    }

    private func setupTableView() {
        self.tableView.registerNib(ofType: BlogTableViewCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.prefetchDataSource = self
    }
}

// MARK: BlogListByTagViewController - BlogListByTagViewProtocol -
extension BlogListByTagViewController: BlogListByTagViewProtocol {
    func reloadData() {
        self.tableView.reloadData()
    }

    func getVisibleIndexPath() -> [IndexPath] {
        return self.tableView.indexPathsForVisibleRows ?? []
    }

    func updateNavigationWith(title: String) {
        navigationItem.title = title
    }
}

// MARK: - UITableview DataSource
extension BlogListByTagViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRowInSection()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: BlogTableViewCell.self, for: indexPath)
        let item = self.presenter.itemForRow(at: indexPath)
        cell.config(with: item)
        return cell
    }
}

// MARK: - UITableView Delegate
extension BlogListByTagViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.onTableViewDidSelectedRow(at: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
}

// MARK: - UITableView DataSourcePrefetching
extension BlogListByTagViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        self.presenter.onPrefetchItem(at: indexPaths)
    }
}
