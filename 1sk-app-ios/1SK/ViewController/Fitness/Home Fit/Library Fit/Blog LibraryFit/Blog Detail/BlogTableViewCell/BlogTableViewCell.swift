//
//  BlogTableViewCell.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//

import UIKit

class BlogTableViewCell: UITableViewCell {

    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        contentImageView.isSkeletonable = true
        contentImageView.superview?.isSkeletonable = true
        titleLabel.isSkeletonable = true
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        clear()
    }

    func config(with blog: BlogModel?) {
        guard let `blog` = blog else {
            contentImageView.superview?.showSkeleton()
            return
        }
        contentImageView.setImageWith(imageUrl: blog.thumbImage ?? "")
        titleLabel.text = blog.title
        hideSkeleton()
    }

    private func clear() {
        contentImageView.image = nil
        titleLabel.text = ""
    }
}
