//
//  TagCollectionViewCell.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(with tag: TagModel?) {
        titleLabel.text = tag?.name
    }
}
