//
//  
//  BlogLibraryFitConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

// MARK: - View
protocol BlogLibraryFitViewProtocol: AnyObject {
    func onViewDidLoad()
    
    // UITableView
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
}

// MARK: - Presenter
protocol BlogLibraryFitPresenterProtocol {
    func onViewDidLoad()
    func onDidSelectCategory(category: CategoryChildrenModel?)
    
    // UITableView
    func numberOfRow(in section: Int) -> Int
    func itemForRow(at indexPath: IndexPath) -> BlogModel?
    func onLoadMoreAction()
    func onDidSelectRowAt(indexPath: IndexPath)
}

// MARK: - Interactor Input
protocol BlogLibraryFitInteractorInputProtocol {
    func getListBlog(categoryId: Int, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol BlogLibraryFitInteractorOutputProtocol: AnyObject {
    func onGetListBlogFinish(with result: Result<[BlogModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol BlogLibraryFitRouterProtocol: BaseRouterProtocol {
    func gotoBlogDetailsViewController(blogModel: BlogModel)
}
