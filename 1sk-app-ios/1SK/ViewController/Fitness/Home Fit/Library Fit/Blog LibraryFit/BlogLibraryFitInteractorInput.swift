//
//  
//  BlogLibraryFitInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class BlogLibraryFitInteractorInput {
    weak var output: BlogLibraryFitInteractorOutputProtocol?
    var blogService: BlogServiceProtocol?
}

// MARK: - BlogLibraryFitInteractorInputProtocol
extension BlogLibraryFitInteractorInput: BlogLibraryFitInteractorInputProtocol {
    func getListBlog(categoryId: Int, page: Int, perPage: Int) {
        self.blogService?.getListBlog(categoryId: categoryId, tagsId: nil, keywords: nil, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListBlogFinish(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
