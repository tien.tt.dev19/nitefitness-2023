//
//  
//  BlogLibraryFitPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class BlogLibraryFitPresenter {

    weak var view: BlogLibraryFitViewProtocol?
    private var interactor: BlogLibraryFitInteractorInputProtocol
    private var router: BlogLibraryFitRouterProtocol

    init(interactor: BlogLibraryFitInteractorInputProtocol,
         router: BlogLibraryFitRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var listBlogs: [BlogModel] = []
    private var categoryId = CategoryType.blogFit.rawValue
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        self.router.showHud()
        self.interactor.getListBlog(categoryId: self.categoryId, page: page, perPage: self.limit)
    }
    
}

// MARK: - BlogLibraryFitPresenterProtocol
extension BlogLibraryFitPresenter: BlogLibraryFitPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    func onDidSelectCategory(category: CategoryChildrenModel?) {
        self.categoryId = category?.id ?? CategoryType.blogFit.rawValue
        self.getData(in: 1)
    }
    
    func numberOfRow(in section: Int) -> Int {
        return self.listBlogs.count
    }
    
    func itemForRow(at indexPath: IndexPath) -> BlogModel? {
        return self.listBlogs[indexPath.row]
    }
    
    func onLoadMoreAction() {
        if self.listBlogs.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func onDidSelectRowAt(indexPath: IndexPath) {
        let blog = self.listBlogs[indexPath.row]
        self.router.gotoBlogDetailsViewController(blogModel: blog)
    }
}

// MARK: - BlogLibraryFitInteractorOutput 
extension BlogLibraryFitPresenter: BlogLibraryFitInteractorOutputProtocol {
    func onGetListBlogFinish(with result: Result<[BlogModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            for item in listModel {
                item.categoryType = .blogFit
            }
            
            if page <= 1 {
                self.listBlogs = listModel
                self.view?.onReloadData()
                
            } else {
                for object in listModel {
                    self.listBlogs.append(object)
                    self.view?.onInsertRow(at: self.listBlogs.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listBlogs.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            break
        }
        
        self.router.hideHud()
    }
}
