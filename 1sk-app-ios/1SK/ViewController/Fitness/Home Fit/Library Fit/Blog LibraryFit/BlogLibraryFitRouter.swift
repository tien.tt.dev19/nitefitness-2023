//
//  
//  BlogLibraryFitRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class BlogLibraryFitRouter: BaseRouter {
    static func setupModule() -> BlogLibraryFitViewController {
        let viewController = BlogLibraryFitViewController()
        let router = BlogLibraryFitRouter()
        let interactorInput = BlogLibraryFitInteractorInput()
        let presenter = BlogLibraryFitPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.blogService = BlogService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - BlogLibraryFitRouterProtocol
extension BlogLibraryFitRouter: BlogLibraryFitRouterProtocol {
    func gotoBlogDetailsViewController(blogModel: BlogModel) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: .blogFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
