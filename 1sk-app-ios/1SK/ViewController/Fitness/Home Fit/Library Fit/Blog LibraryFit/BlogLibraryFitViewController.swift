//
//  
//  BlogLibraryFitViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class BlogLibraryFitViewController: BaseViewController {

    var presenter: BlogLibraryFitPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitUiTableView()
    }
    
    // MARK: - Action
    
}

// MARK: - BlogLibraryFitViewProtocol
extension BlogLibraryFitViewController: BlogLibraryFitViewProtocol {
    func onViewDidLoad() {
        
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
}

// MARK: - UITableViewDataSource
extension BlogLibraryFitViewController: UITableViewDataSource {
    func setInitUiTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewBlog.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRow(in: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewBlog.self, for: indexPath)
        cell.config(with: self.presenter.itemForRow(at: indexPath))
        return cell
    }
}

// MARK: - UITableViewDelegate
extension BlogLibraryFitViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRowAt(indexPath: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}
