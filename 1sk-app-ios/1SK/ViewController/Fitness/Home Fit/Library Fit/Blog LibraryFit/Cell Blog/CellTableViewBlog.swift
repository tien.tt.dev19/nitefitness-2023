//
//  CellTableViewBlog.swift
//  1SK
//
//  Created by vuongbachthu on 7/29/21.
//

import UIKit

class CellTableViewBlog: UITableViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var view_LineBottom: UIView!
    
    var isHiddenLine = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.img_Image.isSkeletonable = true
        self.img_Image.superview?.isSkeletonable = true
        self.lbl_Title.isSkeletonable = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.isSkeletonable = true
        self.img_Image.image = nil
        self.lbl_Title.text = ""
    }

    func config(with blog: BlogModel?) {
        guard let `blog` = blog else {
            self.showSkeleton()
            return
        }
        self.view_LineBottom.isHidden = self.isHiddenLine
        self.img_Image.setImageWith(imageUrl: blog.thumbImage ?? "")
        self.lbl_Title.text = blog.title
        self.hideSkeleton()
    }
}
