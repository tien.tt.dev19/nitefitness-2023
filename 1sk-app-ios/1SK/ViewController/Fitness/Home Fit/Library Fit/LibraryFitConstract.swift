//
//  
//  LibraryFitConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

// MARK: - View
protocol LibraryFitViewProtocol: AnyObject {
    func onViewDidLoad()
}

// MARK: - Presenter
protocol LibraryFitPresenterProtocol {
    func onViewDidLoad()
    
    func getListCategory(_ type: CategoryType) -> [ItemMenu]
    func onDidSelectItem(item: ItemMenu)
    func getCategorySelected(item: ItemMenu) -> CategoryChildrenModel?
}

// MARK: - Interactor Input
protocol LibraryFitInteractorInputProtocol {
    func getListCategoryVideoFit(categoryId: Int)
    func getListCategoryBlogFit(categoryId: Int)
}

// MARK: - Interactor Output
protocol LibraryFitInteractorOutputProtocol: AnyObject {
    func onGetListCategoryVideoFitFinish(with result: Result<[CategoryParentModel], APIError>)
    func onGetListCategoryBlogFitFinish(with result: Result<[CategoryParentModel], APIError>)
}

// MARK: - Router
protocol LibraryFitRouterProtocol: BaseRouterProtocol {

}
