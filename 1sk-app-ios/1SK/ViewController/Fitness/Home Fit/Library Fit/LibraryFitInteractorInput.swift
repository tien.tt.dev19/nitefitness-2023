//
//  
//  LibraryFitInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class LibraryFitInteractorInput {
    weak var output: LibraryFitInteractorOutputProtocol?
    var configService: ConfigServiceProtocol?
}

// MARK: - LibraryFitInteractorInputProtocol
extension LibraryFitInteractorInput: LibraryFitInteractorInputProtocol {
    func getListCategoryVideoFit(categoryId: Int) {
        self.configService?.getListCategoriesChildren(categoryId: categoryId, completion: { [weak self] result in
            self?.output?.onGetListCategoryVideoFitFinish(with: result.unwrapSuccessModel())
        })
    }
    
    func getListCategoryBlogFit(categoryId: Int) {
        self.configService?.getListCategoriesChildren(categoryId: categoryId, completion: { [weak self] result in
            self?.output?.onGetListCategoryBlogFitFinish(with: result.unwrapSuccessModel())
        })
    }
}
