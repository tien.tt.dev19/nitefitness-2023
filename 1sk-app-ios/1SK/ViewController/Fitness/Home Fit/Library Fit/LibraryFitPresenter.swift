//
//  
//  LibraryFitPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class LibraryFitPresenter {

    weak var view: LibraryFitViewProtocol?
    private var interactor: LibraryFitInteractorInputProtocol
    private var router: LibraryFitRouterProtocol

    init(interactor: LibraryFitInteractorInputProtocol,
         router: LibraryFitRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    private var categoryVideoFit: CategoryParentModel?
    private var categoryBlogFit: CategoryParentModel?
    
    var listItemMenu: [ItemMenu] = []
    
    func getInitListCategory() {
        self.interactor.getListCategoryVideoFit(categoryId: 1)
        self.interactor.getListCategoryBlogFit(categoryId: 2)
    }
}

// MARK: - LibraryFitPresenterProtocol
extension LibraryFitPresenter: LibraryFitPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onViewDidLoad()
        self.getInitListCategory()
    }
    
    func getListCategory(_ type: CategoryType) -> [ItemMenu] {
        self.listItemMenu.removeAll()
        
        switch type {
        case .videoFit:
            guard let children = self.categoryVideoFit?.children else {
                return []
            }
            for (index, category) in children.enumerated() {
                let item = ItemMenu()
                item.index = index
                item.name = category.name
                item.categoryType = .videoFit
                
                if category.isSelected == true {
                    item.isSelected = true
                }
                self.listItemMenu.append(item)
            }
            
        case .blogFit:
            guard let children = self.categoryBlogFit?.children else {
                return []
            }
            for (index, category) in children.enumerated() {
                let item = ItemMenu()
                item.index = index
                item.name = category.name
                item.categoryType = .blogFit
                
                if category.isSelected == true {
                    item.isSelected = true
                }
                self.listItemMenu.append(item)
            }
            
        default:
            break
        }
        
        return self.listItemMenu
    }
    
    func onDidSelectItem(item: ItemMenu) {
        switch item.categoryType {
        case .videoFit:
            if let object = self.categoryVideoFit?.children?.first(where: { $0.isSelected == true }) {
                object.isSelected = false
            }
            self.categoryVideoFit?.children?[item.index ?? 0].isSelected = true
            
        case .blogFit:
            if let object = self.categoryBlogFit?.children?.first(where: { $0.isSelected == true }) {
                object.isSelected = false
            }
            self.categoryBlogFit?.children?[item.index ?? 0].isSelected = true
            
        default:
            break
        }
    }
    
    func getCategorySelected(item: ItemMenu) -> CategoryChildrenModel? {
        switch item.categoryType {
        case .videoFit:
            return self.categoryVideoFit?.children?[item.index ?? 0]
            
        case .blogFit:
            return self.categoryBlogFit?.children?[item.index ?? 0]
            
        default:
            return nil
        }
    }
}

// MARK: - LibraryFitInteractorOutput 
extension LibraryFitPresenter: LibraryFitInteractorOutputProtocol {
    func onGetListCategoryVideoFitFinish(with result: Result<[CategoryParentModel], APIError>) {
        switch result {
        case .success(let listModel):
            let all = CategoryChildrenModel.init(id: CategoryType.videoFit.rawValue, name: "Tất cả")
            all.isSelected = true
            if let model = listModel.first {
                model.children?.insert(all, at: 0)
                self.categoryVideoFit = model
            }
            
        case .failure:
            SKToast.shared.showToast(content: "Lỗi tải danh mục")
        }
        self.router.hideHud()
    }
    
    func onGetListCategoryBlogFitFinish(with result: Result<[CategoryParentModel], APIError>) {
        switch result {
        case .success(let listModel):
            let all = CategoryChildrenModel.init(id: CategoryType.blogFit.rawValue, name: "Tất cả")
            all.isSelected = true
            if let model = listModel.first {
                model.children?.insert(all, at: 0)
                self.categoryBlogFit = model
            }
            
        case .failure:
            SKToast.shared.showToast(content: "Lỗi tải danh mục")
        }
        self.router.hideHud()
    }
}
