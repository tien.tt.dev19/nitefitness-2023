//
//  
//  LibraryFitRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class LibraryFitRouter: BaseRouter {
    static func setupModule() -> LibraryFitViewController {
        let viewController = LibraryFitViewController()
        let router = LibraryFitRouter()
        let interactorInput = LibraryFitInteractorInput()
        let presenter = LibraryFitPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.configService = ConfigService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - LibraryFitRouterProtocol
extension LibraryFitRouter: LibraryFitRouterProtocol {
    
}
