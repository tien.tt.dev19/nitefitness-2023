//
//  
//  LibraryFitViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class LibraryFitViewController: BaseViewController {

    var presenter: LibraryFitPresenterProtocol!
    
    @IBOutlet weak var view_MenuBar: UIView!
    @IBOutlet weak var view_MenuBarShadow: UIView!
    @IBOutlet weak var btn_Video: UIButton!
    @IBOutlet weak var btn_Blog: UIButton!
    
    @IBOutlet weak var view_Category: UIView!
    @IBOutlet weak var lbl_Category: UILabel!
    
    @IBOutlet weak var view_Container: UIView!
    
    private lazy var pageVideoViewController = VideoLibraryFitRouter.setupModule()
    private lazy var pageBlogViewController = BlogLibraryFitRouter.setupModule()
    
    var categoryType = CategoryType.videoFit
    var titleSelectVideoFit = "Tất cả"
    var titleSelectBlogFit = "Tất cả"
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.view_MenuBarShadow.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }
    
    // MARK: - Action
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
    
    // MARK: - Action
    @IBAction func onVideoAction(_ sender: Any) {
        self.categoryType = .videoFit
        self.addChildView(childViewController: self.pageVideoViewController)
        self.updateButtonSelectedState(self.btn_Video, isSelected: true)
        self.updateButtonSelectedState(self.btn_Blog, isSelected: false)
        
        self.lbl_Category.text = self.titleSelectVideoFit
    }

    @IBAction func onBlogAction(_ sender: Any) {
        self.categoryType = .blogFit
        self.addChildView(childViewController: self.pageBlogViewController)
        self.updateButtonSelectedState(self.btn_Video, isSelected: false)
        self.updateButtonSelectedState(self.btn_Blog, isSelected: true)
        
        self.lbl_Category.text = self.titleSelectBlogFit
    }

    private func updateButtonSelectedState(_ button: UIButton, isSelected: Bool) {
        button.setTitleColor(isSelected ? R.color.mainColor() : R.color.darkText(), for: .normal)
        button.setTitleColor(isSelected ? R.color.mainColor() : R.color.darkText(), for: .selected)
    }
    
    @IBAction func onCategoryAction(_ sender: Any) {
        let listItem = self.presenter.getListCategory(self.categoryType)
        guard listItem.count > 0 else {
            SKToast.shared.showToast(content: "Không có danh mục nào")
            return
        }
        let controller = MenuCategoryViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.listItem = listItem
        self.navigationController?.present(controller, animated: false)
    }
}

// MARK: - LibraryFitViewProtocol
extension LibraryFitViewController: LibraryFitViewProtocol {
    func onViewDidLoad() {
        self.addChildView(childViewController: self.pageVideoViewController)
    }
}

// MARK: - AlertMoreDropdownViewDelegate
extension LibraryFitViewController: MenuCategoryViewDelegate {
    func onDidSelectItem(item: ItemMenu) {
        self.presenter.onDidSelectItem(item: item)
        
        let category = self.presenter.getCategorySelected(item: item)
        
        
        switch item.categoryType {
        case .videoFit:
            self.titleSelectVideoFit = item.name ?? ""
            self.lbl_Category.text = self.titleSelectVideoFit
            self.pageVideoViewController.presenter.onDidSelectCategory(category: category)
            
        case .blogFit:
            self.titleSelectBlogFit = item.name ?? ""
            self.lbl_Category.text = self.titleSelectBlogFit
            self.pageBlogViewController.presenter.onDidSelectCategory(category: category)
            
        default:
            break
        }
    }
}
