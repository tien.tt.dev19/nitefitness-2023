//
//  CellTableViewMenuItem.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//

import UIKit

class CellTableViewMenuItem: UITableViewCell {

    @IBOutlet weak var lbl_ItemName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(item: ItemMenu) {
        self.lbl_ItemName.text = item.name
        if item.isSelected == true {
            self.lbl_ItemName.textColor = R.color.mainColor()
        } else {
            self.lbl_ItemName.textColor = R.color.darkText()
        }
    }
    
}
