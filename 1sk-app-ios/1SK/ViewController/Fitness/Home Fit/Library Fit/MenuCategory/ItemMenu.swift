//
//  ItemMenu.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//

import Foundation

class ItemMenu: NSObject {
    var index: Int?
    var name: String?
    var isHiddenLine: Bool?
    var isSelected: Bool?
    var categoryType: CategoryType?
    
    override init() {
        //
    }
    
    init(index: Int, name: String, isSelected: Bool, isHiddenLine: Bool) {
        self.index = index
        self.name = name
        self.isSelected = isSelected
        self.isHiddenLine = isHiddenLine
    }
}

enum CategoryType: Int {
    case videoFit = 1
    case blogFit = 2
    case videoCare = 3
    case blogCare = 4
    case none = 0
}
