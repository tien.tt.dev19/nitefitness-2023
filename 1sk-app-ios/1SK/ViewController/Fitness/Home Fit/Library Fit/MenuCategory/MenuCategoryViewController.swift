//
//  MenuCategoryViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//

import UIKit

protocol MenuCategoryViewDelegate: AnyObject {
    func onDidSelectItem(item: ItemMenu)
}

class MenuCategoryViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var constraint_height_ContentView: NSLayoutConstraint!
    
    weak var delegate: MenuCategoryViewDelegate?
    
    var listItem: [ItemMenu] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.setInitUITableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }

}

// MARK: - Gesture View
extension MenuCategoryViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension MenuCategoryViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.5
            self.constraint_height_ContentView.constant = CGFloat(self.listItem.count * 40)
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            //
        }
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.3) {
            self.view_Bg.alpha = 0.0
            self.constraint_height_ContentView.constant = 0
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - UITableViewDataSource
extension MenuCategoryViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewMenuItem.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.listItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewMenuItem.self, for: indexPath)
        let item = self.listItem[indexPath.row]
        cell.config(item: item)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MenuCategoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.listItem[indexPath.row]
        self.delegate?.onDidSelectItem(item: item)
        
        self.setAnimationHidden()
    }
}
