//
//  CellTableViewDiscoveryVideo.swift
//  1SK
//
//  Created by vuongbachthu on 7/20/21.
//

import UIKit

class CellTableViewDiscoveryVideo: UITableViewCell {
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var view_LineBottom: UIView!
    
    var isHiddenLine = false
    var isStateCare = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.img_Image.superview?.isSkeletonable = true
        self.img_Image.isSkeletonable = true
        self.lbl_Title.isSkeletonable = true
        self.lbl_Content.isSkeletonable = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(with video: VideoModel?) {
        guard let `video` = video else {
            self.showSkeleton()
            return
        }
        self.view_LineBottom.isHidden = self.isHiddenLine
        self.img_Image.setImageWith(imageUrl: video.image ?? "")
        self.lbl_Title.text = video.name
        
        let minute = video.duration?.toTimeInterval() ?? 0
        self.lbl_Content.text = "\(minute.toFullTimeString())" //"\(item.category?.name ?? "") - \(item.getDetailsContent())"
        if self.isStateCare == true {
            self.lbl_Content.isHidden = true
        }
        self.hideSkeleton()
    }
}
