//
//  VideoTableViewCell.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//

import UIKit

class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        videoImageView.isSkeletonable = true
        titleLabel.isSkeletonable = true
    }

    func config(with item: VideoModel?) {
        guard let `item` = item else {
            showSkeleton()
            return
        }
        videoImageView.setImageWith(imageUrl: item.image ?? "")
        titleLabel.text = item.name
        categoryLabel.text = item.getDetailsContent()
        hideSkeleton()
    }
}
