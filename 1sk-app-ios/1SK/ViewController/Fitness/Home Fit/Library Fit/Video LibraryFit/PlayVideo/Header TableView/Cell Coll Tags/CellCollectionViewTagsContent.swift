//
//  CellCollectionViewTagsContent.swift
//  1SK
//
//  Created by Thaad on 12/01/2022.
//

import UIKit

protocol CellCollectionViewTagsContentDelegate: AnyObject {
    func onDidSelectTag(tags: TagModel?)
}

class CellCollectionViewTagsContent: UICollectionViewCell {

    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    weak var delegate: CellCollectionViewTagsContentDelegate?
    var tags: TagModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitLayoutView()
    }
    
    func setInitLayoutView() {
        self.view_Content.cornerRadius = self.view_Content.frame.height/2
        self.view_Content.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onDidSelectCell)))
    }
    @objc func onDidSelectCell() {
        self.delegate?.onDidSelectTag(tags: self.tags)
    }

    func config(with tags: TagModel?) {
        self.tags = tags
        self.lbl_Name.text = tags?.name
        self.view_Content.cornerRadius = 16
    }
}
