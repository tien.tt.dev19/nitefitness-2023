//
//  VideoDetailsHeader.swift
//  1SK
//
//  Created by tuyenvx on 25/01/2021.
//

import UIKit

protocol VideoDetailsHeaderDelegate: AnyObject {
    func onDidSelectTag(tags: TagModel?)
}

class VideoDetailsHeader: UIView {
    @IBOutlet private weak var lbl_Title: UILabel!
    @IBOutlet private weak var lbl_Time: UILabel!
    @IBOutlet private weak var lbl_Coach: UILabel!
    @IBOutlet private weak var lbl_Details: UILabel!
    @IBOutlet private weak var lbl_Desc: UILabel!
    @IBOutlet private weak var lbl_Source: UILabel!
    
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    weak var delegate: VideoDetailsHeaderDelegate?
    var listTags: [TagModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUICollectionView()
    }

    func config(with video: VideoModel) {
        if video.categoryType == .blogCare {
            self.lbl_Time.isHidden = true
            self.lbl_Details.isHidden = true
            self.lbl_Coach.isHidden = true
        }
        
        self.lbl_Title.text = video.name
        
        if let minute = video.duration?.toTimeInterval() {
            let timeAtributedString = NSMutableAttributedString(string: "Thời gian tập: ", attributes: [.foregroundColor: R.color.subTitle()!])
            timeAtributedString.append(NSAttributedString(string: "\(minute.toFullTimeString())"))
            self.lbl_Time.attributedText = timeAtributedString
            self.lbl_Time.isHidden = false
        } else {
            self.lbl_Time.isHidden = true
        }
        
        if let coachName = video.coach?.name, coachName.count > 1 {
            let coachAtributedString = NSMutableAttributedString(string: "Huấn luyện viên: ", attributes: [.foregroundColor: R.color.subTitle()!])
            coachAtributedString.append(NSAttributedString(string: coachName))
            self.lbl_Coach.attributedText = coachAtributedString
            self.lbl_Coach.isHidden = false
        } else {
            self.lbl_Coach.isHidden = true
        }
        
        self.lbl_Details.attributedText = video.getDetailsAtributedContent()
        self.lbl_Desc.text = video.description
        
        let sourceAtributedString = NSMutableAttributedString(string: "Nguồn: ", attributes: [.foregroundColor: R.color.darkText()!])
        sourceAtributedString.append(NSAttributedString(string: video.source ?? "www.1sk.vn"))
        self.lbl_Source.attributedText = sourceAtributedString
        
        self.listTags = video.tags ?? []
        if self.listTags.count > 0 {
            self.coll_CollectionView.reloadData()
            self.coll_CollectionView.isHidden = false
        }
    }

    func getHeight() -> CGFloat {
        let caculateSize = systemLayoutSizeFitting(CGSize(width: Constant.Screen.width, height: 0))
        return caculateSize.height
    }
}

// MARK: UICollectionViewDataSource
extension VideoDetailsHeader: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewTagsContent.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.isHidden = true
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listTags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewTagsContent.self, for: indexPath)
        let tag = self.listTags[indexPath.row]
        cell.config(with: tag)
        cell.delegate = self
        return cell
    }
}

// CellCollectionViewTagsContentDelegate
extension VideoDetailsHeader: CellCollectionViewTagsContentDelegate {
    func onDidSelectTag(tags: TagModel?) {
        self.delegate?.onDidSelectTag(tags: tags)
    }
}
