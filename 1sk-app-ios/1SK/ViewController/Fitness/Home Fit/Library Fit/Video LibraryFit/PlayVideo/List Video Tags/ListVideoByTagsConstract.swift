//
//  
//  ListVideoByTagsConstract.swift
//  1SK
//
//  Created by Thaad on 12/01/2022.
//
//

import UIKit

// MARK: - View
protocol ListVideoByTagsViewProtocol: AnyObject {
    func onUpdateTitleNav(title: String?)
    func onReloadData()
    func onSetNotFoundView(isHidden: Bool)
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol ListVideoByTagsPresenterProtocol {
    func onViewDidLoad()
    
    // UITableView
    func numberOfRows() -> Int
    func cellForRow(at index: IndexPath) -> VideoModel?
    func didSelectRow(at index: IndexPath)
    func onLoadMoreAction()
}

// MARK: - Interactor Input
protocol ListVideoByTagsInteractorInputProtocol {
    func getListVideoTags(categoryId: Int, tag: String, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol ListVideoByTagsInteractorOutputProtocol: AnyObject {
    func onGetListVideoTagsFinish(with result: Result<[VideoModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol ListVideoByTagsRouterProtocol: BaseRouterProtocol {
    func onPopViewController()
}
