//
//  
//  ListVideoByTagsInteractorInput.swift
//  1SK
//
//  Created by Thaad on 12/01/2022.
//
//

import UIKit

class ListVideoByTagsInteractorInput {
    weak var output: ListVideoByTagsInteractorOutputProtocol?
    var fitnessService: FitnessServiceProtocol?
}

// MARK: - ListVideoByTagsInteractorInputProtocol
extension ListVideoByTagsInteractorInput: ListVideoByTagsInteractorInputProtocol {
    func getListVideoTags(categoryId: Int, tag: String, page: Int, perPage: Int) {
        self.fitnessService?.getListVideosTags(categoryId: categoryId, tag: tag, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListVideoTagsFinish(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
