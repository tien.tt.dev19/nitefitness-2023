//
//  
//  ListVideoByTagsPresenter.swift
//  1SK
//
//  Created by Thaad on 12/01/2022.
//
//

import UIKit

protocol ListVideoByTagsPresenterDelegate: AnyObject {
    func onSelectVideoByTag(video: VideoModel?)
}

class ListVideoByTagsPresenter {

    weak var view: ListVideoByTagsViewProtocol?
    private var interactor: ListVideoByTagsInteractorInputProtocol
    private var router: ListVideoByTagsRouterProtocol
    weak var delegate: ListVideoByTagsPresenterDelegate?
    
    init(interactor: ListVideoByTagsInteractorInputProtocol,
         router: ListVideoByTagsRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var categoryType: CategoryType?
    var tags: TagModel?
    var listVideos: [VideoModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        
        guard let tags = self.tags?.name, let categoryId = self.categoryType?.rawValue else {
            return
        }
        
        self.router.showHud()
        self.interactor.getListVideoTags(categoryId: categoryId, tag: tags, page: page, perPage: self.limit)
    }
    
    func checkDataNotFound() {
        // Set Status View Data Not Found
        if self.listVideos.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - ListVideoByTagsPresenterProtocol
extension ListVideoByTagsPresenter: ListVideoByTagsPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onUpdateTitleNav(title: self.tags?.name)
        self.getData(in: 1)
    }
    
    // MARK: - UITableView
    func numberOfRows() -> Int {
        return self.listVideos.count
    }

    func cellForRow(at index: IndexPath) -> VideoModel? {
        return self.listVideos[index.row]
    }

    func didSelectRow(at index: IndexPath) {
        let video = self.listVideos[index.row]
        self.delegate?.onSelectVideoByTag(video: video)
        self.router.onPopViewController()
    }
    
    func onLoadMoreAction() {
        if self.listVideos.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            if self.listVideos.count > 10 {
                SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            }
            self.view?.onLoadingSuccess()
        }
    }
}

// MARK: - ListVideoByTagsInteractorOutput 
extension ListVideoByTagsPresenter: ListVideoByTagsInteractorOutputProtocol {
    func onGetListVideoTagsFinish(with result: Result<[VideoModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let videosModel):
            if page <= 1 {
                self.listVideos = videosModel
                self.view?.onReloadData()
                
            } else {
                for object in videosModel {
                    self.listVideos.append(object)
                    self.view?.onInsertRow(at: self.listVideos.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listVideos.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            break
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
}
