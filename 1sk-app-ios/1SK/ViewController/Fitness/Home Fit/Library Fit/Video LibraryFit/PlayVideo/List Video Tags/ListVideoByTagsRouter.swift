//
//  
//  ListVideoByTagsRouter.swift
//  1SK
//
//  Created by Thaad on 12/01/2022.
//
//

import UIKit

class ListVideoByTagsRouter: BaseRouter {
    static func setupModule(tags: TagModel?, categoryType: CategoryType?, delegate: ListVideoByTagsPresenterDelegate?) -> ListVideoByTagsViewController {
        let viewController = ListVideoByTagsViewController()
        let router = ListVideoByTagsRouter()
        let interactorInput = ListVideoByTagsInteractorInput()
        let presenter = ListVideoByTagsPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.tags = tags
        presenter.categoryType = categoryType
        interactorInput.output = presenter
        interactorInput.fitnessService = FitnessService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - ListVideoByTagsRouterProtocol
extension ListVideoByTagsRouter: ListVideoByTagsRouterProtocol {
    func onPopViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
