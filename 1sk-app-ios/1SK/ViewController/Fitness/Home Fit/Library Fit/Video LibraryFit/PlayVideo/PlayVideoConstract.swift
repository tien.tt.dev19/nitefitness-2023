//
//  
//  PlayVideoConstract.swift
//  1SK
//
//  Created by vuongbachthu on 9/3/21.
//
//

import UIKit

// MARK: - View
protocol PlayVideoViewProtocol: AnyObject {
    func onReloadDataVideo(with video: VideoModel?, categoryType: CategoryType?)
    func onReloadDataRelated()
    func getShareView() -> UIView
    func setVideoResolution(_ resolution: VideoResolution)
}

// MARK: - Presenter
protocol PlayVideoPresenterProtocol {
    func onViewDidLoad()
    
    func onBackAction()
    func onShareAction()
    func onSettingAction()
    
    func setCurrentResolution(_ resolution: VideoResolution)
    func getCurrentResolution() -> VideoResolution
    
    func onDidSelectTag(tags: TagModel?)
    
    // UITableView
    func tableViewNumberOfRowInSection() -> Int
    func tableViewItemForRow(at index: IndexPath) -> VideoModel?
    func onTableViewDidSelectedRow(at index: IndexPath)
}

// MARK: - Interactor Input
protocol PlayVideoInteractorInputProtocol {
    func getRelatedVideo(with id: Int, categories: [CategoriesVideo])
    func getVideoDetails(with slug: String)
    func getVideoDetails(with id: Int)
    
    func setSeeVideo(videoId: Int)
}

// MARK: - Interactor Output
protocol PlayVideoInteractorOutputProtocol: AnyObject {
    func onGetRelatedVideoFinish(with result: Result<[VideoModel], APIError>)
    func onGetVideoDetailsFinish(with result: Result<VideoModel, APIError>)
    
    func onSetSeeVideoFinish(with result: Result<EmptyModel, APIError>)
}

// MARK: - Router
protocol PlayVideoRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
    func showVideoResolutionSelectionViewController(currentResolution: VideoResolution, delegate: SelectionViewControllerDelegate?)
    func gotoListVideoByTagsViewController(tags: TagModel?, categoryType: CategoryType?, delegate: ListVideoByTagsPresenterDelegate?)
}
