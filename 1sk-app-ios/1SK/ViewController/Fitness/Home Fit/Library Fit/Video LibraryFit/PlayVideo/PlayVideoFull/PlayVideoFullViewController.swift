//
//  PlayVideoFullViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/4/21.
//

import UIKit
import AVFoundation

protocol PlayVideoFullViewDelegate: AnyObject {
    func setResolutionFullVideo(_ resolution: VideoResolution)
}

class PlayVideoFullViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var view_Player: UIView!
    @IBOutlet weak var view_Control: UIView!
    @IBOutlet weak var view_AVPlayerControl: UIView!
    
    @IBOutlet weak var stack_Setting: UIStackView!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var btn_Setting: UIButton!
    @IBOutlet weak var btn_Minisize: UIButton!
    @IBOutlet weak var lbl_Title: UILabel!
    
    weak var delegate: PlayVideoFullViewDelegate?
    
    var video: VideoModel?
    
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var avControlView: AVPlayerControlViewOld?
    
    var timerControl: Timer?
    var timeControlDuration: TimeInterval = 5
    
    var resolution: VideoResolution?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }

    override func viewWillDisappear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avPlayerLayer?.frame = self.view_Player.bounds
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitAVPlayerControlView()
        self.setInitAVPlayer()
        self.setInitAnimationViewControl()
        self.setInitData()
    }
    
    func setInitAVPlayerControlView() {
        self.avControlView?.frame = CGRect(x: 0, y: 0, width: self.view_AVPlayerControl.width, height: self.view_AVPlayerControl.height)
        self.view_AVPlayerControl.addSubview(self.avControlView!)
    }
    
    func setInitAVPlayer() {
        self.view_Player.layer.addSublayer(self.avPlayerLayer!)
        self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }
    
    func setInitData() {
        self.lbl_Title.text = self.video?.name
    }
    
    func onPlay() {
        self.avPlayer?.play()
        self.avControlView?.btn_Play.setImage(R.image.ic_pause(), for: .normal)
    }

    func onPause() {
        self.avPlayer?.pause()
        self.avControlView?.btn_Play.setImage(R.image.ic_play(), for: .normal)
    }

    // MARK: - Action
    @IBAction func onShareAction(_ sender: Any) {
        guard let slug = self.video?.slug else {
            SKToast.shared.showToast(content: "Không thể chia sẻ video này")
            return
        }
        
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.video?.name
        meta.socialMetaDescText = self.video?.description
        meta.socialMetaImageURL = self.video?.image
        
        let universalLink = "\(SHARE_URL)\(slug)\(ShareType.video.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                
                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }
                
                let controller = UIActivityViewController(activityItems: [urlShare], applicationActivities: nil)
                controller.popoverPresentationController?.sourceView = self.view_Player
                controller.popoverPresentationController?.sourceRect = self.view_Player.bounds
                self.present(controller, animated: true, completion: nil)
                
                print("createDynamicLinksShare The short URL is: \(urlShare)")
            })
            
        } else {
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }

    @IBAction func onSettingAction(_ sender: Any) {
        self.onPause()

        let controller = SelectionViewController.loadFromNib()
        controller.modalPresentationStyle = .overFullScreen
        controller.currentVideoResolution = self.resolution ?? .auto
        controller.type = .videoResolution
        controller.delegate = self
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func onMinisizeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: SelectionViewControllerDelegate
extension PlayVideoFullViewController: SelectionViewControllerDelegate {
    func didSelectItem(of type: SelectionType, at index: Int) {
        if type == .videoResolution {
            let selectedResolution = VideoResolution.allCases[index]
            if selectedResolution == self.resolution {
                self.onPlay()
            } else {
                self.resolution = selectedResolution
                self.delegate?.setResolutionFullVideo(selectedResolution)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.onPlay()
                }
            }
        }
    }
    
    func didDismissView() {
        self.onPlay()
    }
}

// MARK: Animation View Control
extension PlayVideoFullViewController {
    func setInitAnimationViewControl() {
        self.view_Player.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapViewPlayerAction)))
        self.view_Control.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapViewControlAction)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onActionPlayControl), name: .onActionPlayControl, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onActionLoading), name: .onActionLoading, object: nil)
        
        self.setViewControlShow()
    }
    
    @objc func onActionPlayControl() {
        self.setTimerViewControlHidden()
    }
    
    @objc func onActionLoading() {
        self.self.setViewControlShow()
    }
    
    @objc func onTapViewPlayerAction() {
        self.setViewControlShow()
    }
    
    @objc func onTapViewControlAction() {
        self.timeControlDuration = 3
    }
    
    func setViewControlShow() {
        if self.view_Control.isHidden == true {
            self.view_Control.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_Control.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
        
        self.setTimerViewControlHidden()
    }
    
    func setViewControlHidden() {
        if self.avControlView?.avState == .readyToPlay && self.avControlView?.avRate == .isPlay {
            UIView.animate(withDuration: 1.0) {
                self.view_Control.alpha = 0
                self.view.layoutIfNeeded()

            } completion: { _ in
                self.view_Control.isHidden = true
            }
        }
    }
}

// MARK: Timer Control
extension PlayVideoFullViewController {
    func setTimerViewControlHidden() {
        if self.timerControl == nil {
            self.timeControlDuration = 5
            self.timerControl = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimeControlDidChange), userInfo: nil, repeats: true)
            
        } else {
            self.timeControlDuration = 3
        }
    }
    
    @objc func onTimeControlDidChange() {
        if self.timeControlDuration > 1 {
            self.timeControlDuration -= 1
            
        } else {
            self.setInvalidateTimeControl()
            self.setViewControlHidden()
        }
    }
    
    func setInvalidateTimeControl() {
        self.timerControl?.invalidate()
        self.timerControl = nil
    }
}
