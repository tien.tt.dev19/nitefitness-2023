//
//  
//  PlayVideoInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 9/3/21.
//
//

import UIKit

class PlayVideoInteractorInput {
    weak var output: PlayVideoInteractorOutputProtocol?
    var fitnessService: FitnessServiceProtocol?
}

// MARK: - PlayVideoInteractorInputProtocol
extension PlayVideoInteractorInput: PlayVideoInteractorInputProtocol {
    func getRelatedVideo(with id: Int, categories: [CategoriesVideo]) {
        self.fitnessService?.getListRelatedVideo(with: id, categories: categories, completion: { [weak self] result in
            self?.output?.onGetRelatedVideoFinish(with: result.unwrapSuccessModel())
        })
    }

    func getVideoDetails(with slug: String) {
        self.fitnessService?.getVideoDetails(with: slug, completion: { [weak self] result in
            self?.output?.onGetVideoDetailsFinish(with: result.unwrapSuccessModel())
        })
    }
    
    func getVideoDetails(with id: Int) {
        self.fitnessService?.getVideoDetailByID(with: id, completion: { [weak self] result in
            self?.output?.onGetVideoDetailsFinish(with: result.unwrapSuccessModel())
        })
    }
    
    func setSeeVideo(videoId: Int) {
        self.fitnessService?.setSeeVideo(videoId: videoId, completion: { [weak self] result in
            self?.output?.onSetSeeVideoFinish(with: result.unwrapSuccessModel())
        })
    }
}
