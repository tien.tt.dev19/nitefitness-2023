//
//  
//  PlayVideoPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 9/3/21.
//
//

import UIKit

class PlayVideoPresenter {

    weak var view: PlayVideoViewProtocol?
    private var interactor: PlayVideoInteractorInputProtocol
    private var router: PlayVideoRouterProtocol

    init(interactor: PlayVideoInteractorInputProtocol,
         router: PlayVideoRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var categoryType: CategoryType?
    
    var video: VideoModel?
    var relatedVideo: [VideoModel] = []
    var resolution: VideoResolution = .auto
    
    // MARK: - API
    private func getData(of video: VideoModel?) {
        self.router.showHud()
        self.getVideoDetails(of: video)
    }
    
    private func getVideoDetails(of video: VideoModel?) {
        if let id = video?.id {
            self.interactor.getVideoDetails(with: id)
            
            if self.categoryType == .videoFit {
                self.interactor.setSeeVideo(videoId: id)
            }
        }
    }

    private func getRelatedVideo() {
        guard let videoId = self.video?.id else {
            return
        }
        guard let categories = self.video?.categories else {
            return
        }
        self.interactor.getRelatedVideo(with: videoId, categories: categories)
    }
}

// MARK: - PlayVideoPresenterProtocol
extension PlayVideoPresenter: PlayVideoPresenterProtocol {
    func onViewDidLoad() {
        self.getData(of: self.video)
    }
    
    func onBackAction() {
        self.router.popViewController()
    }
    
    func onShareAction() {
        guard let slug = self.video?.slug else {
            SKToast.shared.showToast(content: "Không thể chia sẻ video này")
            return
        }
        
        self.router.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.video?.name
        meta.socialMetaDescText = self.video?.description
        meta.socialMetaImageURL = self.video?.image
        
        let universalLink = "\(SHARE_URL)\(slug)\(ShareType.video.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                self.router.hideHud()
                
                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }
                
                guard let shareView = self.view?.getShareView() else {
                    return
                }
                self.router.showShareViewController(with: [urlShare], sourceView: shareView)
                
                print("createLinkShareFitnessPost The short URL is: \(urlShare)")
            })
            
        } else {
            self.router.hideHud()
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }
    
    func onSettingAction() {
        self.router.showVideoResolutionSelectionViewController(currentResolution: self.resolution, delegate: self)
    }
    
    func setCurrentResolution(_ resolution: VideoResolution) {
        self.resolution = resolution
    }
    
    func getCurrentResolution() -> VideoResolution {
        return self.resolution
    }
    
    func onDidSelectTag(tags: TagModel?) {
        self.router.gotoListVideoByTagsViewController(tags: tags, categoryType: self.categoryType, delegate: self)
    }
    
    // MARK: - UITableView
    func tableViewNumberOfRowInSection() -> Int {
        return self.relatedVideo.count
    }

    func tableViewItemForRow(at index: IndexPath) -> VideoModel? {
        return relatedVideo[index.row]
    }

    func onTableViewDidSelectedRow(at index: IndexPath) {
        let video = self.relatedVideo[index.row]
        self.video = video
        self.getData(of: video)
    }
}

// MARK: - PlayVideoInteractorOutput 
extension PlayVideoPresenter: PlayVideoInteractorOutputProtocol {
    func onGetVideoDetailsFinish(with result: Result<VideoModel, APIError>) {
        switch result {
        case .success(let video):
            self.video = video
            self.view?.onReloadDataVideo(with: self.video, categoryType: self.categoryType)
            self.getRelatedVideo()
            
        case .failure:
            break
        }
        self.router.hideHud()
    }

    func onGetRelatedVideoFinish(with result: Result<[VideoModel], APIError>) {
        switch result {
        case .success(let videos):
            self.relatedVideo = videos
            self.view?.onReloadDataRelated()
            
        case .failure:
            break
        }
        self.router.hideHud()
    }
    
    func onSetSeeVideoFinish(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            break
            
        case .failure:
            break
        }
    }
}

// MARK: - SelectionViewControllerDelegate
extension PlayVideoPresenter: SelectionViewControllerDelegate {
    func didSelectItem(of type: SelectionType, at index: Int) {
        switch type {
        case .videoResolution:
            let selectedResolution = VideoResolution.allCases[index]
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.view?.setVideoResolution(selectedResolution)
            }
            
        default:
            break
        }
    }
    
    func didDismissView() {
        //
    }
}

// MARK: - ListVideoByTagsPresenterDelegate
extension PlayVideoPresenter: ListVideoByTagsPresenterDelegate {
    func onSelectVideoByTag(video: VideoModel?) {
        self.video = video
        self.getData(of: video)
    }
}
