//
//  
//  PlayVideoRouter.swift
//  1SK
//
//  Created by vuongbachthu on 9/3/21.
//
//

import UIKit

class PlayVideoRouter: BaseRouter {
    static func setupModule(with video: VideoModel, categoryType: CategoryType?) -> PlayVideoViewController {
        let viewController = PlayVideoViewController()
        let router = PlayVideoRouter()
        let interactorInput = PlayVideoInteractorInput()
        let presenter = PlayVideoPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.categoryType = categoryType
        presenter.video = video
        interactorInput.output = presenter
        interactorInput.fitnessService = FitnessService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - PlayVideoRouterProtocol
extension PlayVideoRouter: PlayVideoRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        let controller = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        controller.popoverPresentationController?.sourceView = sourceView
        controller.popoverPresentationController?.sourceRect = sourceView.bounds
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func showVideoResolutionSelectionViewController(currentResolution: VideoResolution, delegate: SelectionViewControllerDelegate?) {
        let controller = SelectionViewController.loadFromNib()
        controller.modalPresentationStyle = .overFullScreen
        controller.currentVideoResolution = currentResolution
        controller.type = .videoResolution
        controller.delegate = delegate
        viewController?.present(controller, animated: false, completion: nil)
    }
    
    func gotoListVideoByTagsViewController(tags: TagModel?, categoryType: CategoryType?, delegate: ListVideoByTagsPresenterDelegate?) {
        let controller = ListVideoByTagsRouter.setupModule(tags: tags, categoryType: categoryType, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
}
