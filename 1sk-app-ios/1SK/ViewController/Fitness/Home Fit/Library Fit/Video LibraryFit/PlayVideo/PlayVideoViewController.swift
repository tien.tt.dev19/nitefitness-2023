//
//  
//  PlayVideoViewController.swift
//  1SK
//
//  Created by vuongbachthu on 9/3/21.
//
//

import UIKit
import AVFoundation

class PlayVideoViewController: BaseViewController {

    var presenter: PlayVideoPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    @IBOutlet weak var view_Player: UIView!
    @IBOutlet weak var view_Control: UIView!
    @IBOutlet weak var view_AVPlayerControl: UIView!
    
    @IBOutlet weak var btn_Back: UIButton!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var btn_Setting: UIButton!
    @IBOutlet weak var btn_FullScreen: UIButton!
    @IBOutlet weak var stack_Setting: UIStackView!
    
    var videoDetailsHeader: VideoDetailsHeader!
    
    var video: VideoModel?
    
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var avControlView: AVPlayerControlViewOld?
    
    var timerControl: Timer?
    var timeControlDuration: TimeInterval = 5
    
    var isPlayFullScreen = false
    var valueSliderChange: Float = 0
    
    var progressValue: CGFloat = 0
    
    var needHideLoad = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.isPlayFullScreen {
            self.view_Player.layer.addSublayer(self.avPlayerLayer!)
            self.view_AVPlayerControl.addSubview(self.avControlView!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.isInteractivePopGestureEnable = false
        
        if self.isPlayFullScreen {
            self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: self.view_Player.width, height: self.view_Player.height)
            self.avControlView?.frame = CGRect(x: 0, y: 0, width: self.view_AVPlayerControl.width, height: self.view_AVPlayerControl.height)
            
            self.isPlayFullScreen = false
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.isInteractivePopGestureEnable = true
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avPlayerLayer?.frame = self.view_Player.bounds
    }
    
    deinit {
        gIsShowMarketingPopup = false
    }
    
    // MARK: - Setup
    private func setupInit() {
        gIsShowMarketingPopup = true
        self.setInitAVPlayerControlView()
        self.setInitUITableView()
        self.setInitAnimationViewControl()
    }
    
    func setInitAVPlayerControlView() {
        self.avControlView = AVPlayerControlViewOld.loadFromNib()
        self.avControlView?.delegate = self
        self.avControlView?.frame = CGRect(x: 0, y: 0, width: self.view_AVPlayerControl.width, height: self.view_AVPlayerControl.height)
        self.view_AVPlayerControl.addSubview(self.avControlView!)
        
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.setInvalidateTimeControl()
        self.setRemoveObserver()
        self.presenter.onBackAction()
    }
    
    @IBAction func onShareAction(_ sender: Any) {
        self.presenter.onShareAction()
    }
    
    @IBAction func onSettingAction(_ sender: Any) {
        self.presenter.onSettingAction()
        
    }
    
    @IBAction func onFullScreenAction(_ sender: Any) {
        self.showPlayVideoFullViewController()
    }
}

// MARK: - PlayVideoFullViewDelegate
extension PlayVideoViewController: PlayVideoFullViewDelegate {
    func showPlayVideoFullViewController() {
        self.isPlayFullScreen = true
        
        let controller = PlayVideoFullViewController()
        controller.delegate = self
        controller.video = self.video
        controller.avPlayer = self.avPlayer
        controller.avPlayerLayer = self.avPlayerLayer
        controller.avControlView = self.avControlView
        controller.resolution = self.presenter.getCurrentResolution()
        
        self.navigationController?.show(controller, sender: nil)
        
        self.view_Control.alpha = 0
        self.view_Control.isHidden = true
    }
    
    func setResolutionFullVideo(_ resolution: VideoResolution) {
        self.setVideoResolution(resolution)
    }
}

// MARK: - PlayVideoViewProtocol
extension PlayVideoViewController: PlayVideoViewProtocol {
    func onReloadDataVideo(with video: VideoModel?, categoryType: CategoryType?) {
        guard let `video` = video else {
            return
        }
        video.categoryType = categoryType
        
        self.video = video
        print("videoUrl: name:", video.name ?? "")
        self.setInitAVPlayer(videoUrl: video.resolution?._360p ?? video.link ?? "")
        
        self.videoDetailsHeader.config(with: video)
//        self.tbv_TableView.reloadData()
        self.videoDetailsHeader.frame = CGRect(x: 0, y: 0, width: Constant.Screen.width, height: self.videoDetailsHeader.getHeight())
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            self.tbv_TableView.setContentOffset(.zero, animated: false)
//        }
        
    }
    
    func setVideoResolution(_ resolution: VideoResolution) {
        if resolution != self.presenter.getCurrentResolution() {
            self.presenter.setCurrentResolution(resolution)
            
            switch resolution {
            case .auto:
                self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize.zero
                
            case ._1080:
                self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1920, height: 1080)
                
            case ._720:
                self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1280, height: 720)
                
            case ._480:
                self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 854, height: 480)
                
            case ._360:
                self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 640, height: 360)
            }
        }
    }
    
    func onReloadDataRelated() {
        self.tbv_TableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tbv_TableView.setContentOffset(.zero, animated: false)
        }
    }

    func getShareView() -> UIView {
        return self.btn_Share
    }
}

// MARK: AVPlayer Video
extension PlayVideoViewController {
    func setInitAVPlayer(videoUrl: String) {
        print("videoUrl:", videoUrl)
        
        guard let url = URL(string: videoUrl) else {
            SKToast.shared.showToast(content: "Error video link: \(videoUrl)")
            return
        }
        
        if self.avPlayer == nil {
            self.preparelayVideoWithFisrt(url: url)
            
        } else {
            self.preparePlayVideoNext(url: url)
        }
    }
    
    func preparelayVideoWithFisrt(url: URL) {
        let avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer = AVPlayer(playerItem: avPlayerItem)
        
//        self.avPlayer?.currentItem?.preferredPeakBitRate = 6000000
        
//        self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1024, height: 576)
        
//        self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1920, height: 1080)
//        self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1280, height: 720)
//        self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 854, height: 480)
//        self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 640, height: 360)
        
        self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
        self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: self.view_Player.width, height: self.view_Player.height)
        self.avPlayerLayer?.videoGravity = .resizeAspect
        self.view_Player.layer.addSublayer(self.avPlayerLayer!)
        
        self.onAddObserverCurrentItem()
        self.onAddObserverKVOPlayer()
        
        self.avPlayer?.play()
    }
    
    func preparePlayVideoNext(url: URL) {
        let avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer?.replaceCurrentItem(with: avPlayerItem)
        
        self.onAddObserverCurrentItem()
        self.onResetViewControl()
        self.setViewControlShow()
        
        self.avPlayer?.play()
    }
    
    func onResetViewControl() {
        self.avControlView?.avState = .loading
        self.avControlView?.ind_Loading.startAnimating()
        self.avControlView?.slider_TimeTracking.setValue(0, animated: true)
        self.avControlView?.btn_Play.isHidden = true
    }
    
}

// MARK: - Player Handler Control
extension PlayVideoViewController {
    func onPlay() {
        self.avPlayer?.play()
        self.avControlView?.btn_Play.setImage(R.image.ic_pause(), for: .normal)
        NotificationCenter.default.post(name: .onActionPlayControl, object: nil)
        
        self.setViewControlShow()
    }
    
    func onPause() {
        self.avPlayer?.pause()
        self.avControlView?.btn_Play.setImage(R.image.ic_fit_pause(), for: .normal)
    }
    
    func onReplay() {
        self.avPlayer?.seek(to: CMTime.zero)
        self.onPlay()
    }
}

// MARK: AVPlayerControl
extension PlayVideoViewController: AVPlayerControlViewDelegate {
    func onPlayAction() {
        guard let avPlayer = self.avPlayer else {
            return
        }
        
        switch self.avControlView?.avState {
        case .readyToPlay:
            if avPlayer.isPlaying {
                self.onPause()
                
            } else {
                self.onPlay()
            }
            
        case .playedToTheEnd:
            self.onReplay()
            
        default:
            break
        }
    }
    
    func onSliderEventAction(event: SliderEvent) {
        switch event {
        case .began:
            self.avControlView?.avState = .seeking
        case .ended:
            self.setTimerViewControlHidden()
            
        default:
            break
        }
    }
    
    func onSliderValueChangeAction(value: Float) {
        self.needHideLoad = true
        self.valueSliderChange = value
        
        self.avControlView?.avState = .loading
        self.avControlView?.ind_Loading.startAnimating()
        self.avControlView?.btn_Play.isHidden = true
        
        guard let duration = self.avPlayer?.currentItem?.asset.duration,
              let item = self.avPlayer?.currentItem else { return }
        
        let valueChanged = Double(self.avControlView?.slider_TimeTracking.value ?? 0) * duration.seconds
        let timeIntervalChanged = CMTime(seconds: valueChanged, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        
        item.seek(to: timeIntervalChanged) { _ in
            if self.avPlayer?.isPlaying == true {
                self.avPlayer?.play()
            }
        }
    }
    
    func onScreenSizeAction(isPlayFull: Bool) {
        //
    }
}

// MARK: KVO Observer
extension PlayVideoViewController {
    func onAddObserverKVOPlayer() {
        self.avPlayer?.addObserver(self, forKeyPath: AVConstant.rate, options: [.old, .new], context: nil)
        
        self.onTimeObserver()
    }
    
    func setRemoveObserver() {
        self.avPlayer?.currentItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: nil)
        self.avPlayer?.currentItem?.removeObserver(self, forKeyPath: AVConstant.loadedTimeRangesKey, context: nil)
        self.avPlayer?.removeObserver(self, forKeyPath: AVConstant.rate, context: nil)
    }
    
    func onAddObserverCurrentItem() {
        self.avPlayer?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.initial, .new], context: nil)
        self.avPlayer?.currentItem?.addObserver(self, forKeyPath: AVConstant.loadedTimeRangesKey, options: [.initial, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        switch keyPath {
        case #keyPath(AVPlayerItem.status):
            var status: AVPlayerItem.Status = .unknown
            if let statusNumber = change?[.newKey] as? NSNumber,
                let newStatus = AVPlayerItem.Status(rawValue: statusNumber.intValue) {
                status = newStatus
            }
            
            switch status {
            case .readyToPlay:
                self.avControlView?.btn_Play.setImage(R.image.ic_pause(), for: .normal)
                self.avControlView?.ind_Loading.stopAnimating()
                self.avControlView?.btn_Play.isHidden = false
                
                if self.avControlView?.avState == .seeking || self.avControlView?.avState == .loading {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                        self.avControlView?.avState = .readyToPlay
                        self.valueSliderChange = 0
                        
                        print("observeValue readyToPlay 1.5")
                    }
                } else {
                    self.avControlView?.avState = .readyToPlay
                    print("observeValue readyToPlay")
                }
                
                self.setViewControlShow()
                
            case .failed:
                print("observeValue failed")
                
            case .unknown:
                print("observeValue unknown")
                
            @unknown default:
                fatalError()
            }
            
        case AVConstant.loadedTimeRangesKey:
            self.onLoadedTimeRangedChanged()
            
        case AVConstant.rate:
            if self.avPlayer?.rate == 1 {
                self.avControlView?.avRate = .isPlay
                
            } else {
                self.avControlView?.avRate = .isPause
            }
             
        default:
            break
        }
    }
    
    private func onLoadedTimeRangedChanged() {
        let duration = self.avPlayer?.currentItem?.asset.duration
        let durationSeconds = CMTimeGetSeconds(duration ?? .zero)
        
        let totalBuffer = self.avPlayer?.currentItem?.totalBuffer()
//        let currentBuffer = self.avPlayer?.currentItem?.currentBuffer()
//        let presentationSize = self.avPlayer?.currentItem?.presentationSize
        
        self.progressValue = CGFloat(totalBuffer ?? 0) / CGFloat(durationSeconds)
        self.avControlView?.view_Progress.setProgress(Float(progressValue), animated: true)
        
//        print("<AVPlayer> Loading progressValue:", progressValue)
//        print("<AVPlayer> Loading totalBuffer:", totalBuffer ?? 0)
//        print("<AVPlayer> Loading currentBuffer:", currentBuffer ?? 0)
//        print("<AVPlayer> Loading presentationSize:", presentationSize ?? 0)
//        print("<AVPlayer> Loading ===============================\n")
    }
    
    func onTimeObserver() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC)) //DispatchQueue.main
        
        self.avPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: nil, using: { [weak self] _ in
//        self.avPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: DispatchQueue.main, using: { [weak self] _ in
            guard let currentItem = self?.avPlayer?.currentItem, let durationCMTimeFormat = self?.avPlayer?.currentItem?.asset.duration else {
                return
            }
            
            let duration = CMTimeGetSeconds(durationCMTimeFormat).rounded(toPlaces: 1)
            let currentTime = currentItem.currentTime().seconds.rounded(toPlaces: 1)
            
            if self?.avControlView?.avState != .seeking {
                if self?.valueSliderChange ?? 0 == 0 {
                    let sliderValue = Float(currentTime / Double(duration))
                    self?.avControlView?.slider_TimeTracking.setValue(sliderValue, animated: true)
                    
                    let sliderValuePlay = sliderValue.rounded(toPlaces: 3)
                    let sliderValueLoad = Float(self!.progressValue).rounded(toPlaces: 3)
                    
//                    print("<AVPlayer> addTimeObserver: xxx sliderValuePlay: ", sliderValuePlay)
//                    print("<AVPlayer> addTimeObserver: xxx sliderValueLoad: ", sliderValueLoad)
//                    print("<AVPlayer> addTimeObserver: xxx ======================= \n")
                    
                    if sliderValuePlay == sliderValueLoad && sliderValueLoad > 0.0 {
                        self?.avControlView?.avState = .loading
                        self?.avControlView?.btn_Play.isHidden = true
                        self?.avControlView?.ind_Loading.startAnimating()
                        self?.setViewControlShow()
                        
                        NotificationCenter.default.post(name: .onActionLoading, object: nil)
//                        print("<AVPlayer> addTimeObserver: xxx ==")
                    }
                    
                } else {
                    self?.avControlView?.slider_TimeTracking.setValue(self?.valueSliderChange ?? 0, animated: true)
//                    print("<AVPlayer> addTimeObserver: valueSliderChange: ", self?.valueSliderChange ?? 0)
                    if self?.needHideLoad == true {
                        self?.avControlView?.avState = .readyToPlay
                        self?.avControlView?.ind_Loading.stopAnimating()
                        self?.avControlView?.btn_Play.isHidden = false
                        self?.setViewControlShow()
                        self?.needHideLoad = false
                    }
                    
                }
            }
            
            let currentTimeFormat = AVUtils.formatDurationTime(time: Int(currentTime))
            let durationFormat = AVUtils.formatDurationTime(time: Int(duration))
            
            self?.setTimePlayer(withDuration: durationFormat, currentTime: currentTimeFormat)
            
            // Played To The End
            if currentTimeFormat == durationFormat {
                self?.avControlView?.avState = .playedToTheEnd
                self?.avControlView?.btn_Play.setImage(R.image.ic_replay(), for: .normal)
                self?.avControlView?.ind_Loading.stopAnimating()
                self?.avControlView?.btn_Play.isHidden = false
                self?.setViewControlShow()
                print("<AVPlayer> addTimeObserver: Played To The End")
            }
            
            print("<AVPlayer> addTimeObserver: duration: ", duration)
            print("<AVPlayer> addTimeObserver: currentTime: ", currentTime)
            print("<AVPlayer> addTimeObserver: ========================\n")
        })
    }
    
    private func setTimePlayer(withDuration duration: String, currentTime: String) {
        self.avControlView?.lbl_TimePlayer.text = String(format: "%@/%@", currentTime, duration)
    }
    
    
}


// MARK: Animation View Control
extension PlayVideoViewController {
    func setInitAnimationViewControl() {
        self.view_Player.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapViewPlayerAction)))
        self.view_Control.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapViewControlAction)))
        
        self.setViewControlShow()
    }
    
    @objc func onTapViewPlayerAction() {
        self.setViewControlShow()
    }
    
    @objc func onTapViewControlAction() {
        self.timeControlDuration = 5
    }
    
    func setViewControlShow() {
        if self.view_Control.isHidden == true {
            self.view_Control.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_Control.alpha = 1
                self.view.layoutIfNeeded()
            }
        }
        
        self.setTimerViewControlHidden()
    }
    
    func setViewControlHidden() {
        if self.avControlView?.avState == .readyToPlay && self.avControlView?.avRate == .isPlay {
            UIView.animate(withDuration: 1.0) {
                self.view_Control.alpha = 0
                self.view.layoutIfNeeded()

            } completion: { _ in
                self.view_Control.isHidden = true
            }
        }
    }
}

// MARK: Timer Control
extension PlayVideoViewController {
    func setTimerViewControlHidden() {
        if self.timerControl == nil {
            self.timeControlDuration = 5
            self.timerControl = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimeControlDidChange), userInfo: nil, repeats: true)
            
        } else {
            self.timeControlDuration = 3
        }
    }
    
    @objc func onTimeControlDidChange() {
        if self.timeControlDuration > 1 {
            self.timeControlDuration -= 1
            
        } else {
            self.setInvalidateTimeControl()
            self.setViewControlHidden()
        }
    }
    
    func setInvalidateTimeControl() {
        self.timerControl?.invalidate()
        self.timerControl = nil
    }
}


// MARK: - UITableView DataSource
extension PlayVideoViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: VideoTableViewCell.self)
        self.tbv_TableView.delegate = self
        self.tbv_TableView.dataSource = self
        
        self.videoDetailsHeader = VideoDetailsHeader.loadFromNib()
        self.videoDetailsHeader.delegate = self
        self.tbv_TableView.tableHeaderView = self.videoDetailsHeader
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.tableViewNumberOfRowInSection()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: VideoTableViewCell.self, for: indexPath)
        let item = presenter.tableViewItemForRow(at: indexPath)
        cell.config(with: item)
        return cell
    }
}

// MARK: - UITableView Delegate
extension PlayVideoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onTableViewDidSelectedRow(at: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
}

// MARK: - VideoDetailsHeaderDelegate
extension PlayVideoViewController: VideoDetailsHeaderDelegate {
    func onDidSelectTag(tags: TagModel?) {
        self.onPause()
        self.presenter.onDidSelectTag(tags: tags)
    }
}
