//
//  
//  VideoLibraryFitConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

// MARK: - View
protocol VideoLibraryFitViewProtocol: AnyObject {
    func onScrollToTopTableView()
    func onReloadDataTableView()
    func onSetNotFoundView(isHidden: Bool)
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol VideoLibraryFitPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onDidSelectCategory(category: CategoryChildrenModel?)
    
    // UITableView
    func numberOfRowsTableView() -> Int
    func cellForRowTableView(at indexPath: IndexPath) -> VideoModel
    func didSelectRowTableView(at indexPath: IndexPath)
    func onLoadMoreAction()
}

// MARK: - Interactor Input
protocol VideoLibraryFitInteractorInputProtocol {
    func getListVideo(categoryId: Int, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol VideoLibraryFitInteractorOutputProtocol: AnyObject {
    func onGetListVideoFinish(with result: Result<[VideoModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol VideoLibraryFitRouterProtocol: BaseRouterProtocol {
    func showVideoDetailsViewController(with video: VideoModel)
}
