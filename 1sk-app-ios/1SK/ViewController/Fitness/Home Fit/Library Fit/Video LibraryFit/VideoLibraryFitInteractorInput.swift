//
//  
//  VideoLibraryFitInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class VideoLibraryFitInteractorInput {
    weak var output: VideoLibraryFitInteractorOutputProtocol?
    var fitnessService: FitnessServiceProtocol?
}

// MARK: - LibraryFitVideoInteractorInputProtocol
extension VideoLibraryFitInteractorInput: VideoLibraryFitInteractorInputProtocol {
    func getListVideo(categoryId: Int, page: Int, perPage: Int) {
        self.fitnessService?.getListVideo(categoryId: categoryId, tagsId: nil, keywords: nil, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListVideoFinish(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
