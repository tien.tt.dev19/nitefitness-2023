//
//  
//  VideoLibraryFitPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class VideoLibraryFitPresenter {

    weak var view: VideoLibraryFitViewProtocol?
    private var interactor: VideoLibraryFitInteractorInputProtocol
    private var router: VideoLibraryFitRouterProtocol

    init(interactor: VideoLibraryFitInteractorInputProtocol,
         router: VideoLibraryFitRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    private var listVideos: [VideoModel] = []
    private var categoryId = CategoryType.videoFit.rawValue
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        self.router.showHud()
        self.interactor.getListVideo(categoryId: self.categoryId, page: page, perPage: self.limit)
    }
    
    func checkDataNotFound() {
        // Set Status View Data Not Found
        if self.listVideos.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - VideoLibraryFitPresenterProtocol
extension VideoLibraryFitPresenter: VideoLibraryFitPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    func onViewDidAppear() {
        //
    }
    
    func onDidSelectCategory(category: CategoryChildrenModel?) {
        self.categoryId = category?.id ?? 0
        self.getData(in: 1)
    }
    
    // UITableView
    func numberOfRowsTableView() -> Int {
        return self.listVideos.count
    }
    
    func cellForRowTableView(at indexPath: IndexPath) -> VideoModel {
        return self.listVideos[indexPath.row]
    }
    
    func didSelectRowTableView(at indexPath: IndexPath) {
        let video = self.listVideos[indexPath.row]
        self.router.showVideoDetailsViewController(with: video)
        
    }
    
    func onLoadMoreAction() {
        if self.listVideos.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
}

// MARK: - VideoLibraryFitInteractorOutput
extension VideoLibraryFitPresenter: VideoLibraryFitInteractorOutputProtocol {
    func onGetListVideoFinish(with result: Result<[VideoModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let videosModel):
            if page <= 1 {
                self.listVideos = videosModel
                self.view?.onReloadDataTableView()
                
            } else {
                for object in videosModel {
                    self.listVideos.append(object)
                    self.view?.onInsertRow(at: self.listVideos.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listVideos.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            break
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
}
