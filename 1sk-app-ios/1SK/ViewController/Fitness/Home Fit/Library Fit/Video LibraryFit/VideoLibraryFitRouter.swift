//
//  
//  VideoLibraryFitRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class VideoLibraryFitRouter: BaseRouter {
    static func setupModule() -> VideoLibraryFitViewController {
        let viewController = VideoLibraryFitViewController()
        let router = VideoLibraryFitRouter()
        let interactorInput = VideoLibraryFitInteractorInput()
        let presenter = VideoLibraryFitPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.fitnessService = FitnessService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - VideoLibraryFitRouterProtocol
extension VideoLibraryFitRouter: VideoLibraryFitRouterProtocol {
    func showVideoDetailsViewController(with video: VideoModel) {
        let controller = PlayVideoRouter.setupModule(with: video, categoryType: .videoFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
