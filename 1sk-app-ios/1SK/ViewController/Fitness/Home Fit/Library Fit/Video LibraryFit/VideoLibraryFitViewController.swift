//
//  
//  VideoLibraryFitViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class VideoLibraryFitViewController: BaseViewController {

    var presenter: VideoLibraryFitPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitUITableView()
    }
    
    // MARK: - Action
    
}

// MARK: - LibraryFitVideoViewProtocol
extension VideoLibraryFitViewController: VideoLibraryFitViewProtocol {
    func onScrollToTopTableView() {
        self.tbv_TableView.setContentOffset(.zero, animated: false)
    }
    
    func onReloadDataTableView() {
        self.tbv_TableView.reloadData()
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
}

// MARK: - UITableViewDataSource
extension VideoLibraryFitViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewDiscoveryVideo.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRowsTableView()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewDiscoveryVideo.self, for: indexPath)
        let item = self.presenter.cellForRowTableView(at: indexPath)
        cell.config(with: item)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension VideoLibraryFitViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.didSelectRowTableView(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.subviews.first as? CellTableViewDiscoveryVideo) != nil {
            let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            
            if isReachingEnd && self.isLoading == false {
                self.isLoading = true
                self.presenter.onLoadMoreAction()
            }
        }
    }
}
