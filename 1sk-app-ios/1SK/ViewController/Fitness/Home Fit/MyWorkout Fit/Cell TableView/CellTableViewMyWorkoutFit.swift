//
//  CellTableViewMyWorkoutFit.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//

import UIKit

class CellTableViewMyWorkoutFit: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
