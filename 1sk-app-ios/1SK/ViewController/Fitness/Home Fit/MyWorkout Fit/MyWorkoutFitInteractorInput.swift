//
//  
//  MyWorkoutFitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MyWorkoutFitInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol MyWorkoutFitInteractorOutputProtocol: AnyObject {
    
}

// MARK: - MyWorkoutFit InteractorInput
class MyWorkoutFitInteractorInput {
    weak var output: MyWorkoutFitInteractorOutputProtocol?
}

// MARK: - MyWorkoutFit InteractorInputProtocol
extension MyWorkoutFitInteractorInput: MyWorkoutFitInteractorInputProtocol {

}
