//
//  
//  MyWorkoutFitRouter.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MyWorkoutFitRouterProtocol {

}

// MARK: - MyWorkoutFit Router
class MyWorkoutFitRouter {
    weak var viewController: MyWorkoutFitViewController?
    
    static func setupModule() -> MyWorkoutFitViewController {
        let viewController = MyWorkoutFitViewController()
        let router = MyWorkoutFitRouter()
        let interactorInput = MyWorkoutFitInteractorInput()
        let viewModel = MyWorkoutFitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MyWorkoutFit RouterProtocol
extension MyWorkoutFitRouter: MyWorkoutFitRouterProtocol {
    
}
