//
//  
//  MyWorkoutFitViewController.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol MyWorkoutFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - MyWorkoutFit ViewController
class MyWorkoutFitViewController: BaseViewController {
    var router: MyWorkoutFitRouterProtocol!
    var viewModel: MyWorkoutFitViewModelProtocol!
    
    @IBOutlet weak var tbv_MyWorkoutFit: UITableView!

    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        tbv_MyWorkoutFit.registerNib(ofType: CellTableViewMyWorkoutFit.self)
        tbv_MyWorkoutFit.delegate = self
        tbv_MyWorkoutFit.dataSource = self
    }
    
    // MARK: - Action
    
}

// MARK: - UITableViewDataSource
extension MyWorkoutFitViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 118
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewMyWorkoutFit.self, for: indexPath)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MyWorkoutFitViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - MyWorkoutFit ViewProtocol
extension MyWorkoutFitViewController: MyWorkoutFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}
