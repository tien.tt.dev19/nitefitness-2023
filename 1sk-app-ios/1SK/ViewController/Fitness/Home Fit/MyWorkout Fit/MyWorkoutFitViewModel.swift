//
//  
//  MyWorkoutFitViewModel.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MyWorkoutFitViewModelProtocol {
    func onViewDidLoad()
    //func onViewDidAppear()
    
//     UITableView
    func numberOfSections() -> Int
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> Any?
    func didSelectRow(at indexPath: IndexPath)
    
    // UICollectionView
    //func numberOfItems() -> Int
    //func cellForItem(at indexPath: IndexPath) -> Any
    //func didSelectItem(at indexPath: IndexPath)
}

// MARK: - MyWorkoutFit ViewModel
class MyWorkoutFitViewModel {
    weak var view: MyWorkoutFitViewProtocol?
    private var interactor: MyWorkoutFitInteractorInputProtocol

    init(interactor: MyWorkoutFitInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - MyWorkoutFit ViewModelProtocol
extension MyWorkoutFitViewModel: MyWorkoutFitViewModelProtocol {
    
    func onViewDidLoad() {
        
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfRows() -> Int {
        return 10
    }
    
    func cellForRow(at indexPath: IndexPath) -> Any? {
        return UITableViewCell()
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        
    }
}

// MARK: - MyWorkoutFit InteractorOutputProtocol
extension MyWorkoutFitViewModel: MyWorkoutFitInteractorOutputProtocol {

}
