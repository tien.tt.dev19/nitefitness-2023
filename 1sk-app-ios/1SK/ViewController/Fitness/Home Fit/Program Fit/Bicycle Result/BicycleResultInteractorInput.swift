//
//  
//  BicycleResultInteractorInput.swift
//  1SK
//
//  Created by Thaad on 20/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol BicycleResultInteractorInputProtocol {
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], objectSection: IndoorBikeModel)
}

// MARK: - Interactor Output Protocol
protocol BicycleResultInteractorOutputProtocol: AnyObject {
    func onSetUploadFileLogWokoutFinished(with result: Result<[IndoorBikeListData], APIError>, objectSection: IndoorBikeModel)
}

// MARK: - BicycleResult InteractorInput
class BicycleResultInteractorInput {
    weak var output: BicycleResultInteractorOutputProtocol?
    var fitService: FitServiceProtocol?
}

// MARK: - BicycleResult InteractorInputProtocol
extension BicycleResultInteractorInput: BicycleResultInteractorInputProtocol {
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], objectSection: IndoorBikeModel) {
        self.fitService?.setUploadLogFileBikeWokout(sectionId: sectionId, startTimeSection: startTimeSection, endTimeSection: endTimeSection, filePaths: filePaths, completion: { [weak self] result in
            self?.output?.onSetUploadFileLogWokoutFinished(with: result.unwrapSuccessModel(), objectSection: objectSection)
        })
    }
}
