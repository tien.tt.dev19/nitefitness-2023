//
//  
//  BicycleResultRouter.swift
//  1SK
//
//  Created by Thaad on 20/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol BicycleResultRouterProtocol {
    func popToRootViewController()
}

// MARK: - BicycleResult Router
class BicycleResultRouter {
    weak var viewController: BicycleResultViewController?
    
    static func setupModule(exercise: ExerciseFitModel?, objectSection: IndoorBikeModel?) -> BicycleResultViewController {
        let viewController = BicycleResultViewController()
        let router = BicycleResultRouter()
        let interactorInput = BicycleResultInteractorInput()
        let viewModel = BicycleResultViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.exercise = exercise
        viewModel.objectSection = objectSection
        interactorInput.output = viewModel
        interactorInput.fitService = FitService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - BicycleResult RouterProtocol
extension BicycleResultRouter: BicycleResultRouterProtocol {
    func popToRootViewController() {
        self.viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
