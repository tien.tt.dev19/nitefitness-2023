//
//  
//  BicycleResultViewController.swift
//  1SK
//
//  Created by Thaad on 20/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol BicycleResultViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setUIData(exercise: ExerciseFitModel?)
}

// MARK: - BicycleResult ViewController
class BicycleResultViewController: BaseViewController {
    var router: BicycleResultRouterProtocol!
    var viewModel: BicycleResultViewModelProtocol!
    
    @IBOutlet weak var prg_Wokout: UIProgressView!
    @IBOutlet weak var lbl_CaloriesWokout: UILabel!
    @IBOutlet weak var lbl_Distance: UILabel!
    @IBOutlet weak var lbl_Calories: UILabel!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.isInteractivePopGestureEnable = false
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.router.popToRootViewController()
    }
    
}

// MARK: - BicycleResult ViewProtocol
extension BicycleResultViewController: BicycleResultViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setUIData(exercise: ExerciseFitModel?) {
        if let energy = exercise?.workout?.sumCalories, energy > 0 {
            self.lbl_CaloriesWokout.text = "\(Int(energy))/\(exercise?.kcal ?? 0)"
            self.lbl_Calories.text = Int(energy).asString
            
            if let kcal = exercise?.kcal {
                let kcalDouble = Double(kcal)
                let value = energy / kcalDouble
                self.prg_Wokout.setProgress(Float(value), animated: true)
                
            } else {
                self.prg_Wokout.setProgress(Float(1.0), animated: true)
            }
            
        } else {
            self.lbl_CaloriesWokout.text = "--/\(exercise?.kcal ?? 0)"
            self.lbl_Calories.text = "--"
            
            self.prg_Wokout.setProgress(Float(0), animated: true)
        }
        
        if let totalDistance = exercise?.workout?.sumDistance {
            let distance = totalDistance / 1000
            self.lbl_Distance.text = distance.rounded(toPlaces: 1).asString
        }
    }
}
