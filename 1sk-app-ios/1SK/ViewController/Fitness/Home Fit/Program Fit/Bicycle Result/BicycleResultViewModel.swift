//
//  
//  BicycleResultViewModel.swift
//  1SK
//
//  Created by Thaad on 20/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol BicycleResultViewModelProtocol {
    func onViewDidLoad()
}

// MARK: - BicycleResult ViewModel
class BicycleResultViewModel {
    weak var view: BicycleResultViewProtocol?
    private var interactor: BicycleResultInteractorInputProtocol
    private let realm: RealmManagerProtocol = RealmManager()
    
    init(interactor: BicycleResultInteractorInputProtocol) {
        self.interactor = interactor
    }

    var exercise: ExerciseFitModel?
    var objectSection: IndoorBikeModel?
    
    func setUploadDataLogExercise() {
        guard let object: IndoorBikeModel = self.realm.object({$0.id == self.objectSection?.id}) else {
            print("./UPLOAD: data nil")
            return
        }
        
        let endTime = Int(Date().timeIntervalSince1970)
        IndoorBikeManager.shared.onSetEndTimeFile(endTimeFile: endTime, objectSection: object)
        IndoorBikeManager.shared.onSetEndTimeSection(endTimeSection: endTime, objectSection: object)
        
        let startTimeSection = object.start_time ?? 0
        let endTimeSection = object.end_time ?? 0
        
        let filePaths = object.log_path.array.map { $0.filePath}
        
        guard filePaths.count > 0 else {
            print("./UPLOAD: filePaths nil")
            return
        }
        self.view?.showHud()
        self.interactor.setUploadLogFileBikeWokout(sectionId: object.id, startTimeSection: startTimeSection, endTimeSection: endTimeSection, filePaths: filePaths, objectSection: object)
    }
}

// MARK: - BicycleResult ViewModelProtocol
extension BicycleResultViewModel: BicycleResultViewModelProtocol {
    func onViewDidLoad() {
        self.view?.setUIData(exercise: self.exercise)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.setUploadDataLogExercise()
        }
    }
}

// MARK: - BicycleResult InteractorOutputProtocol
extension BicycleResultViewModel: BicycleResultInteractorOutputProtocol {
    func onSetUploadFileLogWokoutFinished(with result: Result<[IndoorBikeListData], APIError>, objectSection: IndoorBikeModel) {
        switch result {
        case .success(let model):
            print("./UPLOAD: Finished true: ", model)
            
            IndoorBikeManager.shared.onRemoveFileLogData(objectSection: objectSection)
            
        case .failure(let error):
            print("./UPLOAD: Finished false: ", error)
        }
        self.view?.hideHud()
    }
}
