//
//  AlertPopupCompleteWorkoutViewController.swift
//  1SK
//
//  Created by Thaad on 21/04/2022.
//

import UIKit

protocol AlertPopupCompleteWorkoutViewDelegate: AnyObject {
    func onAlertPopupCompleteWorkoutDismissAction()
}

class AlertPopupCompleteWorkoutViewController: UIViewController {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    weak var delegate: AlertPopupCompleteWorkoutViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /// Auto Dismiss
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.onTapGestureAction()
        }
    }
}

// MARK: - Init Animation
extension AlertPopupCompleteWorkoutViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertPopupCompleteWorkoutViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.delegate?.onAlertPopupCompleteWorkoutDismissAction()
        self.hiddenAnimate()
    }
}
