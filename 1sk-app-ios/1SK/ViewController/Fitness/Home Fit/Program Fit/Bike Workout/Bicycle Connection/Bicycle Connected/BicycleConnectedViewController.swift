//
//  BicycleConnectedViewController.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//

import UIKit
import CoreBluetooth

class BicycleConnectedViewController: UIViewController {

    @IBOutlet weak var lbl_Name: UILabel!
    
    var model: CBPeripheral? {
        didSet {
            self.lbl_Name.text = model?.name
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
