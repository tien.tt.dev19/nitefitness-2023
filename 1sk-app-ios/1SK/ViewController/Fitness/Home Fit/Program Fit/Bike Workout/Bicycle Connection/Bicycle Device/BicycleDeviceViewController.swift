//
//  BicycleDeviceViewController.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//

import UIKit
import CoreBluetooth

protocol BicycleDeviceViewDelegate: AnyObject {
    func onDidSelectDevice(peripheral: CBPeripheral, isAuto: Bool)
}

class BicycleDeviceViewController: UIViewController {

    @IBOutlet weak var tbv_TableView: UITableView!
    
    weak var delegate: BicycleDeviceViewDelegate?
    
    var listPeripherals: [CBPeripheral] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setInitUITableView()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
//            guard self.listPeripherals.count == 1 else {
//                return
//            }
//            self.delegate?.onDidSelectDevice(peripheral: self.listPeripherals[0], isAuto: true)
//        }
//    }
    
    func onReloadData() {
        guard self.tbv_TableView != nil else {
            return
        }
        self.tbv_TableView.reloadData()
    }
}

// MARK: UITableViewDataSource
extension BicycleDeviceViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewBicycleDevice.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listPeripherals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewBicycleDevice.self, for: indexPath)
        cell.model = self.listPeripherals[indexPath.row]
        return cell
    }
}

// MARK: UITableViewDelegate
extension BicycleDeviceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.onDidSelectDevice(peripheral: self.listPeripherals[indexPath.row], isAuto: false)
    }
}
