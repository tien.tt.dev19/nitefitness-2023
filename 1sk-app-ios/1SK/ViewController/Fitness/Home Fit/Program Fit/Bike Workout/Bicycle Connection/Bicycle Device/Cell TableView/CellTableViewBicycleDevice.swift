//
//  CellTableViewBicycleDevice.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//

import UIKit
import CoreBluetooth

class CellTableViewBicycleDevice: UITableViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var btn_Status: UIButton!
    @IBOutlet weak var lbl_Status: UILabel!
    
    var model: CBPeripheral? {
        didSet {
            self.lbl_Name.text = model?.name
            
            if self.model?.state == .connected {
                self.lbl_Name.textColor = R.color.mainColor()
                self.btn_Status.isSelected = true
                self.lbl_Status.text = "Đã kết nối"
                self.lbl_Status.textColor = R.color.mainColor()
                
            } else {
                self.lbl_Name.textColor = R.color.color_Gray_Text()
                self.btn_Status.isSelected = false
                self.lbl_Status.text = "Chưa kết nối"
                self.lbl_Status.textColor = UIColor.black
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.btn_Status.setImage(R.image.ic_ble_connection(), for: .normal)
        self.btn_Status.setImage(R.image.ic_ble_connected(), for: .selected)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
