//
//  BicycleNotFoundViewController.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//

import UIKit

protocol BicycleNotFoundViewDelegate: AnyObject {
    func onRetryFindAction()
}

class BicycleNotFoundViewController: UIViewController {

    weak var delegate: BicycleNotFoundViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onRetryFindAction(_ sender: Any) {
        self.delegate?.onRetryFindAction()
    }
}
