//
//  
//  BicycleConnectionInteractorInput.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol BicycleConnectionInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol BicycleConnectionInteractorOutputProtocol: AnyObject {
    
}

// MARK: - BicycleConnection InteractorInput
class BicycleConnectionInteractorInput {
    weak var output: BicycleConnectionInteractorOutputProtocol?
}

// MARK: - BicycleConnection InteractorInputProtocol
extension BicycleConnectionInteractorInput: BicycleConnectionInteractorInputProtocol {

}
