//
//  
//  BicycleConnectionRouter.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol BicycleConnectionRouterProtocol {
    func onDismissViewController()
}

// MARK: - BicycleConnection Router
class BicycleConnectionRouter {
    weak var viewController: BicycleConnectionViewController?
    
    static func setupModule(delegate: BicycleConnectionViewDelegate?) -> BicycleConnectionViewController {
        let viewController = BicycleConnectionViewController()
        let router = BicycleConnectionRouter()
        let interactorInput = BicycleConnectionInteractorInput()
        let viewModel = BicycleConnectionViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - BicycleConnection RouterProtocol
extension BicycleConnectionRouter: BicycleConnectionRouterProtocol {
    func onDismissViewController() {
        self.viewController?.dismiss(animated: true)
    }
}
