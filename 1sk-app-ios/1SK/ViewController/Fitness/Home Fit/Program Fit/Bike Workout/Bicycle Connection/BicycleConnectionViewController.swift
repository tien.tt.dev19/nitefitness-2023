//
//  
//  BicycleConnectionViewController.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//
//

import UIKit
import CoreBluetooth

enum StateViewBicycleConnection {
    case Pair
    case Finding
    case Device
    case Connected
    case NotFound
}

protocol BicycleConnectionViewDelegate: AnyObject {
    func onBicycleConnectSuccess(peripheral: CBPeripheral)
}

// MARK: - ViewProtocol
protocol BicycleConnectionViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - BicycleConnection ViewController
class BicycleConnectionViewController: BaseViewController {
    var router: BicycleConnectionRouterProtocol!
    var viewModel: BicycleConnectionViewModelProtocol!
    weak var delegate: BicycleConnectionViewDelegate?
    
    private lazy var viewBicycleStartPair = BicycleStartPairViewController()
    private lazy var viewBicycleFinding = BicycleFindingViewController()
    private lazy var viewBicycleDevice = BicycleDeviceViewController()
    private lazy var viewBicycleConnected = BicycleConnectedViewController()
    private lazy var viewBicycleNotFound = BicycleNotFoundViewController()
    
    var state: StateViewBicycleConnection = .Pair
    
    @IBOutlet weak var view_Container: UIView!
    @IBOutlet weak var constraint_right_BtnClose: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUIButtonClose()
        self.setStateView(state: .Pair, device: nil)
        self.setInitBluetoothManager()
    }
    
    func setInitUIButtonClose() {
        switch UIDevice.current.screen {
        case .iPhones_6_6s_7_8, .iPhones_6Plus_6sPlus_7Plus_8Plus, .iPhones_5_5s_5c_SE:
            self.constraint_right_BtnClose.constant = 16
            
        default:
            self.constraint_right_BtnClose.constant = -24
        }
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.router.onDismissViewController()
    }
    
}

// MARK: - BicycleConnection ViewProtocol
extension BicycleConnectionViewController: BicycleConnectionViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - BLE Manager
extension BicycleConnectionViewController: BluetoothManagerBikeDelegate {
    func setInitBluetoothManager() {
        print("<BLE> BicycleConnection setInitBluetoothManager")
        guard BluetoothManagerBike.shared == nil else {
            BluetoothManagerBike.shared?.delegate = self
            return
        }
        BluetoothManagerBike.shared = BluetoothManagerBike()
        BluetoothManagerBike.shared?.deviceType = .bikeYesoul
        BluetoothManagerBike.shared?.delegate = self
    }
    
    func onDidUpdateState(central: CBCentralManager) {
        print("<BLE> BicycleConnection onDidUpdateState central:", central.state)
        switch central.state {
        case .poweredOn:
            BluetoothManagerBike.shared?.setStartScanPeripherals()
            
        default:
            break
        }
    }
    
    func onDidTimeoutScanDevice() {
        print("<BLE> BicycleConnection onDidTimeoutScanDevice")
        self.hideHud()
        
        BluetoothManagerBike.shared?.setStopScanPeripherals()
        guard BluetoothManagerBike.shared?.listPeripherals.count ?? 0 > 0 else {
            self.setStateView(state: .NotFound, device: nil)
            return
        }
        guard self.state != .Device else {
            return
        }
        self.setStateView(state: .Device, device: nil)
    }
    
    func onDidDiscover(peripheral: CBPeripheral) {
        print("<BLE> BicycleConnection onDidDiscover peripheral: \(peripheral.uuid) - \(peripheral.name ?? "null")")
        if BluetoothManagerBike.shared?.listPeripherals.count == 1 {
            self.setStateView(state: .Device, device: nil)
        } else {
            self.viewBicycleDevice.listPeripherals = BluetoothManagerBike.shared?.listPeripherals ?? []
            self.viewBicycleDevice.onReloadData()
        }
    }
    
    func onDidFailToConnect(peripheral: CBPeripheral) {
        print("<BLE> BicycleConnection onDidFailToConnect peripheral: \(peripheral.uuid) - \(peripheral.name ?? "null")")
        self.hideHud()
        //SKToast.shared.showToast(content: "Kết nối thất bại")
    }
    
    func onDidConnected(peripheral: CBPeripheral) {
        print("<BLE> BicycleConnection onDidConnect peripheral: \(peripheral.uuid) - \(peripheral.name ?? "null")")
        BluetoothManagerBike.shared?.setStopScanPeripherals()
        self.delegate?.onBicycleConnectSuccess(peripheral: peripheral)
        self.viewBicycleDevice.onReloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.setStateView(state: .Connected, device: peripheral)
        }
    }
    
    func onDidDisconnect(peripheral: CBPeripheral) {
        print("<BLE> BicycleConnection onDidDisconnect peripheral:", peripheral.uuid)
        self.hideHud()
    }
}

// MARK: - State View
extension BicycleConnectionViewController {
    func setStateView(state: StateViewBicycleConnection, device: CBPeripheral?) {
        self.state = state
        self.hideHud()
        
        switch state {
        case .Pair:
            self.addChildView(childViewController: self.viewBicycleStartPair)
            self.viewBicycleStartPair.delegate = self
            
        case .Finding:
            self.addChildView(childViewController: self.viewBicycleFinding)
            BluetoothManagerBike.shared?.setStartScanPeripherals()
            BluetoothManagerBike.shared?.setStartTimer(timeout: 10.0)
            
        case .Device:
            self.addChildView(childViewController: self.viewBicycleDevice)
            self.viewBicycleDevice.delegate = self
            self.viewBicycleDevice.listPeripherals = BluetoothManagerBike.shared?.listPeripherals ?? []
            self.viewBicycleDevice.onReloadData()
            
        case .Connected:
            self.addChildView(childViewController: self.viewBicycleConnected)
            self.viewBicycleConnected.model = device
            
        case .NotFound:
            self.addChildView(childViewController: self.viewBicycleNotFound)
            self.viewBicycleNotFound.delegate = self
        }
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: - BicycleStartPairViewDelegate
extension BicycleConnectionViewController: BicycleStartPairViewDelegate {
    func onStartPairAction() {
        switch BluetoothManagerBike.shared?.state() {
        case .poweredOn:
            print("<BLE> BicycleConnection central.state is .poweredOn")
            //SKToast.shared.showToast(content: "central.state is .poweredOn")
            self.setStateView(state: .Finding, device: nil)
            
        case .poweredOff:
            print("<BLE> BicycleConnection central.state is .poweredOff")
            //SKToast.shared.showToast(content: "central.state is .poweredOff")
            self.alertOpenSettingsBluetooth(title: "Bluetooth chưa bật", message: "1SK Muốn sử dụng Bluetooth cho các kết nối mới \nBạn có thể cho phép các kết nối mới trong cài đặt")
            
        case .unknown:
            print("<BLE> BicycleConnection central.state is .unknown")
            //SKToast.shared.showToast(content: "central.state is .unknown")
            self.alertDefault(title: "Bluetooth", message: "Không xác định")
            
        case .resetting:
            print("<BLE> BicycleConnection central.state is .resetting")
            //SKToast.shared.showToast(content: "central.state is .resetting")
            self.alertDefault(title: "Bluetooth", message: "Đang kết nối thiết bị")
            
        case .unsupported:
            print("<BLE> BicycleConnection central.state is .unsupported")
            //SKToast.shared.showToast(content: "central.state is .unsupported")
            self.alertDefault(title: "Bluetooth", message: "Thiết bị này không hỗ trợ")
            
        case .unauthorized:
            print("<BLE> BicycleConnection central.state is .unauthorized")
            //SKToast.shared.showToast(content: "central.state is .unauthorized")
            self.alertOpenSettingsURLString(title: "1SK muốn sử dụng Bluetooth", message: "Bạn có thể cho phép các kết nối mới trong cài đặt sử dụng dữ liệu của thiết bị")
            
        default:
            print("<BLE> BicycleConnection central.state is .default")
            //SKToast.shared.showToast(content: "central.state is .default")
            self.alertDefault(title: "Bluetooth", message: "Chưa khởi tạo")
        }
    }
}

// MARK: - BicycleDeviceViewDelegate
extension BicycleConnectionViewController: BicycleDeviceViewDelegate {
    func onDidSelectDevice(peripheral: CBPeripheral, isAuto: Bool) {
        if isAuto {
            self.setConnectPeripheral(peripheral)
            
        } else {
            self.setConnectPeripheral(peripheral)
            
            //self.presentAlertPopupConfirmConnect(peripheral: peripheral)
        }
    }
    
    func setConnectPeripheral(_ peripheral: CBPeripheral) {
        self.showHud()
        BluetoothManagerBike.shared?.setConnectDevice(peripheral: peripheral)
        DispatchQueue.main.asyncAfter(deadline: .now() + 10) {
            self.hideHud()
            if peripheral.state != .connected {
                BluetoothManagerBike.shared?.setCancelConnectDevice(peripheral: peripheral)
                //SKToast.shared.showToast(content: "Kết nối thiết bị thất bại")
            }
        }
    }
}

// MARK: - BicycleNotFoundViewDelegate
extension BicycleConnectionViewController: BicycleNotFoundViewDelegate {
    func onRetryFindAction() {
        self.setStateView(state: .Finding, device: nil)
    }
}

// MARK: AlertPopupConfirmTitleLandscapeViewDelegate
extension BicycleConnectionViewController: AlertPopupConfirmTitleLandscapeViewDelegate {
    func presentAlertPopupConfirmConnect(peripheral: CBPeripheral?) {
        let controller = AlertPopupConfirmTitleLandscapeViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        controller.lblTitle = "Kết nối thiết bị"
        controller.lblContent = peripheral?.name ?? "null"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xác nhận"
        
        controller.object = peripheral
        controller.tag = 0
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirmCancelAction(object: Any?, tag: Int) {
        print("onAlertPopupConfirmCancelAction")
    }
    
    func onAlertPopupConfirmAgreeAction(object: Any?, tag: Int) {
        switch tag {
        case 0:
            guard let peripheral = object as? CBPeripheral else {
                return
            }
            self.setConnectPeripheral(peripheral)
            
        case 1:
            break
            
        default:
            break
        }
    }
}
