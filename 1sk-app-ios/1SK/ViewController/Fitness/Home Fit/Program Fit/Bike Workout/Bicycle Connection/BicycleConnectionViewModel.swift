//
//  
//  BicycleConnectionViewModel.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol BicycleConnectionViewModelProtocol {
    func onViewDidLoad()
}

// MARK: - BicycleConnection ViewModel
class BicycleConnectionViewModel {
    weak var view: BicycleConnectionViewProtocol?
    private var interactor: BicycleConnectionInteractorInputProtocol

    init(interactor: BicycleConnectionInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - BicycleConnection ViewModelProtocol
extension BicycleConnectionViewModel: BicycleConnectionViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - BicycleConnection InteractorOutputProtocol
extension BicycleConnectionViewModel: BicycleConnectionInteractorOutputProtocol {

}
