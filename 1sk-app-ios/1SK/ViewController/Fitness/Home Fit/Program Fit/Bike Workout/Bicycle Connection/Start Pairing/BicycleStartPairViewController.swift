//
//  BicycleStartPairViewController.swift
//  1SK
//
//  Created by Thaad on 18/04/2022.
//

import UIKit

protocol BicycleStartPairViewDelegate: AnyObject {
    func onStartPairAction()
}

class BicycleStartPairViewController: BaseViewController {

    weak var delegate: BicycleStartPairViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    deinit {
        print("deinit BicycleStartPairingViewController")
    }

    @IBAction func onStartPairAction(_ sender: Any) {
        self.delegate?.onStartPairAction()
    }
}
