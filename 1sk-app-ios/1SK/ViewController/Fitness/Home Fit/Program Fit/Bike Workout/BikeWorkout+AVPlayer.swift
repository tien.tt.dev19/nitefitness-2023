//
//  BikeWorkout+AVPlayer.swift
//  1SK
//
//  Created by Thaad on 20/04/2022.
//

import Foundation
import AVFoundation
import UIKit

// MARK: AVPlayer Video
extension BikeWorkoutViewController {
    func setInitAVPlayer(exercise: ExerciseFitModel?) {
        let videoUrl = "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"
        
        guard let url = URL(string: videoUrl) else {
            SKToast.shared.showToast(content: "Error video link: \(videoUrl)")
            return
        }
        
        self.setTimerStartCountDown()
        self.btn_Resolution.setTitle(VideoResolution.auto.name, for: .normal)
        
        if self.avPlayer == nil {
            self.preparelayVideoWithFisrt(url: url)
            
        } else {
            self.preparePlayVideoNext(url: url)
        }
    }
    
    func setDeinitAVPlayer() {
        self.avPlayer?.pause()
        self.avPlayer = nil
        self.avPlayerLayer = nil
        
        self.setTimerStopCountDown()
        self.setRemoveObserver()
    }
    
    func preparelayVideoWithFisrt(url: URL) {
        let avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer = AVPlayer(playerItem: avPlayerItem)
        
        self.avPlayerLayer = AVPlayerLayer(player: self.avPlayer)
        self.avPlayerLayer?.frame = CGRect(x: 0, y: 0, width: self.view_Player.width, height: self.view_Player.height)
        self.avPlayerLayer?.videoGravity = .resizeAspect
        self.view_Player.layer.addSublayer(self.avPlayerLayer!)
        
        self.onAddObserverCurrentItem()
        self.onAddObserverKVOPlayer()
        
        self.avPlayer?.play()
        self.onPlay()
    }
    
    func preparePlayVideoNext(url: URL) {
        let avPlayerItem = AVPlayerItem(url: url)
        self.avPlayer?.replaceCurrentItem(with: avPlayerItem)
        
        self.onAddObserverCurrentItem()
        self.onResetViewControl()
        
        self.avPlayer?.play()
        self.onPlay()
    }
    
    func onResetViewControl() {
        self.pgr_Tracking.setProgress(Float(0), animated: true)
    }
    
}

// MARK: - Gesture AVPlayer
extension BikeWorkoutViewController {
    func setInitGestureAVPlayer() {
//        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.onDoubleTapGesture))
//        gesture.numberOfTapsRequired = 2
//        self.view_Player.addGestureRecognizer(gesture)
    }
    
    @objc func onDoubleTapGesture() {
//        if self.btn_Play.isHidden {
//            self.setShowUIControlView()
//
//        } else {
//            if self.avPlayer?.isPlaying == true {
//                self.onPause()
//
//            } else {
//                self.onPlay()
//            }
//        }
    }
    
    func setShowUIControlView() {
        UIView.animate(withDuration: 0.3) {
            self.btn_Play.isHidden = false
            self.constraint_top_NavView.constant = 0
                self.view.layoutIfNeeded()
            
        } completion: { _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + self.timeViewControll) {
                self.setHiddenUIControlView()
            }
        }
    }
    
    func setHiddenUIControlView() {
        guard self.avPlayer?.isPlaying == true else {
            return
        }
        UIView.animate(withDuration: 0.3) {
            self.btn_Play.isHidden = true
            self.constraint_top_NavView.constant = -40
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            //
        }
    }
}

// MARK: - Player Handler Control
extension BikeWorkoutViewController {
    func onPlay() {
        self.avPlayer?.play()
        self.btn_Play.isSelected = true
        
        DispatchQueue.main.asyncAfter(deadline: .now() + self.timeViewControll) {
            self.setHiddenUIControlView()
        }
    }
    
    func onPause() {
        self.avPlayer?.pause()
        self.btn_Play.isSelected = false
        self.setShowUIControlView()
    }
    
    func onReplay() {
        self.avPlayer?.seek(to: CMTime.zero)
        self.onPlay()
    }
}

// MARK: KVO Observer
extension BikeWorkoutViewController {
    func onAddObserverKVOPlayer() {
        self.avPlayer?.addObserver(self, forKeyPath: AVConstant.rate, options: [.old, .new], context: nil)
        
        self.onTimeObserver()
    }
    
    func setRemoveObserver() {
        self.avPlayer?.currentItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), context: nil)
        self.avPlayer?.currentItem?.removeObserver(self, forKeyPath: AVConstant.loadedTimeRangesKey, context: nil)
        self.avPlayer?.removeObserver(self, forKeyPath: AVConstant.rate, context: nil)
        self.removePeriodicTimeObserver()
        
//        print("<AVPlayer> setRemoveObserver")
    }
    
    func onAddObserverCurrentItem() {
        self.avPlayer?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.initial, .new], context: nil)
        self.avPlayer?.currentItem?.addObserver(self, forKeyPath: AVConstant.loadedTimeRangesKey, options: [.initial, .new], context: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        switch keyPath {
        case #keyPath(AVPlayerItem.status):
            break
            
        case AVConstant.loadedTimeRangesKey:
            self.onLoadedTimeRangedChanged()
            
        case AVConstant.rate:
             break
            
        default:
            break
        }
    }
    
    private func onLoadedTimeRangedChanged() {
        let duration = self.avPlayer?.currentItem?.asset.duration
        let durationSeconds = CMTimeGetSeconds(duration ?? .zero)
        
        let totalBuffer = self.avPlayer?.currentItem?.totalBuffer()
        //let currentBuffer = self.avPlayer?.currentItem?.currentBuffer()
        //let presentationSize = self.avPlayer?.currentItem?.presentationSize
        
        self.progressValue = CGFloat(totalBuffer ?? 0) / CGFloat(durationSeconds)
        self.pgr_Loading.setProgress(Float(self.progressValue), animated: true)
    }
    
    func removePeriodicTimeObserver() {
        // If a time observer exists, remove it
        if let token = self.avTimeObserver {
            self.avPlayer?.removeTimeObserver(token)
            self.avTimeObserver = nil
        }
    }
    
    func onTimeObserver() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        self.avTimeObserver = self.avPlayer?.addPeriodicTimeObserver(forInterval: interval, queue: nil, using: { [weak self] _ in
            guard let currentItem = self?.avPlayer?.currentItem, let durationCMTimeFormat = self?.avPlayer?.currentItem?.asset.duration else {
                return
            }
            
            let duration = CMTimeGetSeconds(durationCMTimeFormat).rounded(toPlaces: 1)
            let currentTime = currentItem.currentTime().seconds.rounded(toPlaces: 1)
            
            if self?.timePlayer == -1 {
                //self?.timePlayer = duration
                
            } else {
                if self?.timePlayer ?? 0 > 0 {
                    self?.timePlayer -= 1
                    
                } else if self?.timePlayer ?? 0 == 0 {
                    self?.presentAlertPopupCompleteWorkout()
                }
                
            }
            
            let sliderValue = Float(currentTime / Double(duration))
            self?.pgr_Tracking.setProgress(Float(sliderValue), animated: true)
            
            let currentTimeFormat = AVUtils.formatDurationTime(time: Int(currentTime))
            let durationFormat = AVUtils.formatDurationTime(time: Int(duration))
            
            self?.setTimePlayer(currentTime: currentTimeFormat, duration: durationFormat)
            
            // Played To The End
            if currentTimeFormat == durationFormat {
                print("<AVPlayer> addTimeObserver: Played To The End")
            }
            
            print("<AVPlayer> addTimeObserver: duration: ", duration)
            print("<AVPlayer> addTimeObserver: currentTime: ", currentTime)
            print("<AVPlayer> addTimeObserver: ========================\n")
        })
    }
    
    func setTimePlayer(currentTime: String, duration: String) {
        let timeCountDown = AVUtils.formatDurationTime(time: Int(self.timePlayer))
        let durationTotal = AVUtils.formatDurationTime(time: self.timePlayerTotal)
        
        self.lbl_TimePlayer.text = String(format: "%@ / %@", timeCountDown, durationTotal)
    }
}

// MARK: Timer Control
extension BikeWorkoutViewController {
    func setTimerStartCountDown() {
        //self.timerControl = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimeControlDidChange), userInfo: nil, repeats: true)
    }
    
    @objc func onTimeControlDidChange() {
        if self.timePlayer > 1 {
            self.timePlayer -= 1
        }
    }
    
    func setTimerStopCountDown() {
        self.timerControl?.invalidate()
        self.timerControl = nil
        print("<AVPlayer> setTimerStopCountDown")
    }
}

// MARK: - SelectionViewControllerDelegate
extension BikeWorkoutViewController: SelectionViewControllerDelegate {
    func didSelectItem(of type: SelectionType, at index: Int) {
        switch type {
        case .videoResolution:
            let selectedResolution = VideoResolution.allCases[index]
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.setVideoResolution(selectedResolution)
            }
            
        default:
            break
        }
    }
    
    func didDismissView() {
        //
    }
    
    func setVideoResolution(_ resolution: VideoResolution) {
        print("<AVPlayer> setVideoResolution resolution:", resolution.name)
        self.btn_Resolution.setTitle(resolution.name, for: .normal)
        
        switch resolution {
        case .auto:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize.zero
            
        case ._1080:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1920, height: 1080)
            
        case ._720:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 1280, height: 720)
            
        case ._480:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 854, height: 480)
            
        case ._360:
            self.avPlayer?.currentItem?.preferredMaximumResolution = CGSize(width: 640, height: 360)
        }
    }
}

// MARK: - UIApplication
extension BikeWorkoutViewController {
    func setInitUIApplication() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppWillEnterForeground), name: .applicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppDidEnterBackground), name: .applicationDidEnterBackground, object: nil)
        
    }
    
    @objc func onAppWillEnterForeground() {
        self.onPlay()
    }
    
    @objc func onAppDidEnterBackground() {
        self.onPause()
    }
}
