//
//  BikeWorkout+BLE.swift
//  1SK
//
//  Created by Thaad on 20/04/2022.
//

import Foundation
import CoreBluetooth

// MARK: - BLE Manager
extension BikeWorkoutViewController: BluetoothManagerBikeDelegate, BicycleConnectionViewDelegate {
    func setInitBluetoothManager() {
        print("<BLE>BikeWorkoutsetInitBluetoothManager")
        
        self.setInitUIViewInfoDevice()
        guard let device = UserDefaults.standard.LIST_DEVICE_CONNECT.first(where: {$0.type == DeviceType.bikeYesoul.rawValue}) else {
            return
        }
        self.deviceConnect = device
        print("<BLE>BikeWorkoutsetInitBluetoothManager device: \(device.uuid ?? "Null") - \(device.name ?? "Null")")
        if BluetoothManagerBike.shared != nil {
            self.setDeinitBluetoothManager()
        }
        
        BluetoothManagerBike.shared = BluetoothManagerBike()
        BluetoothManagerBike.shared?.deviceType = .bikeYesoul
        BluetoothManagerBike.shared?.delegate = self
        
    }
    
    func setDeinitBluetoothManager() {
        BluetoothManagerBike.shared = nil
    }
    
    // BluetoothManagerDelegate
    func onDidUpdateState(central: CBCentralManager) {
        print("<BLE>BikeWorkoutonDidUpdateState central:", central.state)
        guard let bluetooth = BluetoothManagerBike.shared else {
            return
        }
        bluetooth.setStartScanPeripherals()
    }
    
    func onDidTimeoutScanDevice() {
        print("<BLE>BikeWorkoutonDidTimeoutScanDevice")
    }
    
    func onDidDiscover(peripheral: CBPeripheral) {
        print("<BLE>BikeWorkoutonDidDiscover peripheral: \(peripheral.uuid) - \(peripheral.name ?? "null")")
        guard peripheral.uuid == self.deviceConnect?.uuid else {
            return
        }
        BluetoothManagerBike.shared?.setConnectDevice(peripheral: peripheral)
    }
    
    func onDidFailToConnect(peripheral: CBPeripheral) {
        print("<BLE>BikeWorkoutonDidFailToConnect peripheral: \(peripheral.uuid) - \(peripheral.name ?? "null")")
    }
    
    func onDidConnected(peripheral: CBPeripheral) {
        print("<BLE>BikeWorkoutonDidConnect peripheral:", peripheral.uuid)
        guard let bluetooth = BluetoothManagerBike.shared else {
            return
        }
        bluetooth.setStopScanPeripherals()
        self.onBicycleConnectSuccess(peripheral: peripheral)
    }
    
    func onDidDisconnect(peripheral: CBPeripheral) {
        print("<BLE>BikeWorkoutonDidDisconnect peripheral:", peripheral.uuid)
        
        BluetoothManagerBike.shared?.deviceConnected = nil
        
        self.lbl_StatusConnect.text = "Chưa có xe đạp được kết nối"
        self.lbl_InstantaneousCadence.text = "--"
        self.lbl_InstantaneousPower.text = "--"
        self.lbl_ResistanceLevel.text = "--"
        
        self.btn_Disconnect.isHidden = true
        self.btn_Connection.isHidden = false
        
        // Change file name and save
        self.viewModel.onSetEndTimeFileLog(endTimeFile: Int(Date().timeIntervalSince1970))
        
        FileHelper.shared.printContentsOfDocumentDirectory(title: "./fit/log:", path: "/fit/log/")
    }
    
    // BicycleConnectionViewDelegate
    func onBicycleConnectSuccess(peripheral: CBPeripheral) {
        peripheral.delegate = self
        BluetoothManagerBike.shared?.delegate = self
        
        self.lbl_StatusConnect.text = "Thiết bị đã kết nối:\n\(peripheral.name ?? "Null")"
        self.btn_Connection.isHidden = true
        self.btn_Disconnect.isHidden = false
        
        self.setShowViewInfoDevice()
        self.viewModel.onSetDeviceNameSectionLog(deviceName: peripheral.name)
    }
}

// MARK: CBPeripheralDelegate
extension BikeWorkoutViewController: CBPeripheralDelegate {
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        guard let services = peripheral.services else { return }
        for service in services {
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        guard let characteristics = service.characteristics else {return}
        for characteristics in characteristics {
            print("<BLE> ------------ \(characteristics.uuid)")
            
            if characteristics.properties.contains(.read) {
                peripheral.readValue(for: characteristics)
            }
            
            if characteristics.properties.contains(.write) {
                //characteristics.uuid
            }
            
            if characteristics.properties.contains(.writeWithoutResponse) {
                
            }
            
            if characteristics.properties.contains(.notify) {
                peripheral.setNotifyValue(true, for: characteristics)
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if let data = characteristic.value {
            if characteristic.uuid.uuidString == CharacteristicIndoorBikeData.uuidString {
                print("<BLE> characteristic.value: ", [UInt8](data))
                print("<BLE> characteristic.value: ---------------------------- End 1 time")
                
                self.onDecodeIndoorBikeMess(with: data)
            }
        }
    }
    
    func onDecodeIndoorBikeMess(with data: Data) {
        self.onSaveDataToFile(with: data)
        
        let hrData: Result<CharacteristicIndoorBikeData, BluetoothDecodeError> = CharacteristicIndoorBikeData.decode(with: data)
        switch hrData {
        case .success(let char):
            self.onDidUpdateValue(char: char)
            
            print("<BLE> TỐC ĐỘ: \(char.instantaneousSpeed?.speed.value ?? -1)")
            print("<BLE> TỐC ĐỘ TRUNG BÌNH: \(char.averageSpeed?.speed.value ?? -1)") //Not found
            print("<BLE> NHỊP ĐỘ: \(char.instantaneousCadence?.value ?? -1)")
            print("<BLE> NHỊP ĐỘ TRUNG BÌNH: \(char.averageCadence?.value ?? -1)")
            print("<BLE> TỔNG QUÃNG ĐƯỜNG: \(char.totalDistance?.value ?? -1)")
            print("<BLE> KHÁNG LỰC: \(char.resistanceLevel ?? -1)")
            print("<BLE> LỰC ĐẠP TỨC THỜI: \(char.instantaneousPower?.power.value ?? -1)")
            print("<BLE> LỰC ĐẠP TRUNG BÌNH: \(char.averagePower?.power.value ?? -1)")
            print("<BLE> NHỊP TIM: \(char.heartRate?.value ?? -1)") //Not found
            print("<BLE> CALO TIÊU HAO: \(char.energy.total?.value ?? -1)")
            print("<BLE> TRAO ĐỔI CHẤT TƯƠNG ĐƯƠNG: \(char.metabolicEquivalent ?? 1)") //Not found
            print("<BLE> MACHINE TIME: \(char.time)")
            print("<BLE> ---------------------------------------------------------")
            
        case .failure(let error):
            print(error)
        }
    }
    
    func onDidUpdateValue(char: CharacteristicIndoorBikeData) {
        if char.instantaneousCadence?.value != nil {
            self.lbl_InstantaneousCadence.text = "\(Int(char.instantaneousCadence?.value ?? 0))"
            self.lbl_InstantaneousPower.text = "\(Int(char.instantaneousPower?.power.value ?? 0))"
            self.lbl_ResistanceLevel.text = "\(char.resistanceLevel ?? 0)"
            
            self.viewModel.setCharacteristic(char)
            
            self.setTotalDistance(char: char)
            self.setTotalCalories(char: char)
            
        } else {
            self.viewModel.setSpeed(char.instantaneousSpeed?.speed.value)
        }
    }
    
    func setTotalDistance(char: CharacteristicIndoorBikeData) {
        if let distance = char.totalDistance?.value {
            if distance > 0 {
                self.totalDistance = distance
                
            } else {
                if self.totalDistance > 0 {
                    self.listDistance.append(self.totalDistance)
                    self.totalDistance = 0
                    
                    //SKToast.shared.showToast(content: "listDistance.append")
                }
            }
            
            if self.listDistance.count > 0 {
                var history: Double = 0
                for item in self.listDistance {
                    history += item
                }
                let totalDistance = history + self.totalDistance
                let distanceRounded = (totalDistance / 1000).rounded(toPlaces: 1)
                //let distanceRounded = (history + self.totalDistance)
                self.lbl_TotalDistance.text = "\(distanceRounded) km"
                self.viewModel.setTotalDistance(totalDistance)
                
            } else {
                let distanceRounded = (self.totalDistance / 1000).rounded(toPlaces: 1)
                //let distanceRounded = self.totalDistance
                self.lbl_TotalDistance.text = "\(distanceRounded) km"
                self.viewModel.setTotalDistance(self.totalDistance)
            }
        }
    }
    
    func setTotalCalories(char: CharacteristicIndoorBikeData) {
        if let calo = char.energy.total?.value {
            if calo > 0 {
                self.totalCalorie = calo
                print("setTotalCalories calo > 0: ", self.totalCalorie)
                
            } else {
                if self.totalCalorie > 0 {
                    self.listCalories.append(self.totalCalorie)
                    self.totalCalorie = 0
                    
                    print("setTotalCalories totalCalorie > 0: ", self.totalCalorie)
                    
                } else {
                    print("setTotalCalories totalCalorie <= 0: ", self.totalCalorie)
                }
            }
            
            if self.listCalories.count > 0 {
                var history: Double = 0
                for item in self.listCalories {
                    history += item
                }
                let total = history + self.totalCalorie
                self.lbl_EnergyCalories.text = "\(Int(total)) kcal"
                self.viewModel.setTotalCalories(total)
                
                print("setTotalCalories listCalories.count > 0: total ", total)
                
            } else {
                self.lbl_EnergyCalories.text = "\(Int(self.totalCalorie)) kcal"
                self.viewModel.setTotalCalories(self.totalCalorie)
                
                print("setTotalCalories listCalories.count <= 0: totalCalorie ", self.totalCalorie)
            }
        }
    }
}

// MARK: Update UI
extension BikeWorkoutViewController {
    func setInitUIViewInfoDevice() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
            if BluetoothManagerBike.shared?.deviceConnected?.state != .connected {
                self.setHiddenViewInfoDevice()
            }
        }
    }
    
    func setHiddenViewInfoDevice() {
        UIView.animate(withDuration: 0.5) {
            self.view_InfoDeviceActivity.alpha = 0
            self.view_InfoDeviceBottom.alpha = 0
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            //            self.setUpdateUIViewStatusConnect()
        }
    }
    
    func setShowViewInfoDevice() {
        UIView.animate(withDuration: 0.5) {
            self.view_InfoDeviceActivity.alpha = 1
            self.view_InfoDeviceBottom.alpha = 1
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            //
        }
    }
}

// MARK: File Helper
extension BikeWorkoutViewController {
    func onSaveDataToFile(with data: Data) {
        var startTimeFile = Int(Date().timeIntervalSince1970)
        
        if let fileCurrent = self.viewModel.ojectSectionValue?.log_path.last {
            if let start_time = fileCurrent.start_time, start_time > 0 && fileCurrent.end_time == 0 {
                startTimeFile = start_time
                
                print("<Write Data> Save File 2 Update file..")
            } else {
                print("<Write Data> Save File 1 New file.")
            }
        } else {
            print("<Write Data> Save File 0 New file.")
        }
        
        let fileName = String(format: "%@.bin", startTimeFile.asString)
        guard let directory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let folderPath = directory.appendingPathComponent(Folder.FitLog)
        
        // if Directory dose not exist create one
        if !FileManager.default.fileExists(atPath: folderPath.path) {
            print("<Write Data> dir not exist.")
            try? FileManager.default.createDirectory(atPath: folderPath.path, withIntermediateDirectories: true, attributes: nil)
        }
        
        let filePath = folderPath.appendingPathComponent(fileName).path
        if FileManager.default.fileExists(atPath: filePath) {
            self.onWriteFile(filePath: filePath, data: data)
            
        } else {
            self.onCreateFile(filePath: filePath, fileName: fileName, startTimeFile: startTimeFile, data: data)
        }
    }
    
    func onWriteFile(filePath: String, data: Data) {
        let path = (filePath as NSString).expandingTildeInPath
        
        // write to file.
        if let fh = FileHandle(forWritingAtPath: path) {
            print("<Write Data> file path: ", path)
            fh.seekToEndOfFile()
            fh.write(data)
            fh.closeFile()
        }
        
        print("<Write Data> Save File 3 Write file...")
        //self.onPrintRawDataFile(filePath: filePath)
    }
    
    func onCreateFile(filePath: String, fileName: String, startTimeFile: Int, data: Data) {
        FileManager.default.createFile(atPath: filePath, contents: nil, attributes: nil)
        print("<Write Data> Save File 4 Create file.")
        
        if FileManager.default.fileExists(atPath: filePath) {
            self.onWriteFile(filePath: filePath, data: data)
        }
        
        FileHelper.shared.printContentsOfDocumentDirectory(title: "./fit/log:", path: "/fit/log/")
        self.viewModel.onCreateFileSuccess(fileName: fileName, startTimeFile: startTimeFile)
    }
    
    func onPrintRawDataFile(filePath: URL) {
        do {
            let rawData: Data = try Data(contentsOf: filePath)
            // Return the raw data as an array of bytes.
            print("<Write Data> File content: ", [UInt8](rawData))

        } catch {
            // Couldn't read the file.
            print("<Write Data> File content error: ", error.localizedDescription)
        }
    }
}
