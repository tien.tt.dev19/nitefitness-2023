//
//  
//  BikeWorkoutInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol BikeWorkoutInteractorInputProtocol {
    
}

// MARK: - Interactor Output Protocol
protocol BikeWorkoutInteractorOutputProtocol: AnyObject {
    
}

// MARK: - BikeWorkout InteractorInput
class BikeWorkoutInteractorInput {
    weak var output: BikeWorkoutInteractorOutputProtocol?
    var fitService: FitServiceProtocol?
}

// MARK: - BikeWorkout InteractorInputProtocol
extension BikeWorkoutInteractorInput: BikeWorkoutInteractorInputProtocol {
    
}
