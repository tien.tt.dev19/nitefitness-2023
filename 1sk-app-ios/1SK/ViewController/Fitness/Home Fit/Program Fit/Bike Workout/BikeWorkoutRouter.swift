//
//  
//  BikeWorkoutRouter.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol BikeWorkoutRouterProtocol {
    func popViewController()
    func presentBicycleConnectionRouter(delegate: BicycleConnectionViewDelegate?)
    func showBicycleResultRouter(exercise: ExerciseFitModel?, objectSection: IndoorBikeModel?)
}

// MARK: - BikeWorkout Router
class BikeWorkoutRouter {
    weak var viewController: BikeWorkoutViewController?
    
    static func setupModule(model: ExerciseFitModel?) -> BikeWorkoutViewController {
        let viewController = BikeWorkoutViewController()
        let router = BikeWorkoutRouter()
        let interactorInput = BikeWorkoutInteractorInput()
        let viewModel = BikeWorkoutViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.exercise = model
        interactorInput.output = viewModel
        interactorInput.fitService = FitService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - BikeWorkout RouterProtocol
extension BikeWorkoutRouter: BikeWorkoutRouterProtocol {
    func popViewController() {
        self.viewController?.isInteractivePopGestureEnable = true
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentBicycleConnectionRouter(delegate: BicycleConnectionViewDelegate?) {
        let controller = BicycleConnectionRouter.setupModule(delegate: delegate)
        self.viewController?.present(controller, animated: true)
    }
    
    func showBicycleResultRouter(exercise: ExerciseFitModel?, objectSection: IndoorBikeModel?) {
        let controller = BicycleResultRouter.setupModule(exercise: exercise, objectSection: objectSection)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
}
