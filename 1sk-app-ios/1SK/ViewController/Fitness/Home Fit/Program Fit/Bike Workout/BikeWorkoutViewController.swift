//
//  
//  BikeWorkoutViewController.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit
import AVFoundation
import RealmSwift
import CoreBluetooth

// MARK: - ViewProtocol
protocol BikeWorkoutViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onSetDataUI(exercise: ExerciseFitModel?)
    
    //AVPlayer
    func onInitPlayVideo(exercise: ExerciseFitModel?)
}

// MARK: - BikeWorkout ViewController
class BikeWorkoutViewController: BaseViewController {
    var router: BikeWorkoutRouterProtocol!
    var viewModel: BikeWorkoutViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var view_Player: UIView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_Play: UIButton!
    @IBOutlet weak var btn_Resolution: UIButton!
    @IBOutlet weak var constraint_top_NavView: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_TimePlayer: UILabel!
    @IBOutlet weak var pgr_Loading: UIProgressView!
    @IBOutlet weak var pgr_Tracking: UIProgressView!
    
    @IBOutlet weak var lbl_StatusConnect: UILabel!
    @IBOutlet weak var btn_Expand: UIButton!
    @IBOutlet weak var constraint_right_ConnectInfo: NSLayoutConstraint!
    
    @IBOutlet weak var btn_Connection: UIButton!
    @IBOutlet weak var btn_Disconnect: UIButton!
    
    @IBOutlet weak var view_InfoDeviceActivity: UIView!
    @IBOutlet weak var lbl_InstantaneousCadence: UILabel!
    @IBOutlet weak var lbl_InstantaneousPower: UILabel!
    @IBOutlet weak var lbl_ResistanceLevel: UILabel!
    
    @IBOutlet weak var view_InfoDeviceBottom: UIView!
    @IBOutlet weak var lbl_TotalDistance: UILabel!
    @IBOutlet weak var lbl_EnergyCalories: UILabel!
    
    @IBOutlet weak var view_BgPopover: UIView!
    @IBOutlet weak var view_PopoerCadence: UIView!
    @IBOutlet weak var view_PopoerPower: UIView!
    @IBOutlet weak var view_PopoerCalories: UIView!
    
    var deviceConnect: DeviceSchema?
    
    var avPlayer: AVPlayer?
    var avPlayerLayer: AVPlayerLayer?
    var avTimeObserver: Any?
    var progressValue: CGFloat = 0
    var timePlayer: TimeInterval = -1
    var timePlayerTotal: Int = 0
    var timerControl: Timer?
    var resolution: VideoResolution?
    var timeViewControll = 6.0
    
    var isNextController = false

    var totalDistance: Double = 0
    var totalCalorie: Double = 0
    var listDistance: [Double] = []
    var listCalories: [Double] = []
    
    var isShowAlertPopupCompleteWorkout = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.avPlayerLayer?.frame = self.view_Player.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard self.isNextController else {
            return
        }
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        guard self.isNextController else {
            return
        }
        self.setDeinitAVPlayer()
        self.setDeinitBluetoothManager()
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    deinit {
        gIsShowMarketingPopup = false
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        if self.btn_Play.isHidden {
            self.setShowUIControlView()
            
        } else {
            if self.avPlayer?.isPlaying == true {
                self.onPause()
            } else {
                self.onPlay()
            }
        }
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUI()
        self.setInitGesturePopoverView()
        self.setInitGestureAVPlayer()
        self.setInitUIApplication()
        self.setInitBluetoothManager()
    }
    
    func setInitUI() {
        gIsShowMarketingPopup = true
        self.isInteractivePopGestureEnable = false
        
        self.btn_Play.setImage(R.image.ic_pause_workout(), for: .normal)
        self.btn_Play.setImage(R.image.ic_play_workout(), for: .selected)
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.presentAlertPopupConfirmFinishWorkout()
    }

    @IBAction func onConnectDeviceAction(_ sender: Any) {
        self.router.presentBicycleConnectionRouter(delegate: self)
    }
    
    @IBAction func onDisconnectDeviceAction(_ sender: Any) {
        self.presentAlertPopupConfirmDisconnectDevice()
    }
    
    @IBAction func onExpandConnectViewAction(_ sender: Any) {
        self.setUpdateUIViewStatusConnect()
    }
    
    @IBAction func onPlayAction(_ sender: Any) {
        if self.avPlayer?.isPlaying == true {
            self.onPause()
            
        } else {
            self.onPlay()
        }
    }
    
    @IBAction func onResolutionAction(_ sender: Any) {
        let controller = SelectionViewController.loadFromNib()
        controller.modalPresentationStyle = .overFullScreen
        controller.currentVideoResolution = self.resolution ?? .auto
        controller.type = .videoResolution
        controller.delegate = self
        self.present(controller, animated: false, completion: nil)
    }
    
    @IBAction func onInstantaneousCadenceAction(_ sender: Any) {
        self.setUIPopoverCadence(state: 1)
    }
    
    @IBAction func onInstantaneousPowerAction(_ sender: Any) {
        self.setUIPopoverCadence(state: 2)
    }
    
    @IBAction func onResistanceLevelAction(_ sender: Any) {
        self.setUIPopoverCadence(state: 3)
    }
}

// MARK: Update UI View
extension BikeWorkoutViewController {
    func setUpdateUIViewStatusConnect() {
        if self.constraint_right_ConnectInfo.constant > -2 {
            UIView.animate(withDuration: 0.5) {
                self.constraint_right_ConnectInfo.constant = -177
                self.view.layoutIfNeeded()
            } completion: { _ in
                self.btn_Expand.setImage(R.image.ic_arrow_expand_left(), for: .normal)
            }
            
        } else {
            UIView.animate(withDuration: 0.5) {
                self.constraint_right_ConnectInfo.constant = -1
                self.view.layoutIfNeeded()
            } completion: { _ in
                self.btn_Expand.setImage(R.image.ic_arrow_collapse_right(), for: .normal)
            }
        }
    }
}

// MARK: - UIPopoverPresentationControllerDelegate
extension BikeWorkoutViewController: UIPopoverPresentationControllerDelegate {
    func setInitGesturePopoverView() {
        self.view_BgPopover.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapPopoverAction)))
    }
    
    @objc func onTapPopoverAction() {
        self.setUIPopoverCadence(state: 0)
    }
    
    func setUIPopoverCadence(state: Int) {
        switch state {
        case 1:
            self.view_BgPopover.isHidden = false
            self.view_PopoerCadence.isHidden = false
            self.view_PopoerPower.isHidden = true
            self.view_PopoerCalories.isHidden = true
            
        case 2:
            self.view_BgPopover.isHidden = false
            self.view_PopoerCadence.isHidden = true
            self.view_PopoerPower.isHidden = false
            self.view_PopoerCalories.isHidden = true
            
        case 3:
            self.view_BgPopover.isHidden = false
            self.view_PopoerCadence.isHidden = true
            self.view_PopoerPower.isHidden = true
            self.view_PopoerCalories.isHidden = false
            
        default:
            self.view_BgPopover.isHidden = true
            self.view_PopoerCadence.isHidden = true
            self.view_PopoerPower.isHidden = true
            self.view_PopoerCalories.isHidden = true
        }
    }
}

// MARK: - BikeWorkout ViewProtocol
extension BikeWorkoutViewController: BikeWorkoutViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onSetDataUI(exercise: ExerciseFitModel?) {
        self.lbl_Title.text = exercise?.name
        guard let videoTime = exercise?.videoTime else {
            return
        }
        self.timePlayerTotal = videoTime * 60
        self.timePlayer = TimeInterval(self.timePlayerTotal)
        self.setTimePlayer(currentTime: "00:00", duration: "00:00")
    }
    
    func onInitPlayVideo(exercise: ExerciseFitModel?) {
        self.setInitAVPlayer(exercise: exercise)
    }
}

// MARK: AlertPopupConfirmTitleLandscapeViewDelegate
extension BikeWorkoutViewController: AlertPopupConfirmTitleLandscapeViewDelegate {
    func presentAlertPopupConfirmDisconnectDevice() {
        let controller = AlertPopupConfirmTitleLandscapeViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.tag = 1
        
        controller.lblTitle = "Hủy kết nối"
        controller.lblContent = "Bạn có chắc muốn hủy kết nối thiết bị hiện tại"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xác nhận"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func presentAlertPopupConfirmFinishWorkout() {
        let controller = AlertPopupConfirmTitleLandscapeViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.tag = 2
        
        controller.lblTitle = "Bài tập chưa hoàn thành, bạn muốn rời đi?"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xác nhận"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirmCancelAction(object: Any?, tag: Int) {
        switch tag {
        case 1:
            break
            
        case 2:
            break
            
        default:
            break
        }
    }
    
    func onAlertPopupConfirmAgreeAction(object: Any?, tag: Int) {
        switch tag {
        case 1:
            guard let peripheral = BluetoothManagerBike.shared?.deviceConnected else {
                return
            }
            BluetoothManagerBike.shared?.setDisconnectDevice(peripheral: peripheral)
            
        case 2:
            self.isNextController = true
            
//            let endTime = Int(Date().timeIntervalSince1970)
//            self.viewModel.onSetEndTimeFileLog(endTimeFile: endTime)
//            self.viewModel.onSetEndTimeSectionLog(endTimeSection: endTime)
            
            self.router.showBicycleResultRouter(exercise: self.viewModel.exerciseValue, objectSection: self.viewModel.ojectSectionValue)
            
        default:
            break
        }
    }
}

// MARK: AlertPopupCompleteWorkoutViewDelegate
extension BikeWorkoutViewController: AlertPopupCompleteWorkoutViewDelegate {
    func presentAlertPopupCompleteWorkout() {
        guard self.isShowAlertPopupCompleteWorkout else {
            return
        }
        self.isShowAlertPopupCompleteWorkout = false
        let controller = AlertPopupCompleteWorkoutViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupCompleteWorkoutDismissAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.isNextController = true
            
//            let endTime = Int(Date().timeIntervalSince1970)
//            self.viewModel.onSetEndTimeFileLog(endTimeFile: endTime)
//            self.viewModel.onSetEndTimeSectionLog(endTimeSection: endTime)
            
            self.router.showBicycleResultRouter(exercise: self.viewModel.exerciseValue, objectSection: self.viewModel.ojectSectionValue)
        }
    }
}
