//
//  
//  BikeWorkoutViewModel.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol BikeWorkoutViewModelProtocol {
    func onViewDidLoad()
    
    // Value
    var exerciseValue: ExerciseFitModel? { get set }
    var ojectSectionValue: IndoorBikeModel? { get set }
    
    // Set
    func setCharacteristic(_ char: CharacteristicIndoorBikeData)
    func setTotalDistance(_ value: Double?)
    func setTotalCalories(_ value: Double?)
    func setSpeed(_ value: Double?)
    
    // On
    func onCreateFileSuccess(fileName: String, startTimeFile: Int)
    func onSetEndTimeFileLog(endTimeFile: Int)
//    func onSetEndTimeSectionLog(endTimeSection: Int)
    func onSetDeviceNameSectionLog(deviceName: String?)
}

// MARK: - BikeWorkout ViewModel
class BikeWorkoutViewModel {
    weak var view: BikeWorkoutViewProtocol?
    private var interactor: BikeWorkoutInteractorInputProtocol
    private let realm: RealmManagerProtocol = RealmManager()
    
    init(interactor: BikeWorkoutInteractorInputProtocol) {
        self.interactor = interactor
    }

    var exercise: ExerciseFitModel?
    var objectSection: IndoorBikeModel?
    var objectFile: FileLogPathModel?
    
    func setInitDataLog() {
        // Create New Section Log
        guard self.objectSection == nil else {
            return
        }
        self.objectSection = IndoorBikeModel()
        self.objectSection?.start_time = Int(Date().timeIntervalSince1970)
        self.objectSection?.device_type = DeviceType.bikeYesoul.rawValue
        self.realm.write(self.objectSection)
        
        let models1: [IndoorBikeModel] = self.realm.objects()
        for item in models1 {
            print("FileLogData item.id", item.id)
            print("FileLogData item.device_name", item.device_name ?? "null")
            print("FileLogData item.device_type", item.device_type ?? "null")
            print("FileLogData item.startTime", item.start_time ?? -1)
            print("FileLogData item.endTime", item.end_time ?? -1)
            print("FileLogData -----------------------")
        }
    }
}

// MARK: - BikeWorkout ViewModelProtocol
extension BikeWorkoutViewModel: BikeWorkoutViewModelProtocol {
    func onViewDidLoad() {
        self.setInitDataLog()
        self.view?.onSetDataUI(exercise: self.exercise)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.view?.onInitPlayVideo(exercise: self.exercise)
        }
    }
    
    // Value
    var exerciseValue: ExerciseFitModel? {
        get {
            return self.exercise
        }
        set {
            self.exercise = newValue
        }
    }
    
    var ojectSectionValue: IndoorBikeModel? {
        get {
            return self.objectSection
        }
        set {
            self.objectSection = newValue
        }
    }
    
    // Action
    func setCharacteristic(_ char: CharacteristicIndoorBikeData) {
        let workout = WorkoutExercise()
        
        //workout.instantaneousSpeed = char.instantaneousSpeed?.speed.value
        workout.averageSpeed = char.averageSpeed?.speed.value
        workout.instantaneousCadence = char.instantaneousCadence?.value
        workout.averageCadence = char.averageCadence?.value
        workout.totalDistance = char.totalDistance?.value
        workout.resistanceLevel = char.resistanceLevel
        workout.instantaneousPower = char.instantaneousPower?.power.value
        workout.averagePower = char.averagePower?.power.value
        workout.heartRate = char.heartRate?.value
        workout.energy = char.energy.total?.value
        workout.metabolicEquivalent = char.metabolicEquivalent
        workout.time = char.time.remaining?.value
        
        self.exercise?.workout = workout
    }
    
    func setTotalDistance(_ value: Double?) {
        self.exercise?.workout?.sumDistance = value
    }
    
    func setTotalCalories(_ value: Double?) {
        self.exercise?.workout?.sumCalories = value
    }
    
    func setSpeed(_ value: Double?) {
        self.exercise?.workout?.instantaneousSpeed = value
    }
    
    func onCreateFileSuccess(fileName: String, startTimeFile: Int) {
        if let object: IndoorBikeModel = self.realm.object({$0.id == self.objectSection?.id}) {
            self.objectFile = FileLogPathModel()
            self.objectFile?.file_name = fileName
            self.objectFile?.start_time = startTimeFile
            self.objectFile?.end_time = 0
            
            self.realm.update {
                object.log_path.append(self.objectFile!)
            }
            
            print("FileLogData id:", object.id)
            print("FileLogData device_name:", object.device_name ?? "null")
            print("FileLogData device_type:", object.device_type ?? "null")
            print("FileLogData start_time:", object.start_time ?? -1)
            print("FileLogData end_time:", object.end_time ?? -1)
            print("FileLogData -----------------------")
            
            for file in object.log_path {
                print("FileLogData                id:", file.id)
                print("FileLogData                file_name:", file.file_name ?? "null")
                print("FileLogData                end_time:", file.start_time ?? -1)
                print("FileLogData                end_time:", file.end_time ?? -1)
                print("FileLogData                -----------------------")
            }

        }
    }
    
    func onSetEndTimeFileLog(endTimeFile: Int) {
        IndoorBikeManager.shared.onSetEndTimeFile(endTimeFile: endTimeFile, objectSection: self.objectSection)
    }

//    func onSetEndTimeSectionLog(endTimeSection: Int) {
//        IndoorBikeManager.shared.onSetEndTimeSection(endTimeSection: endTimeSection, objectSection: self.objectSection)
//    }
    
    func onSetDeviceNameSectionLog(deviceName: String?) {
        IndoorBikeManager.shared.onSetDeviceNameSection(deviceName: deviceName, objectSection: self.objectSection)
    }
}

// MARK: - BikeWorkout InteractorOutputProtocol
extension BikeWorkoutViewModel: BikeWorkoutInteractorOutputProtocol {
    
}
