//
//  CellTableViewProgramFit.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//

import UIKit

class CellTableViewProgramFit: UITableViewCell {

    @IBOutlet weak var lbl_ProgramFit: UILabel!
    @IBOutlet weak var img_ProgramFit: UIImageView!
    
    var model: EquipmentModel? {
        didSet {
            self.lbl_ProgramFit.text = self.model?.name
            self.img_ProgramFit.setImage(with: self.model?.image ?? "", completion: nil)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
