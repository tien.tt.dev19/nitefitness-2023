//
//  
//  ProgramFitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 12/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ProgramFitInteractorInputProtocol {
    func getListWorkoutEquipments(perPage: Int)
}

// MARK: - Interactor Output Protocol
protocol ProgramFitInteractorOutputProtocol: AnyObject {
    func onGetListWorkoutEquipmentsFinished(with result: Result<[EquipmentModel], APIError>)
}

// MARK: - ProgramFit InteractorInput
class ProgramFitInteractorInput {
    weak var output: ProgramFitInteractorOutputProtocol?
    var fitService: FitServiceProtocol?
}

// MARK: - ProgramFit InteractorInputProtocol
extension ProgramFitInteractorInput: ProgramFitInteractorInputProtocol {
    func getListWorkoutEquipments(perPage: Int) {
        self.fitService?.getListWorkoutEquipments(perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListWorkoutEquipmentsFinished(with: result.unwrapSuccessModel())
        })
    }
}
