//
//  
//  ProgramFitRouter.swift
//  1SK
//
//  Created by Thaad on 12/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ProgramFitRouterProtocol {
    func showWorkoutProgramRouter(model: EquipmentModel)
}

// MARK: - ProgramFit Router
class ProgramFitRouter {
    weak var viewController: ProgramFitViewController?
    
    static func setupModule() -> ProgramFitViewController {
        let viewController = ProgramFitViewController()
        let router = ProgramFitRouter()
        let interactorInput = ProgramFitInteractorInput()
        let viewModel = ProgramFitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitService = FitService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ProgramFit RouterProtocol
extension ProgramFitRouter: ProgramFitRouterProtocol {
    func showWorkoutProgramRouter(model: EquipmentModel) {
        let controller = WorkoutProgramRouter.setupModule(model: model)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
