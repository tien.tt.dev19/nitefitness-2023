//
//  
//  ProgramFitViewController.swift
//  1SK
//
//  Created by Thaad on 12/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ProgramFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onReloadData()
}

// MARK: - ProgramFit ViewController
class ProgramFitViewController: BaseViewController {
    var router: ProgramFitRouterProtocol!
    var viewModel: ProgramFitViewModelProtocol!
    
    @IBOutlet weak var tbv_ProgramFit: UITableView!
    
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - ProgramFit ViewProtocol
extension ProgramFitViewController: ProgramFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_ProgramFit.reloadData()
    }
}

//MARK: - RefeshControll
extension ProgramFitViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_ProgramFit.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}


// MARK: - UITableViewDataSource
extension ProgramFitViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_ProgramFit.registerNib(ofType: CellTableViewProgramFit.self)
        self.tbv_ProgramFit.dataSource = self
        self.tbv_ProgramFit.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewProgramFit.self, for: indexPath)
        cell.model = self.viewModel.cellForRow(at: indexPath)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ProgramFitViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.viewModel.cellForRow(at: indexPath)
        self.router.showWorkoutProgramRouter(model: model)
    }
}
