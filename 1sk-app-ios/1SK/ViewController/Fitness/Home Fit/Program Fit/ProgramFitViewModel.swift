//
//  
//  ProgramFitViewModel.swift
//  1SK
//
//  Created by Thaad on 12/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ProgramFitViewModelProtocol {
    func onViewDidLoad()
    func onRefreshAction()
    
    // UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> EquipmentModel
    func didSelectRow(at indexPath: IndexPath)
}

// MARK: - ProgramFit ViewModel
class ProgramFitViewModel {
    weak var view: ProgramFitViewProtocol?
    private var interactor: ProgramFitInteractorInputProtocol
    
    init(interactor: ProgramFitInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listEquipmentModel: [EquipmentModel] = []
}

// MARK: - ProgramFit ViewModelProtocol
extension ProgramFitViewModel: ProgramFitViewModelProtocol {
    func onViewDidLoad() {
        self.view?.showHud()
        self.interactor.getListWorkoutEquipments(perPage: 100)
    }
    
    func onRefreshAction() {
        self.view?.showHud()
        self.interactor.getListWorkoutEquipments(perPage: 100)
    }
    
    // UITableView
    func numberOfRows() -> Int {
        return self.listEquipmentModel.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> EquipmentModel {
        return self.listEquipmentModel[indexPath.row]
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        //
    }
}

// MARK: - ProgramFit InteractorOutputProtocol
extension ProgramFitViewModel: ProgramFitInteractorOutputProtocol {
    func onGetListWorkoutEquipmentsFinished(with result: Result<[EquipmentModel], APIError>) {
        switch result {
        case .success(let model):
            self.listEquipmentModel = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.view?.hideHud()
    }
}
