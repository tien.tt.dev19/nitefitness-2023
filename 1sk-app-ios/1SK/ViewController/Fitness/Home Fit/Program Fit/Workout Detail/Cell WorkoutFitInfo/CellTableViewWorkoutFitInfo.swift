//
//  CellTableViewWorkoutFitInfo.swift
//  1SK
//
//  Created by Thaad on 15/04/2022.
//

import UIKit

class CellTableViewWorkoutFitInfo: UITableViewCell {

    @IBOutlet weak var ic_Icon: UIImageView!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Desc: UILabel!
    
    @IBOutlet weak var view_LineIndicator: UIView!
    
    var indexPath: IndexPath?
    var model: [InformationExercise]? {
        didSet {
            guard let info = self.model, info.count > 0 else {
                return
            }
            
            switch self.indexPath?.row {
            case 0:
                self.lbl_Time.text = "\(info[0].minute ?? 0) phút"
                self.lbl_Desc.text = info[0].title
                self.ic_Icon.image = R.image.ic_fit_workout_info_0()
                self.view_LineIndicator.isHidden = true
                
            case 1:
                self.lbl_Time.text = "\(info[1].minute ?? 0) phút"
                self.lbl_Desc.text = info[1].title
                self.ic_Icon.image = R.image.ic_fit_workout_info_1()
                self.view_LineIndicator.isHidden = true
                
            case 2:
                self.lbl_Time.text = "\(info[2].minute ?? 0) phút"
                self.lbl_Desc.text = info[2].title
                self.ic_Icon.image = R.image.ic_fit_workout_info_2()
                
                self.view_LineIndicator.isHidden = false
                
            default:
                break
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
