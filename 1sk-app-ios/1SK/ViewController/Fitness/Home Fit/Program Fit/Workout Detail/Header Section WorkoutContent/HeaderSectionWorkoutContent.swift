//
//  HeaderSectionWorkoutContent.swift
//  1SK
//
//  Created by Thaad on 15/04/2022.
//

import UIKit

class HeaderSectionWorkoutContent: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lbl_ExerciseContent: UILabel!
    
    var model: ExerciseFitModel? {
        didSet {
            guard let content = self.model?.description else { return }
            self.lbl_ExerciseContent.text = content
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
