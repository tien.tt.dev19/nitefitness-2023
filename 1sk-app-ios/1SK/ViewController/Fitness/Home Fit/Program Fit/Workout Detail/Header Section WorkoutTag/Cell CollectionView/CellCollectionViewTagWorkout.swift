//
//  CellCollectionViewTagWorkout.swift
//  1SK
//
//  Created by Thaad on 15/04/2022.
//

import UIKit

class CellCollectionViewTagWorkout: UICollectionViewCell {
    
    @IBOutlet weak var lb_TagList: UILabel!

    var model: String? {
        didSet {
            self.lb_TagList.text = self.model?.uppercased()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
