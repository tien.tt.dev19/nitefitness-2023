//
//  HeaderSectionWorkoutTag.swift
//  1SK
//
//  Created by Thaad on 15/04/2022.
//

import UIKit

protocol HeaderSectionWorkoutTagDelegate: AnyObject {
    func onReloadDataSuccess(height: CGFloat)
}

class HeaderSectionWorkoutTag: UITableViewHeaderFooterView {

    @IBOutlet weak var coll_CollectionView: UICollectionView!
    @IBOutlet weak var constraint_height_ContentView: NSLayoutConstraint!
    
    weak var delegate: HeaderSectionWorkoutTagDelegate?
    
    var listTag: [String] = []
    
    var model: ExerciseFitModel? {
        didSet {
            if let videoTime = self.model?.videoTime {
                let time = videoTime * 60
                let timePlayer = AVUtils.formatDurationTime(time: Int(time))
                self.listTag.append(timePlayer)
            }
            
            if let level = self.model?.level {
                self.listTag.append("Mức độ \(level.name ?? "Level")")
            }
            
            if let subject = self.model?.subject, subject.count > 0 {
                for item in subject {
                    self.listTag.append(item.name ?? "Subject")
                }
            }
            
            self.coll_CollectionView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.delegate?.onReloadDataSuccess(height: self.coll_CollectionView.contentSize.height)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitUICollectionView()
    }
}

// MARK: UICollectionViewDataSource
extension HeaderSectionWorkoutTag: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewTagWorkout.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listTag.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewTagWorkout.self, for: indexPath)
        cell.model = self.listTag[indexPath.row]
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension HeaderSectionWorkoutTag: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("didSelectItemAt")
    }
}
