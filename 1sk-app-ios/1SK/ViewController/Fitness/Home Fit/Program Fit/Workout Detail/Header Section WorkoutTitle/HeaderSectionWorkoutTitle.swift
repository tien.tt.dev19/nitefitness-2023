//
//  HeaderSectionWorkoutTitle.swift
//  1SK
//
//  Created by Thaad on 15/04/2022.
//

import UIKit

class HeaderSectionWorkoutTitle: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lbl_titleWorkout: UILabel!
    @IBOutlet weak var lbl_createdDate: UILabel!
    @IBOutlet weak var lbl_kcal: UILabel!
    @IBOutlet weak var img_workout_image: UIImageView!
    
    var model: ExerciseFitModel? {
        didSet {
            self.img_workout_image.setImage(with: self.model?.image ?? "", completion: nil)
            self.lbl_titleWorkout.text = self.model?.name
            self.lbl_kcal.text = self.model?.kcal?.asString
            
            if let createdAt = self.model?.createdAt {
                let dateString = Utils.shared.getTimeStringByTimeStamp(timeStamp: Double(createdAt), timeFomat: "dd/MM/YY")
                self.lbl_createdDate.text = dateString
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
