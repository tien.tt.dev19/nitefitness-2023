//
//  
//  WorkoutDetailInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit
import Kingfisher

// MARK: - Interactor Input Protocol
protocol WorkoutDetailInteractorInputProtocol: AnyObject {
    
}

// MARK: - Interactor Output Protocol
protocol WorkoutDetailInteractorOutputProtocol: AnyObject {
    
}

// MARK: - WorkoutDetail InteractorInput
class WorkoutDetailInteractorInput {
    weak var output: WorkoutDetailInteractorOutputProtocol?
    var fitService: FitServiceProtocol?
}

// MARK: - WorkoutDetail InteractorInputProtocol
extension WorkoutDetailInteractorInput: WorkoutDetailInteractorInputProtocol {
   
}
