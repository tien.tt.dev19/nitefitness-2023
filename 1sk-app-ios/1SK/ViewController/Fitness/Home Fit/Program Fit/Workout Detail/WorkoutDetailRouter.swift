//
//  
//  WorkoutDetailRouter.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol WorkoutDetailRouterProtocol {
    func popViewController()
    func showWorkoutFitRouter(exercise: ExerciseFitModel?)
    func showShareActivityViewController(with shareItem: [Any], sourceView: UIView)
}

// MARK: - WorkoutDetail Router
class WorkoutDetailRouter {
    weak var viewController: WorkoutDetailViewController?
    
    static func setupModule(with data: ExerciseFitModel) -> WorkoutDetailViewController {
        let viewController = WorkoutDetailViewController()
        let router = WorkoutDetailRouter()
        let interactorInput = WorkoutDetailInteractorInput()
        let viewModel = WorkoutDetailViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.exerciseDetail = data
        interactorInput.fitService = FitService()
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - WorkoutDetail RouterProtocol
extension WorkoutDetailRouter: WorkoutDetailRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showWorkoutFitRouter(exercise: ExerciseFitModel?) {
        let controller = BikeWorkoutRouter.setupModule(model: exercise)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showShareActivityViewController(with shareItem: [Any], sourceView: UIView) {
        let controller = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        controller.popoverPresentationController?.sourceView = sourceView
        controller.popoverPresentationController?.sourceRect = sourceView.bounds
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
