//
//  
//  WorkoutDetailViewController.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol WorkoutDetailViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onReloadData()
    
    //UITableView
    //func onReloadData()
}

// MARK: - WorkoutDetail ViewController
class WorkoutDetailViewController: BaseViewController {
    var router: WorkoutDetailRouterProtocol!
    var viewModel: WorkoutDetailViewModelProtocol!
    
    @IBOutlet weak var view_nav_Blur: UIView!
    @IBOutlet weak var view_nav_Fill: UIView!
    @IBOutlet weak var view_nav_Fill_Bg: UIView!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
//    var isUpdateHeightColl = true
//    var heightCollectionView: CGFloat = 32
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitNavigationBar()
        self.setInitUITableView()
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.popViewController()
    }
    
    @IBAction func onShareAction(_ sender: Any) {
        self.setShareAction(self.view)
    }
    
    @IBAction func onStartWorkoutAction(_ sender: Any) {
        self.router.showWorkoutFitRouter(exercise: self.viewModel.getExerciseDetail())
    }
    
    func setShareAction(_ shareButton: UIView) {
//        guard let slug = "self.blog?.friendlyUrl" else {
//            SKToast.shared.showToast(content: "Không thể chia sẻ bài viết này")
//            return
//        }
        
        let slug = "self.blog?.friendlyUrl"
        
        self.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = "self.blog?.title"
        meta.socialMetaDescText = "self.blog?.shortDescription"
        meta.socialMetaImageURL = "self.blog?.thumbImage"
        
        let universalLink = "\(SHARE_URL)\(slug)\(ShareType.blog.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                self.hideHud()

                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }

                self.router.showShareActivityViewController(with: [urlShare], sourceView: shareButton)
                print("createLinkShareFitnessPost The short URL is: \(urlShare)")
            })

        } else {
            self.hideHud()
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }
}

// MARK: - WorkoutDetail ViewProtocol
extension WorkoutDetailViewController: WorkoutDetailViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
}

// MARK: UITableViewDataSource
extension WorkoutDetailViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionWorkoutTitle.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionWorkoutTag.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionWorkoutContent.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionWorkoutInfo.self)
        
        self.tbv_TableView.registerNib(ofType: CellTableViewWorkoutFitInfo.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionWorkoutTitle.self)
            header.model = self.viewModel.getExerciseDetail()
            return header
            
        case 1:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionWorkoutTag.self)
            header.delegate = self
            header.model = self.viewModel.getExerciseDetail()
            return header
            
        case 2:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionWorkoutContent.self)
            header.model = self.viewModel.getExerciseDetail()
            return header
            
        case 3:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionWorkoutInfo.self)
            return header
            
        default:
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 3:
            return self.viewModel.numberOfRows(in: section)
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 3:
            let cell = tableView.dequeueCell(ofType: CellTableViewWorkoutFitInfo.self, for: indexPath)
            cell.indexPath = indexPath
            cell.model = self.viewModel.getExerciseDetail()?.information
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: UITableViewDelegate
extension WorkoutDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        self.setStateNavigationBar(contentOffset: contentOffset)
    }
}

// MARK: - Navigation Bar
extension WorkoutDetailViewController {
    func setInitNavigationBar() {
        self.view_nav_Fill.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.view_nav_Fill.isHidden = true
        self.view_nav_Fill_Bg.isHidden = true
    }
    
    func setStateNavigationBar(contentOffset: CGFloat) {
        if contentOffset <= 0.0 {
            if self.view_nav_Fill.isHidden == false {
                UIView.animate(withDuration: 0.5) {
                    self.view_nav_Fill.alpha = 0
                    self.view_nav_Fill_Bg.alpha = 0
                    self.view.layoutIfNeeded()
                    
                } completion: { _ in
                    self.view_nav_Fill.isHidden = true
                    self.view_nav_Fill_Bg.isHidden = true
                }
            }
        }
        
        if contentOffset > 44.0 {
            if self.view_nav_Fill.isHidden == true {
                
                self.view_nav_Fill.isHidden = false
                self.view_nav_Fill.alpha = 0
                
                self.view_nav_Fill_Bg.isHidden = false
                self.view_nav_Fill_Bg.alpha = 0
                
                UIView.animate(withDuration: 0.5) {
                    self.view_nav_Fill.alpha = 1
                    self.view_nav_Fill_Bg.alpha = 1
                    self.view.layoutIfNeeded()
                    
                } completion: { _ in
                    //
                }
            }
        }
    }
}

// MARK: HeaderSectionWorkoutTagDelegate
extension WorkoutDetailViewController: HeaderSectionWorkoutTagDelegate {
    func onReloadDataSuccess(height: CGFloat) {
//        if self.isUpdateHeightColl {
//            self.isUpdateHeightColl = false
//            self.heightCollectionView = height
//
//            print("onReloadDataSuccess height:", height)
//
//            let indexSet = IndexSet(integer: 1)
//            self.tbv_TableView.reloadSections(indexSet, with: .automatic)
//        }
    }
}
