//
//  
//  WorkoutDetailViewModel.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol WorkoutDetailViewModelProtocol {
    func onViewDidLoad()
    
    // UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow(at indexPath: IndexPath) -> Any?
    func didSelectRow(at indexPath: IndexPath)
    
    // Value
    func getExerciseDetail() -> ExerciseFitModel?
}

// MARK: - WorkoutDetail ViewModel
class WorkoutDetailViewModel {
    weak var view: WorkoutDetailViewProtocol?
    private var interactor: WorkoutDetailInteractorInputProtocol
    
    init(interactor: WorkoutDetailInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var exerciseDetail: ExerciseFitModel?
}

// MARK: - WorkoutDetail ViewModelProtocol
extension WorkoutDetailViewModel: WorkoutDetailViewModelProtocol {
    func onViewDidLoad() {
        self.view?.onReloadData()
    }
    
    func numberOfRows(in section: Int) -> Int {
        switch section {
        case 3:
            return 3
            
        default:
            return 0
        }
    }
    
    func cellForRow(at indexPath: IndexPath) -> Any? {
        return nil
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        
    }
    
    func getExerciseDetail() -> ExerciseFitModel? {
        return self.exerciseDetail
    }
}

// MARK: - WorkoutDetail InteractorOutputProtocol
extension WorkoutDetailViewModel: WorkoutDetailInteractorOutputProtocol {
   
}
