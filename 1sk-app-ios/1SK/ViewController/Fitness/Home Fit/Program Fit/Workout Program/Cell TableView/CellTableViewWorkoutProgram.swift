//
//  CellTableViewWorkoutProgram.swift
//  1SK
//
//  Created by Valerian on 13/04/2022.
//

import UIKit

class CellTableViewWorkoutProgram: UITableViewCell {
    
    @IBOutlet weak var view_Parent: UIStackView!
    @IBOutlet weak var view_Subjects: UIStackView!
    @IBOutlet weak var lbl_Subjects: UILabel!
    @IBOutlet weak var lbl_TimeVideo: UILabel!
    @IBOutlet weak var lbl_Level: UILabel!
    @IBOutlet weak var lbl_WorkoutTitle: UILabel!
    @IBOutlet weak var img_WorkoutImage: UIImageView!

    var model: ExerciseFitModel? {
        didSet {
            self.lbl_WorkoutTitle.text = self.model?.name
            self.lbl_TimeVideo.text = "\(String(describing: self.model?.videoTime ?? 0)) Phút"
            self.lbl_Level.text = "Mức độ \(String(describing: self.model?.level?.name ?? ""))"
            self.img_WorkoutImage.setImage(with: self.model?.image ?? "", completion: nil)
            
            self.lbl_Subjects.text = self.getAllSubjects(self.model)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func getAllSubjects(_ data: ExerciseFitModel?) -> String {
        guard let list = data?.subject else {
            return ""
        }
        var temp: [String] = []
        for item in list {
            if let name = item.name {
                temp.append(name)
            }
        }
        return temp.joined(separator: ", ")
    }
}
