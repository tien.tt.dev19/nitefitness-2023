//
//  
//  WorkoutProgramInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol WorkoutProgramInteractorInputProtocol {
    func getListExercise(with equipmentId: Int, perPage: Int)
    func getExerciseDetail(with id: Int)
}

// MARK: - Interactor Output Protocol
protocol WorkoutProgramInteractorOutputProtocol: AnyObject {
    func onGetListExerciseFinished(with result: Result<[ExerciseFitModel], APIError>)
    func onGetExerciseDetailFinished(with result: Result<ExerciseFitModel, APIError>)
}

// MARK: - WorkoutProgram InteractorInput
class WorkoutProgramInteractorInput {
    weak var output: WorkoutProgramInteractorOutputProtocol?
    var fitService: FitServiceProtocol?
}

// MARK: - WorkoutProgram InteractorInputProtocol
extension WorkoutProgramInteractorInput: WorkoutProgramInteractorInputProtocol {
    func getExerciseDetail(with id: Int) {
        self.fitService?.getExerciseDetail(with: id, completion: { [weak self] result in
            self?.output?.onGetExerciseDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListExercise(with equipmentId: Int, perPage: Int) {
        self.fitService?.getListExercises(perPage: perPage, equipmentId: equipmentId, completion: { [weak self] result in
            self?.output?.onGetListExerciseFinished(with: result.unwrapSuccessModel())
        })
    }
}
