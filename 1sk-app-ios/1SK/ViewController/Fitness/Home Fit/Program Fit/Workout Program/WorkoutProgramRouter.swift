//
//  
//  WorkoutProgramRouter.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol WorkoutProgramRouterProtocol {
    func showWorkoutDetailRouter(with model: ExerciseFitModel)
}

// MARK: - WorkoutProgram Router
class WorkoutProgramRouter {
    weak var viewController: WorkoutProgramViewController?
    
    static func setupModule(model: EquipmentModel) -> WorkoutProgramViewController {
        let viewController = WorkoutProgramViewController()
        let router = WorkoutProgramRouter()
        let interactorInput = WorkoutProgramInteractorInput()
        let viewModel = WorkoutProgramViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        viewModel.data = model
        interactorInput.fitService = FitService()
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - WorkoutProgram RouterProtocol
extension WorkoutProgramRouter: WorkoutProgramRouterProtocol {
    func showWorkoutDetailRouter(with model: ExerciseFitModel) {
        let controller = WorkoutDetailRouter.setupModule(with: model)
        self.viewController?.show(controller, sender: nil)
    }
}
