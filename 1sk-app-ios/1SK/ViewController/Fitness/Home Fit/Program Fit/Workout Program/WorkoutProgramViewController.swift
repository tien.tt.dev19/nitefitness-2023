//
//  
//  WorkoutProgramViewController.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol WorkoutProgramViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onReloadData()
    func goToWorkoutDetail(with data: ExerciseFitModel)
    
    //UITableView
}

// MARK: - WorkoutProgram ViewController
class WorkoutProgramViewController: BaseViewController {
    var router: WorkoutProgramRouterProtocol!
    var viewModel: WorkoutProgramViewModelProtocol!
    private var refreshControl: UIRefreshControl?

    @IBOutlet weak var tbv_WorkOutProgram: UITableView!

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - WorkoutProgram ViewProtocol
extension WorkoutProgramViewController: WorkoutProgramViewProtocol {

    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.title = self.viewModel.onGetTitle()
        self.tbv_WorkOutProgram.reloadData()
    }
    
    func goToWorkoutDetail(with data: ExerciseFitModel) {
        self.router.showWorkoutDetailRouter(with: data)
    }
}

//MARK: - RefeshControll
extension WorkoutProgramViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_WorkOutProgram.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension WorkoutProgramViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_WorkOutProgram.registerNib(ofType: CellTableViewWorkoutProgram.self)
        self.tbv_WorkOutProgram.dataSource = self
        self.tbv_WorkOutProgram.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewWorkoutProgram.self, for: indexPath)
        cell.model = self.viewModel.cellForRow(at: indexPath)
        if self.viewModel.cellForRow(at: indexPath).subject?.count == 0 {
            cell.view_Subjects.isHidden = true
            cell.view_Parent.removeArrangedSubview(cell.view_Subjects)
        } else {
            cell.view_Subjects.isHidden = false
            cell.view_Parent.insertArrangedSubview(cell.view_Subjects, at: 0)
        }
        return cell
    }
}

// MARK: - UITableViewDelegate
extension WorkoutProgramViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let idExercise = self.viewModel.cellForRow(at: indexPath).id ?? -1
        self.viewModel.onGetWorkOutDetail(with: idExercise)
    }
}
