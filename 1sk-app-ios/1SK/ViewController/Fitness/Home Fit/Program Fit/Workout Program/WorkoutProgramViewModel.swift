//
//  
//  WorkoutProgramViewModel.swift
//  1SK
//
//  Created by Thaad on 13/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol WorkoutProgramViewModelProtocol {
    func onViewDidLoad()
    func onRefreshAction()
    func onGetWorkOutDetail(with id: Int)
    func onGetTitle() -> String
    
    // UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> ExerciseFitModel
    func didSelectRow(at indexPath: IndexPath)
}

// MARK: - WorkoutProgram ViewModel
class WorkoutProgramViewModel {
    weak var view: WorkoutProgramViewProtocol?
    private var interactor: WorkoutProgramInteractorInputProtocol
    
    init(interactor: WorkoutProgramInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var data: EquipmentModel?
    var listExercises: [ExerciseFitModel] = []
}

// MARK: - WorkoutProgram ViewModelProtocol
extension WorkoutProgramViewModel: WorkoutProgramViewModelProtocol {

    func onViewDidLoad() {
        self.view?.showHud()
        self.interactor.getListExercise(with: self.data?.id ?? -1, perPage: 100)
    }
    
    func onRefreshAction() {
        self.view?.showHud()
        self.interactor.getListExercise(with: self.data?.id ?? -1, perPage: 100)
    }
    
    func onGetWorkOutDetail(with id: Int) {
        self.view?.showHud()
        self.interactor.getExerciseDetail(with: id)
    }
    
    func numberOfRows() -> Int {
        return self.listExercises.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> ExerciseFitModel {
        return self.listExercises[indexPath.row]
    }
    
    func onGetTitle() -> String {
        return self.data?.name ?? ""
    }
    
    func didSelectRow(at indexPath: IndexPath) { }
}

// MARK: - WorkoutProgram InteractorOutputProtocol
extension WorkoutProgramViewModel: WorkoutProgramInteractorOutputProtocol {
    func onGetExerciseDetailFinished(with result: Result<ExerciseFitModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.goToWorkoutDetail(with: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetListExerciseFinished(with result: Result<[ExerciseFitModel], APIError>) {
        switch result {
        case .success(let model):
            self.listExercises = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
