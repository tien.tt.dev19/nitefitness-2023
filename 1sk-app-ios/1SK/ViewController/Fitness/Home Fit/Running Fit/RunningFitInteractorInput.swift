//
//  
//  RunningFitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol RunningFitInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol RunningFitInteractorOutputProtocol: AnyObject {
    
}

// MARK: - RunningFit InteractorInput
class RunningFitInteractorInput {
    weak var output: RunningFitInteractorOutputProtocol?
}

// MARK: - RunningFit InteractorInputProtocol
extension RunningFitInteractorInput: RunningFitInteractorInputProtocol {

}
