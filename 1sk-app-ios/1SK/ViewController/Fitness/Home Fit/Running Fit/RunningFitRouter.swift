//
//  
//  RunningFitRouter.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol RunningFitRouterProtocol {

}

// MARK: - RunningFit Router
class RunningFitRouter {
    weak var viewController: RunningFitViewController?
    
    static func setupModule() -> RunningFitViewController {
        let viewController = RunningFitViewController()
        let router = RunningFitRouter()
        let interactorInput = RunningFitInteractorInput()
        let viewModel = RunningFitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - RunningFit RouterProtocol
extension RunningFitRouter: RunningFitRouterProtocol {
    
}
