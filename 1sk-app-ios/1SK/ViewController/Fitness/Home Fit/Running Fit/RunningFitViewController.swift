//
//  
//  RunningFitViewController.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol RunningFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - RunningFit ViewController
class RunningFitViewController: BaseViewController {
    var router: RunningFitRouterProtocol!
    var viewModel: RunningFitViewModelProtocol!
    
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Chạy bộ"
    }
    
    // MARK: - Action
    
}

// MARK: - RunningFit ViewProtocol
extension RunningFitViewController: RunningFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}
