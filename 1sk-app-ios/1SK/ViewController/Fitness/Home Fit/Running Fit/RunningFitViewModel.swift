//
//  
//  RunningFitViewModel.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol RunningFitViewModelProtocol {
    func onViewDidLoad()
    //func onViewDidAppear()
    
    // UITableView
    //func numberOfSections() -> Int
    //func numberOfRows() -> Int
    //func cellForRow(at indexPath: IndexPath) -> Any?
    //func didSelectRow(at indexPath: IndexPath)
    
    // UICollectionView
    //func numberOfItems() -> Int
    //func cellForItem(at indexPath: IndexPath) -> Any
    //func didSelectItem(at indexPath: IndexPath)
}

// MARK: - RunningFit ViewModel
class RunningFitViewModel {
    weak var view: RunningFitViewProtocol?
    private var interactor: RunningFitInteractorInputProtocol

    init(interactor: RunningFitInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - RunningFit ViewModelProtocol
extension RunningFitViewModel: RunningFitViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - RunningFit InteractorOutputProtocol
extension RunningFitViewModel: RunningFitInteractorOutputProtocol {

}
