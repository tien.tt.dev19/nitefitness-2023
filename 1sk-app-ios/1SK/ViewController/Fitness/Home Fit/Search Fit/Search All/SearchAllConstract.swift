//
//  
//  SearchAllConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

// MARK: - View
protocol SearchAllViewProtocol: AnyObject {
    func onReloadData()
}

// MARK: - Presenter
protocol SearchAllPresenterProtocol {
    func onViewDidLoad()
    func onSearchAction(keywords: String)
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow_Workout(at indexPath: IndexPath) -> WorkoutModel?
    func cellForRow_Video(at indexPath: IndexPath) -> VideoModel?
    func cellForRow_Blog(at indexPath: IndexPath) -> BlogModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onSeeMoreFooterAction(section: Int?)
    func onRefreshAction()
    
    /// Value
    func getTotalWorkout() -> Int
    func getTotalVideo() -> Int
    func getTotalBlog() -> Int
    func getKeywords() -> String
}

// MARK: - Interactor Input
protocol SearchAllInteractorInputProtocol {
    func getListWorkoutSearch(keywords: String, page: Int, perPage: Int)
    func getListVideoSearch(keywords: String, page: Int, perPage: Int)
    func getListBlogSearch(keywords: String, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol SearchAllInteractorOutputProtocol: AnyObject {
    func onGetListWorkoutSearchFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int)
    func onGetListVideoSearchFinished(with result: Result<[VideoModel], APIError>, page: Int, total: Int)
    func onGetListBlogSearchFinished(with result: Result<[BlogModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol SearchAllRouterProtocol: BaseRouterProtocol {
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?)
    func showVideoDetailsViewController(with video: VideoModel)
    func gotoBlogDetailsViewController(blogModel: BlogModel)
}
