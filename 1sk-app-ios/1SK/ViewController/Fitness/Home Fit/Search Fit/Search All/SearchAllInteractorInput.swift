//
//  
//  SearchAllInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchAllInteractorInput {
    weak var output: SearchAllInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
    var fitnessService: FitnessServiceProtocol?
    var blogService: BlogServiceProtocol?
}

// MARK: - SearchAllInteractorInputProtocol
extension SearchAllInteractorInput: SearchAllInteractorInputProtocol {
    func getListWorkoutSearch(keywords: String, page: Int, perPage: Int) {
        self.exerciseService?.getListWorkoutSearch(keywords: keywords, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListWorkoutSearchFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
    func getListVideoSearch(keywords: String, page: Int, perPage: Int) {
        self.fitnessService?.getListVideosSearch(categoryId: CategoryType.videoFit.rawValue, keywords: keywords, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListVideoSearchFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
    func getListBlogSearch(keywords: String, page: Int, perPage: Int) {
        self.blogService?.getListBlogSearch(categoryId: CategoryType.blogFit.rawValue, keywords: keywords, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListBlogSearchFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
