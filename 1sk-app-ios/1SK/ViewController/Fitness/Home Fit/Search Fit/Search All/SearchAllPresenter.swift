//
//  
//  SearchAllPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

protocol SearchAllPresenterDelegate: AnyObject {
    func onSeeMoreFooterAction(section: Int?)
}

class SearchAllPresenter {

    weak var view: SearchAllViewProtocol?
    private var interactor: SearchAllInteractorInputProtocol
    private var router: SearchAllRouterProtocol
    weak var delegate: SearchAllPresenterDelegate?
    
    init(interactor: SearchAllInteractorInputProtocol,
         router: SearchAllRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var keywords: String = ""
    
    var listWorkout: [WorkoutModel] = []
    var listVideo: [VideoModel] = []
    var listBlog: [BlogModel] = []
    
    var totalWorkout: Int?
    var totalVideo: Int?
    var totalBlog: Int?
    
    let dispatchGroup = DispatchGroup()
    
    func getDataInit() {
        if self.keywords.count > 0 {
            self.router.showHud()
            
//            self.dispatchGroup.enter()
//            self.interactor.getListWorkoutSearch(keywords: self.keywords, page: 1, perPage: 10)

            self.dispatchGroup.enter()
            self.interactor.getListVideoSearch(keywords: self.keywords, page: 1, perPage: 10)

            self.dispatchGroup.enter()
            self.interactor.getListBlogSearch(keywords: self.keywords, page: 1, perPage: 10)
            
            // 1
            self.dispatchGroup.notify(queue: .main, execute: { [weak self] in
                self?.router.hideHud()
                self?.view?.onReloadData()
            })
            
        } else {
            self.listWorkout.removeAll()
            self.listVideo.removeAll()
            self.listBlog.removeAll()
            self.view?.onReloadData()
        }
    }
    
    
}

// MARK: - SearchAllPresenterProtocol
extension SearchAllPresenter: SearchAllPresenterProtocol {
    func onViewDidLoad() {
        self.getDataInit()
    }
    
    func onSearchAction(keywords: String) {
        self.keywords = keywords
        self.getDataInit()
    }
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int {
        switch section {
        case 0:
            return self.listWorkout.count
            
        case 1:
            return self.listVideo.count
            
        case 2:
            return self.listBlog.count
            
        default:
            return 0
        }
    }
    
    func cellForRow_Workout(at indexPath: IndexPath) -> WorkoutModel? {
        return self.listWorkout[indexPath.row]
    }
    
    func cellForRow_Video(at indexPath: IndexPath) -> VideoModel? {
        return self.listVideo[indexPath.row]
    }
    
    func cellForRow_Blog(at indexPath: IndexPath) -> BlogModel? {
        return self.listBlog[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            let workout = self.listWorkout[indexPath.row]
            self.router.gotoDetailWorkoutViewController(with: workout, delegate: nil)
            
        case 1:
            let video = self.listVideo[indexPath.row]
            self.router.showVideoDetailsViewController(with: video)
            
        case 2:
            let blog = self.listBlog[indexPath.row]
            self.router.gotoBlogDetailsViewController(blogModel: blog)
            
        default:
            break
        }
    }
    
    func onSeeMoreFooterAction(section: Int?) {
        self.delegate?.onSeeMoreFooterAction(section: section)
    }
    
    func onRefreshAction() {
        self.getDataInit()
    }
    
    /// Geter
    func getTotalWorkout() -> Int {
        return self.totalWorkout ?? 0
    }
    
    func getTotalVideo() -> Int {
        return self.totalVideo ?? 0
    }
    
    func getTotalBlog() -> Int {
        return self.totalBlog ?? 0
    }
    
    func getKeywords() -> String {
        return self.keywords
    }
}

// MARK: - SearchAllInteractorOutput 
extension SearchAllPresenter: SearchAllInteractorOutputProtocol {
    func onGetListWorkoutSearchFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            self.listWorkout = model
            self.totalWorkout = total
            
        case .failure:
            break
        }
        self.dispatchGroup.leave()
    }
    
    func onGetListVideoSearchFinished(with result: Result<[VideoModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            self.listVideo = model
            self.totalVideo = total
            
        case .failure:
            break
        }
        self.dispatchGroup.leave()
    }
    
    func onGetListBlogSearchFinished(with result: Result<[BlogModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            self.listBlog = model
            self.totalBlog = total
            
        case .failure:
            break
        }
        self.dispatchGroup.leave()
    }
}
