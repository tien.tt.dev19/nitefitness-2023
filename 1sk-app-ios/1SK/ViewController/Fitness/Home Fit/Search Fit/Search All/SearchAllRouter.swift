//
//  
//  SearchAllRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchAllRouter: BaseRouter {
    static func setupModule(delegate: SearchAllPresenterDelegate?) -> SearchAllViewController {
        let viewController = SearchAllViewController()
        let router = SearchAllRouter()
        let interactorInput = SearchAllInteractorInput()
        let presenter = SearchAllPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        interactorInput.fitnessService = FitnessService()
        interactorInput.blogService = BlogService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SearchAllRouterProtocol
extension SearchAllRouter: SearchAllRouterProtocol {
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?) {
        let workoutViewController = MyWorkoutRouter.setupModule(with: workout, delegate: delegate)
        self.viewController?.navigationController?.pushViewController(workoutViewController, animated: true)
    }
    
    func showVideoDetailsViewController(with video: VideoModel) {
        let controller = PlayVideoRouter.setupModule(with: video, categoryType: .videoFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoBlogDetailsViewController(blogModel: BlogModel) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: .blogFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
