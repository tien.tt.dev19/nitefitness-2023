//
//  
//  SearchAllViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchAllViewController: BaseViewController {

    var presenter: SearchAllPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var stack_NoData_Workout: UIStackView!
    @IBOutlet weak var lbl_NoData_Desc: UILabel!
    
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitTableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - SearchAllViewProtocol
extension SearchAllViewController: SearchAllViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
        
        let countWorkout = self.presenter.numberOfRows(in: 0)
        let countVideo = self.presenter.numberOfRows(in: 1)
        let countBlog = self.presenter.numberOfRows(in: 2)
        
        if countWorkout == 0 && countVideo == 0 && countBlog == 0 {
            let keywords = self.presenter.getKeywords()
            if keywords.count > 0 {
                self.lbl_NoData_Desc.text = "Không có kết quả phù hợp\nvới từ khóa tìm kiếm"
            } else {
                self.lbl_NoData_Desc.text = "Mời bạn nhập nội dung tìm kiếm"
            }
            self.stack_NoData_Workout.isHidden = false
        } else {
            self.stack_NoData_Workout.isHidden = true
        }
    }
}

// MARK: Refresh Control
extension SearchAllViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension SearchAllViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewWorkout.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewDiscoveryVideo.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewBlog.self)
        
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderSearch.self)
        self.tbv_TableView.registerFooterNib(ofType: SectionFooterSearchAll.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    /// Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let count = self.presenter.numberOfRows(in: section)
        if count == 0 {
            return 0
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: SectionHeaderSearch.self)

        switch section {
        case 0:
            let total = self.presenter.getTotalWorkout()
            header.setCount(title: "Bài tập", value: total)

        case 1:
            let total = self.presenter.getTotalVideo()
            header.setCount(title: "Video", value: total)

        case 2:
            let total = self.presenter.getTotalBlog()
            header.setCount(title: "Bài viết", value: total)

        default:
            break
        }
        
        return header
    }
    
    /// Footer
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let count = self.presenter.numberOfRows(in: section)
        if count == 0 {
            return 0
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueHeaderView(ofType: SectionFooterSearchAll.self)
        footer.delegate = self
        footer.section = section
        return footer
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueCell(ofType: CellTableViewWorkout.self, for: indexPath)
            if indexPath.row == self.presenter.numberOfRows(in: 0) - 1 {
                cell.isHiddenLine = true
            } else {
                cell.isHiddenLine = false
            }
            cell.config(with: self.presenter.cellForRow_Workout(at: indexPath))
            return cell
            
        case 1:
            let cell = tableView.dequeueCell(ofType: CellTableViewDiscoveryVideo.self, for: indexPath)
            if indexPath.row == self.presenter.numberOfRows(in: 0) - 1 {
                cell.isHiddenLine = true
            } else {
                cell.isHiddenLine = false
            }
            cell.config(with: self.presenter.cellForRow_Video(at: indexPath))
            return cell
            
        case 2:
            let cell = tableView.dequeueCell(ofType: CellTableViewBlog.self, for: indexPath)
            if indexPath.row == self.presenter.numberOfRows(in: 0) - 1 {
                cell.isHiddenLine = true
            } else {
                cell.isHiddenLine = false
            }
            cell.config(with: self.presenter.cellForRow_Blog(at: indexPath))
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: - UITableViewDelegate
extension SearchAllViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRow(at: indexPath)
    }
}

// MARK: - SectionFooterSearchAllDelegate
extension SearchAllViewController: SectionFooterSearchAllDelegate {
    func onSeeMoreFooterAction(section: Int?) {
        self.presenter.onSeeMoreFooterAction(section: section)
    }
}
