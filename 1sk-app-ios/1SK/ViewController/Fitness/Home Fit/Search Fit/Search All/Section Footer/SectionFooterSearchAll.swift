//
//  SectionFooterSearchAll.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//

import UIKit

protocol SectionFooterSearchAllDelegate: AnyObject {
    func onSeeMoreFooterAction(section: Int?)
}

class SectionFooterSearchAll: UITableViewHeaderFooterView {

    weak var delegate: SectionFooterSearchAllDelegate?
    var section: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func onSeeMoreFooterAction(_ sender: Any) {
        self.delegate?.onSeeMoreFooterAction(section: self.section)
    }

}
