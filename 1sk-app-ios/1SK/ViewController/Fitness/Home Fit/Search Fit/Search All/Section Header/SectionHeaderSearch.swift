//
//  SectionHeaderSearch.swift
//  1SK
//
//  Created by vuongbachthu on 12/13/21.
//

import UIKit

class SectionHeaderSearch: UITableViewHeaderFooterView {
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Count: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setCount(title: String, value: Int) {
        self.lbl_Title.text = title
        self.lbl_Count.text = "(\(value))"
    }

}
