//
//  
//  SearchBlogConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

// MARK: - View
protocol SearchBlogViewProtocol: AnyObject {
    func onReloadData()
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol SearchBlogPresenterProtocol {
    func onViewDidLoad()
    func onSearchAction(keywords: String)
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow_Blog(at indexPath: IndexPath) -> BlogModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onLoadMoreAction()
    func onRefreshAction()
    
    /// Value
    func getTotalBlog() -> Int
    func getKeywords() -> String
}

// MARK: - Interactor Input
protocol SearchBlogInteractorInputProtocol {
    func getListBlogSearch(keywords: String, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol SearchBlogInteractorOutputProtocol: AnyObject {
    func onGetListBlogSearchFinished(with result: Result<[BlogModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol SearchBlogRouterProtocol: BaseRouterProtocol {
    func gotoBlogDetailsViewController(blogModel: BlogModel)
}
