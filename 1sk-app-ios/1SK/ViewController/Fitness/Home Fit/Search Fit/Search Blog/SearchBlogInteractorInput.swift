//
//  
//  SearchBlogInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchBlogInteractorInput {
    weak var output: SearchBlogInteractorOutputProtocol?
    var blogService: BlogServiceProtocol?
}

// MARK: - SearchBlogInteractorInputProtocol
extension SearchBlogInteractorInput: SearchBlogInteractorInputProtocol {
    func getListBlogSearch(keywords: String, page: Int, perPage: Int) {
        self.blogService?.getListBlogSearch(categoryId: CategoryType.blogFit.rawValue, keywords: keywords, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListBlogSearchFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
