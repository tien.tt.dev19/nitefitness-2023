//
//  
//  SearchBlogPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchBlogPresenter {

    weak var view: SearchBlogViewProtocol?
    private var interactor: SearchBlogInteractorInputProtocol
    private var router: SearchBlogRouterProtocol

    init(interactor: SearchBlogInteractorInputProtocol,
         router: SearchBlogRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var keywords: String = ""
    var listBlog: [BlogModel] = []
    
    var totalBlog: Int?
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        if self.keywords.count > 0 {
            self.page = page
            if page == 1 {
                self.page = 1
                self.isOutOfData = false
            }
            self.router.showHud()
            self.interactor.getListBlogSearch(keywords: self.keywords, page: page, perPage: self.limit)
            
        } else {
            self.listBlog.removeAll()
            self.view?.onReloadData()
        }
    }
}

// MARK: - SearchBlogPresenterProtocol
extension SearchBlogPresenter: SearchBlogPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    func onSearchAction(keywords: String) {
        self.keywords = keywords
        self.getData(in: 1)
    }
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.listBlog.count
    }
    
    func cellForRow_Blog(at indexPath: IndexPath) -> BlogModel? {
        return self.listBlog[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        let blog = self.listBlog[indexPath.row]
        self.router.gotoBlogDetailsViewController(blogModel: blog)
    }
    
    func onLoadMoreAction() {
        if self.listBlog.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func onRefreshAction() {
        self.getData(in: 1)
    }
    
    /// Value
    func getTotalBlog() -> Int {
        return self.total
    }
    
    func getKeywords() -> String {
        return self.keywords
    }
}

// MARK: - SearchBlogInteractorOutput 
extension SearchBlogPresenter: SearchBlogInteractorOutputProtocol {
    func onGetListBlogSearchFinished(with result: Result<[BlogModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.listBlog = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.listBlog.append(object)
                    self.view?.onInsertRow(at: self.listBlog.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listBlog.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            break
        }
        self.router.hideHud()
    }
}
