//
//  
//  SearchBlogRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchBlogRouter: BaseRouter {
    static func setupModule() -> SearchBlogViewController {
        let viewController = SearchBlogViewController()
        let router = SearchBlogRouter()
        let interactorInput = SearchBlogInteractorInput()
        let presenter = SearchBlogPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.blogService = BlogService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SearchBlogRouterProtocol
extension SearchBlogRouter: SearchBlogRouterProtocol {
    func gotoBlogDetailsViewController(blogModel: BlogModel) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: .blogFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
