//
//  
//  SearchBlogViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchBlogViewController: BaseViewController {

    var presenter: SearchBlogPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var stack_NoData_Workout: UIStackView!
    @IBOutlet weak var lbl_NoData_Desc: UILabel!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitTableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - SearchBlogViewProtocol
extension SearchBlogViewController: SearchBlogViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
        
        let count = self.presenter.numberOfRows(in: 0)
        if count == 0 {
            let keywords = self.presenter.getKeywords()
            if keywords.count > 0 {
                self.lbl_NoData_Desc.text = "Không có kết quả phù hợp\nvới từ khóa tìm kiếm"
            } else {
                self.lbl_NoData_Desc.text = "Mời bạn nhập nội dung tìm kiếm"
            }
            self.stack_NoData_Workout.isHidden = false
        } else {
            self.stack_NoData_Workout.isHidden = true
        }
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
}

// MARK: Refresh Control
extension SearchBlogViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension SearchBlogViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewBlog.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderSearch.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let count = self.presenter.numberOfRows(in: section)
        if count == 0 {
            return 0
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: SectionHeaderSearch.self)
        let count = self.presenter.getTotalBlog()
        header.setCount(title: "Bài viết", value: count)
        
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewBlog.self, for: indexPath)
        
        cell.config(with: self.presenter.cellForRow_Blog(at: indexPath))
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SearchBlogViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRow(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.subviews.first as? CellTableViewBlog) != nil {
            let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            
            if isReachingEnd && self.isLoading == false {
                self.isLoading = true
                self.presenter.onLoadMoreAction()
            }
        }
    }
}
