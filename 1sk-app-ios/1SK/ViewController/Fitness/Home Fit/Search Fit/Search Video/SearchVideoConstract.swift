//
//  
//  SearchVideoConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

// MARK: - View
protocol SearchVideoViewProtocol: AnyObject {
    func onReloadData()
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol SearchVideoPresenterProtocol {
    func onViewDidLoad()
    func onSearchAction(keywords: String)
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow_Video(at indexPath: IndexPath) -> VideoModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onLoadMoreAction()
    func onRefreshAction()
    
    /// Value
    func getTotalVideo() -> Int
    func getKeywords() -> String
}

// MARK: - Interactor Input
protocol SearchVideoInteractorInputProtocol {
    func getListVideoSearch(keywords: String, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol SearchVideoInteractorOutputProtocol: AnyObject {
    func onGetListVideoSearchFinished(with result: Result<[VideoModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol SearchVideoRouterProtocol: BaseRouterProtocol {
    func showVideoDetailsViewController(with video: VideoModel)
}
