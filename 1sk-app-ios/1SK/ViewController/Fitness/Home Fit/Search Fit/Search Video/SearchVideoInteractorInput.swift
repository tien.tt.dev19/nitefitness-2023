//
//  
//  SearchVideoInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchVideoInteractorInput {
    weak var output: SearchVideoInteractorOutputProtocol?
    var fitnessService: FitnessServiceProtocol?
}

// MARK: - SearchVideoInteractorInputProtocol
extension SearchVideoInteractorInput: SearchVideoInteractorInputProtocol {
    func getListVideoSearch(keywords: String, page: Int, perPage: Int) {
        self.fitnessService?.getListVideosSearch(categoryId: CategoryType.videoFit.rawValue, keywords: keywords, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListVideoSearchFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
