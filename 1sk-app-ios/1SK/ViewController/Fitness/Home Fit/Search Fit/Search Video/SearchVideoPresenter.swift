//
//  
//  SearchVideoPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchVideoPresenter {

    weak var view: SearchVideoViewProtocol?
    private var interactor: SearchVideoInteractorInputProtocol
    private var router: SearchVideoRouterProtocol

    init(interactor: SearchVideoInteractorInputProtocol,
         router: SearchVideoRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var keywords: String = ""
    var listVideo: [VideoModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        if self.keywords.count > 0 {
            self.page = page
            if page == 1 {
                self.page = 1
                self.isOutOfData = false
            }
            self.router.showHud()
            self.interactor.getListVideoSearch(keywords: self.keywords, page: page, perPage: self.limit)
            
        } else {
            self.listVideo.removeAll()
            self.view?.onReloadData()
        }
    }
}

// MARK: - SearchVideoPresenterProtocol
extension SearchVideoPresenter: SearchVideoPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    func onSearchAction(keywords: String) {
        self.keywords = keywords
        self.getData(in: 1)
    }
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.listVideo.count
    }
    
    func cellForRow_Video(at indexPath: IndexPath) -> VideoModel? {
        return self.listVideo[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        let video = self.listVideo[indexPath.row]
        self.router.showVideoDetailsViewController(with: video)
    }
    
    func onLoadMoreAction() {
        if self.listVideo.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func onRefreshAction() {
        self.getData(in: 1)
    }
    
    /// Value
    func getTotalVideo() -> Int {
        return self.total
    }
    
    func getKeywords() -> String {
        return self.keywords
    }
}

// MARK: - SearchVideoInteractorOutput 
extension SearchVideoPresenter: SearchVideoInteractorOutputProtocol {
    func onGetListVideoSearchFinished(with result: Result<[VideoModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.listVideo = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.listVideo.append(object)
                    self.view?.onInsertRow(at: self.listVideo.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listVideo.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            break
        }
        self.router.hideHud()
    }
}
