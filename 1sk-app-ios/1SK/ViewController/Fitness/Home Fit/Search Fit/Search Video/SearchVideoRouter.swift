//
//  
//  SearchVideoRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchVideoRouter: BaseRouter {
    static func setupModule() -> SearchVideoViewController {
        let viewController = SearchVideoViewController()
        let router = SearchVideoRouter()
        let interactorInput = SearchVideoInteractorInput()
        let presenter = SearchVideoPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.fitnessService = FitnessService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SearchVideoRouterProtocol
extension SearchVideoRouter: SearchVideoRouterProtocol {
    func showVideoDetailsViewController(with video: VideoModel) {
        let controller = PlayVideoRouter.setupModule(with: video, categoryType: .videoFit)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
