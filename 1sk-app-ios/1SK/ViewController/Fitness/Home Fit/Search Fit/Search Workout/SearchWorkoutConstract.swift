//
//  
//  SearchWorkoutConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

// MARK: - View
protocol SearchWorkoutViewProtocol: AnyObject {
    func onReloadData()
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol SearchWorkoutPresenterProtocol {
    func onViewDidLoad()
    func onSearchAction(keywords: String)
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow_Workout(at indexPath: IndexPath) -> WorkoutModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onLoadMoreAction()
    func onRefreshAction()
    
    /// Value
    func getTotalWorkout() -> Int
    func getKeywords() -> String
}

// MARK: - Interactor Input
protocol SearchWorkoutInteractorInputProtocol {
    func getListWorkoutSearch(keywords: String, page: Int, perPage: Int)
}

// MARK: - Interactor Output
protocol SearchWorkoutInteractorOutputProtocol: AnyObject {
    func onGetListWorkoutSearchFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol SearchWorkoutRouterProtocol: BaseRouterProtocol {
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?)
}
