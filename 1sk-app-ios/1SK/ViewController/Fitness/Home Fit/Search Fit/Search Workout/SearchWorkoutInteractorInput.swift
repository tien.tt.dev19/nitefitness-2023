//
//  
//  SearchWorkoutInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchWorkoutInteractorInput {
    weak var output: SearchWorkoutInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - SearchWorkoutInteractorInputProtocol
extension SearchWorkoutInteractorInput: SearchWorkoutInteractorInputProtocol {
    func getListWorkoutSearch(keywords: String, page: Int, perPage: Int) {
        self.exerciseService?.getListWorkoutSearch(keywords: keywords, page: page, perPage: perPage, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetListWorkoutSearchFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
}
