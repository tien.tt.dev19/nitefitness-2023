//
//  
//  SearchWorkoutPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchWorkoutPresenter {

    weak var view: SearchWorkoutViewProtocol?
    private var interactor: SearchWorkoutInteractorInputProtocol
    private var router: SearchWorkoutRouterProtocol

    init(interactor: SearchWorkoutInteractorInputProtocol,
         router: SearchWorkoutRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var keywords: String = ""
    var listWorkout: [WorkoutModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private func getData(in page: Int) {
        if self.keywords.count > 0 {
            self.page = page
            if page == 1 {
                self.page = 1
                self.isOutOfData = false
            }
            self.router.showHud()
            self.interactor.getListWorkoutSearch(keywords: self.keywords, page: page, perPage: self.limit)
            
        } else {
            self.listWorkout.removeAll()
            self.view?.onReloadData()
        }
    }
    
}

// MARK: - SearchWorkoutPresenterProtocol
extension SearchWorkoutPresenter: SearchWorkoutPresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    func onSearchAction(keywords: String) {
        self.keywords = keywords
        self.getData(in: 1)
    }
    
    /// UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.listWorkout.count
    }
    
    func cellForRow_Workout(at indexPath: IndexPath) -> WorkoutModel? {
        return self.listWorkout[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        let workout = self.listWorkout[indexPath.row]
        self.router.gotoDetailWorkoutViewController(with: workout, delegate: nil)
    }
    
    func onLoadMoreAction() {
        if self.listWorkout.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func onRefreshAction() {
        self.getData(in: 1)
    }
    
    /// Value
    func getTotalWorkout() -> Int {
        return self.total
    }
    
    func getKeywords() -> String {
        return self.keywords
    }
}

// MARK: - SearchWorkoutInteractorOutput 
extension SearchWorkoutPresenter: SearchWorkoutInteractorOutputProtocol {
    func onGetListWorkoutSearchFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.listWorkout = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.listWorkout.append(object)
                    self.view?.onInsertRow(at: self.listWorkout.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listWorkout.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            break
        }
        self.router.hideHud()
    }
}
