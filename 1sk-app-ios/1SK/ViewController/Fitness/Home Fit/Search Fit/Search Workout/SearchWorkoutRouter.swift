//
//  
//  SearchWorkoutRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/10/21.
//
//

import UIKit

class SearchWorkoutRouter: BaseRouter {
    static func setupModule() -> SearchWorkoutViewController {
        let viewController = SearchWorkoutViewController()
        let router = SearchWorkoutRouter()
        let interactorInput = SearchWorkoutInteractorInput()
        let presenter = SearchWorkoutPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SearchWorkoutRouterProtocol
extension SearchWorkoutRouter: SearchWorkoutRouterProtocol {
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?) {
        let workoutViewController = MyWorkoutRouter.setupModule(with: workout, delegate: delegate)
        self.viewController?.navigationController?.pushViewController(workoutViewController, animated: true)
    }
}
