//
//  
//  SearchFitConstract.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

// MARK: - View
protocol SearchFitViewProtocol: AnyObject {
    func onSeeMoreFooterAction(section: Int?)
}

// MARK: - Presenter
protocol SearchFitPresenterProtocol {
    func onViewDidLoad()
    
    // Action
    func onBackAction()
    func onClearTextSearchAction()
    
    // ViewController
    func searchAllViewController() ->  SearchAllViewController
    func searchWorkoutViewController() ->  SearchWorkoutViewController
    func searchVideoViewController() ->  SearchVideoViewController
    func searchBlogViewController() ->  SearchBlogViewController
}

// MARK: - Interactor Input
protocol SearchFitInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol SearchFitInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol SearchFitRouterProtocol {
    func popViewController()
}
