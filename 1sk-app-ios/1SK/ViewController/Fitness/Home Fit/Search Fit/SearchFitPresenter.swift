//
//  
//  SearchFitPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class SearchFitPresenter {

    weak var view: SearchFitViewProtocol?
    private var interactor: SearchFitInteractorInputProtocol
    private var router: SearchFitRouterProtocol

    init(interactor: SearchFitInteractorInputProtocol,
         router: SearchFitRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - SearchFitPresenterProtocol
extension SearchFitPresenter: SearchFitPresenterProtocol {
    func onViewDidLoad() {
        
    }
    
    // Action
    func onBackAction() {
        self.router.popViewController()
    }
    
    func onClearTextSearchAction() {
        
    }
    
    // ViewController
    func searchAllViewController() -> SearchAllViewController {
        return SearchAllRouter.setupModule(delegate: self)
    }
    
    func searchWorkoutViewController() -> SearchWorkoutViewController {
        return SearchWorkoutRouter.setupModule()
    }
    
    func searchVideoViewController() -> SearchVideoViewController {
        return SearchVideoRouter.setupModule()
    }
    
    func searchBlogViewController() -> SearchBlogViewController {
        return SearchBlogRouter.setupModule()
    }
    
}

// MARK: - SearchFitInteractorOutput 
extension SearchFitPresenter: SearchFitInteractorOutputProtocol {

}

// MARK: SearchAllPresenterDelegate
extension SearchFitPresenter: SearchAllPresenterDelegate {
    func onSeeMoreFooterAction(section: Int?) {
        self.view?.onSeeMoreFooterAction(section: section)
    }
}
