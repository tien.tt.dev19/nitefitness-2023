//
//  
//  SearchFitRouter.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class SearchFitRouter {
    weak var viewController: SearchFitViewController?
    static func setupModule() -> SearchFitViewController {
        let viewController = SearchFitViewController()
        let router = SearchFitRouter()
        let interactorInput = SearchFitInteractorInput()
        let presenter = SearchFitPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SearchFitRouterProtocol
extension SearchFitRouter: SearchFitRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
