//
//  
//  SearchFitViewController.swift
//  1SK
//
//  Created by vuongbachthu on 12/7/21.
//
//

import UIKit

class SearchFitViewController: BaseViewController {

    var presenter: SearchFitPresenterProtocol!
    
    @IBOutlet weak var tf_Search: UITextField!
    @IBOutlet weak var view_ContainerPage: UIView!
    
    // Page tab
    private lazy var pageViewController: VXPageViewController = VXPageViewController()
    
    var searchAllVC: SearchAllViewController!
    var searchWorkoutVC: SearchWorkoutViewController!
    var searchVideoVC: SearchVideoViewController!
    var searchBlogVC: SearchBlogViewController!
    
    private var isShowKeyboad = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitPageViewController()
        self.setInitUITextField()
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.presenter.onBackAction()
    }
    
}

// MARK: - SearchFitViewProtocol
extension SearchFitViewController: SearchFitViewProtocol {
    func onSeeMoreFooterAction(section: Int?) {
        print("onSeeMoreFooterAction section:", section ?? -1)
        switch section {
        case 0:
            self.pageViewController.setSelectedIndex(1, animated: true)

        case 1:
            self.pageViewController.setSelectedIndex(1, animated: true)

        case 2:
            self.pageViewController.setSelectedIndex(2, animated: true)

        default:
            break
        }
    }
}

// MARK: VX Page Bar
extension SearchFitViewController {
    private func setInitPageViewController() {
        let barConfig = VXPageBarConfig(height: 48,
                                      selectedColor: R.color.darkText(),
                                      unSelectedColor: R.color.subTitle(),
                                      selectedFont: R.font.robotoMedium(size: 16),
                                      unSelectedFont: R.font.robotoRegular(size: 16),
                                      underLineHeight: 3,
                                      underLineWidth: 60,
                                      underLineColor: R.color.mainColor(),
                                      backgroundColor: .white)
        
        self.searchAllVC = self.presenter.searchAllViewController()
        self.searchWorkoutVC = self.presenter.searchWorkoutViewController()
        self.searchVideoVC = self.presenter.searchVideoViewController()
        self.searchBlogVC = self.presenter.searchBlogViewController()
        
        let pageAll = VXPageItem(viewController: self.searchAllVC, title: "Tất cả")
//        let pageWorkout = VXPageItem(viewController: self.searchWorkoutVC, title: "Bài tập")
        let pageVideo = VXPageItem(viewController: self.searchVideoVC, title: "Video")
        let pageBlog = VXPageItem(viewController: self.searchBlogVC, title: "Bài viết")
        
        self.pageViewController.setPageItems([pageAll, pageVideo, pageBlog])
        self.pageViewController.setPageBarConfig(barConfig)
        
        self.addChild(self.pageViewController)
        
        self.view_ContainerPage.addSubview(self.pageViewController.view)
        self.pageViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.pageViewController.didMove(toParent: self)
    }
}

// MARK: - UITextFieldDelegate
extension SearchFitViewController: UITextFieldDelegate {
    private func setInitUITextField() {
        self.tf_Search.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.searchAllVC.presenter.onSearchAction(keywords: self.tf_Search.text ?? "")
//        self.searchWorkoutVC.presenter.onSearchAction(keywords: self.tf_Search.text ?? "")
        self.searchVideoVC.presenter.onSearchAction(keywords: self.tf_Search.text ?? "")
        self.searchBlogVC.presenter.onSearchAction(keywords: self.tf_Search.text ?? "")
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        self.searchAllVC.presenter.onSearchAction(keywords: "")
//        self.searchWorkoutVC.presenter.onSearchAction(keywords: "")
        self.searchVideoVC.presenter.onSearchAction(keywords: "")
        self.searchBlogVC.presenter.onSearchAction(keywords: "")
        return true
    }
}
