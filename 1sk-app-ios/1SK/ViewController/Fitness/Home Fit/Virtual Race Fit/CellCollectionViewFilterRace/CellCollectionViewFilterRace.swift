//
//  CellCollectionViewFilterRace.swift
//  1SK
//
//  Created by TrungDN on 14/07/2022.
//

import UIKit

class CellCollectionViewFilterRace: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            self.mainView.backgroundColor = isSelected ? R.color.mainColor() : UIColor(hex: "EEEEEE")
            self.titleLabel.textColor = isSelected ? UIColor.white : UIColor(hex: "737678")
        }
    }
    
    var model: TypeRaceFilter? {
        didSet {
            self.setData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: - helpers
extension CellCollectionViewFilterRace {
    private func setData() {
        guard let model = self.model else { return }
        self.titleLabel.text = model.filterTitle
    }
}
