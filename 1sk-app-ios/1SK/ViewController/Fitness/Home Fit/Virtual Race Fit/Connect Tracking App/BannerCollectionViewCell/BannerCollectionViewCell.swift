//
//  BannerCollectionViewCell.swift
//  1SK
//
//  Created by TrungDN on 29/07/2022.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bannerImageView: UIImageView!
    
    var model: RaceBannersModel? {
        didSet {
            self.setData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    private func setData() {
        guard let model = model else {
            return
        }
        self.bannerImageView.setImageWith(imageUrl: model.image ?? "")
    }

}
