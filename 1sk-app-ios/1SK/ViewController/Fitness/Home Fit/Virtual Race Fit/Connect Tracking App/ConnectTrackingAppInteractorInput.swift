//
//  
//  ConnectTrackingAppInteractorInput.swift
//  1SK
//
//  Created by Valerian on 20/07/2022.
//
//

import UIKit
import Alamofire

// MARK: - Interactor Input Protocol
protocol ConnectTrackingAppInteractorInputProtocol {
    func onConnectStrava(code: String)
    func onDisconnectStrava()
    func onIdentifyProviders()
    func onGetMeIdentifyProviders()
    func onRequestStravaToken(params: [String: Any])
}

// MARK: - Interactor Output Protocol
protocol ConnectTrackingAppInteractorOutputProtocol: AnyObject {
    func didConnectStravaFinised(with result: Result<EmptyModel, APIError>)
    func didDisconnectStravaFinised(with result: Result<EmptyModel, APIError>)
    func didIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>)
    func didGetMeIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>)
}

// MARK: - ConnectTrackingApp InteractorInput
class ConnectTrackingAppInteractorInput {
    weak var output: ConnectTrackingAppInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - ConnectTrackingApp InteractorInputProtocol
extension ConnectTrackingAppInteractorInput: ConnectTrackingAppInteractorInputProtocol {
    func onConnectStrava(code: String) {
        self.fitRaceSevice?.onConnectStravaCode(code: code, completion: { [weak self] result in
            self?.output?.didConnectStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onDisconnectStrava() {
        self.fitRaceSevice?.onDisConnectStrava(completion: { [weak self] result in
            self?.output?.didDisconnectStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onIdentifyProviders() {
        self.fitRaceSevice?.onIdentifyProviders(completion: { [weak self] result in
            self?.output?.didIdentifyProvidersStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetMeIdentifyProviders() {
        self.fitRaceSevice?.onGetMeIdentifyProvider(completion: { [weak self] result in
            self?.output?.didGetMeIdentifyProvidersStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onRequestStravaToken(params: [String: Any]) {
        AF.request("https://www.strava.com/oauth/token", method: .post, parameters: params).response { response in
            guard let data = response.data else { return }
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                print("tien log json : \(json)")
                if let refresh_token = json["refresh_token"] as? String {
                    print("tien log refresh_token : \(refresh_token)")
                    self.fitRaceSevice?.onConnectStravaCode(code: refresh_token, completion: { [weak self] result in
                        self?.output?.didConnectStravaFinised(with: result.unwrapSuccessModel())
                    })
                }
            }
        }
    }
}
