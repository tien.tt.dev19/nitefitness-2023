//
//  
//  ConnectTrackingAppRouter.swift
//  1SK
//
//  Created by Valerian on 20/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ConnectTrackingAppRouterProtocol {
    
}

// MARK: - ConnectTrackingApp Router
class ConnectTrackingAppRouter {
    weak var viewController: ConnectTrackingAppViewController?
    
    static func setupModule(with banner: [RaceBannersModel]) -> ConnectTrackingAppViewController {
        let viewController = ConnectTrackingAppViewController()
        let router = ConnectTrackingAppRouter()
        let interactorInput = ConnectTrackingAppInteractorInput()
        let viewModel = ConnectTrackingAppViewModel(interactor: interactorInput)
        viewModel.banners = banner
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.fitRaceSevice = FitRaceService()
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ConnectTrackingApp RouterProtocol
extension ConnectTrackingAppRouter: ConnectTrackingAppRouterProtocol {

}
