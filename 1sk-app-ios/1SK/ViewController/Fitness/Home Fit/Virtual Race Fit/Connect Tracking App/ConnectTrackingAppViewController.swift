//
//  
//  ConnectTrackingAppViewController.swift
//  1SK
//
//  Created by Valerian on 20/07/2022.
//
//

import UIKit
import Alamofire
import AuthenticationServices


// MARK: - ViewProtocol
protocol ConnectTrackingAppViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func didConnectStrava(with model: StravaModel?)
    func didDisconnectStrava()
    
    //UITableView
    //func onReloadData()
}

// MARK: - ConnectTrackingApp ViewController
class ConnectTrackingAppViewController: BaseViewController {
    var router: ConnectTrackingAppRouterProtocol!
    var viewModel: ConnectTrackingAppViewModelProtocol!
    
    @IBOutlet weak var img_Strava: UIImageView!
    @IBOutlet weak var view_ProfileUser: UIView!
    @IBOutlet weak var btn_ConnectStrava: UIButton!
    @IBOutlet weak var lbl_ConnectPaddingLabel: PaddingLabel!
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var img_UserAvatar: UIImageView!
    @IBOutlet weak var lbl_UserId: UILabel!
    @IBOutlet weak var coll_Banners: UICollectionView!
    @IBOutlet weak var lbl_Link: UILabel!
    @IBOutlet weak var lbl_Guide: UILabel!
    @IBOutlet weak var stv_ConnectSuccess: UIStackView!
    @IBOutlet weak var view_Link: UIView!
    @IBOutlet weak var lbl_Related: UILabel!
    @IBOutlet weak var view_Stava: UIView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onGetMeIdentifyProviders()
    }
    
    // MARK: - Init
    private func setupInit() {
        DispatchQueue.main.async {
            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
            let cellHeight = (self.coll_Banners.frame.width - 5) / 2
            layout.itemSize = CGSize(width: cellHeight, height: cellHeight)
            layout.minimumInteritemSpacing = 5
            layout.minimumLineSpacing = 5
            self.coll_Banners!.collectionViewLayout = layout
            self.coll_Banners.isHidden = false
        }
       
        self.title = "Thông tin đăng ký"
        self.isInteractivePopGestureEnable = false
        self.lbl_UserName.text = gUser?.fullName
        self.img_UserAvatar.setImageWith(imageUrl: gUser?.avatar ?? "")
        self.lbl_UserId.text = "\(gUser?.id ?? 0)"
        self.coll_Banners.registerNib(ofType: BannerCollectionViewCell.self)
        
        guard let text = self.lbl_Link.text else { return }
        let textRange = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(.underlineStyle,
                                    value: NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        // Add other attributes if needed
        self.lbl_Link.attributedText = attributedText

        self.lbl_Link.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowLink(_:))))
        self.view_Link.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowLinkView(_:))))
        self.lbl_Related.isHidden = self.viewModel.collectionView(self.coll_Banners, numberOfItemsInSection: 0) == 0
        
        NotificationCenter.default.addObserver(self, selector: #selector(onConnectStrava), name: .applicationStravaUrlCallBack, object: nil)
    }
    
    deinit {
        self.isInteractivePopGestureEnable = true
        NotificationCenter.default.removeObserver(self, name: .applicationStravaUrlCallBack, object: nil)
    }
    
    // MARK: - Action
    
    @IBAction private func onConnectToStravaAction(_ sender: UIButton) {
        if self.btn_ConnectStrava.titleLabel?.text == "Kết nối" {
            self.viewModel.onAuthenStrava(vc: self)
        } else {
            self.viewModel.onDisconnectStrava()
        }
    }
    
    @objc private func onShowLink(_ sender: UITapGestureRecognizer) {
        self.onOpenLinkToSafari(url: GUIDE_CONNECTING_STRAVA_URL)
    }
    
    @objc private func onShowLinkView(_ sender: UITapGestureRecognizer) {
        self.onOpenLinkToSafari(url: GUIDE_TRACKING_STRAVA_URL)
    }
        
    override func setLeftBackButton(_ isInteractivePopGestureEnable: Bool = true) {
        self.setLeftBarButton(style: .back) { [weak self] _ in
            if let vc = self?.navigationController?.viewControllers.filter({$0 is DetailRacingViewController}).first as? DetailRacingViewController {
                self?.navigationController?.popToViewController(vc, animated: true)
            }
        }
        self.isInteractivePopGestureEnable = isInteractivePopGestureEnable
    }
}

// MARK: - ConnectTrackingApp ViewProtocol
extension ConnectTrackingAppViewController: ConnectTrackingAppViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func didConnectStrava(with model: StravaModel?) {
        self.view_Stava.isHidden = false
        self.lbl_Link.isHidden = true
        self.img_Strava.isHidden = true
        
        self.view_ProfileUser.isHidden = false
        self.view_Link.isHidden = false
        
        self.lbl_Guide.isHidden = true
        self.stv_ConnectSuccess.isHidden = false

        self.btn_ConnectStrava.backgroundColor = .white
        self.btn_ConnectStrava.setTitleColor(R.color.mainColor(), for: .normal)
        self.btn_ConnectStrava.setTitle("Ngắt kết nối", for: .normal)
        
        if let stravaInfo = model?.info {
            self.lbl_UserName.text = "\(stravaInfo.firstname ?? "") \(stravaInfo.lastname ?? "")"
            self.img_UserAvatar.setImageWith(imageUrl: stravaInfo.profile ?? "")
            self.lbl_UserId.text = "\(stravaInfo.id ?? 0)"
        } else {
            self.lbl_UserName.text = gUser?.fullName
            self.img_UserAvatar.setImageWith(imageUrl: gUser?.avatar ?? "")
            self.lbl_UserId.text = "\(gUser?.id ?? 0)"
        }
    }
    
    func didDisconnectStrava() {
        self.view_Stava.isHidden = false
        self.lbl_Link.isHidden = false
        self.img_Strava.isHidden = false
        self.view_ProfileUser.isHidden = true
        self.view_Link.isHidden = true
        
        self.stv_ConnectSuccess.isHidden = false
        self.lbl_Guide.isHidden = false
        
        self.btn_ConnectStrava.backgroundColor = R.color.mainColor()
        self.btn_ConnectStrava.setTitleColor(.white, for: .normal)
        self.btn_ConnectStrava.setTitle("Kết nối", for: .normal)
    }
    
    @objc func onConnectStrava() {
        self.showHud()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.viewModel.onConnectStrava(code: self.getCode(from: gStravaURL)!)
        }
    }
    
    private func getCode(from url: URL?) -> String? {
        guard let url = url?.absoluteString else { return nil }
        
        let urlComponents: URLComponents? = URLComponents(string: url)
        let code: String? = urlComponents?.queryItems?.filter { $0.name == "code" }.first?.value
        
        return code
    }
}

//MARK: - ASWebAuthenticationPresentationContextProviding
extension ConnectTrackingAppViewController: ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        UIApplication.shared.windows[0]
    }
}

//MARK: - UICollectionViewDelegate
extension ConnectTrackingAppViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let url = URL(string: self.viewModel.collectionView(collectionView, cellForItemAt: indexPath).url ?? "") else {
          return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
}

//MARK: - UICollectionViewDataSource
extension ConnectTrackingAppViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.collectionView(collectionView, numberOfItemsInSection: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: BannerCollectionViewCell.self, for: indexPath)
        cell.model = self.viewModel.collectionView(collectionView, cellForItemAt: indexPath)
        return cell
    }

}

//MARK: - UICollectionViewDelegateFlowLayout
extension ConnectTrackingAppViewController: UICollectionViewDelegateFlowLayout {
}
