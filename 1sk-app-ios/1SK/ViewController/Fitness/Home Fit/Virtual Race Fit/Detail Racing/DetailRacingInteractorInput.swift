//
//  
//  DetailRacingInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol DetailRacingInteractorInputProtocol {
    func getVirtualRaceDetail(raceId: Int)
    func getVirtualRaceLeaderboard(raceId: Int, customerId: Int)
}

// MARK: - Interactor Output Protocol
protocol DetailRacingInteractorOutputProtocol: AnyObject {
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>)
    func onGetVirtualRaceLeaderboardFinished(with result: Result<[LeaderboardModel], APIError>)
}

// MARK: - DetailRacing InteractorInput
class DetailRacingInteractorInput {
    weak var output: DetailRacingInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - DetailRacing InteractorInputProtocol
extension DetailRacingInteractorInput: DetailRacingInteractorInputProtocol {
    func getVirtualRaceDetail(raceId: Int) {
        self.fitRaceSevice?.onGetVirtualRaceDetail(id: raceId, completion: { [weak self] result in
            self?.output?.onGetVirtualRaceDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getVirtualRaceLeaderboard(raceId: Int, customerId: Int) {
        self.fitRaceSevice?.onGetVirtualRaceLeaderboard(raceId: raceId, customerId: customerId, page: 1, perPage: 10, completion: { [weak self] result in
            self?.output?.onGetVirtualRaceLeaderboardFinished(with: result.unwrapSuccessModel())
        })
    }
}
