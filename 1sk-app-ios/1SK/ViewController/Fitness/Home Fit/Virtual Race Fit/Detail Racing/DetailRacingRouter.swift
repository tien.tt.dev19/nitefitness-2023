//
//  
//  DetailRacingRouter.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol DetailRacingRouterProtocol {
    func showInfoRegisterRaceRouter(data: RaceModel)
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
}

// MARK: - DetailRacing Router
class DetailRacingRouter {
    weak var viewController: DetailRacingViewController?

    static func setupModule(with id: Int, status: TypeRaceFilter) -> DetailRacingViewController {
        let viewController = DetailRacingViewController()
        viewController.hidesBottomBarWhenPushed = true
        let router = DetailRacingRouter()
        let interactorInput = DetailRacingInteractorInput()

        let viewModel = DetailRacingViewModel(interactor: interactorInput)
        viewModel.raceId = id
        viewModel.status = status
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - DetailRacing RouterProtocol
extension DetailRacingRouter: DetailRacingRouterProtocol {
    func showInfoRegisterRaceRouter(data: RaceModel) {
        let controller = InfoRegisterRaceRouter.setupModule(data: data)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        let controller = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        controller.popoverPresentationController?.sourceView = sourceView
        controller.popoverPresentationController?.sourceRect = sourceView.bounds
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
