//
//  
//  DetailRacingViewController.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit
import WebKit

// MARK: - ViewProtocol
protocol DetailRacingViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func reloadView(with raceModel: RaceModel, leaderboardModels: [LeaderboardModel])
    func getShareView() -> UIView
    func showShareViewController(shareItem: [Any], sourceView: UIView)
}

// MARK: - DetailRacing ViewController
class DetailRacingViewController: BaseViewController {
    var router: DetailRacingRouterProtocol!
    var viewModel: DetailRacingViewModelProtocol!
    
    // Navigation
    @IBOutlet weak var view_nav_Fill_Bg: UIView!
    @IBOutlet weak var view_nav_Fill: UIView!
    @IBOutlet weak var view_nav_Blur: UIView!
    @IBOutlet weak var lbl_nav_Title: UILabel!
    
    @IBOutlet weak var view_Scroll: UIScrollView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var view_PageMenu: UIView!
    
    // Header
    @IBOutlet weak var img_ThumbImage: UIImageView!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_SportType: UILabel!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var lbl_RegisterAnable: UILabel!
    
    // Participant Stack View
    @IBOutlet weak var stack_Participant: UIStackView!
    @IBOutlet weak var lbl_Participant: UILabel!
    
    // Price stack view
    @IBOutlet weak var stack_Price: UIStackView!
    @IBOutlet weak var stack_Registered: UIStackView!
    @IBOutlet weak var lbl_Price: UILabel!
    
    // Time view
    @IBOutlet weak var view_Time: UIView!
    @IBOutlet weak var lbl_TitleTime: UILabel!
    @IBOutlet weak var lbl_Day: UILabel!
    @IBOutlet weak var lbl_Hour: UILabel!
    @IBOutlet weak var lbl_Minute: UILabel!
    @IBOutlet weak var lbl_Second: UILabel!
    
    // Result view
    @IBOutlet weak var view_Result: UIView!
    @IBOutlet weak var lbl_Athletes: UILabel!
    @IBOutlet weak var lbl_Distance: UILabel!
    @IBOutlet weak var lbl_Group: UILabel!
    
    // Progress view
    @IBOutlet weak var view_Progress: UIView!
    @IBOutlet weak var progress_Result: UIProgressView!
    @IBOutlet weak var lbl_Result: UILabel!
    @IBOutlet weak var lbl_Progress: UILabel!
    
    // Bottom view
    @IBOutlet weak var view_Bottom: UIView!
    @IBOutlet weak var view_Status: UIView!
    @IBOutlet weak var view_RegisterAnable: UIView!
    @IBOutlet weak var view_Register: UIView!
    
    // Constraints
    @IBOutlet weak var constraint_top_ThumbImage: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_ThumbImage: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_Content: NSLayoutConstraint!
    
    private let pageBarHeight: CGFloat = 48
    private let pageBarTopConstraint: CGFloat = 16
    
    private lazy var pageViewController: VXPageViewController = VXPageViewController()
    
    var pageInfoDetailRacingRouter = InfoDetailRacingRouter.setupModule()
    var pageLeaderboardDetailRacingRouter = LeaderboardDetailRacingRouter.setupModule()    
    var timer: Timer?
    var status: TypeRaceFilter!
    
    var countDountTime: Int = 0 {
        didSet {
            if self.countDountTime == 0 {
                self.timer?.invalidate()
                self.timer = nil
                self.viewModel.onViewWillApear()
                
            } else {
                let days = self.countDountTime / 86400
                if days / 10 > 0 {
                    self.lbl_Day.text = String(days)
                } else {
                    self.lbl_Day.text = "0\(days)"
                }
                
                let hour = self.countDountTime % 86400 / 3600
                if hour / 10 > 0 {
                    self.lbl_Hour.text = String(hour)
                } else {
                    self.lbl_Hour.text = "0\(hour)"
                }
                
                let minute = self.countDountTime % 3600 / 60
                if minute / 10 > 0 {
                    self.lbl_Minute.text = String(minute)
                } else {
                    self.lbl_Minute.text = "0\(minute)"
                }
                
                let second = self.countDountTime % 3600 % 60
                if second / 10 > 0 {
                    self.lbl_Second.text = String(second)
                } else {
                    self.lbl_Second.text = "0\(second)"
                }

            }
        }
    }
    
    var inforContentHeight: CGFloat = 0 {
        didSet {
            if selectedIndex == 0 {
                self.constraint_height_Content.constant = self.pageBarHeight + self.pageBarTopConstraint + self.inforContentHeight
                
            } else {
                self.constraint_height_Content.constant = self.pageBarHeight + self.pageBarTopConstraint + self.leaderboardHeight
            }
        }
    }
    
    var leaderboardHeight: CGFloat = 0 {
        didSet {
            if selectedIndex == 1 {
                self.constraint_height_Content.constant = self.pageBarHeight + self.pageBarTopConstraint + self.leaderboardHeight
            } else {
                self.constraint_height_Content.constant = self.pageBarHeight + self.pageBarTopConstraint + self.inforContentHeight
            }
        }
    }
    
    var selectedIndex: Int = 0
    
    var thumbImageViewDefaultTopConstraint: CGFloat = 0
    var thumbImageViewDefaultHeightConstraint: CGFloat = 0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view_Content.roundCorner(corners: [.topRight, .topLeft], radius: 16)
        self.timer?.invalidate()
        self.timer = nil
        self.viewModel.onViewWillApear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewModel.onViewDidDisappear()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    deinit {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    @IBAction func onBackSelected(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onShareSelected(_ sender: UIButton) {
        self.viewModel.onShareSelected()
    }
    
    @IBAction func onJointSelected(_ sender: UIButton) {
        guard let raceDetailData = self.viewModel.onGetRaceDetailData() else { return }
        self.router.showInfoRegisterRaceRouter(data: raceDetailData)
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitPageViewController()
        self.setInitNavigationBar()
        
        self.view_Scroll.alpha = 0
        self.view_Scroll.delegate = self
        
        self.view_Progress.isHidden = true
        self.view_Result.isHidden = true
                
        self.thumbImageViewDefaultTopConstraint = self.constraint_top_ThumbImage.constant
        self.thumbImageViewDefaultHeightConstraint = self.constraint_height_ThumbImage.constant
        
        if let top = UIApplication.shared.windows.first?.safeAreaInsets.top {
            view_Scroll.contentInset.top = -top
        }
    }
    
    // MARK: - Action
}

// MARK: - DetailRacing ViewProtocol
extension DetailRacingViewController: DetailRacingViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func getShareView() -> UIView {
        return self.btn_Share
    }
    
    func showShareViewController(shareItem: [Any], sourceView: UIView) {
        self.router.showShareViewController(with: shareItem, sourceView: sourceView)
    }
    
    func configView(with status: TypeRaceFilter, raceModel: RaceModel) {
        let startDate = raceModel.startAt?.toDate(.ymdhms)?.timeIntervalSince1970 ?? 0
        let endDate = raceModel.finishAt?.toDate(.ymdhms)?.timeIntervalSince1970 ?? 0
        let regiterDate = raceModel.registerExpireAt?.toDate(.ymdhms)?.timeIntervalSince1970 ?? 0
    
        if regiterDate < Date().timeIntervalSince1970 {
            self.view_RegisterAnable.backgroundColor = UIColor(hex: "C4C4C4")
            self.lbl_RegisterAnable.text = "Hết hạn đăng ký"
        }
    
        if let isRegistered = raceModel.isRegistered {
            self.stack_Registered.isHidden = !isRegistered
            self.view_Progress.isHidden = !isRegistered
            if isRegistered {
                self.view_Bottom.isHidden = isRegistered
                var unit = ""
                if let sport = raceModel.sport {
                    unit = sport.unit ?? ""
                }
                if let customerGoal = raceModel.customerGoal {
                    if let stravaTracking = raceModel.stravaTracking {
                        
                        let totalDistanceCustomer = (stravaTracking.totalDistanceCustomer ?? 0) / 1000
                        let roundedDistanceCustomer = Float(round(totalDistanceCustomer * 100) / 100)
                        let progress = roundedDistanceCustomer / (Float(customerGoal.goal ?? "1") ?? 1)
                        self.progress_Result.progress = progress
                        self.lbl_Result.text = "\(roundedDistanceCustomer)/\(customerGoal.goal ?? "") \(unit)"
                        self.lbl_Progress.text = "Đã thực hiện \(Int(progress * 100))%"
                        
                        let totalDistance = (stravaTracking.totalDistance ?? 0) / 1000
                        let roundedDistance = Float(round(totalDistance * 100) / 100)
                        self.lbl_Distance.text = "\(roundedDistance) Km"
                    }
                }
            } else {
                self.view_Bottom.isHidden = regiterDate < Date().timeIntervalSince1970
            }
        }
        
        switch status {
        case .inprocess:
            self.view_Time.isHidden = false
            self.stack_Participant.isHidden = false
            self.stack_Price.isHidden = false
            self.view_Result.isHidden = true
            self.lbl_Status.text = status.filterTitle
            self.lbl_TitleTime.text = "Kết thúc sau"
            self.lbl_Status.textColor = UIColor(hex: "FFFFFF")
            self.view_Status.backgroundColor = UIColor(hex: "00C2C5")
            let secondLeft = abs(Int(endDate - Date().timeIntervalSince1970))
            self.countDountTime = secondLeft

            self.timer?.invalidate()
            self.timer = nil
 
            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
                DispatchQueue.main.async {
                    self.countDountTime -= 1
                    if !self.view_Bottom.isHidden && regiterDate < Date().timeIntervalSince1970 {
                        self.view_Bottom.isHidden = true
                    }
                }
            })
            RunLoop.main.add(self.timer!, forMode: .common)

        case .upcoming:
            self.view_Time.isHidden = false
            self.stack_Participant.isHidden = false
            self.stack_Price.isHidden = false
            self.view_Result.isHidden = true
            self.lbl_Status.text = status.filterTitle
            self.lbl_TitleTime.text = "Bắt đầu sau"
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = UIColor(hex: "FEB11A")
            let secondLeft = abs(Int(startDate - Date().timeIntervalSince1970))
            self.countDountTime = secondLeft
            
            self.timer?.invalidate()
            self.timer = nil

            self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
                self.countDountTime -= 1
                if !self.view_Bottom.isHidden && regiterDate < Date().timeIntervalSince1970 {
                    self.view_Bottom.isHidden = true
                }
            })
            RunLoop.main.add(self.timer!, forMode: .common)

        case .ended:
            if !self.stack_Registered.isHidden {
                self.stack_Registered.isHidden = true
            }
            self.timer?.invalidate()
            self.timer = nil
            self.view_Time.isHidden = true
            self.stack_Participant.isHidden = true
            self.stack_Price.isHidden = true
            self.view_Result.isHidden = false
            self.lbl_Status.text = status.filterTitle
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = UIColor(hex: "737678")
            self.lbl_Athletes.text = "\(raceModel.totalParticipants ?? 0)"
            self.view_Register.isHidden = true
        
        default:
            break
        }
    }
    
    func reloadView(with raceModel: RaceModel, leaderboardModels: [LeaderboardModel]) {
        self.img_ThumbImage.setImageWith(imageUrl: raceModel.thumbnail ?? "")
        self.lbl_Title.text = raceModel.name ?? ""
        self.lbl_Participant.text = "\(raceModel.totalParticipants ?? 0) người đăng ký"
        
        self.pageInfoDetailRacingRouter.reloadData(raceModel: raceModel)
        self.pageLeaderboardDetailRacingRouter.reloadData(listData: leaderboardModels, raceModel: self.viewModel.raceModel)
        
        if let sport = raceModel.sport {
            self.lbl_SportType.text = sport.name?.uppercased()
        }
        
        guard let state = raceModel.statusOfHappening, state != -1, let status = TypeRaceFilter(rawValue: state) else {
            return
        }
        
        self.status = status
        self.configView(with: status, raceModel: raceModel)
        
        // reload view animation
        UIView.transition(with: self.view_Scroll,
                          duration: 0.35,
                          options: .transitionCrossDissolve,
                          animations: { self.view_Scroll.alpha = 1 })
        
    }
}

// MARK: VX Page Bar
extension DetailRacingViewController {
    private func setInitPageViewController() {
        let barConfig = VXPageBarConfig(height: self.pageBarHeight,
                                        selectedColor: R.color.darkText(),
                                        unSelectedColor: R.color.subTitle(),
                                        selectedFont: R.font.robotoMedium(size: 16),
                                        unSelectedFont: R.font.robotoRegular(size: 16),
                                        underLineHeight: 3,
                                        underLineWidth: (self.view.width / 2) - 32,
                                        underLineColor: R.color.mainColor(),
                                        backgroundColor: .white)
        
        let pageInfo = self.pageInfoDetailRacingRouter
        pageInfo.delegate = self
        
        let pageLeaderboard = self.pageLeaderboardDetailRacingRouter
        pageLeaderboard.delegate = self
        
        let pageInfoItem = VXPageItem(viewController: pageInfo, pageBarItem: VXPageBarItem(title: "Thông tin"))
        let pageLeaderboardItem = VXPageItem(viewController: pageLeaderboard, pageBarItem: VXPageBarItem(title: "Bảng xếp hạng"))
        
        self.pageViewController.setPageItems([pageInfoItem, pageLeaderboardItem])
        self.pageViewController.setPageBarConfig(barConfig)
        self.pageViewController.delegate = self
        
        self.addChild(self.pageViewController)
        self.view_PageMenu.addSubview(self.pageViewController.view)
        self.pageViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.pageViewController.didMove(toParent: self)
    }
}

// MARK: UIScrollViewDelegate
extension DetailRacingViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        self.setStateNavigationBar(contentOffset: contentOffset)
        
        let offsetY = scrollView.contentOffset.y
        if offsetY < 0 {
            self.constraint_height_ThumbImage.constant = self.thumbImageViewDefaultHeightConstraint - offsetY
            self.constraint_top_ThumbImage.constant = offsetY
        } else {
            self.constraint_height_ThumbImage.constant = self.thumbImageViewDefaultHeightConstraint
            self.constraint_top_ThumbImage.constant = self.thumbImageViewDefaultTopConstraint
        }
    }
}

// MARK: - LeaderboardDetailRacingDelegate
extension DetailRacingViewController: LeaderboardDetailRacingDelegate {
    func onSendContentHeight(height: CGFloat) {
        self.leaderboardHeight = height
    }
}

// MARK: - LeaderboardDetailRacingDelegate
extension DetailRacingViewController: InfoDetailRacingViewControllerDelegate {
    func updateContentSize(with height: CGFloat) {
        self.inforContentHeight = height
    }
    
    func updateRaceDetailData(race: RaceModel) {
        DispatchQueue.main.async {
            self.viewModel.onReloadNewData(raceID: race.id ?? -1, status: .inprocess)
            self.view_Scroll.scrollToTop(animated: true)
        }
    }
    
    func removePreviousViewController() {
        self.removeFromNavigationController()
    }
}

// MARK: VXPageViewDelegate
extension DetailRacingViewController: VXPageViewDelegate {
    func onDidChangeSelectedPage(at index: Int) {
        self.selectedIndex = index
        if selectedIndex == 0 {
            self.constraint_height_Content.constant = self.pageBarHeight + self.pageBarTopConstraint + self.inforContentHeight
            
        } else {
            self.constraint_height_Content.constant = self.pageBarHeight + self.pageBarTopConstraint + self.leaderboardHeight
        }
    }
}

// MARK: - Navigation Bar
extension DetailRacingViewController {
    func setInitNavigationBar() {
        self.view_nav_Fill.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.view_nav_Fill.isHidden = true
        self.view_nav_Fill_Bg.isHidden = true
    }
    
    func setStateNavigationBar(contentOffset: CGFloat) {
        let position = self.img_ThumbImage.frame.height - 44
        
        if contentOffset <= position {
            if self.view_nav_Fill.isHidden == false {
                UIView.animate(withDuration: 0.5) {
                    self.view_nav_Fill.alpha = 0
                    self.view_nav_Fill_Bg.alpha = 0
                    self.view.layoutIfNeeded()
                    
                } completion: { _ in
                    self.view_nav_Fill.isHidden = true
                    self.view_nav_Fill_Bg.isHidden = true
                }
            }
        }
        
        if contentOffset > position {
            if self.view_nav_Fill.isHidden == true {
                self.view_nav_Fill.isHidden = false
                self.view_nav_Fill.alpha = 0
                
                self.view_nav_Fill_Bg.isHidden = false
                self.view_nav_Fill_Bg.alpha = 0
                
                self.lbl_nav_Title.text = self.viewModel.onGetRaceDetailData()?.name ?? ""
                
                UIView.animate(withDuration: 0.5) {
                    self.view_nav_Fill.alpha = 1
                    self.view_nav_Fill_Bg.alpha = 1
                    self.view.layoutIfNeeded()
                    
                } completion: { _ in
                    
                }
            }
        }
    }
}
