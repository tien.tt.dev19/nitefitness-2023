//
//  
//  DetailRacingViewModel.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol DetailRacingViewModelProtocol {
    func onViewDidLoad()
    func onViewWillApear()
    func onViewDidAppear()
    func onViewDidDisappear()
    
    func onGetLeaderBoardData() -> [LeaderboardModel]
    func onGetRaceDetailData() -> RaceModel?
    func onShareSelected()
    func onReloadNewData(raceID: Int, status: TypeRaceFilter)
    
    var raceModel: RaceModel? { get set }
}

// MARK: - DetailRacing ViewModel
class DetailRacingViewModel {
    weak var view: DetailRacingViewProtocol?
    private var interactor: DetailRacingInteractorInputProtocol
    
    init(interactor: DetailRacingInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var raceModel: RaceModel?
    var leaderboardModels: [LeaderboardModel] = []
    var listVirtualRace: [RaceModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 20
    private var isOutOfData = false
    
    private let MAX_API = 2
    var finalStatus = true
    
    private var numberOfApi = 0 {
        didSet {
            if numberOfApi == MAX_API {
                if finalStatus {
                    // reload view
                    if let raceModel = raceModel {
                        self.view?.reloadView(with: raceModel, leaderboardModels: self.leaderboardModels)
                    }
                } else {
                    print("Đã có lỗi xảy ra")
                }
                self.view?.hideHud()
            }
        }
    }
    
    var raceId: Int!
    var status: TypeRaceFilter!
    
    func getData() {
        self.view?.showHud()
        self.numberOfApi = 0
        self.interactor.getVirtualRaceDetail(raceId: self.raceId)
        
        if let userId = gUser?.id {
            self.interactor.getVirtualRaceLeaderboard(raceId: self.raceId, customerId: userId)
        }
    }
}

// MARK: - DetailRacing ViewModelProtocol
extension DetailRacingViewModel: DetailRacingViewModelProtocol {
    func onViewDidLoad() {}
    
    func onViewWillApear() {
        self.getData()
    }
    
    func onViewDidAppear() {
        self.setInitObserverEvent()
    }
    
    func onViewDidDisappear() {
        self.setRemoveObserverEvent()
    }
    
    func onReloadNewData(raceID: Int, status: TypeRaceFilter) {
        self.raceId = raceID
        self.status = .inprocess
        
        self.view?.showHud()
        self.numberOfApi = 0
        self.interactor.getVirtualRaceDetail(raceId: self.raceId)
        
        if let userId = gUser?.id {
            self.interactor.getVirtualRaceLeaderboard(raceId: self.raceId, customerId: userId)
        }
    }
    
    func onShareSelected() {
        guard let slug = self.raceModel?.slug else {
            SKToast.shared.showToast(content: "Không thể chia sẻ giải đua này")
            return
        }
        
        self.view?.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.raceModel?.name
        meta.socialMetaDescText = self.raceModel?.information
        meta.socialMetaImageURL = self.raceModel?.thumbnail
        
        let universalLink = "\(SHARE_URL)\(slug)\(ShareType.virtualRace.rawValue)"
        
        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, universalLink: universalLink) {
            linkBuilder.shorten(completion: { url, warnings, error in
                self.view?.hideHud()
                
                if let listWarning = warnings {
                    for (index, warning) in listWarning.enumerated() {
                        print("createLinkShareFitnessPost warning: \(index) - \(warning)")
                    }
                }

                if let err = error {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost error: \(err.localizedDescription)")
                    return
                }

                guard let urlShare = url else {
                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
                    print("createLinkShareFitnessPost url: nil")
                    return
                }
                
                guard let shareView = self.view?.getShareView() else {
                    return
                }
                self.view?.showShareViewController(shareItem: [urlShare], sourceView: shareView)
                
                print("createLinkShareFitnessPost The short URL is: \(urlShare)")
            })
            
        } else {
            self.view?.hideHud()
            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
        }
    }
    
    func onGetLeaderBoardData() -> [LeaderboardModel] {
        return self.leaderboardModels
    }
    
    func onGetRaceDetailData() -> RaceModel? {
        if let data = self.raceModel {
            return data
        }
        return nil
    }
}

// MARK: HandleSocketEvent
extension DetailRacingViewModel {
    func setInitObserverEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUserRegisterVRAction), name: .UserRegisterVR, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onVRRegistrationJustExpiredEventAction), name: .VRRegistrationJustExpiredEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onVRShouldUpdateRankingEventAction), name: .VRShouldUpdateRankingEvent, object: nil)
    }
    
    func setRemoveObserverEvent() {
        NotificationCenter.default.removeObserver(self, name: .UserRegisterVR, object: nil)
        NotificationCenter.default.removeObserver(self, name: .VRRegistrationJustExpiredEvent, object: nil)
        NotificationCenter.default.removeObserver(self, name: .VRShouldUpdateRankingEvent, object: nil)
    }
    
    @objc func onUserRegisterVRAction(notification: NSNotification) {
        guard let race = notification.object as? RaceModel else {
            return
        }
        guard race.id == self.raceModel?.id else {
            return
        }
        self.getData()
    }
    
    @objc func onVRRegistrationJustExpiredEventAction(notification: NSNotification) {
        guard let listId = notification.object as? [Int] else {
            return
        }
        guard listId.first(where: {$0 == self.raceModel?.id}) != nil else {
            return
        }
        self.getData()
    }
    
    @objc func onVRShouldUpdateRankingEventAction(notification: NSNotification) {
        guard let listId = notification.object as? [Int] else {
            return
        }
        guard listId.first(where: {$0 == self.raceModel?.id}) != nil else {
            return
        }
        self.getData()
    }
}

// MARK: - DetailRacing InteractorOutputProtocol
extension DetailRacingViewModel: DetailRacingInteractorOutputProtocol {
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>) {
        switch result {
        case .success(let model):
            self.raceModel = model
            
        case .failure(let error):
            self.finalStatus = false
            debugPrint(error)
        }
        self.numberOfApi += 1
    }
    
    func onGetVirtualRaceLeaderboardFinished(with result: Result<[LeaderboardModel], APIError>) {
        switch result {
        case .success(let model):
            self.leaderboardModels = model
            
        case .failure(let error):
            self.finalStatus = false
            debugPrint(error)
        }
        self.numberOfApi += 1
    }
}
