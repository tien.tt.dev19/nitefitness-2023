//
//  
//  InfoDetailRacingInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol InfoDetailRacingInteractorInputProtocol {
    func getVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter)
    func getVirtualRaceDetail(raceId: Int)
}

// MARK: - Interactor Output Protocol
protocol InfoDetailRacingInteractorOutputProtocol: AnyObject {
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>, page: Int, total: Int)
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>)
}

// MARK: - InfoDetailRacing InteractorInput
class InfoDetailRacingInteractorInput {
    weak var output: InfoDetailRacingInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol!
}

// MARK: - InfoDetailRacing InteractorInputProtocol
extension InfoDetailRacingInteractorInput: InfoDetailRacingInteractorInputProtocol {
    func getVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.fitRaceSevice?.onGetListVisualRace(page: page, perPage: perPage, filterType: filterType, completion: { [weak self] result in
            self?.output?.onGetVirtualRacingFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
    
    func getVirtualRaceDetail(raceId: Int) {
        self.fitRaceSevice?.onGetVirtualRaceDetail(id: raceId, completion: { [weak self] result in
            self?.output?.onGetVirtualRaceDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
