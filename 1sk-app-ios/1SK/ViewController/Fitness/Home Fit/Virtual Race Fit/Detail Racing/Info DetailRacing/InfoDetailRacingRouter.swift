//
//  
//  InfoDetailRacingRouter.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol InfoDetailRacingRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter)
    func showInfoRegisterRaceRouter(data: RaceModel)
}

// MARK: - InfoDetailRacing Router
class InfoDetailRacingRouter {
    weak var viewController: InfoDetailRacingViewController?
    
    static func setupModule() -> InfoDetailRacingViewController {
        let viewController = InfoDetailRacingViewController()
        let router = InfoDetailRacingRouter()
        let interactorInput = InfoDetailRacingInteractorInput()
        let viewModel = InfoDetailRacingViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.fitRaceSevice = FitRaceService()
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - InfoDetailRacing RouterProtocol
extension InfoDetailRacingRouter: InfoDetailRacingRouterProtocol {
    func showInfoRegisterRaceRouter(data: RaceModel) {
        let controller = InfoRegisterRaceRouter.setupModule(data: data)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter) {
        let controller = DetailRacingRouter.setupModule(with: id, status: status)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
