//
//  
//  InfoDetailRacingViewController.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit
import TagListView
import WebKit

protocol InfoDetailRacingViewControllerDelegate: AnyObject {
    func updateContentSize(with height: CGFloat)
    func updateRaceDetailData(race: RaceModel)
    func removePreviousViewController()
}

// MARK: - ViewProtocol
protocol InfoDetailRacingViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func setShowDetailVirtualRace(race: RaceModel)
}

// MARK: - InfoDetailRacing ViewController
class InfoDetailRacingViewController: BaseViewController {
    var router: InfoDetailRacingRouterProtocol!
    var viewModel: InfoDetailRacingViewModelProtocol!
    
    var needReloadHeight = false
    
    weak var delegate: InfoDetailRacingViewControllerDelegate?
    
    @IBOutlet weak var contentScrollView: UIScrollView!
    
    @IBOutlet weak var tagListView: TagListView!
    @IBOutlet weak var prizeCollectionView: UICollectionView!
    
    @IBOutlet weak var runTimeLabel: UILabel!
    @IBOutlet weak var prizeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var startTimeDateLabel: UILabel!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var endTimeDateLabel: UILabel!
    @IBOutlet weak var registerDateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var limitLabel: UILabel!
    @IBOutlet weak var awardView: UIView!
    @IBOutlet weak var awardSeperatorView: UIView!
    
    @IBOutlet weak var web_Infomation: WKWebView!
    @IBOutlet weak var constraint_height_Webview: NSLayoutConstraint!
    
    @IBOutlet weak var view_info: UIView!
    @IBOutlet weak var view_award: UIView!
    @IBOutlet weak var view_rule: UIView!
    @IBOutlet weak var view_utilities: UIView!
    
    @IBOutlet weak var web_WebView_info: WKWebView!
    @IBOutlet weak var web_WebView_award: WKWebView!
    @IBOutlet weak var web_WebView_rule: WKWebView!
    @IBOutlet weak var web_WebView_utilities: WKWebView!
    
    @IBOutlet weak var constraint_height_Webview_Info: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_Webview_award: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_Webview_rule: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_Webview_utilities: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_inprocessListRace: NSLayoutConstraint!
    
    @IBOutlet weak var coll_VirtualRace: UICollectionView!
    @IBOutlet weak var view_inprocessListRace: UIView!
    
    private var awardRaces: [AwardRace] = [] {
        didSet {
            DispatchQueue.main.async {
                self.prizeCollectionView.reloadData()
            }
        }
    }
    
    private var isLoading = false
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.needReloadHeight {
            self.delegate?.updateContentSize(with: self.contentScrollView.contentSize.height)
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitWKWebView()
        
        self.prizeCollectionView.registerNib(ofType: PrizeCollectionViewCell.self)
        self.coll_VirtualRace.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_VirtualRace.delegate = self
        self.coll_VirtualRace.dataSource = self
        self.coll_VirtualRace.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    
        self.prizeCollectionView.delegate = self
        self.prizeCollectionView.dataSource = self
        
        self.tagListView.textFont = R.font.robotoMedium(size: 16)!
        self.tagListView.alignment = .left // possible values are [.leading, .trailing, .left, .center, .right]
    }
    
    func reloadData(raceModel: RaceModel) {
        self.tagListView.removeAllTags()
        self.web_Infomation.loadHTMLString(raceModel.information?.asHtmlStoreProductInfo ?? "", baseURL: nil)
        
        var unit = ""
        if let sport = raceModel.sport {
            unit = sport.unit ?? ""
        }
        
        if let goals = raceModel.goals {
            for goal in goals {
                self.tagListView.addTag("\(goal.goal ?? "") \(unit)")
            }
        }
        
        if let limit = raceModel.limit {
            self.limitLabel.text = "\(limit)"
        } else {
            self.limitLabel.text = "Không giới hạn"
        }
        
        if let startDate = raceModel.startAt?.toDate(.ymdhms) {
            self.startTimeLabel.text = "\(startDate.toString(.h))h\(startDate.toString(.m))"
            self.startTimeDateLabel.text = startDate.toString(.dmySlash)
        }
        
        if let endDate = raceModel.finishAt?.toDate(.ymdhms) {
            self.endTimeLabel.text = "\(endDate.toString(.h))h\(endDate.toString(.m))"
            self.endTimeDateLabel.text = endDate.toString(.dmySlash)
        }
        
        if let registerExpireAt = raceModel.registerExpireAt?.toDate(.ymdhms) {
            self.registerDateLabel.text = "\(registerExpireAt.toString(.hm)), \(registerExpireAt.toString(.dmySlash))"
        }
        
        self.locationLabel.text = raceModel.location ?? ""
        
        if let award = raceModel.awards {
            self.awardView.isHidden = award.isEmpty
            self.awardSeperatorView.isHidden = award.isEmpty
            self.awardRaces = award
            
        } else {
            self.awardView.isHidden = true
            self.awardSeperatorView.isHidden = true
        }
        
        if let raceInformation = raceModel.information {
            self.web_WebView_info.loadHTMLString(raceInformation.asHtmlStoreProductInfo ?? "", baseURL: nil)
        } else {
            self.view_info.isHidden = true
        }
        
        if let raceaward = raceModel.award {
            self.web_WebView_award.loadHTMLString(raceaward.asHtmlStoreProductInfo ?? "", baseURL: nil)
        } else {
            self.view_award.isHidden = true
        }
        
        if let raceRule = raceModel.rule {
            self.web_WebView_rule.loadHTMLString(raceRule.asHtmlStoreProductInfo ?? "", baseURL: nil)
        } else {
            self.view_rule.isHidden = true
        }
        
        if let raceUtilities = raceModel.utilities {
            self.web_WebView_utilities.loadHTMLString(raceUtilities.asHtmlStoreProductInfo ?? "", baseURL: nil)
            
        } else {
            self.view_utilities.isHidden = true
        }
    }
}

// MARK: - InfoDetailRacing ViewProtocol
extension InfoDetailRacingViewController: InfoDetailRacingViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        if self.viewModel.listVirtualRace.count == 0 {
            self.view_inprocessListRace.isHidden = true
        }
        
        UIView.transition(with: self.coll_VirtualRace, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_VirtualRace.reloadData() })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.delegate?.updateContentSize(with: self.contentScrollView.contentSize.height)
        }
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.coll_VirtualRace.insertItems(at: [IndexPath.init(row: index, section: 0)])
    }
    
    func setShowDetailVirtualRace(race: RaceModel) {
        self.router.showDetailRacingRouter(with: race.id ?? -1, status: .inprocess)
        self.delegate?.removePreviousViewController()
    }
}

//MARK: - UICollectionViewDelegate
extension InfoDetailRacingViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.onGetRaceDetail(raceID: self.viewModel.listVirtualRace[indexPath.item].id ?? -1)
    }
}

//MARK: - UICollectionViewDataSource
extension InfoDetailRacingViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.prizeCollectionView:
            return self.awardRaces.count
            
        case self.coll_VirtualRace:
            return self.viewModel.getDataInprocess().count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.prizeCollectionView:
            let cell = collectionView.dequeuCell(ofType: PrizeCollectionViewCell.self, for: indexPath)
            cell.model = self.awardRaces[indexPath.item]
            return cell
            
        case self.coll_VirtualRace:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listVirtualRace[indexPath.row]
            cell.configUI()
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension InfoDetailRacingViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case self.prizeCollectionView:
            return CGSize(width: 90, height: 120)
            
        case self.coll_VirtualRace:
            return CGSize(width: 224, height: 330)
            
        default:
            return CGSize(width: 0, height: 0)
        }
    }
}

// MARK: - WKNavigationDelegate
extension InfoDetailRacingViewController: WKNavigationDelegate {
    func setInitWKWebView() {
        [self.web_WebView_info,
         self.web_WebView_award,
         self.web_WebView_rule,
         self.web_WebView_utilities].forEach { webView in
            webView?.navigationDelegate = self
            webView?.scrollView.isScrollEnabled = false
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        switch webView {
        case self.web_WebView_info:
            self.web_WebView_info.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
                
                if complete != nil {
                    self.web_WebView_info.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                        if let `height` = height as? CGFloat {
                            self.constraint_height_Webview_Info.constant = height
                        }
                    })
                }
            })
            
            
        case self.web_WebView_award:
            self.web_WebView_award.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
                
                if complete != nil {
                    self.web_WebView_award.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                        if let `height` = height as? CGFloat {
                            self.constraint_height_Webview_award.constant = height
                        }
                    })
                }
            })
            
            
        case self.web_WebView_rule:
            self.web_WebView_rule.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
                
                if complete != nil {
                    self.web_WebView_rule.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                        if let `height` = height as? CGFloat {
                            self.constraint_height_Webview_rule.constant = height
                        }
                    })
                }
            })
            
            
        case self.web_WebView_utilities:
            self.web_WebView_utilities.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
                
                if complete != nil {
                    self.web_WebView_utilities.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                        if let `height` = height as? CGFloat {
                            self.constraint_height_Webview_utilities.constant = height
                        }
                    })
                }
            })
            
        default:
            break
        }
    }
}
