//
//  
//  InfoDetailRacingViewModel.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol InfoDetailRacingViewModelProtocol {
    func onViewDidLoad()
    func getDataInprocess() -> [RaceModel]
    func onGetRaceDetail(raceID: Int)
    
    var listVirtualRace: [RaceModel] { get set }
}

// MARK: - InfoDetailRacing ViewModel
class InfoDetailRacingViewModel {
    weak var view: InfoDetailRacingViewProtocol?
    private var interactor: InfoDetailRacingInteractorInputProtocol
    
    var listVirtualRace: [RaceModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 50
    private var isOutOfData = false

    init(interactor: InfoDetailRacingInteractorInputProtocol) {
        self.interactor = interactor
    }
}

// MARK: - InfoDetailRacing ViewModelProtocol
extension InfoDetailRacingViewModel: InfoDetailRacingViewModelProtocol {
    func onViewDidLoad() {
        self.page = 1
        
        self.view?.showHud()
        
        if page == 1 {
            self.isOutOfData = false
        }
        
        self.interactor.getVirtualRacing(page: page, perPage: perPage, filterType: .inprocess)
    }
    
    func getDataInprocess() -> [RaceModel] {
        return self.listVirtualRace
    }
    
    func onGetRaceDetail(raceID: Int) {
        self.view?.showHud()
        self.interactor.getVirtualRaceDetail(raceId: raceID)
    }
}

// MARK: - InfoDetailRacing InteractorOutputProtocol
extension InfoDetailRacingViewModel: InfoDetailRacingInteractorOutputProtocol {
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.listVirtualRace = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.listVirtualRace.append(object)
                    self.view?.onInsertRow(at: self.listVirtualRace.count - 1)
                }
            }
            
            self.page = page
            self.total = total
            
            // Set Out Of Data
            if self.listVirtualRace.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.view?.hideHud()
    }
    
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.setShowDetailVirtualRace(race: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
