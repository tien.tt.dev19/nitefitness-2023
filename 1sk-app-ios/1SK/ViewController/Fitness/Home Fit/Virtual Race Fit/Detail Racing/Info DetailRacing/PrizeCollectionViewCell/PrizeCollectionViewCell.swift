//
//  PrizeCollectionViewCell.swift
//  1SK
//
//  Created by Be More on 19/07/2022.
//

import UIKit

class PrizeCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var awardImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var model: AwardRace? {
        didSet {
            self.setData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func setData() {
        guard let model = model else {
            return
        }

        self.awardImageView.setImageWith(imageUrl: model.image ?? "")
        self.titleLabel.text = model.name
    }
}
