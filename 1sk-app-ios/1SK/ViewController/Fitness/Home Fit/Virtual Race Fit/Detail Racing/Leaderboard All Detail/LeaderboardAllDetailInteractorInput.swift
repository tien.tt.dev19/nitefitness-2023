//
//  
//  LeaderboardAllDetailInteractorInput.swift
//  1SK
//
//  Created by Thaad on 16/08/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol LeaderboardAllDetailInteractorInputProtocol {
    func getVirtualRaceLeaderboard(raceId: Int, page: Int, perPage: Int)
}

// MARK: - Interactor Output Protocol
protocol LeaderboardAllDetailInteractorOutputProtocol: AnyObject {
    func onGetVirtualRaceLeaderboardFinished(with result: Result<[LeaderboardModel], APIError>, page: Int, total: Int)
}

// MARK: - LeaderboardAllDetail InteractorInput
class LeaderboardAllDetailInteractorInput {
    weak var output: LeaderboardAllDetailInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - LeaderboardAllDetail InteractorInputProtocol
extension LeaderboardAllDetailInteractorInput: LeaderboardAllDetailInteractorInputProtocol {
    func getVirtualRaceLeaderboard(raceId: Int, page: Int, perPage: Int) {
        self.fitRaceSevice?.onGetVirtualRaceLeaderboard(raceId: raceId, customerId: nil, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetVirtualRaceLeaderboardFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
}
