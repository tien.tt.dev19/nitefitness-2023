//
//  
//  LeaderboardAllDetailRouter.swift
//  1SK
//
//  Created by Thaad on 16/08/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol LeaderboardAllDetailRouterProtocol {

}

// MARK: - LeaderboardAllDetail Router
class LeaderboardAllDetailRouter {
    weak var viewController: LeaderboardAllDetailViewController?
    
    static func setupModule(raceModel: RaceModel?) -> LeaderboardAllDetailViewController {
        let viewController = LeaderboardAllDetailViewController()
        let router = LeaderboardAllDetailRouter()
        let interactorInput = LeaderboardAllDetailInteractorInput()
        let viewModel = LeaderboardAllDetailViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.raceModel = raceModel
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - LeaderboardAllDetail RouterProtocol
extension LeaderboardAllDetailRouter: LeaderboardAllDetailRouterProtocol {
    
}
