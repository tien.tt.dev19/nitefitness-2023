//
//  
//  LeaderboardAllDetailViewController.swift
//  1SK
//
//  Created by Thaad on 16/08/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol LeaderboardAllDetailViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
}

// MARK: - LeaderboardAllDetail ViewController
class LeaderboardAllDetailViewController: BaseViewController {
    var router: LeaderboardAllDetailRouterProtocol!
    var viewModel: LeaderboardAllDetailViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = self.viewModel.raceModel?.name
        
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - LeaderboardAllDetail ViewProtocol
extension LeaderboardAllDetailViewController: LeaderboardAllDetailViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
}

// MARK: - UITableViewDataSource
extension LeaderboardAllDetailViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewGroup.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewPerson.self)

        self.tbv_TableView.delegate = self
        self.tbv_TableView.dataSource = self
        
        self.tbv_TableView.separatorStyle = .none
        self.tbv_TableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        //self.tbv_TableView.isScrollEnabled = false
        
        self.tbv_TableView.emptyDataSetSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listLeaderboard.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewPerson.self, for: indexPath)
        cell.model = self.viewModel.listLeaderboard[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension LeaderboardAllDetailViewController: UITableViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: - EmptyDataSetSource
extension LeaderboardAllDetailViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có dữ liệu")
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
}

// MARK: Refresh Control
extension LeaderboardAllDetailViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}
