//
//  
//  LeaderboardAllDetailViewModel.swift
//  1SK
//
//  Created by Thaad on 16/08/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol LeaderboardAllDetailViewModelProtocol {
    func onViewDidLoad()
    
    func onRefreshAction()
    func onLoadMoreAction()
    
    var raceModel: RaceModel? { get set}
    var listLeaderboard: [LeaderboardModel] { get set }
}

// MARK: - LeaderboardAllDetail ViewModel
class LeaderboardAllDetailViewModel {
    weak var view: LeaderboardAllDetailViewProtocol?
    private var interactor: LeaderboardAllDetailInteractorInputProtocol

    init(interactor: LeaderboardAllDetailInteractorInputProtocol) {
        self.interactor = interactor
    }

    var raceModel: RaceModel?
    var listLeaderboard: [LeaderboardModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 30
    private var isOutOfData = false
    
    func getData(page: Int) {
        guard let raceId = self.raceModel?.id else {
            return
        }
        if page == 1 {
            self.isOutOfData = false
        }
        
        self.view?.showHud()
        self.interactor.getVirtualRaceLeaderboard(raceId: raceId, page: page, perPage: self.perPage)
    }
}

// MARK: - LeaderboardAllDetail ViewModelProtocol
extension LeaderboardAllDetailViewModel: LeaderboardAllDetailViewModelProtocol {
    func onViewDidLoad() {
        self.getData(page: 1)
    }
    
    func onRefreshAction() {
        self.getData(page: 1)
    }
    
    func onLoadMoreAction() {
        if self.listLeaderboard.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
}

// MARK: - LeaderboardAllDetail InteractorOutputProtocol
extension LeaderboardAllDetailViewModel: LeaderboardAllDetailInteractorOutputProtocol {
    func onGetVirtualRaceLeaderboardFinished(with result: Result<[LeaderboardModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listLeaderboard = listModel
                self.view?.onReloadData()
                
            } else {
                for object in listModel {
                    self.listLeaderboard.append(object)
                    self.view?.onInsertRow(at: self.listLeaderboard.count - 1)
                }
            }
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listLeaderboard.count >= self.total {
                self.isOutOfData = true
            }
            
            // Set Status View Data Not Found
//            if self.listLeaderboard.count > 0 {
//                self.view?.onSetNotFoundView(isHidden: true)
//            } else {
//                self.view?.onSetNotFoundView(isHidden: false)
//            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
//            if self.listLeaderboard.count > 0 {
//                self.view?.onSetNotFoundView(isHidden: true)
//            } else {
//                self.view?.onSetNotFoundView(isHidden: false)
//            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.view?.hideHud()
    }
}
