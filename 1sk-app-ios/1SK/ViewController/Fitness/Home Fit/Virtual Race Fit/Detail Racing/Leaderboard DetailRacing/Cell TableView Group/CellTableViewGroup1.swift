//
//  CellTableViewPerson.swift
//  1SK
//
//  Created by Valerian on 19/07/2022.
//

import UIKit

class CellTableViewGroup: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
