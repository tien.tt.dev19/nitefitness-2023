//
//  CellTableViewGroup.swift
//  1SK
//
//  Created by Valerian on 19/07/2022.
//

import UIKit

class CellTableViewPerson: UITableViewCell {
    
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var lbl_Rank: UILabel!
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_FullName: UILabel!
    @IBOutlet weak var lbl_TotalValue: UILabel!

    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Distance: UILabel!
    
    var model: LeaderboardModel? {
        didSet {
            self.setDataUI()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

// MARK: - Helpers
extension CellTableViewPerson {
    private func setDataUI() {
        guard let model = self.model else {
            return
        }
        self.lbl_Rank.text = model.rank?.asString
        self.img_Avatar.setImageWith(imageUrl: model.avatar ?? "")
        self.lbl_FullName.text = model.fullName

        let distance = (model.totalDistance ?? 0) / 1000
        let roundedDistance = Float(round(distance * 100) / 100)
        self.lbl_TotalValue.text = String(roundedDistance) + "km"
        
        // Status
        if let goal = model.goal?.goal, let totalDistance = model.totalDistance {
            self.lbl_Distance.text = "Cự li: \(goal)km"
            
            let distance = Float(goal * 1000)
            if totalDistance >= distance {
                self.lbl_Status.text = "Hoàn thành"
                
                if model.customerId == gUser?.id {
                    self.lbl_Status.textColor = .white
                    self.lbl_Distance.textColor = .white
                    
                } else {
                    self.lbl_Status.textColor = UIColor(hex: "FEB11A")
                    self.lbl_Distance.textColor = R.color.subTitle()
                }
                
            } else {
                self.lbl_Status.text = "Chưa hoàn thành"
                
                if model.customerId == gUser?.id {
                    self.lbl_Status.textColor = .white
                    self.lbl_Distance.textColor = .white
                    
                } else {
                    self.lbl_Status.textColor = UIColor(hex: "C4C4C4")
                    self.lbl_Distance.textColor = R.color.subTitle()
                }
            }
            
        } else {
            self.lbl_Distance.text = "Cự li: 0km"
            self.lbl_Status.text = "Không xác định"
            
            if model.customerId == gUser?.id {
                self.lbl_Status.textColor = .white
                self.lbl_Distance.textColor = .white
                
            } else {
                self.lbl_Status.textColor = UIColor(hex: "C4C4C4")
                self.lbl_Distance.textColor = R.color.subTitle()
            }
        }
        

        // Rank
        guard let stravaId = model.customerId, let id = gUser?.id, stravaId == id else {
            if model.rank == 1 {
                self.lbl_Rank.textColor = UIColor(hex: "FE5416")
                
            } else if model.rank == 2 {
                self.lbl_Rank.textColor = UIColor(hex: "FFA552")
                
            } else if model.rank == 3 {
                self.lbl_Rank.textColor = UIColor(hex: "49C2E9")
                
            } else {
                self.lbl_Rank.textColor = R.color.subTitle()
            }
            
            self.mainView.backgroundColor = .white
            self.lbl_FullName.textColor = UIColor(hex: "232930")
            self.lbl_TotalValue.textColor = R.color.mainColor()
            self.seperatorView.isHidden = false
            return
        }
        
        self.seperatorView.isHidden = true
        self.lbl_FullName.textColor = .white
        self.lbl_TotalValue.textColor = .white
        self.mainView.backgroundColor = R.color.mainColor()
        self.lbl_Rank.textColor = .white
        
    }
}
