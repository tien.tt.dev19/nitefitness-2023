//
//  FooterSectionLeaderboard.swift
//  1SK
//
//  Created by Thaad on 19/08/2022.
//

import UIKit

protocol FooterSectionLeaderboardDelegate: AnyObject {
    func onSeeMoreAction()
}

class FooterSectionLeaderboard: UITableViewHeaderFooterView {
    weak var delegate: FooterSectionLeaderboardDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    @IBAction func onSeeMoreAction(_ sender: Any) {
        print("onSeeMoreAction")
        self.delegate?.onSeeMoreAction()
    }
}
