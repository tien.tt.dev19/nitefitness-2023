//
//  
//  LeaderboardDetailRacingInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol LeaderboardDetailRacingInteractorInputProtocol {
}

// MARK: - Interactor Output Protocol
protocol LeaderboardDetailRacingInteractorOutputProtocol: AnyObject {
}

// MARK: - LeaderboardDetailRacing InteractorInput
class LeaderboardDetailRacingInteractorInput {
    weak var output: LeaderboardDetailRacingInteractorOutputProtocol?
}

// MARK: - LeaderboardDetailRacing InteractorInputProtocol
extension LeaderboardDetailRacingInteractorInput: LeaderboardDetailRacingInteractorInputProtocol {
}
