//
//  
//  LeaderboardDetailRacingRouter.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol LeaderboardDetailRacingRouterProtocol {
    func showLeaderboardAllDetailRouter(raceModel: RaceModel?)
}

// MARK: - LeaderboardDetailRacing Router
class LeaderboardDetailRacingRouter {
    weak var viewController: LeaderboardDetailRacingViewController?
    
    static func setupModule() -> LeaderboardDetailRacingViewController {
        let viewController = LeaderboardDetailRacingViewController()
        let router = LeaderboardDetailRacingRouter()
        let interactorInput = LeaderboardDetailRacingInteractorInput()
        let viewModel = LeaderboardDetailRacingViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - LeaderboardDetailRacing RouterProtocol
extension LeaderboardDetailRacingRouter: LeaderboardDetailRacingRouterProtocol {
    func showLeaderboardAllDetailRouter(raceModel: RaceModel?) {
        let controller = LeaderboardAllDetailRouter.setupModule(raceModel: raceModel)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
