//
//  
//  LeaderboardDetailRacingViewController.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit


// MARK: - LeaderboardDetailRacingDelegate
protocol LeaderboardDetailRacingDelegate: AnyObject {
    func onSendContentHeight(height: CGFloat)
}

// MARK: - ViewProtocol
protocol LeaderboardDetailRacingViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
}

// MARK: - LeaderboardDetailRacing ViewController
class LeaderboardDetailRacingViewController: BaseViewController {
    var router: LeaderboardDetailRacingRouterProtocol!
    var viewModel: LeaderboardDetailRacingViewModelProtocol!
    weak var delegate: LeaderboardDetailRacingDelegate?
    
    @IBOutlet weak var btn_Group: UIButton!
    @IBOutlet weak var btn_Person: UIButton!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var stack_StackView: UIStackView!
    
    @IBOutlet weak var view_AllLeaderboard: UIView!
    @IBOutlet weak var stack_TableView: UIStackView!
    
    private let numberOfGroup = 0
    private let rowHeight = 70
    private let topBarHeight = 150
    
    var isPerson = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.stack_StackView.addBackground(color: UIColor(hex: "EEEEEE"))
        self.view_AllLeaderboard.isHidden = true
        
        self.setInitUITableView()
    }
    
    func reloadData(listData: [LeaderboardModel], raceModel: RaceModel?) {
        self.viewModel.listLeaderboard = listData
        self.viewModel.raceModel = raceModel
        
        self.tbv_TableView.reloadDataWithCompletion {
            let number = self.viewModel.numberOfRows() == 0 ? 4 : self.viewModel.numberOfRows()
            var contentHeight = CGFloat(self.topBarHeight + (number * self.rowHeight))
            if self.isPerson {
                contentHeight = CGFloat(self.topBarHeight + (number * self.rowHeight))
                
                let heightViewALl: CGFloat = 8 + 16
                if self.viewModel.listLeaderboard.count > 0 {
                    self.delegate?.onSendContentHeight(height: contentHeight + heightViewALl)
                    self.view_AllLeaderboard.isHidden = false
                    
                } else {
                    self.delegate?.onSendContentHeight(height: contentHeight + heightViewALl)
                    self.view_AllLeaderboard.isHidden = true
                }
                
            } else {
                contentHeight = CGFloat(self.topBarHeight + (self.numberOfGroup == 0 ? 4 : self.numberOfGroup  * self.rowHeight))
                let heightViewALl: CGFloat = 8 + 16
                self.delegate?.onSendContentHeight(height: contentHeight + heightViewALl)
            }
        }
    }
    
    // MARK: - Action
    @IBAction func onGroupTapAction(_ sender: UIButton) {
        self.isPerson = false
        self.view_AllLeaderboard.isHidden = true
        self.btn_Group.backgroundColor = UIColor(hex: "27C9CC")
        self.btn_Group.titleLabel?.textColor = .white
        
        self.btn_Person.backgroundColor = UIColor(hex: "EEEEEE")
        self.btn_Person.titleLabel?.textColor = UIColor(hex: "737678")
        self.tbv_TableView.reloadDataWithCompletion {
            let contentHeight = CGFloat(self.topBarHeight + (self.numberOfGroup == 0 ? 4 : self.numberOfGroup * self.rowHeight))
            self.delegate?.onSendContentHeight(height: contentHeight)
        }
        
    }
    
    @IBAction func onPersonTapAction(_ sender: UIButton) {
        self.isPerson = true
        self.view_AllLeaderboard.isHidden = false
        self.btn_Person.backgroundColor = UIColor(hex: "27C9CC")
        self.btn_Person.setTitleColor(.white, for: .normal)
        
        self.btn_Group.backgroundColor = UIColor(hex: "EEEEEE")
        self.btn_Group.titleLabel?.textColor = UIColor(hex: "737678")
        self.tbv_TableView.reloadDataWithCompletion {
            let contentHeight = CGFloat(self.topBarHeight + (self.viewModel.numberOfRows() == 0 ? 4 : self.viewModel.numberOfRows() * self.rowHeight))
            
            let heightViewALl: CGFloat = 8 + 16

            if self.viewModel.listLeaderboard.count > 0 {
                self.delegate?.onSendContentHeight(height: contentHeight + heightViewALl)
                self.view_AllLeaderboard.isHidden = false
                
            } else {
                self.delegate?.onSendContentHeight(height: contentHeight + heightViewALl)
                self.view_AllLeaderboard.isHidden = true
            }
        }
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
    
    @IBAction func onViewAllLeaderboardAction(_ sender: Any) {
        self.router.showLeaderboardAllDetailRouter(raceModel: self.viewModel.raceModel)
    }
    
}

// MARK: - LeaderboardDetailRacing ViewProtocol
extension LeaderboardDetailRacingViewController: LeaderboardDetailRacingViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - UITableViewDataSource
extension LeaderboardDetailRacingViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewGroup.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewPerson.self)
//        self.tbv_TableView.registerFooterNib(ofType: FooterSectionLeaderboard.self)

        self.tbv_TableView.delegate = self
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.emptyDataSetSource = self
        self.tbv_TableView.separatorStyle = .none
        self.tbv_TableView.isScrollEnabled = false
    }
    
//    // UITableView - Footer
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footer = tableView.dequeueHeaderView(ofType: FooterSectionLeaderboard.self)
//        footer.delegate = self
//        return footer
//    }
//
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        if self.isPerson == true {
//            if self.viewModel.listLeaderboard.count > 0 {
//                return 44
//            } else {
//                return 0
//            }
//        } else {
//            return 0
//        }
//    }
    
    // UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isPerson {
            return self.viewModel.numberOfRows()
        }
        return numberOfGroup
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.isPerson {
            let cell = tableView.dequeueCell(ofType: CellTableViewPerson.self, for: indexPath)
            cell.model = self.viewModel.cellForRow(at: indexPath)
            return cell
            
        } else {
            let cell = tableView.dequeueCell(ofType: CellTableViewGroup.self, for: indexPath)
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension LeaderboardDetailRacingViewController: UITableViewDelegate {
    //
}

// MARK: - EmptyDataSetSource
extension LeaderboardDetailRacingViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có dữ liệu")
    }
}

// MARK: - FooterSectionLeaderboardDelegate
extension LeaderboardDetailRacingViewController: FooterSectionLeaderboardDelegate {
    func onSeeMoreAction() {
        self.router.showLeaderboardAllDetailRouter(raceModel: self.viewModel.raceModel)
    }
}
