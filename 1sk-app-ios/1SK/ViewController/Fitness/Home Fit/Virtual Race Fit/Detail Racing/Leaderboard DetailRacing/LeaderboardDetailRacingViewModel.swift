//
//  
//  LeaderboardDetailRacingViewModel.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol LeaderboardDetailRacingViewModelProtocol {
    func onViewDidLoad()
    
     //UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> LeaderboardModel?
    
    var raceModel: RaceModel? { get set}
    var listLeaderboard: [LeaderboardModel] { get set }
}

// MARK: - LeaderboardDetailRacing ViewModel
class LeaderboardDetailRacingViewModel {
    weak var view: LeaderboardDetailRacingViewProtocol?
    private var interactor: LeaderboardDetailRacingInteractorInputProtocol
    
    init(interactor: LeaderboardDetailRacingInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var raceModel: RaceModel?
    var listLeaderboard: [LeaderboardModel] = []
}

// MARK: - LeaderboardDetailRacing ViewModelProtocol
extension LeaderboardDetailRacingViewModel: LeaderboardDetailRacingViewModelProtocol {
    func onViewDidLoad() { }
    
    func numberOfRows() -> Int {
        return self.listLeaderboard.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> LeaderboardModel? {
        return self.listLeaderboard[indexPath.row]
    }
}

// MARK: - LeaderboardDetailRacing InteractorOutputProtocol
extension LeaderboardDetailRacingViewModel: LeaderboardDetailRacingInteractorOutputProtocol {
    
}
