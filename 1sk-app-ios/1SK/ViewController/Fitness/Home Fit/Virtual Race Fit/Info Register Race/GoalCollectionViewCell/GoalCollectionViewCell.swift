//
//  GoalCollectionViewCell.swift
//  1SK
//
//  Created by TrungDN on 20/07/2022.
//

import UIKit

protocol GoalCollectionViewCellDelegate: AnyObject {
    func selectedGoal(with id: Int)
}

class GoalCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var view_content: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    weak var delegate: GoalCollectionViewCellDelegate?
    
    override var isSelected: Bool {
        didSet {
            self.lbl_Title.textColor = isSelected ? UIColor.white : UIColor(hex: "232930")
            self.view_content.backgroundColor = isSelected ? R.color.mainColor() : UIColor.white
            guard let model = self.model, let id = model.id else { return }
            self.delegate?.selectedGoal(with: id)
        }
    }
    
    var unit: String = ""
    
    var model: GoalRace? {
        didSet {
            guard let model = model else {
                return
            }
            self.lbl_Title.text = "\(model.goal ?? "") \(unit)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
