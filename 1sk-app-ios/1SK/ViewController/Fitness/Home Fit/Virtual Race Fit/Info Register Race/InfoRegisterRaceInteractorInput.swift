//
//  
//  InfoRegisterRaceInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol InfoRegisterRaceInteractorInputProtocol {
    func setRegister(register: RegisterParam)
}

// MARK: - Interactor Output Protocol
protocol InfoRegisterRaceInteractorOutputProtocol: AnyObject {
    func onSetRegisterFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - InfoRegisterRace InteractorInput
class InfoRegisterRaceInteractorInput {
    var fitRaceSevice: FitRaceSeviceProtocol?
    weak var output: InfoRegisterRaceInteractorOutputProtocol?
}

// MARK: - InfoRegisterRace InteractorInputProtocol
extension InfoRegisterRaceInteractorInput: InfoRegisterRaceInteractorInputProtocol {
    func setRegister(register: RegisterParam) {
        self.fitRaceSevice?.onRegister(register: register, completion: { [weak self] result in
            guard let `self` = self else { return }
            self.output?.onSetRegisterFinished(with: result.unwrapSuccessModel())
        })
    }
}
