//
//  
//  InfoRegisterRaceRouter.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol InfoRegisterRaceRouterProtocol {
    func showConnectTrackingApp(with banner: [RaceBannersModel])
}

// MARK: - InfoRegisterRace Router
class InfoRegisterRaceRouter {
    weak var viewController: InfoRegisterRaceViewController?
    
    static func setupModule(data: RaceModel) -> InfoRegisterRaceViewController {
        let viewController = InfoRegisterRaceViewController()
        let router = InfoRegisterRaceRouter()
        let interactorInput = InfoRegisterRaceInteractorInput()
        interactorInput.fitRaceSevice = FitRaceService()
        let viewModel = InfoRegisterRaceViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.raceModel = data
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - InfoRegisterRace RouterProtocol
extension InfoRegisterRaceRouter: InfoRegisterRaceRouterProtocol {
    func showConnectTrackingApp(with banner: [RaceBannersModel]) {
        let controller = ConnectTrackingAppRouter.setupModule(with: banner)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
