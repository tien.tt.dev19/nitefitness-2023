//
//  
//  InfoRegisterRaceViewController.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit
import DLRadioButton
import IQKeyboardManagerSwift

struct RegisterParam {
    var raceId: Int
    var goalId: Int
    var name: String
    var birthDay: String
    var phone: String
    var gender: Int
    var address: String
    var email: String
    
    func toDictionary() -> [String: Any] {
        var json: [String: Any] = [:]
        json["virtual_race_id"] = self.raceId
        json["full_name"] = self.name
        
        if !String.isNilOrEmpty(self.birthDay) {
            json["birthday"] = self.birthDay
        }
        
        if !String.isNilOrEmpty(self.phone) {
            json["phone_number"] = self.phone
        }
       
        if !String.isNilOrEmpty(self.address) {
            json["address"] = self.address
        }

        if !String.isNilOrEmpty(self.email) {
            json["email"] = self.email
        }
        
        json["gender"] = self.gender
        
        if self.goalId != -1 {
            json["goal_id"] = self.goalId
        }

        return json
    }
}

// MARK: - ViewProtocol
protocol InfoRegisterRaceViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func gotoStavaConnect(with banner: [RaceBannersModel])
}

// MARK: - InfoRegisterRace ViewController
class InfoRegisterRaceViewController: BaseViewController {
    var router: InfoRegisterRaceRouterProtocol!
    var viewModel: InfoRegisterRaceViewModelProtocol!
    
    var userRegisterParam: RegisterParam = RegisterParam(raceId: -1, goalId: -1, name: "", birthDay: "", phone: "", gender: -1, address: "", email: "")
    
    var unit = ""
    
    var goals: [GoalRace] = [] {
        didSet {
            self.coll_goal.reloadData()
        }
    }
    
    @IBOutlet weak var img_ImageVR: UIImageView!
    @IBOutlet weak var lbl_RVTitle: UILabel!
    @IBOutlet weak var lbl_RaceDescription: UILabel!
    
    @IBOutlet weak var coll_goal: UICollectionView!
    @IBOutlet weak var lbl_GroupCreatedGuide: UILabel!
    @IBOutlet weak var img_Arrow: UIImageView!
    
    @IBOutlet weak var tf_Name: SKTextField!
    @IBOutlet weak var tf_Birthday: SKTextField!
    @IBOutlet weak var tf_Phone: SKTextField!
    @IBOutlet weak var tf_Email: SKTextField!
    @IBOutlet weak var tf_Address: SKTextField!
    
    @IBOutlet weak var btn_Male: DLRadioButton!
    @IBOutlet weak var btn_Female: DLRadioButton!
    @IBOutlet weak var lbl_EmptyName: UILabel!
    @IBOutlet weak var lbl_EmptyPhone: UILabel!
    @IBOutlet weak var lbl_EmptyEmail: UILabel!
    @IBOutlet weak var lbl_EmptyGoal: UILabel!
    @IBOutlet weak var lbl_ChoseGoal: UILabel!
    
    private var sk_tf_Name: UITextField {
        return self.tf_Name.titleTextField
    }
    
    private var sk_tf_Birthday: UITextField {
        return self.tf_Birthday.titleTextField
    }
    
    private var sk_tf_Phone: UITextField {
        return self.tf_Phone.titleTextField
    }
    
    private var sk_tf_Email: UITextField {
        return self.tf_Email.titleTextField
    }
    
    private var sk_tf_Address: UITextField {
        return self.tf_Address.titleTextField
    }
    
    @IBOutlet weak var view_Group: UIView!
    @IBOutlet weak var groupViewHeightConstriant: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.setInitUITextField()
        self.setInitRadioButton()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view_Group.addShadow(width: 0, height: 4, color: .black, radius: 8, opacity: 0.25)
    }
    
    @IBAction func onShowGroups(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseIn) {
            if self.groupViewHeightConstriant.constant == 0 {
                self.groupViewHeightConstriant.constant = 252
            } else {
                self.groupViewHeightConstriant.constant = 0
            }
            
            let radians = atan2f(Float(self.img_Arrow.transform.b), Float(self.img_Arrow.transform.a))
            let degrees = radians * (180 / .pi)
            let transform = CGAffineTransform(rotationAngle: Double((180 + degrees)) * .pi / 180)
            self.img_Arrow.transform = transform
            
            self.view.layoutIfNeeded()
        } completion: { finished in
        }

    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Thông tin đăng ký"
        
        let atributeGoal_0 = NSAttributedString(string: "Chọn thử thách", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "737678"), NSAttributedString.Key.font: R.font.robotoRegular(size: 16)!])
        let atributeGoal_1 = NSAttributedString(string: "*", attributes: [
            NSAttributedString.Key.font: R.font.robotoRegular(size: 16)!,
            NSAttributedString.Key.foregroundColor: UIColor(hex: "FF3030")])
        let atributeGoal = NSMutableAttributedString(attributedString: atributeGoal_0)
        atributeGoal.append(atributeGoal_1)
        self.lbl_ChoseGoal.attributedText = atributeGoal
        
        self.coll_goal.delegate = self
        self.coll_goal.dataSource = self
        self.coll_goal.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_goal.emptyDataSetSource = self
        self.coll_goal.registerNib(ofType: GoalCollectionViewCell.self)
        
        if let raceDetailData = self.viewModel.onGetRaceDetailData() {
            self.img_ImageVR.setImageWith(imageUrl: raceDetailData.thumbnail ?? "")
            self.lbl_RaceDescription.text = raceDetailData.name ?? ""
            
            if let sport = raceDetailData.sport {
                self.lbl_RVTitle.text = sport.name?.uppercased()
                self.unit = sport.unit ?? ""
            }
            
            if let raceGoals = raceDetailData.goals {
                self.goals = raceGoals
                if raceGoals.count == 1 {
                    self.coll_goal.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredHorizontally)
                    self.userRegisterParam.goalId = raceGoals[0].id ?? 0
                }
            } else {
                self.goals = []
            }
            
            self.userRegisterParam.raceId = raceDetailData.id ?? 0
        }
        
        let atributeLogin_0 = NSAttributedString(string: "Bạn có thể tạo nhóm và mời các thành viên khác vào nhóm để tham gia giải đua.", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor(hex: "232930"), NSAttributedString.Key.font: R.font.robotoRegular(size: 16)!])
        
        let atributeLogin_1 = NSAttributedString(string: "Tạo nhóm", attributes: [
            NSAttributedString.Key.font: R.font.robotoRegular(size: 16)!,
            NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.black])
       
        let atributeLogin = NSMutableAttributedString(attributedString: atributeLogin_0)
        atributeLogin.append(atributeLogin_1)
        
        self.lbl_GroupCreatedGuide.attributedText = atributeLogin
        self.lbl_GroupCreatedGuide.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCreateGroupSelected(_:))))
                
        self.sk_tf_Name.text = gUser?.fullName ?? ""
        self.sk_tf_Birthday.text = gUser?.birthday ?? ""
        self.sk_tf_Phone.text = gUser?.phoneNumber ?? ""
        self.sk_tf_Email.text = gUser?.email ?? ""
        self.sk_tf_Address.text = gUser?.address ?? ""
        
        self.userRegisterParam.name = gUser?.fullName ?? ""
        self.userRegisterParam.gender = gUser?.gender?.id ?? 0
        self.userRegisterParam.birthDay = gUser?.birthday ?? ""
        self.userRegisterParam.phone = gUser?.phoneNumber ?? ""
        self.userRegisterParam.email = gUser?.email ?? ""
        self.userRegisterParam.address = gUser?.address ?? ""
        
        switch gUser?.gender {
        case .male:
            self.btn_Male.isSelected = true
            self.btn_Female.isSelected = false
            
        case .female:
            self.btn_Male.isSelected = false
            self.btn_Female.isSelected = true
            
        default:
            self.btn_Male.isSelected = false
            self.btn_Female.isSelected = false
        }
        
        for textField in [self.sk_tf_Name, self.sk_tf_Birthday, self.sk_tf_Phone, self.sk_tf_Email, self.sk_tf_Address] {
            self.updateTypeLabelHiddenState(of: textField)
        }
    }
    
    @objc private func onCreateGroupSelected(_ sender: UITapGestureRecognizer) {
        print("Create groups")
    }
    
    func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
    
    // MARK: - Action
    
    @IBAction func onEditInfoAction(_ sender: Any) {
//        self.presentAlertEditInfoRegisterRaceRouter()
    }
    
    @IBAction func onMoveToConnectTrackingApp(_ sender: Any) {
        var finalStatus = true
        
        if self.userRegisterParam.goalId == -1 {
            self.lbl_EmptyGoal.isHidden = false
            finalStatus = false
        }
        
        if self.userRegisterParam.name.trimmingCharacters(in: .whitespacesAndNewlines).count < 2 || self.userRegisterParam.name.trimmingCharacters(in: .whitespacesAndNewlines).count > 32 {
            self.tf_Name.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_EmptyName.isHidden = false
            finalStatus = false
        }
        
        
        if String.isNilOrEmpty(self.userRegisterParam.email.trimmingCharacters(in: .whitespacesAndNewlines)) {
            self.tf_Email.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_EmptyEmail.isHidden = false
            self.lbl_EmptyEmail.text = "Vui lòng nhập thông tin"
            finalStatus = false
        } else if let isValidateEmail = self.userRegisterParam.email.trimmingCharacters(in: .whitespacesAndNewlines).isValidEmail() {
            if !isValidateEmail {
                self.tf_Email.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
                self.lbl_EmptyEmail.isHidden = false
                self.lbl_EmptyEmail.text = "Địa chỉ email không đúng (gợi ý: email@domain.com)"
                finalStatus = false
            }
        }
        
        if String.isNilOrEmpty(self.userRegisterParam.phone) {
            self.tf_Phone.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_EmptyPhone.isHidden = false
            self.lbl_EmptyPhone.text = "Vui lòng nhập thông tin"
            finalStatus = false
        } else if self.userRegisterParam.phone.count < 10 || self.userRegisterParam.phone.first != "0" {
            self.tf_Phone.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_EmptyPhone.isHidden = false
            self.lbl_EmptyPhone.text = "Số điện thoại không hợp lệ"
            finalStatus = false
        }
        
        guard self.userRegisterParam.gender < 2 else {
            finalStatus = false
            return
        }

        if finalStatus {
            self.viewModel.onRegister(with: self.userRegisterParam)
        }
    }
    
    @IBAction func onGroupCreateAction(_ sender: Any) {
        print("Handle Group Create")
    }
}

// MARK: - InfoRegisterRace ViewProtocol
extension InfoRegisterRaceViewController: InfoRegisterRaceViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func gotoStavaConnect(with banner: [RaceBannersModel]) {
        self.router.showConnectTrackingApp(with: banner)
    }
}

// MARK: UICollectionViewDelegate
extension InfoRegisterRaceViewController: UICollectionViewDelegate {
    
}

// MARK: UICollectionViewDataSource
extension InfoRegisterRaceViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.goals.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: GoalCollectionViewCell.self, for: indexPath)
        cell.unit = self.unit
        cell.model = self.goals[indexPath.item]
        cell.delegate = self
        return cell
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension InfoRegisterRaceViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 106, height: 44)
    }
}

// MARK: GoalCollectionViewCellDelegate
extension InfoRegisterRaceViewController: GoalCollectionViewCellDelegate {
    func selectedGoal(with id: Int) {
        self.lbl_EmptyGoal.isHidden = true
        self.userRegisterParam.goalId = id
    }
}

// MARK: EmptyDataSetSource
extension InfoRegisterRaceViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có dữ liệu")
    }
}

// MARK: UITextFieldDelegate
extension InfoRegisterRaceViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.sk_tf_Name.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Phone.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Email.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Address.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        
        self.tf_Name.setContentType(.name)
        self.tf_Name.setKeyboardType(.alphabet)
        self.tf_Phone.setContentType(.telephoneNumber)
        self.tf_Phone.setKeyboardType(.phonePad)
        
        self.sk_tf_Name.delegate = self
        self.sk_tf_Birthday.delegate = self
        self.sk_tf_Phone.delegate = self
        self.sk_tf_Email.delegate = self
        self.sk_tf_Address.delegate = self

        self.tf_Name.setEnable(isEnable: true)
        self.tf_Birthday.setEnable(isEnable: true)
        self.tf_Phone.setEnable(isEnable: true)
        self.tf_Address.setEnable(isEnable: true)
        self.tf_Email.setEnable(isEnable: true)
        
        self.tf_Name.updateTypeLabelHiddenState()
        self.tf_Birthday.updateTypeLabelHiddenState()
        self.tf_Phone.updateTypeLabelHiddenState()
        self.tf_Address.updateTypeLabelHiddenState()
        self.tf_Email.updateTypeLabelHiddenState()
        
        self.sk_tf_Name.clearButtonMode = .whileEditing
        self.sk_tf_Birthday.clearButtonMode = .whileEditing
        self.sk_tf_Address.clearButtonMode = .whileEditing
        self.sk_tf_Email.clearButtonMode = .whileEditing
        self.sk_tf_Phone.clearButtonMode = .whileEditing
    }
    
    @objc func onTextFieldDidChange(_ textField: UITextField) {
        let newText = textField.text ?? ""
        
        print("onTextFieldDidChange newText:", newText)
        
        switch textField {
        case self.sk_tf_Name:
            if !String.isNilOrEmpty(newText) {
                self.tf_Name.bgView.borderColor = UIColor(hex: "D3DBE3")
                self.lbl_EmptyName.isHidden = true
            }
            self.userRegisterParam.name = newText
        case self.sk_tf_Phone:
            if !String.isNilOrEmpty(newText) {
                self.tf_Phone.bgView.borderColor = UIColor(hex: "D3DBE3")
                self.lbl_EmptyPhone.isHidden = true
            }
            self.userRegisterParam.phone = newText
        case self.sk_tf_Email:
            if !String.isNilOrEmpty(newText) {
                self.tf_Email.bgView.borderColor = UIColor(hex: "D3DBE3")
                self.lbl_EmptyEmail.isHidden = true
            }
            self.userRegisterParam.email = newText
        case self.sk_tf_Address:
            self.userRegisterParam.address = newText
        default:
            break
        }
    }
    
    private func updateTypeLabelHiddenState(of textField: UITextField) {
        switch textField {
        case self.sk_tf_Name:
            self.tf_Name.updateTypeLabelHiddenState()
            
        case self.sk_tf_Birthday:
            self.tf_Birthday.updateTypeLabelHiddenState()
            
        case self.sk_tf_Address:
            self.tf_Address.updateTypeLabelHiddenState()
            
        case self.sk_tf_Email:
            self.tf_Email.updateTypeLabelHiddenState()

        case self.sk_tf_Phone:
            self.tf_Phone.updateTypeLabelHiddenState()

        default:
            break
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case self.sk_tf_Birthday:
            self.view.endEditing(true)
            self.showAlertCalendarDateViewAction()
            return false
        default:
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.sk_tf_Phone:
            if textField.text?.count ?? 0 < 3 {
                return true
                
            } else {
                let text0 = textField.text?.first
                switch text0 {
                case "0":
                    if textField.text?.count ?? 0 < 10 {
                        return true
                    } else {
                        return false
                    }
                    
                case "+":
                    if textField.text?.count ?? 0 < 12 {
                        return true
                    } else {
                        return false
                    }
                    
                default:
                    if textField.text?.count ?? 0 < 12 {
                        return true
                    } else {
                        return false
                    }
                }
            }
            
        case self.sk_tf_Name:
            if textField.text?.count ?? 0 < 32 {
                return true
            } else {
                return false
            }
        default:
            return true
        }
    }
}

// MARK: - InitRadioButton
extension InfoRegisterRaceViewController {
    private func setInitRadioButton() {
        self.btn_Male.addTarget(self, action: #selector(self.onMaleAction), for: .touchUpInside)
        self.btn_Female.addTarget(self, action: #selector(self.onFemaleAction), for: .touchUpInside)
    }

    @objc private func onMaleAction() {
        self.userRegisterParam.gender = 0
        
        self.btn_Male.isSelected = true
        self.btn_Female.isSelected = false
    }

    @objc private func onFemaleAction() {
        self.userRegisterParam.gender = 1
        
        self.btn_Male.isSelected = false
        self.btn_Female.isSelected = true
    }
}

// MARK: - AlertCalendarDateViewDelegate
extension InfoRegisterRaceViewController: AlertDatePickerViewDelegate {
    func showAlertCalendarDateViewAction() {
        let controller = AlertDatePickerViewController()
        controller.modalPresentationStyle = .custom
        controller.alertTitle = "Chọn ngày sinh"
        controller.dateSelect = self.sk_tf_Birthday.text ?? ""
        controller.delegate = self

        self.present(controller, animated: true)
    }
    
    func onAlertDatePickerCancelAction() {
        //
    }
    
    func onAlertDatePickerAgreeAction(day: String) {
        self.sk_tf_Birthday.text = day
        self.tf_Birthday.updateTypeLabelHiddenState()
        self.userRegisterParam.birthDay = day
    }
}
