//
//  
//  InfoRegisterRaceViewModel.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol InfoRegisterRaceViewModelProtocol {
    func onViewDidLoad()
    func onGetRaceDetailData() -> RaceModel?
    
    func onRegister(with registerParam: RegisterParam)
}

// MARK: - InfoRegisterRace ViewModel
class InfoRegisterRaceViewModel {
    weak var view: InfoRegisterRaceViewProtocol?
    private var interactor: InfoRegisterRaceInteractorInputProtocol
    
    var raceModel: RaceModel?
    
    init(interactor: InfoRegisterRaceInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - InfoRegisterRace ViewModelProtocol
extension InfoRegisterRaceViewModel: InfoRegisterRaceViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onGetRaceDetailData() -> RaceModel? {
        return self.raceModel
    }
    
    func onRegister(with registerParam: RegisterParam) {
        self.view?.showHud()
        self.interactor.setRegister(register: registerParam)
    }
}

// MARK: - InfoRegisterRace InteractorOutputProtocol
extension InfoRegisterRaceViewModel: InfoRegisterRaceInteractorOutputProtocol {
    func onSetRegisterFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            self.view?.gotoStavaConnect(with: self.raceModel?.banners ?? [])
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
}
