//
//  
//  RacingFitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol RacingFitInteractorInputProtocol {
    func getRegisteredVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter)
    func getVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter)
}

// MARK: - Interactor Output Protocol
protocol RacingFitInteractorOutputProtocol: AnyObject {
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>, page: Int, total: Int)
    func onGetRegisteredVirtualRacing(with result: Result<[RaceModel], APIError>, page: Int, total: Int)
}

// MARK: - RacingFit InteractorInput
class RacingFitInteractorInput {
    weak var output: RacingFitInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - RacingFit InteractorInputProtocol
extension RacingFitInteractorInput: RacingFitInteractorInputProtocol {
    func getRegisteredVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.fitRaceSevice?.onGetListVisualRaceRegistered(page: page, perPage: perPage, filterType: filterType, completion: { [weak self] result in
            self?.output?.onGetRegisteredVirtualRacing(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
    
    func getVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.fitRaceSevice?.onGetListVisualRace(page: page, perPage: perPage, filterType: filterType, completion: { [weak self] result in
            self?.output?.onGetVirtualRacingFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
}
