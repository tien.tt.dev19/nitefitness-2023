//
//  
//  RacingFitRouter.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol RacingFitRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter)
}

// MARK: - RacingFit Router
class RacingFitRouter {
    weak var viewController: RacingFitViewController?
    
    static func setupModule(typeRace: TypeRaceFilter) -> RacingFitViewController {
        let viewController = RacingFitViewController()
        let router = RacingFitRouter()
        let interactorInput = RacingFitInteractorInput()
        let viewModel = RacingFitViewModel(interactor: interactorInput)
        viewModel.typeRace = typeRace
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - RacingFit RouterProtocol
extension RacingFitRouter: RacingFitRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter) {
        let controller = DetailRacingRouter.setupModule(with: id, status: status)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
