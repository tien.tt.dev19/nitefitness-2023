//
//  
//  RacingFitViewController.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol RacingFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
}

// MARK: - RacingFit ViewController
class RacingFitViewController: BaseViewController {
    var router: RacingFitRouterProtocol!
    var viewModel: RacingFitViewModelProtocol!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    @IBOutlet weak var coll_Filter: UICollectionView!
    @IBOutlet weak var coll_VirtualRace: UICollectionView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewModel.onViewDidDisappear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitStateUI()
        self.setInitUICollectionView()
        self.setInitRefreshControl()
    }
    
    func setInitStateUI() {
        self.title = self.viewModel.typeRace?.headerTitle
        self.coll_Filter.isHidden = self.viewModel.typeRace != .registered
    }
}

// MARK: - RacingFit ViewProtocol
extension RacingFitViewController: RacingFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        UIView.transition(with: self.coll_VirtualRace, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_VirtualRace.reloadData() })
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.coll_VirtualRace.insertItems(at: [IndexPath.init(row: index, section: 0)])
    }
}

// MARK: Refresh Control
extension RacingFitViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.coll_VirtualRace.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: - UICollectionViewDataSource
extension RacingFitViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_Filter.registerNib(ofType: CellCollectionViewFilterRace.self)
        self.coll_Filter.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Filter.delegate = self
        self.coll_Filter.dataSource = self
        self.coll_Filter.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        
        self.coll_VirtualRace.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_VirtualRace.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        self.coll_VirtualRace.delegate = self
        self.coll_VirtualRace.dataSource = self
        self.coll_VirtualRace.emptyDataSetSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.coll_Filter:
            return self.viewModel.listFilterRace.count
            
        case self.coll_VirtualRace:
            return self.viewModel.listVirtualRace.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.coll_Filter:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewFilterRace.self, for: indexPath)
            cell.model = self.viewModel.listFilterRace[indexPath.row]
            return cell
            
        case self.coll_VirtualRace:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listVirtualRace[indexPath.row]
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension RacingFitViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case self.coll_Filter:
            let padding: CGFloat = 12
            let textSize = self.viewModel.listFilterRace[indexPath.item].filterTitle.width(withConstrainedHeight: 17, font: R.font.robotoRegular(size: 14)!)
            let cellWidth = padding * 2 + textSize
            return CGSize(width: cellWidth, height: 32)
            
        case self.coll_VirtualRace:
            let padding: CGFloat = 16
            let cellWidth = collectionView.frame.width - padding * 2
            let cellHeight: CGFloat = 325
            return CGSize(width: cellWidth, height: cellHeight)
            
        default:
            return .zero
        }
    }
}

// MARK: - UICollectionViewDelegate
extension RacingFitViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.coll_Filter:
            collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            self.viewModel.typeFilterCurrent = self.viewModel.listFilterRace[indexPath.row]
            
        case self.coll_VirtualRace:
            guard let raceId = self.viewModel.listVirtualRace[indexPath.row].id else {
                return
            }
            self.router.showDetailRacingRouter(with: raceId, status: self.viewModel.typeFilterCurrent)
            
        default:
            break
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.coll_Filter {
            return
        }
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: - EmptyDataSetSource
extension RacingFitViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có dữ liệu")
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_cup")
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
}
