//
//  
//  RacingFitViewModel.swift
//  1SK
//
//  Created by Thaad on 13/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol RacingFitViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onViewDidDisappear()
    
    func onRefreshAction()
    func onLoadMoreAction()
    
    var listFilterRace: [TypeRaceFilter] { get set }
    var listVirtualRace: [RaceModel] { get set }
    
    var typeRace: TypeRaceFilter? { get set }
    var typeFilterCurrent: TypeRaceFilter { get set }
}

// MARK: - RacingFit ViewModel
class RacingFitViewModel {
    weak var view: RacingFitViewProtocol?
    private var interactor: RacingFitInteractorInputProtocol
    
    init(interactor: RacingFitInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listFilterRace: [TypeRaceFilter] = [.all, .inprocess, .upcoming, .ended]
    var listVirtualRace: [RaceModel] = []
    
    var typeRace: TypeRaceFilter?
    var typeFilterCurrent: TypeRaceFilter = .all {
        didSet {
            self.getData(page: 1)
        }
    }
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 20
    private var isOutOfData = false
    
    func getData(page: Int) {
        self.view?.showHud()

        if page == 1 {
            self.isOutOfData = false
        }
                
        if self.typeRace == .registered {
            self.interactor.getRegisteredVirtualRacing(page: page, perPage: self.perPage, filterType: self.typeFilterCurrent)

        } else {
            guard let type = self.typeRace else { return }
            self.interactor.getVirtualRacing(page: page, perPage: perPage, filterType: type)
        }
    }
}

// MARK: - RacingFit ViewModelProtocol
extension RacingFitViewModel: RacingFitViewModelProtocol {
    func onViewDidLoad() {
        self.getData(page: 1)
    }
    
    func onViewDidAppear() {
        self.setInitObserverEvent()
    }
    
    func onViewDidDisappear() {
        self.setRemoveObserverEvent()
    }
    
    func onRefreshAction() {
        self.getData(page: 1)
    }
    
    func onLoadMoreAction() {
        if self.listVirtualRace.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)
            
        } else {
            //SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
}

// MARK: HandleSocketEvent
extension RacingFitViewModel {
    func setInitObserverEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUserRegisterVRAction), name: .UserRegisterVR, object: nil)
    }
    
    func setRemoveObserverEvent() {
        NotificationCenter.default.removeObserver(self, name: .UserRegisterVR, object: nil)
    }
    
    @objc func onUserRegisterVRAction(notification: NSNotification) {
        guard self.typeRace == .opened else {
            return
        }
        guard let race = notification.object as? RaceModel else {
            return
        }
        guard self.listVirtualRace.first(where: {$0.id == race.id}) != nil else {
            return
        }
        self.getData(page: 1)
    }
}

// MARK: - RacingFit InteractorOutputProtocol
extension RacingFitViewModel: RacingFitInteractorOutputProtocol {
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.listVirtualRace = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.listVirtualRace.append(object)
                    self.view?.onInsertRow(at: self.listVirtualRace.count - 1)
                }
            }
            
            self.page = page
            self.total = total
            
            // Set Out Of Data
            if self.listVirtualRace.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.view?.hideHud()
    }
    
    func onGetRegisteredVirtualRacing(with result: Result<[RaceModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                switch self.typeRace {
                case .registered:
                    switch self.typeFilterCurrent {
                    case .all:
                        self.listVirtualRace = model
                        
                    default:
                        self.listVirtualRace = model.filter({$0.typeRace == self.typeFilterCurrent})
                    }
                default:
                    self.listVirtualRace = model
                }
                self.view?.onReloadData()

            } else {
                for object in model {
                    switch self.typeRace {
                    case .registered:
                        switch self.typeFilterCurrent {
                        case .all:
                            self.listVirtualRace.append(object)
                            
                        default:
                            if object.typeRace == self.typeFilterCurrent {
                                self.listVirtualRace.append(object)
                            }
                        }
                    default:
                        self.listVirtualRace.append(object)
                    }
                    self.view?.onInsertRow(at: self.listVirtualRace.count - 1)
                }
            }

            self.total = total
            self.page = page

            // Set Out Of Data
            if self.listVirtualRace.count >= self.total {
                self.isOutOfData = true
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.view?.hideHud()
    }
}
