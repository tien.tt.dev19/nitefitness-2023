//
//  
//  VirtualRaceFitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol VirtualRaceFitInteractorInputProtocol {
    func getVirtualRacingRegistered(page: Int, perPage: Int, filterType: TypeRaceFilter)
    func getVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter)
    func getVirtualRaceDetail(raceId: Int)
}

// MARK: - Interactor Output Protocol
protocol VirtualRaceFitInteractorOutputProtocol: AnyObject {
    func onGetVirtualRacingRegisteredFinished(with result: Result<[RaceModel], APIError>, filterType: TypeRaceFilter)
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>, filterType: TypeRaceFilter)
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>)
}

// MARK: - VirtualRaceFit InteractorInput
class VirtualRaceFitInteractorInput {
    weak var output: VirtualRaceFitInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - VirtualRaceFit InteractorInputProtocol
extension VirtualRaceFitInteractorInput: VirtualRaceFitInteractorInputProtocol {
    func getVirtualRacingRegistered(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.fitRaceSevice?.onGetListVisualRaceRegistered(page: page, perPage: perPage, filterType: filterType, completion: { [weak self] result in
            self?.output?.onGetVirtualRacingRegisteredFinished(with: result.unwrapSuccessModel(), filterType: filterType)
        })
    }
    
    func getVirtualRacing(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.fitRaceSevice?.onGetListVisualRace(page: page, perPage: perPage, filterType: filterType, completion: { [weak self] result in
            self?.output?.onGetVirtualRacingFinished(with: result.unwrapSuccessModel(), filterType: filterType)
        })
    }
    
    func getVirtualRaceDetail(raceId: Int) {
        self.fitRaceSevice?.onGetVirtualRaceDetail(id: raceId, completion: { [weak self] result in
            self?.output?.onGetVirtualRaceDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
