//
//  
//  VirtualRaceFitRouter.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol VirtualRaceFitRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter)
    func showRacingFitRouter(typeRace: TypeRaceFilter)
}

// MARK: - VirtualRaceFit Router
class VirtualRaceFitRouter {
    weak var viewController: VirtualRaceFitViewController?
    
    static func setupModule() -> VirtualRaceFitViewController {
        let viewController = VirtualRaceFitViewController()
        let router = VirtualRaceFitRouter()
        let interactorInput = VirtualRaceFitInteractorInput()
        let viewModel = VirtualRaceFitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - VirtualRaceFit RouterProtocol
extension VirtualRaceFitRouter: VirtualRaceFitRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter) {
        let controller = DetailRacingRouter.setupModule(with: id, status: status)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showRacingFitRouter(typeRace: TypeRaceFilter) {
        let controller = RacingFitRouter.setupModule(typeRace: typeRace)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
