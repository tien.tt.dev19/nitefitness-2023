//
//  
//  VirtualRaceFitViewController.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol VirtualRaceFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData(filterType: TypeRaceFilter)
    func setShowDetailVirtualRace(race: RaceModel?)
}

// MARK: - VirtualRaceFit ViewController
class VirtualRaceFitViewController: BaseViewController {
    var router: VirtualRaceFitRouterProtocol!
    var viewModel: VirtualRaceFitViewModelProtocol!
    
    @IBOutlet weak var view_Registered: UIView!
    @IBOutlet weak var view_Inprocess: UIView!
    @IBOutlet weak var view_Upcoming: UIView!
    @IBOutlet weak var view_Opened: UIView!
    @IBOutlet weak var view_Ended: UIView!
    
    @IBOutlet weak var coll_Filter: UICollectionView!
    @IBOutlet weak var coll_Registered: UICollectionView!
    @IBOutlet weak var coll_Inprocess: UICollectionView!
    @IBOutlet weak var coll_Upcoming: UICollectionView!
    @IBOutlet weak var coll_Opened: UICollectionView!
    @IBOutlet weak var coll_Ended: UICollectionView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillApear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.viewModel.onViewDidDisappear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Giải đua ảo"
        self.setInitUICollectionView()
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
    
    @IBAction func onViewAllMyRace(_ sender: UIButton) {
        self.router.showRacingFitRouter(typeRace: .registered)
    }
    
    @IBAction func onViewAllInprogress(_ sender: UIButton) {
        self.router.showRacingFitRouter(typeRace: .inprocess)
    }
    
    @IBAction func onViewAllUpcomming(_ sender: UIButton) {
        self.router.showRacingFitRouter(typeRace: .upcoming)
    }

    @IBAction func onViewAllEnded(_ sender: UIButton) {
        self.router.showRacingFitRouter(typeRace: .ended)
    }
    
    @IBAction func onViewAllOpened(_ sender: UIButton) {
        self.router.showRacingFitRouter(typeRace: .opened)
    }
    
}

// MARK: - VirtualRaceFit ViewProtocol
extension VirtualRaceFitViewController: VirtualRaceFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData(filterType: TypeRaceFilter) {
        switch filterType {
        case .registered:
            if self.viewModel.listRaceRegistered.count > 0 {
                self.view_Registered.isHidden = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIView.transition(with: self.coll_Registered, duration: 0.35, options: .transitionCrossDissolve, animations: {
                    self.coll_Registered.reloadData()
                })
            }
        case .upcoming:
            if self.viewModel.listRaceUpcoming.count > 0 {
                self.view_Upcoming.isHidden = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIView.transition(with: self.coll_Upcoming, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_Upcoming.reloadData() })
            }
        case .inprocess:
            if self.viewModel.listRaceInprocess.count > 0 {
                self.view_Inprocess.isHidden = false
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIView.transition(with: self.coll_Inprocess, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_Inprocess.reloadData() })
            }
        case .opened:
            self.view_Opened.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIView.transition(with: self.coll_Opened, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_Opened.reloadData() })
            }
        case .ended:
            self.view_Ended.isHidden = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                UIView.transition(with: self.coll_Ended, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_Ended.reloadData() })
            }
        default:
            break
        }
    }
    
    func setShowDetailVirtualRace(race: RaceModel?) {
        guard let raceId = race?.id else { return }
        self.router.showDetailRacingRouter(with: raceId, status: .registered)
    }
}

// MARK: - UICollectionViewDataSource
extension VirtualRaceFitViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.view_Registered.isHidden = true
        self.view_Inprocess.isHidden = true
        self.view_Upcoming.isHidden = true
        self.view_Opened.isHidden = true
        self.view_Ended.isHidden = true
        
        self.coll_Filter.registerNib(ofType: CellCollectionViewFilterRace.self)
        self.coll_Filter.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Filter.delegate = self
        self.coll_Filter.dataSource = self
        self.coll_Filter.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        
        self.coll_Registered.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_Registered.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Registered.delegate = self
        self.coll_Registered.dataSource = self
        self.coll_Registered.emptyDataSetSource = self
        
        self.coll_Inprocess.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_Inprocess.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Inprocess.delegate = self
        self.coll_Inprocess.dataSource = self
        self.coll_Inprocess.emptyDataSetSource = self
        
        self.coll_Upcoming.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_Upcoming.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Upcoming.delegate = self
        self.coll_Upcoming.dataSource = self
        self.coll_Upcoming.emptyDataSetSource = self
        
        self.coll_Opened.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_Opened.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Opened.delegate = self
        self.coll_Opened.dataSource = self
        self.coll_Opened.emptyDataSetSource = self
        
        self.coll_Ended.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_Ended.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_Ended.delegate = self
        self.coll_Ended.dataSource = self
        self.coll_Ended.emptyDataSetSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.coll_Filter:
            return self.viewModel.listFilterRace.count
            
        case self.coll_Registered:
            return self.viewModel.listRaceRegistered.count
            
        case self.coll_Inprocess:
            return self.viewModel.listRaceInprocess.count
            
        case self.coll_Upcoming:
            return self.viewModel.listRaceUpcoming.count
            
        case self.coll_Opened:
            return self.viewModel.listRaceOpened.count
            
        case self.coll_Ended:
            return self.viewModel.listRaceEnded.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.coll_Filter:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewFilterRace.self, for: indexPath)
            cell.model = self.viewModel.listFilterRace[indexPath.row]
            return cell
            
        case self.coll_Registered:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            if self.viewModel.listRaceRegistered.count > 0 {
                cell.model = self.viewModel.listRaceRegistered[indexPath.row]
            }
            return cell
            
        case self.coll_Inprocess:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listRaceInprocess[indexPath.row]
            return cell
            
        case self.coll_Upcoming:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listRaceUpcoming[indexPath.row]
            return cell
            
        case self.coll_Opened:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listRaceOpened[indexPath.row]
            return cell
            
        case self.coll_Ended:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listRaceEnded[indexPath.row]
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension VirtualRaceFitViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case self.coll_Filter:
            let padding: CGFloat = 12
            let textSize = self.viewModel.listFilterRace[indexPath.item].filterTitle.width(withConstrainedHeight: 17, font: R.font.robotoRegular(size: 14)!)
            let cellWidth = padding * 2 + textSize
            return CGSize(width: cellWidth, height: 32)
            
        default:
            return CGSize(width: 224, height: 330)
        }
    }
}

// MARK: - UICollectionViewDelegate
extension VirtualRaceFitViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.coll_Filter:
            collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            self.viewModel.typeFilterCurrent = self.viewModel.listFilterRace[indexPath.row]

        case self.coll_Registered:
            let registered = self.viewModel.listRaceRegistered
            
            guard self.viewModel.listRaceRegistered.count > 0 else {
                return
            }
            let race = self.viewModel.listRaceRegistered[indexPath.row]
            guard let raceId = race.id else { return }
            self.router.showDetailRacingRouter(with: raceId, status: .registered)

        case self.coll_Inprocess:
            let race = self.viewModel.listRaceInprocess[indexPath.row]
            guard let raceId = race.id else { return }
            self.router.showDetailRacingRouter(with: raceId, status: .registered)
            
        case self.coll_Upcoming:
            let race = self.viewModel.listRaceUpcoming[indexPath.row]
            guard let raceId = race.id else { return }
            self.router.showDetailRacingRouter(with: raceId, status: .registered)

        case self.coll_Opened:
            let race = self.viewModel.listRaceOpened[indexPath.row]
            guard let raceId = race.id else { return }
            self.router.showDetailRacingRouter(with: raceId, status: .registered)

        case self.coll_Ended:
            let race = self.viewModel.listRaceEnded[indexPath.row]
            guard let raceId = race.id else { return }
            self.router.showDetailRacingRouter(with: raceId, status: .registered)

        default:
            break
        }
    }
}

// MARK: - EmptyDataSetSource
extension VirtualRaceFitViewController: EmptyDataSetSource {
    func customView(forEmptyDataSet scrollView: UIScrollView) -> UIView? {
        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        
        let emptyImage = UIImageView(image: UIImage(named: "ic_cup"))
        
        contentView.addSubview(emptyImage)
        
        emptyImage.anchor(top: contentView.topAnchor)
        emptyImage.setDimensions(width: 60, height: 60)
        emptyImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor, constant: -16).isActive = true
        
        let titleLabel = UILabel()
        titleLabel.textColor = R.color.subTitle()
        titleLabel.attributedText = self.getMessageNoData(message: "Chưa có dữ liệu")
        
        contentView.addSubview(titleLabel)
        titleLabel.anchor(top: emptyImage.bottomAnchor)
        titleLabel.centerX(inView: emptyImage)
        
        return contentView
    }
}
