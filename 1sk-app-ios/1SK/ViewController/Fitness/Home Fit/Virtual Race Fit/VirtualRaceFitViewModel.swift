//
//  
//  VirtualRaceFitViewModel.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol VirtualRaceFitViewModelProtocol {
    func onViewDidLoad()
    func onViewWillApear()
    func onViewDidAppear()
    func onViewDidDisappear()
    
    func setShowDetailVirtualRace(race: RaceModel?)

    var listFilterRace: [TypeRaceFilter] { get set }
    var listRaceRegistered: [RaceModel] { get set }
    var listRaceInprocess: [RaceModel] { get set }
    var listRaceUpcoming: [RaceModel] { get set }
    var listRaceOpened: [RaceModel] { get set }
    var listRaceEnded: [RaceModel] { get set }
    
    var typeFilterCurrent: TypeRaceFilter { get set }
}

// MARK: - VirtualRaceFit ViewModel
class VirtualRaceFitViewModel {
    weak var view: VirtualRaceFitViewProtocol?
    private var interactor: VirtualRaceFitInteractorInputProtocol
    
    init(interactor: VirtualRaceFitInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listFilterRace: [TypeRaceFilter] = [.all, .inprocess, .upcoming, .ended]
    
    var listRaceRegistered: [RaceModel] = []
    var listRaceInprocess: [RaceModel] = []
    var listRaceUpcoming: [RaceModel] = []
    var listRaceOpened: [RaceModel] = []
    var listRaceEnded: [RaceModel] = []
    
    var typeFilterCurrent: TypeRaceFilter = .all {
        didSet {
            self.getDataRegisteredFilter()
        }
    }
    
    private func getDataRegisteredFilter() {
        self.view?.showHud()
        self.listRaceRegistered.removeAll()
        self.interactor.getVirtualRacingRegistered(page: 1, perPage: 10, filterType: self.typeFilterCurrent)
    }
    
    private func getInitData() {
        self.view?.showHud()
        self.interactor.getVirtualRacing(page: 1, perPage: 10, filterType: .opened)
        self.interactor.getVirtualRacing(page: 1, perPage: 10, filterType: .ended)
    }
}

// MARK: - VirtualRaceFit ViewModelProtocol
extension VirtualRaceFitViewModel: VirtualRaceFitViewModelProtocol {
    func onViewDidLoad() {
        self.getDataRegisteredFilter()
    }
    
    func onViewWillApear() {
        self.getInitData()
    }
    
    func onViewDidAppear() {
        self.setInitObserverEvent()
    }
    
    func onViewDidDisappear() {
        self.setRemoveObserverEvent()
    }
    
    func setShowDetailVirtualRace(race: RaceModel?) {
        guard let id = race?.id else { return }
        self.view?.showHud()
        self.interactor.getVirtualRaceDetail(raceId: id)
    }
}

// MARK: HandleSocketEvent
extension VirtualRaceFitViewModel {
    func setInitObserverEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onUserRegisterVRAction), name: .UserRegisterVR, object: nil)
    }
    
    func setRemoveObserverEvent() {
        NotificationCenter.default.removeObserver(self, name: .UserRegisterVR, object: nil)
    }
    
    @objc func onUserRegisterVRAction(notification: NSNotification) {
        guard let race = notification.object as? RaceModel else {
            return
        }
        guard self.listRaceOpened.first(where: {$0.id == race.id}) != nil else {
            return
        }
        self.interactor.getVirtualRacing(page: 1, perPage: 10, filterType: .opened)
    }
    
    @objc func onVRRegistrationJustExpiredEventAction(notification: NSNotification) {
        self.interactor.getVirtualRacing(page: 1, perPage: 10, filterType: .opened)
    }
}

// MARK: - VirtualRaceFit InteractorOutputProtocol
extension VirtualRaceFitViewModel: VirtualRaceFitInteractorOutputProtocol {
    func onGetVirtualRacingRegisteredFinished(with result: Result<[RaceModel], APIError>, filterType: TypeRaceFilter) {
        switch result {
        case .success(let model):
            switch filterType {
            case .all:
                self.listRaceRegistered = model
                
            default:
                self.listRaceRegistered = model.filter({$0.typeRace == filterType})
            }
            
            self.view?.onReloadData(filterType: .registered)
            
        case .failure:
            self.view?.onReloadData(filterType: .registered)
        }
        self.view?.hideHud()
    }
    
    func onGetVirtualRacingFinished(with result: Result<[RaceModel], APIError>, filterType: TypeRaceFilter) {
        switch result {
        case .success(let model):
            switch filterType {
            case .upcoming:
                self.listRaceUpcoming = model
                
            case .inprocess:
                self.listRaceInprocess = model
                
            case .opened:
                self.listRaceOpened = model
                self.view?.onReloadData(filterType: .opened)
                
            case .ended:
                self.listRaceEnded = model
                self.view?.onReloadData(filterType: .ended)
                
            default:
                break
            }
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
    
    func onGetVirtualRaceDetailFinished(with result: Result<RaceModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.setShowDetailVirtualRace(race: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}

// MARK: - TypeRaceFilter
enum TypeRaceFilter: Int, CaseIterable {
    case all = 0
    case inprocess = 1
    case upcoming = 2
    case ended = 3
    case opened = 4
    case registered = 5
    
    var state: Int {
        switch self {
        case .all:
            return 0
        case .inprocess:
            return 1
        case .upcoming:
            return 2
        case .ended:
            return 3
        case .opened:
            return 4
        case .registered:
            return 5
        }
    }
    
    var filterTitle: String {
        switch self {
        case .all:
            return "Tất cả"
        case .inprocess:
            return "Đang diễn ra"
        case .upcoming:
            return "Sắp diễn ra"
        case .ended:
            return "Đã kết thúc"
        case .opened:
            return "Đang mở"
        case .registered:
            return "Giải đua đã đăng ký"
        }
    }
    
    var headerTitle: String {
        switch self {
        case .all:
            return "Tất cả"
        case .inprocess:
            return "Giải đua đang diễn ra"
        case .upcoming:
            return "Giải đua sắp diễn ra"
        case .ended:
            return "Giải đua đã kết thúc"
        case .opened:
            return "Giải đua đang mở"
        case .registered:
            return "Giải đua đã đăng ký"
        }
    }
}
