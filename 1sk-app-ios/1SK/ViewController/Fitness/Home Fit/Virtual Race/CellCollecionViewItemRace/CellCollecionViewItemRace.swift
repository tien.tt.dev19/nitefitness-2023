//
//  CellCollecionViewItemRace.swift
//  1SK
//
//  Created by TrungDN on 14/07/2022.
//

import UIKit

class CellCollecionViewItemRace: UICollectionViewCell {
    
    @IBOutlet weak var virtualRaceImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var dateIconImageView: UIImageView!
    @IBOutlet weak var participantIconImageView: UIImageView!
    @IBOutlet weak var priceIconImageView: UIImageView!

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var participantLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var participantStackView: UIStackView!
    @IBOutlet weak var prizeStackView: UIStackView!
    
    @IBOutlet weak var progressView: UIView!
    @IBOutlet weak var resultProgressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var view_Status: UIView!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var view_RegisterAnable: UIView!
    
    @IBOutlet weak var stack_StackView: UIStackView!
    
    var model: RaceModel? {
        didSet {
            self.setData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configUI() {
        self.stack_StackView.isHidden = true
    }

    private func setData() {
        guard let model = self.model else {
            return
        }
        
        let startDate = model.startAt?.toDate(.ymdhms)?.toString(.dmySlash) ?? ""
        let endDate = model.finishAt?.toDate(.ymdhms)?.toString(.dmySlash) ?? ""
        let regiterDate = model.registerExpireAt?.toDate(.ymdhms)?.timeIntervalSince1970 ?? 0
        
        if let isRegistered = model.isRegistered {
            self.prizeStackView.isHidden = isRegistered
            self.participantStackView.isHidden = isRegistered
            self.progressView.isHidden = !isRegistered
            if isRegistered {
                var unit = ""
                if let sport = model.sport {
                    unit = sport.unit ?? ""
                }
                if let customerGoal = model.customerGoal {
                    if let stravaTracking = model.stravaTracking {
                        let totalDistanceCustomer = (stravaTracking.totalDistanceCustomer ?? 0) / 1000
                        let roundedDistanceCustomer = Float(round(totalDistanceCustomer * 100) / 100)
                        let progress = roundedDistanceCustomer / (Float(customerGoal.goal ?? "1") ?? 1)
                        self.resultProgressView.progress = progress
                        self.resultLabel.text = "\(roundedDistanceCustomer)/\(customerGoal.goal ?? "") \(unit)"
                        self.progressLabel.text = "Đã thực hiện \(Int(progress * 100))%"
                    }
                }
            }
        }

        self.titleLabel.text = model.name ?? ""
        self.dateLabel.text = "\(startDate) - \(endDate)"
        self.virtualRaceImageView.setImageWith(imageUrl: model.thumbnail ?? "")
        self.participantLabel.text = "\(model.totalParticipants ?? 0) người tham gia"
                
        if regiterDate < Date().timeIntervalSince1970 {
            self.view_RegisterAnable.isHidden = true
        } else {
            self.view_RegisterAnable.isHidden = false
        }
        
        switch model.typeRace {
        case .inprocess:
            self.lbl_Status.text = model.typeRace?.filterTitle
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = UIColor(hex: "00C2C5")
            
        case .upcoming:
            self.lbl_Status.text = model.typeRace?.filterTitle
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = UIColor(hex: "FEB11A")
            
        case .ended:
            self.lbl_Status.text = model.typeRace?.filterTitle
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = UIColor(hex: "737678")
            
        default:
            break
        }
    }
}
