//
//  
//  VirtualRaceInteractorInput.swift
//  1SK
//
//  Created by Tiến Trần on 20/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol VirtualRaceInteractorInputProtocol {
    func get_VirtualRacingRegistered(page: Int, perPage: Int, filterType: TypeRaceFilter)
}

// MARK: - Interactor Output Protocol
protocol VirtualRaceInteractorOutputProtocol: AnyObject {
    func onGet_VirtualRacingRegistered_Finished(with result: Result<[RaceModel], APIError>, filterType: TypeRaceFilter, page: Int, total: Int)
}

// MARK: - VirtualRace InteractorInput
class VirtualRaceInteractorInput {
    weak var output: VirtualRaceInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - VirtualRace InteractorInputProtocol
extension VirtualRaceInteractorInput: VirtualRaceInteractorInputProtocol {
    func get_VirtualRacingRegistered(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.fitRaceSevice?.onGetListVisualRace(page: page, perPage: perPage, filterType: filterType, completion: { [weak self] result in
            self?.output?.onGet_VirtualRacingRegistered_Finished(with: result.unwrapSuccessModel(), filterType: filterType, page: page, total: result.getTotal() ?? 0)
        })
    }
}
