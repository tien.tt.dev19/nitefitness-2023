//
//  
//  VirtualRaceRouter.swift
//  1SK
//
//  Created by Tiến Trần on 20/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol VirtualRaceRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter) 
}

// MARK: - VirtualRace Router
class VirtualRaceRouter {
    weak var viewController: VirtualRaceViewController?
    
    static func setupModule() -> VirtualRaceViewController {
        let viewController = VirtualRaceViewController()
        let router = VirtualRaceRouter()
        let interactorInput = VirtualRaceInteractorInput()
        let viewModel = VirtualRaceViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - VirtualRace RouterProtocol
extension VirtualRaceRouter: VirtualRaceRouterProtocol {
    func showDetailRacingRouter(with id: Int, status: TypeRaceFilter) {
        let controller = DetailRacingRouter.setupModule(with: id, status: status)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
