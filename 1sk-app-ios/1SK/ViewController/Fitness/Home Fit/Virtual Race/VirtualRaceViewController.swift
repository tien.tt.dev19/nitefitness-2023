//
//  
//  VirtualRaceViewController.swift
//  1SK
//
//  Created by Tiến Trần on 20/09/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol VirtualRaceViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
}

// MARK: - VirtualRace ViewController
class VirtualRaceViewController: BaseViewController {
    var router: VirtualRaceRouterProtocol!
    var viewModel: VirtualRaceViewModelProtocol!
    
    @IBOutlet weak var coll_RaceFilter: UICollectionView!
    @IBOutlet weak var coll_Racingfit: UICollectionView!
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Giải đua"
        self.setInitCollectionView()
    }
    
    // MARK: - Action
}

//MARK: - UICollectionViewDataSource
extension VirtualRaceViewController: UICollectionViewDataSource {
    func setInitCollectionView() {
        self.coll_RaceFilter.registerNib(ofType: CellCollectionViewFilterRace.self)
        self.coll_RaceFilter.delegate = self
        self.coll_RaceFilter.dataSource = self
        self.coll_RaceFilter.selectItem(at: IndexPath(item: 0, section: 0), animated: true, scrollPosition: .centeredVertically)
        
        self.coll_Racingfit.registerNib(ofType: CellCollecionViewItemRace.self)
        self.coll_Racingfit.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 16, right: 0)
        self.coll_Racingfit.delegate = self
        self.coll_Racingfit.dataSource = self
        self.coll_Racingfit.emptyDataSetSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_RaceFilter.collectionViewLayout = layout
        
        let layoutRacingFit = UICollectionViewFlowLayout()
        layoutRacingFit.scrollDirection = .vertical
        layoutRacingFit.minimumLineSpacing = 16
        layoutRacingFit.minimumInteritemSpacing = 16
        layoutRacingFit.sectionInset = UIEdgeInsets(top: 5, left: 16, bottom: 16, right: 16)
        self.coll_Racingfit.collectionViewLayout = layoutRacingFit
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.coll_RaceFilter:
            return self.viewModel.listFilterRace.count
            
        case self.coll_Racingfit:
            return self.viewModel.listRace.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.coll_RaceFilter:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewFilterRace.self, for: indexPath)
            cell.model = self.viewModel.listFilterRace[indexPath.row]
            return cell
            
        case self.coll_Racingfit:
            let cell = collectionView.dequeuCell(ofType: CellCollecionViewItemRace.self, for: indexPath)
            cell.model = self.viewModel.listRace[indexPath.row]
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

extension VirtualRaceViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch collectionView {
        case self.coll_RaceFilter:
            let padding: CGFloat = 12
            let textSize = self.viewModel.listFilterRace[indexPath.item].filterTitle.width(withConstrainedHeight: 17, font: R.font.robotoRegular(size: 14)!)
            let cellWidth = padding * 2 + textSize
            return CGSize(width: cellWidth, height: 32)
            
        case self.coll_Racingfit:
            let padding: CGFloat = 16
            let cellWidth = collectionView.frame.width - padding * 2
            let cellHeight: CGFloat = 325
            return CGSize(width: cellWidth, height: cellHeight)
            
        default:
            return .zero
        }
    }
}

//MARK: - UICollectionViewDelegate
extension VirtualRaceViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.coll_RaceFilter:
            collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
            self.viewModel.typeFilterCurrent = self.viewModel.listFilterRace[indexPath.row]
            
        case self.coll_Racingfit:
            let race = self.viewModel.listRace[indexPath.row]
            guard let raceId = race.id else { return }
            self.router.showDetailRacingRouter(with: raceId, status: .registered)
            
        default:
            break
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.coll_RaceFilter {
            return
        }
        
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: - VirtualRace ViewProtocol
extension VirtualRaceViewController: VirtualRaceViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        UIView.transition(with: self.coll_Racingfit, duration: 0.35, options: .transitionCrossDissolve, animations: { self.coll_Racingfit.reloadData() })
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.coll_Racingfit.insertItems(at: [IndexPath.init(row: index, section: 0)])
    }
}

// MARK: - EmptyDataSetSource
extension VirtualRaceViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có dữ liệu")
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_cup")
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
}
