//
//  
//  VirtualRaceViewModel.swift
//  1SK
//
//  Created by Tiến Trần on 20/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol VirtualRaceViewModelProtocol {
    var listFilterRace: [TypeRaceFilter] { get set }
    var listRace: [RaceModel] { get set }
    var typeFilterCurrent: TypeRaceFilter { get set }
    
    func onViewDidLoad()
    func onLoadMoreAction()
}

// MARK: - VirtualRace ViewModel
class VirtualRaceViewModel {
    weak var view: VirtualRaceViewProtocol?
    private var interactor: VirtualRaceInteractorInputProtocol

    init(interactor: VirtualRaceInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 10
    private var isOutOfData = false
    
    var listFilterRace: [TypeRaceFilter] = [.all, .inprocess, .upcoming, .ended]
    var listRace: [RaceModel] = []
    var typeFilterCurrent: TypeRaceFilter = .all {
        didSet {
            self.page = 1
            self.getData(page: 1)
        }
    }
    
    func getData(page: Int) {
        self.view?.showHud()
        
        if page == 1 {
            self.isOutOfData = false
        }
        
        self.interactor.get_VirtualRacingRegistered(page: page, perPage: self.perPage, filterType: self.typeFilterCurrent)
    }
}

// MARK: - VirtualRace ViewModelProtocol
extension VirtualRaceViewModel: VirtualRaceViewModelProtocol {
    func onViewDidLoad() {
        self.view?.showHud()
        self.interactor.get_VirtualRacingRegistered(page: 1, perPage: 10, filterType: .all)
    }
    
    func onloadRacingList(page: Int, perPage: Int, filterType: TypeRaceFilter) {
        self.view?.showHud()
        self.interactor.get_VirtualRacingRegistered(page: 1, perPage: 10, filterType: filterType)
    }
    
    func onLoadMoreAction() {
        if self.listRace.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
        }
    }
}
 
// MARK: - VirtualRace InteractorOutputProtocol
extension VirtualRaceViewModel: VirtualRaceInteractorOutputProtocol {
    func onGet_VirtualRacingRegistered_Finished(with result: Result<[RaceModel], APIError>, filterType: TypeRaceFilter, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.listRace = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.listRace.append(object)
                    self.view?.onInsertRow(at: self.listRace.count - 1)
                }
            }
            
            self.page = page
            self.total = total
            
            // Set Out Of Data
            if self.listRace.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
                        
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        
        self.view?.hideHud()
    }
}
