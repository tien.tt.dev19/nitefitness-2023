//
//  
//  WalkingFitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol WalkingFitInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol WalkingFitInteractorOutputProtocol: AnyObject {
    
}

// MARK: - WalkingFit InteractorInput
class WalkingFitInteractorInput {
    weak var output: WalkingFitInteractorOutputProtocol?
}

// MARK: - WalkingFit InteractorInputProtocol
extension WalkingFitInteractorInput: WalkingFitInteractorInputProtocol {

}
