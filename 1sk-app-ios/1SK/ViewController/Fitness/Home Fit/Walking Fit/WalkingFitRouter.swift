//
//  
//  WalkingFitRouter.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol WalkingFitRouterProtocol {

}

// MARK: - WalkingFit Router
class WalkingFitRouter {
    weak var viewController: WalkingFitViewController?
    
    static func setupModule() -> WalkingFitViewController {
        let viewController = WalkingFitViewController()
        let router = WalkingFitRouter()
        let interactorInput = WalkingFitInteractorInput()
        let viewModel = WalkingFitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - WalkingFit RouterProtocol
extension WalkingFitRouter: WalkingFitRouterProtocol {
    
}
