//
//  
//  WalkingFitViewController.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol WalkingFitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - WalkingFit ViewController
class WalkingFitViewController: BaseViewController {
    var router: WalkingFitRouterProtocol!
    var viewModel: WalkingFitViewModelProtocol!
    
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Đi bộ"
    }
    
    // MARK: - Action
    
}

// MARK: - WalkingFit ViewProtocol
extension WalkingFitViewController: WalkingFitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}
