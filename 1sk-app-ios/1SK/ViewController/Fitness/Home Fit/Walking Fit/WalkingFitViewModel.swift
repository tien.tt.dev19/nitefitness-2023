//
//  
//  WalkingFitViewModel.swift
//  1SK
//
//  Created by Thaad on 06/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol WalkingFitViewModelProtocol {
    func onViewDidLoad()
    //func onViewDidAppear()
    
    // UITableView
    //func numberOfSections() -> Int
    //func numberOfRows() -> Int
    //func cellForRow(at indexPath: IndexPath) -> Any?
    //func didSelectRow(at indexPath: IndexPath)
    
    // UICollectionView
    //func numberOfItems() -> Int
    //func cellForItem(at indexPath: IndexPath) -> Any
    //func didSelectItem(at indexPath: IndexPath)
}

// MARK: - WalkingFit ViewModel
class WalkingFitViewModel {
    weak var view: WalkingFitViewProtocol?
    private var interactor: WalkingFitInteractorInputProtocol

    init(interactor: WalkingFitInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - WalkingFit ViewModelProtocol
extension WalkingFitViewModel: WalkingFitViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - WalkingFit InteractorOutputProtocol
extension WalkingFitViewModel: WalkingFitInteractorOutputProtocol {

}
