//
//  
//  CreateWorkoutConstract.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

// MARK: View -
protocol CreateWorkoutViewProtocol: AnyObject {
    func updateView(with workout: WorkoutModel?, state: StateViewScreen)
    func updateView(with item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType)
    func updatePostButtonEnable(_ isEnable: Bool) 
}

// MARK: Interactor -
protocol CreateWorkoutInteractorInputProtocol {
    func createWorkout(name: String, time: Int, subjectID: Int, levelID: Int, typeID: Int)
}

protocol CreateWorkoutInteractorOutputProtocol: AnyObject {
    func onCreateWorkoutFinished(with result: Result<WorkoutModel, APIError>)
}
// MARK: Presenter -
protocol CreateWorkoutPresenterProtocol {
    func onViewDidLoad()
    func onSelectedLabelDidSelected(with type: WorkoutSelectionViewController.ItemType)
    func onNameTextFieldTextDidChanged(with content: String)
    func onTimePickerDidChanged(with value: Double)
    func onCreateButtonDidTapped()
}

// MARK: Router -
protocol CreateWorkoutRouterProtocol: BaseRouterProtocol {
    func showWorkoutSelectionViewController(with type: WorkoutSelectionViewController.ItemType, selectedID: Int?, delegate: WorkoutSelectionViewControllerDelegate?)

    func pop()
}
