//
//  
//  CreateWorkoutInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class CreateWorkoutInteractorInput {
    weak var output: CreateWorkoutInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - CreateWorkoutInteractorInputProtocol
extension CreateWorkoutInteractorInput: CreateWorkoutInteractorInputProtocol {
    func createWorkout(name: String, time: Int, subjectID: Int, levelID: Int, typeID: Int) {
        exerciseService?.createWorkout(name: name, time: time, subjectID: subjectID, levelID: levelID, typeID: typeID, completion: { [weak self] result in
            self?.output?.onCreateWorkoutFinished(with: result.unwrapSuccessModel())
        })
    }
}
