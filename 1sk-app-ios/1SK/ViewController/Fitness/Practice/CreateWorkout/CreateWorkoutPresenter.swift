//
//  
//  CreateWorkoutPresenter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

protocol CreateWorkoutPresenterDelegate: AnyObject {
    func didCreateWorkoutSuccess(_ workout: WorkoutModel)
}

class CreateWorkoutPresenter {
    var state: StateViewScreen = .new
    var workout: WorkoutModel?
    var name: String = ""
    var totalTime: Int = 0
    var subjectID: Int = -1
    var levelID: Int = -1
    var typeID: Int = -1

    weak var delegate: CreateWorkoutPresenterDelegate?
    
    weak var view: CreateWorkoutViewProtocol?
    private var interactor: CreateWorkoutInteractorInputProtocol
    private var router: CreateWorkoutRouterProtocol

    init(interactor: CreateWorkoutInteractorInputProtocol,
         router: CreateWorkoutRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    private func updatePostButtonEnableState() {
        let isEmpty = name.isEmpty || totalTime <= 0 || subjectID == -1 || typeID == -1 || levelID == -1
        let isDiffirent = name != workout?.name || totalTime != workout?.time || subjectID != workout?.subject?.id ?? 0 || typeID != workout?.type?.id || levelID != workout?.level?.id ?? 0
        let isEnable = !isEmpty && isDiffirent
        view?.updatePostButtonEnable(isEnable)
    }
}

// MARK: - CreateWorkoutPresenterProtocol
extension CreateWorkoutPresenter: CreateWorkoutPresenterProtocol {
    func onViewDidLoad() {
        view?.updateView(with: workout, state: state)
    }

    func onNameTextFieldTextDidChanged(with content: String) {
        self.name = content
        updatePostButtonEnableState()
    }

    func onTimePickerDidChanged(with value: Double) {
        self.totalTime = value.intValue
        updatePostButtonEnableState()
    }

    func onSelectedLabelDidSelected(with type: WorkoutSelectionViewController.ItemType) {
        var id: Int?
        switch type {
        case .subject:
            id = subjectID
        case .level:
            id = levelID
        case .type:
            id = typeID
        }
        router.showWorkoutSelectionViewController(with: type, selectedID: id, delegate: self)
    }

    func onCreateButtonDidTapped() {
        router.showHud()
        interactor.createWorkout(name: name, time: totalTime, subjectID: subjectID, levelID: levelID, typeID: typeID)
    }
}

// MARK: - CreateWorkoutPresenter: CreateWorkoutInteractorOutput -
extension CreateWorkoutPresenter: CreateWorkoutInteractorOutputProtocol {
    func onCreateWorkoutFinished(with result: Result<WorkoutModel, APIError>) {
        switch result {
        case .success(let workoutModel):
            delegate?.didCreateWorkoutSuccess(workoutModel)
            router.pop()
            
        case .failure:
            router.showToast(content: "Đã có lỗi xảy ra")
        }
        router.hideHud()
    }
}

// MARK: - WorkoutSelectionViewControllerDelegate
extension CreateWorkoutPresenter: WorkoutSelectionViewControllerDelegate {
    func didSelected(item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType) {
        switch type {
        case .subject:
            subjectID = item.id
        case .level:
            levelID = item.id
        case .type:
            typeID = item.id
        }
        view?.updateView(with: item, type: type)
        updatePostButtonEnableState()
    }
}
