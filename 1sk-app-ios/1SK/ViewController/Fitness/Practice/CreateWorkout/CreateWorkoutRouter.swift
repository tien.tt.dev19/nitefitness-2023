//
//
//  CreateWorkoutRouter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class CreateWorkoutRouter: BaseRouter {
    static func setupModule(with state: StateViewScreen, workout: WorkoutModel?, delegate: CreateWorkoutPresenterDelegate?) -> CreateWorkoutViewController {
        let viewController = CreateWorkoutViewController()
        let router = CreateWorkoutRouter()
        let interactorInput = CreateWorkoutInteractorInput()
        let presenter = CreateWorkoutPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.state = state
        presenter.workout = workout
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - CreateWorkoutRouterProtocol
extension CreateWorkoutRouter: CreateWorkoutRouterProtocol {
    func showWorkoutSelectionViewController(with type: WorkoutSelectionViewController.ItemType, selectedID: Int?, delegate: WorkoutSelectionViewControllerDelegate?) {
        let workoutSelectionItemViewController = WorkoutSelectionViewController.loadFromNib()
        workoutSelectionItemViewController.type = type
        workoutSelectionItemViewController.delegate = delegate
        workoutSelectionItemViewController.selectedID = selectedID
        workoutSelectionItemViewController.modalPresentationStyle = .overFullScreen
        viewController?.present(workoutSelectionItemViewController, animated: false, completion: nil)
    }

    func pop() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
