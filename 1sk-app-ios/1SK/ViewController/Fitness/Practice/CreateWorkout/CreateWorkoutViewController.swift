//
//  
//  CreateWorkoutViewController.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class CreateWorkoutViewController: BaseViewController {
    
    @IBOutlet weak var nameSKTextField: SKTextField!
    @IBOutlet weak var timeSKTextField: SKTextField!
    @IBOutlet weak var subjectSKTextField: SKTextField!
    @IBOutlet weak var levelSKTextField: SKTextField!
    @IBOutlet weak var typeSKTextField: SKTextField!
    @IBOutlet weak var totalTimePickerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var totalTimePicker: UIPickerView!
    
    private let mutiplerNumber = 100 // mutilplier data to make infinity date picker

    private var nameTextField: UITextField {
        return nameSKTextField.titleTextField
    }

    var presenter: CreateWorkoutPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        presenter.onViewDidLoad()
    }
    // MARK: - Setup
    private func setupDefaults() {
        setupTextField()
        setupNavigation()
        setupTotalTimePicker()
    }

    private func setupNavigation() {
        navigationItem.title = "Tạo bài tập"
        setRightBarButton(style: .saveDisable, handler: nil)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }

    private func setupTextField() {
        nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        nameSKTextField.setKeyboardType(.default)
        nameTextField.delegate = self
        timeSKTextField.delegate = self
        subjectSKTextField.delegate = self
        levelSKTextField.delegate = self
        typeSKTextField.delegate = self

    }

    private func setupTotalTimePicker() {
        totalTimePicker.setValue(R.color.black(), forKeyPath: "textColor")
        totalTimePicker.delegate = self
        totalTimePicker.dataSource = self
        totalTimePicker.selectRow(24 * mutiplerNumber / 2, inComponent: 0, animated: false)
        totalTimePicker.selectRow(60 * mutiplerNumber / 2, inComponent: 2, animated: false)
        totalTimePicker.selectRow(60 * mutiplerNumber / 2, inComponent: 4, animated: false)
        totalTimePickerHeightConstraint.constant = 0
    }
    // MARK: - Action
    @objc func textFieldDidChange(_ textField: UITextField) {
        presenter.onNameTextFieldTextDidChanged(with: textField.text ?? "")
        updateTypeLabelHiddenState(of: textField)
        setTotalTimePickerHidden(true)
    }

    private func setTotalTimePickerHidden(_ isHidden: Bool) {
        let height: CGFloat = isHidden ? 0 : 216
        guard totalTimePickerHeightConstraint.constant != height else {
            return
        }
        totalTimePickerHeightConstraint.constant = height
        UIView.animate(withDuration: Constant.Number.animationTime) {
            self.view.layoutIfNeeded()
        }
    }
    // MARK: - Helper
    private func updateTypeLabelHiddenState(of textField: UITextField) {
        switch textField {
        case nameTextField:
            nameSKTextField.updateTypeLabelHiddenState()
        default:
            break
        }
    }
}

// MARK: - CreateWorkoutViewProtocol
extension CreateWorkoutViewController: CreateWorkoutViewProtocol {
    func updateView(with workout: WorkoutModel?, state: StateViewScreen) {
//        config(with: profile)
        let isEdit = state != .normal
        [nameSKTextField, timeSKTextField, subjectSKTextField, levelSKTextField, typeSKTextField].forEach({ $0?.setEnable(isEnable: isEdit)})
    }

    func updatePostButtonEnable(_ isEnable: Bool) {
        guard isEnable != navigationItem.rightBarButtonItem?.isEnabled else {
            return
        }

        if isEnable {
            setRightBarButton(style: .saveEnable) { [weak self] _ in
                self?.navigationItem.rightBarButtonItem?.isEnabled = true
                self?.presenter.onCreateButtonDidTapped()
            }
        } else {
            setRightBarButton(style: .saveDisable, handler: nil)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

    func updateView(with item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType) {
        switch type {
        case .subject:
            subjectSKTextField.setValue(with: item.name)
        case .type:
            typeSKTextField.setValue(with: item.name)
        case .level:
            levelSKTextField.setValue(with: item.name)
        }
    }
}

// MARK: - UITextFieldDelegate
extension CreateWorkoutViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            didSelect(timeSKTextField)
        default:
            break
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        updateTypeLabelHiddenState(of: textField)
        if textField == nameTextField {
            setTotalTimePickerHidden(true)
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        updateTypeLabelHiddenState(of: textField)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case nameTextField:
            var `string` = string
            string.removeAll(where: {$0 == " "})
            if string.rangeOfCharacter(from: .letters) != nil || string == ""{
                return true
            } else {
                return false
            }
        default:
            break
        }
        return true
    }
}

// MARK: - SKSelectedLabelDelegate
extension CreateWorkoutViewController: SKSelectedLabelDelegate {
    func didSelect(_ scrollDownLabel: SKTextField) {
        view.endEditing(true)
        switch scrollDownLabel {
        case subjectSKTextField:
            presenter.onSelectedLabelDidSelected(with: .subject)
        case levelSKTextField:
            presenter.onSelectedLabelDidSelected(with: .level)
        case typeSKTextField:
            presenter.onSelectedLabelDidSelected(with: .type)
        default:
            break
        }
        setTotalTimePickerHidden(scrollDownLabel != timeSKTextField)
    }
}
// MARK: - UIPickerViewDataSource
extension CreateWorkoutViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if pickerView == totalTimePicker {
            return 6
        } else {
            return 4
        }
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == totalTimePicker {
            switch component {
            case 0:
                return mutiplerNumber * 24
            case 2, 4:
                return mutiplerNumber * 60
            case 1, 3, 5:
                return 1
            default:
                return 0
            }
        } else {
            switch component {
            case 0:
                return mutiplerNumber * 60
            case 2:
                return mutiplerNumber * 60
            case 1, 3:
                return 1
            default:
                return 0
            }
        }
    }
}
// MARK: - UIPickerViewDelegate
extension CreateWorkoutViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            let value = row % 24
            return "\(value)"
        case 1:
            return "giờ"
        case 2:
            let value = row % 60
            return "\(value)"
        case 3:
            return "phút"
        case 4:
            let value = row % 60
            return "\(value)"
        case 5:
            return "giây"
        default:
            return ""
        }
    }

    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        switch component {
        case 0, 2, 4:
            return 40
        case 1, 3, 5:
            return 57
        default:
            return 0
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let hour = pickerView.selectedRow(inComponent: 0) % 24
        let minute = pickerView.selectedRow(inComponent: 2) % 60
        let second = pickerView.selectedRow(inComponent: 4) % 60
        let totalTime = hour * 3600 + minute * 60 + second
        timeSKTextField.setValue(with: totalTime.doubleValue.toFullTimeString())
        presenter.onTimePickerDidChanged(with: totalTime.doubleValue)
    }

    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
}
