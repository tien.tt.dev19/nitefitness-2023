//
//  
//  EditWorkoutConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

// MARK: - View
protocol EditWorkoutViewProtocol: AnyObject {
    func setInitViewState(with workout: WorkoutModel?, state: StateViewScreen)
    func updateView(with item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType)
    func onUpdateStateEnableButtonSave(_ isEnable: Bool, viewState: StateViewScreen)
}

// MARK: - Presenter
protocol EditWorkoutPresenterProtocol {
    func onViewDidLoad()
    func onSelectedLabelDidSelected(with type: WorkoutSelectionViewController.ItemType)
    func onNameTextFieldTextDidChanged(with content: String)
    func onTimePickerDidChanged(with value: Double)
    func onSaveCreateWorkoutAction()
    func onSaveUpdateWorkoutAction()
}

// MARK: - Interactor Input
protocol EditWorkoutInteractorInputProtocol {
    func createWorkout(name: String, time: Int, subjectID: Int, levelID: Int, typeID: Int)
    func setUpdateWorkout(workout: WorkoutModel, detailAdd: WorkoutDetail?)
}

// MARK: - Interactor Output
protocol EditWorkoutInteractorOutputProtocol: AnyObject {
    func onCreateWorkoutFinished(with result: Result<WorkoutModel, APIError>)
    func onUpdateWorkoutFinished(with result: Result<WorkoutModel, APIError>, total: Int)
}

// MARK: - Router
protocol EditWorkoutRouterProtocol: BaseRouterProtocol {
    func showWorkoutSelectionViewController(with type: WorkoutSelectionViewController.ItemType, selectedID: Int?, delegate: WorkoutSelectionViewControllerDelegate?)

    func pop()
}
