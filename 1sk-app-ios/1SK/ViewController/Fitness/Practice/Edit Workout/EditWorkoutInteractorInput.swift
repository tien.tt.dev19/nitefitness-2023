//
//  
//  EditWorkoutInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class EditWorkoutInteractorInput {
    weak var output: EditWorkoutInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - EditWorkoutInteractorInputProtocol
extension EditWorkoutInteractorInput: EditWorkoutInteractorInputProtocol {
    func createWorkout(name: String, time: Int, subjectID: Int, levelID: Int, typeID: Int) {
        exerciseService?.createWorkout(name: name, time: time, subjectID: subjectID, levelID: levelID, typeID: typeID, completion: { [weak self] result in
            self?.output?.onCreateWorkoutFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setUpdateWorkout(workout: WorkoutModel, detailAdd: WorkoutDetail?) {
        self.exerciseService?.updateWorkout(workout: workout, detailAdd: detailAdd, completion: { [weak self] result in
            self?.output?.onUpdateWorkoutFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0)
        })
    }
}
