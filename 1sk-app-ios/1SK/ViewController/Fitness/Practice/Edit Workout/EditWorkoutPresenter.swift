//
//  
//  EditWorkoutPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

protocol EditWorkoutPresenterDelegate: AnyObject {
    func onDidSaveWorkoutSuccess(workout: WorkoutModel)
}

class EditWorkoutPresenter {

    weak var view: EditWorkoutViewProtocol?
    private var interactor: EditWorkoutInteractorInputProtocol
    private var router: EditWorkoutRouterProtocol
    weak var delegate: EditWorkoutPresenterDelegate?
    
    var state: StateViewScreen = .new
    var workout: WorkoutModel?
    var name: String = ""
    var totalTime: Int = 0
    var subjectID: Int = -1
    var levelID: Int = -1
    var typeID: Int = -1
    
    init(interactor: EditWorkoutInteractorInputProtocol,
         router: EditWorkoutRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    private func onValidateEnableStateButtonSave() {
        let isEmpty = self.name.isEmpty || self.subjectID == -1 || self.typeID == -1 || self.levelID == -1
        
        let isDiffirent = self.name != self.workout?.name || self.subjectID != self.workout?.subject?.id ?? 0 || self.typeID != self.workout?.type?.id ?? 0 || self.levelID != self.workout?.level?.id
        
        let isEnable = !isEmpty && isDiffirent
        
        self.view?.onUpdateStateEnableButtonSave(isEnable, viewState: self.state)
    }
}

// MARK: - EditWorkoutPresenterProtocol
extension EditWorkoutPresenter: EditWorkoutPresenterProtocol {
    func onViewDidLoad() {
        if self.state == .edit {
            self.name = self.workout?.name ?? ""
            self.totalTime = self.workout?.time ?? 0
            self.subjectID = self.workout?.subject?.id ?? -1
            self.levelID = self.workout?.level?.id ?? -1
            self.typeID = self.workout?.type?.id ?? -1
        }
        self.view?.setInitViewState(with: self.workout, state: self.state)
    }

    func onNameTextFieldTextDidChanged(with content: String) {
        self.name = content
        self.onValidateEnableStateButtonSave()
    }

    func onTimePickerDidChanged(with value: Double) {
        self.totalTime = value.intValue
        self.onValidateEnableStateButtonSave()
    }

    func onSelectedLabelDidSelected(with type: WorkoutSelectionViewController.ItemType) {
        var id: Int?
        switch type {
        case .subject:
            id = self.subjectID
        case .level:
            id = self.levelID
        case .type:
            id = self.typeID
        }
        self.router.showWorkoutSelectionViewController(with: type, selectedID: id, delegate: self)
    }

    func onSaveCreateWorkoutAction() {
        self.router.showHud()
        let nameTrimmed = self.name.trimmingCharacters(in: .whitespaces)
        self.interactor.createWorkout(name: nameTrimmed, time: self.totalTime, subjectID: self.subjectID, levelID: self.levelID, typeID: self.typeID)
    }
    
    func onSaveUpdateWorkoutAction() {
        if self.workout?.subject == nil {
            self.workout?.subject = SubjectModel.init(id: Int(), name: "")
        }
        if self.workout?.level == nil {
            self.workout?.level = LevelModel.init(id: Int(), name: "")
        }
        if self.workout?.type == nil {
            self.workout?.type = TypeModel.init(id: Int(), name: "")
        }
        
        self.workout?.name = self.name.trimmingCharacters(in: .whitespaces)
        self.workout?.time = self.totalTime
        self.workout?.subject?.id = self.subjectID
        self.workout?.level?.id = self.levelID
        self.workout?.type?.id = self.typeID
        
        self.router.showHud()
        self.interactor.setUpdateWorkout(workout: self.workout!, detailAdd: nil)
    }
}

// MARK: - EditWorkoutInteractorOutput 
extension EditWorkoutPresenter: EditWorkoutInteractorOutputProtocol {
    func onCreateWorkoutFinished(with result: Result<WorkoutModel, APIError>) {
        switch result {
        case .success(let workoutModel):
            self.delegate?.onDidSaveWorkoutSuccess(workout: workoutModel)
            self.router.pop()
            
        case .failure:
            self.router.showToast(content: "Đã có lỗi xảy ra")
        }
        self.router.hideHud()
    }
    
    func onUpdateWorkoutFinished(with result: Result<WorkoutModel, APIError>, total: Int) {
        switch result {
        case .success(let workoutModel):
            self.delegate?.onDidSaveWorkoutSuccess(workout: workoutModel)
            self.router.pop()
            
        case .failure:
            self.router.showToast(content: "Đã có lỗi xảy ra")
        }
        self.router.hideHud()
    }
}

// MARK: - WorkoutSelectionViewControllerDelegate
extension EditWorkoutPresenter: WorkoutSelectionViewControllerDelegate {
    func didSelected(item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType) {
        switch type {
        case .subject:
            self.subjectID = item.id
        case .level:
            self.levelID = item.id
        case .type:
            self.typeID = item.id
        }
        self.view?.updateView(with: item, type: type)
        self.onValidateEnableStateButtonSave()
        
        print("didSelected: ", item.id)
    }
}
