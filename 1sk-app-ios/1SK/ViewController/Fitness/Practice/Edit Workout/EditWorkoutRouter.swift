//
//  
//  EditWorkoutRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class EditWorkoutRouter: BaseRouter {
//    weak var viewController: EditWorkoutViewController?
    static func setupModule(with state: StateViewScreen, workout: WorkoutModel?, delegate: EditWorkoutPresenterDelegate?) -> EditWorkoutViewController {
        let viewController = EditWorkoutViewController()
        let router = EditWorkoutRouter()
        let interactorInput = EditWorkoutInteractorInput()
        let presenter = EditWorkoutPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.state = state
        presenter.workout = workout
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
    
}

// MARK: - EditWorkoutRouterProtocol
extension EditWorkoutRouter: EditWorkoutRouterProtocol {
    func showWorkoutSelectionViewController(with type: WorkoutSelectionViewController.ItemType, selectedID: Int?, delegate: WorkoutSelectionViewControllerDelegate?) {
        let workoutSelectionItemViewController = WorkoutSelectionViewController.loadFromNib()
        workoutSelectionItemViewController.type = type
        workoutSelectionItemViewController.delegate = delegate
        workoutSelectionItemViewController.selectedID = selectedID
        workoutSelectionItemViewController.modalPresentationStyle = .overFullScreen
        viewController?.present(workoutSelectionItemViewController, animated: false, completion: nil)
    }

    func pop() {
        viewController?.navigationController?.popViewController(animated: true)
    }
}
