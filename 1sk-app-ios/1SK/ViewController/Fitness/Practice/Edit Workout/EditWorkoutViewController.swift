//
//  
//  EditWorkoutViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class EditWorkoutViewController: BaseViewController {
    
    @IBOutlet weak var tf_sk_Name: SKTextField!
    @IBOutlet weak var tf_sk_Subject: SKTextField!
    @IBOutlet weak var tf_sk_Level: SKTextField!
    @IBOutlet weak var tf_sk_Type: SKTextField!
    @IBOutlet weak var errorNetworkView: UIView!
    
    private let mutiplerNumber = 100 // mutilplier data to make infinity date picker

    private var tf_NameWorkout: UITextField {
        return self.tf_sk_Name.titleTextField
    }
    
    var presenter: EditWorkoutPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
        if gIsInternetConnectionAvailable {
            self.errorNetworkView.removeFromSuperview()
        }
    }
    
    private func setupDefaults() {
        self.setupTextField()
        self.setupNavigation()
    }

    private func setupNavigation() {
        self.navigationItem.title = "Tạo bài tập"
        self.setRightBarButton(style: .saveDisable, handler: nil)
        self.navigationItem.rightBarButtonItem?.isEnabled = false
    }

    private func setupTextField() {
        self.tf_NameWorkout.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        self.tf_sk_Name.setKeyboardType(.alphabet)
        self.tf_NameWorkout.delegate = self
        self.tf_sk_Subject.delegate = self
        self.tf_sk_Level.delegate = self
        self.tf_sk_Type.delegate = self

    }
    
    // MARK: - Action
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.presenter.onNameTextFieldTextDidChanged(with: textField.text ?? "")
        self.updateTypeLabelHiddenState(of: textField)
    }
    
    @IBAction func handleReload(_ sender: Any) {
        if gIsInternetConnectionAvailable {
            self.errorNetworkView.removeFromSuperview()
        }
    }
    
    // MARK: - Helper
    private func updateTypeLabelHiddenState(of textField: UITextField) {
        switch textField {
        case self.tf_NameWorkout:
            self.tf_sk_Name.updateTypeLabelHiddenState()
        default:
            break
        }
    }
    
}

// MARK: - EditWorkoutViewProtocol
extension EditWorkoutViewController: EditWorkoutViewProtocol {
    func setInitViewState(with workout: WorkoutModel?, state: StateViewScreen) {
        let isEdit = state != .normal
        [self.tf_sk_Name, self.tf_sk_Subject, self.tf_sk_Level, self.tf_sk_Type].forEach({ $0?.setEnable(isEnable: isEdit)})
        
        if state == .edit {
            self.navigationItem.title = "Sửa thông tin"
            
            self.tf_NameWorkout.text = workout?.name
            self.tf_sk_Name.updateTypeLabelHiddenState()

            if let subject = workout?.subject {
                self.updateView(with: subject, type: .subject)
            }

            if let level = workout?.level {
                self.updateView(with: level, type: .level)
            }

            if let type = workout?.type {
                self.updateView(with: type, type: .type)
            }
        }
    }
    
    func updateView(with item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType) {
        switch type {
        case .subject:
            self.tf_sk_Subject.setValue(with: item.name)
        case .type:
            self.tf_sk_Type.setValue(with: item.name)
        case .level:
            self.tf_sk_Level.setValue(with: item.name)
        }
    }
    
    func onUpdateStateEnableButtonSave(_ isEnable: Bool, viewState: StateViewScreen) {
        guard isEnable != self.navigationItem.rightBarButtonItem?.isEnabled else {
            return
        }

        if isEnable {
            self.setRightBarButton(style: .saveEnable) { [weak self] _ in
                self?.navigationItem.rightBarButtonItem?.isEnabled = true
                
                switch viewState {
                case .new:
                    self?.presenter.onSaveCreateWorkoutAction()

                case .edit:
                    self?.presenter.onSaveUpdateWorkoutAction()

                default:
                    break
                }
            }
        } else {
            self.setRightBarButton(style: .saveDisable, handler: nil)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }

}

// MARK: - UITextFieldDelegate
extension EditWorkoutViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case self.tf_NameWorkout:
            break
        default:
            break
        }
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
        if textField == self.tf_NameWorkout {
            //
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField {
        case self.tf_NameWorkout:
            return true
            
//            var `string` = string
//            string.removeAll(where: {$0 == " "})
//            if string.rangeOfCharacter(from: .letters) != nil || string == ""{
//                return true
//            } else {
//                return false
//            }
        default:
            break
        }
        return true
    }
}

// MARK: - SKSelectedLabelDelegate
extension EditWorkoutViewController: SKSelectedLabelDelegate {
    func didSelect(_ scrollDownLabel: SKTextField) {
        view.endEditing(true)
        switch scrollDownLabel {
        case self.tf_sk_Subject:
            self.presenter.onSelectedLabelDidSelected(with: .subject)
        case self.tf_sk_Level:
            self.presenter.onSelectedLabelDidSelected(with: .level)
        case self.tf_sk_Type:
            self.presenter.onSelectedLabelDidSelected(with: .type)
        default:
            break
        }
    }
}
