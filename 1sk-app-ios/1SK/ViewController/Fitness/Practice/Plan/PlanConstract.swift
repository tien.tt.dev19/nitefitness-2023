//
//  
//  PlanConstract.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

// MARK: View -
protocol PlanViewProtocol: AnyObject {

}

// MARK: Interactor -
protocol PlanInteractorInputProtocol {

}

protocol PlanInteractorOutputProtocol: AnyObject {
}
// MARK: Presenter -
protocol PlanPresenterProtocol {
    func onViewDidLoad()
    
}

// MARK: Router -
protocol PlanRouterProtocol {

}
