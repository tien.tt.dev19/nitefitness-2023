//
//  
//  PlanPresenter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class PlanPresenter {

    weak var view: PlanViewProtocol?
    private var interactor: PlanInteractorInputProtocol
    private var router: PlanRouterProtocol

    init(interactor: PlanInteractorInputProtocol,
         router: PlanRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - PlanPresenterProtocol
extension PlanPresenter: PlanPresenterProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - PlanPresenter: PlanInteractorOutput -
extension PlanPresenter: PlanInteractorOutputProtocol {

}
