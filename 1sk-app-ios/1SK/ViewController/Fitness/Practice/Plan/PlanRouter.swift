//
//  
//  PlanRouter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class PlanRouter {
    weak var viewController: PlanViewController?
    static func setupModule() -> PlanViewController {
        let viewController = PlanViewController()
        let router = PlanRouter()
        let interactorInput = PlanInteractorInput()
        let presenter = PlanPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - PlanRouterProtocol
extension PlanRouter: PlanRouterProtocol {
    
}
