//
//  
//  PlanViewController.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class PlanViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private lazy var header = viewHeaderTableView()
    
    var presenter: PlanPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
//        self.setupTableView()
    }
    
    
}

// MARK: - PlanViewProtocol
extension PlanViewController: PlanViewProtocol {
    
}

// MARK: - UITableViewDataSource
extension PlanViewController: UITableViewDataSource {
    private func viewHeaderTableView() -> PlanHeaderTableView {
        let header = PlanHeaderTableView.loadFromNib()
        header.translatesAutoresizingMaskIntoConstraints = true
        header.autoresizingMask = .flexibleWidth
        header.frame = CGRect(origin: .zero, size: CGSize(width: Constant.Screen.width, height: 43))
        //header.delegate = self
        return header
    }
    
    private func setupTableView() {
        self.tableView.registerNib(ofType: CellTableViewWorkout.self)
        //self.tableView.registerNib(ofType: LoadmoreTableViewCell.self)
        self.tableView.dataSource = self
        //self.tbv_SampleList.prefetchDataSource = self
        self.tableView.delegate = self
        self.tableView.tableHeaderView = self.viewHeaderTableView()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewWorkout.self, for: indexPath)
        return cell
    }
    
    
}

// MARK: - UITableViewDelegate
extension PlanViewController: UITableViewDelegate {
    
}
