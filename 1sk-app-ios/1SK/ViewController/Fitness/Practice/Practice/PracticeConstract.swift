//
//  
//  PracticeConstract.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

// MARK: View -
protocol PracticeViewProtocol: AnyObject {
    func setCreateNewItemViewHidden(_ isHidden: Bool)
    func onDidSaveWorkoutSuccess(workout: WorkoutModel)
}

// MARK: Interactor -
protocol PracticeInteractorInputProtocol {

}

protocol PracticeInteractorOutputProtocol: AnyObject {
    
}

// MARK: Presenter -
protocol PracticePresenterProtocol {
    func onViewDidLoad()
    func onButtonBackDidTapped()
    func onButtonAddNewItemDidTapped()
    func onButtonCreateWorkoutDidTapped()
    func onButtonCreatePlanDidTapped()
    func onBackgroundDidTapped()
}

// MARK: Router -
protocol PracticeRouterProtocol {
    func pop()
//    func gotoWorkoutViewController(with state: StateViewScreen, workout: WorkoutModel?, delegate: CreateWorkoutPresenterDelegate?)
    
    // vuongbachthu
    func gotoEditWorkoutViewController(with state: StateViewScreen, workout: WorkoutModel?, delegate: EditWorkoutPresenterDelegate?)
}
