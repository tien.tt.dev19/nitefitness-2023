//
//  
//  PracticePresenter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class PracticePresenter {

    weak var view: PracticeViewProtocol?
    private var interactor: PracticeInteractorInputProtocol
    private var router: PracticeRouterProtocol

    init(interactor: PracticeInteractorInputProtocol,
         router: PracticeRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - PracticePresenterProtocol
extension PracticePresenter: PracticePresenterProtocol {
    func onViewDidLoad() {

    }

    func onButtonBackDidTapped() {
        router.pop()
    }

    func onButtonAddNewItemDidTapped() {
        view?.setCreateNewItemViewHidden(false)
    }

    func onButtonCreateWorkoutDidTapped() {
        //router.gotoWorkoutViewController(with: .new, workout: nil, delegate: self)
        self.router.gotoEditWorkoutViewController(with: .new, workout: nil, delegate: self)
        self.view?.setCreateNewItemViewHidden(true)
    }

    func onButtonCreatePlanDidTapped() {
        view?.setCreateNewItemViewHidden(true)
    }

    func onBackgroundDidTapped() {
        view?.setCreateNewItemViewHidden(true)
    }
}

// MARK: - PracticePresenter: PracticeInteractorOutput -
extension PracticePresenter: PracticeInteractorOutputProtocol {

}

// MARK: - CreateWorkoutPresenterDelegate
extension PracticePresenter: CreateWorkoutPresenterDelegate {
    func didCreateWorkoutSuccess(_ workout: WorkoutModel) {
        
    }
}

// MARK: - EditWorkoutPresenterDelegate
extension PracticePresenter: EditWorkoutPresenterDelegate {
    func onDidSaveWorkoutSuccess(workout: WorkoutModel) {
        self.view?.onDidSaveWorkoutSuccess(workout: workout)
    }
}
