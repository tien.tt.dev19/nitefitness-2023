//
//  
//  PracticeRouter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class PracticeRouter {
    weak var viewController: PracticeViewController?
    static func setupModule() -> PracticeViewController {
        let viewController = PracticeViewController()
        let router = PracticeRouter()
        let interactorInput = PracticeInteractorInput()
        let presenter = PracticePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        viewController.hidesBottomBarWhenPushed = true
        return viewController
    }
}

// MARK: - PracticeRouterProtocol
extension PracticeRouter: PracticeRouterProtocol {
    func pop() {
        viewController?.navigationController?.popViewController(animated: true)
    }
    
    // vuongbachthu
    func gotoEditWorkoutViewController(with state: StateViewScreen, workout: WorkoutModel?, delegate: EditWorkoutPresenterDelegate?) {
        let createWorkoutViewController = EditWorkoutRouter.setupModule(with: state, workout: workout, delegate: delegate)
        self.viewController?.navigationController?.pushViewController(createWorkoutViewController, animated: true)
    }
}
