//
//  
//  PracticeViewController.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class PracticeViewController: BaseViewController {

    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var addNewItemView: UIView!
    @IBOutlet weak var addNewItemViewHeightConstraint: NSLayoutConstraint!
    
    private var workoutViewController: WorkoutViewController!
    private var planViewController: PlanViewController!
    private lazy var pageViewController: VXPageViewController = VXPageViewController()

    var presenter: PracticePresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setupPageViewController()
        self.addNewItemViewHeightConstraint.constant = 0
        self.transparentView.isHidden = true
        self.isInteractivePopGestureEnable = true
        self.titleView.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }

    private func setupPageViewController() {
        self.workoutViewController = WorkoutRouter.setupModule()
        self.planViewController = PlanRouter.setupModule()
        //
        let barConfig = VXPageBarConfig(height: 48,
                                      selectedColor: R.color.darkText(),
                                      unSelectedColor: R.color.subTitle(),
                                      selectedFont: R.font.robotoMedium(size: 16),
                                      unSelectedFont: R.font.robotoRegular(size: 16),
                                      underLineHeight: 3,
                                      underLineWidth: 105,
                                      underLineColor: R.color.mainColor(),
                                      backgroundColor: .white)
        let workoutPageItem = VXPageItem(viewController: self.workoutViewController, title: "Bài tập")
        let planPageItem = VXPageItem(viewController: self.planViewController, title: "Kế hoạch tập")
        self.pageViewController.setPageItems([workoutPageItem, planPageItem])
        self.pageViewController.setPageBarConfig(barConfig)

        self.addChild(self.pageViewController)
        self.containerView.addSubview(self.pageViewController.view)
        self.pageViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.pageViewController.didMove(toParent: self)
    }
    
    // MARK: - Action
    @IBAction func buttonBackDidTapped(_ sender: Any) {
        self.presenter.onButtonBackDidTapped()
    }

    @IBAction func buttonAddNewItemDidTapped(_ sender: Any) {
        self.presenter.onButtonAddNewItemDidTapped()
    }
    
    @IBAction func buttonCreateWorkoutDidTapped(_ sender: Any) {
        self.presenter.onButtonCreateWorkoutDidTapped()
    }

    @IBAction func buttonCreatePlanDidTapped(_ sender: Any) {
        self.presenter.onButtonCreatePlanDidTapped()
    }

    @IBAction func backgroundDidTapped(_ sender: Any) {
        self.presenter.onBackgroundDidTapped()
    }
}

// MARK: - PracticeViewProtocol
extension PracticeViewController: PracticeViewProtocol {
    func setCreateNewItemViewHidden(_ isHidden: Bool) {
        self.addNewItemViewHeightConstraint.constant = isHidden ? 0 : 75
        if !isHidden {
            self.transparentView.isHidden = false
        }
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            if isHidden {
                self.transparentView.isHidden = true
            }
        })
    }
    
    func onDidSaveWorkoutSuccess(workout: WorkoutModel) {
        self.workoutViewController.onDidCreateWorkoutSuccess(workout: workout)
    }
}
