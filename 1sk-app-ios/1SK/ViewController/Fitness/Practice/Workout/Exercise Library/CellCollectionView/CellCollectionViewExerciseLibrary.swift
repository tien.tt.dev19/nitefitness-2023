//
//  CellCollectionViewExerciseLibrary.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//

import UIKit

protocol CellCollectionViewExerciseLibraryDelegate: AnyObject {
    func onDidSelectSubjectCell(subject: SubjectModel, indexPath: IndexPath)
}

class CellCollectionViewExerciseLibrary: UICollectionViewCell {
    @IBOutlet weak var view_BgContent: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    weak var delegate: CellCollectionViewExerciseLibraryDelegate?
    
    private var subject: SubjectModel?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.view_BgContent.cornerRadius = self.view_BgContent.frame.height/2
        
        self.view_BgContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchSelectSubjectCell)))
    }
    
    @objc func onTouchSelectSubjectCell() {
        if self.subject?.isSelected != true {
            self.subject?.isSelected = true
            self.view_BgContent.backgroundColor = R.color.mainColor()
            self.lbl_Title.textColor = .white
            
            self.delegate?.onDidSelectSubjectCell(subject: self.subject!, indexPath: self.indexPath!)
        }
    }
    
    func setDataCell(subject: SubjectModel) {
        self.subject = subject
        self.lbl_Title.text = subject.name
        
        if subject.isSelected == true {
            self.view_BgContent.backgroundColor = R.color.mainColor()
            self.lbl_Title.textColor = .white
            
        } else {
            self.view_BgContent.backgroundColor = R.color.bgCell()
            self.lbl_Title.textColor = R.color.text_2()
        }
    }
}
