//
//  
//  ExerciseLibraryConstract.swift
//  1SK
//
//  Created by vuongbachthu on 6/30/21.
//
//

import UIKit

// MARK: - View
protocol ExerciseLibraryViewProtocol: AnyObject {
    func setBageFilter(bage: Int)
    
    // MARK: - CollectionView
    func onReloadColl()
    
    // MARK: - TableView
    func onReloadTable()
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
    func onSetNotFoundView(isHidden: Bool)
    
}

// MARK: - Presenter
protocol ExerciseLibraryPresenterProtocol {
    func onViewDidLoad()
    
    // MARK: - ActionView
    func onSearchExerciseAction()
    func onFilterExerciseAction()
    
    // MARK: - CollectionView
    func numberOfItemsInSectionColl(in section: Int) -> Int
    func onDidSelectRowColl(at index: IndexPath)
    func onCellForItemColl(at index: IndexPath) -> SubjectModel
    func onDidSelectSubjectCell(subject: SubjectModel)
    func onDeselectedSubject(index: Int)
    
    
    // MARK: - TableView
    func numberOfItemsInSectionTable(in section: Int) -> Int
    func onCellForItemTable(at index: IndexPath) -> WorkoutModel
    func onDidSelectRowTable(at index: IndexPath)
    func onLoadMoreAction()
}

// MARK: - Interactor Input
protocol ExerciseLibraryInteractorInputProtocol {
    func getFilterValues()
    func getSubjectValue()
    func getWorkoutLibrary(filter: Filter)
}

// MARK: - Interactor Output
protocol ExerciseLibraryInteractorOutputProtocol: AnyObject {
    func onGetSubjectValueFinished(with result: Result<[SubjectModel], APIError>, total: Int)
    func onGetFilterValuesFinished(with result: Result<FilterDataValue, APIError>, total: Int)
    func onGetWorkoutLibraryFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol ExerciseLibraryRouterProtocol: BaseRouterProtocol {
    func gotoSearchExerciseViewController(with delegate: SearchExercisePresenterDelegate?)
    func gotoFilterExserciseViewController(with listFilterValues: [FilterAttributeModel], delegate: FilterExercisePresenterDelegate?)
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?)
}
