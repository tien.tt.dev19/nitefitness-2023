//
//  
//  ExerciseLibraryInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 6/30/21.
//
//

import UIKit

class ExerciseLibraryInteractorInput {
    weak var output: ExerciseLibraryInteractorOutputProtocol?
    var exserciseService: ExerciseServiceProtocol?
}

// MARK: - ExerciseLibraryInteractorInputProtocol
extension ExerciseLibraryInteractorInput: ExerciseLibraryInteractorInputProtocol {
    func getWorkoutLibrary(filter: Filter) {
        self.exserciseService?.getListWorkoutLibrary(filter: filter, completion: { [weak self] result in
            self?.output?.onGetWorkoutLibraryFinished(with: result.unwrapSuccessModel(), page: filter.page!, total: result.getTotal() ?? 0)
        })
    }
    
    func getSubjectValue() {
        self.exserciseService?.getListSubject(completion: { [weak self] result in
            self?.output?.onGetSubjectValueFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0)
        })
    }
    
    func getFilterValues() {
        self.exserciseService?.getFilterValueExerciseWorkour(completion: { [weak self] result in
            self?.output?.onGetFilterValuesFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0)
        })
    }
}
