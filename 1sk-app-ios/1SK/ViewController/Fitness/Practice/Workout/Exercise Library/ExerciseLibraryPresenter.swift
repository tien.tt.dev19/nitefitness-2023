//
//  
//  ExerciseLibraryPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 6/30/21.
//
//

import UIKit

protocol ExerciseLibraryPresenterDelegate: AnyObject {
    func onBookmarkWorkoutLibrarySuccess(workout: WorkoutModel)
    func onCloneWorkoutLibrarySuccess(at workout: WorkoutModel)
}

class ExerciseLibraryPresenter {

    weak var view: ExerciseLibraryViewProtocol?
    private var interactor: ExerciseLibraryInteractorInputProtocol
    private var router: ExerciseLibraryRouterProtocol

    weak var delegate: ExerciseLibraryPresenterDelegate?
    
    private var listFilterValues: [FilterAttributeModel] = []
    private var listSubjectValue: [SubjectModel] = []
    private var listWorkoutLibrary: [WorkoutModel] = []
    
    private var filter = Filter()
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    private var bageNumber = 0
    
    init(interactor: ExerciseLibraryInteractorInputProtocol,
         router: ExerciseLibraryRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    private func getData(in page: Int) {
        self.filter.page = page
        self.filter.limit = self.limit
        self.filter.sort = "DESC"
        self.filter.order = "created_at"
        
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        self.router.showHud()
        self.interactor.getWorkoutLibrary(filter: self.filter)
    }
    
    func checkDataNotFound() {
        // Set Status View Data Not Found
        if self.listWorkoutLibrary.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - ExerciseLibraryPresenterProtocol
extension ExerciseLibraryPresenter: ExerciseLibraryPresenterProtocol {
    
    func onViewDidLoad() {
        self.interactor.getSubjectValue()
        self.interactor.getFilterValues()
        
        self.getData(in: 1)
    }
    
    // MARK: - ActionView
    func onSearchExerciseAction() {
        self.router.gotoSearchExerciseViewController(with: self)
    }
    
    func onFilterExerciseAction() {
        self.router.gotoFilterExserciseViewController(with: self.listFilterValues, delegate: self)
    }
    
    // MARK: - CollectionView
    func numberOfItemsInSectionColl(in section: Int) -> Int {
        return self.listSubjectValue.count
    }
    
    func onCellForItemColl(at index: IndexPath) -> SubjectModel {
        return self.listSubjectValue[index.row]
    }
    
    func onDidSelectRowColl(at index: IndexPath) {
        
    }
    
    func onDidSelectSubjectCell(subject: SubjectModel) {
        if !self.filter.subject.isEmpty {
            self.filter.subject.removeAll()
        }
        self.filter.subject.append(String(subject.id))
        self.getData(in: 1)
    }
    
    func onDeselectedSubject(index: Int) {
        self.listSubjectValue[index].isSelected = false
    }
    
    
    // MARK: - TableView
    func numberOfItemsInSectionTable(in section: Int) -> Int {
        return self.listWorkoutLibrary.count
    }
    
    func onCellForItemTable(at index: IndexPath) -> WorkoutModel {
        return self.listWorkoutLibrary[index.row]
    }
    
    func onDidSelectRowTable(at index: IndexPath) {
        self.router.gotoDetailWorkoutViewController(with: self.listWorkoutLibrary[index.row], delegate: self)
    }
    
    func onLoadMoreAction() {
        if self.listWorkoutLibrary.count >= 10 && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
}

// MARK: - ExerciseLibraryInteractorOutput 
extension ExerciseLibraryPresenter: ExerciseLibraryInteractorOutputProtocol {
    func onGetSubjectValueFinished(with result: Result<[SubjectModel], APIError>, total: Int) {
        switch result {
        case .success(let listSubject):
            self.listSubjectValue = listSubject
            
            let subject = SubjectModel.init(id: 0, name: "Tất cả")
            subject.isSelected = true
            self.listSubjectValue.insert(subject, at: 0)
            
            self.view?.onReloadColl()
            self.router.hideErrorView()
            
        case .failure:
            self.router.showErrorView(with: nil, delegate: self)
        }
        
    }
    
    func onGetFilterValuesFinished(with result: Result<FilterDataValue, APIError>, total: Int) {
        switch result {
        case .success(let filterValues):
            let typesValue = FilterAttributeModel(types: filterValues.types)
            let toolsValue = FilterAttributeModel(tools: filterValues.tools)
            self.listFilterValues = [typesValue, toolsValue]
            self.router.hideErrorView()
        case .failure:
            self.router.showErrorView(with: nil, delegate: self)
        }
        
    }
    
    func onGetWorkoutLibraryFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listWorkout):
            if page <= 1 {
                self.listWorkoutLibrary = listWorkout
                self.view?.onReloadTable()
                
            } else {
                for object in listWorkout {
                    self.listWorkoutLibrary.append(object)
                    self.view?.onInsertRow(at: self.listWorkoutLibrary.count - 1)
                }
            }
            self.total = total
            
            // Set Out Of Data
            if self.listWorkoutLibrary.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
}

// MARK: - FilterExercisePresenterDelegate
/** Các selected filter sẽ tự động tham chiếu về đây */
extension ExerciseLibraryPresenter: FilterExercisePresenterDelegate {
    func onDidUnfilteredAction() {
        self.filter.type.removeAll()
        self.filter.tool.removeAll()
        self.filter.filter.removeAll()
        self.bageNumber = 0
        
        self.view?.setBageFilter(bage: self.bageNumber)
        self.getData(in: 1)
    }
    
    func onDidFilterAction() {
        self.filter.type.removeAll()
        self.filter.tool.removeAll()
        self.filter.filter.removeAll()
        self.bageNumber = 0
        
        for item in self.listFilterValues {
            if item.type == "type" {
                for value in item.values! {
                    if value.isSelected == true {
                        self.filter.type.append(String(value.id ?? 0))
                        self.bageNumber += 1
                    }
                }
            }
            if item.type == "tool" {
                for value in item.values! {
                    if value.isSelected == true {
                        self.filter.tool.append(String(value.id ?? 0))
                        self.bageNumber += 1
                    }
                }
            }
            if item.type == "filter" {
                for value in item.values! {
                    if value.isSelected == true {
                        self.filter.filter.append(String(value.id ?? 0))
                        self.bageNumber += 1
                    }
                }
            }
        }
        
        self.view?.setBageFilter(bage: self.bageNumber)
        self.getData(in: 1)
        
        print("\n-------------------------------------------")
        for filterItem in self.listFilterValues {
            print("\nonDidFilterAction: type:", filterItem.type ?? "null")
            for valueItem in filterItem.values! {
                print("onDidFilterAction: value: \(valueItem.name!) - \(valueItem.isSelected ?? false)")
            }
        }
        print("\n==========================================\n")
        
    }
}

// MARK: - ErrorViewDelegate
extension ExerciseLibraryPresenter: ErrorViewDelegate {
    func onRetryButtonDidTapped(_ errorView: UIView) {
        print("onRetryButtonDidTapped")
    }
}

// MARK: - MyWorkoutPresenterDelegate
extension ExerciseLibraryPresenter: MyWorkoutPresenterDelegate {
    func onBookmarkWorkoutSuccess(at workout: WorkoutModel) {
        for (index, _workout) in self.listWorkoutLibrary.enumerated() {
            if _workout.id == workout.id {
                self.listWorkoutLibrary[index] = workout
                break
            }
        }
        self.view?.onReloadTable()
        
        self.delegate?.onBookmarkWorkoutLibrarySuccess(workout: workout)
    }
    
    func onDeleteWorkoutSuccess(at workoutId: Int?) {
        print("onDeleteWorkoutSuccess")
    }
    
    func onUpdateWorkoutSuccess(at workout: WorkoutModel) {
        // Did Edit Workout when clone
        self.delegate?.onCloneWorkoutLibrarySuccess(at: workout)
    }
    
    func onCloneWorkoutSuccess(at workout: WorkoutModel) {
        self.delegate?.onCloneWorkoutLibrarySuccess(at: workout)
    }
}

// MARK: - SearchExercisePresenterDelegate
extension ExerciseLibraryPresenter: SearchExercisePresenterDelegate {
    func onBookmarkWorkoutSearchSuccess(workout: WorkoutModel) {
        for (index, _workout) in self.listWorkoutLibrary.enumerated() {
            if _workout.id == workout.id {
                self.listWorkoutLibrary[index] = workout
                break
            }
        }
        self.view?.onReloadTable()
        self.delegate?.onBookmarkWorkoutLibrarySuccess(workout: workout)
    }
    
    func onCloneWorkoutSearchSuccess(at workout: WorkoutModel) {
        self.delegate?.onCloneWorkoutLibrarySuccess(at: workout)
    }
}
