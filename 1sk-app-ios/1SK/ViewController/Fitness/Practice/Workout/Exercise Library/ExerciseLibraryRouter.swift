//
//  
//  ExerciseLibraryRouter.swift
//  1SK
//
//  Created by vuongbachthu on 6/30/21.
//
//

import UIKit

class ExerciseLibraryRouter: BaseRouter {
//    weak var viewController: ExerciseLibraryViewController?
    static func setupModule(with delegate: ExerciseLibraryPresenterDelegate?) -> ExerciseLibraryViewController {
        let viewController = ExerciseLibraryViewController()
        let router = ExerciseLibraryRouter()
        let interactorInput = ExerciseLibraryInteractorInput()
        let presenter = ExerciseLibraryPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exserciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - ExerciseLibraryRouterProtocol
extension ExerciseLibraryRouter: ExerciseLibraryRouterProtocol {
    func gotoSearchExerciseViewController(with delegate: SearchExercisePresenterDelegate?) {
        let controller = SearchExerciseRouter.setupModule(with: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoFilterExserciseViewController(with listFilterValues: [FilterAttributeModel], delegate: FilterExercisePresenterDelegate?) {
        let controller = FilterExerciseRouter.setupModule(with: listFilterValues, delegate: delegate)
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?) {
        let workoutViewController = MyWorkoutRouter.setupModule(with: workout, delegate: delegate)
        self.viewController?.navigationController?.pushViewController(workoutViewController, animated: true)
    }
}
