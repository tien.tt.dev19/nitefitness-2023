//
//  
//  ExerciseLibraryViewController.swift
//  1SK
//
//  Created by vuongbachthu on 6/30/21.
//
//

import UIKit

class ExerciseLibraryViewController: BaseViewController {
    @IBOutlet weak var view_BgFilter: UIView!
    @IBOutlet weak var view_Bage: UIView!
    @IBOutlet weak var lbl_Bage: UILabel!
    
    @IBOutlet weak var coll_Category: UICollectionView!
    @IBOutlet weak var tbv_Exercise: UITableView!
    
    @IBOutlet weak var view_NotFound: UIView!
    
    var presenter: ExerciseLibraryPresenterProtocol!
    
    var indexPathSubject: IndexPath?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.title = "Thư viện bài tập"
        self.setButtonSearchExercise()
        self.setInitCollectionView()
        self.setInitTableView()
        self.setBageFilter(bage: 0)
    }
    
    // MARK: - Action
    @IBAction func onFilterExerciseAction(_ sender: Any) {
        self.presenter.onFilterExerciseAction()
    }
    
}

// MARK: - UIBarButtonItem
extension ExerciseLibraryViewController {
    private func setButtonSearchExercise() {
        let btn_Search = UIBarButtonItem(image: UIImage(named: "ic_search"), style: .plain, target: self, action: #selector(self.onSearchExerciseAction))
        self.navigationItem.setRightBarButton(btn_Search, animated: true)
    }
    @objc func onSearchExerciseAction() {
        self.presenter.onSearchExerciseAction()
    }
}

// MARK: - UICollectionViewDataSource
extension ExerciseLibraryViewController: UICollectionViewDataSource {
    func setInitCollectionView() {
        self.coll_Category.registerNib(ofType: CellCollectionViewExerciseLibrary.self)
        self.coll_Category.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.estimatedItemSize = CGSize(width: 58, height: 32)
        layout.minimumInteritemSpacing = 8
        self.coll_Category.collectionViewLayout = layout
        
        self.indexPathSubject = IndexPath(row: 0, section: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let row = self.presenter.numberOfItemsInSectionColl(in: section)
        return row
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewExerciseLibrary.self, for: indexPath)
        let subject = self.presenter.onCellForItemColl(at: indexPath)
        cell.delegate = self
        cell.indexPath = indexPath
        cell.setDataCell(subject: subject)
        
        return cell
    }
}

// MARK: - CellCollectionViewExerciseLibraryDelegate
extension ExerciseLibraryViewController: CellCollectionViewExerciseLibraryDelegate {
    func onDidSelectSubjectCell(subject: SubjectModel, indexPath: IndexPath) {
        self.presenter.onDidSelectSubjectCell(subject: subject)
        self.presenter.onDeselectedSubject(index: self.indexPathSubject!.row)
        
        if let cell = self.coll_Category.cellForItem(at: self.indexPathSubject!) as? CellCollectionViewExerciseLibrary {
            cell.view_BgContent.backgroundColor = R.color.bgCell()
            cell.lbl_Title.textColor = R.color.text_2()
        }
        self.indexPathSubject = indexPath
    }
}


// MARK: - UITableViewDataSource
extension ExerciseLibraryViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_Exercise.registerNib(ofType: CellTableViewWorkout.self)
        self.tbv_Exercise.dataSource = self
        self.tbv_Exercise.delegate = self
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItemsInSectionTable(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewWorkout.self, for: indexPath)
        
        let workout = self.presenter.onCellForItemTable(at: indexPath)
        cell.config(with: workout)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ExerciseLibraryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRowTable(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}

// MARK: - ExerciseLibraryViewProtocol
extension ExerciseLibraryViewController: ExerciseLibraryViewProtocol {
    func onReloadColl() {
        self.coll_Category.reloadData()
    }
    
    func onReloadTable() {
        self.tbv_Exercise.reloadData()
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_Exercise.beginUpdates()
        self.tbv_Exercise.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_Exercise.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func setBageFilter(bage: Int) {
        if bage == 0 {
            self.view_Bage.isHidden = true
            self.lbl_Bage.text = "0"
            
        } else {
            self.view_Bage.isHidden = false
            self.lbl_Bage.text = bage.asString
        }
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
}
