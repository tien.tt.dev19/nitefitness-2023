//
//  CellCollectionViewFilter.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//

import UIKit

protocol CellCollectionViewFilterDelegate: AnyObject {
    func onDidSelectValueFilterAction()
}

class CellCollectionViewFilter: UICollectionViewCell {

    @IBOutlet weak var view_BgContent: UIView!
    @IBOutlet weak var btn_CheckBox: UIButton!
    @IBOutlet weak var lbl_Name: UILabel!
    
    weak var delegate: CellCollectionViewFilterDelegate?
    var valuesModel: ValuesModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.view_BgContent.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onCheckBoxAction(_:))))
    }
    
    @IBAction func onCheckBoxAction(_ sender: Any) {
        self.btn_CheckBox.isSelected = !self.btn_CheckBox.isSelected
        
        if self.btn_CheckBox.isSelected == true {
            self.valuesModel?.isSelected = true
            
        } else {
            self.valuesModel?.isSelected = false
        }
        
        self.delegate?.onDidSelectValueFilterAction()
    }
    
    func setDataCell(value: ValuesModel?) {
        self.valuesModel = value
        self.lbl_Name.text = value?.name
        
        if value?.isSelected == true {
            self.btn_CheckBox.isSelected = true
            
        } else {
            self.btn_CheckBox.isSelected = false
        }
    }
}
