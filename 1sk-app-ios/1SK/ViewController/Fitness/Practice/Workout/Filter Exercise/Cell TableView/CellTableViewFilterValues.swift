//
//  CellTableViewFilterValues.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//

import UIKit

protocol CellTableViewFilterValuesDelegate: AnyObject {
    func onDidSelectValueFilterAction()
}

class CellTableViewFilterValues: UITableViewCell {
    
    @IBOutlet weak var coll_FilterValue: UICollectionView!
    
    weak var delegate: CellTableViewFilterValuesDelegate?
    
    var filterValuesModel: FilterAttributeModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitCollectionView()
    }
    
    override func prepareForReuse() {
        self.filterValuesModel = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func onReloadData() {
        self.coll_FilterValue.reloadData()
    }
}

// MARK: - UICollectionViewDataSource
extension CellTableViewFilterValues: UICollectionViewDataSource {
    func setInitCollectionView() {
        self.coll_FilterValue.registerNib(ofType: CellCollectionViewFilter.self)
        self.coll_FilterValue.dataSource = self
        self.coll_FilterValue.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filterValuesModel?.values?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewFilter.self, for: indexPath)
        let valueModel = self.filterValuesModel?.values?[indexPath.row]
        cell.delegate = self
        cell.setDataCell(value: valueModel)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension CellTableViewFilterValues: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (self.coll_FilterValue.width / 2) - 8, height: 24)
    }
}

// MARK: - UICollectionViewDelegate
extension CellTableViewFilterValues: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

// MARK: - CellCollectionViewFilterDelegate
extension CellTableViewFilterValues: CellCollectionViewFilterDelegate {
    func onDidSelectValueFilterAction() {
//        print("onDidSelectValueFilterAction: \n\n")
        
//        for item in self.filterValuesModel!.values! {
//            print("onDidSelectValueFilterAction: name!: \(item.name!) - isSelected: \(item.isSelected ?? false)" )
//        }
        
        self.delegate?.onDidSelectValueFilterAction()
    }
}
