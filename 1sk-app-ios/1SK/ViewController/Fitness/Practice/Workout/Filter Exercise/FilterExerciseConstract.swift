//
//  
//  FilterExerciseConstract.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

// MARK: - View
protocol FilterExerciseViewProtocol: AnyObject {
    func onDidDismissViewAction()
    func onUpdateStateSkipEnabled(_ isEnabled: Bool)
}

// MARK: - Presenter
protocol FilterExercisePresenterProtocol {
    func onViewDidLoad()
    
    func onUnfilteredAction()
    func onFilterAction()
    
    // MARK: - TableView
    func numberOfSections() -> Int
    func numberOfItemsInSection(in section: Int) -> Int
    func heightForRow(at index: IndexPath) -> CGFloat?
    func itemForHeader(at section: Int) -> FilterAttributeModel?
    func itemForRow(at index: IndexPath) -> ValuesModel?
    func onDidSelectCellFilter()
}

// MARK: - Interactor Input
protocol FilterExerciseInteractorInputProtocol {
    
}

// MARK: - Interactor Output
protocol FilterExerciseInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol FilterExerciseRouterProtocol {

}
