//
//  
//  FilterExercisePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

protocol FilterExercisePresenterDelegate: AnyObject {
    func onDidUnfilteredAction()
    func onDidFilterAction()
}

class FilterExercisePresenter {

    weak var view: FilterExerciseViewProtocol?
    private var interactor: FilterExerciseInteractorInputProtocol
    private var router: FilterExerciseRouterProtocol
    weak var delegate: FilterExercisePresenterDelegate?
    
    var listFilterValues: [FilterAttributeModel] = []
    
    init(interactor: FilterExerciseInteractorInputProtocol,
         router: FilterExerciseRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    func setCheckStateFilter() {
        var isSelected = false
        
        print("\n-------------------------------------------")
        for filterItem in self.listFilterValues {
            print("\nonDidSelectCellFilter: type:", filterItem.type ?? "null")
            for valueItem in filterItem.values! {
                if valueItem.isSelected == true {
                    isSelected = true
                }
                print("onDidSelectCellFilter: value: \(valueItem.name!) - \(valueItem.isSelected ?? false)")
            }
        }
        
        self.view?.onUpdateStateSkipEnabled(isSelected)
    }
}

// MARK: - FilterExercisePresenterProtocol
extension FilterExercisePresenter: FilterExercisePresenterProtocol {
    func onViewDidLoad() {
        self.setCheckStateFilter()
    }
    
    func onUnfilteredAction() {
        for filterItem in self.listFilterValues {
            for valueItem in filterItem.values! {
                valueItem.isSelected = false
            }
        }
        
        self.delegate?.onDidUnfilteredAction()
        self.view?.onDidDismissViewAction()
    }
    
    func onFilterAction() {
        self.delegate?.onDidFilterAction()
        self.view?.onDidDismissViewAction()
    }
    
    // MARK: - TableView
    func numberOfSections() -> Int {
        return self.listFilterValues.count
    }
    
    func numberOfItemsInSection(in section: Int) -> Int {
        let row = self.listFilterValues[section].values?.count ?? 0
        return row
    }
    
    func heightForRow(at index: IndexPath) -> CGFloat? {
        let count = self.listFilterValues[index.section].values?.count ?? 0
        
        let heightCellColl = 24 + 14
        let paddingCellTable = 8 + 10
        
        if count > 0 {
            if (count % 2) == 0 {
                let height = (count/2) * heightCellColl
                return CGFloat(height + paddingCellTable)
                
            } else {
                let sum = count + 1
                let height = (sum/2) * heightCellColl
                return CGFloat(height + paddingCellTable)
            }
            
        } else {
            return 0
        }
    }
    
    func itemForHeader(at section: Int) -> FilterAttributeModel? {
        let secetion = self.listFilterValues[section]
        return secetion
    }
    
    func itemForRow(at index: IndexPath) -> ValuesModel? {
        let section = self.listFilterValues[index.section]
        let values = section.values![index.row]
        return values
    }
    
    func onDidSelectCellFilter() {
        self.setCheckStateFilter()
    }
}

// MARK: - FilterExerciseInteractorOutput 
extension FilterExercisePresenter: FilterExerciseInteractorOutputProtocol {
    ///
}
