//
//  
//  FilterExerciseRouter.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

class FilterExerciseRouter {
    weak var viewController: FilterExerciseViewController?
    static func setupModule(with listFilterValues: [FilterAttributeModel], delegate: FilterExercisePresenterDelegate?) -> FilterExerciseViewController {
        let viewController = FilterExerciseViewController()
        let router = FilterExerciseRouter()
        let interactorInput = FilterExerciseInteractorInput()
        let presenter = FilterExercisePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.listFilterValues = listFilterValues
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - FilterExerciseRouterProtocol
extension FilterExerciseRouter: FilterExerciseRouterProtocol {
    
}
