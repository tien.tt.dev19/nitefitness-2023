//
//  
//  FilterExerciseViewController.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

class FilterExerciseViewController: BaseViewController {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var tbv_FilterValues: UITableView!
    @IBOutlet weak var btn_SkipFilter: UIButton!
    
    var presenter: FilterExercisePresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setShowAnimation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }

    // MARK: - Setup
    private func setupDefaults() {
        self.setInitTableView()
        self.setInitGestureView()
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.setHiddenAnimation()
    }
    
    @IBAction func onUnfilteredAction(_ sender: Any) {
        self.tbv_FilterValues.reloadData()
        self.presenter.onUnfilteredAction()
    }
    
    @IBAction func onFilterAction(_ sender: Any) {
        self.presenter.onFilterAction()
    }
}

// MARK: - FilterExerciseViewProtocol
extension FilterExerciseViewController: FilterExerciseViewProtocol {
    func onDidDismissViewAction() {
        self.setHiddenAnimation()
    }
    
    func onUpdateStateSkipEnabled(_ isEnabled: Bool) {
        self.btn_SkipFilter.isEnabled = isEnabled
    }
}

// MARK: - Animation
extension FilterExerciseViewController {
    func setShowAnimation() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            self.view_Bg.alpha = 0.6
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setHiddenAnimation() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -700
            self.view.layoutIfNeeded()
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension FilterExerciseViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setHiddenAnimation()
    }
}

// MARK: - UITableViewDataSource
extension FilterExerciseViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_FilterValues.registerNib(ofType: CellTableViewFilterValues.self)
        self.tbv_FilterValues.register(UINib(nibName: "HeaderViewTitle", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderViewTitle")
        self.tbv_FilterValues.dataSource = self
        self.tbv_FilterValues.delegate = self
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderViewTitle") as? HeaderViewTitle
        let data = self.presenter.itemForHeader(at: section)
        headerView?.lbl_Title.text = data?.name

        return headerView
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.presenter.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.presenter.heightForRow(at: indexPath) ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewFilterValues.self, for: indexPath)
        let data = self.presenter.itemForHeader(at: indexPath.section)
        cell.delegate = self
        cell.filterValuesModel = data
        cell.onReloadData()
        return cell
    }
}

// MARK: - UITableViewDelegate
extension FilterExerciseViewController: UITableViewDelegate {
    //
}

// MARK: - CellTableViewFilterValuesDelegate
extension FilterExerciseViewController: CellTableViewFilterValuesDelegate {
    func onDidSelectValueFilterAction() {
        self.presenter.onDidSelectCellFilter()
    }
}
