//
//  
//  AddExserciseConstract.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//
//

import UIKit

// MARK: - View
protocol AddExserciseViewProtocol: AnyObject {
    func reloadCollectionView()
    func reloadTableView()
    func getVisibleIndexPath() -> [IndexPath]
    
    func onInsertRow(at index: Int)
    func onSetNotFoundView(isHidden: Bool)
    func onLoadingSuccess()
    
    func setBageFilter(bage: Int)
    
}

// MARK: - Interactor
protocol AddExserciseInteractorInputProtocol {
    func getListExsercise(filter: Filter)
    func getFilterValues()
    func setAddExercise(workout: WorkoutModel, detailAdd: WorkoutDetail)
}

protocol AddExserciseInteractorOutputProtocol: AnyObject {
    func onGetExserciseFinished(with result: Result<[ExerciseModel], APIError>, page: Int, total: Int)
    func onGetFilterValuesFinished(with result: Result<FilterDataValue, APIError>, total: Int)
    
    func onAddExerciseFinished(with result: Result<WorkoutModel, APIError>, total: Int)
}
// MARK: - Presenter
protocol AddExsercisePresenterProtocol {
    func onViewDidLoad()
    
    
    func onFilterWorkoutAction()
    
    // Collection View
    func numberOfRowColl(in section: Int) -> Int
    func itemForRowColl(at index: IndexPath) -> ValuesModel
    func onRemoveItemColl(item: ValuesModel)
    
    // TableView
    func numberOfRowTable(in section: Int) -> Int
    func itemForRowTable(at index: IndexPath) -> ExerciseModel?
    func onDidSelectRowAtTable(at index: IndexPath)
    func onLoadMoreAction()
    
    func onTextFieldDidEndEditing(keyword: String)
    func onSearchExercisedAction(keyword: String)
}

// MARK: - Router
protocol AddExserciseRouterProtocol: BaseRouterProtocol {
    func gotoDetailExserciseViewController(with exercise: ExerciseModel, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen)
    
    func gotoFilterExserciseViewController(with listFilterValues: [FilterAttributeModel], delegate: FilterExercisePresenterDelegate?)
}
