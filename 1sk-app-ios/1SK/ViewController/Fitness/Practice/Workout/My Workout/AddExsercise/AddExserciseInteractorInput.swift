//
//  
//  AddExserciseInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//
//

import UIKit

class AddExserciseInteractorInput {
    weak var output: AddExserciseInteractorOutputProtocol?
    var exserciseService: ExerciseServiceProtocol?
    var filterService: FilterServiceProtocol?
}

// MARK: - AddExserciseInteractorInputProtocol
extension AddExserciseInteractorInput: AddExserciseInteractorInputProtocol {
    func getListExsercise(filter: Filter) {
        self.exserciseService?.getListExsercise(filter: filter, completion: { [weak self] result in
            self?.output?.onGetExserciseFinished(with: result.unwrapSuccessModel(), page: filter.page!, total: result.getTotal() ?? 0)
        })
    }
    
    func getFilterValues() {
        self.exserciseService?.getFilterValueExerciseWorkour(completion: { [weak self] result in
            self?.output?.onGetFilterValuesFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0)
        })
    }
    
    func setAddExercise(workout: WorkoutModel, detailAdd: WorkoutDetail) {
        self.exserciseService?.updateWorkout(workout: workout, detailAdd: detailAdd, completion: { [weak self] result in
            self?.output?.onAddExerciseFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0)
        })
    }
}
