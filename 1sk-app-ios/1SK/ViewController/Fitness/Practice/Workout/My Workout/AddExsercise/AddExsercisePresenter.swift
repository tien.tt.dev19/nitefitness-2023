//
//  
//  AddExsercisePresenter.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//
//

import UIKit

protocol AddExsercisePresenterDelegate: AnyObject {
    func onDidAddExercise(workout: WorkoutModel)
}

class AddExsercisePresenter {
    weak var view: AddExserciseViewProtocol?
    private var interactor: AddExserciseInteractorInputProtocol
    private var router: AddExserciseRouterProtocol
    weak var delegate: AddExsercisePresenterDelegate?
    
    var workout: WorkoutModel!
    
    var listExsercise: [ExerciseModel] = []
    var listFilterValues: [FilterAttributeModel] = []
    
    var listValuesSelected: [ValuesModel] = []
    
    // Loadmore
    private var pageTotal = 0
    private var page = 0
    private let limit = 10
    private var isOutOfData = false

    private var bageNumber = 0

    var filter = Filter()
    
    init(interactor: AddExserciseInteractorInputProtocol,
         router: AddExserciseRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    // MARK: - API
    private func loadItem(in page: Int) {
        self.filter.page = page
        self.filter.limit = self.limit
        self.filter.sort = "DESC"
        self.filter.order = "created_at"
        if !self.filter.subject.isEmpty {
            self.filter.subject.removeAll()
        }
        self.filter.subject.append(String(self.workout.subject?.id ?? 0))
        
        if page == 1 {
            //
        }
        self.router.showHud()
        self.interactor.getListExsercise(filter: self.filter)
    }

    func getData() {
        self.onRefreshData()
    }
    
    func onRefreshData() {
        self.page = 1
        self.isOutOfData = false
        self.loadItem(in: self.page)
    }
}

// MARK: - AddExsercisePresenterProtocol
extension AddExsercisePresenter: AddExsercisePresenterProtocol {
    
    func onViewDidLoad() {
        self.onRefreshData()
        self.interactor.getFilterValues()
    }
    
    // MARK: - Action
    func onFilterWorkoutAction() {
        self.router.gotoFilterExserciseViewController(with: self.listFilterValues, delegate: self)
    }
    
    // TextField
    func onTextFieldDidEndEditing(keyword: String) {
        self.filter.keyword = keyword
    }
    
    func onSearchExercisedAction(keyword: String) {
        self.filter.keyword = keyword
        self.onRefreshData()
    }
    
    // MARK: - Collection
    func numberOfRowColl(in section: Int) -> Int {
        return self.listValuesSelected.count
    }

    func itemForRowColl(at index: IndexPath) -> ValuesModel {
        return self.listValuesSelected[index.row]
    }
    
    func onRemoveItemColl(item: ValuesModel) {
        if let indexOffset = self.listValuesSelected.firstIndex(where: {$0.id == item.id}) {
            
            for item in self.listFilterValues {
                var isContinue = true
                for value in item.values! {
                    if value.id == self.listValuesSelected[indexOffset].id {
                        value.isSelected = false
                        isContinue = false
                        break
                    }
                }
                if isContinue == false {
                    break
                }
            }
            
            self.onDidFilterAction()
        }
    }

    // MARK: - Tableview
    func numberOfRowTable(in section: Int) -> Int {
        return self.listExsercise.count
    }

    func itemForRowTable(at index: IndexPath) -> ExerciseModel? {
        if index.row < self.listExsercise.count {
            return self.listExsercise[index.row]
        }
        return nil
    }

    func onDidSelectRowAtTable(at index: IndexPath) {
        let exsercise = self.listExsercise[index.row]
        self.router.gotoDetailExserciseViewController(with: exsercise, delegate: self, state: .add)
    }
    
    func onLoadMoreAction() {
        if self.isOutOfData {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
            
        } else {
            if self.listExsercise.count >= 10 {
                self.loadItem(in: self.page + 1)
            }
        }
    }

}

// MARK: - AddExserciseInteractorOutput
extension AddExsercisePresenter: AddExserciseInteractorOutputProtocol {
    func onGetExserciseFinished(with result: Result<[ExerciseModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let exsercises):
            if page <= 1 {
                self.listExsercise = exsercises
                self.checkRemoveExerciseExist()
                
            } else {
                for object in exsercises {
                    // checkRemoveExerciseExist
                    if self.workout.details?.first(where: { $0.exercise?.id == object.id }) == nil {
                        self.listExsercise.append(object)
                        self.view?.onInsertRow(at: self.listExsercise.count - 1)
                    }
                }
            }
            
            self.pageTotal = total
            self.page = page
            self.router.hideErrorView()
            
            if self.listExsercise.count == self.pageTotal {
                self.isOutOfData = true
            }
            
            if self.listExsercise.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure(let err):
            SKToast.shared.showToast(content: err.message)
            
            if self.listExsercise.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.router.hideHud()
    }
    
    func onGetFilterValuesFinished(with result: Result<FilterDataValue, APIError>, total: Int) {
        switch result {
        case .success(let filterValues):
            let typesValue = FilterAttributeModel(types: filterValues.types)
            let toolsValue = FilterAttributeModel(tools: filterValues.tools)
            let levelsValue = FilterAttributeModel(levels: filterValues.levels)
            let subjectsValue = FilterAttributeModel(subjects: filterValues.subjects)
            self.listFilterValues = [typesValue, toolsValue, levelsValue, subjectsValue].filter { return !($0.values ?? []).isEmpty }
            self.router.hideErrorView()
            
        case .failure(let err):
            SKToast.shared.showToast(content: err.message)
        }
        
        self.router.hideHud()
    }
    
    func onAddExerciseFinished(with result: Result<WorkoutModel, APIError>, total: Int) {
        switch result {
        case .success(let workoutModel):
            self.pageTotal = total
            self.workout = workoutModel
            self.delegate?.onDidAddExercise(workout: workoutModel)
            self.router.hideErrorView()
            self.router.showToast(content: "Đã thêm động tác vào bài tập")
            
            self.checkRemoveExerciseExist()
            
        case .failure(let err):
            print("onAddExerciseFinished failure: \(err.statusCode) - ", err.message)
            SKToast.shared.showToast(content: err.message)
        }
        
        self.router.hideHud()
    }
    
    func checkRemoveExerciseExist() {
        print("\ncheckRemoveExerciseExist ------------------")
        if let details = self.workout.details {
            for detail in details {
                for (index, ex) in self.listExsercise.enumerated() {
                    if ex.id == detail.exercise?.id {
                        print("checkRemoveExerciseExist Exist Not name: ", ex.name ?? "")
                        self.listExsercise.remove(at: index)
                    } else {
                        print("checkRemoveExerciseExist Exist name: ", ex.name ?? "")
                    }
                }
            }
            print("checkRemoveExerciseExist ==================\n")
        }
        self.view?.reloadTableView()
    }
}

// MARK: - ErrorViewDelegate
extension AddExsercisePresenter: ErrorViewDelegate {
    func onRetryButtonDidTapped(_ errorView: UIView) {
        print("onRetryButtonDidTapped")
        self.loadItem(in: self.page)
    }
}

// MARK: - AddExsercisePresenterDelegate
extension AddExsercisePresenter: DetailExercisePresenterDelegate {
    func onUpdateExerciseAction(detail: WorkoutDetailModel) {
        /// Not Use
    }
    
    func onAddExerciseDetailAction(exsercise: ExerciseModel) {
        let order = self.workout.details?.count ?? 0
        
        // Exsercise new add
        var workoutDetail = WorkoutDetail()
        workoutDetail.excercise = Exsercise()
        workoutDetail.excercise?.id = exsercise.id
        workoutDetail.excercise?.time = exsercise.time
        workoutDetail.order = order + 1
        
        self.router.showHud()
        self.interactor.setAddExercise(workout: self.workout, detailAdd: workoutDetail)
        
    }
}

// MARK: - FilterExercisePresenterDelegate
/** Các selected filter sẽ tự động tham chiếu về đây */
extension AddExsercisePresenter: FilterExercisePresenterDelegate {
    func onDidUnfilteredAction() {
        self.filter.type.removeAll()
        self.filter.tool.removeAll()
        self.filter.filter.removeAll()
        self.bageNumber = 0
        self.listValuesSelected.removeAll()
        
        self.view?.setBageFilter(bage: self.bageNumber)
        self.view?.reloadCollectionView()
        
        self.onRefreshData()
    }
    
    func onDidFilterAction() {
        self.filter.type.removeAll()
        self.filter.tool.removeAll()
        self.filter.filter.removeAll()
        self.bageNumber = 0
        self.listValuesSelected.removeAll()
        
        for item in self.listFilterValues {
            if item.type == "type" {
                for value in item.values! {
                    if value.isSelected == true {
                        self.filter.type.append(String(value.id ?? 0))
                        self.bageNumber += 1
                        self.listValuesSelected.append(value)
                    }
                }
            }
            if item.type == "tool" {
                for value in item.values! {
                    if value.isSelected == true {
                        self.filter.tool.append(String(value.id ?? 0))
                        self.bageNumber += 1
                        self.listValuesSelected.append(value)
                    }
                }
            }
            if item.type == "filter" {
                for value in item.values! {
                    if value.isSelected == true {
                        self.filter.filter.append(String(value.id ?? 0))
                        self.bageNumber += 1
                        self.listValuesSelected.append(value)
                    }
                }
            }
        }
        
        self.view?.setBageFilter(bage: self.bageNumber)
        self.view?.reloadCollectionView()
        
        self.onRefreshData()
        
        for filterItem in self.listFilterValues {
            print("\nonDidFilterAction: -------------------------------------------")
            print("onDidFilterAction: type:", filterItem.type ?? "null")
            for valueItem in filterItem.values! {
                print("onDidFilterAction: value: \(valueItem.name!) - \(valueItem.isSelected ?? false)")
            }
        }
        print("\nonDidFilterAction: ==========================================\n")
        
    }
}
