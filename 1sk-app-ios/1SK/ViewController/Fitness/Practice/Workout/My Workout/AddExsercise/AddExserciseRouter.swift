//
//  
//  AddExserciseRouter.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//
//

import UIKit

class AddExserciseRouter: BaseRouter {
    static func setupModule(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?) -> AddExserciseViewController {
        let viewController = AddExserciseViewController()
        let router = AddExserciseRouter()
        let interactorInput = AddExserciseInteractorInput()
        let presenter = AddExsercisePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.workout = workout
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exserciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - AddExserciseRouterProtocol
extension AddExserciseRouter: AddExserciseRouterProtocol {
    func gotoDetailExserciseViewController(with exercise: ExerciseModel, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen) {
        let controller = DetailExerciseRouter.setupModule(with: nil, exercise: exercise, delegate: delegate, state: state)
            self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func gotoFilterExserciseViewController(with listFilterValues: [FilterAttributeModel], delegate: FilterExercisePresenterDelegate?) {
        let controller = FilterExerciseRouter.setupModule(with: listFilterValues, delegate: delegate)
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
