//
//  
//  AddExserciseViewController.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//
//

import UIKit

class AddExserciseViewController: BaseViewController {

    @IBOutlet weak var view_Navigation: UIView!
    
    @IBOutlet weak var lbl_Badge: UILabel!
    @IBOutlet weak var view_Bage: UIView!
    
    @IBOutlet weak var tf_Search: UITextField!
    @IBOutlet weak var btn_ClearText: UIButton!
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    @IBOutlet weak var view_NotFound: UIView!
    
    var presenter: AddExsercisePresenterProtocol!
    
    private var isKeyboadShow = false
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitUITextField()
        self.setInitUICollectionView()
        self.setInitUITableView()
        self.setInitHandleKeyboard()
        
        self.setBageFilter(bage: 0)
        self.view_Navigation.addShadow(width: 0, height: 4, color: R.color.black()!, radius: 4, opacity: 0.1)
    }
    
    // MARK: - Action
    @IBAction func buttonCloseDidTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func buttonFilterDidTapped(_ sender: Any) {
        self.presenter.onFilterWorkoutAction()
    }

    @IBAction func onClearTextAction(_ sender: Any) {
        self.tf_Search.text = ""
        self.btn_ClearText.isHidden = true
        
        self.presenter.onTextFieldDidEndEditing(keyword: self.tf_Search.text ?? "")
        
        if !self.isKeyboadShow {
            self.presenter.onSearchExercisedAction(keyword: self.tf_Search.text ?? "")
        }
    }
}

// MARK: - AddExserciseViewProtocol
extension AddExserciseViewController: AddExserciseViewProtocol {
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func reloadCollectionView() {
        self.coll_CollectionView.reloadData()
    }

    func reloadTableView() {
        self.tbv_TableView.reloadData()
    }

    func getVisibleIndexPath() -> [IndexPath] {
        return self.tbv_TableView.indexPathsForVisibleRows ?? []
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func setBageFilter(bage: Int) {
        if bage == 0 {
            self.view_Bage.isHidden = true
            self.lbl_Badge.text = "0"
            
        } else {
            self.view_Bage.isHidden = false
            self.lbl_Badge.text = bage.asString
        }
    }
}

// MARK: - UITextFieldDelegate
extension AddExserciseViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Search.delegate = self
        self.btn_ClearText.isHidden = true
        self.tf_Search.attributedPlaceholder = NSAttributedString(string: "Tìm động tác", attributes: [.foregroundColor: R.color.subTitle()!])
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.presenter.onTextFieldDidEndEditing(keyword: textField.text ?? "")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 0 {
            self.btn_ClearText.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.text?.count ?? 0 > 0 {
            self.btn_ClearText.isHidden = false
        } else {
            self.btn_ClearText.isHidden = true
        }
        
        self.presenter.onSearchExercisedAction(keyword: self.tf_Search.text ?? "")
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text?.count ?? 0 == 0 {
            self.btn_ClearText.isHidden = true
        } else {
            if self.btn_ClearText.isHidden {
                self.btn_ClearText.isHidden = false
            }
        }
    }
}

// MARK: - UICollectionViewDataSource
extension AddExserciseViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: ExserciseFilterCollectionViewCell.self)
        self.coll_CollectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 32)
        layout.estimatedItemSize = CGSize(width: 50, height: 32)
        layout.minimumInteritemSpacing = 10
        self.coll_CollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let row = self.presenter.numberOfRowColl(in: section)
        if row == 0 {
            self.coll_CollectionView.isHidden = true
        } else {
            self.coll_CollectionView.isHidden = false
        }
        return row
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: ExserciseFilterCollectionViewCell.self, for: indexPath)
        
        let data = self.presenter.itemForRowColl(at: indexPath)
        
        cell.config(with: data)
        cell.delegate = self
        return cell
    }
}

// MARK: - ExserciseFilterCollectionViewCellDelegate
extension AddExserciseViewController: ExserciseFilterCollectionViewCellDelegate {
    func onButtonRemoveSuggestAction(_ cell: ExserciseFilterCollectionViewCell) {
        self.presenter.onRemoveItemColl(item: cell.valuesModel!)
    }
}

// MARK: - UITableViewDataSource
extension AddExserciseViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewExercise.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfRowTable(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewExercise.self, for: indexPath)
        let item = self.presenter.itemForRowTable(at: indexPath)
        cell.config(with: item)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AddExserciseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRowAtTable(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}

// MARK: setKeyboardEvent
extension AddExserciseViewController {
    func setInitHandleKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboardSize:", keyboardSize)
            self.isKeyboadShow = true
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.isKeyboadShow = false
    }
}
