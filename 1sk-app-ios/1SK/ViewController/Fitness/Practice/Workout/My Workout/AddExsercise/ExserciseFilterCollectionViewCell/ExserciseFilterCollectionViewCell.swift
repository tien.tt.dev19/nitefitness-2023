//
//  ExserciseFilterCollectionViewCell.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//

import UIKit

protocol ExserciseFilterCollectionViewCellDelegate: AnyObject {
    func onButtonRemoveSuggestAction(_ cell: ExserciseFilterCollectionViewCell)
}

class ExserciseFilterCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    
    weak var delegate: ExserciseFilterCollectionViewCellDelegate?
    
    var valuesModel: ValuesModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func buttonCloseDidTapped(_ sender: Any) {
        self.delegate?.onButtonRemoveSuggestAction(self)
    }

    func config(with valuesModel: ValuesModel) {
        self.valuesModel = valuesModel
        self.nameLabel.text = valuesModel.name
    }
}
