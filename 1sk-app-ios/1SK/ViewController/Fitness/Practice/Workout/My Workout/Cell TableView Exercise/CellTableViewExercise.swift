//
//  CellTableViewExercise.swift
//  1SK
//
//  Created by tuyenvx on 23/06/2021.
//

import UIKit

class CellTableViewExercise: UITableViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.img_Image.isSkeletonable = true
        self.lbl_Title.isSkeletonable = true
    }

    func config(with item: WorkoutDetailModel?) {
        guard let `item` = item else {
            self.showSkeleton()
            return
        }
        self.img_Image.setImageWith(imageUrl: item.exercise?.image ?? "", placeHolder: UIImage(named: "img_default"))
        self.lbl_Title.text = item.exercise?.name
        
        self.lbl_Time.text = item.type?.name
        
        self.hideSkeleton()
    }

    func config(with item: ExerciseModel?) {
        
        guard let `item` = item else {
            showSkeleton()
            return
        }
        self.img_Image.setImageWith(imageUrl: item.image ?? "", placeHolder: UIImage(named: "img_default"))
        self.lbl_Title.text = item.name
        self.lbl_Time.text = item.type?.name //"x\(item.time ?? 0)"

        self.hideSkeleton()
    }
}
