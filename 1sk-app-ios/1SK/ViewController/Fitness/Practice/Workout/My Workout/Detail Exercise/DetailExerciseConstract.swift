//
//  
//  DetailExerciseConstract.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

// MARK: - View
protocol DetailExerciseViewProtocol: AnyObject {

}

// MARK: - Presenter
protocol DetailExercisePresenterProtocol {
    func onViewDidLoad()
    
    func stateView() -> StateViewScreen
    
    func pageExerciseAddViewController() -> ExerciseAddViewController
    func pageExerciseEditViewController() -> ExerciseEditViewController
    func pageExerciseViewViewController() -> ExerciseViewViewController
    
    func pageVideoTutorialViewController() -> VideoTutorialViewController
}

// MARK: - Interactor Input
protocol DetailExerciseInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol DetailExerciseInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol DetailExerciseRouterProtocol {
    
}
