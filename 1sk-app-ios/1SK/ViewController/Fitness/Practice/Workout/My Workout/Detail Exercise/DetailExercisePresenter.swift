//
//  
//  DetailExercisePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

protocol ExercisePresenterDelegate: AnyObject {
    func onAddExerciseAction(exsercise: ExerciseModel)
    func onUpdateExerciseAction(detail: WorkoutDetailModel)
}

protocol DetailExercisePresenterDelegate: AnyObject {
    func onAddExerciseDetailAction(exsercise: ExerciseModel)
    func onUpdateExerciseAction(detail: WorkoutDetailModel)
}

class DetailExercisePresenter {

    weak var view: DetailExerciseViewProtocol?
    private var interactor: DetailExerciseInteractorInputProtocol
    private var router: DetailExerciseRouterProtocol
    
    weak var delegate: DetailExercisePresenterDelegate?
    var detail: WorkoutDetailModel?
    var exercise: ExerciseModel?
    var state: StateViewScreen = .normal
    
    init(interactor: DetailExerciseInteractorInputProtocol,
         router: DetailExerciseRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - DetailExercisePresenterProtocol
extension DetailExercisePresenter: DetailExercisePresenterProtocol {
    func onViewDidLoad() {
        
    }
    
    func stateView() -> StateViewScreen {
        return self.state
    }
    
    func pageExerciseAddViewController() -> ExerciseAddViewController {
        return ExerciseAddRouter.setupModule(with: self.exercise, delegate: self)
    }
    
    func pageExerciseEditViewController() -> ExerciseEditViewController {
        return ExerciseEditRouter.setupModule(with: self.detail, delegate: self)
    }
    
    func pageExerciseViewViewController() -> ExerciseViewViewController {
        return ExerciseViewRouter.setupModule(with: self.detail, exercise: self.exercise, delegate: self)
    }
    
    func pageVideoTutorialViewController() -> VideoTutorialViewController {
        return VideoTutorialRouter.setupModule(with: self.detail, exercise: self.exercise)
    }
}

//MARK: - DetailExerciseInteractorOutput
extension DetailExercisePresenter: DetailExerciseInteractorOutputProtocol {

}

//MARK: - DetailExercisePresenterDelegate
extension DetailExercisePresenter: DetailExercisePresenterDelegate {
    func onAddExerciseDetailAction(exsercise: ExerciseModel) {
        print("onAddExerciseAction DetailExercisePresenter")
    }
}

//MARK: - ExercisePresenterDelegate
extension DetailExercisePresenter: ExercisePresenterDelegate {
    func onUpdateExerciseAction(detail: WorkoutDetailModel) {
        self.delegate?.onUpdateExerciseAction(detail: detail)
    }
    
    func onAddExerciseAction(exsercise: ExerciseModel) {
        self.delegate?.onAddExerciseDetailAction(exsercise: exsercise)
    }
}
