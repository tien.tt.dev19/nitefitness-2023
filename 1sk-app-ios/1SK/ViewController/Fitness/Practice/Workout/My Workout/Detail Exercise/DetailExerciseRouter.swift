//
//  
//  DetailExerciseRouter.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

class DetailExerciseRouter {
    weak var viewController: DetailExerciseViewController?
    static func setupModule(with detail: WorkoutDetailModel?, exercise: ExerciseModel?, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen) -> DetailExerciseViewController {
        let viewController = DetailExerciseViewController()
        let router = DetailExerciseRouter()
        let interactorInput = DetailExerciseInteractorInput()
        let presenter = DetailExercisePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.detail = detail
        presenter.exercise = exercise
        presenter.delegate = delegate
        presenter.state = state
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - DetailExerciseRouterProtocol
extension DetailExerciseRouter: DetailExerciseRouterProtocol {
    
}
