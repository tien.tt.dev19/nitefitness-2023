//
//  
//  DetailExerciseViewController.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

class DetailExerciseViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    
    var presenter: DetailExercisePresenterProtocol!
    
    private lazy var pageViewController: VXPageViewController = VXPageViewController()
    
    var exerciseAddViewController: ExerciseAddViewController!
    var exerciseEditViewController: ExerciseEditViewController!
    var exerciseViewViewController: ExerciseViewViewController!
    
    var videoTutorialViewController: VideoTutorialViewController!
    
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.containerView.roundCorner(corners: [.topLeft, .topRight], radius: 20)
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setupPageViewController()
    }
    
    private func setupPageViewController() {
        let barConfig = VXPageBarConfig(height: 48,
                                      selectedColor: R.color.darkText(),
                                      unSelectedColor: R.color.subTitle(),
                                      selectedFont: R.font.robotoMedium(size: 16),
                                      unSelectedFont: R.font.robotoRegular(size: 16),
                                      underLineHeight: 3,
                                      underLineWidth: 130,
                                      underLineColor: R.color.mainColor(),
                                      backgroundColor: .white)
        
        self.videoTutorialViewController = self.presenter.pageVideoTutorialViewController()
        
        switch self.presenter.stateView() {
        case .add:
            self.exerciseAddViewController = self.presenter.pageExerciseAddViewController()
            
            let pageExercise = VXPageItem(viewController: self.exerciseAddViewController, title: "Động tác")
            let pageVideoTutorial = VXPageItem(viewController: self.videoTutorialViewController, title: "Video hướng dẫn")
            self.pageViewController.setPageItems([pageExercise, pageVideoTutorial])
            self.pageViewController.setPageBarConfig(barConfig)
            
        case .edit:
            self.exerciseEditViewController = self.presenter.pageExerciseEditViewController()
            let pageExercise = VXPageItem(viewController: self.exerciseEditViewController, title: "Động tác")
            let pageVideoTutorial = VXPageItem(viewController: self.videoTutorialViewController, title: "Video hướng dẫn")
            self.pageViewController.setPageItems([pageExercise, pageVideoTutorial])
            self.pageViewController.setPageBarConfig(barConfig)
            
        case .view:
            self.exerciseViewViewController = self.presenter.pageExerciseViewViewController()
            let pageExercise = VXPageItem(viewController: self.exerciseViewViewController, title: "Động tác")
            let pageVideoTutorial = VXPageItem(viewController: self.videoTutorialViewController, title: "Video hướng dẫn")
            self.pageViewController.setPageItems([pageExercise, pageVideoTutorial])
            self.pageViewController.setPageBarConfig(barConfig)
        
        default:
            break
        }
        
        self.addChild(self.pageViewController)
        self.containerView.addSubview(self.pageViewController.view)
        self.pageViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.pageViewController.didMove(toParent: self)
        self.pageViewController.delegate = self
    }
    
    // MARK: - Action
}

// MARK: - VXPageViewDelegate
extension DetailExerciseViewController: VXPageViewDelegate {
    func onDidChangeSelectedPage(at index: Int) {
        self.videoTutorialViewController.onChangePageMenu(index: index)
        self.exerciseViewViewController.onChangePageMenu(index: index)
    }
}

// MARK: - DetailExerciseViewProtocol
extension DetailExerciseViewController: DetailExerciseViewProtocol {
    
}
