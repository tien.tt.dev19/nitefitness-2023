//
//  
//  ExerciseAddConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

// MARK: - View
protocol ExerciseAddViewProtocol: AnyObject {
    func onViewDidLoad(exercise: ExerciseModel)
    
    func onMinusTimeActionSuccess(value: Int)
    func onPlusTimeActionSuccess(value: Int)
    
    func onMinusNumberActionSuccess(value: Int)
    func onPlusNumberActionSuccess(value: Int)
    
    func onDismissViewController()
}

// MARK: - Presenter
protocol ExerciseAddPresenterProtocol {
    func onViewDidLoad()
    
    func onMinusTimeAction()
    func onPlusTimeAction()
    
    func onMinusNumberAction()
    func onPlusNumberAction()
    
    func onAddExerciseAction()
}

// MARK: - Interactor Input
protocol ExerciseAddInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol ExerciseAddInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol ExerciseAddRouterProtocol: BaseRouterProtocol {

}
