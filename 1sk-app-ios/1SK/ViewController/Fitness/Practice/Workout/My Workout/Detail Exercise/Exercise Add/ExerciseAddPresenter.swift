//
//  
//  ExerciseAddPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseAddPresenter {

    weak var view: ExerciseAddViewProtocol?
    private var interactor: ExerciseAddInteractorInputProtocol
    private var router: ExerciseAddRouterProtocol
    
    weak var delegate: ExercisePresenterDelegate?
    var exercise: ExerciseModel?
    
    init(interactor: ExerciseAddInteractorInputProtocol,
         router: ExerciseAddRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - ExerciseAddPresenterProtocol
extension ExerciseAddPresenter: ExerciseAddPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onViewDidLoad(exercise: self.exercise!)
    }
    
    // MARK: - New
    func onMinusTimeAction() {
        if self.exercise?.time ?? 0 > 1 {
            self.exercise?.time! -= 1
            self.view?.onMinusTimeActionSuccess(value: self.exercise?.time ?? 1)
        } else {
            SKToast.shared.showToast(content: "Thời gian bài tập tối thiểu là 1 giây")
        }
    }
    
    func onPlusTimeAction() {
        if self.exercise?.time ?? 0 <= 5400 {
            self.self.exercise?.time! += 1
            self.view?.onPlusTimeActionSuccess(value: self.exercise?.time ?? 1)
            
        } else {
            SKToast.shared.showToast(content: "Thời gian bài tập tối đa là 90 phút")
        }
    }
    
    func onMinusNumberAction() {
        if self.exercise?.time ?? 0 > 1 {
            self.exercise?.time! -= 1
            self.view?.onMinusNumberActionSuccess(value: self.exercise?.time ?? 1)
        } else {
            SKToast.shared.showToast(content: "Thời gian bài tập tối thiểu là 1 lần")
        }
    }
    
    func onPlusNumberAction() {
        self.exercise?.time! += 1
        self.view?.onPlusNumberActionSuccess(value: self.exercise?.time ?? 1)
    }
    
    func onAddExerciseAction() {
        self.delegate?.onAddExerciseAction(exsercise: self.exercise!)
        self.view?.onDismissViewController()
    }
    
}

// MARK: - ExerciseAddInteractorOutput 
extension ExerciseAddPresenter: ExerciseAddInteractorOutputProtocol {

}
