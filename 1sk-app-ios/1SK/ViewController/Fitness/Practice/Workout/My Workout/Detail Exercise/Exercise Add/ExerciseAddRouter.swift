//
//  
//  ExerciseAddRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseAddRouter: BaseRouter {
//    weak var viewController: ExerciseAddViewController?
    static func setupModule(with exercise: ExerciseModel?, delegate: ExercisePresenterDelegate?) -> ExerciseAddViewController {
        let viewController = ExerciseAddViewController()
        let router = ExerciseAddRouter()
        let interactorInput = ExerciseAddInteractorInput()
        let presenter = ExerciseAddPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.exercise = exercise
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - ExerciseAddRouterProtocol
extension ExerciseAddRouter: ExerciseAddRouterProtocol {
    
}
