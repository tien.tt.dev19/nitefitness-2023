//
//  
//  ExerciseDoConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

// MARK: - View
protocol ExerciseDoViewProtocol: AnyObject {
    func onViewDidLoad(detail: WorkoutDetailModel?)
}

// MARK: - Presenter
protocol ExerciseDoPresenterProtocol {
    func onViewDidLoad()
    
    func onDismissDetailViewAction()
}

// MARK: - Interactor Input
protocol ExerciseDoInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol ExerciseDoInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol ExerciseDoRouterProtocol {

}
