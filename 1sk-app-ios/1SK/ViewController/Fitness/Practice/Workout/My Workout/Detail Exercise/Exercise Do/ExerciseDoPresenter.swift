//
//  
//  ExerciseDoPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

protocol ExerciseDoPresenterDelegate: AnyObject {
    func onDismissDetailViewAction()
}

class ExerciseDoPresenter {

    weak var view: ExerciseDoViewProtocol?
    private var interactor: ExerciseDoInteractorInputProtocol
    private var router: ExerciseDoRouterProtocol

    weak var delegate: ExerciseDoPresenterDelegate?
    
    var detail: WorkoutDetailModel?
    
    init(interactor: ExerciseDoInteractorInputProtocol,
         router: ExerciseDoRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - ExerciseDoPresenterProtocol
extension ExerciseDoPresenter: ExerciseDoPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onViewDidLoad(detail: self.detail)
    }
    
    func onDismissDetailViewAction() {
        self.delegate?.onDismissDetailViewAction()
    }
}

// MARK: - ExerciseDoInteractorOutput 
extension ExerciseDoPresenter: ExerciseDoInteractorOutputProtocol {

}
