//
//  
//  ExerciseDoRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseDoRouter {
    weak var viewController: ExerciseDoViewController?
    static func setupModule(with detail: WorkoutDetailModel?, delegate: ExerciseDoPresenterDelegate?) -> ExerciseDoViewController {
        let viewController = ExerciseDoViewController()
        let router = ExerciseDoRouter()
        let interactorInput = ExerciseDoInteractorInput()
        let presenter = ExerciseDoPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.detail = detail
        presenter.delegate = delegate
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - ExerciseDoRouterProtocol
extension ExerciseDoRouter: ExerciseDoRouterProtocol {
    
}
