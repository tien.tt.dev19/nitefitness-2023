//
//  
//  ExerciseDoViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit
import AVKit
import AVFoundation

class ExerciseDoViewController: BaseViewController {

    @IBOutlet weak var view_Bg: UIView!
    
    @IBOutlet weak var lbl_MovementTitle: UILabel!
    @IBOutlet weak var view_MovementIndicator: UIView!
    
    @IBOutlet weak var lbl_VideoTitle: UILabel!
    @IBOutlet weak var view_VideoIndicator: UIView!
    
    @IBOutlet weak var img_ImageExercise: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var view_PlayerView: UIView!
    
    var presenter: ExerciseDoPresenterProtocol!
    
    // A&V-Player
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var isPlaying = false
    var listVideoUrl: [URL] = []
    
    private var timeVideoDuration: TimeInterval = 0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        presenter.onViewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.playerLayer?.frame = self.view_PlayerView.bounds
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.player?.currentItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        self.player?.currentItem?.removeObserver(self, forKeyPath: AVConstant.duration)
        self.player = nil
        self.playerLayer = nil
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setMenuPage(page: 0)
    }
    
    func setMenuPage(page: Int) {
        if page == 0 {
            self.player?.seek(to: CMTime.zero)
            self.player?.play()
            
            self.lbl_MovementTitle.textColor = .darkText
            self.view_MovementIndicator.isHidden = false
            
            self.lbl_VideoTitle.textColor = .lightGray
            self.view_VideoIndicator.isHidden = true
            
            self.img_ImageExercise.isHidden = true
            self.view_PlayerView.isHidden = false
            
        } else {
            self.player?.seek(to: CMTime.zero)
            self.player?.play()
            
            self.lbl_MovementTitle.textColor = .lightGray
            self.view_MovementIndicator.isHidden = true
            
            self.lbl_VideoTitle.textColor = .darkText
            self.view_VideoIndicator.isHidden = false
            
            self.img_ImageExercise.isHidden = true
            self.view_PlayerView.isHidden = false
            
        }
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.presenter.onDismissDetailViewAction()
        }
    }
    
    @IBAction func onMovementPageAction(_ sender: Any) {
        self.setMenuPage(page: 0)
    }
    
    @IBAction func onVideoPageAction(_ sender: Any) {
        self.setMenuPage(page: 1)
    }
}

// MARK: - ExerciseDoViewProtocol
extension ExerciseDoViewController: ExerciseDoViewProtocol {
    func onViewDidLoad(detail: WorkoutDetailModel?) {
        self.lbl_Title.text = detail?.exercise?.name
        self.lbl_Content.text = detail?.exercise?.description
        self.img_ImageExercise.setImageWith(imageUrl: detail?.exercise?.image ?? "", placeHolder: R.image.default_avatar())
        
        if let url = URL(string: detail?.exercise?.video ?? "") {
            self.playVideoWithInitFisrt(url: url)
        }
    }
}

// MARK: AV Player
extension ExerciseDoViewController {
    func playVideoWithInitFisrt(url: URL) {
        let currentPlayerItem = AVPlayerItem(url: url)
        self.player = AVPlayer(playerItem: currentPlayerItem)
        
        self.onAddObserverKVOPlayer()
        
        self.playerLayer = AVPlayerLayer(player: self.player)
        self.playerLayer?.videoGravity = .resize
        self.view_PlayerView.layer.addSublayer(self.playerLayer!)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.player?.play()
        }
        
    }
    
    func onRePlay() {
        self.player?.seek(to: CMTime.zero)
        self.player?.play()
    }
}

// MARK: KVO Observer
extension ExerciseDoViewController {
    func onAddObserverKVOPlayer() {
        self.player?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: .new, context: nil)
        self.player?.currentItem?.addObserver(self, forKeyPath: AVConstant.duration, options: [.new, .initial], context: nil)
        self.onListenTimeObserver()
    }
    
    func onListenTimeObserver() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        
        self.player?.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { [weak self] _ in
            guard let currentItem = self?.player?.currentItem else {return}
            
            let duration = currentItem.duration.seconds.rounded(toPlaces: 1)
            let seconds = currentItem.currentTime().seconds.rounded(toPlaces: 1)
            //let timeString = self?.getTimeString(from: currentItem.currentTime())
            
//            print("\naddTimeObserver: ======================== ")
//            print("addTimeObserver: duration: ", duration)
//            print("addTimeObserver: seconds: ", seconds)
            
            print("addTimeObserver: seconds:", seconds)
            
            if seconds == duration {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self?.onRePlay()
                }
            }
            
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            var status: AVPlayerItem.Status = .unknown
            if let statusNumber = change?[.newKey] as? NSNumber,
                let newStatus = AVPlayerItem.Status(rawValue: statusNumber.intValue) {
                status = newStatus
            }
            
            switch status {
            case .readyToPlay:
                self.player?.play()
                
            case .failed:
                print("observeValue failed")
                
            case .unknown:
                print("observeValue unknown")
                
            @unknown default:
                fatalError()
            }
            
        } else if keyPath == AVConstant.duration, let duration = self.player?.currentItem?.duration.seconds, duration > 0.0 {
            print("observeValue duration ", duration)
        }
    }
    
    func getTimeString(from time: CMTime) -> String {
        let totalSeconds = CMTimeGetSeconds(time)
        let hours = Int(totalSeconds/3600)
        let minutes = Int(totalSeconds/60) % 60
        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        if hours > 0 {
            return String(format: "%i:%02i:%02i", arguments: [hours, minutes, seconds])
            
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
        //self.durationLabel.text = getTimeString(from: player.currentItem!.duration)
    }
}
