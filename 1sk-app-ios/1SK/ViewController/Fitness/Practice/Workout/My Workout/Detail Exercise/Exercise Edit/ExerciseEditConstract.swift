//
//  
//  ExerciseEditConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

// MARK: - View
protocol ExerciseEditViewProtocol: AnyObject {
    func onViewDidLoad(detail: WorkoutDetailModel?)
    
    func onMinusTimeActionSuccess(value: Int)
    func onPlusTimeActionSuccess(value: Int)
    
    func onMinusNumberActionSuccess(value: Int)
    func onPlusNumberActionSuccess(value: Int)
    
    func onDismissViewController()
}

// MARK: - Presenter
protocol ExerciseEditPresenterProtocol {
    func onViewDidLoad()
    
    func onMinusTimeAction()
    func onPlusTimeAction()
    
    func onMinusNumberAction()
    func onPlusNumberAction()
    
    func onUpdateExerciseAction()
}

// MARK: - Interactor Input
protocol ExerciseEditInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol ExerciseEditInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol ExerciseEditRouterProtocol: BaseRouterProtocol {

}
