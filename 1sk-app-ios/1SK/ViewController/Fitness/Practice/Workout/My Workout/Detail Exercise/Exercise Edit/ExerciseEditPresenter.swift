//
//  
//  ExerciseEditPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseEditPresenter {

    weak var view: ExerciseEditViewProtocol?
    private var interactor: ExerciseEditInteractorInputProtocol
    private var router: ExerciseEditRouterProtocol

    weak var delegate: ExercisePresenterDelegate?
    var detail: WorkoutDetailModel?
    
    init(interactor: ExerciseEditInteractorInputProtocol,
         router: ExerciseEditRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - ExerciseEditPresenterProtocol
extension ExerciseEditPresenter: ExerciseEditPresenterProtocol {
    func onViewDidLoad() {
        self.view?.onViewDidLoad(detail: self.detail)
    }
    
    // MARK: - New
    func onMinusTimeAction() {
        if self.detail?.time ?? 0 > 1 {
            self.detail?.time! -= 1
            self.view?.onMinusTimeActionSuccess(value: self.detail?.time ?? 1)
        } else {
            SKToast.shared.showToast(content: "Thời gian bài tập tối thiểu là 1 giây")
        }
    }
    
    func onPlusTimeAction() {
        if self.detail?.time ?? 0 <= 5400 {
            self.self.detail?.time! += 1
            self.view?.onPlusTimeActionSuccess(value: self.detail?.time ?? 1)
            
        } else {
            SKToast.shared.showToast(content: "Thời gian bài tập tối đa là 90 phút")
        }
    }
    
    func onMinusNumberAction() {
        if self.detail?.time ?? 0 > 1 {
            self.detail?.time! -= 1
            self.view?.onMinusNumberActionSuccess(value: self.detail?.time ?? 1)
        } else {
            SKToast.shared.showToast(content: "Thời gian bài tập tối thiểu là 1 lần")
        }
    }
    
    func onPlusNumberAction() {
        self.detail?.time! += 1
        self.view?.onPlusNumberActionSuccess(value: self.detail?.time ?? 1)
    }
    
    func onUpdateExerciseAction() {
        self.delegate?.onUpdateExerciseAction(detail: self.detail!)
        self.view?.onDismissViewController()
    }
}

// MARK: - ExerciseEditInteractorOutput 
extension ExerciseEditPresenter: ExerciseEditInteractorOutputProtocol {

}
