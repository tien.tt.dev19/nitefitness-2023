//
//  
//  ExerciseEditRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseEditRouter: BaseRouter {
//    weak var viewController: ExerciseEditViewController?
    static func setupModule(with detail: WorkoutDetailModel?, delegate: ExercisePresenterDelegate?) -> ExerciseEditViewController {
        let viewController = ExerciseEditViewController()
        let router = ExerciseEditRouter()
        let interactorInput = ExerciseEditInteractorInput()
        let presenter = ExerciseEditPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.detail = detail
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - ExerciseEditRouterProtocol
extension ExerciseEditRouter: ExerciseEditRouterProtocol {
    
}
