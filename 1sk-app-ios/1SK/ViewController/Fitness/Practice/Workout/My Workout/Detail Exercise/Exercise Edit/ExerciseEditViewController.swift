//
//  
//  ExerciseEditViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseEditViewController: BaseViewController {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var stack_Time: UIStackView!
    @IBOutlet weak var stack_Number: UIStackView!
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Number: UILabel!
    
    var presenter: ExerciseEditPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {

    }
    
    // MARK: - Action
    // MARK: - Action
    @IBAction func onMinusTimeAction(_ sender: Any) {
        self.presenter.onMinusTimeAction()
    }
    @IBAction func onPlusTimeAction(_ sender: Any) {
        self.presenter.onPlusTimeAction()
    }
    
    @IBAction func onMinusNumberAction(_ sender: Any) {
        self.presenter.onMinusNumberAction()
    }
    @IBAction func onPlusNumberAction(_ sender: Any) {
        self.presenter.onPlusNumberAction()
    }
    
    @IBAction func onUpdateExerciseAction(_ sender: Any) {
        self.presenter.onUpdateExerciseAction()
    }
    
    @IBAction func onCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - ExerciseEditViewProtocol
extension ExerciseEditViewController: ExerciseEditViewProtocol {
    func onViewDidLoad(detail: WorkoutDetailModel?) {
        self.lbl_Title.text = detail?.exercise?.name
        self.lbl_Content.text = detail?.exercise?.description
        self.img_Image.setImageWith(imageUrl: detail?.exercise?.image ?? "", placeHolder: UIImage(named: "img_default"))
        
        switch detail?.exercise?.typeDisplay {
        case TypeDisplay.time.rawValue:
            self.lbl_Time.text = "\(detail?.time ?? 0)"
            self.stack_Number.isHidden = true
            
        case TypeDisplay.number.rawValue:
            self.lbl_Number.text = "\(detail?.time ?? 0)"
            self.stack_Time.isHidden = true
            
        default:
            break
        }
    }
    
    func onMinusTimeActionSuccess(value: Int) {
        self.lbl_Time.text = "\(value)s"
    }
    func onPlusTimeActionSuccess(value: Int) {
        self.lbl_Time.text = "\(value)s"
    }
    
    func onMinusNumberActionSuccess(value: Int) {
        self.lbl_Number.text = "\(value)"
    }
    func onPlusNumberActionSuccess(value: Int) {
        self.lbl_Number.text = "\(value)"
    }
    
    func onDismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
}
