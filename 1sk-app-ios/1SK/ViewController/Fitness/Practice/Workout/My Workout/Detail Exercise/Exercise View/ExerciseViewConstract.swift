//
//  
//  ExerciseViewConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

// MARK: - View
protocol ExerciseViewViewProtocol: AnyObject {
    func onViewDidLoad(detail: WorkoutDetailModel)
    func onViewDidLoad(exercise: ExerciseModel?)
}

// MARK: - Presenter
protocol ExerciseViewPresenterProtocol {
    func onViewDidLoad()
}

// MARK: - Interactor Input
protocol ExerciseViewInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol ExerciseViewInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol ExerciseViewRouterProtocol {

}
