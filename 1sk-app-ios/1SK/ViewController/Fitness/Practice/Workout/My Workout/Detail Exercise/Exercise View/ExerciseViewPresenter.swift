//
//  
//  ExerciseViewPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseViewPresenter {

    weak var view: ExerciseViewViewProtocol?
    private var interactor: ExerciseViewInteractorInputProtocol
    private var router: ExerciseViewRouterProtocol

    weak var delegate: ExercisePresenterDelegate?
    var detail: WorkoutDetailModel?
    var exercise: ExerciseModel?
    
    init(interactor: ExerciseViewInteractorInputProtocol,
         router: ExerciseViewRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - ExerciseViewPresenterProtocol
extension ExerciseViewPresenter: ExerciseViewPresenterProtocol {
    func onViewDidLoad() {
        if self.detail != nil {
            self.view?.onViewDidLoad(detail: self.detail!)
        } else {
            self.view?.onViewDidLoad(exercise: self.exercise)
        }
    }
}

// MARK: - ExerciseViewInteractorOutput 
extension ExerciseViewPresenter: ExerciseViewInteractorOutputProtocol {

}
