//
//  
//  ExerciseViewRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/16/21.
//
//

import UIKit

class ExerciseViewRouter {
    weak var viewController: ExerciseViewViewController?
    static func setupModule(with detail: WorkoutDetailModel?, exercise: ExerciseModel?, delegate: ExercisePresenterDelegate?) -> ExerciseViewViewController {
        let viewController = ExerciseViewViewController()
        let router = ExerciseViewRouter()
        let interactorInput = ExerciseViewInteractorInput()
        let presenter = ExerciseViewPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.detail = detail
        presenter.exercise = exercise
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - ExerciseViewRouterProtocol
extension ExerciseViewRouter: ExerciseViewRouterProtocol {
    
}
