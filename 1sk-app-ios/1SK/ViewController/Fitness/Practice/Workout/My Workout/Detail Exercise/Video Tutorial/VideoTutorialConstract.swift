//
//  
//  VideoTutorialConstract.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

// MARK: - View
protocol VideoTutorialViewProtocol: AnyObject {
    func onViewDidLoad(detail: WorkoutDetailModel?)
    func onViewDidLoad(exercise: ExerciseModel?)
}

// MARK: - Presenter
protocol VideoTutorialPresenterProtocol {
    func onViewDidLoad()
}

// MARK: - Interactor Input
protocol VideoTutorialInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol VideoTutorialInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol VideoTutorialRouterProtocol {

}
