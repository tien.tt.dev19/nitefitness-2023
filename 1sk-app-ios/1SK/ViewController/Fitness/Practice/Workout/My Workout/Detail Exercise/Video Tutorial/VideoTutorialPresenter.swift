//
//  
//  VideoTutorialPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

class VideoTutorialPresenter {

    weak var view: VideoTutorialViewProtocol?
    private var interactor: VideoTutorialInteractorInputProtocol
    private var router: VideoTutorialRouterProtocol

    var detail: WorkoutDetailModel?
    var exercise: ExerciseModel?
    
    init(interactor: VideoTutorialInteractorInputProtocol,
         router: VideoTutorialRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - VideoTutorialPresenterProtocol
extension VideoTutorialPresenter: VideoTutorialPresenterProtocol {
    func onViewDidLoad() {
        if self.detail != nil {
            self.view?.onViewDidLoad(detail: self.detail)
        } else {
            self.view?.onViewDidLoad(exercise: self.exercise)
        }
    }
}

// MARK: - VideoTutorialInteractorOutput 
extension VideoTutorialPresenter: VideoTutorialInteractorOutputProtocol {

}
