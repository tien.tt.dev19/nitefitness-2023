//
//  
//  VideoTutorialRouter.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit

class VideoTutorialRouter {
    weak var viewController: VideoTutorialViewController?
    static func setupModule(with detail: WorkoutDetailModel?, exercise: ExerciseModel?) -> VideoTutorialViewController {
        let viewController = VideoTutorialViewController()
        let router = VideoTutorialRouter()
        let interactorInput = VideoTutorialInteractorInput()
        let presenter = VideoTutorialPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.detail = detail
        presenter.exercise = exercise
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - VideoTutorialRouterProtocol
extension VideoTutorialRouter: VideoTutorialRouterProtocol {
    
}
