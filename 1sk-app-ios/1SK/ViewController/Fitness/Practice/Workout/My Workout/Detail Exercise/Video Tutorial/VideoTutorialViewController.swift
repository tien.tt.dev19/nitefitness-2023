//
//  
//  VideoTutorialViewController.swift
//  1SK
//
//  Created by vuongbachthu on 6/29/21.
//
//

import UIKit
import AVKit
import AVFoundation

class VideoTutorialViewController: BaseViewController {
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Number: UILabel!
    
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var view_PlayerView: UIView!
    
    var presenter: VideoTutorialPresenterProtocol!
    
    // A&V-Player
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var listVideoUrl: [URL] = []
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.player?.currentItem?.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        
        self.player?.currentItem?.removeObserver(self, forKeyPath: AVConstant.duration)
        self.player = nil
        self.playerLayer = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.playerLayer?.frame = self.view_PlayerView.bounds
    }
    
    @IBAction func onCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        
    }
    
    func onChangePageMenu(index: Int) {
        switch index {
        case 0:
            self.player?.pause()
            
        case 1:
            self.player?.play()
            
        default:
            break
        }
    }
    
    deinit {
        print("VideoTutorialViewController deinit")
    }
}

// MARK: - VideoTutorialViewProtocol
extension VideoTutorialViewController: VideoTutorialViewProtocol {
    func onViewDidLoad(detail: WorkoutDetailModel?) {
        self.lbl_Title.text = detail?.exercise?.name
        self.lbl_Content.text = detail?.exercise?.description
        
        self.img_Image.setImageWith(imageUrl: detail?.exercise?.image ?? "", placeHolder: UIImage(named: "img_default"))
        
        switch detail?.exercise?.typeDisplay {
        case TypeDisplay.time.rawValue:
            self.lbl_Time.text = "Thời gian tập: \((detail?.time ?? 0).asTimeFormat)"
            self.lbl_Number.isHidden = true
            
        case TypeDisplay.number.rawValue:
            self.lbl_Number.text = "Số lần tập: \(detail?.time ?? 0) lần"
            self.lbl_Time.isHidden = true
            
        default:
            break
        }
        
        if let url = URL(string: detail?.exercise?.video ?? "") {
            self.img_Image.isHidden = true
            self.playVideoWithInitFisrt(url: url)
        }
        
    }
    
    func onViewDidLoad(exercise: ExerciseModel?) {
        self.lbl_Title.text = exercise?.name
        self.lbl_Content.text = exercise?.description
        self.img_Image.setImageWith(imageUrl: exercise?.image ?? "", placeHolder: UIImage(named: "img_default"))
        
        switch exercise?.typeDisplay {
        case TypeDisplay.time.rawValue:
            self.lbl_Time.text = "Thời gian tập: \(exercise?.time?.asTimeFormat ?? "00:00")"
            self.lbl_Number.isHidden = true
            
        case TypeDisplay.number.rawValue:
            self.lbl_Number.text = "Số lần tập: \(exercise?.time ?? 0) lần"
            self.lbl_Time.isHidden = true
            
        default:
            break
        }
        
        if let url = URL(string: exercise?.movement ?? "") {
            self.img_Image.isHidden = true
            self.playVideoWithInitFisrt(url: url)
        }
    }
}

// MARK: AV Player
extension VideoTutorialViewController {
    func playVideoWithInitFisrt(url: URL) {
        let currentPlayerItem = AVPlayerItem(url: url)
        if self.player == nil {
            self.player = AVPlayer(playerItem: currentPlayerItem)
        }
        self.onAddObserverKVOPlayer()
        
        if self.playerLayer == nil {
            self.playerLayer = AVPlayerLayer(player: self.player)
        }
        self.playerLayer?.videoGravity = .resize
        self.view_PlayerView.layer.addSublayer(self.playerLayer!)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.player?.pause()
        }
        
    }
    
    func onRePlay() {
        self.player?.seek(to: CMTime.zero)
        self.player?.play()
    }
}

// MARK: KVO Observer
extension VideoTutorialViewController {
    func onAddObserverKVOPlayer() {
        self.player?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: .new, context: nil)
        self.player?.currentItem?.addObserver(self, forKeyPath: AVConstant.duration, options: [.new, .initial], context: nil)
        self.onListenTimeObserver()
    }
    
    func onListenTimeObserver() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        
        self.player?.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { [weak self] _ in
            guard let currentItem = self?.player?.currentItem else {return}
            
            let duration = currentItem.duration.seconds.rounded(toPlaces: 1)
            let seconds = currentItem.currentTime().seconds.rounded(toPlaces: 1)
            //let timeString = self?.getTimeString(from: currentItem.currentTime())
            
//            print("\naddTimeObserver: ======================== ")
//            print("addTimeObserver: duration: ", duration)
//            print("addTimeObserver: seconds: ", seconds)
            
            print("addTimeObserver: seconds:", seconds)
            
            if seconds == duration {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self?.onRePlay()
                }
            }
            
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            var status: AVPlayerItem.Status = .unknown
            if let statusNumber = change?[.newKey] as? NSNumber,
                let newStatus = AVPlayerItem.Status(rawValue: statusNumber.intValue) {
                status = newStatus
            }
            
            switch status {
            case .readyToPlay:
                print("observeValue readyToPlay")
                
            case .failed:
                print("observeValue failed")
                
            case .unknown:
                print("observeValue unknown")
                
            @unknown default:
                fatalError()
            }
            
        } else if keyPath == AVConstant.duration, let duration = self.player?.currentItem?.duration.seconds, duration > 0.0 {
            print("observeValue duration ", duration)
        }
    }
    
    func getTimeString(from time: CMTime) -> String {
        let totalSeconds = CMTimeGetSeconds(time)
        let hours = Int(totalSeconds/3600)
        let minutes = Int(totalSeconds/60) % 60
        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        if hours > 0 {
            return String(format: "%i:%02i:%02i", arguments: [hours, minutes, seconds])
            
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
        //self.durationLabel.text = getTimeString(from: player.currentItem!.duration)
    }
}
