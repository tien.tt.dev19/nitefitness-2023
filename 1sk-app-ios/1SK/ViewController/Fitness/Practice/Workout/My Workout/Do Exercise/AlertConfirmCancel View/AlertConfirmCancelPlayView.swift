//
//  AlertConfirmCancelPlayView.swift
//  1SK
//
//  Created by vuongbachthu on 7/9/21.
//

import UIKit

class AlertConfirmCancelPlayView: UIView {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var btn_Dismiss: UIButton!
    @IBOutlet weak var btn_Continue: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setPrepareViewForInitView() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
    }
    
    func setShowAlertAnimate() {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.alpha = 1
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func setRemoveAlertAnimate(viewWithTag: UIView, tag: Int) {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                viewWithTag.removeFromSuperview()
            }
        })
    }

}
