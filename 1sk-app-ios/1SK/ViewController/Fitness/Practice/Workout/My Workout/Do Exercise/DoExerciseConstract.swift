//
//  
//  DoExerciseConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

// MARK: - View
protocol DoExerciseViewProtocol: AnyObject {
    func onViewDidLoad(workout: WorkoutModel)
    func onDismissDetailViewAction()
}

// MARK: - Presenter
protocol DoExercisePresenterProtocol {
    func onViewDidLoad(controller: UIViewController)
    
    func onShowHub()
    func onHiddenHub()
    func onToastMessage(message: String)
    func onShowUnsupportView()
    
    func onBackItemNavigationAction()
    func onMusicItemNavigationAction(isPlay: Bool)
    func onInfoDetailAction(index: Int)
    
    func getDetailExercise(at index: Int) -> WorkoutDetailModel?
    func getUrlsListVideo() -> [URL]
    func getAudioMusic() -> MusicModel?
    
    func onSkipExercise(index: Int)
    func onFinishedExercise(index: Int)
    
    func onFinistWorkout(timePractice: Int, timeStart: Double)
    
}

// MARK: - Interactor Input
protocol DoExerciseInteractorInputProtocol {
    
}

// MARK: - Interactor Output
protocol DoExerciseInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol DoExerciseRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func popToRootViewController()
    func gotoDetailExserciseDoViewController(with detail: WorkoutDetailModel, delegate: ExerciseDoPresenterDelegate?)
    func gotoFinishWorkoutViewController(with workout: WorkoutModel, timeStart: Double, timePractice: Int, isSaveCategory: Bool)
}
