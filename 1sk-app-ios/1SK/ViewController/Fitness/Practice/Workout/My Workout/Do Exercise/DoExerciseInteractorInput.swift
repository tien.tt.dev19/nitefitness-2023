//
//  
//  DoExerciseInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class DoExerciseInteractorInput {
    weak var output: DoExerciseInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - DoExerciseInteractorInputProtocol
extension DoExerciseInteractorInput: DoExerciseInteractorInputProtocol {
    
}
