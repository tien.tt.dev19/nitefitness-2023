//
//  
//  DoExercisePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit
import AVKit
import AVFoundation

protocol DoExercisePresenterDelegate: AnyObject {
    func onActionDoExercise()
}

class DoExercisePresenter {
    weak var view: DoExerciseViewProtocol?
    private var interactor: DoExerciseInteractorInputProtocol
    private var router: DoExerciseRouterProtocol
    weak var delegate: DoExercisePresenterDelegate?
    
    var workout: WorkoutModel!
    private var isPlayOnline = true
    var isSaveCategory = false
    
    init(interactor: DoExerciseInteractorInputProtocol,
         router: DoExerciseRouterProtocol, isPlayOnline: Bool) {
        self.interactor = interactor
        self.router = router
        self.isPlayOnline = isPlayOnline
    }
    
}

// MARK: - DoExercisePresenterProtocol
extension DoExercisePresenter: DoExercisePresenterProtocol {
    func onShowUnsupportView() {
        self.router.showToast(content: "Tính năng này chỉ hỗ trợ iOS 13.0 trở lên.")
    }
    
    func onViewDidLoad(controller: UIViewController) {
        self.view?.onViewDidLoad(workout: self.workout)
        
    }
    
    // MARK: - Base Action
    func onShowHub() {
        self.router.showHud()
    }
    
    func onHiddenHub() {
        self.router.hideHud()
    }
    
    func onToastMessage(message: String) {
        self.router.showToast(content: message)
    }
    
    // MARK: - Navigation Action
    func onBackItemNavigationAction() {
        if isSaveCategory {
            self.router.popToRootViewController()
            NotificationCenter.default.post(name: .showFitnessHomePage, object: nil)
        } else {
            self.router.popViewController()
        }
    }
    
    func onMusicItemNavigationAction(isPlay: Bool) {
        //
    }
    
    func onInfoDetailAction(index: Int) {
        if let details = self.workout.details {
            if !details.isEmpty {
                let detail = details[index]
                self.router.gotoDetailExserciseDoViewController(with: detail, delegate: self)
            }
        }
    }
    
    // MARK: - Player Action
    func getDetailExercise(at index: Int) -> WorkoutDetailModel? {
        if let details = self.workout.details {
            if !details.isEmpty {
                return details[index]
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    
    func getUrlsListVideo() -> [URL] {
        var listVideoURL = [URL]()
        
        if let details = self.workout.details {
            for detail in details {
                if isPlayOnline {
                    if let videoPath = detail.exercise?.movementM3u8 {
                        let videoURL = URL(string: videoPath)!
                        listVideoURL.append(videoURL)
                    }
                } else {
                    if let videoPath = detail.exercise?.videoPath {
                        let videoURL = URL(fileURLWithPath: videoPath)
                        listVideoURL.append(videoURL)
                    }
                }
            }
        }
        return listVideoURL
    }
    
    func getAudioMusic() -> MusicModel? {
        return self.workout.music
    }
    
    func onSkipExercise(index: Int) {
        self.workout.details?[index].exercise?.status = 2
    }
    
    func onFinishedExercise(index: Int) {
        self.workout.details?[index].exercise?.status = 1
    }
    
    func onFinistWorkout(timePractice: Int, timeStart: Double) {
        self.router.gotoFinishWorkoutViewController(with: self.workout, timeStart: timeStart, timePractice: timePractice, isSaveCategory: isSaveCategory)
    }
}

// MARK: - DoExerciseInteractorOutput 
extension DoExercisePresenter: DoExerciseInteractorOutputProtocol {
    
}

// MARK: - ExerciseDoPresenterDelegate
extension DoExercisePresenter: ExerciseDoPresenterDelegate {
    func onDismissDetailViewAction() {
        self.view?.onDismissDetailViewAction()
    }
}
