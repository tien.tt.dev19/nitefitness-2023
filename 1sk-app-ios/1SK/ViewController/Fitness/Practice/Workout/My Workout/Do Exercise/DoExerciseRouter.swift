//
//  
//  DoExerciseRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class DoExerciseRouter: BaseRouter {
//    weak var viewController: DoExerciseViewController?
    static func setupModule(with workout: WorkoutModel?, isPlayOnline: Bool, isSaveCategory: Bool, delegate: DoExercisePresenterDelegate?) -> DoExerciseViewController {
        let viewController = DoExerciseViewController()
        let router = DoExerciseRouter()
        let interactorInput = DoExerciseInteractorInput()
        let presenter = DoExercisePresenter(interactor: interactorInput, router: router, isPlayOnline: isPlayOnline)
        presenter.isSaveCategory = isSaveCategory
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.workout = workout
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - DoExerciseRouterProtocol
extension DoExerciseRouter: DoExerciseRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func popToRootViewController() {
        self.viewController?.navigationController?.popToRootViewController(animated: false)
    }
    
    func gotoDetailExserciseDoViewController(with detail: WorkoutDetailModel, delegate: ExerciseDoPresenterDelegate?) {
        let controller = ExerciseDoRouter.setupModule(with: detail, delegate: delegate)
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func gotoFinishWorkoutViewController(with workout: WorkoutModel, timeStart: Double, timePractice: Int, isSaveCategory: Bool) {
        let controller = FinishWorkoutRouter.setupModule(with: workout, timeStart: timeStart, timePractice: timePractice, isSaveCategory: isSaveCategory)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
