//
//  
//  DoExerciseViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit
import AVKit
import AVFoundation
import MediaPlayer

class DoExerciseViewController: BaseViewController {

    // Init View
    @IBOutlet weak var lbl_TitleNavigation: UILabel!
    @IBOutlet weak var view_ProgressView: UIView!
    @IBOutlet weak var view_PlayerView: UIView!
    @IBOutlet weak var btn_Music: UIButton!

    // Palyer View
    @IBOutlet weak var stack_TimePlayer: UIStackView!
    @IBOutlet weak var lbl_TimePlayer: UILabel!
    @IBOutlet weak var lbl_NameExercisePlayer: UILabel!
    @IBOutlet weak var btn_Previous: UIButton!
    @IBOutlet weak var btn_Play: UIButton!
    @IBOutlet weak var btn_Next: UIButton!
    @IBOutlet weak var btn_Finish: UIButton!

    // Relax View
    @IBOutlet weak var lbl_TimeRelax: UILabel!
    @IBOutlet weak var btn_Skip: UIButton!
    @IBOutlet weak var constraint_right_RelaxView: NSLayoutConstraint!

    @IBOutlet weak var stack_TimeContinue: UIStackView!
    @IBOutlet weak var lbl_StepContinue: UILabel!
    @IBOutlet weak var lbl_NameExerciseContinue: UILabel!
    @IBOutlet weak var lbl_TimeContinue: UILabel!
    @IBOutlet weak var lbl_workoutGuild: UILabel!
    
    var presenter: DoExercisePresenterProtocol!
    var circleProgressView: CircleProgressView!

    var view_AlertCancel = AlertConfirmCancelPlayView()

    private var timerPlayer = Timer()
    private var timerRelax = Timer()
    private var timerPractice = Timer()

    private var timeExDuration: TimeInterval = 0
    private var numberExDuration: Int = 0
    private var typeDisplay: TypeDisplay?

    private var timeRelaxDuration: TimeInterval = 10

    private var endTimePlayer: Date?
    private var currentVideoIndex: Int = 0

    var listVideoUrl: [URL] = []

    // A&V Music
    var audioEngine: AVAudioEngine?
    var playerMusic: AVAudioPlayerNode?
    var isTouchButtonPlayMusic = true

    // A&V-Player
    var player: AVPlayer?
    var playerLayer: AVPlayerLayer?
    var isPlaying = false
    var isPlayEnd = false

    var timePracticeCount = 0
    var timeStart: Double = 0
    var isShowInfoDetail = false
    var isShowGuild = false

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad(controller: self)
        self.lbl_workoutGuild.isUserInteractionEnabled = true
        self.lbl_workoutGuild.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleShowGuild)))
        self.btn_Skip.isEnabled = false
        self.setupRemoteTransportControls()
    }

    override func viewWillAppear(_ animated: Bool) {
//        if !UIDevice.current.orientation.isLandscape {
            AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !isShowGuild {
            AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
            let commandCenter = MPRemoteCommandCenter.shared()
            commandCenter.playCommand.removeTarget(self)
            commandCenter.pauseCommand.removeTarget(self)
            self.player = nil
            self.playerLayer = nil
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        self.onStopCountdownRelax()
        self.onStopCountdownPlayer()
        self.onStopCountPractice()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.playerLayer?.frame = self.view_PlayerView.bounds
    }

    deinit {
        print("deinit DoExerciseViewController")
    }

    // MARK: - Setup
    private func setupDefaults() {
        self.setGestureView()
    }

    @objc private func handleShowGuild() {
//        if #available(iOS 13.0, *) {
            self.isShowInfoDetail = true
            self.isShowGuild = true
            self.onPause(isTouch: true)
            self.presenter.onInfoDetailAction(index: self.currentVideoIndex)
//        } else {
//            self.presenter.onShowUnsupportView()
//        }
    }

    // MARK: - Action
    @IBAction func onBackItemNavigationAction(_ sender: Any) {
        self.setShowAlertCancelView()
    }

    @IBAction func onMusicItemNavigationAction(_ sender: Any) {
        self.btn_Music.isSelected = !self.btn_Music.isSelected
        self.setPlayerMusic(isPlay: self.btn_Music.isSelected)
        self.isTouchButtonPlayMusic = self.btn_Music.isSelected
    }

    @IBAction func onInfoDetailAction(_ sender: Any) {
//        if #available(iOS 13.0, *) {
            self.isShowInfoDetail = true
            self.isShowGuild = true
            self.onPause(isTouch: true)
            self.presenter.onInfoDetailAction(index: self.currentVideoIndex)
//        } else {
//            self.presenter.onShowUnsupportView()
//        }
    }

    @IBAction func onAddTimeRelaxAction(_ sender: Any) {
        self.timeRelaxDuration += 20
        self.lbl_TimeRelax.text = self.timeRelaxDuration.asTimeFormat
    }

    @IBAction func onSkipAction(_ sender: Any) {
        self.onSkip()
    }

    // MARK: - Handle Player Action
    @IBAction func onPlayerAction(_ sender: Any) {
        if self.isPlaying {
            self.onPause(isTouch: true)
        } else {
            self.onPlay()
        }
    }

    @IBAction func onPreviousVideoAction(_ sender: Any) {
        self.onPrevious()
    }
    
    @IBAction func onNextVideoAction(_ sender: Any) {
        self.onNext()
    }

}

// MARK: - DoExerciseViewProtocol
extension DoExerciseViewController: DoExerciseViewProtocol {
    func onViewDidLoad(workout: WorkoutModel) {
        self.setCountdownProgressView()
        self.listVideoUrl = self.presenter.getUrlsListVideo()
        self.setPlayerExercise(index: 0, isAutoNext: true)
        
        self.onInitPlayMusic()
    }
    
    func onDismissDetailViewAction() {
        self.isShowGuild = false
        self.isShowInfoDetail = false
        self.onPlay()
    }
}

// MARK: AV Player Audio
extension DoExerciseViewController {
    func onInitPlayMusic() {
        if let music = self.presenter.getAudioMusic() {
            if let pathUrl = music.path {
                let audioURL = URL(fileURLWithPath: pathUrl, isDirectory: true)
                do {
                    let audioFile = try AVAudioFile(forReading: audioURL)
                    guard let buffer = AVAudioPCMBuffer(pcmFormat: audioFile.processingFormat, frameCapacity: .init(audioFile.length)) else { return }
                    try audioFile.read(into: buffer)
                    
                    self.audioEngine = AVAudioEngine()
                    self.playerMusic = AVAudioPlayerNode()
                    
                    self.audioEngine?.attach(self.playerMusic!)
                    self.audioEngine?.connect(self.playerMusic!, to: self.audioEngine!.mainMixerNode, format: buffer.format)
                    try audioEngine?.start()

                    //self.playerMusic?.play()
                    self.playerMusic?.scheduleBuffer(buffer, at: nil, options: .loops)
                    self.setPlayerMusic(isPlay: true)
                    print("onStartPlayMusic playerMusic init true", pathUrl)
                } catch {
                    print("onStartPlayMusic url error: ", error.localizedDescription)
                }
            } else {
                print("onStartPlayMusic pathUrl nil")
            }
        } else {
            print("onStartPlayMusic music nil")
        }
    }

    func setupRemoteTransportControls() {
        // Get the shared MPRemoteCommandCenter
        let commandCenter = MPRemoteCommandCenter.shared()

        // Add handler for Play Command
        commandCenter.playCommand.addTarget { [weak self] event in
            guard let `self` = self else { return .commandFailed }
            if self.player?.rate == 0.0 {
                self.player?.play()
                if self.playerMusic?.isPlaying == false {
                    self.playerMusic?.play()
                }
                return .success
            }
            return .commandFailed
        }

        // Add handler for Pause Command
        commandCenter.pauseCommand.addTarget { [weak self] event in
            guard let `self` = self else { return .commandFailed }
            if self.player?.rate == 1.0 {
                self.player?.pause()
                if self.playerMusic?.isPlaying == true {
                    self.playerMusic?.pause()
                }
                return .success
            }
            return .commandFailed
        }
    }

    func setPlayerMusic(isPlay: Bool) {
        if self.playerMusic != nil {
            if isPlay {
                self.btn_Music.isSelected = true
                if self.playerMusic?.isPlaying == false {
                    if audioEngine?.isRunning == true {
                        self.playerMusic?.play()
                    } else {
                        self.onInitPlayMusic()
                    }
                }
            } else {
                self.btn_Music.isSelected = false
                if self.playerMusic?.isPlaying == true {
                    self.playerMusic?.pause()
                }
            }
        }
    }
}

// MARK: AV Player Video
extension DoExerciseViewController {
    func setPlayerExercise(index: Int, isAutoNext: Bool) {
        if index <= self.listVideoUrl.count - 1 {
            print("setPlayerExercise Success index:", index)
            guard let detail = self.presenter.getDetailExercise(at: index) else { return }
            switch detail.exercise?.typeDisplay {
            case TypeDisplay.time.rawValue:
                self.typeDisplay = .time
                self.timeExDuration = TimeInterval(detail.time ?? 0)
                self.lbl_TimePlayer.text = self.timeExDuration.asTimeFormat
                self.lbl_TimeContinue.text = self.timeExDuration.asTimeFormat
            case TypeDisplay.number.rawValue:
                self.typeDisplay = .number
                self.numberExDuration = detail.time ?? 0
                self.lbl_TimePlayer.text = "x\(self.numberExDuration)"
                self.lbl_TimeContinue.text = "x\(self.numberExDuration)"
            default:
                return
            }

            self.lbl_NameExercisePlayer.text = detail.exercise?.name
            self.lbl_NameExerciseContinue.text = detail.exercise?.name
            self.lbl_StepContinue.text = "Tiếp theo (\(index + 1)/\(self.listVideoUrl.count))"
            if self.player == nil {
                self.playVideoWithInitFisrt()
            } else {
                self.playVideoWithItemIndex(index: index, isAutoNext: isAutoNext)
            }
            
        } else {
            self.onPause(isTouch: false)
            print("setPlayerExercise Falure index:", index)
        }
        
        let countListVideo = self.listVideoUrl.count
        if index == 0 {
            self.btn_Previous.alpha = 0
            self.btn_Next.alpha = 1
            
        } else if index == countListVideo - 1 {
            self.btn_Previous.alpha = 1
            self.btn_Next.alpha = 0
            
        } else {
            self.btn_Previous.alpha = 1
            self.btn_Next.alpha = 1
        }
        
        if countListVideo <= 1 {
            self.btn_Previous.alpha = 0
            self.btn_Next.alpha = 0
        }
    }
    
    func playVideoWithInitFisrt() {
        if self.listVideoUrl.count > 0 {
            self.currentVideoIndex = 0
            
//            let link1 = "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8"
//            let url = URL(string: link1)!
//            let currentPlayerItem = AVPlayerItem(url: url)

            let currentPlayerItem = AVPlayerItem(url: self.listVideoUrl[self.currentVideoIndex])
            self.player = AVPlayer(playerItem: currentPlayerItem)
            self.onAddObserverKVOPlayer()
            
            self.playerLayer = AVPlayerLayer(player: self.player)
            self.playerLayer?.videoGravity = .resizeAspect
            self.view_PlayerView.layer.addSublayer(self.playerLayer!)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.player?.pause()
            }
            
        }
    }
    
    func playVideoWithItemIndex(index: Int, isAutoNext: Bool) {
        if self.listVideoUrl.count > index {
            self.currentVideoIndex = index
            
            let currentPlayerItem = AVPlayerItem(url: self.listVideoUrl[index])
            self.player?.replaceCurrentItem(with: currentPlayerItem)
            
            self.player?.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.player?.pause()
            }
                
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                UIView.animate(withDuration: 1.0) {
                    self.stack_TimeContinue.alpha = 1
                    self.stack_TimePlayer.alpha = 0
                    self.view_PlayerView.alpha = 1
                    self.btn_Skip.isEnabled = true
                    self.view.layoutIfNeeded()
                }
                
                if !isAutoNext {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        UIView.animate(withDuration: 0.5) {
                            self.stack_TimeContinue.alpha = 0
                            self.btn_Skip.isEnabled = false
                            self.stack_TimePlayer.alpha = 1
                            self.view.layoutIfNeeded()
                            
                        } completion: { _ in
                            self.onPlay()
                        }
                    }
                }
            }
        }
    }
    
    func onPlay() {
        self.isPlaying = true
        self.player?.play()
        
        switch self.typeDisplay {
        case .time:
            self.onStartCountdownPlayer()
            
        case .number:
            break
        default:
            return
        }
        
        self.btn_Play.setImage(UIImage(named: "ic-play-process"), for: .normal)
        self.btn_Play.isEnabled = true
        self.btn_Next.isEnabled = true
        self.btn_Previous.isEnabled = true
        
        if self.isTouchButtonPlayMusic {
            self.setPlayerMusic(isPlay: true)
        }
    }
    
    func onRePlay() {
        self.isPlaying = true
        self.player?.seek(to: CMTime.zero)
        self.player?.play()
        self.btn_Play.setImage(UIImage(named: "ic-play-process"), for: .normal)
        
    }

    func onPause(isTouch: Bool) {
        self.isPlaying = false
        self.player?.pause()
        self.onStopCountdownPlayer()
        self.btn_Play.setImage(UIImage(named: "ic-play-stop"), for: .normal)
        
        if isTouch {
            self.setPlayerMusic(isPlay: false)
        }
    }

    func onPrevious() {
        if self.currentVideoIndex > 0 {
            self.onPause(isTouch: false)

            self.lbl_TimePlayer.text = ""
            self.btn_Play.isEnabled = false
            self.btn_Previous.isEnabled = false
            self.btn_Next.isEnabled = false

            UIView.animate(withDuration: 1.0) {
                self.constraint_right_RelaxView.constant = 0
                self.stack_TimePlayer.alpha = 0
                self.view_PlayerView.alpha = 0
                self.view.layoutIfNeeded()
            } completion: { _ in
//                self.setPlayerExercise(index: self.currentVideoIndex - 1, isAutoNext: false)
                self.setPlayerExercise(index: self.currentVideoIndex - 1, isAutoNext: true)
                self.onStartCountdownRelax()
            }
        }
    }
    
    func onNext() {
        if self.currentVideoIndex < self.listVideoUrl.count - 1 {
            self.onPause(isTouch: false)
            
            self.lbl_TimePlayer.text = ""
            self.btn_Play.isEnabled = false
            self.btn_Previous.isEnabled = false
            self.btn_Next.isEnabled = false
            
            UIView.animate(withDuration: 1.0) {
                self.constraint_right_RelaxView.constant = 0
                
                self.stack_TimePlayer.alpha = 0
                self.view_PlayerView.alpha = 0
                self.view.layoutIfNeeded()
                
            } completion: { _ in
//                self.setPlayerExercise(index: self.currentVideoIndex + 1, isAutoNext: false)
                
                self.setPlayerExercise(index: self.currentVideoIndex + 1, isAutoNext: true)
                self.onStartCountdownRelax()
            }
        }
    }
    
    func onNextAuto() {
        self.onPause(isTouch: false)
        self.presenter.onFinishedExercise(index: self.currentVideoIndex)
        
        if self.currentVideoIndex < self.listVideoUrl.count - 1 {
            UIView.animate(withDuration: 1.0) {
                self.constraint_right_RelaxView.constant = 0
                self.stack_TimePlayer.alpha = 0
                self.view_PlayerView.alpha = 0
                self.view.layoutIfNeeded()
                
            } completion: { _ in
                self.setPlayerExercise(index: self.currentVideoIndex + 1, isAutoNext: true)
                self.onStartCountdownRelax()
            }
        } else {
            self.onFinish()
            
        }
    }
    
    func onSkip() {
//        self.timeRelaxDuration = 10
//        self.lbl_TimeRelax.text = self.timeRelaxDuration.asTimeFormat
//        self.presenter.onSkipExercise(index: self.currentVideoIndex)
//
//        self.setPlayerExercise(index: self.currentVideoIndex + 1, isAutoNext: true)
        
        
        self.onStopCountdownRelax()
        //self.presenter.onSkipExercise(index: self.currentVideoIndex)

//        UIView.animate(withDuration: 1.0) {
//            self.view_ProgressView.alpha = 0
//            self.stack_TimeContinue.alpha = 0.1
//            self.constraint_right_RelaxView.constant = -300
//            self.view.layoutIfNeeded()
//
//        } completion: { _ in
//            self.onPlay()
//            self.setPlayerExercise(index: self.currentVideoIndex + 1, isAutoNext: false)
//        }
        UIView.animate(withDuration: 0.5) {
            self.constraint_right_RelaxView.constant = -300
            self.stack_TimeContinue.alpha = 0
            self.btn_Skip.isEnabled = false
            self.stack_TimePlayer.alpha = 1
            self.view.layoutIfNeeded()
        } completion: { _ in
            self.onPlay()
        }
    }
    
    func onFinish() {
        self.player?.pause()
        
        self.onStopCountdownPlayer()
        self.onStopCountPractice()
        
        self.btn_Play.setImage(UIImage(named: "ic-play-success"), for: .normal)
        
        self.btn_Previous.isHidden = false
        self.btn_Next.isHidden = false
        
        self.btn_Play.isHidden = true
        self.btn_Finish.isHidden = false
        self.btn_Finish.isEnabled = true
        
        self.btn_Previous.isEnabled = false
        self.btn_Next.isEnabled = false
        
        self.btn_Previous.alpha = 1
        self.btn_Next.alpha = 1
        
        self.isPlaying = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
            self.presenter.onFinistWorkout(timePractice: self.timePracticeCount, timeStart: self.timeStart)
        }
        
    }
}

// MARK: Timer Practice
extension DoExerciseViewController {
    func onStartCountPractice() {
        self.timeStart = Utils.shared.getTimeSystemWidthStamp()
        self.timerPractice = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimeCountPractice), userInfo: nil, repeats: true)
        
        //self.onInitPlayMusic()
    }
    
    @objc func updateTimeCountPractice() {
        self.timePracticeCount += 1
//        print("updateTimeCountPractice: ", timePracticeCount)
    }
    
    func onStopCountPractice() {
        self.timerPractice.invalidate()
        self.setPlayerMusic(isPlay: false)
        
        print("onStopCountPractice")
    }
}

// MARK: Timer Relax
extension DoExerciseViewController {
    func onStartCountdownRelax() {
        self.timeRelaxDuration = 10
        self.lbl_TimeRelax.text = self.timeRelaxDuration.asTimeFormat
        self.timerRelax = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTimeCountdownRelax), userInfo: nil, repeats: true)
        
//        if self.currentVideoIndex == self.listVideoUrl.count - 1 {
//            self.btn_Skip.isHidden = true
//        } else {
//            self.btn_Skip.isHidden = false
//        }
    }
    
    @objc func updateTimeCountdownRelax() {
        if self.timeRelaxDuration > 1 {
            self.timeRelaxDuration -= 1
            self.lbl_TimeRelax.text = self.timeRelaxDuration.asTimeFormat
            
        } else {
            self.lbl_TimeRelax.text = "00:01"
            self.onStopCountdownRelax()
            
            UIView.animate(withDuration: 1.0) {
                self.constraint_right_RelaxView.constant = -300
                self.stack_TimeContinue.alpha = 0
                self.btn_Skip.isEnabled = false
                self.stack_TimePlayer.alpha = 1
                self.view.layoutIfNeeded()

            } completion: { _ in
                if self.isShowInfoDetail == false {
                    self.onPlay()
                }
            }
        }
    }
    
    func onStopCountdownRelax() {
        self.timerRelax.invalidate()
        print("onStopCountdownRelax")
    }
}

// MARK: Timer Player - Time
extension DoExerciseViewController {
    func onStartCountdownPlayer() {
        self.endTimePlayer = Date().addingTimeInterval(self.timeExDuration)
        self.timerPlayer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateTimeCountdownPlayer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimeCountdownPlayer() {
        if self.timeExDuration > 0 {
            self.timeExDuration = self.endTimePlayer?.timeIntervalSinceNow ?? 0
            self.lbl_TimePlayer.text = self.timeExDuration.asTimeFormat
            
            if self.timeExDuration <= 1 {
                self.btn_Play.isEnabled = false
                self.btn_Next.isEnabled = false
                self.btn_Previous.isEnabled = false
            }
        } else {
            self.lbl_TimePlayer.text = "00:00"
            self.onNextAuto()
        }
    }
    
    func onStopCountdownPlayer() {
        self.timerPlayer.invalidate()
        print("onStopCountdownPlayer")
    }
}

// MARK: CircleProgressViewDelegate
extension DoExerciseViewController: CircleProgressViewDelegate {
    func setCountdownProgressView() {
        self.circleProgressView = CircleProgressView(frame: CGRect(x: 0, y: 0, width: self.view_ProgressView.width, height: self.view_ProgressView.height))
        self.circleProgressView.center = self.view_ProgressView.center
        self.circleProgressView.delegate = self
        self.circleProgressView.timeInterval = 5
        self.circleProgressView.createCircularPath()
        self.circleProgressView.startProgress()
        self.view_ProgressView.addSubview(self.circleProgressView)
        
        self.btn_Next.isEnabled = false
        self.btn_Previous.isEnabled = false
    }
    
    func onCountdownStartPlaySuccess() {
        self.onPlay()
        self.onStartCountPractice()
        
        self.view_ProgressView.isHidden = true
        self.btn_Play.isHidden = false
        
        self.btn_Next.isEnabled = true
        self.btn_Previous.isEnabled = true
        
        UIView.animate(withDuration: 1.0) {
            self.lbl_TitleNavigation.alpha = 0.0
            
        } completion: { _ in
            self.lbl_TitleNavigation.text = ""
            self.lbl_TitleNavigation.alpha = 1
        }
    }
}


// MARK: KVO Observer
extension DoExerciseViewController {
    func onAddObserverKVOPlayer() {
//        self.player?.currentItem?.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: .new, context: nil)
//        self.player?.currentItem?.addObserver(self, forKeyPath: AVConstant.duration, options: [.new, .initial], context: nil)
        self.onListenTimeObserver()
    }
    
    func onListenTimeObserver() {
        let interval = CMTime(seconds: 1.0, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        
        self.player?.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { [weak self] _ in
            guard let currentItem = self?.player?.currentItem else {return}
            
            let duration = currentItem.duration.seconds.rounded(toPlaces: 1)
            let seconds = currentItem.currentTime().seconds.rounded(toPlaces: 1)
            //let timeString = self?.getTimeString(from: currentItem.currentTime())
            
//            print("\naddTimeObserver: ======================== ")
//            print("addTimeObserver: duration: ", duration)
//            print("addTimeObserver: seconds: ", seconds)
            
            print("addTimeObserver: seconds: \(self?.numberExDuration ?? 0)", seconds)
            
            if seconds == duration {
                if self?.isPlayEnd == false {
                    self?.isPlayEnd = true
                    
                    switch self?.typeDisplay {
                    case .time:
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self?.onRePlay()
                        }
                        
                    case .number:
                        if self?.numberExDuration ?? 0 > 1 {
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                self?.numberExDuration -= 1
                                self?.lbl_TimePlayer.text = "x\(self?.numberExDuration ?? 0)"
                                
                                self?.onRePlay()
                                
                                print("addTimeObserver: onRePlay: ", self?.numberExDuration ?? 0)
                            }
                            
                        } else {
                            self?.onNextAuto()
                        }
                        
                    default:
                        return
                    }
                    
                }
                
            } else {
                self?.isPlayEnd = false
            }
            
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == #keyPath(AVPlayerItem.status) {
            var status: AVPlayerItem.Status = .unknown
            if let statusNumber = change?[.newKey] as? NSNumber,
                let newStatus = AVPlayerItem.Status(rawValue: statusNumber.intValue) {
                status = newStatus
            }
            
            switch status {
            case .readyToPlay:
                print("observeValue readyToPlay")
                
            case .failed:
                print("observeValue failed")
                
            case .unknown:
                print("observeValue unknown")
                
            @unknown default:
                fatalError()
            }
            
        } else if keyPath == AVConstant.duration, let duration = self.player?.currentItem?.duration.seconds, duration > 0.0 {
            print("observeValue duration ", duration)
        }
    }
    
    func getTimeString(from time: CMTime) -> String {
        let totalSeconds = CMTimeGetSeconds(time)
        let hours = Int(totalSeconds / 3600)
        let minutes = Int(totalSeconds / 60 ) % 60
        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        if hours > 0 {
            return String(format: "%i:%02i:%02i", arguments: [hours, minutes, seconds])
            
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
    }
}

// MARK: - Init Alert View
extension DoExerciseViewController {
    func setShowAlertCancelView() {
        self.view_AlertCancel = AlertConfirmCancelPlayView.loadFromNib()
        self.view_AlertCancel.tag = 113
        self.view_AlertCancel.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view_AlertCancel.setPrepareViewForInitView()
        
        self.view_AlertCancel.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchUpBgAlertViewAction)))
        self.view_AlertCancel.btn_Dismiss.addTarget(self, action: #selector(self.onDismissViewAction), for: .touchUpInside)
        self.view_AlertCancel.btn_Continue.addTarget(self, action: #selector(self.onContinueViewAction), for: .touchUpInside)
        
        self.view.addSubview(self.view_AlertCancel)
        self.view_AlertCancel.setShowAlertAnimate()
    }
    
    @objc func onTouchUpBgAlertViewAction() {
//        self.setRemoveSubviewAlert()
    }
    
    @objc func onDismissViewAction() {
        ///
        self.presenter.onBackItemNavigationAction()
    }
    
    @objc func onContinueViewAction() {
        self.setRemoveSubviewAlert()
    }
    
    func setRemoveSubviewAlert() {
        if let viewWithTag = self.view.viewWithTag(113) {
            self.view_AlertCancel.setRemoveAlertAnimate(viewWithTag: viewWithTag, tag: 113)
        }
    }
}

// MARK: - Gesture View
extension DoExerciseViewController {
    func setGestureView() {
        self.view_PlayerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onPlayerAction(_:))))
    }
}
