//
//  
//  FinishWorkoutConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/10/21.
//
//

import UIKit

// MARK: - View
protocol FinishWorkoutViewProtocol: AnyObject {
    func onViewDidLoad(naneWorkout: String, numberFinished: Int, timePractice: Int)
}

// MARK: - Presenter
protocol FinishWorkoutPresenterProtocol {
    func onViewDidLoad()
    
    func onToastMessage(message: String)
    
    func onChangeRate(rate: RateState)
    func onSaveWorkout()
    
}

// MARK: - Interactor Input
protocol FinishWorkoutInteractorInputProtocol {
    func setFinishWorkoutSaveActivityUser(workout: WorkoutModel)
    func setRateWorkout(workout: WorkoutModel)
}

// MARK: - Interactor Output
protocol FinishWorkoutInteractorOutputProtocol: AnyObject {
    func onFinishWorkoutSaveActivityUserFinished(with result: Result<ActivityUserModel, APIError>)
    
    func onRateWorkoutFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - Router
protocol FinishWorkoutRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func popToRootViewController()
}
