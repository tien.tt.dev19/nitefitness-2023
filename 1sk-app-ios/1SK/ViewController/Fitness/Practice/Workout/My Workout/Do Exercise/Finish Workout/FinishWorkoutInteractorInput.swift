//
//  
//  FinishWorkoutInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 7/10/21.
//
//

import UIKit

class FinishWorkoutInteractorInput {
    weak var output: FinishWorkoutInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - FinishWorkoutInteractorInputProtocol
extension FinishWorkoutInteractorInput: FinishWorkoutInteractorInputProtocol {
    func setRateWorkout(workout: WorkoutModel) {
        self.exerciseService?.workoutRate(workout: workout, completion: { [weak self] result in
            self?.output?.onRateWorkoutFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setFinishWorkoutSaveActivityUser(workout: WorkoutModel) {
        self.exerciseService?.finishWorkoutSaveActivityUser(workout: workout, completion: { [weak self] result in
            self?.output?.onFinishWorkoutSaveActivityUserFinished(with: result.unwrapSuccessModel())
        })
    }
}
