//
//  
//  FinishWorkoutPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/10/21.
//
//

import UIKit

class FinishWorkoutPresenter {

    weak var view: FinishWorkoutViewProtocol?
    private var interactor: FinishWorkoutInteractorInputProtocol
    private var router: FinishWorkoutRouterProtocol
    var workout: WorkoutModel?
    var rate: RateState = .None
    
    var timeStart: Double = 0
    var timePractice: Int = 0
    var numberFinished: Int = 0
    var isSaveCategory: Bool = false
    
    init(interactor: FinishWorkoutInteractorInputProtocol,
         router: FinishWorkoutRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    func getNumberFinished() {
        if let details = workout?.details {
            for detail in details {
                if detail.exercise?.status != 2 {
                    numberFinished += 1
                }
            }
        }
    }
}

// MARK: - FinishWorkoutPresenterProtocol
extension FinishWorkoutPresenter: FinishWorkoutPresenterProtocol {
    func onViewDidLoad() {
        //self.getNumberFinished()
        self.view?.onViewDidLoad(naneWorkout: self.workout?.name ?? "", numberFinished: self.workout?.details?.count ?? 0, timePractice: self.timePractice)
    }
    
    func onToastMessage(message: String) {
        self.router.showToast(content: message)
    }
    
    func onChangeRate(rate: RateState) {
        self.rate = rate
        self.workout?.rate = rate.rawValue
    }
    
    func onSaveWorkout() {
        if self.rate == .None {
            self.onToastMessage(message: "Bạn vui lòng đánh giá chất lượng bài tập để tiếp tục")
            return
        }

        let startTime = Utils.shared.getTimeStringByTimeStamp(timeStamp: self.timeStart, timeFomat: "yyyy-MM-dd HH:mm:ss")
        self.workout?.timeStart = startTime

        self.workout?.timePractice = self.timePractice.asTimeFormatFull

        self.router.showHud()
        self.interactor.setFinishWorkoutSaveActivityUser(workout: self.workout!)
    }
}

// MARK: - FinishWorkoutInteractorOutput 
extension FinishWorkoutPresenter: FinishWorkoutInteractorOutputProtocol {
    func onFinishWorkoutSaveActivityUserFinished(with result: Result<ActivityUserModel, APIError>) {
        switch result {
        case .success:
            self.interactor.setRateWorkout(workout: self.workout!)
            
        case .failure:
            SKToast.shared.showToast()
            self.router.hideHud()
        }
    }
    
    func onRateWorkoutFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Đánh giá bài tập thành công")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                if self.isSaveCategory {
                    self.router.popToRootViewController()
                    NotificationCenter.default.post(name: .showFitnessHomePage, object: nil)
                } else {
                    self.router.popViewController()
                }
            }
        case .failure:
            SKToast.shared.showToast()
        }
        
        self.router.hideHud()
    }
    
}

enum RateState: Int {
    case Bad = 1
    case Normal = 2
    case Good = 3
    case None = 0
}
