//
//  
//  FinishWorkoutRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/10/21.
//
//

import UIKit

class FinishWorkoutRouter: BaseRouter {
//    weak var viewController: FinishWorkoutViewController?
    static func setupModule(with workout: WorkoutModel, timeStart: Double, timePractice: Int, isSaveCategory: Bool) -> FinishWorkoutViewController {
        let viewController = FinishWorkoutViewController()
        let router = FinishWorkoutRouter()
        let interactorInput = FinishWorkoutInteractorInput()
        let presenter = FinishWorkoutPresenter(interactor: interactorInput, router: router)
        presenter.isSaveCategory = isSaveCategory
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.workout = workout
        presenter.timeStart = timeStart
        presenter.timePractice = timePractice
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - FinishWorkoutRouterProtocol
extension FinishWorkoutRouter: FinishWorkoutRouterProtocol {
    func popViewController() {
        if let vc = viewController?.navigationController?.viewControllers.filter({$0 is MyWorkoutViewController}).first as? MyWorkoutViewController {
            viewController?.navigationController?.popToViewController(vc, animated: true)
        }
    }
    
    func popToRootViewController() {
        viewController?.navigationController?.popToRootViewController(animated: false)
    }
}
