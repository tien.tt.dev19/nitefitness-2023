//
//  
//  FinishWorkoutViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/10/21.
//
//

import UIKit

class FinishWorkoutViewController: BaseViewController {

    @IBOutlet weak var btn_BadRate: UIButton!
    @IBOutlet weak var btn_NormalRate: UIButton!
    @IBOutlet weak var btn_GoodRate: UIButton!
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Number: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    var presenter: FinishWorkoutPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {

    }
    
    // MARK: - Action
    
    @IBAction func onRateBadAction(_ sender: Any) {
        self.btn_BadRate.isSelected = !self.btn_BadRate.isSelected
        if self.btn_BadRate.isSelected {
            self.btn_NormalRate.isSelected = false
            self.btn_GoodRate.isSelected = false
            self.presenter.onChangeRate(rate: .Bad)
        } else {
            self.presenter.onChangeRate(rate: .None)
        }
    }
    
    @IBAction func onRateNormalAction(_ sender: Any) {
        self.btn_NormalRate.isSelected = !self.btn_NormalRate.isSelected
        if self.btn_NormalRate.isSelected {
            self.btn_BadRate.isSelected = false
            self.btn_GoodRate.isSelected = false
            self.presenter.onChangeRate(rate: .Normal)
        } else {
            self.presenter.onChangeRate(rate: .None)
        }
    }
    
    @IBAction func onRateGoodAction(_ sender: Any) {
        self.btn_GoodRate.isSelected = !self.btn_GoodRate.isSelected
        if self.btn_GoodRate.isSelected {
            self.btn_NormalRate.isSelected = false
            self.btn_BadRate.isSelected = false
            self.presenter.onChangeRate(rate: .Good)
        } else {
            self.presenter.onChangeRate(rate: .None)
        }
    }
    
    @IBAction func onSaveAction(_ sender: Any) {
        self.presenter.onSaveWorkout()
    }
}

// MARK: - FinishWorkoutViewProtocol
extension FinishWorkoutViewController: FinishWorkoutViewProtocol {
    func onViewDidLoad(naneWorkout: String, numberFinished: Int, timePractice: Int) {
        self.lbl_Name.text = naneWorkout
        self.lbl_Number.text = numberFinished.asString
        self.lbl_Time.text = timePractice.asTimeFormat
        
        
        
    }
}
