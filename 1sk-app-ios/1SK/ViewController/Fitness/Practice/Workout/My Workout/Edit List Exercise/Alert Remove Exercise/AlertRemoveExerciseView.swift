//
//  AlertRemoveExerciseView.swift
//  1SK
//
//  Created by vuongbachthu on 7/3/21.
//

import UIKit

class AlertRemoveExerciseView: UIView {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var lbl_Content: UILabel!
    @IBOutlet weak var btn_Dismiss: UIButton!
    @IBOutlet weak var btn_ConfirmRemove: UIButton!
    
    var content = "Content"
    var isUpdate = false
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setPrepareViewForInitView() {
        self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.alpha = 0
    }
    
    func setShowAlertAnimate() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            UIView.animate(withDuration: 0.25, animations: { () -> Void in
                self.alpha = 1
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
            })
        }
    }
    
    func setRemoveAlertAnimate(viewWithTag: UIView, tag: Int) {
        UIView.animate(withDuration: 0.25, animations: { () -> Void in
            self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.alpha = 0
            
        }, completion: { (finished) -> Void in
            if finished == true {
                viewWithTag.removeFromSuperview()
            }
        })
    }
}
