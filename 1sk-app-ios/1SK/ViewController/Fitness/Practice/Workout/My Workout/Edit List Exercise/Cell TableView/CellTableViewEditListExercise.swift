//
//  CellTableViewEditListExercise.swift
//  1SK
//
//  Created by vuongbachthu on 7/3/21.
//

import UIKit

protocol CellTableViewEditListExerciseDelegate: AnyObject {
    func onDidRemoveExerciseAction(at indexPath: IndexPath)
    func onDidSelectRow(at indexPath: IndexPath)
}

class CellTableViewEditListExercise: UITableViewCell {

    @IBOutlet weak var view_BgCell: UIView!
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var btn_Remove: UIButton!
    
    weak var delegate: CellTableViewEditListExerciseDelegate?
    var indexPath: IndexPath?
    weak var workoutDetail: WorkoutDetailModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.view_BgCell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchUpBgCellAction)))
    }
    
    @objc func onTouchUpBgCellAction() {
        self.view_BgCell.backgroundColor = UIColor.lightGray
        self.delegate?.onDidSelectRow(at: self.indexPath!)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            UIView.animate(withDuration: 0.3) {
                self.view_BgCell.backgroundColor = UIColor.white
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(with item: WorkoutDetailModel?) {
        self.workoutDetail = item
        guard let `item` = item else {
            self.showSkeleton()
            return
        }
        self.img_Image.setImageWith(imageUrl: item.exercise?.image ?? "", placeHolder: UIImage(named: "img_default"))
        self.lbl_Name.text = item.exercise?.name
        
        switch item.exercise?.typeDisplay {
        case TypeDisplay.time.rawValue:
            self.lbl_Time.text = item.time?.asTimeFormat
            
        case TypeDisplay.number.rawValue:
            self.lbl_Time.text = "x\(item.time)"
            
        default:
            break
        }
        
        self.hideSkeleton()
    }
    
    @IBAction func onRemoveExerciseAction(_ sender: Any) {
        self.delegate?.onDidRemoveExerciseAction(at: self.indexPath!)
    }
    
}
