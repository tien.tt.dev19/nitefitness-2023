//
//  
//  EditListExerciseConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

// MARK: - View
protocol EditListExerciseViewProtocol: AnyObject {
    func onReloadData(workout: WorkoutModel)
}

// MARK: - Presenter
protocol EditListExercisePresenterProtocol {
    func onViewDidLoad()
    
    // MARK: - Action View
    func onDidBackNavigationItemAction()
    func onDidAddExerciseAction()
    func onDidMenuExerciseAction()
    
    // MARK: - TableView
    func numberOfRows(in section: Int) -> Int
    func itemDataForRow(at index: IndexPath) -> WorkoutDetailModel?
    func onDidSelectRow(at index: IndexPath)
    func onDidRemoveConfirm(at index: IndexPath)
    func onMoveRowAt(sourceIndexPath: IndexPath, destinationIndexPath: IndexPath)
    func onDragSessionWillBegin()
    func onDragSessionDidEnd()
}

// MARK: - Interactor Input
protocol EditListExerciseInteractorInputProtocol {
    func setEdiUpdateWorkout(workout: WorkoutModel, detailAdd: WorkoutDetail?)
}

// MARK: - Interactor Output
protocol EditListExerciseInteractorOutputProtocol: AnyObject {
    func onEdiUpdateWorkoutFinished(with result: Result<WorkoutModel, APIError>, total: Int)
}

// MARK: - Router
protocol EditListExerciseRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func gotoAddExserciseViewController(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?)
    func gotoDetailExserciseViewController(with detail: WorkoutDetailModel, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen)
}
