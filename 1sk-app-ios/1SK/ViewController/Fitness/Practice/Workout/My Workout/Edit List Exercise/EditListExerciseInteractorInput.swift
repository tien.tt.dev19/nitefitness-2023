//
//  
//  EditListExerciseInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class EditListExerciseInteractorInput {
    weak var output: EditListExerciseInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - EditListExerciseInteractorInputProtocol
extension EditListExerciseInteractorInput: EditListExerciseInteractorInputProtocol {
    func setEdiUpdateWorkout(workout: WorkoutModel, detailAdd: WorkoutDetail?) {
        self.exerciseService?.updateWorkout(workout: workout, detailAdd: detailAdd, completion: { [weak self] result in
            self?.output?.onEdiUpdateWorkoutFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0)
        })
    }
}
