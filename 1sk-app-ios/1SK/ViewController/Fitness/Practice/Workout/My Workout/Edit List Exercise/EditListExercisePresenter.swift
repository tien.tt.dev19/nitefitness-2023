//
//  
//  EditListExercisePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

protocol EditListExercisePresenterDelegate: AnyObject {
    func onDidEditListExerciseAction(workout: WorkoutModel)
}

enum EditListStatus {
    case Add
    case Reorder
    case Remove
    case Edit
    case None
}

class EditListExercisePresenter {

    weak var view: EditListExerciseViewProtocol?
    private var interactor: EditListExerciseInteractorInputProtocol
    private var router: EditListExerciseRouterProtocol
    weak var delegate: EditListExercisePresenterDelegate?
    
    var workout: WorkoutModel!
    
    var editListStatus: EditListStatus = .None
    
    init(interactor: EditListExerciseInteractorInputProtocol,
         router: EditListExerciseRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

// MARK: - EditListExercisePresenterProtocol
extension EditListExercisePresenter: EditListExercisePresenterProtocol {
    func onViewDidLoad() {
        
    }
    
    // MARK: - Action View
    func onDidBackNavigationItemAction() {
        self.router.popViewController()
    }
    
    func onDidAddExerciseAction() {
        self.router.gotoAddExserciseViewController(with: self.workout, delegate: self)
    }
    
    func onDidMenuExerciseAction() {
        print("onDidMenuExerciseAction")
    }
    
    // MARK: - TableView
    func numberOfRows(in section: Int) -> Int {
        return self.workout.details?.count ?? 0
    }
    
    func itemDataForRow(at index: IndexPath) -> WorkoutDetailModel? {
        return self.workout.details?[index.row]
    }
    
    func onDidSelectRow(at index: IndexPath) {
        if let details = self.workout.details {
            if !details.isEmpty {
                let detail = details[index.row]
                self.router.gotoDetailExserciseViewController(with: detail, delegate: self, state: .edit)
            }
        }
    }
    
    func onMoveRowAt(sourceIndexPath: IndexPath, destinationIndexPath: IndexPath) {
        if let item = self.workout.details?.remove(at: sourceIndexPath.row) {
            self.workout.details?.insert(item, at: destinationIndexPath.row)
        }
    }
    
    func onDragSessionWillBegin() {
        print("onDragSessionWillBegin \n\n")
    }
    
    func onDragSessionDidEnd() {
        if let details = self.workout.details {
            for (index, item) in details.enumerated() {
                item.order = index + 1
                print("onDragSessionDidEnd: name: \(item.order ?? 0) -", item.exercise?.name ?? "")
            }
        }
        print("\n\n")
        
        self.editListStatus = .Reorder
        self.router.showHud()
        self.interactor.setEdiUpdateWorkout(workout: self.workout, detailAdd: nil)
    }
    
    func onDidRemoveConfirm(at index: IndexPath) {
        if let videoUrl = self.workout.details?[index.row].exercise?.video, let fileName = videoUrl.split(separator: "/").last {
            let pathVideo = String(format: "%@/%@/%@", "workout", String(self.workout.id ?? 0), String(fileName))
            FileHelper.shared.setRemoveVideo(path: pathVideo)
        }
        
        self.workout.details?.remove(at: index.row)
        if let details = self.workout.details {
            for (index, item) in details.enumerated() {
                item.order = index + 1
            }
        }

        self.editListStatus = .Remove
        self.router.showHud()
        self.interactor.setEdiUpdateWorkout(workout: self.workout, detailAdd: nil)
        
    }
}

// MARK: - EditListExerciseInteractorOutput 
extension EditListExercisePresenter: EditListExerciseInteractorOutputProtocol {
    func onEdiUpdateWorkoutFinished(with result: Result<WorkoutModel, APIError>, total: Int) {
        switch result {
        case .success(let workoutModel):
            self.workout = workoutModel
            let details = self.workout.details?.sorted(by: { $0.order ?? 0 < $1.order ?? 0 })
            self.workout.details = details
            
            self.view?.onReloadData(workout: self.workout)
            
            switch self.editListStatus {
            case .Add:
                self.router.showToast(content: "Đã Thêm động tác")
                
            case .Reorder:
                self.router.showToast(content: "Đã sắp xếp động tác xong")
                
            case .Remove:
                self.router.showToast(content: "Đã xóa động tác")
                
            case .Edit:
                self.router.showToast(content: "Đã sửa động tác")
                
            default:
                break
            }
            
            
            self.delegate?.onDidEditListExerciseAction(workout: self.workout)
            
        case .failure:
            self.router.showToast(content: "Đã có lỗi xảy ra")
        }
        self.router.hideHud()
    }
}

// MARK: - AddExsercisePresenterDelegate
extension EditListExercisePresenter: AddExsercisePresenterDelegate {
    func onDidAddExercise(workout: WorkoutModel) {
        self.workout = workout
        let details = self.workout.details?.sorted(by: { $0.order ?? 0 < $1.order ?? 0 })
        self.workout.details = details
        
        self.view?.onReloadData(workout: self.workout)
        self.delegate?.onDidEditListExerciseAction(workout: self.workout)
    }
}

// MARK: - DetailExercisePresenterDelegate
extension EditListExercisePresenter: DetailExercisePresenterDelegate {
    func onUpdateExerciseAction(detail: WorkoutDetailModel) {
        if let detailEdit = self.workout.details?.enumerated().first(where: {$0.element.id == detail.id}) {
            self.workout.details?[detailEdit.offset] = detail
            
            self.editListStatus = .Edit
            self.router.showHud()
            self.interactor.setEdiUpdateWorkout(workout: self.workout, detailAdd: nil)
        }
    }
    
    func onAddExerciseDetailAction(exsercise: ExerciseModel) {
        /// Not use
    }
}
