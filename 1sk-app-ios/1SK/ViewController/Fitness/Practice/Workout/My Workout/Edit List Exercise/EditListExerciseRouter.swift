//
//  
//  EditListExerciseRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class EditListExerciseRouter: BaseRouter {
//    weak var viewController: EditListExerciseViewController?
    static func setupModule(with workout: WorkoutModel, delegate: EditListExercisePresenterDelegate?) -> EditListExerciseViewController {
        let viewController = EditListExerciseViewController()
        let router = EditListExerciseRouter()
        let interactorInput = EditListExerciseInteractorInput()
        let presenter = EditListExercisePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.workout = workout
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - EditListExerciseRouterProtocol
extension EditListExerciseRouter: EditListExerciseRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func gotoAddExserciseViewController(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?) {
        
        let controller = AddExserciseRouter.setupModule(with: workout, delegate: delegate)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func gotoDetailExserciseViewController(with detail: WorkoutDetailModel, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen) {
        let controller = DetailExerciseRouter.setupModule(with: detail, exercise: nil, delegate: delegate, state: state)
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
