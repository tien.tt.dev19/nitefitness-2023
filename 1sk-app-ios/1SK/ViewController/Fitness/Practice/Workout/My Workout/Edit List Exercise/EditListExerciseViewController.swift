//
//  
//  EditListExerciseViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//
//

import UIKit

class EditListExerciseViewController: BaseViewController {

    @IBOutlet weak var view_NavigationBar: UIView!
    @IBOutlet weak var lbl_TitleNavigationBar: UILabel!
    @IBOutlet weak var tbv_Exercise: UITableView!
    
    var presenter: EditListExercisePresenterProtocol!
    
    var view_AlertRemove = AlertRemoveExerciseView()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.view_NavigationBar.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.setInitTableView()
    }
    
    // MARK: - Action
    
    @IBAction func onBackNavigationItemAction(_ sender: Any) {
        self.presenter.onDidBackNavigationItemAction()
    }
    
    @IBAction func onAddNavigationItemAction(_ sender: Any) {
        self.presenter.onDidAddExerciseAction()
    }
}

// MARK: - EditListExerciseViewProtocol
extension EditListExerciseViewController: EditListExerciseViewProtocol {
    func onReloadData(workout: WorkoutModel) {
        self.tbv_Exercise.reloadData()
    }
}

// MARK: - Init Alert View
extension EditListExerciseViewController {
    func setShowAlertRemoveView(indexPath: IndexPath) {
        self.view_AlertRemove = AlertRemoveExerciseView.loadFromNib()
        self.view_AlertRemove.tag = 112
        self.view_AlertRemove.indexPath = indexPath
        self.view_AlertRemove.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view_AlertRemove.setPrepareViewForInitView()
        
        self.view_AlertRemove.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchUpBgAlertViewAction)))
        self.view_AlertRemove.btn_Dismiss.addTarget(self, action: #selector(self.onTouchUpBgAlertViewAction), for: .touchUpInside)
        self.view_AlertRemove.btn_ConfirmRemove.addTarget(self, action: #selector(self.onRemoveConfirmViewAction), for: .touchUpInside)
        
        self.view.addSubview(self.view_AlertRemove)
        self.view_AlertRemove.setShowAlertAnimate()
    }
    
    @objc func onTouchUpBgAlertViewAction() {
        self.setRemoveSubviewAlert()
    }
    
    @objc func onDismissViewAction() {
        self.setRemoveSubviewAlert()
    }
    
    @objc func onRemoveConfirmViewAction() {
        self.presenter.onDidRemoveConfirm(at: self.view_AlertRemove.indexPath!)
        self.setRemoveSubviewAlert()
    }
    
    func setRemoveSubviewAlert() {
        if let viewWithTag = self.view.viewWithTag(112) {
            self.view_AlertRemove.setRemoveAlertAnimate(viewWithTag: viewWithTag, tag: 112)
        }
    }
}

// MARK: - UITableViewDataSource
extension EditListExerciseViewController: UITableViewDataSource {
    private func setInitTableView() {
        self.tbv_Exercise.registerNib(ofType: CellTableViewEditListExercise.self)
        
        self.tbv_Exercise.dragInteractionEnabled = true
        self.tbv_Exercise.isEditing = true
        
        self.tbv_Exercise.dataSource = self
        self.tbv_Exercise.delegate = self
        
        self.tbv_Exercise.dragDelegate = self
        self.tbv_Exercise.dropDelegate = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewEditListExercise.self, for: indexPath)
        cell.delegate = self
        cell.indexPath = indexPath
        cell.config(with: self.presenter.itemDataForRow(at: indexPath))
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension EditListExerciseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.presenter.onMoveRowAt(sourceIndexPath: sourceIndexPath, destinationIndexPath: destinationIndexPath)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}

// MARK: - UITableViewDragDelegate
extension EditListExerciseViewController: UITableViewDragDelegate {
    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        return []
    }
    
    func tableView(_ tableView: UITableView, dragSessionWillBegin session: UIDragSession) {
        self.presenter.onDragSessionWillBegin()
    }
    
    func tableView(_ tableView: UITableView, dragSessionDidEnd session: UIDragSession) {
        self.presenter.onDragSessionDidEnd()
    }
}

// MARK: - UITableViewDropDelegate
extension EditListExerciseViewController: UITableViewDropDelegate {
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {
        ///
    }
}

// MARK: - CellTableViewEditListExerciseDelegate
extension EditListExerciseViewController: CellTableViewEditListExerciseDelegate {
    func onDidRemoveExerciseAction(at indexPath: IndexPath) {
        self.setShowAlertRemoveView(indexPath: indexPath)
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        self.presenter.onDidSelectRow(at: indexPath)
    }
}
