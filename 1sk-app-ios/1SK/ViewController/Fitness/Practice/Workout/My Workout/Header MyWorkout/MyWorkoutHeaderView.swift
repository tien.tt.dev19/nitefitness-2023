//
//  MyWorkoutHeaderView.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//

import UIKit

protocol MyWorkoutHeaderViewDelegate: AnyObject {
    func onBackHeaderAction()
    func onShareHeaderAction()
    func onOptionHeaderAction()
    func onAddExerciseHeaderAction()
    func onBookmarkHeaderAction()
    
    func onHiddenBookmarkNav(_ isHidden: Bool)
    func onHiddenButtonMorekNav(_ isHidden: Bool)
    func onSelectedBookmarkNav(_ isSelected: Bool)
}

class MyWorkoutHeaderView: UIView {
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var view_GradientImage: UIView!
    @IBOutlet weak var lbl_NameWorkout: UILabel!
    @IBOutlet weak var img_UserAvatar: UIImageView!
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Level: UILabel!
    @IBOutlet weak var lbl_Type: UILabel!
    @IBOutlet weak var lbl_CountExercise: UILabel!
    @IBOutlet weak var lbl_Tool: UILabel!
    
    @IBOutlet weak var stack_RightNav: UIStackView!
    @IBOutlet weak var btn_Bookmark: UIButton!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var btn_More: UIButton!
    
    @IBOutlet weak var emptyViewHeightConstraint: NSLayoutConstraint!
    
//    let userDAO = GenericDAO<UserObject>()
    
    var estimateHeight: CGFloat {
        let height = systemLayoutSizeFitting(CGSize(width: Constant.Screen.width, height: .infinity), withHorizontalFittingPriority: .required, verticalFittingPriority: .fittingSizeLevel).height
        return height
    }

    weak var delegate: MyWorkoutHeaderViewDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [
            UIColor(red: 0, green: 0.008, blue: 0.025, alpha: 1).cgColor,
            UIColor(red: 0.039, green: 0.052, blue: 0.072, alpha: 0.26).cgColor,
            UIColor(red: 0.062, green: 0.078, blue: 0.099, alpha: 0.19).cgColor,
            UIColor(red: 0.083, green: 0.101, blue: 0.124, alpha: 0.14).cgColor,
            UIColor(red: 0.134, green: 0.158, blue: 0.185, alpha: 0.69).cgColor,
            UIColor(red: 0.137, green: 0.161, blue: 0.188, alpha: 0).cgColor
          ]
        gradientLayer.locations = [0, 0.28, 0.45, 0.61, 0.98, 1]
        gradientLayer.startPoint = CGPoint(x: 1, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.frame = CGRect(origin: .zero, size: CGSize(width: Constant.Screen.width, height: Constant.Screen.width * 219 / 375))
        view_GradientImage.layer.addSublayer(gradientLayer)
    }
    // MARK: - Action
    @IBAction func buttonBackDidTapped(_ sender: Any) {
        delegate?.onBackHeaderAction()
    }

    @IBAction func buttonShareDidTapped(_ sender: Any) {
        delegate?.onShareHeaderAction()
    }

    @IBAction func buttonOptionDidTapped(_ sender: Any) {
        delegate?.onOptionHeaderAction()
    }

    @IBAction func buttonAddExerciseDidTapped(_ sender: Any) {
        delegate?.onAddExerciseHeaderAction()
    }
    
    @IBAction func onBookmarkWorkoutAction(_ sender: Any) {
        if gIsInternetConnectionAvailable {
            self.btn_Bookmark.isSelected = !self.btn_Bookmark.isSelected
        }
        self.delegate?.onBookmarkHeaderAction()
        self.delegate?.onSelectedBookmarkNav(self.btn_Bookmark.isSelected)
    }

    func config(with workout: WorkoutModel) {
        if let url = workout.details?.first?.exercise?.image {
            self.img_Image.setImageWith(imageUrl: url)
            self.img_Image.contentMode = .scaleAspectFit
            self.img_Image.backgroundColor = .white
            self.view_GradientImage.alpha = 0.53
            
        } else {
            self.img_Image.contentMode = .center
            self.img_Image.image = R.image.ic_logo_gray()
            self.view_GradientImage.alpha = 0
        }
        
        if workout.userName == nil || workout.userName == "" {
//            let user = self.userDAO.getFirstObject()
//            self.img_UserAvatar.setImageWith(imageUrl: user?.avatar ?? "", placeHolder: R.image.default_avatar())
//            self.lbl_UserName.text = user?.fullName ?? "n/a"
            
            
            self.img_UserAvatar.setImageWith(imageUrl: gUser?.avatar ?? "", placeHolder: R.image.default_avatar())
            self.lbl_UserName.text = gUser?.fullName ?? "n/a"
            
        } else {
            self.img_UserAvatar.setImageWith(imageUrl: workout.userAvatar ?? "", placeHolder: R.image.default_avatar())
            self.lbl_UserName.text = workout.fullName ?? "n/a"
        }
        
        self.lbl_NameWorkout.text = workout.name
        
        let timeAtributedString = NSMutableAttributedString(string: "Thời gian tập: ", attributes: [.foregroundColor: R.color.subTitle()!])
        let minute = Double(workout.time ?? 0) .getTimeString()
        timeAtributedString.append(NSAttributedString(string: "\(minute)"))
        self.lbl_Time.attributedText = timeAtributedString
        
        let levelAttributedString = NSMutableAttributedString(string: "Cấp độ: ", attributes: [.foregroundColor: R.color.subTitle()!])
        levelAttributedString.append(NSAttributedString(string: workout.level?.name ?? ""))
        self.lbl_Level.attributedText = levelAttributedString

        let typeAttributedString = NSMutableAttributedString(string: "Loại bài tập: ", attributes: [.foregroundColor: R.color.subTitle()!])
        typeAttributedString.append(NSAttributedString(string: workout.type?.name ?? ""))
        self.lbl_Type.attributedText = typeAttributedString
        
        var listTool = [ToolModel]()
        var listToolStr = ""
        guard let details = workout.details else { return }
        for item in details {
            if let excercise = item.exercise {
                if let tools = excercise.tools {
                    for (index, tool) in tools.enumerated() {
                        listTool.append(tool)
                        
                        if index == 0 {
                            listToolStr = tool.name
                        } else {
                            listToolStr += ", \(tool.name)"
                        }
                    }
                }
            }
        }
        let toolAttributedString = NSMutableAttributedString(string: "Dụng cụ: ", attributes: [.foregroundColor: R.color.subTitle()!])
        toolAttributedString.append(NSAttributedString(string: listToolStr))
        self.lbl_Tool.attributedText = toolAttributedString
        
        self.emptyViewHeightConstraint.constant = details.isEmpty ? 107.5 : 0
        self.lbl_CountExercise.text = "Động tác \(details.isEmpty ? "" : " (\(details.count))")"
        
//        let userID = GenericDAO<UserObject>().getFirstObject()?.uuid
        let userID = gUser?.id
        if userID == workout.userId {
            self.btn_Bookmark.isHidden = true
            self.delegate?.onHiddenBookmarkNav(self.btn_Bookmark.isHidden)
            self.btn_More.isHidden = false
            self.delegate?.onHiddenButtonMorekNav(self.btn_More.isHidden)
        } else {
            // TODO: Show bookmark and button more on the next version
            self.btn_Bookmark.isHidden = true
            self.btn_More.isHidden = true
            self.btn_Bookmark.isSelected = workout.isBookmark != 0
            self.delegate?.onHiddenButtonMorekNav(self.btn_More.isHidden)
            self.delegate?.onHiddenBookmarkNav(self.btn_Bookmark.isHidden)
            self.delegate?.onSelectedBookmarkNav(self.btn_Bookmark.isSelected)
        }
    }
}
