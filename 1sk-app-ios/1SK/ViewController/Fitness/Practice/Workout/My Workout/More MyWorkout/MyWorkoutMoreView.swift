//
//  MyWorkoutMoreView.swift
//  1SK
//
//  Created by vuongbachthu on 7/2/21.
//

import UIKit

class MyWorkoutMoreView: UIView {

    @IBOutlet weak var view_BgMore: UIView!
    @IBOutlet weak var view_ContentMore: UIView!
    @IBOutlet weak var btn_EditWorkout: UIButton!
    @IBOutlet weak var btn_EditListExercise: UIButton!
    @IBOutlet weak var btn_DeleteWorkout: UIButton!
    @IBOutlet weak var btn_CloneWorkout: UIButton!
    
    @IBOutlet weak var constraint_height_ContentMore: NSLayoutConstraint!
    
    @IBOutlet weak var view_EditWorkout: UIView!
    @IBOutlet weak var view_ListWorkout: UIView!
    @IBOutlet weak var view_DeleteWorkout: UIView!
    @IBOutlet weak var view_CloneWorkout: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setPrepareForInitView()
    }
    
    func setPrepareForInitView() {
        self.view_BgMore.alpha = 0
        self.view_ContentMore.alpha = 1
        
        self.view_BgMore.isHidden = true
        self.view_ContentMore.isHidden = true
        
        self.constraint_height_ContentMore.constant = 0
    }
    
    func setShowMoreView() {
        UIView.animate(withDuration: 0.3) {
            self.view_BgMore.alpha = 0.7
            self.view_ContentMore.alpha = 1
            
            self.view_BgMore.isHidden = false
            self.view_ContentMore.isHidden = false
            
            self.constraint_height_ContentMore.constant = 111
            self.layoutIfNeeded()
            
        } completion: { _ in
            //
        }
    }
    
    func setHiddenMoreView() {
        UIView.animate(withDuration: 0.5) {
            self.view_BgMore.alpha = 0
            self.view_ContentMore.alpha = 0
            
            self.constraint_height_ContentMore.constant = 0
            self.layoutIfNeeded()
            
        } completion: { _ in
            self.view_BgMore.isHidden = true
            self.view_ContentMore.isHidden = true
        }
    }
}
