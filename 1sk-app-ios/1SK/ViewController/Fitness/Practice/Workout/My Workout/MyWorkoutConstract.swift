//
//  
//  MyWorkoutConstract.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//
//

import UIKit

// MARK: View -
protocol MyWorkoutViewProtocol: AnyObject {
    func onViewDidLoad(workout: WorkoutModel?)
    func onReloadData(workout: WorkoutModel)
    
    func onShowMoreView()
    func onShowUnsupportView()
    func onLoadingIndicator(_ show: Bool)
    func onUpdateValueLoading(value: String)
    func onUnlockStartButton()
}

// MARK: Presenter -
protocol MyWorkoutPresenterProtocol {
    var isDowloadSuccess: Bool { get set }
    var isCancelDowload: Bool { get set }
    
    func onViewDidLoad()
    func onBackAction()
    func onShareAction(_ shareButton: UIButton)
    func onMoreAction()
    func onAddExserciseAction()
    
    func onStartPracticeWorkoutAction()
    func onStartOnlinePracticeWorkoutAction()
    
    func onBookmarkWorkoutAction()
    func countListExercise() -> Int
    func userIdWorkout() -> Int
    func checkDownloadedVideo() -> Bool
    func showToashEror()
    
    // MARK: More Action
    func onEditWorkoutAction()
    func onEditListExerciseAction()
    func onDeleteWorkoutConfirmAction()
    func onCloneWorkoutMoreViewAction()
    
    // MARK: - TableView
    func getHeaderItem() -> WorkoutModel
    func numberOfRow(in section: Int) -> Int
    func itemForRow(at index: IndexPath) -> WorkoutDetailModel?
    func onDidSelectRow(at index: IndexPath)
}

// MARK: Interactor - Input
protocol MyWorkoutInteractorInputProtocol {
    func setDeleteWorkout(workoutId: Int)
    func setBookmarkWorkout(workoutId: Int)
    func setCloneWorkout(workoutId: Int)
    func getMusicExercise()
    func saveCategory(with id: Int)
}

// MARK: Interactor - Output
protocol MyWorkoutInteractorOutputProtocol: AnyObject {
    func onSetDeleteWorkoutFinished(with result: Result<EmptyModel, APIError>)
    func onSetBookmarkWorkoutFinished(with result: Result<WorkoutModel, APIError>)
    func onGetMusicExerciseFinished(with result: Result<[MusicModel], APIError>)
    func onSetCloneFinished(with result: Result<WorkoutModel, APIError>)
    func onSaveCategoryFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: Router -
protocol MyWorkoutRouterProtocol: BaseRouterProtocol {
    func popViewController()
    func gotoAddExserciseViewController(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?)
    func gotoDoExserciseViewController(with workout: WorkoutModel, isPlayOnline: Bool, isSaveCategory: Bool, delegate: DoExercisePresenterDelegate?)
    func gotoDetailExserciseViewController(with detail: WorkoutDetailModel, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen)
    func gotoEditListExerciseViewController(with workout: WorkoutModel, delegate: EditListExercisePresenterDelegate?)
    func gotoEditWorkoutViewController(with state: StateViewScreen, workout: WorkoutModel?, delegate: EditWorkoutPresenterDelegate?)
    func showShareViewController(with shareItem: [Any], sourceView: UIView)
}
