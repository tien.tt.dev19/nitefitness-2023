//
//  
//  MyWorkoutInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//
//

import UIKit

class MyWorkoutInteractorInput {
    weak var output: MyWorkoutInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - MyWorkoutInteractorInputProtocol
extension MyWorkoutInteractorInput: MyWorkoutInteractorInputProtocol {
    func saveCategory(with id: Int) {
        self.exerciseService?.saveCategory(with: id, completion: { [weak self] result in
            self?.output?.onSaveCategoryFinished(with: result.unwrapSuccessModel())
        })
    }

    func setCloneWorkout(workoutId: Int) {
        self.exerciseService?.setCloneWorkout(workoutId: workoutId, completion: { [weak self] result in
            self?.output?.onSetCloneFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setBookmarkWorkout(workoutId: Int) {
        self.exerciseService?.bookmark(workoutId: workoutId, completion: { [weak self] result in
            self?.output?.onSetBookmarkWorkoutFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setDeleteWorkout(workoutId: Int) {
        self.exerciseService?.deleteWorkout(workoutId: workoutId, completion: { [weak self] result in
            self?.output?.onSetDeleteWorkoutFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getMusicExercise() {
        self.exerciseService?.music(completion: { [weak self] result in
            self?.output?.onGetMusicExerciseFinished(with: result.unwrapSuccessModel())
        })
    }
}
