//
//  
//  MyWorkoutPresenter.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//
//

import UIKit
import Alamofire
import FirebaseDynamicLinks

protocol MyWorkoutPresenterDelegate: AnyObject {
    func onDeleteWorkoutSuccess(at workoutId: Int?)
    func onUpdateWorkoutSuccess(at workout: WorkoutModel)
    func onBookmarkWorkoutSuccess(at workout: WorkoutModel)
    func onCloneWorkoutSuccess(at workout: WorkoutModel)
}

class MyWorkoutPresenter {
    weak var view: MyWorkoutViewProtocol?
    private var interactor: MyWorkoutInteractorInputProtocol
    private var router: MyWorkoutRouterProtocol
    weak var delegate: MyWorkoutPresenterDelegate?
    
    var workout: WorkoutModel!
    var music: MusicModel?
    var isChoseCategory: Bool = false
    private var isPlayOnline = false
    private var isDoneDowloading = true
    private var isCanceledDowload = false
    
    var completedUnitCount: Int64 = 0
    var completedUnitTotal: Int64 = 0
    
    var movementSizeTotal: Int = 0
    var movementSizeTotalString: String = "0"
    
    init(interactor: MyWorkoutInteractorInputProtocol,
         router: MyWorkoutRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
}

// MARK: - Download Video
extension MyWorkoutPresenter {
    func checkDownloadedVideo() -> Bool {
        var isSuccess = true
        if let details = self.workout.details {
            for (index, detail) in details.enumerated() {
                if let videoUrl = self.workout.details?[index].exercise?.video, let fileName = videoUrl.split(separator: "/").last {
                    
                    if let urlFile = FileHelper.shared.getUrlFile(workoutId: self.workout.id ?? 0, fileName: String(fileName)) {
                        self.workout.details?[index].exercise?.videoPath = urlFile.path
                        self.workout.details?[index].exercise?.videoName = String(fileName)
                        print("checkDownloadedVideo Downloaded: \(index) - ", urlFile)
                        
                    } else {
                        isSuccess = false
                        
                        let movementSize = detail.exercise?.movementSize ?? 0
                        self.movementSizeTotal += movementSize
                        
                        print("checkDownloadedVideo movementSize:", Units(bytes: Int64(movementSize)).getReadableUnit() )
                    }
                }
            }
        }
        
        self.movementSizeTotalString = Units(bytes: Int64(self.movementSizeTotal)).getUnitCapacity(format: "%.1f")
        
        self.view?.onReloadData(workout: self.workout)
        return isSuccess
    }
    
    func checkDownloadingVideo(index: Int) {
        guard let details = self.workout.details else { return }
        if let videoUrl = details[index].exercise?.video, let fileName = videoUrl.split(separator: "/").last {
            
            if let urlFile = FileHelper.shared.getUrlFile(workoutId: self.workout.id ?? 0, fileName: String(fileName)) {
                self.workout.details?[index].exercise?.videoPath = urlFile.path
                self.workout.details?[index].exercise?.videoName = String(fileName)
                print("checkDownloadingVideo Downloaded: \(index) - ", urlFile)
                
                if index == details.count - 1 {
                    self.onLoadingSuccess()
                } else {
                    self.checkDownloadingVideo(index: index + 1)
                }
            } else {
                self.onDownloadVideo(videoUrl: videoUrl, index: index)
            }
        }
    }
    
    func onDownloadVideo(videoUrl: String, index: Int) {
        FileHelper.shared.onDownloadVideoExercise(urlFile: videoUrl, workoutId: self.workout.id ?? 0) { success, message, error, progress, fileURL in
            // Set ProgressView
            if let value = progress {
                // is dowloading
                self.isDowloadSuccess = false
                self.completedUnitCount = value
                let total = self.completedUnitTotal + value
                let valueSize = Units(bytes: total).getUnitCapacity(format: "%.2f")
                self.view?.onUpdateValueLoading(value: "\(valueSize)/\(self.movementSizeTotalString)")
            }
            
            if success {
                self.workout.details?[index].exercise?.videoPath = fileURL!.path
                let fileName = videoUrl.split(separator: "/").last
                self.workout.details?[index].exercise?.videoName = String(fileName!)

                self.completedUnitTotal += self.completedUnitCount

                print("onDownloadVideoExercise: success: index \(index)", fileURL ?? "nil")

                if let details = self.workout.details, index < details.count - 1 {
                    if self.isCancelDowload {
                        self.isDowloadSuccess = true
                        return
                    }
                    self.checkDownloadingVideo(index: index + 1)
                } else {
                    self.isDowloadSuccess = true
                    if !self.isPlayOnline {
                        self.onLoadingSuccess()
                    }
                }
            } else {
                if error != nil {
                    print("onDownloadVideoExercise: failure: \(error ?? -1) - \(message ?? "nil")")
                }
            }
        }
    }
    
    func onLoadingSuccess() {
        self.view?.onLoadingIndicator(false)
        if self.workout.music == nil {
            self.workout.music = self.music
        }
        if let id = self.workout.id {
            self.router.showHud()
            self.interactor.saveCategory(with: id)
        }
    }
}

// MARK: - Download Audio
extension MyWorkoutPresenter {
    func checkDownloadAudio() {
        let fileName = String(format: "%@-audio.aac", String(self.workout.id ?? -1))
        if self.workout.music == nil {
            if let urlFile = FileHelper.shared.getUrlFile(workoutId: self.workout.id ?? -1, fileName: fileName) {
                self.music = MusicModel()
                self.music?.path = urlFile.path

                print("onDownloadFileMusic Check: path ", self.music?.path ?? "Null")
                //FileHelper.shared.printContentsOfDocumentDirectory(title: "onDownloadFileMusic Check:", path: "/workout/\(self.workout.id)/")

            } else {
                self.router.showHud()
                self.interactor.getMusicExercise()
            }
        } else {
                self.router.showHud()
                self.onDownloadFileWorkoutMusic()
        }
    }
    
    func onDownloadFileMusic() {
       if let url = self.music?.link {
            FileHelper.shared.onDownloadAudioExercise(urlFile: url, workoutId: self.workout.id ?? -1) { success, _, _, _, fileURL in
                // Set ProgressView
                //print("onDownloadFileMusic progress: ", progress ?? 0.0)

                if success {
                    self.music?.path = fileURL!.path

                    print("onDownloadFileMusic: success path: \(self.music?.path ?? "Null")")
                    //FileHelper.shared.printContentsOfDocumentDirectory(title: "onDownloadFileMusic Download:", path: "/workout/\(self.workout.id)/")
                    self.router.hideHud()
                    
                } else {
                    if !gIsInternetConnectionAvailable {
                        self.router.hideHud()
                    }
                }
            }
        }
    }
    
    
    func onDownloadFileWorkoutMusic() {
        if let url = self.workout.music?.link {
            let fileName = String(format: "%@-audio.aac", String(self.workout.id ?? -1))
            if let _ = FileHelper.shared.getUrlFile(workoutId: self.workout.id ?? -1, fileName: fileName) {
                let deleteFile = String(format: "%@/%@/%@", "workout", String(self.workout.id ?? -1), fileName)
                FileHelper.shared.setRemoveVideo(path: deleteFile)
            }
            
            FileHelper.shared.onDownloadAudioExercise(urlFile: url, workoutId: self.workout.id ?? -1) { success, _, _, _, fileURL in
                // Set ProgressView
                //print("onDownloadFileMusic progress: ", progress ?? 0.0)

                if success {
                    self.workout.music?.path = fileURL!.path
                    print("onDownloadFileMusic: success path: \(self.workout.music?.path ?? "Null")")
//                    FileHelper.shared.printContentsOfDocumentDirectory(title: "onDownloadFileMusic Download:", path: "/workout/\(self.workout.id)/"
                    self.router.hideHud()
                } else {
                    if !gIsInternetConnectionAvailable {
                        self.router.hideHud()
                    }
                }
                
            }
        }
    }
}

// MARK: - MyWorkoutPresenterProtocol
extension MyWorkoutPresenter: MyWorkoutPresenterProtocol {
    func showToashEror() {
        self.router.showToast(content: "Đã có lỗi xảy ra")
    }
    
    var isCancelDowload: Bool {
        get {
            return self.isCanceledDowload
        }
        set {
            self.isCanceledDowload = newValue
        }
    }
    
    var isDowloadSuccess: Bool {
        get {
            return self.isDoneDowloading
        }
        set {
            self.isDoneDowloading = newValue
        }
    }
    
    func onStartOnlinePracticeWorkoutAction() {
        self.isPlayOnline = true
        if self.workout.music == nil {
            self.workout.music = self.music
        }
        if let id = self.workout.id {
            self.router.showHud()
            self.interactor.saveCategory(with: id)
        }
    }
    
    func onViewDidLoad() {
        self.view?.onViewDidLoad(workout: self.workout)
        self.checkDownloadAudio()
    }

    // MARK: - Action View
    func onBackAction() {
        self.router.popViewController()
    }

    func onShareAction(_ shareButton: UIButton) {
        guard let workoutId = self.workout.id else {
            SKToast.shared.showToast(content: "Không thể chia sẻ bài tập này")
            return
        }
        
        self.router.showHud()
        let meta = MetaSocialShare()
        meta.socialMetaTitle = self.workout.name
        meta.socialMetaDescText = self.workout.fullName
        meta.socialMetaImageURL = self.workout.details?.first?.exercise?.image
        
//        let linkShare = String(format: ShareLinkUrl.fitnessWorkout.rawValue, "\(workoutId)")
        
//        if let linkBuilder = DynamicLinksManager.shared.createDynamicLinksShare(meta: meta, linkShare: linkShare) {
//            linkBuilder.shorten(completion: { url, warnings, error in
//                self.router.hideHud()
//
//                if let listWarning = warnings {
//                    for (index, warning) in listWarning.enumerated() {
//                        print("createLinkShareWorkout warning: \(index) - \(warning)")
//                    }
//                }
//
//                if let err = error {
//                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
//                    print("createLinkShareWorkout error: \(err.localizedDescription)")
//                    return
//                }
//
//                guard let urlShare = url else {
//                    SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
//                    print("createLinkShareWorkout url: nil")
//                    return
//                }
//
//                self.router.showShareViewController(with: [urlShare], sourceView: shareButton)
//
//                print("createLinkShareWorkout The short URL is: \(urlShare)")
//            })
//
//        } else {
//            self.router.hideHud()
//            SKToast.shared.showToast(content: "Chức năng chưa hoạt động")
//        }
    }

    func onMoreAction() {
        self.view?.onShowMoreView()
    }

    func onAddExserciseAction() {
        router.gotoAddExserciseViewController(with: self.workout, delegate: self)
    }

    func onBookmarkWorkoutAction() {
        self.router.showHud()
        self.interactor.setBookmarkWorkout(workoutId: self.workout.id ?? -1)
    }

    func onStartPracticeWorkoutAction() {
        self.isPlayOnline = false
        self.router.showHud()
        if self.workout.music == nil {
            self.workout.music = self.music
        }
        
        if gIsInternetConnectionAvailable {
            if let id = self.workout.id {
                self.interactor.saveCategory(with: id)
            }
        } else {
            self.router.hideHud()
            if checkDownloadedVideo() {
                self.router.gotoDoExserciseViewController(with: self.workout, isPlayOnline: false, isSaveCategory: self.isChoseCategory, delegate: self)
                self.view?.onUnlockStartButton()
            } else {
                self.router.showToast(content: "Đã có lỗi xảy ra")
            }
        }
    }

    func countListExercise() -> Int {
        return self.workout.details?.count ?? 0
    }

    func userIdWorkout() -> Int {
        return self.workout.userId ?? -1
    }

    // MARK: More Action
    func onEditWorkoutAction() {
        self.router.gotoEditWorkoutViewController(with: .edit, workout: self.workout, delegate: self)
    }

    func onEditListExerciseAction() {
        self.router.gotoEditListExerciseViewController(with: self.workout, delegate: self)
    }

    func onDeleteWorkoutConfirmAction() {
        self.router.showHud()
        self.interactor.setDeleteWorkout(workoutId: self.workout.id ?? -1)
    }

    func onCloneWorkoutMoreViewAction() {
        self.router.showHud()
        self.interactor.setCloneWorkout(workoutId: self.workout.id ?? -1)
    }
    
    // MARK: - Table View
    func getHeaderItem() -> WorkoutModel {
        return self.workout
    }

    func numberOfRow(in section: Int) -> Int {
        return workout.details?.count ?? 0
    }

    func itemForRow(at index: IndexPath) -> WorkoutDetailModel? {
        let detail = self.workout.details?[index.row]
        detail?.subject = self.workout.subject
        detail?.level = self.workout.level
        detail?.type = self.workout.type
        
        return detail
    }

    func onDidSelectRow(at index: IndexPath) {
        if let details = self.workout.details {
            if !details.isEmpty {
                let detail = details[index.row]
                self.router.gotoDetailExserciseViewController(with: detail, delegate: nil, state: .view)
            }
        }
    }
    
    
}

// MARK: - MyWorkoutInteractorOutput
extension MyWorkoutPresenter: MyWorkoutInteractorOutputProtocol {
    func onSaveCategoryFinished(with result: Result<EmptyModel, APIError>) {
        self.router.hideHud()
        switch result {
        case .success:
            if isPlayOnline {
                self.router.gotoDoExserciseViewController(with: self.workout, isPlayOnline: true, isSaveCategory: self.isChoseCategory, delegate: self)
                self.view?.onUnlockStartButton()
            } else {
                if self.checkDownloadedVideo() {
                    self.router.gotoDoExserciseViewController(with: self.workout, isPlayOnline: false, isSaveCategory: self.isChoseCategory, delegate: self)
                    self.view?.onUnlockStartButton()
                } else {
                    self.view?.onLoadingIndicator(true)
                    self.checkDownloadingVideo(index: 0)
                }
            }
        case .failure:
            self.router.showToast(content: "Đã có lỗi xảy ra")
        }
    }
    
    func onSetDeleteWorkoutFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            FileHelper.shared.setRemoveDirectory(path: "/workout/\(self.workout.id ?? -1)/")
            
            self.delegate?.onDeleteWorkoutSuccess(at: self.workout.id)
            self.router.showToast(content: "Đã xóa xong bài tập")
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.router.popViewController()
            }
            
        case .failure:
            self.router.showToast(content: "Đã có lỗi xảy ra")
        }
        self.router.hideHud()
    }
    
    func onSetBookmarkWorkoutFinished(with result: Result<WorkoutModel, APIError>) {
        switch result {
        case .success(let workoutModel):
            if workoutModel.isBookmark == 0 {
                SKToast.shared.showToast(content: "Đã xóa khỏi danh sách bài tập của tôi")
            } else {
                SKToast.shared.showToast(content: "Đã lưu vào danh sách bài tập của tôi")
            }
            
            self.workout.isBookmark = workoutModel.isBookmark
            self.view?.onReloadData(workout: self.workout)
            
            let workoutBookmark = workoutModel
            workoutBookmark.userName = self.workout.userName
            workoutBookmark.userAvatar = self.workout.userAvatar
            
            self.delegate?.onBookmarkWorkoutSuccess(at: workoutBookmark)
            
        case .failure:
            self.router.showToast(content: "Đã có lỗi xảy ra")
        }
        self.router.hideHud()
    }
    
    func onGetMusicExerciseFinished(with result: Result<[MusicModel], APIError>) {
        switch result {
        case .success(let listModel):
            if let music = listModel.randomElement() {
                self.music = music
                self.onDownloadFileMusic()
            }
            
        case .failure:
            self.router.hideHud()
            SKToast.shared.showToast(content: "Không thể tải nhạc")
        }
    }
    
    func onSetCloneFinished(with result: Result<WorkoutModel, APIError>) {
        switch result {
        case .success(let workoutModel):
            self.workout = workoutModel
            self.onViewDidLoad()
            self.delegate?.onCloneWorkoutSuccess(at: workoutModel)
            
            self.onEditWorkoutAction()
            
        case .failure:
            SKToast.shared.showToast(content: "Không thể lưu bài tập này")
        }
        
        self.router.hideHud()
    }
}

// MARK: - DoExercisePresenterDelegate
extension MyWorkoutPresenter: DoExercisePresenterDelegate {
    func onActionDoExercise() {
        print("onActionDoExercise")
    }
}

// MARK: - AddExsercisePresenterDelegate
extension MyWorkoutPresenter: AddExsercisePresenterDelegate {
    func onDidAddExercise(workout: WorkoutModel) {
        self.workout = workout
        self.view?.onReloadData(workout: self.workout)
        self.delegate?.onUpdateWorkoutSuccess(at: self.workout)
    }
    
    func onDidAddExercise(details: [WorkoutDetailModel]) {
        
    }
}

// MARK: - EditWorkoutPresenterDelegate
extension MyWorkoutPresenter: EditWorkoutPresenterDelegate {
    func onDidSaveWorkoutSuccess(workout: WorkoutModel) {
        self.workout = workout
        self.view?.onReloadData(workout: self.workout)
        self.router.showToast(content: "Đã cập nhật tin bài tập")
        self.delegate?.onUpdateWorkoutSuccess(at: self.workout)
    }
}

// MARK: - EditListExercisePresenterDelegate
extension MyWorkoutPresenter: EditListExercisePresenterDelegate {
    func onDidEditListExerciseAction(workout: WorkoutModel) {
        self.workout = workout
        self.view?.onReloadData(workout: self.workout)
        self.delegate?.onUpdateWorkoutSuccess(at: self.workout)
    }
}
