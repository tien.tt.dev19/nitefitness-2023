//
//  
//  MyWorkoutRouter.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//
//

import UIKit

class MyWorkoutRouter: BaseRouter {
    static func setupModule(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?, isChoseCategory: Bool = false) -> MyWorkoutViewController {
        let viewController = MyWorkoutViewController()
        viewController.hidesBottomBarWhenPushed = true
        let router = MyWorkoutRouter()
        let interactorInput = MyWorkoutInteractorInput()
        let presenter = MyWorkoutPresenter(interactor: interactorInput, router: router)
        presenter.isChoseCategory = isChoseCategory
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.workout = workout
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - MyWorkoutRouterProtocol
extension MyWorkoutRouter: MyWorkoutRouterProtocol {
    
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }

    func gotoAddExserciseViewController(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?) {
        
        let controller = AddExserciseRouter.setupModule(with: workout, delegate: delegate)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func gotoDetailExserciseViewController(with detail: WorkoutDetailModel, delegate: DetailExercisePresenterDelegate?, state: StateViewScreen) {
        let controller = DetailExerciseRouter.setupModule(with: detail, exercise: nil, delegate: delegate, state: state)
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func gotoEditListExerciseViewController(with workout: WorkoutModel, delegate: EditListExercisePresenterDelegate?) {
        let controller = EditListExerciseRouter.setupModule(with: workout, delegate: delegate)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoEditWorkoutViewController(with state: StateViewScreen, workout: WorkoutModel?, delegate: EditWorkoutPresenterDelegate?) {
        let controller = EditWorkoutRouter.setupModule(with: state, workout: workout, delegate: delegate)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }

    func gotoDoExserciseViewController(with workout: WorkoutModel, isPlayOnline: Bool, isSaveCategory: Bool, delegate: DoExercisePresenterDelegate?) {
        let controller = DoExerciseRouter.setupModule(with: workout, isPlayOnline: isPlayOnline, isSaveCategory: isSaveCategory, delegate: delegate)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showShareViewController(with shareItem: [Any], sourceView: UIView) {
        let shareVC = UIActivityViewController(activityItems: shareItem, applicationActivities: nil)
        shareVC.popoverPresentationController?.sourceView = sourceView
        shareVC.popoverPresentationController?.sourceRect = sourceView.bounds
        self.viewController?.present(shareVC, animated: true, completion: nil)
    }
}
