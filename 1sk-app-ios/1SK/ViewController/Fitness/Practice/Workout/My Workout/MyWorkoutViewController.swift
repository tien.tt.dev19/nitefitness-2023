//
//  
//  MyWorkoutViewController.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//
//

import UIKit

class MyWorkoutViewController: BaseViewController {

    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var lbl_TitleNav: UILabel!
    @IBOutlet weak var btn_Bookmark: UIButton!
    @IBOutlet weak var btn_Share: UIButton!
    @IBOutlet weak var btn_More: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var view_Footer: UIView!
    
    @IBOutlet weak var onlinePracticeButton: UIButtonRoundBlue!
    @IBOutlet weak var btn_StartPractice: UIButton!
    
    @IBOutlet weak var view_Loading: UIView!
    @IBOutlet weak var indicator_Loadding: UIActivityIndicatorView!
    @IBOutlet weak var lbl_LoadingValue: UILabel!
    
    var view_AlertCancel = AlertConfirmCancelPlayView()
    let alertView = UIAlertController(title: "Thông báo", message: "Tiến trình tải sẽ bị dừng khi bạn chuyển qua tập online", preferredStyle: .alert)
    
    var presenter: MyWorkoutPresenterProtocol!
    
    var view_MoreView = MyWorkoutMoreView()
    var view_HeaderView = MyWorkoutHeaderView()
    var view_AlertDelete = AlertRemoveExerciseView()
    
    private lazy var headerView = self.setInitHeaderView()
    private var isTapStartButton = true
    
    private var isCheckNavBar = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.onlinePracticeButton.isHidden = self.presenter.checkDownloadedVideo()
        let title = self.presenter.checkDownloadedVideo() ? "Bắt đầu tập" : "Tải xuống"
        self.btn_StartPractice.setTitle(title, for: .normal)
        self.btn_StartPractice.backgroundColor = self.presenter.checkDownloadedVideo() ? R.color.mainColor() : UIColor(hex: "ECF0F3")

        let titleColor: UIColor = self.presenter.checkDownloadedVideo() ? .white : .black
        self.btn_StartPractice.setTitleColor(titleColor, for: .normal)
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitTableView()
        self.setInitNavigationBar()
    }
    
    func setInitNavigationBar() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.view_Nav.isHidden = true
        
        self.view_Footer.addShadow(width: 0, height: -4, color: .black, radius: 4, opacity: 0.1)
    }
    
    func setStateNavigationBar(contentOffset: CGFloat) {
        if self.isCheckNavBar {
            if contentOffset <= 0.0 {
                self.isCheckNavBar = false
                if self.view_Nav.isHidden == false {
                    UIView.animate(withDuration: 0.5) {
                        self.view_Nav.alpha = 0
                        self.view.layoutIfNeeded()
                        
                    } completion: { _ in
                        self.view_Nav.isHidden = true
                        self.isCheckNavBar = true
                    }
                } else {
                    self.isCheckNavBar = true
                }
            }
            
            if contentOffset > 44.0 {
                self.isCheckNavBar = false
                if self.view_Nav.isHidden == true {
                    
                    self.view_Nav.isHidden = false
                    self.view_Nav.alpha = 0
                    
                    UIView.animate(withDuration: 0.5) {
                        self.view_Nav.alpha = 1
                        self.view.layoutIfNeeded()
                        
                    } completion: { _ in
                        self.isCheckNavBar = true
                    }
                } else {
                    self.isCheckNavBar = true
                }
            }
        }
    }
    
    // MARK: - Action
    @IBAction func onStartOnlinePractice(_ sender: Any) {
        if !self.presenter.isDowloadSuccess {
            self.setShowAlertCancelView()
        } else {
            self.presenter.onStartOnlinePracticeWorkoutAction()
        }
    }
    
    func setShowAlertBackView() {
        let alertView = UIAlertController(title: "Thông báo", message: "Bạn có muốn dừng tiến trình tải lại?", preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel) { _ in
            self.alertView.dismiss(animated: true, completion: nil)
        }
        
        let agreeAction = UIAlertAction(title: "Đồng ý", style: .default) { _ in
            self.presenter.isCancelDowload = true
            self.view_Loading.isHidden = true
            self.btn_StartPractice.isHidden = false
            self.presenter.onBackAction()
        }
        
        if alertView.actions.isEmpty {
            alertView.addAction(cancelAction)
            alertView.addAction(agreeAction)
        }
        self.present(alertView, animated: true, completion: nil)
    }

    func setShowAlertCancelView() {
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel) { _ in
            self.alertView.dismiss(animated: true, completion: nil)
        }
        
        let agreeAction = UIAlertAction(title: "Đồng ý", style: .default) { _ in
            self.presenter.isCancelDowload = true
            self.view_Loading.isHidden = true
            self.btn_StartPractice.isHidden = false
            self.presenter.onStartOnlinePracticeWorkoutAction()
        }
        
        if alertView.actions.isEmpty {
            alertView.addAction(cancelAction)
            alertView.addAction(agreeAction)
        }
        self.present(alertView, animated: true, completion: nil)
    }
    
    @objc func touchUpBgAlertViewAction() {
        self.setRemoveSubviewAlert()
    }
    
    @objc func dismissViewAction() {
        ///
//        self.presenter.onBackItemNavigationAction()
    }
    
    @objc func onContinueViewAction() {
//        self.setRemoveSubviewAlert()
    }
    
    @IBAction func onStartPracticeWorkoutAction(_ sender: Any) {
        if gIsInternetConnectionAvailable {
            if self.isTapStartButton {
                self.isTapStartButton = false
                self.presenter.onStartPracticeWorkoutAction()
            } else {
                print("onStartPracticeWorkoutAction")
            }
        } else {
            self.presenter.showToashEror()
        }
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        self.onBackHeaderAction()
    }
    
    @IBAction func onBookmarkAction(_ sender: Any) {
        self.btn_Bookmark.isSelected = !self.btn_Bookmark.isSelected
        self.onBookmarkHeaderAction()
    }
    
    @IBAction func onShareAction(_ sender: Any) {
        self.onShareHeaderAction()
    }
    
    @IBAction func onMoreAction(_ sender: Any) {
        self.onOptionHeaderAction()
    }
}

// MARK: - MyWorkoutViewProtocol
extension MyWorkoutViewController: MyWorkoutViewProtocol {
    func onShowUnsupportView() {
        let message = "Tính năng này chỉ hỗ trợ iOS 13.0 trở lên."
        let alertView = UIAlertController(title: "Thông báo", message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel) { _ in
        }
        
        if alertView.actions.isEmpty {
            alertView.addAction(cancelAction)
        }
        self.present(alertView, animated: true, completion: nil)
    }
    
    func onViewDidLoad(workout: WorkoutModel?) {
        self.lbl_TitleNav.text = workout?.name
        
        self.headerView.config(with: self.presenter.getHeaderItem())
        self.headerView.frame = CGRect(origin: .zero, size: CGSize(width: Constant.Screen.width, height: self.headerView.estimateHeight))
        self.tableView.reloadData()
        
        if  workout?.details?.count ?? 0 > 0 {
            self.view_Footer.isHidden = false
        } else {
            self.view_Footer.isHidden = true
        }
        
    }
    
    func onReloadData(workout: WorkoutModel) {
        self.headerView.config(with: self.presenter.getHeaderItem())
        self.headerView.frame = CGRect(origin: .zero, size: CGSize(width: Constant.Screen.width, height: self.headerView.estimateHeight))
        self.tableView.reloadData()
        
        if  workout.details?.count ?? 0 > 0 {
            self.view_Footer.isHidden = false

        } else {
            self.view_Footer.isHidden = true
        }
    }
    
    func onShowMoreView() {
        self.setInitAddMoreView()
    }
    
    func onLoadingIndicator(_ show: Bool) {
        if show {
            self.btn_StartPractice.isHidden = true
            self.view_Loading.isHidden = false
            self.indicator_Loadding.startAnimating()
        } else {
            self.btn_StartPractice.isHidden = false
            self.view_Loading.isHidden = true
            self.indicator_Loadding.stopAnimating()
        }
    }
    
    func onUpdateValueLoading(value: String) {
        self.lbl_LoadingValue.text = value
    }
    
    func onUnlockStartButton() {
        self.alertView.dismiss(animated: true)
        self.isTapStartButton = true
    }
}

// MARK: - Init More View
extension MyWorkoutViewController {
    func setInitAddMoreView() {
        self.view_MoreView = MyWorkoutMoreView.loadFromNib()
        self.view_MoreView.tag = 111
        self.view_MoreView.frame = CGRect(x: 0, y: 0, width: self.view.width, height: self.view.height)
        self.view_MoreView.view_BgMore.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchUpBgMoreViewAction)))
        
        if  self.presenter.countListExercise() > 0 {
            self.view_MoreView.view_ListWorkout.isHidden = false
        } else {
            self.view_MoreView.view_ListWorkout.isHidden = true
        }
        
//        let userID = GenericDAO<UserObject>().getFirstObject()?.uuid
        let userID = gUser?.id
        if userID == self.presenter.userIdWorkout() {
            self.view_MoreView.view_EditWorkout.isHidden = false
            self.view_MoreView.view_ListWorkout.isHidden = false
            self.view_MoreView.view_DeleteWorkout.isHidden = false
            self.view_MoreView.view_CloneWorkout.isHidden = true
            
        } else {
            self.view_MoreView.view_EditWorkout.isHidden = true
            self.view_MoreView.view_ListWorkout.isHidden = true
            self.view_MoreView.view_DeleteWorkout.isHidden = true
            self.view_MoreView.view_CloneWorkout.isHidden = false
        }

        self.view_MoreView.btn_EditWorkout.addTarget(self, action: #selector(self.onEditWorkoutMoreViewAction), for: .touchUpInside)
        self.view_MoreView.btn_EditListExercise.addTarget(self, action: #selector(self.onEditListExerciseMoreViewAction), for: .touchUpInside)
        self.view_MoreView.btn_DeleteWorkout.addTarget(self, action: #selector(self.onDeleteWorkoutMoreViewAction), for: .touchUpInside)
        self.view_MoreView.btn_CloneWorkout.addTarget(self, action: #selector(self.onCloneWorkoutMoreViewAction), for: .touchUpInside)

        self.view.addSubview(self.view_MoreView)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.view_MoreView.setShowMoreView()
        }
    }

    @objc func onTouchUpBgMoreViewAction() {
        self.setRemoveSubviewMore()
    }

    @objc func onEditWorkoutMoreViewAction() {
        self.presenter.onEditWorkoutAction()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setRemoveSubviewMore()
        }
    }

    @objc func onEditListExerciseMoreViewAction() {
        self.presenter.onEditListExerciseAction()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setRemoveSubviewMore()
        }
    }

    @objc func onDeleteWorkoutMoreViewAction() {
        self.setRemoveSubviewMore()
        self.setShowAlertDeleteView()
    }

    @objc func onCloneWorkoutMoreViewAction() {
        self.setRemoveSubviewMore()
        self.presenter.onCloneWorkoutMoreViewAction()
    }

    func setRemoveSubviewMore() {
        if let viewWithTag = self.view.viewWithTag(111) {
            self.view_MoreView.setHiddenMoreView()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                viewWithTag.removeFromSuperview()
            }
        }
    }
}

// MARK: - Init Alert View
extension MyWorkoutViewController {
    func setShowAlertDeleteView() {
        self.view_AlertDelete = AlertRemoveExerciseView.loadFromNib()
        self.view_AlertDelete.tag = 112
        self.view_AlertDelete.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view_AlertDelete.setPrepareViewForInitView()
        
        self.view_AlertDelete.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchUpBgAlertViewAction)))
        self.view_AlertDelete.btn_Dismiss.addTarget(self, action: #selector(self.onTouchUpBgAlertViewAction), for: .touchUpInside)
        self.view_AlertDelete.btn_ConfirmRemove.addTarget(self, action: #selector(self.onDeleteWorkoutConfirmAction), for: .touchUpInside)
        
        self.view.addSubview(self.view_AlertDelete)
        self.view_AlertDelete.setShowAlertAnimate()
    }
    
    @objc func onTouchUpBgAlertViewAction() {
        self.setRemoveSubviewAlert()
    }
    
    @objc func onDismissViewAction() {
        self.setRemoveSubviewAlert()
    }
    
    @objc func onDeleteWorkoutConfirmAction() {
        self.presenter.onDeleteWorkoutConfirmAction()
        self.setRemoveSubviewAlert()
    }
    
    func setRemoveSubviewAlert() {
        if let viewWithTag = self.view.viewWithTag(112) {
            self.view_AlertDelete.setRemoveAlertAnimate(viewWithTag: viewWithTag, tag: 112)
        }
    }
}

// MARK: - MyWorkoutHeaderViewDelegate
extension MyWorkoutViewController: MyWorkoutHeaderViewDelegate {
    private func setInitHeaderView() -> MyWorkoutHeaderView {
        self.view_HeaderView = MyWorkoutHeaderView.loadFromNib()
        self.view_HeaderView.translatesAutoresizingMaskIntoConstraints = true
        self.view_HeaderView.autoresizingMask = .flexibleWidth
        self.view_HeaderView.frame = .zero
        self.view_HeaderView.delegate = self
        return self.view_HeaderView
    }
    
    func onAddExerciseHeaderAction() {
        self.presenter.onAddExserciseAction()
    }

    func onBackHeaderAction() {
        if self.presenter.checkDownloadedVideo() {
            self.presenter.onBackAction()
        } else {
            if !self.presenter.isDowloadSuccess {
                self.setShowAlertBackView()
            } else {
                self.presenter.onBackAction()
            }
        }
    }

    func onShareHeaderAction() {
        self.presenter.onShareAction(self.btn_Share)
    }

    func onOptionHeaderAction() {
        self.presenter.onMoreAction()
    }
    
    func onBookmarkHeaderAction() {
        self.presenter.onBookmarkWorkoutAction()
    }
    
    func onHiddenBookmarkNav(_ isHidden: Bool) {
        self.btn_Bookmark.isHidden = isHidden
    }
    
    func onSelectedBookmarkNav(_ isSelected: Bool) {
        self.btn_Bookmark.isSelected = isSelected
    }

    func onHiddenButtonMorekNav(_ isHidden: Bool) {
        self.btn_More.isHidden = isHidden
    }
}

// MARK: - UITableViewDataSource
extension MyWorkoutViewController: UITableViewDataSource {
    private func setInitTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.registerNib(ofType: CellTableViewExercise.self)
        self.tableView.tableHeaderView = self.headerView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRow(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewExercise.self, for: indexPath)
        let workoutDetail = self.presenter.itemForRow(at: indexPath)
        cell.config(with: workoutDetail)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MyWorkoutViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRow(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        self.setStateNavigationBar(contentOffset: contentOffset)
    }
}
