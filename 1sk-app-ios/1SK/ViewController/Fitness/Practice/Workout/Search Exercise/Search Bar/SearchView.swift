//
//  SearchView.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//

import UIKit

class SearchView: UIView {

    @IBOutlet weak var view_BgContent: UIView!
    @IBOutlet weak var tf_Search: UITextField!
    @IBOutlet weak var btn_ClearText: UIButton!
    
}
