//
//  
//  SearchExerciseConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//
//

import UIKit

// MARK: - View
protocol SearchExerciseViewProtocol: AnyObject {
    // MARK: - TableView
    func onReloadDataTable()
    func onSetNotFoundView(isHidden: Bool)
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
}

// MARK: - Presenter
protocol SearchExercisePresenterProtocol {
    func onViewDidLoad()
    
    // MARK: - UITableView
    func numberOfItemsInSectionTable(in section: Int) -> Int
    func onCellForItemTable(at index: IndexPath) -> WorkoutModel
    func onDidSelectRowAtTable(at index: IndexPath)
    func onLoadMoreAction()
    
    // MARK: - UITextField
    func onTextFieldShouldReturn(keyword: String)
}

// MARK: - Interactor Input
protocol SearchExerciseInteractorInputProtocol {
    func getWorkoutLibrary(filter: Filter)
}

// MARK: - Interactor Output
protocol SearchExerciseInteractorOutputProtocol: AnyObject {
    func onGetWorkoutLibraryFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int)
}

// MARK: - Router
protocol SearchExerciseRouterProtocol: BaseRouterProtocol {
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?)
}
