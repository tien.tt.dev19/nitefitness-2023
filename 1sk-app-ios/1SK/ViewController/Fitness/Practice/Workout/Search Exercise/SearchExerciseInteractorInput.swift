//
//  
//  SearchExerciseInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//
//

import UIKit

class SearchExerciseInteractorInput {
    weak var output: SearchExerciseInteractorOutputProtocol?
    var exserciseService: ExerciseServiceProtocol?
}

// MARK: - SearchExerciseInteractorInputProtocol
extension SearchExerciseInteractorInput: SearchExerciseInteractorInputProtocol {
    func getWorkoutLibrary(filter: Filter) {
        self.exserciseService?.getListWorkoutLibrary(filter: filter, completion: { [weak self] result in
            self?.output?.onGetWorkoutLibraryFinished(with: result.unwrapSuccessModel(), page: filter.page!, total: result.getTotal() ?? 0)
        })
    }
}
