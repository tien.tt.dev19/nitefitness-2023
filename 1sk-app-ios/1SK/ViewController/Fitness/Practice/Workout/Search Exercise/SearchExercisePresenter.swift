//
//  
//  SearchExercisePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//
//

import UIKit

protocol SearchExercisePresenterDelegate: AnyObject {
    func onBookmarkWorkoutSearchSuccess(workout: WorkoutModel)
    func onCloneWorkoutSearchSuccess(at workout: WorkoutModel)
}

class SearchExercisePresenter {

    weak var view: SearchExerciseViewProtocol?
    private var interactor: SearchExerciseInteractorInputProtocol
    private var router: SearchExerciseRouterProtocol

    weak var delegate: SearchExercisePresenterDelegate?
    
    private var listWorkoutLibrary: [WorkoutModel] = []
    private var filter = Filter()
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    init(interactor: SearchExerciseInteractorInputProtocol,
         router: SearchExerciseRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    private func getData(in page: Int) {
        self.filter.page = page
        self.filter.limit = self.limit
        self.filter.sort = "DESC"
        self.filter.order = "created_at"
        
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
            
        }
        
        self.router.showHud()
        self.interactor.getWorkoutLibrary(filter: self.filter)
    }
}

// MARK: - SearchExercisePresenterProtocol
extension SearchExercisePresenter: SearchExercisePresenterProtocol {
    func onViewDidLoad() {
        self.getData(in: 1)
    }
    
    // MARK: - TableView
    func numberOfItemsInSectionTable(in section: Int) -> Int {
        return self.listWorkoutLibrary.count
    }
    
    func onCellForItemTable(at index: IndexPath) -> WorkoutModel {
        return self.listWorkoutLibrary[index.row]
    }
    
    func onDidSelectRowAtTable(at index: IndexPath) {
        self.router.gotoDetailWorkoutViewController(with: self.listWorkoutLibrary[index.row], delegate: self)
    }
    
    func onLoadMoreAction() {
        if self.listWorkoutLibrary.count >= self.limit && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    // MARK: - UITextField
    func onTextFieldShouldReturn(keyword: String) {
        self.filter.keyword = keyword
        self.getData(in: 1)
    }
}

// MARK: - SearchExerciseInteractorOutput 
extension SearchExercisePresenter: SearchExerciseInteractorOutputProtocol {
    func onGetWorkoutLibraryFinished(with result: Result<[WorkoutModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listWorkout):
            if page <= 1 {
                self.listWorkoutLibrary = listWorkout
                self.view?.onReloadDataTable()
                
            } else {
                for object in listWorkout {
                    self.listWorkoutLibrary.append(object)
                    self.view?.onInsertRow(at: self.listWorkoutLibrary.count - 1)
                }
            }
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listWorkoutLibrary.count >= self.total {
                self.isOutOfData = true
            }
            
            self.router.hideErrorView()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure(let error):
            print("onGetWorkoutLibraryFinished error", error.description)
            self.router.showErrorView(with: nil, delegate: self)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.checkDataNotFound()
        self.router.hideHud()
    }
    
    func checkDataNotFound() {
        // Set Status View Data Not Found
        if self.listWorkoutLibrary.count > 0 {
            self.view?.onSetNotFoundView(isHidden: true)
        } else {
            self.view?.onSetNotFoundView(isHidden: false)
        }
    }
}

// MARK: - ErrorViewDelegate
extension SearchExercisePresenter: ErrorViewDelegate {
    func onRetryButtonDidTapped(_ errorView: UIView) {
        print("onRetryButtonDidTapped")
    }
}

// MARK: - MyWorkoutPresenterDelegate
extension SearchExercisePresenter: MyWorkoutPresenterDelegate {
    
    func onBookmarkWorkoutSuccess(at workout: WorkoutModel) {
        for (index, _workout) in self.listWorkoutLibrary.enumerated() {
            if _workout.id == workout.id {
                self.listWorkoutLibrary[index] = workout
                break
            }
        }
        self.view?.onReloadDataTable()
        self.delegate?.onBookmarkWorkoutSearchSuccess(workout: workout)
    }
    
    func onDeleteWorkoutSuccess(at workoutId: Int?) {
        print("onDeleteWorkoutSuccess")
    }
    
    func onUpdateWorkoutSuccess(at workout: WorkoutModel) {
        // Did Edit Workout when clone
        self.delegate?.onCloneWorkoutSearchSuccess(at: workout)
    }
    
    func onCloneWorkoutSuccess(at workout: WorkoutModel) {
        self.delegate?.onCloneWorkoutSearchSuccess(at: workout)
    }
}
