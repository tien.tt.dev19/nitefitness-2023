//
//  
//  SearchExerciseRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//
//

import UIKit

class SearchExerciseRouter: BaseRouter {
//    weak var viewController: SearchExerciseViewController?
    static func setupModule(with delegate: SearchExercisePresenterDelegate?) -> SearchExerciseViewController {
        let viewController = SearchExerciseViewController()
        let router = SearchExerciseRouter()
        let interactorInput = SearchExerciseInteractorInput()
        let presenter = SearchExercisePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        interactorInput.output = presenter
        interactorInput.exserciseService = ExerciseService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SearchExerciseRouterProtocol
extension SearchExerciseRouter: SearchExerciseRouterProtocol {
    func gotoDetailWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?) {
        let workoutViewController = MyWorkoutRouter.setupModule(with: workout, delegate: delegate)
        self.viewController?.navigationController?.pushViewController(workoutViewController, animated: true)
    }
}
