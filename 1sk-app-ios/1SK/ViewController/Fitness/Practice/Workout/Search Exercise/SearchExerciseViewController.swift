//
//  
//  SearchExerciseViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/1/21.
//
//

import UIKit

class SearchExerciseViewController: BaseViewController {

    @IBOutlet weak var tbv_Exercise: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    
    let view_SearchView = SearchView.loadFromNib()
    
    var presenter: SearchExercisePresenterProtocol!
    
    private var isKeyboadShow = false
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.view_SearchView.tf_Search.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view_SearchView.tf_Search.resignFirstResponder()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitSearchView()
        self.setInitTableView()
        self.setInitHandleKeyboard()
    }
    
    
    // MARK: - Action
    
}

// MARK: - UITextFieldDelegate
extension SearchExerciseViewController: UITextFieldDelegate {
    private func setInitSearchView() {
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.width, height: 32))
        
        self.view_SearchView.frame = CGRect(x: 0, y: 0, width: titleView.width, height: titleView.height)
        self.view_SearchView.view_BgContent.cornerRadius = self.view_SearchView.height / 2
        self.view_SearchView.btn_ClearText.addTarget(self, action: #selector(self.onClearTextSearchAction), for: .touchUpInside)
        self.view_SearchView.tf_Search.delegate = self
        
        titleView.addSubview(self.view_SearchView)
        self.navigationItem.titleView = titleView
    }
    
    @objc func onClearTextSearchAction() {
        self.view_SearchView.tf_Search.text = ""
        self.view_SearchView.btn_ClearText.isHidden = true
        
        if !self.isKeyboadShow {
            self.presenter.onTextFieldShouldReturn(keyword: self.view_SearchView.tf_Search.text ?? "")
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 0 {
            self.view_SearchView.btn_ClearText.isHidden = false
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count ?? 0 > 0 {
            self.view_SearchView.btn_ClearText.isHidden = false
        } else {
            self.view_SearchView.btn_ClearText.isHidden = true
        }
        self.presenter.onTextFieldShouldReturn(keyword: textField.text ?? "")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view_SearchView.endEditing(true)
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.text?.count ?? 0 == 0 {
            self.view_SearchView.btn_ClearText.isHidden = true
        } else {
            if self.view_SearchView.btn_ClearText.isHidden {
                self.view_SearchView.btn_ClearText.isHidden = false
            }
        }
    }
    
}

// MARK: - UITableViewDataSource
extension SearchExerciseViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_Exercise.registerNib(ofType: CellTableViewWorkout.self)
        self.tbv_Exercise.dataSource = self
        self.tbv_Exercise.delegate = self
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 115
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfItemsInSectionTable(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewWorkout.self, for: indexPath)
        let workout = self.presenter.onCellForItemTable(at: indexPath)
        cell.config(with: workout)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SearchExerciseViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRowAtTable(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}

// MARK: - SearchExerciseViewProtocol
extension SearchExerciseViewController: SearchExerciseViewProtocol {
    func onReloadDataTable() {
        self.tbv_Exercise.reloadData()
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_Exercise.beginUpdates()
        self.tbv_Exercise.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_Exercise.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
}

// MARK: setKeyboardEvent
extension SearchExerciseViewController {
    func setInitHandleKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            print("keyboardSize:", keyboardSize)
            self.isKeyboadShow = true
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.isKeyboadShow = false
    }
}
