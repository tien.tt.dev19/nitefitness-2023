//
//  CellTableViewWorkout.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//

import UIKit

class CellTableViewWorkout: UITableViewCell {
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var bookmarkImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!

    @IBOutlet weak var view_LineBottom: UIView!
    
    var isHiddenLine = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.videoImageView.isSkeletonable = true
        self.titleLabel.isSkeletonable = true
    }

    func config(with item: WorkoutModel?) {
        guard let `item` = item else {
            self.showSkeleton()
            return
        }
        
        self.view_LineBottom.isHidden = self.isHiddenLine
        
        self.videoImageView.setImageWith(imageUrl: item.details?.first?.exercise?.image ?? "", placeHolder: UIImage(named: "img_default"))

        self.titleLabel.text = item.name
        self.categoryLabel.text = item.getDetailsContent()
        if gUser?.id == item.userId {
            self.bookmarkImageView.isHidden = true
        } else {
            self.bookmarkImageView.isHidden = item.isBookmark == 0
        }
        self.hideSkeleton()
    }
}
