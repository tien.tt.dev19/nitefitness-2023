//
//  FooterSectionMyWorkoutView.swift
//  1SK
//
//  Created by vuongbachthu on 8/12/21.
//

import UIKit

protocol FooterSectionMyWorkoutViewDelegate: AnyObject {
    func onTapLoadMoreAction()
}

class FooterSectionMyWorkoutView: UITableViewHeaderFooterView {

    @IBOutlet weak var view_LoadMore: UIView!
    @IBOutlet weak var lbl_LoadMore: UILabel!
    
    weak var delegate: FooterSectionMyWorkoutViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view_LoadMore.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBgAction)))
    }
    
    @objc func onTouchBgAction() {
        self.delegate?.onTapLoadMoreAction()
        self.view_LoadMore.isHidden = true
    }

}
