//
//  HeaderSectionSuggestView.swift
//  1SK
//
//  Created by vuongbachthu on 8/12/21.
//

import Foundation
import UIKit

protocol HeaderSectionSuggestViewDelegate: AnyObject {
    func onTapLibraryAction()
}

class HeaderSectionSuggestView: UITableViewHeaderFooterView {
    
    weak var delegate: HeaderSectionSuggestViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTouchBgAction)))
    }
    
    @objc func onTouchBgAction() {
        self.delegate?.onTapLibraryAction()
    }
}
