//
//  
//  WorkoutConstract.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

// MARK: - View
protocol WorkoutViewProtocol: AnyObject {
    func reloadData()
    func setEmptyWorkoutHidden(_ isHidden: Bool)
    func getVisibleIndexPath() -> [IndexPath]
    func onReloadSections(in section: Int)
}

// MARK: - Interactor
protocol WorkoutInteractorInputProtocol {
    func getWorkoutList(_ userID: Int, page: Int, limit: Int)
    func getWorkoutSuggest()
}

protocol WorkoutInteractorOutputProtocol: AnyObject {
    func onGetWorkoutListFinished(with result: Result<[WorkoutModel], APIError>, total: Int, page: Int)
    func onGetWorkoutSuggestFinished(with result: Result<[WorkoutModel], APIError>)
}

// MARK: - Presenter
protocol WorkoutPresenterProtocol {
    func onViewDidLoad()
    func onTapLibraryAction()
    func onTapLoadMoreAction()
    
    // UITableView
    func numberMyWorkoutOfRow() -> Int
    func numberSuggestWorkoutOfRow() -> Int
    
    func itemForRow(at index: IndexPath) -> WorkoutModel?
    func onItemDidSelected(at indexPath: IndexPath)
    func isHiddenLoadmore() -> Bool
    func onRefreshAction()
    
    // Other
    func onDidCreateWorkoutSuccess(workout: WorkoutModel)
}

// MARK: - Router
protocol WorkoutRouterProtocol: BaseRouterProtocol {
    func gotoMyWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?)
    func gotoExerciseLibraryViewController(with delegate: ExerciseLibraryPresenterDelegate?)
    
    func gotoAddExserciseViewController(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?)
}
