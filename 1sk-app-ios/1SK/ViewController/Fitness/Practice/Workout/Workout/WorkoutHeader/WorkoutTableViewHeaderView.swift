//
//  WorkoutTableViewHeaderView.swift
//  1SK
//
//  Created by tuyenvx on 21/06/2021.
//

import UIKit

protocol WorkoutTableViewHeaderViewDelegate: AnyObject {
    func buttonLibraryDidTapped()
}

class WorkoutTableViewHeaderView: UIView {
    @IBOutlet weak var myWorkoutView: UIView!
    @IBOutlet weak var emptyWorkoutView: UIView!

    weak var delegate: WorkoutTableViewHeaderViewDelegate?

    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func buttonLibraryWorkoutDidTapped(_ sender: Any) {
        delegate?.buttonLibraryDidTapped()
    }

    func setEmptyWorkoutViewHidden(_ isHidden: Bool) {
        emptyWorkoutView.isHidden = isHidden
        myWorkoutView.isHidden = !isHidden
    }
}
