//
//  
//  WorkoutInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class WorkoutInteractorInput {
    weak var output: WorkoutInteractorOutputProtocol?
    var exerciseService: ExerciseServiceProtocol?
}

// MARK: - WorkoutInteractorInputProtocol
extension WorkoutInteractorInput: WorkoutInteractorInputProtocol {
    func getWorkoutList(_ userID: Int, page: Int, limit: Int) {
        self.exerciseService?.getListWorkout(userID: userID, page: page, limit: limit, completion: { [weak self] result in
            self?.output?.onGetWorkoutListFinished(with: result.unwrapSuccessModel(), total: result.getTotal() ?? 0, page: page)
        })
    }
    
    func getWorkoutSuggest() {
        self.exerciseService?.getListWorkoutSuggest(completion: { [weak self] result in
            self?.output?.onGetWorkoutSuggestFinished(with: result.unwrapSuccessModel())
        })
    }
}
