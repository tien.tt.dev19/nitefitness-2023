//
//  
//  WorkoutPresenter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class WorkoutPresenter {
    weak var view: WorkoutViewProtocol?
    private var interactor: WorkoutInteractorInputProtocol
    private var router: WorkoutRouterProtocol

    init(interactor: WorkoutInteractorInputProtocol,
         router: WorkoutRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    // Tableview
    private var listMyWorkouts: [WorkoutModel] = []
    private var listSuggestWorkouts: [WorkoutModel] = []

    private let defaultNumberOfWorkout = 5
    private var isExpand: Bool = false

    private var hasLoadmore: Bool {
        return !self.isExpand && self.listMyWorkouts.count > self.defaultNumberOfWorkout
    }

    // Loadmore
    private var totalItem = 0
    private let limit = 100

    // MARK: - Get API Data
    private func getDataWorkout(in page: Int) {
        self.router.showHud()
        let userID = gUser?.id ?? -1
        self.interactor.getWorkoutList(userID, page: page, limit: limit)
    }
}

// MARK: - WorkoutPresenterProtocol
extension WorkoutPresenter: WorkoutPresenterProtocol {
    func onViewDidLoad() {
        self.getDataWorkout(in: 1)
        self.interactor.getWorkoutSuggest()
    }

    func onTapLibraryAction() {
        self.router.gotoExerciseLibraryViewController(with: self)
    }
    
    func onTapLoadMoreAction() {
        self.isExpand = true
        self.view?.reloadData()
    }

    // MARK: UITableView
    
    func isHiddenLoadmore() -> Bool {
        return self.isExpand ? true : (self.listMyWorkouts.count > self.defaultNumberOfWorkout ? false : true)
    }
    
    func numberMyWorkoutOfRow() -> Int {
        return self.isExpand ? self.listMyWorkouts.count : min(self.defaultNumberOfWorkout, self.listMyWorkouts.count)
    }
    
    func numberSuggestWorkoutOfRow() -> Int {
        return self.listSuggestWorkouts.count
    }

    func itemForRow(at index: IndexPath) -> WorkoutModel? {
        switch index.section {
        case 0:
            return index.row < self.listMyWorkouts.count ? self.listMyWorkouts[index.row] : nil
            
        case 1:
            let workout = self.listSuggestWorkouts[index.row]
            return workout
            
        default:
            return nil
        }
    }

    func onItemDidSelected(at indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if indexPath.row < self.listMyWorkouts.count {
                self.router.gotoMyWorkoutViewController(with: self.listMyWorkouts[indexPath.row], delegate: self)
            }
            
        case 1:
            self.router.gotoMyWorkoutViewController(with: self.listSuggestWorkouts[indexPath.row], delegate: self)
            
        default:
            break
        }
    }
    
    func onRefreshAction() {
        self.getDataWorkout(in: 1)
    }

    func onDidCreateWorkoutSuccess(workout: WorkoutModel) {
        self.listMyWorkouts.insert(workout, at: 0)
        self.router.gotoAddExserciseViewController(with: workout, delegate: self)
    }
}

// MARK: - WorkoutInteractorOutput
extension WorkoutPresenter: WorkoutInteractorOutputProtocol {
    func onGetWorkoutListFinished(with result: Result<[WorkoutModel], APIError>, total: Int, page: Int) {
        switch result {
        case .success(let workoutsData):
            
            self.listMyWorkouts = workoutsData
            self.totalItem = total
            self.router.hideErrorView()
            
//            for (index, item) in self.listMyWorkouts.enumerated() {
//                let details = item.details.sorted(by: { $0.order < $1.order })
//                self.listMyWorkouts[index].details = details
//            }
            
            self.view?.reloadData()
            
        case .failure:
            break
        }
        
        self.router.hideHud()
    }
    
    func onGetWorkoutSuggestFinished(with result: Result<[WorkoutModel], APIError>) {
        switch result {
        case .success(let listWorkoutModel):
            self.listSuggestWorkouts = listWorkoutModel
            self.view?.onReloadSections(in: 1)
            
        case .failure:
            break
        }
    }
}

// MARK: - ErrorViewDelegate
extension WorkoutPresenter: ErrorViewDelegate {
    func onRetryButtonDidTapped(_ errorView: UIView) {
        self.getDataWorkout(in: 1)
    }
}

// MARK: - MyWorkoutPresenterDelegate
extension WorkoutPresenter: MyWorkoutPresenterDelegate {
    func onBookmarkWorkoutSuccess(at workout: WorkoutModel) {
        if let index = self.listMyWorkouts.firstIndex(where: {$0.id == workout.id}) {
            self.listMyWorkouts.remove(at: index)
            if let workoutSuggest = self.listSuggestWorkouts.first(where: {$0.id == workout.id}) {
                workoutSuggest.isBookmark = 0
            }
            
        } else {
            self.listMyWorkouts.insert(workout, at: 0)
            if let workoutSuggest = self.listSuggestWorkouts.first(where: {$0.id == workout.id}) {
                workoutSuggest.isBookmark = 1
            }
        }
        
        self.view?.reloadData()
    }
    
    func onDeleteWorkoutSuccess(at workoutId: Int?) {
        if let index = self.listMyWorkouts.firstIndex(where: {$0.id == workoutId ?? -1}) {
            self.listMyWorkouts.remove(at: index)
            self.view?.reloadData()
        }
    }
    
    func onUpdateWorkoutSuccess(at workout: WorkoutModel) {
        if let workoutUpdate = self.listMyWorkouts.enumerated().first(where: {$0.element.id == workout.id}) {
            self.listMyWorkouts[workoutUpdate.offset] = workout
            self.view?.reloadData()
            
        } else {
            self.listMyWorkouts.insert(workout, at: 0)
            if let workoutSuggest = self.listSuggestWorkouts.first(where: {$0.id == workout.id}) {
                workoutSuggest.isBookmark = 1
            }
        }
    }
    
    func onCloneWorkoutSuccess(at workout: WorkoutModel) {
        self.listMyWorkouts.insert(workout, at: 0)
        self.view?.reloadData()
    }
}

// MARK: - ExerciseLibraryPresenterDelegate
extension WorkoutPresenter: ExerciseLibraryPresenterDelegate {
    func onBookmarkWorkoutLibrarySuccess(workout: WorkoutModel) {
        if let index = self.listMyWorkouts.firstIndex(where: {$0.id == workout.id}) {
            self.listMyWorkouts.remove(at: index)
            if let workoutSuggest = self.listSuggestWorkouts.first(where: {$0.id == workout.id}) {
                workoutSuggest.isBookmark = 0
            }
            
        } else {
            self.listMyWorkouts.insert(workout, at: 0)
            if let workoutSuggest = self.listSuggestWorkouts.first(where: {$0.id == workout.id}) {
                workoutSuggest.isBookmark = 1
            }
        }
        
        self.view?.reloadData()
    }
    
    func onCloneWorkoutLibrarySuccess(at workout: WorkoutModel) {
        if let index = self.listMyWorkouts.firstIndex(where: {$0.id == workout.id}) {
            self.listMyWorkouts[index] = workout
            
        } else {
            self.listMyWorkouts.insert(workout, at: 0)
        }
        
        self.view?.reloadData()
    }
}

// MARK: - AddExsercisePresenterDelegate
extension WorkoutPresenter: AddExsercisePresenterDelegate {
    func onDidAddExercise(workout: WorkoutModel) {
        self.listMyWorkouts[0] = workout
        self.view?.reloadData()
    }
    
    func onDidAddExercise(details: [WorkoutDetailModel]) {
        // not use
    }
}
