//
//  
//  WorkoutRouter.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class WorkoutRouter: BaseRouter {
    static func setupModule() -> WorkoutViewController {
        let viewController = WorkoutViewController()
        let router = WorkoutRouter()
        let interactorInput = WorkoutInteractorInput()
        
        let presenter = WorkoutPresenter(interactor: interactorInput, router: router)
        interactorInput.output = presenter
        interactorInput.exerciseService = ExerciseService()
        
        viewController.presenter = presenter
        presenter.view = viewController
        
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - WorkoutRouterProtocol
extension WorkoutRouter: WorkoutRouterProtocol {
    func gotoMyWorkoutViewController(with workout: WorkoutModel, delegate: MyWorkoutPresenterDelegate?) {
        let workoutViewController = MyWorkoutRouter.setupModule(with: workout, delegate: delegate)
        self.viewController?.navigationController?.pushViewController(workoutViewController, animated: true)
    }

    func gotoExerciseLibraryViewController(with delegate: ExerciseLibraryPresenterDelegate?) {
        let controller = ExerciseLibraryRouter.setupModule(with: delegate)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoAddExserciseViewController(with workout: WorkoutModel, delegate: AddExsercisePresenterDelegate?) {
        
        let controller = AddExserciseRouter.setupModule(with: workout, delegate: delegate)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
