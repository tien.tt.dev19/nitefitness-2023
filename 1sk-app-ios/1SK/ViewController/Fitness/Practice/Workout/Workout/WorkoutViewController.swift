//
//  
//  WorkoutViewController.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//
//

import UIKit

class WorkoutViewController: BaseViewController {

    @IBOutlet weak var tbv_TableView: UITableView!
    
    var presenter: WorkoutPresenterProtocol!
    
    private lazy var header = createHeaderView()
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tbv_TableView.reloadData()
    }
    
}

// MARK: - Action Workout Create Delegate
extension WorkoutViewController {
    func onDidCreateWorkoutSuccess(workout: WorkoutModel) {
        self.presenter.onDidCreateWorkoutSuccess(workout: workout)
    }
}

// MARK: - WorkoutViewProtocol
extension WorkoutViewController: WorkoutViewProtocol {
    func reloadData() {
        self.tbv_TableView.reloadData()
        self.setEmptyWorkoutHidden(self.presenter.numberMyWorkoutOfRow() > 0)
    }

    func setEmptyWorkoutHidden(_ isHidden: Bool) {
        self.header.setEmptyWorkoutViewHidden(isHidden)
        self.header.frame = CGRect(origin: .zero, size: CGSize(width: Constant.Screen.width, height: isHidden ? 43 : 155))
        self.tbv_TableView.tableHeaderView = self.header
    }

    func getVisibleIndexPath() -> [IndexPath] {
        return self.tbv_TableView.indexPathsForVisibleRows ?? []
    }
    
    func onReloadSections(in section: Int) {
        let indexSet = IndexSet(integer: section)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
}

// MARK: Refresh Control
extension WorkoutViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableView Init
extension WorkoutViewController {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewWorkout.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionSuggestView.self)
        self.tbv_TableView.registerFooterNib(ofType: FooterSectionMyWorkoutView.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.tableHeaderView = self.header
    }

    private func createHeaderView() -> WorkoutTableViewHeaderView {
        let header = WorkoutTableViewHeaderView.loadFromNib()
        header.translatesAutoresizingMaskIntoConstraints = true
        header.autoresizingMask = .flexibleWidth
        header.frame = .zero
        header.delegate = self
        return header
    }

}

// MARK: - UITableViewDataSource
extension WorkoutViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 115
            
        case 1:
            return 115
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 54
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            if self.presenter.isHiddenLoadmore() {
                return 6
            } else {
                return 60
            }
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.presenter.numberMyWorkoutOfRow()
            
        case 1:
            return self.presenter.numberSuggestWorkoutOfRow()
            
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueCell(ofType: CellTableViewWorkout.self, for: indexPath)
            cell.config(with: self.presenter.itemForRow(at: indexPath))
            return cell
            
        case 1:
            let cell = tableView.dequeueCell(ofType: CellTableViewWorkout.self, for: indexPath)
            cell.config(with: self.presenter.itemForRow(at: indexPath))
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            return nil
            
        case 1:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionSuggestView.self)
            header.delegate = self
            return header
            
        default:
            return nil
        }
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let footer = tableView.dequeueHeaderView(ofType: FooterSectionMyWorkoutView.self)
            footer.delegate = self
            footer.view_LoadMore.isHidden = self.presenter.isHiddenLoadmore()
            return footer
        case 1:
            return nil
            
        default:
            return nil
        }
    }
}

// MARK: - UITableViewDelegate
extension WorkoutViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onItemDidSelected(at: indexPath)
    }
}

// MARK: - WorkoutTableViewHeaderViewDelegate
extension WorkoutViewController: WorkoutTableViewHeaderViewDelegate {
    func buttonLibraryDidTapped() {
        self.presenter.onTapLibraryAction()
    }
}

// MARK: - HeaderSectionSuggestViewDelegate
extension WorkoutViewController: HeaderSectionSuggestViewDelegate {
    func onTapLibraryAction() {
        self.presenter.onTapLibraryAction()
    }
}

// MARK: - FooterSectionMyWorkoutViewDelegate
extension WorkoutViewController: FooterSectionMyWorkoutViewDelegate {
    func onTapLoadMoreAction() {
        self.presenter.onTapLoadMoreAction()
    }
}
