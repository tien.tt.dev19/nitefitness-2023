//
//  WorkoutSelectionTableViewCell.swift
//  1SK
//
//  Created by tuyenvx on 18/06/2021.
//

import UIKit

class WorkoutSelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    @IBOutlet weak var underLineView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(with item: WorkoutItemProtocol, isSelected: Bool, isShowUnderLine: Bool) {
        contentLabel.text = item.name
        checkImage.isHidden = !isSelected
        underLineView.isHidden = !isShowUnderLine
    }
}
