//
//  WorkoutSelectectionViewController.swift
//  1SK
//
//  Created by tuyenvx on 17/06/2021.
//

import UIKit

protocol WorkoutSelectionViewControllerDelegate: AnyObject {
    func didSelected(item: WorkoutItemProtocol, type: WorkoutSelectionViewController.ItemType)
}

class WorkoutSelectionViewController: BaseViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    private let exerciseService = ExerciseService()

    var items: [WorkoutItemProtocol] = []
    var type: ItemType = .subject
    var selectedID: Int?
    var selectedItem: WorkoutItemProtocol?
    weak var delegate: WorkoutSelectionViewControllerDelegate?
    
    var safeAreaBottom: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            return window.safeAreaInsets.bottom
        } else {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.bottom ?? 0
        }
    }

    var safeAreaTop: CGFloat {
        if #available(iOS 13.0, *) {
            let window = UIApplication.shared.windows[0]
            return window.safeAreaInsets.top
        } else {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.bottomViewHeightConstraint.constant = 0
        self.bottomView.roundCorners(cornes: [.layerMinXMinYCorner, .layerMaxXMinYCorner], radius: 15)
        self.getListItems()
    }
    // MARK: - Setup
    private func setupTableView() {
        self.tableView.registerNib(ofType: WorkoutSelectionTableViewCell.self)
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }

    // MARK: - Action
    @IBAction func backgroundDidTapped(_ sender: Any) {
        self.hide()
    }

    @IBAction func buttonCancelDidTapped(_ sender: Any) {
        self.hide()
    }

    @IBAction func buttonSelectDidTapped(_ sender: Any) {
        guard let `selectedItem` = selectedItem else {
            return
        }
        self.delegate?.didSelected(item: selectedItem, type: self.type)
        self.hide()
    }

    func show() {
        self.bottomViewHeightConstraint.constant = calculateBottomViewHeight()
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in

        })
    }

    func hide(completion: (() -> Void)? = nil) {
        self.bottomViewHeightConstraint.constant = 0
        UIView.animate(withDuration: Constant.Number.animationTime, animations: {
            self.view.layoutIfNeeded()
        }, completion: { _ in
            self.dismiss(animated: false, completion: nil)
            completion?()
        })
    }

    func onGetDataSuccess(with items: [WorkoutItemProtocol]) {
        self.items = items
        self.hideErrorView()
        self.selectedItem = items.first(where: { $0.id == selectedID })
        self.tableViewHeightConstraint.constant = calculateTableViewHeight()
        self.tableView.reloadData()
        self.updateSelectedButtonState()
        self.show()
    }
    // MARK: - Helper
    private func calculateTableViewHeight() -> CGFloat {
        let maxHeight = Constant.Screen.height - safeAreaBottom - safeAreaTop - 44 - 51
        return min(items.count.cgFloatValue * 38, maxHeight)
    }

    private func calculateBottomViewHeight() -> CGFloat {
        return calculateTableViewHeight() + 51 + safeAreaBottom
    }

    private func updateSelectedButtonState() {
        self.selectButton.isEnabled = selectedItem != nil
        self.selectButton.setTitleColor(selectedItem != nil ? R.color.mainColor()!: R.color.disable()!, for: .normal)
    }
}
// MARK: - API
extension WorkoutSelectionViewController {
    func getListItems() {
        self.showProgressHud()
        switch self.type {
        case .subject:
            self.titleLabel.text = "Chọn bộ môn"
            self.exerciseService.getListSubject { [weak self] result in
                guard let `self` = self else {
                    return
                }
                let newResults = result.unwrapSuccessModel()
                switch newResults {
                case .success(let items):
                    self.onGetDataSuccess(with: items)
                case .failure:
                    self.showErrorView(with: nil, delegate: self)
                }
                self.hideProgressHud()
            }
        case .level:
            self.titleLabel.text = "Chọn cấp độ"
            self.exerciseService.getListLevel { [weak self] result in
                guard let `self` = self else {
                    return
                }
                let newResults = result.unwrapSuccessModel()
                switch newResults {
                case .success(let items):
                    self.onGetDataSuccess(with: items)
                case .failure:
                    self.showErrorView(with: nil, delegate: self)
                }
                self.hideProgressHud()
            }
        case .type:
            self.titleLabel.text = "Chọn loại bài tập"
            self.exerciseService.getListType { [weak self] result in
                guard let `self` = self else {
                    return
                }
                let newResults = result.unwrapSuccessModel()
                switch newResults {
                case .success(let items):
                    self.onGetDataSuccess(with: items)
                case .failure:
                    self.showErrorView(with: nil, delegate: self)
                }
                self.hideProgressHud()
            }
        }
    }
}

// MARK: - UITableViewDataSource
extension WorkoutSelectionViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: WorkoutSelectionTableViewCell.self, for: indexPath)
        let item = items[indexPath.row]
        cell.config(with: item, isSelected: item.id == selectedItem?.id, isShowUnderLine: indexPath.row != items.count - 1)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension WorkoutSelectionViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 38
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedItem = items[indexPath.row]
        self.updateSelectedButtonState()
        tableView.reloadData()
    }
}

extension WorkoutSelectionViewController {
    enum ItemType {
        case subject
        case level
        case type
    }
}

// MARK: - ErrorViewDelegate
extension WorkoutSelectionViewController: ErrorViewDelegate {
    func onRetryButtonDidTapped(_ errorView: UIView) {
        self.getListItems()
    }
}
