//
//  SampleModel.swift
//  1SK
//
//  Created by Thaad on 28/04/2022.
//

import Foundation
import RealmSwift

class SampleModel: Object {
    @Persisted var id: String = ""
    @Persisted var name: String?
    @Persisted var age: Int?
    @Persisted var status: String?
    
    @Persisted var category: String?
    @Persisted var details: String?
    var size: RealmOptional = RealmOptional<Double>()

    convenience init(name: String?) {
        self.init()
        self.id = UUID().uuidString
        self.name = name
    }
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
