//
//  
//  RealmSampleInteractorInput.swift
//  1SK
//
//  Created by Thaad on 28/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol RealmSampleInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol RealmSampleInteractorOutputProtocol: AnyObject {
    
}

// MARK: - RealmSample InteractorInput
class RealmSampleInteractorInput {
    weak var output: RealmSampleInteractorOutputProtocol?
}

// MARK: - RealmSample InteractorInputProtocol
extension RealmSampleInteractorInput: RealmSampleInteractorInputProtocol {

}
