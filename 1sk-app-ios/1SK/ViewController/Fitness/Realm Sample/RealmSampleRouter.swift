//
//  
//  RealmSampleRouter.swift
//  1SK
//
//  Created by Thaad on 28/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol RealmSampleRouterProtocol {

}

// MARK: - RealmSample Router
class RealmSampleRouter {
    weak var viewController: RealmSampleViewController?
    
    static func setupModule() -> RealmSampleViewController {
        let viewController = RealmSampleViewController()
        let router = RealmSampleRouter()
        let interactorInput = RealmSampleInteractorInput()
        let viewModel = RealmSampleViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - RealmSample RouterProtocol
extension RealmSampleRouter: RealmSampleRouterProtocol {
    
}
