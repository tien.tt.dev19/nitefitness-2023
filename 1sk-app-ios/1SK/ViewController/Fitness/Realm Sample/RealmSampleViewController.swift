//
//  
//  RealmSampleViewController.swift
//  1SK
//
//  Created by Thaad on 28/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol RealmSampleViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - RealmSample ViewController
class RealmSampleViewController: BaseViewController {
    var router: RealmSampleRouterProtocol!
    var viewModel: RealmSampleViewModelProtocol!
    
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        let obj_1 = SampleModel.init(name: "name 1")
        let obj_2 = SampleModel.init(name: "name 2")
        let obj_3 = SampleModel.init(name: "name 3")
        let obj_4 = SampleModel.init(name: "name 4")
        
        print("SampleModel obj_1 id: ", obj_1.id)
        print("SampleModel obj_2 id: ", obj_2.id)
        print("SampleModel obj_3 id: ", obj_3.id)
        print("SampleModel obj_4 id: ", obj_4.id)
        
        let realmManager: RealmManagerProtocol = RealmManager()
        
        let models1: [SampleModel] = realmManager.objects()
        print("SampleModel FileLogData:", models1.count) // 0
        
        // let's add some model
        let model: SampleModel = SampleModel()
        model.id = UUID().uuidString
        model.name = "Name Testtt"
        model.category = "Category"
        model.details = nil
        model.size.value = 250
        realmManager.write(model)
        
        let model_x: SampleModel = SampleModel()
        model_x.id = UUID().uuidString
        model_x.name = "Name xxxxxx"
        model_x.category = "Category xxxxx"
        model_x.details = nil
        model_x.size.value = 5000
        realmManager.write(model_x)

        // now table should have one element
        let models2: [SampleModel] = realmManager.objects()
        
        print("SampleModel models2:", models2.count) // 1
        
        for item in models2 {
            print("SampleModel id: \(item.id)")
            print("SampleModel name: \(item.name ?? "null")")
            print("SampleModel age: \(item.age ?? -1)")
            print("SampleModel status: \(item.status ?? "null")")
            print("SampleModel category: \(item.category ?? "null")")
            print("SampleModel details: \(item.details ?? "null")")
            print("SampleModel size: \(item.size.value ?? -1)")
            print("SampleModel ------------------------------\n")
        }
        
        // let's remove inserted modle
//        realmManager.remove(model)

        // now table should be empty again
        let models3: [SampleModel] = realmManager.objects()
        print("SampleModel models3:", models3.count) // 0
    }
    
    // MARK: - Action
    @IBAction func onAddAction(_ sender: Any) {
        print("onAddAction")
        
    }
    
    @IBAction func onUpdateAction(_ sender: Any) {
        print("onUpdateAction")
        
    }
    
    @IBAction func onDeleteAction(_ sender: Any) {
        print("onDeleteAction")
        
    }
    
}

// MARK: - RealmSample ViewProtocol
extension RealmSampleViewController: RealmSampleViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}
