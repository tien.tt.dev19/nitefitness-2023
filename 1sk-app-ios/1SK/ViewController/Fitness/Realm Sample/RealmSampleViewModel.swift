//
//  
//  RealmSampleViewModel.swift
//  1SK
//
//  Created by Thaad on 28/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol RealmSampleViewModelProtocol {
    func onViewDidLoad()
    
}

// MARK: - RealmSample ViewModel
class RealmSampleViewModel {
    weak var view: RealmSampleViewProtocol?
    private var interactor: RealmSampleInteractorInputProtocol

    init(interactor: RealmSampleInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - RealmSample ViewModelProtocol
extension RealmSampleViewModel: RealmSampleViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - RealmSample InteractorOutputProtocol
extension RealmSampleViewModel: RealmSampleInteractorOutputProtocol {

}
