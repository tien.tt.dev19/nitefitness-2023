//
//  AlertUnpairDeviceViewController.swift
//  1SK
//
//  Created by Thaad on 28/11/2022.
//

import UIKit

protocol AlertUnpairDeviceViewDelegate: AnyObject {
    func onAgreeUnpairDevice(device: DeviceRealm?)
}

class AlertUnpairDeviceViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    
    weak var delegate: AlertUnpairDeviceViewDelegate?
    var device: DeviceRealm?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitGestureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }


    @IBAction func onCloseAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAgreeUnpairDevice(device: self.device)
        self.hiddenAnimate()
    }

}

// MARK: - Init Animation
extension AlertUnpairDeviceViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertUnpairDeviceViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }

    @objc func onTapGestureAction() {
        self.hiddenAnimate()
    }
}
