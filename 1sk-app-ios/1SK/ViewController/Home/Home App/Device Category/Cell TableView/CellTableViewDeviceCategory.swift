//
//  CellTableViewDeviceCategory.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import UIKit

protocol CellTableViewDeviceCategoryDelegate: AnyObject {
    func onPairedAction(device: DeviceRealm?)
}

class CellTableViewDeviceCategory: UITableViewCell {

    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var btn_Paired: UIButton!
    
    weak var delegate: CellTableViewDeviceCategoryDelegate?
    
    var device: DeviceRealm? {
        didSet {
            self.img_Icon.image = self.device?.icon
            self.lbl_Name.text = self.device?.name_display
            
            if let uuid = self.device?.uuid, uuid.count > 0 {
                self.btn_Paired.isHidden = false
            } else {
                self.btn_Paired.isHidden = true
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onPairedAction(_ sender: Any) {
        self.delegate?.onPairedAction(device: self.device)
    }
    
}
