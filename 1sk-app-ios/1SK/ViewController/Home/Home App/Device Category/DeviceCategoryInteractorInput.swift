//
//  
//  DeviceCategoryInteractorInput.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol DeviceCategoryInteractorInputProtocol {
    
}

// MARK: - Interactor Output Protocol
protocol DeviceCategoryInteractorOutputProtocol: AnyObject {
    
}

// MARK: - DeviceCategory InteractorInput
class DeviceCategoryInteractorInput {
    weak var output: DeviceCategoryInteractorOutputProtocol?
}

// MARK: - DeviceCategory InteractorInputProtocol
extension DeviceCategoryInteractorInput: DeviceCategoryInteractorInputProtocol {
    
}
