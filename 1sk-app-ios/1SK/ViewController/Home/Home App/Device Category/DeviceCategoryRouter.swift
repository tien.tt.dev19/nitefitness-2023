//
//  
//  DeviceCategoryRouter.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol DeviceCategoryRouterProtocol {
    func onDismiss()
    func showMainProfileRouter(device: DeviceRealm?)
}

// MARK: - DeviceCategory Router
class DeviceCategoryRouter {
    weak var viewController: DeviceCategoryViewController?
    
    static func setupModule(type: TypeDevice?) -> DeviceCategoryViewController {
        let viewController = DeviceCategoryViewController()
        let router = DeviceCategoryRouter()
        let interactorInput = DeviceCategoryInteractorInput()
        let viewModel = DeviceCategoryViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.type = type
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - DeviceCategory RouterProtocol
extension DeviceCategoryRouter: DeviceCategoryRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
    func showMainProfileRouter(device: DeviceRealm?) {
        let controller = MainProfileRouter.setupModule(device: device)
        self.viewController?.show(controller, sender: nil)
    }
}
