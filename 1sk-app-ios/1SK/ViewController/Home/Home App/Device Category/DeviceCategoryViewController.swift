//
//  
//  DeviceCategoryViewController.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol DeviceCategoryViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
}

// MARK: - DeviceCategory ViewController
class DeviceCategoryViewController: BaseViewController {
    var router: DeviceCategoryRouterProtocol!
    var viewModel: DeviceCategoryViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.viewDidAppear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Thiết bị"
        
        self.setInitLayout()
        self.setInitUITableView()
    }
    
    private func setInitLayout() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismiss()
    }
}

// MARK: - DeviceCategory ViewProtocol
extension DeviceCategoryViewController: DeviceCategoryViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension DeviceCategoryViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderInSectionViewDeviceCategory.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewDeviceCategory.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.listCategory.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderInSectionViewDeviceCategory.self)
        header.delegate = self
        header.section = section
        header.category = self.viewModel.listCategory[section]
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let category = self.viewModel.listCategory[section]
        if category.isCollap == true {
            return 0
            
        } else {
            return self.viewModel.listCategory[section].devices?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewDeviceCategory.self, for: indexPath)
        cell.delegate = self
        cell.device = self.viewModel.listCategory[indexPath.section].devices?[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension DeviceCategoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let device = self.viewModel.listCategory[indexPath.section].devices?[indexPath.row] else {
            return
        }
        let profile: ProfileRealm? = gRealm.objects().first(where: {$0.type == device.type})
        device.profile = profile
        self.router.showMainProfileRouter(device: device)
    }
}

// MARK: HeaderInSectionViewDeviceCategoryDelegate
extension DeviceCategoryViewController: HeaderInSectionViewDeviceCategoryDelegate {
    func onStyleAction(collap: Bool, section: Int?) {
        guard let _section = section else {
            return
        }
        guard self.viewModel.listCategory[_section].devices?.count ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Chưa có thiết bị này")
            return
        }
        self.viewModel.listCategory[_section].isCollap = collap
        self.tbv_TableView.reloadSections(IndexSet(integer: _section), with: .fade)
    }
}

// MARK: CellTableViewDeviceCategoryDelegate
extension DeviceCategoryViewController: CellTableViewDeviceCategoryDelegate {
    func onPairedAction(device: DeviceRealm?) {
        self.presentAlertUnpairDeviceView(device: device)
    }
}

// MARK: AlertUnpairDeviceViewDelegate
extension DeviceCategoryViewController: AlertUnpairDeviceViewDelegate {
    func presentAlertUnpairDeviceView(device: DeviceRealm?) {
        let controller = AlertUnpairDeviceViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.device = device
        self.present(controller, animated: false)
    }
    
    func onAgreeUnpairDevice(device: DeviceRealm?) {
        guard let _device = device else {
            return
        }
        
        if _device.type == .SCALES, let peripheral = BluetoothManager.shared.peripheral {
            BluetoothManager.shared.setDisconnectDevice(peripheral: peripheral)
        }
        gRealm.remove(_device)
        
        self.viewModel.onAgreeUnpairDevice(device: _device)
    }
}
