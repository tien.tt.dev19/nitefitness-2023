//
//  
//  DeviceCategoryViewModel.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

class DeviceCategory: NSObject {
    var name: String?
    var icon: UIImage?
    var type: TypeDevice?
    var devices: [DeviceRealm]?
    var isCollap: Bool?
}

// MARK: - ViewModelProtocol
protocol DeviceCategoryViewModelProtocol {
    func onViewDidLoad()
    func onViewWillAppear()
    func viewDidAppear()
    
    func onAgreeUnpairDevice(device: DeviceRealm?)
    
    var listCategory: [DeviceCategory] { get }
}

// MARK: - DeviceCategory ViewModel
class DeviceCategoryViewModel {
    weak var view: DeviceCategoryViewProtocol?
    private var interactor: DeviceCategoryInteractorInputProtocol

    init(interactor: DeviceCategoryInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var type: TypeDevice?
    var listCategory: [DeviceCategory] = []
}

// MARK: - DeviceCategory ViewModelProtocol
extension DeviceCategoryViewModel: DeviceCategoryViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
    
    func onViewWillAppear() {
        self.setCategoryData()
    }
    
    func viewDidAppear() {
        //
    }
    
    func onAgreeUnpairDevice(device: DeviceRealm?) {
        self.setCategoryData()
    }
}

// MARK: Handler
extension DeviceCategoryViewModel {
    func setCategoryData() {
        self.listCategory = []
        
        self.setCategoryScales()
        self.setCategoryBp()
        self.setCategoryJumpRope()
//        self.setCategoryWatch()
        
        self.view?.onReloadData()
    }
    
    // MARK: Scales
    func setCategoryScales() {
        let category = DeviceCategory()
        category.name = "Cân thông minh"
        category.icon = R.image.ic_scale_device_connected()
        category.type = .SCALES
        category.devices = []
        
        if let type = self.type {
            if type == .SCALES {
                category.isCollap = false
            } else {
                category.isCollap = true
            }
        } else {
            category.isCollap = false
        }
        
        // Scales CF398
        let listDeviceCF398: [DeviceRealm] = gRealm.objects().filter({$0.type == .SCALES && $0.code == SKDevice.Code.CF_398})
        if listDeviceCF398.count > 0 {
            listDeviceCF398.forEach { device in
                device.icon = R.image.img_scales_CF398_category()
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.CF_398
            device.name_display = SKDevice.NameDisplay.CF_398
            device.icon = R.image.img_scales_CF398_category()
            device.code = SKDevice.Code.CF_398
            device.type = .SCALES
            category.devices?.append(device)
        }
        
        // Scales CF539
        let listDeviceCF539: [DeviceRealm] = gRealm.objects().filter({$0.type == .SCALES && $0.code == SKDevice.Code.CF_539})
        if listDeviceCF539.count > 0 {
            listDeviceCF539.forEach { device in
                device.icon = R.image.img_scales_CF539_category()
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.CF_539
            device.name_display = SKDevice.NameDisplay.CF_539
            device.icon = R.image.img_scales_CF539_category()
            device.code = SKDevice.Code.CF_539
            device.type = .SCALES
            category.devices?.append(device)
        }
        
        // Scales CF516
        let listDeviceCF516: [DeviceRealm] = gRealm.objects().filter({$0.type == .SCALES && $0.code == SKDevice.Code.CF_516})
        if listDeviceCF516.count > 0 {
            listDeviceCF516.forEach { device in
                device.icon = R.image.img_scales_CF516_category()
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.CF_516
            device.name_display = SKDevice.NameDisplay.CF_516
            device.icon = R.image.img_scales_CF516_category()
            device.code = SKDevice.Code.CF_516
            device.type = .SCALES
            category.devices?.append(device)
        }
        
        self.listCategory.append(category)
    }
    
    // MARK: BP
    func setCategoryBp() {
        let category = DeviceCategory()
        category.name = "Huyết áp"
        category.icon = R.image.ic_bp_device_connected()
        category.type = .BP
        category.devices = []
        
        if let type = self.type {
            if type == .BP {
                category.isCollap = false
            } else {
                category.isCollap = true
            }
        } else {
            category.isCollap = false
        }
        
        // BP DBP-6292B
        let listDBP_6292B: [DeviceRealm] = gRealm.objects().filter({$0.type == .BP && $0.code == SKDevice.Code.DBP_6292B})
        if listDBP_6292B.count > 0 {
            listDBP_6292B.forEach { device in
                device.icon = UIImage(named: "img_bp_category")
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.DBP_6292B
            device.name_display = SKDevice.NameDisplay.DBP_6292B
            device.code = SKDevice.Code.DBP_6292B
            device.icon = UIImage(named: "img_bp_category")
            device.type = .BP
            category.devices?.append(device)
        }
        
        // BP DBP-6277B
        let listDBP_6277B: [DeviceRealm] = gRealm.objects().filter({$0.type == .BP && $0.code == SKDevice.Code.DBP_6277B})
        if listDBP_6277B.count > 0 {
            listDBP_6277B.forEach { device in
                device.icon = UIImage(named: "img_bp_category")
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.DBP_6277B
            device.name_display = SKDevice.NameDisplay.DBP_6277B
            device.code = SKDevice.Code.DBP_6277B
            device.icon = UIImage(named: "img_bp_category")
            device.type = .BP
            category.devices?.append(device)
        }
        
        self.listCategory.append(category)
    }
    
    // MARK: JumpRope
    func setCategoryJumpRope() {
        let category = DeviceCategory()
        category.name = "Dây nhảy"
        category.icon = R.image.ic_jump_rope_device_connected()
        category.type = .JUMP_ROPE
        category.devices = []
        
        if let type = self.type {
            if type == .JUMP_ROPE {
                category.isCollap = false
            } else {
                category.isCollap = true
            }
        } else {
            category.isCollap = false
        }
        
        // JumpRope RS1949LB
        let listRS1949LB: [DeviceRealm] = gRealm.objects().filter({$0.type == .JUMP_ROPE && $0.code == SKDevice.Code.RS_1949LB})
        if listRS1949LB.count > 0 {
            listRS1949LB.forEach { device in
                device.icon = R.image.ic_jumprope()
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.RS_1949LB
            device.name_display = SKDevice.NameDisplay.RS_1949LB
            device.code = SKDevice.Code.RS_1949LB
            device.icon = R.image.ic_jumprope()
            device.type = .JUMP_ROPE
            category.devices?.append(device)
        }
        
        // JumpRope RS2047LB
        let listRS2047LB: [DeviceRealm] = gRealm.objects().filter({$0.type == .JUMP_ROPE && $0.code == SKDevice.Code.RS_2047LB})
        if listRS2047LB.count > 0 {
            listRS2047LB.forEach { device in
                device.icon = R.image.ic_jumprope()
                category.devices?.append(device)
            }
            
        } else {
            let device = DeviceRealm()
            device.name = SKDevice.Name.RS_2047LB
            device.name_display = SKDevice.NameDisplay.RS_2047LB
            device.code = SKDevice.Code.RS_2047LB
            device.icon = R.image.ic_jumprope()
            device.type = .JUMP_ROPE
            category.devices?.append(device)
        }
        
        self.listCategory.append(category)
    }
    
    // MARK: Watch
    func setCategoryWatch() {
        let category = DeviceCategory()
        category.name = "Đồng hồ"
        category.icon = R.image.ic_watch_device_connected()
        category.type = .WATCH
        category.devices = []
        
        if let type = self.type {
            if type == .WATCH {
                category.isCollap = false
            } else {
                category.isCollap = true
            }
        } else {
            category.isCollap = false
        }
        
        // Watch S5
        let listDeviceS5: [DeviceRealm] = gRealm.objects().filter({$0.type == .WATCH})
        if listDeviceS5.count > 0 {
            listDeviceS5.forEach { device in
                category.devices?.append(device)
            }

        } else {
            let device = DeviceRealm()
            device.name = "SSSSSS"
            device.name_display = "S5"
            device.code = "SSSSSS"
            device.icon = R.image.img_bp()
            device.type = .WATCH
            category.devices?.append(device)
        }
        
        self.listCategory.append(category)
    }
}

// MARK: - DeviceCategory InteractorOutputProtocol
extension DeviceCategoryViewModel: DeviceCategoryInteractorOutputProtocol {
    
}
