//
//  HeaderInSectionViewDeviceCategory.swift
//  1SK
//
//  Created by Thaad on 16/12/2022.
//

import UIKit

protocol HeaderInSectionViewDeviceCategoryDelegate: AnyObject {
    func onStyleAction(collap: Bool, section: Int?)
}

class HeaderInSectionViewDeviceCategory: UITableViewHeaderFooterView {
    weak var delegate: HeaderInSectionViewDeviceCategoryDelegate?
    
    @IBOutlet weak var view_Line: UIView!
    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var img_Style: UIImageView!
    @IBOutlet weak var btn_Style: UIButton!
    
    var section: Int? {
        didSet {
            if self.section == 0 {
                self.view_Line.backgroundColor = .white
                
            } else {
                self.view_Line.backgroundColor = R.color.background()
            }
        }
    }
    var category: DeviceCategory? {
        didSet {
            self.img_Icon.image = self.category?.icon
            self.lbl_Name.text = self.category?.name
            self.btn_Style.isSelected = self.category?.isCollap ?? false
            
            if self.category?.isCollap == true {
                self.img_Style.image = R.image.ic_collap_device()
                
            } else {
                self.img_Style.image = R.image.ic_expand_device()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func onStyleAction(_ sender: Any) {
        self.btn_Style.isSelected = !self.btn_Style.isSelected
        self.delegate?.onStyleAction(collap: self.btn_Style.isSelected, section: self.section)
    }
}
