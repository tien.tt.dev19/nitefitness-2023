//
//  AlertConnectDeviceViewController.swift
//  1SKConnect
//
//  Created by Thaad on 07/08/2022.
//

import UIKit

protocol AlertConnectDeviceViewDelegate: AnyObject {
    func onConnectDeviceAction(typeDevice: TypeDevice?)
}

class AlertConnectDeviceViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var img_Device: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Desc: UILabel!
    
    weak var delegate: AlertConnectDeviceViewDelegate?
    
    var typeDevice: TypeDevice?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitDataUI()
        self.setInitGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }

    func setInitDataUI() {
        switch self.typeDevice {
        case .SCALES:
            self.lbl_Title.text = "Cân 1SK"
            self.img_Device.image = R.image.img_scales()
            
        case .SPO2:
            self.lbl_Title.text = "Máy đo SPO2"
            
        case .BP:
            self.lbl_Title.text = "Máy đo huyết áp"
            self.img_Device.image = R.image.img_bp()
            
        case .WATCH:
            self.lbl_Title.text = "Vòng đeo 1SK"
            self.img_Device.image = R.image.img_scales()
            
        case .JUMP_ROPE:
            self.lbl_Title.text = "Dây nhảy 1SK"
            self.img_Device.image = R.image.img_scales()
        
        default:
            break
        }
    }

    @IBAction func onCancelAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    @IBAction func onDetailAction(_ sender: Any) {
        switch self.typeDevice {
        case .SCALES:
            if let url = URL(string: PRODUCT_SCALES_URL) {
                UIApplication.shared.open(url)
            }
            
        case .BP:
            if let url = URL(string: PRODUCT_BP_URL) {
                UIApplication.shared.open(url)
            }
            
        default:
            break
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.hiddenAnimate()
        }
    }
    
    @IBAction func onConnectAction(_ sender: Any) {
        self.delegate?.onConnectDeviceAction(typeDevice: self.typeDevice)
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension AlertConnectDeviceViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertConnectDeviceViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.hiddenAnimate()
    }
}
