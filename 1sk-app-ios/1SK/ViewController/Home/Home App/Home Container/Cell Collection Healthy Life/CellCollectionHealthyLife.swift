//
//  CellCollectionHealthyLife.swift
//  1SK
//
//  Created by Tiến Trần on 05/09/2022.
//

import UIKit

class CellCollectionHealthyLife: UICollectionViewCell {
    
    @IBOutlet weak var img_Thumbnail: UIImageView!
    @IBOutlet weak var lbl_PostTitle: UILabel!
    @IBOutlet weak var lbl_PostDescription: UILabel!
    
    var model: PostHealthModel? {
        didSet {
            if let data = model {
                self.img_Thumbnail.setImage(with: data.yoastHeadJson?.ogImage?.first?.url ?? "", completion: nil)
                self.lbl_PostTitle.text = data.yoastHeadJson?.title ?? ""
                self.lbl_PostDescription.text = data.yoastHeadJson?.ogDescription ?? ""
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
