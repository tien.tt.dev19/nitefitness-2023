//
//  CellCollectionHotProduct.swift
//  1SK
//
//  Created by Tiến Trần on 05/09/2022.
//

import UIKit

class CellCollectionHotProduct: UICollectionViewCell {
    
    @IBOutlet weak var img_ProductImage: UIImageView!
    @IBOutlet weak var lbl_ProductName: UILabel!
    @IBOutlet weak var lbl_SalePrices: UILabel!
    @IBOutlet weak var lbl_OriginalPrices: UILabel!
    @IBOutlet weak var lbl_SalePercent: UILabel!
    @IBOutlet weak var view_SalePercent: UIView!
    
    var model: Product? {
        didSet {
            if let data = model {
                self.img_ProductImage.setImage(with: data.image ?? "", completion: nil)
                self.lbl_ProductName.text = data.name ?? ""
                self.lbl_SalePrices.text = "\(data.frontSalePrice?.formatNumber() ?? "0")đ"
                
                if data.isSalePrice ?? false {
                    self.lbl_OriginalPrices.isHidden = false
                    self.view_SalePercent.isHidden = false
                    self.lbl_SalePercent.text = "-\(data.salePercentage ?? 0)%"
                    self.lbl_OriginalPrices.text = "\(data.priceWithTaxes?.formatNumber() ?? "0")đ"
                    self.lbl_OriginalPrices.strikeThrough(true)
                    
                    return
                }
                
                self.lbl_OriginalPrices.isHidden = true
                self.view_SalePercent.isHidden = true
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
