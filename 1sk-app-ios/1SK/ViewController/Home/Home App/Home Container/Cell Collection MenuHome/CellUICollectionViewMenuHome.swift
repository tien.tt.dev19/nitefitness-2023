//
//  CellUICollectionViewMenuHome.swift
//  1SK
//
//  Created by Thaad on 22/11/2022.
//

import UIKit

class CellUICollectionViewMenuHome: UICollectionViewCell {

    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var img_Paired: UIImageView!
    
    @IBOutlet weak var lbl_Name: UILabel!
    
    var item: MenuHome? {
        didSet {
            self.img_Icon.image = self.item?.icon
            self.lbl_Name.text = self.item?.name
            
            if self.item?.device == nil {
                self.img_Paired.isHidden = true
                
            } else {
                self.img_Paired.isHidden = false
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
