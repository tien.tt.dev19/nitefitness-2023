//
//  
//  HomeContainerInteractorInput.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HomeContainerInteractorInputProtocol {
    func getSliderBanner(typeSlider: SliderType)
    func getProductDetail(id: Int)
    
    func getHotProducts()
    func getHomeHealthPosts()
}

// MARK: - Interactor Output Protocol
protocol HomeContainerInteractorOutputProtocol: AnyObject {
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
    
    func onGetHotProductFinished(with result: Result<[Product], APIError>)
    func onGetHomeHealthPostsFinished(with result: Result<[PostHealthModel], APIError>)
}

// MARK: - HomeContainer InteractorInput
class HomeContainerInteractorInput {
    weak var output: HomeContainerInteractorOutputProtocol?
    
    var storeService: StoreServiceProtocol?
    var workPressService: WorkPressServiceProtocol?
}

// MARK: - HomeContainer InteractorInputProtocol
extension HomeContainerInteractorInput: HomeContainerInteractorInputProtocol {
    func getSliderBanner(typeSlider: SliderType) {
        self.storeService?.getSliderBanner(typeSlider: typeSlider, completion: { [weak self] result in
            self?.output?.onGetSliderBannerFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getHotProducts() {
        self.storeService?.getHotProduct { [weak self] result in
            self?.output?.onGetHotProductFinished(with: result.unwrapSuccessModel())
        }
    }
    
    func getHomeHealthPosts() {
        self.workPressService?.getHomeHealthPosts { [weak self] result in
            self?.output?.onGetHomeHealthPostsFinished(with: result.unwrapSuccessModelWP())
        }
    }
}
