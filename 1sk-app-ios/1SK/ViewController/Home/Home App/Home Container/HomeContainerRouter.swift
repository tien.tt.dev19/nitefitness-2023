//
//  
//  HomeContainerRouter.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HomeContainerRouterProtocol {
    func showDeviceCategoryRouter(type: TypeDevice?)
    
    func showVirtualRaceRouter()
    func showCareRouter()
    func showMainProfileRouter(device: DeviceRealm?)
    
    func showProductStore(product: ProductModel?)
}

// MARK: - HomeContainer Router
class HomeContainerRouter {
    weak var viewController: HomeContainerViewController?
    
    static func setupModule() -> HomeContainerViewController {
        let viewController = HomeContainerViewController()
        let router = HomeContainerRouter()
        let interactorInput = HomeContainerInteractorInput()
        let viewModel = HomeContainerViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        interactorInput.workPressService = WorkPressService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HomeContainer RouterProtocol
extension HomeContainerRouter: HomeContainerRouterProtocol {
    func showDeviceCategoryRouter(type: TypeDevice?) {
        let controller = DeviceCategoryRouter.setupModule(type: type)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showVirtualRaceRouter() {
        let controller = VirtualRaceRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showCareRouter() {
        let controller = CareRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showMainProfileRouter(device: DeviceRealm?) {
        let controller = MainProfileRouter.setupModule(device: device)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
}
