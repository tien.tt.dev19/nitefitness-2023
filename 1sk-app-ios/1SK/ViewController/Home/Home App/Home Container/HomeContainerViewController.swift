//
//  
//  HomeContainerViewController.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit
import ImageSlideshow
import Firebase
import FirebaseAnalytics

// MARK: - ViewProtocol
protocol HomeContainerViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    
    // Slide Show
    func setUISliderBanner(sliderBanner: SliderBannerModel?)
    func onDidSelectItem(product: ProductModel)
    
    func onGetHotProductFinished(products: [Product])
    func onGetHomeHealthPostsFinished(posts: [PostHealthModel])
}

// MARK: - HomeContainer ViewController
class HomeContainerViewController: BaseViewController {
    var router: HomeContainerRouterProtocol!
    var viewModel: HomeContainerViewModelProtocol!
    
    @IBOutlet weak var view_Menu: UIView!
    
    @IBOutlet weak var coll_MenuHome: UICollectionView!
    @IBOutlet weak var coll_ProductHot: UICollectionView!
    @IBOutlet weak var coll_HealthyLife: UICollectionView!
    @IBOutlet weak var constraint_height_ContentHotProduct: NSLayoutConstraint!
    @IBOutlet weak var view_HomeSlider: ImageSlideshow!
    @IBOutlet weak var view_HotProducts: UIView!
    
    var currentPageSlideShow = 0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
        self.setInitUICollectionView()
    }
    
    private func setInitLayout() {
        //self.view_Menu.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }
    
    // MARK: - Action
    @IBAction func onDeviceCategoryDevice(_ sender: UIButton) {
        guard Configure.shared.isLogged() == true else {
            self.presentLoginRouter()
            return
        }
        
        self.router.showDeviceCategoryRouter(type: nil)
    }
    
    @IBAction func onSeeMoreHotProduct(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 2
    }
    
    @IBAction func onSeeMoreHomeHealth(_ sender: UIButton) {
        self.tabBarController?.selectedIndex = 1
    }
}

// MARK: - HomeContainer ViewProtocol
extension HomeContainerViewController: HomeContainerViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.coll_MenuHome.reloadData()
    }
    
    // Slide Show
    func setUISliderBanner(sliderBanner: SliderBannerModel?) {
        guard let sliderModel = sliderBanner else {
            return
        }
        guard let listBanners = sliderModel.items, listBanners.count > 0 else {
            return
        }
        
        var imageSource: [InputSource] = []
        for banner in listBanners {
            if let image = banner.image {
                if let url = URL(string: image) {
                    let kingfisherSource = KingfisherSource(url: url, placeholder: R.image.img_default(), options: nil)
                    imageSource.append(kingfisherSource)
                }
            }
        }
        
        self.setInitImageSlideshow(imageSource: imageSource)
    }
    
    func onDidSelectItem(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
    
    func onGetHotProductFinished(products: [Product]) {
        self.coll_ProductHot.performBatchUpdates({
            let indexSet = IndexSet(integersIn: 0...0)
            self.coll_ProductHot.reloadSections(indexSet)
        }, completion: nil)
        
        let count = self.viewModel.listProductHot.count
        if count > 0 {
            self.view_HotProducts.isHidden = false
        } else {
            self.view_HotProducts.isHidden = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.constraint_height_ContentHotProduct.constant = self.coll_ProductHot.contentSize.height + 16
        }
    }
    
    func onGetHomeHealthPostsFinished(posts: [PostHealthModel]) {
        self.coll_HealthyLife.performBatchUpdates({
            let indexSet = IndexSet(integersIn: 0...0)
            self.coll_HealthyLife.reloadSections(indexSet)
        }, completion: nil)
    }
}

// MARK: ImageSlideshowDelegate
extension HomeContainerViewController: ImageSlideshowDelegate {
    func setInitImageSlideshow(imageSource: [InputSource]) {
        self.view_HomeSlider.slideshowInterval = 3.0
        
        self.view_HomeSlider.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        self.view_HomeSlider.contentScaleMode = UIViewContentMode.scaleAspectFill

        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = R.color.mainColor()
        pageControl.pageIndicatorTintColor = .white
        self.view_HomeSlider.pageIndicator = pageControl

        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        self.view_HomeSlider.activityIndicator = DefaultActivityIndicator()
        self.view_HomeSlider.delegate = self

        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.view_HomeSlider.setImageInputs(imageSource)

        self.view_HomeSlider.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureSlideshow)))
    }
    
    @objc func onTapGestureSlideshow() {
        guard let banner = self.viewModel.sliderBanner?.items?[self.currentPageSlideShow] else {
            return
        }
        
        switch banner.typeBanner {
        case .link:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                guard let url = URL(string: banner.link ?? "") else {
                    return
                }
                UIApplication.shared.open(url)
            }
            
        case .product:
            guard let productId = banner.productId else {
                return
            }
            
            self.viewModel.onDidSelectBanner(productId: productId)
            
        default:
            break
        }
    }
    
    // ImageSlideshowDelegate
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        self.currentPageSlideShow = page
    }
}

//MARK: - UICollectionViewDataSource
extension HomeContainerViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_MenuHome.registerNib(ofType: CellUICollectionViewMenuHome.self)
        self.coll_ProductHot.registerNib(ofType: CellCollectionHotProduct.self)
        self.coll_HealthyLife.registerNib(ofType: CellCollectionHealthyLife.self)
        
        self.coll_MenuHome.dataSource = self
        self.coll_MenuHome.delegate = self
        
        self.coll_ProductHot.dataSource = self
        self.coll_ProductHot.delegate = self
        self.coll_ProductHot.isScrollEnabled = false
        
        self.coll_HealthyLife.dataSource = self
        self.coll_HealthyLife.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164) + 16
        
        let layout_MenuHome = UICollectionViewFlowLayout()
        layout_MenuHome.scrollDirection = .horizontal
        layout_MenuHome.minimumLineSpacing = 0
        layout_MenuHome.minimumInteritemSpacing = 0
        layout_MenuHome.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout_MenuHome.itemSize = CGSize(width: 80, height: self.self.coll_MenuHome.height)
        self.coll_MenuHome.collectionViewLayout = layout_MenuHome
        
        let layout_HotProduct = UICollectionViewFlowLayout()
        layout_HotProduct.minimumLineSpacing = 16
        layout_HotProduct.minimumInteritemSpacing = 16
        layout_HotProduct.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_HotProduct.itemSize = CGSize(width: width, height: height)
        self.coll_ProductHot.collectionViewLayout = layout_HotProduct
        self.constraint_height_ContentHotProduct.constant = height * 2 + 16
        
        let layout_HeathyLife = UICollectionViewFlowLayout()
        layout_HeathyLife.scrollDirection = .horizontal
        layout_HeathyLife.minimumLineSpacing = 16
        layout_HeathyLife.minimumInteritemSpacing = 16
        layout_HeathyLife.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_HeathyLife.itemSize = CGSize(width: 254, height: 245)
        self.coll_HealthyLife.collectionViewLayout = layout_HeathyLife
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.coll_MenuHome:
            return self.viewModel.listMenuHome.count
            
        case self.coll_ProductHot:
            return self.viewModel.listProductHot.count

        case self.coll_HealthyLife:
            return self.viewModel.listPostHealth.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.coll_MenuHome:
            let cell = collectionView.dequeuCell(ofType: CellUICollectionViewMenuHome.self, for: indexPath)
            cell.item = self.viewModel.listMenuHome[indexPath.row]
            return cell
            
        case self.coll_ProductHot:
            let cell = collectionView.dequeuCell(ofType: CellCollectionHotProduct.self, for: indexPath)
            cell.model = self.viewModel.listProductHot[indexPath.row]
            return cell
            
        case self.coll_HealthyLife:
            let cell = collectionView.dequeuCell(ofType: CellCollectionHealthyLife.self, for: indexPath)
            cell.model = self.viewModel.listPostHealth[indexPath.row]
            return cell

        default:
            return UICollectionViewCell()
        }
    }
}

//MARK: - UICollectionViewDelegate
extension HomeContainerViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.coll_MenuHome:
            guard Configure.shared.isLogged() == true else {
                self.presentLoginRouter()
                return
            }
            
            let menu = self.viewModel.listMenuHome[indexPath.row]
            switch menu.type {
            case .VR:
                self.router.showVirtualRaceRouter()
                
            case .CARE:
                self.router.showCareRouter()
                
            case .SCALE:
                self.setSelectDevice(type: .SCALES, device: menu.device)
                
            case .BP:
                self.setSelectDevice(type: .BP, device: menu.device)
                
            case .JUMP_ROPE:
                self.setSelectDevice(type: .JUMP_ROPE, device: menu.device)
                
            case .WATCH:
                self.setSelectDevice(type: .WATCH, device: menu.device)
                
            default:
                break
            }
            
        case self.coll_ProductHot:
            if let productId = self.viewModel.listProductHot[indexPath.row].originalProduct {
                self.viewModel.onDidSelectBanner(productId: productId)
            }
            
        case self.coll_HealthyLife:
            guard let url = URL(string: self.viewModel.listPostHealth[indexPath.row].link ?? "") else {
                return
            }
            UIApplication.shared.open(url)
            
        default:
            break
        }
    }
    
    func setSelectDevice(type: TypeDevice, device: DeviceRealm?) {
        guard gUser?.id ?? 0 > 0 else {
            return
        }
        guard let _device = device else {
            self.router.showDeviceCategoryRouter(type: type)
            return
        }
        self.router.showMainProfileRouter(device: _device)
    }
}

// MARK: - LoginViewDelegate
extension HomeContainerViewController: LoginViewDelegate {
    func presentLoginRouter() {
        let controller = LoginRouter.setupModule(delegate: self)
        controller.modalPresentationStyle = .custom
        self.present(controller, animated: true)
    }
    
    func onLoginSuccess() {
        SKToast.shared.showToast(content: "Đăng nhập thành công")
    }
}
