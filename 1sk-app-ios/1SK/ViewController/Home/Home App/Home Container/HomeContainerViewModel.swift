//
//  
//  HomeContainerViewModel.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//
//

import UIKit

struct MenuHome {
    var name: String?
    var icon: UIImage?
    var type: TypeMenuHome?
    var device: DeviceRealm?
}

enum TypeMenuHome {
    case VR
    case CARE
    case SCALE
    case BP
    case JUMP_ROPE
    case WATCH
}

// MARK: - ViewModelProtocol
protocol HomeContainerViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func onDidSelectBanner(productId: Int)
    func onCreateProfile(type: TypeDevice) -> ProfileRealm
    
    var sliderBanner: SliderBannerModel? { get set }
    var listMenuHome: [MenuHome] { get set }
    var listProductHot: [Product] { get set }
    var listPostHealth: [PostHealthModel] { get set }
}

// MARK: - HomeContainer ViewModel
class HomeContainerViewModel {
    weak var view: HomeContainerViewProtocol?
    private var interactor: HomeContainerInteractorInputProtocol

    init(interactor: HomeContainerInteractorInputProtocol) {
        self.interactor = interactor
    }

    var sliderBanner: SliderBannerModel?
    
    var listMenuHome: [MenuHome] = []
    var listProductHot: [Product] = []
    var listPostHealth: [PostHealthModel] = []
}

// MARK: - HomeContainer ViewModelProtocol
extension HomeContainerViewModel: HomeContainerViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        
        self.interactor.getSliderBanner(typeSlider: .HomeApp)
        self.interactor.getHotProducts()
        self.interactor.getHomeHealthPosts()
    }
    
    func onViewDidAppear() {
        self.getInitDataMenuHome()
    }
    
    func onDidSelectBanner(productId: Int) {
        self.view?.showHud()
        self.interactor.getProductDetail(id: productId)
    }
    
    func onCreateProfile(type: TypeDevice) -> ProfileRealm {
        let profile = ProfileRealm()
        profile.id = gUser?.id
        profile.accessToken = KeyChainManager.shared.accessToken
        profile.fullName = gUser?.fullName
        profile.birthday = gUser?.birthday
        profile.avatar = gUser?.avatar
        profile.gender = gUser?.gender
        profile.height = gUser?.height
        profile.weight = gUser?.weight
        profile.blood = gUser?.blood
        profile.type = type
        
        gRealm.write(profile)
        
        return profile
    }
}

// MARK: - HomeContainer Handler
extension HomeContainerViewModel {
    func getInitDataMenuHome() {
        self.listMenuHome.removeAll()
        
        //MARK: VR
//        let itemVR = MenuHome.init(name: "Giải đua", icon: R.image.ic_home_vr(), type: .VR, device: nil)
//        self.listMenuHome.append(itemVR)
        
        //MARK: Care
        let itemCare = MenuHome.init(name: "Tư vấn", icon: R.image.ic_home_care(), type: .CARE, device: nil)
        self.listMenuHome.append(itemCare)
        
        //MARK: Scales
        let listScale: [DeviceRealm] = gRealm.objects().filter({$0.type == .SCALES})
        if listScale.count > 0 {
            listScale.forEach { device in
                switch device.code {
                case SKDevice.Code.CF_398:
                    let item = MenuHome.init(name: device.name_display, icon: R.image.ic_home_scales_398(), type: .SCALE, device: device)
                    self.listMenuHome.append(item)
                    
                case SKDevice.Code.CF_539:
                    let item = MenuHome.init(name: device.name_display, icon: R.image.ic_home_scales_539(), type: .SCALE, device: device)
                    self.listMenuHome.append(item)
                    
                case SKDevice.Code.CF_516:
                    let item = MenuHome.init(name: device.name_display, icon: R.image.ic_home_scales_516(), type: .SCALE, device: device)
                    self.listMenuHome.append(item)
                    
                default:
                    let item = MenuHome.init(name: device.name_display, icon: R.image.ic_home_scales(), type: .SCALE, device: device)
                    self.listMenuHome.append(item)
                }
            }
        } else {
            let item = MenuHome.init(name: "Cân", icon: R.image.ic_home_scales(), type: .SCALE, device: nil)
            self.listMenuHome.append(item)
        }
        
        //MARK: BP
        let listBp: [DeviceRealm] = gRealm.objects().filter({$0.type == .BP})
        if listBp.count > 0 {
            listBp.forEach { device in
                let item = MenuHome.init(name: device.name_display, icon: R.image.ic_home_bp(), type: .BP, device: device)
                self.listMenuHome.append(item)
            }
        } else {
            let item = MenuHome.init(name: "Huyết áp", icon: R.image.ic_home_bp(), type: .BP, device: nil)
            self.listMenuHome.append(item)
        }
        
        //MARK: Jump Rope
        let listJR: [DeviceRealm] = gRealm.objects().filter({$0.type == .JUMP_ROPE})
        if listJR.count > 0 {
            listJR.forEach { device in
                let item = MenuHome.init(name: device.name_display, icon: UIImage(named: "ic_home_jump_rope"), type: .JUMP_ROPE, device: device)
                self.listMenuHome.append(item)
            }
        } else {
            let item = MenuHome.init(name: "Dây nhảy", icon: UIImage(named: "ic_home_jump_rope"), type: .JUMP_ROPE, device: nil)
            self.listMenuHome.append(item)
        }
        
        //MARK: Watch
//        let listWatch: [DeviceRealm] = gRealm.objects().filter({$0.type == .WATCH})
//        if listWatch.count > 0 {
//            listWatch.forEach { device in
//                let item = MenuHome.init(name: device.name_display, icon: R.image.ic_home_watch(), type: .WATCH, device: device)
//                self.listMenuHome.append(item)
//            }
//        } else {
//            let item = MenuHome.init(name: "Đồng hồ", icon: R.image.ic_home_watch(), type: .WATCH, device: nil)
//            self.listMenuHome.append(item)
//        }
        
        //MARK: Reload Data
        self.view?.onReloadData()
    }
}

// MARK: - HomeContainer InteractorOutputProtocol
extension HomeContainerViewModel: HomeContainerInteractorOutputProtocol {
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>) {
        switch result {
        case .success(let model):
            self.sliderBanner = model
            self.view?.setUISliderBanner(sliderBanner: self.sliderBanner)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onDidSelectItem(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetHotProductFinished(with result: Result<[Product], APIError>) {
        switch result {
        case .success(let model):
            self.listProductHot = model
            self.view?.onGetHotProductFinished(products: self.listProductHot)
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.view?.hideHud()
    }
    
    func onGetHomeHealthPostsFinished(with result: Result<[PostHealthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listPostHealth = model
            self.view?.onGetHomeHealthPostsFinished(posts: self.listPostHealth)
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.view?.hideHud()
    }
}
