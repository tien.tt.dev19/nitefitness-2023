//
//  
//  HomeAppInteractorInput.swift
//  1SK
//
//  Created by Thaad on 05/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HomeAppInteractorInputProtocol {
    func getConfigSetting()
    func getGenerateHashID()
    func getUserTags()
    func getHotlinePhoneNumber()
    func getBlogDetailById(blogId: Int)
    func getVideoDetailById(videoId: Int)
    func getDoctorDetailById(doctorId: Int)
    func getAppointmentDetail(id: Int)
    func getBlogDetailBySlug(slug: String, type: String)
    func getVideoDetailBySlug(slug: String, type: String)
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], objectSection: IndoorBikeModel)
    func getProductDetail(id: Int)
    func getAnnouncement(page: Int, per_page: Int)
}

// MARK: - Interactor Output Protocol
protocol HomeAppInteractorOutputProtocol: AnyObject {
    func onGetConfigSettingFinished(with result: Result<[ConfigModel], APIError>)
    func onGetGenerateHashIDFinished(with result: Result<String, APIError>)
    func onGetTagsFinished(with result: Result<[UserTag], APIError>)
    func onGetHotlinePhoneNumberFinished(with result: Result<HotlineModel, APIError>)
    func onGetBlogDetailByIdFinished(with result: Result<BlogModel, APIError>)
    func onGetVideoDetailByIdFinished(with result: Result<VideoModel, APIError>)
    func onGetDoctorDetailByIdFinished(with result: Result<DoctorModel, APIError>)
    func onGetAppointmentDetailFinished(with result: Result<AppointmentModel, APIError>)
    func onGetBlogDetailsBySlugFinished(with result: Result<BlogModel, APIError>)
    func onGetVideoDetailsBySlugFinished(with result: Result<VideoModel, APIError>)
    func onSetUploadLogFileBikeWokoutFinished(with result: Result<[IndoorBikeListData], APIError>, objectSection: IndoorBikeModel)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
    func onGetAnnouncementFinished(with result: Result<[AnnouncementModels], APIError>, page: Int, total: Int)
}

// MARK: - HomeApp InteractorInput
class HomeAppInteractorInput {
    weak var output: HomeAppInteractorOutputProtocol?
    
    var authService: AuthServiceProtocol?
    var careService: CareServiceProtocol?
    var blogService: BlogServiceProtocol?
    var fitnessService: FitnessServiceProtocol?
    var fitService: FitServiceProtocol?
    var storeService: StoreServiceProtocol?
    var connectService: ConnectServiceProtocol?
    var announcementService: AnnouncementServiceProtocol?
    var workPressService: WorkPressServiceProtocol?
}

// MARK: - HomeApp InteractorInputProtocol
extension HomeAppInteractorInput: HomeAppInteractorInputProtocol {
    func getConfigSetting() {
        self.authService?.getConfig(completion: { [weak self] result in
            self?.output?.onGetConfigSettingFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getGenerateHashID() {
        self.careService?.generateHasUserID( completion: { [weak self] result in
            self?.output?.onGetGenerateHashIDFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getUserTags() {
        self.careService?.getUserTags( completion: { [weak self] result in
            self?.output?.onGetTagsFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getHotlinePhoneNumber() {
        self.careService?.getHotlinePhoneNumber(completion: { [weak self] result in
            self?.output?.onGetHotlinePhoneNumberFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getBlogDetailById(blogId: Int) {
        self.blogService?.getBlogDetail(with: blogId, completion: { [weak self] result in
            self?.output?.onGetBlogDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getVideoDetailById(videoId: Int) {
        self.fitnessService?.getVideoDetailByID(with: videoId, completion: { [weak self] result in
            self?.output?.onGetVideoDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getDoctorDetailById(doctorId: Int) {
        self.careService?.getDoctorDetailById(doctorId: doctorId, completion: { [weak self] result in
            self?.output?.onGetDoctorDetailByIdFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getAppointmentDetail(id: Int) {
        self.careService?.getAppointmentDetail(appointmentId: id, completion: { [weak self] result in
            self?.output?.onGetAppointmentDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getBlogDetailBySlug(slug: String, type: String) {
        self.blogService?.getBlogBySlug(slug: slug, type: type, completion: { [weak self] result in
            self?.output?.onGetBlogDetailsBySlugFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getVideoDetailBySlug(slug: String, type: String) {
        self.fitnessService?.getVideoBySlug(slug: slug, type: type, completion: { [weak self] result in
            self?.output?.onGetVideoDetailsBySlugFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setUploadLogFileBikeWokout(sectionId: String, startTimeSection: Int, endTimeSection: Int, filePaths: [String], objectSection: IndoorBikeModel) {
        self.fitService?.setUploadLogFileBikeWokout(sectionId: sectionId, startTimeSection: startTimeSection, endTimeSection: endTimeSection, filePaths: filePaths, completion: { [weak self] result in
            self?.output?.onSetUploadLogFileBikeWokoutFinished(with: result.unwrapSuccessModel(), objectSection: objectSection)
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getAnnouncement(page: Int, per_page: Int) {
        self.announcementService?.getAnnouncementList(page: page, per_page: per_page) { [weak self] result in
            self?.output?.onGetAnnouncementFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        }
    }
}
