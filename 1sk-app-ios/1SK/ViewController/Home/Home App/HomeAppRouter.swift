//
//  
//  HomeAppRouter.swift
//  1SK
//
//  Created by Thaad on 05/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HomeAppRouterProtocol {
    func showCare()
    func showVirtualRace()
    func gotoCareViewController()
    func showPopupMKTViewController(listPopup: [PopupModel], delegate: PopupMKTViewDelegate?)
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?)
    func gotoHealthServiceVC(doctor: DoctorModel?)
    func gotoBlogDetailsViewController(blogModel: BlogModel, categoryType: CategoryType)
    func gotoVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType)
    func gotoDetailConsultingVC(appointment: AppointmentModel)
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func showProductStore(product: ProductModel?)
    func showMainProfileRouter(device: DeviceRealm?)
    func showDeviceConnectRouter()
}

// MARK: - HomeApp Router
class HomeAppRouter {
    weak var viewController: HomeAppViewController?
    
    static func setupModule() -> HomeAppViewController {
        let viewController = HomeAppViewController()
        let router = HomeAppRouter()
        let interactorInput = HomeAppInteractorInput()
        let viewModel = HomeAppViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        interactorInput.blogService = BlogService()
        interactorInput.fitnessService = FitnessService()
        interactorInput.careService = CareService()
        interactorInput.fitService = FitService()
        interactorInput.storeService = StoreService()
        interactorInput.connectService = ConnectService()
        interactorInput.announcementService = AnnouncementService()
        interactorInput.workPressService = WorkPressService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HomeApp RouterProtocol
extension HomeAppRouter: HomeAppRouterProtocol {
    func showCare() {
        let controller = CareRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showVirtualRace() {
//        let controller = VirtualRaceFitRouter.setupModule()
        let controller = VirtualRaceRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showPopupMKTViewController(listPopup: [PopupModel], delegate: PopupMKTViewDelegate?) {
        guard let tabBarController = self.viewController?.tabBarController else {
            return
        }

        if gIsShowMarketingPopup == false {
            let controller = PopupMKTViewController()
            controller.modalPresentationStyle = .custom
            controller.delegate = delegate
            controller.listPopup = listPopup
            tabBarController.present(controller, animated: false)
        }
    }
    
    func gotoCareViewController() {
        let controller = HealthServiceRouter.setupModule(doctor: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?) {
        let controller = DoctorDetailRouter.setupModule(doctor: doctor, isFrom: isFrom, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoHealthServiceVC(doctor: DoctorModel?) {
        let controller = HealthServiceRouter.setupModule(doctor: doctor)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoBlogDetailsViewController(blogModel: BlogModel, categoryType: CategoryType) {
        let controller = BlogDetailRouter.setupModule(blogModel: blogModel, categoryType: categoryType)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType) {
        let controller = PlayVideoRouter.setupModule(with: video, categoryType: categoryType)
        self.viewController?.navigationController?.pushViewController(controller, animated: true)
    }
    
    func gotoDetailConsultingVC(appointment: AppointmentModel) {
        let controller = DetailConsultingRouter.setupModule(with: appointment)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showMainProfileRouter(device: DeviceRealm?) {
        let controller = MainProfileRouter.setupModule(device: device)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showDeviceConnectRouter() {
        let controller = DeviceConnectRouter.setupModule()
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
    
}
