//
//  HomeAppView+Care.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import Foundation

// MARK: - DoctorDetailPresenterDelegate
extension HomeAppViewModel: DoctorDetailPresenterDelegate {
    func setGoToDoctorDetailView(doctor: DoctorModel?) {
        self.view?.gotoDoctorDetailVC(doctor: doctor, isFrom: "HomeAppViewController", delegate: self)
    }
    
    func onDetailConfirmBookingAction(doctor: DoctorModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.view?.gotoHealthServiceVC(doctor: doctor)
        }
    }
}
