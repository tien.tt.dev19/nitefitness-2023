//
//  HomeAppView+DeepLinks.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import Foundation

// MARK: UIApplication - DeepLinks
extension HomeAppViewModel {
    func setInitUIApplicationDeepLinks() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleOpenDeepLinks), name: .applicationOpenDeepLinks, object: nil)
    }
    
    @objc func onHandleOpenDeepLinks() {
        if let universalLink = gUniversalLink {
            self.view?.onSetTabBarSelectedIndex(0)
            
            let path = universalLink.path.split(separator: "/")
            if path.count > 0 {
                var pathSlug = "\(path[0])"
                
                let indexType = pathSlug.index(pathSlug.endIndex, offsetBy: -2)
                let shareType = "\(pathSlug[indexType...])"
                
                let indexObject = pathSlug.index(pathSlug.endIndex, offsetBy: -3)
                let objectId = "\(pathSlug[...indexObject])"
                print("<AppDelegate> onOpenDeepLinks shareType ", shareType)
                print("<AppDelegate> onOpenDeepLinks objectId ", objectId)
                
                switch shareType {
                case ShareType.blog.rawValue:
                    if pathSlug.count > 0 {
                        pathSlug.removeLast()
                        pathSlug.removeLast()
                        DispatchQueue.main.async {
                            self.view?.showHud()
                            self.interactor.getBlogDetailBySlug(slug: pathSlug, type: shareType)
                        }
                    }
                    
                case ShareType.video.rawValue:
                    if pathSlug.count > 0 {
                        pathSlug.removeLast()
                        pathSlug.removeLast()
                        DispatchQueue.main.async {
                            self.view?.showHud()
                            self.interactor.getVideoDetailBySlug(slug: pathSlug, type: shareType)
                        }
                    }
                    
                case ShareType.doctor.rawValue:
                    if let doctorId = Int(objectId) {
                        DispatchQueue.main.async {
                            self.view?.showHud()
                            self.interactor.getDoctorDetailById(doctorId: doctorId)
                        }
                    }
                    
                case ShareType.workout.rawValue:
                    DispatchQueue.main.async {
                        //self.router.showHud()
                        //self.interactor.getWorkoutDetailById(workoutId: shareId)
                    }
                    
                default:
                    break
                }
                
                gUniversalLink = nil
            }
        }
    }
}
