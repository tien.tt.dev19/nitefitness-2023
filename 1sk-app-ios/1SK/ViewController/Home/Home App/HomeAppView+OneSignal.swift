//
//  HomeAppView.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import Foundation

// MARK: UIApplication - OneSignal
extension HomeAppViewModel {
    func setInitUIApplicationOneSignal() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleNotificationOpenForeground(_:)), name: .applicationOSNotificationOpenedBlock, object: nil)
    }
    
    @objc func onHandleNotificationOpenForeground(_ notification: Notification) {
        if let object = notification.object as? PushNotiModel {
            self.view?.onSetTabBarSelectedIndex(0)
            
            switch object.screen {
            case .appointment:
                guard let appointmentId = object.data?.id else {
                    return
                }
                
                self.view?.showHud()
                self.interactor.getAppointmentDetail(id: appointmentId)
                
            default:
                break
            }
        }
        gPushNotiModel = nil
    }
    
    func onHandleNotificationOpenBackground(pushNotiModel: PushNotiModel?) {
        if let object = pushNotiModel {
            switch object.screen {
            case .appointment:
                guard let appointmentId = object.data?.id else {
                    return
                }
                
                self.view?.showHud()
                self.interactor.getAppointmentDetail(id: appointmentId)
                
            default:
                break
            }
        }
        gPushNotiModel = nil
    }
    
    func onGetAppointmentDetailSuccess(appointment: AppointmentModel) {
        guard let statusId = appointment.status?.id else {
            return
        }
        switch statusId {
        case AppointmentStatus.complete.id, AppointmentStatus.finish.id:
            self.view?.gotoDetailConsultingVC(appointment: appointment)
            
        default:
            self.view?.gotoDetailAppointmentVC(appointment: appointment, delegate: nil)
        }
    }
}
