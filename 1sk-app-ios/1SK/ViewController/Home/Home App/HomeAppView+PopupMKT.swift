//
//  HomeAppView+PopupMKT.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import Foundation

// MARK: PopupMKTViewDelegate
extension HomeAppViewModel: PopupMKTViewDelegate {
    func onSelectPopupAction(popup: PopupModel) {
        switch popup.typeObj {
        case .None:
            self.view?.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.view?.hideHud()
                guard let url = URL(string: popup.destinationUrl ?? "") else { return }
                UIApplication.shared.open(url)
            }
            
        case .Video:
            guard let objectId = popup.objectId else {
                return
            }
            self.view?.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.interactor.getVideoDetailById(videoId: objectId)
            }
            
        case .Blog:
            guard let objectId = popup.objectId else {
                return
            }
            self.view?.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.interactor.getBlogDetailById(blogId: objectId)
            }
            
        case .Doctor:
            guard let objectId = popup.objectId else {
                return
            }
            self.view?.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.interactor.getDoctorDetailById(doctorId: objectId)
            }
            
        case .Booking:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.view?.gotoCareViewController()
            }
            
        default:
            break
        }
    }
}
