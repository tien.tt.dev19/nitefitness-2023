//
//  HomeAppView+Socket.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import Foundation

// MARK: HandleSocketEvent
extension HomeAppViewModel {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onPopupLiveReadyToSendEvent), name: .PopupLiveReadyToSendEvent, object: nil)
    }
    
    @objc func onPopupLiveReadyToSendEvent(notification: NSNotification) {
        print("socket.on care: PopupLiveReadyToSendEvent HomePresenter")
        guard let popup = notification.object as? PopupModel else {
            return
        }
        self.view?.onSetTabBarSelectedIndex(0)
        self.view?.showPopupMKTViewController(listPopup: [popup], delegate: self)
    }
}
