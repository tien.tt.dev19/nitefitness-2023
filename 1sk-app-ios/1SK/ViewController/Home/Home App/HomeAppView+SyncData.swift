//
//  HomeAppView+SyncData.swift
//  1SK
//
//  Created by Thaad on 21/11/2022.
//

import Foundation

extension HomeAppViewModel {
    func setInitSyncFitLogDataFile() {
        //FileHelper.shared.printContentsOfDocumentDirectory(title: "./fit/log:", path: "/fit/log/")
        
        let realm: RealmManagerProtocol = RealmManager()
        let listSection: [IndoorBikeModel] = realm.objects()
        
        guard listSection.count > 0 else {
            FileHelper.shared.setRemoveDirectory(path: "/fit/log")
            return
        }
        
        for object in listSection {
            print("FileLogData id:", object.id)
            print("FileLogData device_name:", object.device_name ?? "null")
            print("FileLogData device_type:", object.device_type ?? "null")
            print("FileLogData start_time:", object.start_time ?? -1)
            print("FileLogData end_time:", object.end_time ?? -1)
            print("FileLogData -----------------------")
            
            for file in object.log_path {
                print("FileLogData                id:", file.id)
                print("FileLogData                file_name:", file.file_name ?? -1)
                print("FileLogData                end_time:", file.start_time ?? -1)
                print("FileLogData                end_time:", file.end_time ?? -1)
                print("FileLogData                -----------------------")
            }
            
            if let startTimeSection = object.start_time {
                let endTimeSection = object.end_time ?? 0

                let filePaths = object.log_path.array.map { return $0.filePath }
                let listFile = FileHelper.shared.getFileInDirectory(path: "/fit/log/")

                if filePaths.count > 0, listFile?.count ?? 0 > 0 {
                    self.interactor.setUploadLogFileBikeWokout(sectionId: object.id, startTimeSection: startTimeSection, endTimeSection: endTimeSection, filePaths: filePaths, objectSection: object)
                    
                } else {
                    print("FileLogData remove id:", object.id)
                    print("FileLogData ")
                    realm.remove(object)
                }
            }
        }
    }
}
