//
//  
//  HomeAppViewController.swift
//  1SK
//
//  Created by Thaad on 05/09/2022.
//
//

import UIKit
import SnapKit
import ImageSlideshow

// MARK: - ViewProtocol
protocol HomeAppViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onSetTabBarSelectedIndex(_ index: Int)
    func gotoCareViewController()
    func showPopupMKTViewController(listPopup: [PopupModel], delegate: PopupMKTViewDelegate?)
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?)
    func gotoHealthServiceVC(doctor: DoctorModel?)
    func gotoBlogDetailsViewController(blogModel: BlogModel, categoryType: CategoryType)
    func gotoVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType)
    func gotoDetailConsultingVC(appointment: AppointmentModel)
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func gotoProductStoreVC(product: ProductModel)
    
}

// MARK: - HomeApp ViewController
class HomeAppViewController: BaseViewController {
    var router: HomeAppRouterProtocol!
    var viewModel: HomeAppViewModelProtocol!
    
    private lazy var homeContainer = HomeContainerRouter.setupModule()
    
    @IBOutlet weak var view_Container: UIView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitChildView()
    }
}

// MARK: - HomeApp ViewProtocol
extension HomeAppViewController: HomeAppViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onSetTabBarSelectedIndex(_ index: Int) {
        DispatchQueue.main.async {
            self.tabBarController?.selectedIndex = index
        }
    }
    
    func showPopupMKTViewController(listPopup: [PopupModel], delegate: PopupMKTViewDelegate?) {
        self.router.showPopupMKTViewController(listPopup: listPopup, delegate: delegate)
    }
    
    func gotoCareViewController() {
        self.router.gotoCareViewController()
    }
    
    func gotoDoctorDetailVC(doctor: DoctorModel?, isFrom: String?, delegate: DoctorDetailPresenterDelegate?) {
        self.router.gotoDoctorDetailVC(doctor: doctor, isFrom: isFrom, delegate: delegate)
    }
    
    func gotoHealthServiceVC(doctor: DoctorModel?) {
        self.router.gotoHealthServiceVC(doctor: doctor)
    }
    
    func gotoBlogDetailsViewController(blogModel: BlogModel, categoryType: CategoryType) {
        self.router.gotoBlogDetailsViewController(blogModel: blogModel, categoryType: categoryType)
    }
    
    func gotoVideoDetailsViewController(with video: VideoModel, categoryType: CategoryType) {
        self.router.gotoVideoDetailsViewController(with: video, categoryType: categoryType)
    }
    
    func gotoDetailConsultingVC(appointment: AppointmentModel) {
        self.router.gotoDetailConsultingVC(appointment: appointment)
    }
    
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: delegate)
    }
    
    func gotoProductStoreVC(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
}

// MARK: - Container View
extension HomeAppViewController {
    private func setInitChildView() {
        self.addChildView(childViewController: self.homeContainer)
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}
