//
//  
//  HomeAppViewModel.swift
//  1SK
//
//  Created by Thaad on 05/09/2022.
//
//

import UIKit
import OneSignal

// MARK: - ViewModelProtocol
protocol HomeAppViewModelProtocol {
    func onViewDidLoad()
}

// MARK: - HomeApp ViewModel
class HomeAppViewModel {
    weak var view: HomeAppViewProtocol?
    var interactor: HomeAppInteractorInputProtocol
    
    init(interactor: HomeAppInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listTagsOneSignal: [UserTag] = []
    var listAnouncement: [AnnouncementModels] = []
    
}

// MARK: - HomeApp ViewModelProtocol
extension HomeAppViewModel: HomeAppViewModelProtocol {
    func onViewDidLoad() {
        self.interactor.getGenerateHashID()
        self.interactor.getUserTags()
        self.interactor.getHotlinePhoneNumber()
        self.interactor.getConfigSetting()
        
        self.interactor.getAnnouncement(page: 1, per_page: 200)
        
        self.setInitUIApplicationOneSignal()
        self.setInitUIApplicationDeepLinks()
        self.setInitHandleSocketEvent()
        self.setInitSyncFitLogDataFile()
        
        self.onHandleNotificationOpenBackground(pushNotiModel: gPushNotiModel)
    }
    
    func onCheckAllMessageIsRead() -> Bool {
        var allRead = false
        for item in self.listAnouncement {
            guard let isRead = item.isRead else { return false }
            if !isRead {
                allRead = false
                return false
            }
            allRead = true
        }
        return allRead
    }
}

// MARK: - HomeApp InteractorOutputProtocol
extension HomeAppViewModel: HomeAppInteractorOutputProtocol {
    func onGetConfigSettingFinished(with result: Result<[ConfigModel], APIError>) {
        switch result {
        case .success(let model):
            gConfigs = model
            
        case .failure:
            break
        }
    }
    
    func onGetGenerateHashIDFinished(with result: Result<String, APIError>) {
        switch result {
        case .success(let hashID):
            if let userID = gUser?.id {
                let id = String(userID)
                
                print("<ongenerateHashIDFinished> \(id)")
                
                OneSignal.setExternalUserId(id, withExternalIdAuthHashToken: hashID, withSuccess: { results in
                    // The results will contain push and email success statuses
                    print("<OneSignal> External user id update complete with results: ", results!.description)
                    // Push can be expected in almost every situation with a success status, but
                    // as a pre-caution its good to verify it exists
                    if let pushResults = results!["push"] {
                        print("<OneSignal> Set external user id push status: ", pushResults)
                    }
                    if let emailResults = results!["email"] {
                        print("<OneSignal> Set external user id email status: ", emailResults)
                    }
                    if let smsResults = results!["sms"] {
                        print("<OneSignal> Set external user id sms status: ", smsResults)
                    }
                    
                }, withFailure: {error in
                    print("<OneSignal> Set external user id done with error: " + error.debugDescription)
                })
            }
            
        case .failure:
            break
        }
    }
    
    func onGetTagsFinished(with result: Result<[UserTag], APIError>) {
        switch result {
        case .success(let listTag):
            self.listTagsOneSignal = listTag
            for tag in self.listTagsOneSignal {
                OneSignal.sendTag(tag.key!, value: tag.value!)
            }
            
        case .failure:
            break
        }
    }
    
    func onGetHotlinePhoneNumberFinished(with result: Result<HotlineModel, APIError>) {
        switch result {
        case .success(let model):
            gPhoneHotline = model.hotline ?? ""
            
        case .failure:
            break
        }
    }
    
    func onGetBlogDetailByIdFinished(with result: Result<BlogModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.blogFit.rawValue:
                    self.view?.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogFit)
                    
                case CategoryType.blogCare.rawValue:
                    self.view?.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onGetVideoDetailByIdFinished(with result: Result<VideoModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.videoFit.rawValue:
                    self.view?.gotoVideoDetailsViewController(with: model, categoryType: .videoFit)
                    
                case CategoryType.videoCare.rawValue:
                    self.view?.gotoVideoDetailsViewController(with: model, categoryType: .videoCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onGetDoctorDetailByIdFinished(with result: Result<DoctorModel, APIError>) {
        switch result {
        case .success(let model):
            self.setGoToDoctorDetailView(doctor: model)
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onGetAppointmentDetailFinished(with result: Result<AppointmentModel, APIError>) {
        switch result {
        case .success(let model):
            self.onGetAppointmentDetailSuccess(appointment: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetBlogDetailsBySlugFinished(with result: Result<BlogModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.blogFit.rawValue:
                    self.view?.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogFit)
                    
                case CategoryType.blogCare.rawValue:
                    self.view?.gotoBlogDetailsViewController(blogModel: model, categoryType: .blogCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onGetVideoDetailsBySlugFinished(with result: Result<VideoModel, APIError>) {
        switch result {
        case .success(let model):
            let object = model.categories?.first(where: {$0.parentId == 0})
            if let categoryId = object?.id {
                switch categoryId {
                case CategoryType.videoFit.rawValue:
                    self.view?.gotoVideoDetailsViewController(with: model, categoryType: .videoFit)
                    
                case CategoryType.videoCare.rawValue:
                    self.view?.gotoVideoDetailsViewController(with: model, categoryType: .videoCare)
                    
                default:
                    break
                }
            }
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onSetUploadLogFileBikeWokoutFinished(with result: Result<[IndoorBikeListData], APIError>, objectSection: IndoorBikeModel) {
        switch result {
        case .success(let model):
            print("./UPLOAD: Finished true: ", model)
            
            IndoorBikeManager.shared.onRemoveFileLogData(objectSection: objectSection)
            
        case .failure(let error):
            print("./UPLOAD: Finished false: ", error)
        }
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.gotoProductStoreVC(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetAnnouncementFinished(with result: Result<[AnnouncementModels], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            self.listAnouncement = model
            if self.onCheckAllMessageIsRead() {
                gTabBarController?.tabBar.removeBadge(index: 3)
                
            } else {
                gTabBarController?.tabBar.addBadge(index: 3)
            }
            
            if self.listAnouncement.isEmpty {
                gTabBarController?.tabBar.removeBadge(index: 3)
            }
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.view?.hideHud()
    }
}
