//
//  AlertDeviceNameListViewController.swift
//  1SK
//
//  Created by Thaad on 01/12/2022.
//

import UIKit

protocol AlertDeviceNameListViewDelegate: AnyObject {
    func onSelectedDevice(with deviceName: DeviceName)
}

class AlertDeviceNameListViewController: UIViewController {
    weak var delegate: AlertDeviceNameListViewDelegate?
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var constraint_height_TableView: NSLayoutConstraint!
    
    var titleAlert: String?
    var listDeviceName: [DeviceName] = []
    var currentDeviceName: DeviceName?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitLayout()
        self.setInitGestureView()
        self.setInitUITableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setReloadData()
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setSelectRow()
    }
    
    func setInitLayout() {
        self.lbl_Title.text = self.titleAlert
    }

    @IBAction func onDismissAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
}

// MARK: - Init Animation
extension AlertDeviceNameListViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertDeviceNameListViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }

    @objc func onTapGestureAction() {
        self.hiddenAnimate()
    }
}

// MARK: - UITableViewDataSource
extension AlertDeviceNameListViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewAlertDeviceNameList.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        if self.listDeviceName.count <= 5 {
            self.constraint_height_TableView.constant = CGFloat(self.listDeviceName.count * 50)
        } else {
            self.constraint_height_TableView.constant = CGFloat(5 * 50)
        }
    }
    
    func setReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func setSelectRow() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let index = self.listDeviceName.firstIndex(where: {$0.name == self.currentDeviceName?.name}) {
                let indexPath = IndexPath(row: index, section: 0)
                self.tbv_TableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listDeviceName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewAlertDeviceNameList.self, for: indexPath)
        cell.currentDeviceName = self.currentDeviceName
        cell.deviceName = self.listDeviceName[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AlertDeviceNameListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let deviceName = self.listDeviceName[indexPath.row]
        self.delegate?.onSelectedDevice(with: deviceName)
        self.hiddenAnimate()
    }
}
