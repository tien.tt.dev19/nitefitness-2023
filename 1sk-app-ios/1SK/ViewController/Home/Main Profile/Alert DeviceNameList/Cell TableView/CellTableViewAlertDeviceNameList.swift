//
//  CellTableViewAlertDeviceNameList.swift
//  1SK
//
//  Created by Thaad on 01/12/2022.
//

import UIKit

class CellTableViewAlertDeviceNameList: UITableViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    
    var currentDeviceName: DeviceName?
    
    var deviceName: DeviceName? {
        didSet {
            self.lbl_Name.text = self.deviceName?.display
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if self.deviceName?.name == self.currentDeviceName?.name {
            self.lbl_Name.textColor = R.color.mainColor()
        } else {
            self.lbl_Name.textColor = .black
        }
    }
    
}
