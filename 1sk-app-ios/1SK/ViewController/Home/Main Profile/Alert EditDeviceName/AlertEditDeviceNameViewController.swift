//
//  AlertEditDeviceNameViewController.swift
//  1SK
//
//  Created by Thaad on 28/11/2022.
//

import UIKit

protocol AlertEditDeviceNameViewDelegate: AnyObject {
    func onEditDeviceNameSuccess()
}

class AlertEditDeviceNameViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var tf_Name: UITextField!
    
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    weak var delegate: AlertEditDeviceNameViewDelegate?
    var device: DeviceRealm?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitUITextField()
        self.setInitHandleKeyboard()
        self.setInitDataUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    private func setInitDataUI() {
        self.tf_Name.text = self.device?.name_display ?? self.device?.name
    }
    
    @IBAction func onCloseAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        guard let name = self.tf_Name.text, name.count > 5 else {
            SKToast.shared.showToast(content: "Tên phải hơn 5 ký tự")
            return
        }
        
        guard !name.isEmptyOrWhitespace() else {
            SKToast.shared.showToast(content: "Tên phải là ký tự")
            return
        }
        
        gRealm.update {
            self.device?.name_display = name.trimmingCondenseWhitespace()
        }

        self.delegate?.onEditDeviceNameSuccess()
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension AlertEditDeviceNameViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - UITextFieldDelegate
extension AlertEditDeviceNameViewController: UITextFieldDelegate {
    private func setInitUITextField() {
        self.tf_Name.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}

// MARK: Handle Keyboard Event
extension AlertEditDeviceNameViewController {
    func setInitHandleKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            UIView.animate(withDuration: 1.0) {
                self.constraint_bottom_ContentView.constant = keyboardSize.height
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 1.0) {
            self.constraint_bottom_ContentView.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
