//
//  AlertSwitchAccountViewController.swift
//  1SKConnect
//
//  Created by Thaad on 14/06/2022.
//

import UIKit

protocol AlertSwitchAccountViewDelegate: AnyObject {
    func onAddProfileAction()
    func onSwitchProfileAction(account: AuthModel)
}

class AlertSwitchAccountViewController: BaseViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    
    @IBOutlet weak var tbv_Account: UITableView!
    @IBOutlet weak var constraint_height_TableView: NSLayoutConstraint!
    
    weak var delegate: AlertSwitchAccountViewDelegate?
    
    var currentProfile: ProfileRealm?
    var listSubAccount: [AuthModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitGestureView()
        self.setInitUITableView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setReloadData()
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.setSelectRow()
    }

    // MARK: Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    @IBAction func onAddAccountAction(_ sender: Any) {
        self.delegate?.onAddProfileAction()
        self.hiddenAnimate()
    }
    
}

// MARK: - Init Animation
extension AlertSwitchAccountViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertSwitchAccountViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.hiddenAnimate()
    }
}

// MARK: - UICollectionViewDataSource
extension AlertSwitchAccountViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_Account.registerNib(ofType: CellTableViewAccount.self)
        self.tbv_Account.dataSource = self
        self.tbv_Account.delegate = self
        
        if self.listSubAccount.count <= 5 {
            self.constraint_height_TableView.constant = CGFloat(self.listSubAccount.count * 70)
        } else {
            self.constraint_height_TableView.constant = CGFloat(5 * 70)
        }
    }
    
    func setReloadData() {
        self.tbv_Account.reloadData()
    }
    
    func setSelectRow() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let index = self.listSubAccount.firstIndex(where: {$0.customer?.id == self.currentProfile?.id}) {
                let indexPath = IndexPath(row: index, section: 0)
                self.tbv_Account.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listSubAccount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewAccount.self, for: indexPath)
        cell.currentProfile = self.currentProfile
        cell.model = self.listSubAccount[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension AlertSwitchAccountViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let account = self.listSubAccount[indexPath.row]
        self.delegate?.onSwitchProfileAction(account: account)
        self.hiddenAnimate()
    }
}
