//
//  BloodPressure+BLE.swift
//  1SK
//
//  Created by Thaad on 29/09/2022.
//

import Foundation
import CoreBluetooth

// MARK: - BluetoothManager
extension BloodPressureViewController {
    func setInitBluetoothManager() {
        print("<Bluetooth> 1SK BP setInitBluetoothManager name:", self.viewModel.device?.name ?? 0)
        
        if let peripheral = BluetoothManager.shared.peripheral {
            BluetoothManager.shared.setDisconnectDevice(peripheral: peripheral)
        }
        
        guard self.viewModel.device?.uuid != nil else {
            print("<Bluetooth> 1SK BP setInitBluetoothManager uuid: nil")
            return
        }
        
        BluetoothManager.shared.delegate_BluetoothManager = self
        if let name = self.viewModel.device?.name {
            BluetoothManager.shared.listDeviceName = [name]
        } else {
            BluetoothManager.shared.listDeviceName = []
        }
        
        self.setScanPeripherals()
    }
    
    func setConnectPeripheral() {
        guard let peripheral = BluetoothManager.shared.peripheral else { return }
        BluetoothManager.shared.setConnectDevice(peripheral: peripheral)
    }
    
    func setDisconnectPeripheral() {
        if let peripheral = BluetoothManager.shared.peripheral {
            BluetoothManager.shared.setDisconnectDevice(peripheral: peripheral)
        } else {
            BluetoothManager.shared.setScanStopPeripherals()
        }
    }
    
    func setScanPeripherals() {
        BluetoothManager.shared.peripheral?.delegate = nil
        BluetoothManager.shared.peripheral = nil
        
        let state = BluetoothManager.shared.stateCBManager()
        if state == .poweredOn {
            BluetoothManager.shared.setScanStartPeripherals()
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                BluetoothManager.shared.setScanStartPeripherals()
            }
        }
    }
}

// MARK: - BluetoothManagerDelegate
extension BloodPressureViewController: BluetoothManagerDelegate {
    func onDidUpdateState(central: CBCentralManager) {
        print("<Bluetooth> 1SK BP onDidUpdateState central.state:", central.state.rawValue)
    }
    
    func onDidTimeoutScanPeripheralDevice() { }
    
    func onDidDiscover(peripheral: CBPeripheral) {
        guard let device: DeviceRealm = gRealm.objects().filter({$0.uuid == peripheral.uuid}).first else {
            return
        }
        guard BluetoothManager.shared.peripheral == nil else {
            return
        }
        print("<Bluetooth> 1SK BP onDidDiscover peripheral: \(device.name ?? "Null") - \(peripheral.name ?? "NULL") - \(peripheral.mac)")
        
        BluetoothManager.shared.setScanStopPeripherals()
        BluetoothManager.shared.peripheral = peripheral
        self.setConnectPeripheral()
    }
    
    func onDidConnected(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK BP onDidConnected peripheral.name:", peripheral.name ?? "NULL")
        self.setInitCBPeripheral(peripheral: peripheral)
        self.view_Header?.setConnectStatus(connected: true)
        self.viewModel.listDataMeasure.removeAll()
    }
    
    func onDidFailToConnect(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK BP onDidFailToConnect peripheral.name:", peripheral.name ?? "NULL")
        peripheral.delegate = nil
        self.setScanPeripherals()
        
        self.view_Header?.setConnectStatus(connected: false)
    }
    
    func onDidDisconnect(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK BP onDidDisconnect peripheral.name:", peripheral.name ?? "NULL")
        peripheral.delegate = nil
        self.setScanPeripherals()
        
        self.view_Header?.setConnectStatus(connected: false)
    }
}

// MARK: - CBPeripheralDelegate
extension BloodPressureViewController: CBPeripheralDelegate {
    func setInitCBPeripheral(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK BP setInitCBPeripheral")
        peripheral.delegate = self
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        peripheral.services?.forEach({ (service) in
            print("<Bluetooth> 1SK BP didDiscoverServices service:", service.debugDescription)
            peripheral.discoverCharacteristics(nil, for: service)
        })
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("<Bluetooth> 1SK BP didDiscoverCharacteristicsFor")
        
        guard error == nil else {
            print("<Bluetooth> 1SK BP didDiscoverCharacteristicsFor error: ", error.debugDescription)
            return
        }
        guard let characteristics = service.characteristics else {
            return
        }
        
        characteristics.forEach { characteristic in
            if characteristic.properties.contains(.read) {
                print("<Bluetooth> 1SK BP uuid .read: \(characteristic.uuid.uuidString)")
                peripheral.readValue(for: characteristic)
            }
            
            if characteristic.properties.contains(.write) {
                print("<Bluetooth> 1SK BP uuid .write: \(characteristic.uuid.uuidString)")
            }
            
            if characteristic.properties.contains(.writeWithoutResponse) {
                print("<Bluetooth> 1SK BP uuid .writeWithoutResponse: \(characteristic.uuid.uuidString)")
            }

            if characteristic.properties.contains(.notify) {
                print("<Bluetooth> 1SK BP uuid .notify: \(characteristic.uuid.uuidString)")
                peripheral.setNotifyValue(true, for: characteristic)
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            print("<Bluetooth> 1SK BP didUpdateValueFor error: ", error.debugDescription)
            return
        }
        guard let data = characteristic.value else {
            return
        }
        let listData = [UInt8](data)
        print("<Bluetooth> 1SK BP didUpdateValueFor characteristic.value: \(data) - \(listData)")
        
        guard characteristic.properties.contains(.notify) else {
            return
        }
        guard listData.count > 2 else {
            return
        }
        
        let checksum = listData.checkSumBp(to: listData.count - 2)
        print("didUpdateValueFor checksum:", checksum)
        
        // decode here
        var currentOffset = 7
        var length = 0
        
        var sys: UInt16 = 0
        length = MemoryLayout<UInt16>.size
        (data as NSData).getBytes(&sys, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        print("<Bluetooth> 1SK BP didUpdateValueFor Sys: \(CFSwapInt16BigToHost(sys)) mmhg")
        
        var dia: UInt16 = 0
        length = MemoryLayout<UInt16>.size
        (data as NSData).getBytes(&dia, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        print("<Bluetooth> 1SK BP didUpdateValueFor Dia: \(CFSwapInt16BigToHost(dia)) mmhg")
        
        var hr: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&hr, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        print("<Bluetooth> 1SK BP didUpdateValueFor Heart rate: \(hr) pul/min")
        
        var fibrillation: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&fibrillation, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        
        var year: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&year, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        
        var month: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&month, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        
        var day: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&day, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        
        var time: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&time, range: NSRange(location: currentOffset, length: length))
        currentOffset += length

        var branch: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&branch, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        
        let dateTime = "\(Int(year) + 2000)-\(month)-\(day) \(time):\(branch)"
        print("<Bluetooth> 1SK BP didUpdateValueFor Time: \(dateTime)")
        
        var group: UInt8 = 0
        length = MemoryLayout<UInt8>.size
        (data as NSData).getBytes(&group, range: NSRange(location: currentOffset, length: length))
        currentOffset += length
        print("<Bluetooth> 1SK BP didUpdateValueFor Group: \(group)")
        
        let sysValue = CFSwapInt16BigToHost(sys)
        let diaValue = CFSwapInt16BigToHost(dia)
        //let date = dateTime.toDate(.ymdhm)
        
        let bp = BloodPressureModel()
        bp.systolic = Int(sysValue)
        bp.diastolic = Int(diaValue)
        bp.heartRate = Int(hr)
        bp.group = Int(group)
        bp.date = Date()
        bp.dateTime = bp.date?.toTimestamp()
        bp.device = self.viewModel.device
        
        self.viewModel.onMeasureData(bp: bp)
        
        if self.viewModel.listDataMeasure.count == 1 {
            self.viewModel.isRefreshData = true
            self.router.presentMeasureBpResultRouter(device: self.viewModel.device, bp: self.viewModel.listDataMeasure.last, isMeasuring: true)
        }
    }
}

// MARK: CheckSum BloodPressure
extension Array where Element == UInt8 {
    func checkSumBp(from startIndex: Int = 0, to endIndex: Int) -> UInt8 {
        guard startIndex < endIndex else {
            fatalError("Start index must less than end index")
        }

        guard endIndex <= count else {
            fatalError("End index must less than array count")
        }
        var checksum: Int = 0
        for index in startIndex...endIndex {
            checksum += Int(self[index])
        }

        checksum += 3
        
        return UInt8(checksum % 256)
    }
}
