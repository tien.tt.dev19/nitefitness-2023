//
//  
//  BloodPressureInteractorInput.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol BloodPressureInteractorInputProtocol {
    func getDataBloodPressure(userId: Int, page: Int, perPage: Int, accessToken: String?)
    func getPostsBp(tag: String)
    func getSliderBanner(typeSlider: SliderType)
    func getConsultIndex(valueCode: String)
}

// MARK: - Interactor Output Protocol
protocol BloodPressureInteractorOutputProtocol: AnyObject {
    func onGetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>)
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>)
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>)
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>)
}

// MARK: - BloodPressure InteractorInput
class BloodPressureInteractorInput {
    weak var output: BloodPressureInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
    var connectService: ConnectServiceProtocol?
    var workPressService: WorkPressServiceProtocol?
}

// MARK: - BloodPressure InteractorInputProtocol
extension BloodPressureInteractorInput: BloodPressureInteractorInputProtocol {
    func getDataBloodPressure(userId: Int, page: Int, perPage: Int, accessToken: String?) {
        self.connectService?.getDataBloodPressureRecent(userId: userId, page: page, perPage: perPage, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetDataBloodPressureFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getPostsBp(tag: String) {
        self.workPressService?.getPostsBp(tag: tag, completion: { [weak self] result in
            self?.output?.onGetPostsScalesFinished(with: result.unwrapSuccessModelWP())
        })
    }
    
    func getSliderBanner(typeSlider: SliderType) {
        self.storeService?.getSliderBanner(typeSlider: typeSlider, completion: { [weak self] result in
            self?.output?.onGetSliderBannerFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getConsultIndex(valueCode: String) {
        self.connectService?.getConsultIndexBp(valueCode: valueCode, completion: { [weak self] result in
            self?.output?.onGetConsultIndexFinished(with: result.unwrapSuccessModel())
        })
    }
}
