//
//  
//  BloodPressureRouter.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol BloodPressureRouterProtocol {
    func presentPairingDeviceRouter(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?)
    func presentMeasureBpResultRouter(device: DeviceRealm?, bp: BloodPressureModel?, isMeasuring: Bool?)
    func presentMeasureBpGuideRouter()
    func presentAlertSettingBluetoothVC()
    func showHistoryMeasureBpRouter(device: DeviceRealm?)
    func showChatMessageRouter()
    func showWebKitRouter(title: String?, url: String?)
}

// MARK: - BloodPressure Router
class BloodPressureRouter {
    weak var viewController: BloodPressureViewController?
    
    static func setupModule(device: DeviceRealm?, delegate: BloodPressureViewDelegate?) -> BloodPressureViewController {
        let viewController = BloodPressureViewController()
        let router = BloodPressureRouter()
        let interactorInput = BloodPressureInteractorInput()
        let viewModel = BloodPressureViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        interactorInput.connectService = ConnectService()
        interactorInput.workPressService = WorkPressService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - BloodPressure RouterProtocol
extension BloodPressureRouter: BloodPressureRouterProtocol {
    func presentPairingDeviceRouter(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?) {
        let controller = PairingDeviceRouter.setupModule(device: device, delegate: delegate)
        self.viewController?.present(controller, animated: true)
    }
    
    func presentMeasureBpResultRouter(device: DeviceRealm?, bp: BloodPressureModel?, isMeasuring: Bool?) {
        let controller = MeasureBpResultRouter.setupModule(device: device, bp: bp, isMeasuring: isMeasuring)
        self.viewController?.present(controller, animated: true)
    }
    
    func presentMeasureBpGuideRouter() {
        let controller = MeasureBpGuideRouter.setupModule()
        self.viewController?.present(controller, animated: true)
    }
    
    func presentAlertSettingBluetoothVC() {
        let controller = AlertSettingBluetoothViewController()
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: false)
    }
    
    func showHistoryMeasureBpRouter(device: DeviceRealm?) {
        let controller = HistoryMeasureBpRouter.setupModule(device: device)
        self.viewController?.present(controller, animated: true)
    }
    
    func showChatMessageRouter() {
        let controller = ChatMessageRouter.setupModule()
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
    
    func showWebKitRouter(title: String?, url: String?) {
        let controller = WebKitRouter.setupModule(title: title, url: url)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
}
