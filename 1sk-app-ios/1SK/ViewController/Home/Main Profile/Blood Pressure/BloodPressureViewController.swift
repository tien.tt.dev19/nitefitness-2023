//
//  
//  BloodPressureViewController.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

protocol BloodPressureViewDelegate: AnyObject {
    func onConnectionAction(device: DeviceRealm?)
}

// MARK: StatesViewMeasureBp
enum StatesViewMeasureBp {
    case warning
    case measuring
    case result
}

// MARK: - ViewProtocol
protocol BloodPressureViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func setDataBpHearder()
    func setDataConsult()
}

// MARK: - BloodPressure ViewController
class BloodPressureViewController: BaseViewController {
    var router: BloodPressureRouterProtocol!
    var viewModel: BloodPressureViewModelProtocol!
    weak var delegate: BloodPressureViewDelegate?
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewBloodPressure?
    var isDevicePaired = false
    
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
        
        self.setLayoutHeaderTableView()
        self.setCheckDevicePaired()
        self.onReloadData()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUIApplication()
        self.setInitUITableView()
        self.setInitRefreshControl()
        self.setInitBluetoothState()
        self.setInitBluetoothManager()
        self.setLayoutHeaderTableView()
    }
    
    func setCheckDevicePaired() {
        if self.isDevicePaired {
            self.isDevicePaired = false
            self.setInitBluetoothManager()
        }
    }
    
    func setInitBluetoothState() {
        guard self.viewModel.device?.mac != nil else {
            return
        }
        guard let state = BluetoothManager.shared.stateCBManager(), state != .unknown, state != .poweredOn else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            switch BluetoothManager.shared.stateCBManager() {
            case .poweredOff:
                print("<Bluetooth> BloodPressure central.state is .poweredOff")
                self.router.presentAlertSettingBluetoothVC()
                
            case .resetting:
                print("<Bluetooth> BloodPressure central.state is .resetting")
                self.alertDefault(title: "Bluetooth", message: "Đang kết nối thiết bị")

            case .unsupported:
                print("<Bluetooth> BloodPressure central.state is .unsupported")
                self.alertDefault(title: "Bluetooth", message: "Thiết bị này không hỗ trợ")

            case .unauthorized:
                print("<Bluetooth> BloodPressure central.state is .unauthorized")
                self.alertOpenSettingsURLString(title: "\"1SK\" muốn sử dụng Bluetooth", message: "1SK cần cho phép truy cập Bluetooth để có thể tiếp tục với tính năng này, nó sẽ giúp bạn có thể kết nối với các thiết bị thông minh. Nếu đồng ý, bạn vui lòng đến Cài đặt và bấm cho phép")
                
            default:
                print("<Bluetooth> BloodPressure central.state is .unknown, .default")
            }
        }
    }
    
    deinit {
        self.setDisconnectPeripheral()
    }
}

// MARK: - BloodPressure ViewProtocol
extension BloodPressureViewController: BloodPressureViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func setDataBpHearder() {
        guard let bloodPressure = self.viewModel.listBloodPressure.first else {
            return
        }
        self.view_Header?.setDataRecentResults(bloodPressure: bloodPressure)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setDataConsult() {
        self.view_Header?.setUpdateDataConsult(consult: self.viewModel.consult)
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: - UIApplication
extension BloodPressureViewController {
    func setInitUIApplication() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppWillEnterForeground), name: .applicationWillEnterForeground, object: nil)
    }
    
    @objc func onAppWillEnterForeground() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.setInitBluetoothManager()
        }
    }
}

// MARK: Refresh Control
extension BloodPressureViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension BloodPressureViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerFooterNib(ofType: FooterSectionViewSmartScales.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewSmartScalesPost.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.viewModel.isPairedDevice(), let sliderBanner = self.viewModel.sliderBanner {
            let footer = tableView.dequeueHeaderView(ofType: FooterSectionViewSmartScales.self)
            footer.sliderBanner = sliderBanner
            return footer
            
        } else {
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.isPairedDevice() {
            return self.viewModel.listPostSuggest.count
            
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewSmartScalesPost.self, for: indexPath)
        cell.post = self.viewModel.listPostSuggest[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension BloodPressureViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let url = URL(string: self.viewModel.listPostSuggest[indexPath.row].link ?? "") else {
            return
        }
        UIApplication.shared.open(url)
    }
}

// MARK: UITableView Header
extension BloodPressureViewController: HeaderTableViewBloodPressureDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewBloodPressure.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setLayoutHeaderTableView() {
        self.view_Header?.setStateLayout(isPaired: self.viewModel.isPairedDevice())
        self.view_Header?.setImageDeviceType(code: self.viewModel.device?.code)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // Delegate
    func onConnectAction() {
        self.router.presentPairingDeviceRouter(device: self.viewModel.device, delegate: self)
    }
    
    func onGuideAction() {
        guard let config = gConfigs.first(where: {$0.key == "hdsd_may_do_huyet_ap_dbp_6277"}) else {
            return
        }
        self.router.showWebKitRouter(title: "Hướng dẫn đo", url: config.value)
    }
    
    func onHistoryAction() {
        self.router.showHistoryMeasureBpRouter(device: self.viewModel.device)
    }
    
    func onAdviseAction() {
        self.router.showChatMessageRouter()
    }
    
    func onDidFinishLoadWebView() {
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: PairingDeviceViewDelegate
extension BloodPressureViewController: PairingDeviceViewDelegate {
    func onConnectionAction(device: DeviceRealm) {
        guard let profile: ProfileRealm = gRealm.objects().first(where: {$0.type == .BP}) else {
            SKToast.shared.showToast(content: "Đang thiết lập hồ sơ")
            return
        }
        self.viewModel.device = device
        self.viewModel.device?.profile = profile
        self.isDevicePaired = true
        
        self.delegate?.onConnectionAction(device: device)
    }
}
