//
//  
//  BloodPressureViewModel.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol BloodPressureViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func onRefreshAction()
    func onMeasureData(bp: BloodPressureModel)
    func isPairedDevice() -> Bool
    
    var device: DeviceRealm? { get set }
    var consult: ConsultModel? { get set }
    var sliderBanner: SliderBannerModel? { get set }
    var listPostSuggest: [PostHealthModel] { get set }
    var isRefreshData: Bool { get set }
    
    var listBloodPressure: [BloodPressureModel] { get set }
    var listDataMeasure: [BloodPressureModel] { get set }
}

// MARK: - BloodPressure ViewModel
class BloodPressureViewModel {
    weak var view: BloodPressureViewProtocol?
    private var interactor: BloodPressureInteractorInputProtocol

    init(interactor: BloodPressureInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    
    var consult: ConsultModel?
    var sliderBanner: SliderBannerModel?
    var listPostSuggest: [PostHealthModel] = []
    
    var listBloodPressure: [BloodPressureModel] = []
    var listDataMeasure: [BloodPressureModel] = []
    
    var isRefreshData: Bool = false
}

// MARK: - BloodPressure ViewModelProtocol
extension BloodPressureViewModel: BloodPressureViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.getDataRecent()
        self.getSliderBanner()
    }
    
    func onViewDidAppear() {
        if self.isRefreshData {
            self.isRefreshData = false
            self.getDataRecent()
        }
    }
    
    func onRefreshAction() {
        self.getDataRecent()
    }
    
    func onMeasureData(bp: BloodPressureModel) {
        self.listDataMeasure.append(bp)
    }
    
    func isPairedDevice() -> Bool {
        if self.device?.uuid != nil {
            return true
            
        } else {
            return false
        }
    }
}

// MARK: - Handle
extension BloodPressureViewModel {
    func getDataRecent() {
        guard let userId = self.device?.profile?.id else {
            return
        }
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        
        self.interactor.getDataBloodPressure(userId: userId, page: 1, perPage: 1, accessToken: accessToken)
    }
    
    func getSliderBanner() {
        self.interactor.getSliderBanner(typeSlider: .Consult)
    }
    
    func getInitDataBp() {
        guard let bp = self.listBloodPressure.first, let index = bp.index else {
            return
        }
        self.interactor.getPostsBp(tag: index.status_VN ?? "")
        self.interactor.getConsultIndex(valueCode: index.status ?? "")
    }
}

// MARK: - BloodPressure InteractorOutputProtocol
extension BloodPressureViewModel: BloodPressureInteractorOutputProtocol {
    func onGetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>) {
        switch result {
        case .success(let listModel):
            self.listBloodPressure = listModel
            self.view?.setDataBpHearder()
            
            self.getInitDataBp()
            
        case .failure:
            break
        }
    }
    
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listPostSuggest = model
            self.view?.onReloadData()
            
        case .failure:
            break
        }
    }
    
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>) {
        switch result {
        case .success(let model):
            self.sliderBanner = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>) {
        switch result {
        case .success(let model):
            self.consult = model
            self.view?.setDataConsult()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
