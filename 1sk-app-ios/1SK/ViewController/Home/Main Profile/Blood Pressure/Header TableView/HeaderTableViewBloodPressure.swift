//
//  HeaderTableViewBloodPressure.swift
//  1SK
//
//  Created by Thaad on 29/09/2022.
//

import Foundation
import UIKit
import WebKit

protocol HeaderTableViewBloodPressureDelegate: AnyObject {
    func onConnectAction()
    func onGuideAction()
    
    func onHistoryAction()
    func onAdviseAction()
    
    func onDidFinishLoadWebView()
}

class HeaderTableViewBloodPressure: UIView {
    weak var delegate: HeaderTableViewBloodPressureDelegate?
    
    @IBOutlet weak var view_Connect: UIView!
    @IBOutlet weak var lbl_Connect: UILabel!
    
    @IBOutlet weak var img_Device: UIImageView!
    
    @IBOutlet weak var view_NotPair: UIStackView!
    @IBOutlet weak var view_Guide: UIView!
    
    @IBOutlet weak var btn_History: UIButton!
    
    @IBOutlet weak var lbl_Time: UILabel!
    
    @IBOutlet weak var lbl_ValueBp: UILabel!
    @IBOutlet weak var lbl_ValueHr: UILabel!
    
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var img_Status: UIImageView!
    
    @IBOutlet weak var view_History: UIView!
    @IBOutlet weak var view_Intro: UIView!
    
    @IBOutlet weak var view_Consult: UIView!
    @IBOutlet weak var lbl_Consult: UILabel!
    
    @IBOutlet weak var view_Advise: UIView!
    @IBOutlet weak var view_AdviseContent: UIView!
    
    @IBOutlet weak var web_Intro: WKWebView!
    @IBOutlet weak var constraint_height_IntroView: NSLayoutConstraint!
    
    private var isConnected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayout()
        self.setInitWKWebView()
    }
    
    func setInitLayout() {
        self.view_AdviseContent.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        
        self.view_Advise.isHidden = true
        self.view_Consult.isHidden = true
        self.btn_History.isHidden = true
    }
    
    func setStateLayout(isPaired: Bool) {
        if isPaired == true {
            self.view_NotPair.isHidden = true
            self.view_Guide.isHidden = false
            
            self.view_History.isHidden = false
            self.view_Intro.isHidden = true
            
            self.view_Connect.isHidden = false
            
        } else {
            self.view_NotPair.isHidden = false
            self.view_Guide.isHidden = true
            
            self.view_History.isHidden = true
            self.view_Intro.isHidden = false
            
            self.view_Connect.isHidden = true
        }
    }
    
    func setDataRecentResults(bloodPressure: BloodPressureModel) {
        self.lbl_Time.text = bloodPressure.date?.toString(.hmdmy)
        self.lbl_Status.text = bloodPressure.index?.status_VN
        //self.lbl_Status.textColor = bloodPressure.index?.color
        self.img_Status.image = bloodPressure.index?.icon
        
        if let sys = bloodPressure.systolic, let dia = bloodPressure.diastolic {
            self.lbl_ValueBp.text = "\(sys)/\(dia)"
            
        } else {
            self.lbl_ValueBp.text = "--/--"
        }
        
        if let hr = bloodPressure.heartRate {
            self.lbl_ValueHr.text = "\(hr)"
        } else {
            self.lbl_ValueHr.text = "--"
        }
        
        //self.view_Advise.isHidden = false
        self.btn_History.isHidden = false
    }
    
    func setImageDeviceType(code: String?) {
        switch code {
        case SKDevice.Code.DBP_6277B:
            self.img_Device.image = R.image.img_bp()
            if let config = gConfigs.first(where: {$0.key == "gt_huyet_ap_DBP_6277B"}) {
                self.setWebKitData(url: config.value)
            }
            
        case SKDevice.Code.DBP_6292B:
            self.img_Device.image = R.image.img_bp()
            if let config = gConfigs.first(where: {$0.key == "gt_huyet_ap_DBP_6292B"}) {
                self.setWebKitData(url: config.value)
            }
            
        default:
            self.img_Device.image = R.image.img_bp()
        }
    }
    
    func setUpdateDataConsult(consult: ConsultModel?) {
        if let content = consult?.content {
            self.lbl_Consult.text = content
            self.view_Consult.isHidden = false
        } else {
            self.view_Consult.isHidden = true
        }
    }
    
    // MARK: - Action
    @IBAction func onConnectAction(_ sender: Any) {
        self.delegate?.onConnectAction()
    }
    
    @IBAction func onSeeMoreAction(_ sender: Any) {
        guard let url = URL(string: PRODUCT_BP_URL) else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func onGuideAction(_ sender: Any) {
        self.delegate?.onGuideAction()
    }
    
    @IBAction func onHistoryAction(_ sender: Any) {
        self.delegate?.onHistoryAction()
    }
    
    @IBAction func onAdviseAction(_ sender: Any) {
        self.delegate?.onAdviseAction()
    }
}

// MARK: State Measure
extension HeaderTableViewBloodPressure {
    func setConnectStatus(connected: Bool) {
        if connected == true {
            self.view_Connect.backgroundColor = R.color.mainColor()
            self.lbl_Connect.text = "Đã kết nối"
            self.isConnected = true
            
        } else {
            self.view_Connect.backgroundColor = R.color.grayBgButton()
            self.lbl_Connect.text = "Chưa kết nối"
            self.isConnected = false
        }
    }
}

// MARK: - WKNavigationDelegate
extension HeaderTableViewBloodPressure: WKNavigationDelegate {
    func setInitWKWebView() {
        self.web_Intro.navigationDelegate = self
        
        self.web_Intro.scrollView.showsHorizontalScrollIndicator = false
        self.web_Intro.scrollView.pinchGestureRecognizer?.isEnabled = false
        self.web_Intro.scrollView.isScrollEnabled = false
    }
    
    func setWebKitData(url: String?) {
        guard let _url = URL(string: url ?? "") else {
            return
        }
        self.web_Intro.load(URLRequest(url: _url))
    }
    
    private func setHeightWebView() {
        self.web_Intro.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_Intro.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if let _height = height as? CGFloat {
                        print("web_WebView didFinish height: ", _height)
                        self.constraint_height_IntroView.constant = _height
                        self.delegate?.onDidFinishLoadWebView()
                    }
                })
            }
        })
    }
    
    // WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("web_WebView didFinish ")
        self.setHeightWebView()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            print("<webView> decidePolicyFor url:", url)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.setHeightWebView()
            }
        }
        
        decisionHandler(.allow)
    }
}
