//
//  CellTableViewBloodPressure.swift
//  1SK
//
//  Created by Thaad on 28/09/2022.
//

import UIKit

class CellTableViewBloodPressure: UITableViewCell {

    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var lbl_Bp: UILabel!
    @IBOutlet weak var lbl_Hr: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    
    var bloodPressure: BloodPressureModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.view_Content.addShadow(offSet: CGSize(width: 0, height: 2), color: .black, radius: 5, opacity: 0.1)
    }
    
    private func setDataUI() {
        self.lbl_Time.text = self.bloodPressure?.date?.toString(.hm)
        self.lbl_Date.text = self.bloodPressure?.date?.toString(.dmySlash)
        self.lbl_Status.text = self.bloodPressure?.index?.desc
        self.lbl_Status.textColor = self.bloodPressure?.index?.color
        
        if let sys = self.bloodPressure?.systolic, let dia = self.bloodPressure?.diastolic {
            self.lbl_Bp.text = "\(sys)/\(dia)"
            
        } else {
            self.lbl_Bp.text = "_ _/_ _"
        }
        
        if let hr = self.bloodPressure?.heartRate {
            self.lbl_Hr.text = "\(hr)"
        } else {
            self.lbl_Hr.text = "_ _"
        }
    }
}
