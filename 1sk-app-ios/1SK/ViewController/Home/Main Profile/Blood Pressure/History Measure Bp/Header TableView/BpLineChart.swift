//
//  BpLineChart.swift
//  1SK
//
//  Created by Thaad on 21/10/2022.
//

import Foundation
import UIKit
import Network

// MARK: PointEntry
class BpEntry: NSObject {
    var minSystolic: Int!
    var maxSystolic: Int!
    
    var minDiastolic: Int!
    var maxDiastolic: Int!
    
    var label: String?
    
    override init() {
        //
    }
    
    init(minSystolic: Int, maxSystolic: Int, minDiastolic: Int, maxDiastolic: Int, label: String) {
        self.minSystolic = minSystolic
        self.maxSystolic = maxSystolic
        
        self.minDiastolic = minDiastolic
        self.maxDiastolic = maxDiastolic
        
        self.label = label
    }
}

enum TypeBpEntry {
    case MIN_SYS
    case MAX_SYS
    case MIN_DIA
    case MAX_DIA
}

// MARK: BpLineChart
class BpLineChart: UIView {
    
    /// Color Line
    let colorChar = UIColor.gray
    let colorSys = #colorLiteral(red: 0.1882352941, green: 0.4941176471, blue: 0.9568627451, alpha: 1)
    let colorDia = #colorLiteral(red: 0.9529411765, green: 0.4117647059, blue: 0.4117647059, alpha: 1)
    
    /// gap between each point
    /// khoảng cách giữa mỗi điểm
    var lineGap: CGFloat = 60.0

    /// preseved space at top of the chart
    /// khoảng trống được chèn sẵn ở trên đầu biểu đồ
    let topSpace: CGFloat = 40.0

    /// preserved space at bottom of the chart to show labels along the Y axis
    /// không gian được bảo toàn ở cuối biểu đồ để hiển thị các nhãn dọc theo trục Y.
    let bottomSpace: CGFloat = 40.0
    
    /// preseeded space on the left side of the chart
    /// khoảng trống được chèn sẵn ở bên trái biểu đồ
    let leftSpace: CGFloat = 30.0

    /// The top most horizontal line in the chart will be 10% higher than the highest value in the chart
    /// Dòng nằm ngang trên cùng trong biểu đồ sẽ cao hơn 10% so với giá trị cao nhất trong biểu đồ
    let topHorizontalLine: CGFloat = 120.0 / 100.0

    /// Dot inner Radius
    /// Bán kính chấm bên trong
    var innerRadius: CGFloat = 6

    /// Dot outer Radius
    /// Bán kính chấm bên ngoài
    var outerRadius: CGFloat = 12

    /// Contains mainLayer and label for each data entry
    /// Chứa mainLayer và nhãn cho mỗi mục nhập dữ liệu
    private let scrollView: UIScrollView = UIScrollView()
    
    /// Contains dataLayer and gradientLayer
    /// Chứa dataLayer và gradientLayer
    private let mainLayer: CALayer = CALayer()

    /// Contains horizontal lines
    /// Chứa các đường ngang
    private let gridLayer: CALayer = CALayer()
    
    /// Contains the main line which represents the data
    /// Chứa dòng chính đại diện cho dữ liệu
    private let dataLayer: CALayer = CALayer()

    /// An array of CGPoint on dataLayer coordinate system that the main line will go through. These points will be calculated from dataEntries array
    /// Một mảng CGPoint trên hệ tọa độ dataLayer mà đường chính sẽ đi qua. Các điểm này sẽ được tính từ mảng dataEntries
    private var listMinSystolicPoint: [CGPoint]?
    private var listMinDiastolicPoint: [CGPoint]?
    private var listMaxSystolicPoint: [CGPoint]?
    private var listMaxDiastolicPoint: [CGPoint]?
    
    private var listVerticalGestureArea: [VerticalGestureArea] = []
    
    private var layerAlert: CALayer?
    private var layerArrow: CAShapeLayer?
    private var layerMaskArrow: CALayer?
    
    private var layerDotMinSys: CALayer?
    private var layerDotMaxSys: CALayer?
    private var layerDotMinDia: CALayer?
    private var layerDotMaxDia: CALayer?
    
    private var minValueY: Int?
    private var maxValueY: Int?
    
    private var isEmptyData = true
    
    var listDataEntries: [BpEntry]? {
        didSet {
            self.setNeedsLayout()
        }
    }
    
    // MARK: Init Chart
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }

    convenience init() {
        self.init(frame: CGRect.zero)
        self.setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }

    private func setupView() {
        //self.scrollView.backgroundColor = UIColor.lightGray
        //self.mainLayer.backgroundColor = UIColor.yellow.cgColor
        //self.gridLayer.backgroundColor = UIColor.purple.cgColor
        //self.dataLayer.backgroundColor = UIColor.orange.cgColor
        
        ////
        self.mainLayer.addSublayer(self.dataLayer)
        self.scrollView.layer.addSublayer(self.mainLayer)

        self.layer.addSublayer(self.gridLayer)
        self.addSubview(self.scrollView)
        //self.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.6941176471, blue: 0.1058823529, alpha: 1)
    }
    
    // MARK: Layout Subviews
    override func layoutSubviews() {
        self.setClearDrawDotSelected()
        self.setClearDrawAlert()
        self.checkEmptyData()
        
        self.scrollView.frame = CGRect(x: self.leftSpace, y: 0, width: self.frame.size.width - self.leftSpace, height: self.frame.size.height)
        self.scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapAction(recognizer:))))
        
        self.scrollView.isScrollEnabled = false
        self.lineGap = CGFloat(Int(self.scrollView.frame.size.width) / (self.listDataEntries?.count ?? 0))
        
        if let dataEntries = self.listDataEntries {
            // Set Data Min - Max
            let listMinData: [BpEntry] = dataEntries.filter({$0.minSystolic > 0 || $0.minDiastolic > 0})
            let listMaxData: [BpEntry] = dataEntries.filter({$0.maxSystolic > 0 || $0.maxDiastolic > 0})
            let minEntry = listMinData.min(by: { $0.minDiastolic < $1.minDiastolic})
            let maxEntry = listMaxData.max(by: { $0.maxSystolic < $1.maxSystolic })
            
            if let min = minEntry?.minDiastolic, let max = maxEntry?.maxSystolic {
                self.minValueY = min - 10
                self.maxValueY = max
                
            } else {
                self.minValueY = 0
                self.maxValueY = 100
            }
            
            // Set Layout
            let width = CGFloat(dataEntries.count) * self.lineGap
            let height = self.frame.size.height
            
            self.scrollView.contentSize = CGSize(width: width, height: height)
            self.mainLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)

            self.dataLayer.frame = CGRect(x: 0, y: self.topSpace, width: self.mainLayer.frame.width, height: self.mainLayer.frame.height - self.topSpace - self.bottomSpace)
            self.gridLayer.frame = CGRect(x: 0, y: self.topSpace, width: self.frame.width, height: self.mainLayer.frame.height - self.topSpace - self.bottomSpace)

            self.listMinSystolicPoint = self.setConvertEntriesToPoints(entries: dataEntries, type: .MIN_SYS)
            self.listMaxSystolicPoint = self.setConvertEntriesToPoints(entries: dataEntries, type: .MAX_SYS)
            self.listMinDiastolicPoint = self.setConvertEntriesToPoints(entries: dataEntries, type: .MIN_DIA)
            self.listMaxDiastolicPoint = self.setConvertEntriesToPoints(entries: dataEntries, type: .MAX_DIA)

            self.setCleanLayer()

            self.setDrawVertical()
            self.setDrawHorizontal()

            self.setDrawChart(points: self.listMinSystolicPoint, color: .clear, type: .MIN_SYS)
            self.setDrawChart(points: self.listMaxSystolicPoint, color: .clear, type: .MAX_SYS)
            self.setDrawChart(points: self.listMinDiastolicPoint, color: .clear, type: .MIN_DIA)
            self.setDrawChart(points: self.listMaxDiastolicPoint, color: .clear, type: .MAX_DIA)
            
//            self.setDrawChart(points: self.listMinSystolicPoint, color: .white, type: .MIN_SYS)
//            self.setDrawChart(points: self.listMaxSystolicPoint, color: .cyan, type: .MAX_SYS)
//            self.setDrawChart(points: self.listMinDiastolicPoint, color: .green, type: .MIN_DIA)
//            self.setDrawChart(points: self.listMaxDiastolicPoint, color: .purple, type: .MAX_DIA)

            self.setDrawLineBridge(minPoints: self.listMinDiastolicPoint, maxPoints: self.listMaxDiastolicPoint, color: self.colorDia)
            self.setDrawLineBridge(minPoints: self.listMinSystolicPoint, maxPoints: self.listMaxSystolicPoint, color: self.colorSys)
            
            self.setDrawDots(points: self.listMinSystolicPoint, type: .MIN_SYS)
            self.setDrawDots(points: self.listMaxSystolicPoint, type: .MAX_SYS)
            self.setDrawDots(points: self.listMinDiastolicPoint, type: .MIN_DIA)
            self.setDrawDots(points: self.listMaxDiastolicPoint, type: .MAX_DIA)
            
            //print("onTapAction points:", self.listMaxSystolicPoint!)
            
            self.setDrawEmptyLayer()
        }
    }
    
    // MARK: Check Empty Data
    private func checkEmptyData() {
        self.isEmptyData = true
        self.listDataEntries?.enumerated().forEach { (index, entry) in
            if entry.minSystolic > 0 {
                self.isEmptyData = false
            }
            if entry.maxSystolic > 0 {
                self.isEmptyData = false
            }
            if entry.minDiastolic > 0 {
                self.isEmptyData = false
            }
            if entry.maxDiastolic > 0 {
                self.isEmptyData = false
            }
        }
    }
    
    // MARK: Draw Empty Layer
    private func setDrawEmptyLayer() {
        if self.isEmptyData == true {
            let size = CGSize(width: self.mainLayer.frame.width, height: 20)
            let point = CGPoint(x: 0, y: (self.mainLayer.frame.height/2)-32)
            
            let textLayer = CATextLayer()
            textLayer.name = "EmptyLayer"
            textLayer.frame = CGRect(origin: point, size: size)
            textLayer.foregroundColor = self.colorChar.cgColor
            textLayer.backgroundColor = UIColor.clear.cgColor
            textLayer.alignmentMode = CATextLayerAlignmentMode.center
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
            textLayer.fontSize = 16
            textLayer.string = "Chưa có dữ liệu"
            self.mainLayer.addSublayer(textLayer)
        }
    }
    
    /**
     Convert an array of PointEntry to an array of CGPoint on dataLayer coordinate system
     Chuyển đổi một mảng PointEntry thành một mảng CGPoint trên hệ tọa độ dataLayer
     */
    // MARK: Convert Entries To Points
    private func setConvertEntriesToPoints(entries: [BpEntry], type: TypeBpEntry) -> [CGPoint] {
        guard let max = self.maxValueY else {
            return []
        }
        guard let min = self.minValueY else {
            return []
        }
        
        var result: [CGPoint] = []
        let minMaxRange: CGFloat = CGFloat(max - min) * self.topHorizontalLine

        entries.enumerated().forEach { (index, entry) in
            var bp: CGFloat = 0
            switch type {
            case .MIN_SYS:
                bp = CGFloat(entries[index].minSystolic ?? -1)
            case .MAX_SYS:
                bp = CGFloat(entries[index].maxSystolic ?? -1)
            case .MIN_DIA:
                bp = CGFloat(entries[index].minDiastolic ?? -1)
            case .MAX_DIA:
                bp = CGFloat(entries[index].maxDiastolic ?? -1)
            }
            
            if bp > 0 {
                let value = (CGFloat(bp) - CGFloat(min)) / minMaxRange
                let pointX = CGFloat(index)*self.lineGap + (40 - self.leftSpace)
                let pointY = self.dataLayer.frame.height * (1 - value)
                
                let point = CGPoint(x: pointX, y: pointY)
                result.append(point)
                
            } else {
                let point = CGPoint(x: -1, y: -1)
                result.append(point)
            }
        }
        
        return result
    }
    
    // MARK:  Clean Layer
    private func setCleanLayer() {
        self.mainLayer.sublayers?.forEach({
            if $0 is CATextLayer {
                $0.removeFromSuperlayer()
            }
        })
        self.dataLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
        self.gridLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
    }
    
    /**
     Create titles at the bottom for all entries showed in the chart
     Tạo tiêu đề ở dưới cùng cho tất cả các mục nhập được hiển thị trong biểu đồ
     */
    // MARK: Draw Vertical
    private func setDrawVertical() {
        guard let dataEntries = self.listDataEntries, dataEntries.count > 0 else {
            return
        }
        self.listVerticalGestureArea.removeAll()
        
        if self.isEmptyData == true {
            self.setDrawVerticalLineRoot()
        }
        
        dataEntries.enumerated().forEach { (index, entry) in
            let point = CGPoint(x: self.lineGap*CGFloat(index) - self.lineGap/2 + (40 - self.leftSpace),
                                y: self.mainLayer.frame.size.height - self.bottomSpace/2 - 8)
            
            // set Gesture Area
            let width = (self.dataLayer.frame.width / CGFloat(self.listDataEntries?.count ?? 0)) - 0.5
            self.listVerticalGestureArea.append(VerticalGestureArea(from: point.x, to: point.x + width))
            
            // Set Draw Vertical Line
            if dataEntries.count > 12 {
                if index.isEven {
                    if self.scrollView.isScrollEnabled == true {
                        self.setDrawVerticalLable(point: point, entry: entry, width: width)
                        self.setDrawVerticalLine(point: point, entry: entry, isEmptyData: isEmptyData)
                        
                    } else {
                        self.setDrawVerticalLable(point: point, entry: entry, width: width + (self.lineGap/4))
                        self.setDrawVerticalLine(point: point, entry: entry, isEmptyData: isEmptyData)
                    }
                    
                } else {
                    //self.setDrawVerticalLine(point: point, entry: entry)
                    
                    // Draw latest
                    /*
                    if index == dataEntries.count - 1 {
                        self.setDrawVerticalLable(point: point, entry: entry, width: width)
                        self.setDrawVerticalLine(point: point, entry: entry, isEmptyData: isEmptyData)
                    } else {
                        self.setDrawVerticalLine(point: point, entry: entry, isEmptyData: isEmptyData)
                    }
                    */
                }

            } else {
                self.setDrawVerticalLable(point: point, entry: entry, width: width)
                self.setDrawVerticalLine(point: point, entry: entry, isEmptyData: isEmptyData)
            }
        }
    }
    
    // MARK: Draw Vertical Line
    private func setDrawVerticalLine(point: CGPoint, entry: BpEntry, isEmptyData: Bool) {
        guard isEmptyData == false else {
            return
        }
        let path = UIBezierPath()
        path.move(to: CGPoint(x: point.x  + self.lineGap/2, y: -self.topSpace + 20))
        path.addLine(to: CGPoint(x: point.x + self.lineGap/2, y: self.dataLayer.frame.size.height))
        let lineLayer = CAShapeLayer()
        lineLayer.path = path.cgPath
        lineLayer.fillColor = UIColor.clear.cgColor
        lineLayer.strokeColor = self.colorChar.cgColor
        lineLayer.lineWidth = 0.5
        self.dataLayer.addSublayer(lineLayer)
    }
    
    // MARK: Draw Vertical Lable
    private func setDrawVerticalLable(point: CGPoint, entry: BpEntry, width: CGFloat) {
        let size = CGSize(width: width, height: 16)
        let pointText = CGPoint(x: point.x, y: point.y)
        
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(origin: pointText, size: size)
        textLayer.foregroundColor = self.colorChar.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 11
        textLayer.string = entry.label
        self.mainLayer.addSublayer(textLayer)
    }
    
    // MARK: Draw Vertical Line Root
    private func setDrawVerticalLineRoot() {
        let height = self.mainLayer.frame.size.height - self.bottomSpace - self.bottomSpace
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.leftSpace, y: -self.bottomSpace + 20))
        path.addLine(to: CGPoint(x: self.leftSpace, y: height))
        let lineLayer = CAShapeLayer()
        lineLayer.path = path.cgPath
        lineLayer.fillColor = UIColor.clear.cgColor
        lineLayer.strokeColor = self.colorChar.cgColor
        lineLayer.lineWidth = 0.5
        self.gridLayer.addSublayer(lineLayer)
    }

    /**
     Create horizontal lines (grid lines) and show the value of each line
     Tạo các đường ngang (đường lưới) và hiển thị giá trị của mỗi dòng
     */
    // MARK: Draw Horizontal
    private func setDrawHorizontal() {
        let gridValues: [CGFloat] = [0, 0.25, 0.5, 0.75, 1]
        //let gridValues: [CGFloat] = [0, 0.2, 0.4, 0.6, 0.8, 1]
        //let gridValues: [CGFloat] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
        
        gridValues.forEach({ value in
            let height = value * self.gridLayer.frame.size.height

            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: height))
            path.addLine(to: CGPoint(x: self.gridLayer.frame.size.width, y: height))

            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.fillColor = UIColor.clear.cgColor
            lineLayer.strokeColor = self.colorChar.cgColor
            lineLayer.lineWidth = 0.5
            if value < 1.0 {
                lineLayer.lineDashPattern = [4, 4]
                if self.isEmptyData {
                    //lineLayer.strokeColor = UIColor.clear.cgColor
                }
            }
            self.gridLayer.addSublayer(lineLayer)

            // Draw Lable Horizontal
            var minMaxGap: CGFloat = 0
            var lineValue: Int = 0
            if let max = self.maxValueY, let min = self.minValueY {
                minMaxGap = CGFloat(max - min) * self.topHorizontalLine
                lineValue = Int((1-value) * minMaxGap) + Int(min)
            }
            let textLayer = CATextLayer()
            textLayer.frame = CGRect(x: 4, y: height, width: 50, height: 16)
            textLayer.foregroundColor = self.colorChar.cgColor
            textLayer.backgroundColor = UIColor.clear.cgColor
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.alignmentMode = CATextLayerAlignmentMode.left
            textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
            textLayer.fontSize = 12
            textLayer.string = "\(lineValue)"
            self.gridLayer.addSublayer(textLayer)
        })
    }
    
    /**
     Draw a zigzag line connecting all points in dataPoints
     Vẽ một đường ngoằn ngoèo nối tất cả các điểm trong dataPoints
     */
    // MARK: Draw Chart
    private func setDrawChart(points: [CGPoint]?, color: UIColor, type: TypeBpEntry) {
        if let dataPoints = points, dataPoints.count > 0, let path = self.setCreatePath(for: points, type: type) {
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.strokeColor = color.cgColor
            lineLayer.fillColor = UIColor.clear.cgColor
            self.dataLayer.addSublayer(lineLayer)
        }
    }

    /**
     Create a zigzag bezier path that connects all points in dataPoints
     Tạo một đường viền ngoằn ngoèo kết nối tất cả các điểm trong dataPoints
     */
    // MARK: Create Path
    private func setCreatePath(for points: [CGPoint]?, type: TypeBpEntry) -> UIBezierPath? {
        guard points?.count ?? 0 > 0 else {
            return nil
        }
        let path = UIBezierPath()
        path.move(to: points![0])
        points!.enumerated().forEach { (index, point) in
            var value = 0
            switch type {
            case .MIN_SYS:
                value = self.listDataEntries?[index].minSystolic ?? 0
            case .MAX_SYS:
                value = self.listDataEntries?[index].maxSystolic ?? 0
            case .MIN_DIA:
                value = self.listDataEntries?[index].minDiastolic ?? 0
            case .MAX_DIA:
                value = self.listDataEntries?[index].maxDiastolic ?? 0
            }
            if value > 0 {
                path.addLine(to: point)
            }
        }
        return path
    }
    
    /**
     Draw the line between 2 points min & max
     Vẽ đường nối giữa 2 điểm min & max
     */
    // MARK: Draw Line Bridge
    private func setDrawLineBridge(minPoints: [CGPoint]?, maxPoints: [CGPoint]?, color: UIColor) {
        minPoints?.enumerated().forEach({ (index, minPoint) in
            if let maxPoint = maxPoints?[index] {
                if minPoint.x > -1 && minPoint.y > -1 && maxPoint.x > -1 && maxPoint.y > -1 {
                    let path = UIBezierPath()
                    path.move(to: CGPoint(x: minPoint.x, y: minPoint.y))
                    path.addLine(to: CGPoint(x: maxPoint.x, y: maxPoint.y))
                    let lineLayer = CAShapeLayer()
                    lineLayer.path = path.cgPath
                    lineLayer.fillColor = UIColor.clear.cgColor
                    lineLayer.strokeColor = color.cgColor
                    lineLayer.lineWidth = 1.5
                    self.dataLayer.addSublayer(lineLayer)
                }
            }
        })
    }
    
    /**
     Create Dots on line points
     Tạo dấu chấm trên các điểm dòng
     */
    // MARK: Draw Dots
    private func setDrawDots(points: [CGPoint]?, type: TypeBpEntry) {
        points?.enumerated().forEach({ (index, point) in
            var value = 0
            var colorDotInner = UIColor.white
            switch type {
            case .MIN_SYS:
                value = self.listDataEntries?[index].minSystolic ?? 0
                colorDotInner = self.colorSys
            case .MAX_SYS:
                value = self.listDataEntries?[index].maxSystolic ?? 0
                colorDotInner = self.colorSys
            case .MIN_DIA:
                value = self.listDataEntries?[index].minDiastolic ?? 0
                colorDotInner = self.colorDia
            case .MAX_DIA:
                value = self.listDataEntries?[index].maxDiastolic ?? 0
                colorDotInner = self.colorDia
            }
            
            if value > 0 {
                let xValue = point.x - (self.outerRadius/2)
                let yValue = point.y - (self.outerRadius/2)
                
                // drawDots
                let dotLayer = DotCALayer()
                dotLayer.dotInnerColor = colorDotInner
                dotLayer.innerRadius = self.innerRadius
                dotLayer.backgroundColor = UIColor.clear.cgColor
                dotLayer.cornerRadius = self.outerRadius / 2
                dotLayer.frame = CGRect(x: xValue, y: yValue, width: self.outerRadius, height: self.outerRadius)
                self.dataLayer.addSublayer(dotLayer)
            }
        })
    }
    
    private func setDrawDotSelected(type: TypeBpEntry, point: CGPoint?) {
        guard let _point = point, _point.x >= 0, _point.y >= 0 else {
            return
        }
        let x = _point.x - (self.outerRadius/2)
        let y = _point.y - (self.outerRadius/2)
        
        switch type {
        case .MIN_SYS:
            self.layerDotMinSys = CALayer()
            self.layerDotMinSys?.frame = CGRect(x: x, y: y, width: self.outerRadius, height: self.outerRadius)
            self.layerDotMinSys?.contents = UIImage(named: "ic_sys")?.cgImage
            self.dataLayer.addSublayer(self.layerDotMinSys!)
            
        case .MAX_SYS:
            self.layerDotMaxSys = CALayer()
            self.layerDotMaxSys?.frame = CGRect(x: x, y: y, width: self.outerRadius, height: self.outerRadius)
            self.layerDotMaxSys?.contents = UIImage(named: "ic_sys")?.cgImage
            self.dataLayer.addSublayer(self.layerDotMaxSys!)
            
        case .MIN_DIA:
            self.layerDotMinDia = CALayer()
            self.layerDotMinDia?.frame = CGRect(x: x, y: y, width: self.outerRadius, height: self.outerRadius)
            self.layerDotMinDia?.contents = UIImage(named: "ic_dia")?.cgImage
            self.dataLayer.addSublayer(self.layerDotMinDia!)
            
        case .MAX_DIA:
            self.layerDotMaxDia = CALayer()
            self.layerDotMaxDia?.frame = CGRect(x: x, y: y, width: self.outerRadius, height: self.outerRadius)
            self.layerDotMaxDia?.contents = UIImage(named: "ic_dia")?.cgImage
            self.dataLayer.addSublayer(self.layerDotMaxDia!)
        }
    }
}

extension BpLineChart {
    // MARK: Tap Action
    @objc func onTapAction(recognizer: UIGestureRecognizer) {
        let pointTap = recognizer.location(in: self.scrollView)
        //print("onTapAction pointTap:", pointTap)
        
        self.listVerticalGestureArea.enumerated().forEach { (index, area) in
            if pointTap.x >= area.from && pointTap.x <= area.to {
                //print("onTapAction point:", point)
                
                self.setDrawAlert(pointX: area.from, entry: self.listDataEntries?[index])
                self.setDrawDotSelected(index: index)
            }
        }
    }
    
    private func setClearDrawDotSelected() {
        let sublayers = self.dataLayer.sublayers
        sublayers?.forEach({ layer in
            switch layer {
            case self.layerDotMinSys:
                layer.removeFromSuperlayer()
                self.layerDotMinSys = nil

            case self.layerDotMaxSys:
                layer.removeFromSuperlayer()
                self.layerDotMaxSys = nil

            case self.layerDotMinDia:
                layer.removeFromSuperlayer()
                self.layerDotMinDia = nil

            case self.layerDotMaxDia:
                layer.removeFromSuperlayer()
                self.layerDotMaxDia = nil

            default:
                break
            }
        })
    }
    
    // MARK: Draw Dot Selected
    private func setDrawDotSelected(index: Int) {
        self.setClearDrawDotSelected()
        
        self.setDrawDotSelected(type: .MIN_SYS, point: self.listMinSystolicPoint?[index])
        self.setDrawDotSelected(type: .MAX_SYS, point: self.listMaxSystolicPoint?[index])
        self.setDrawDotSelected(type: .MIN_DIA, point: self.listMinDiastolicPoint?[index])
        self.setDrawDotSelected(type: .MAX_DIA, point: self.listMaxDiastolicPoint?[index])
    }
    
    private func setClearDrawAlert() { // Clear alertLayer old
        let sublayers = self.mainLayer.sublayers
        sublayers?.forEach({ layer in
            switch layer {
            case self.layerAlert:
                layer.removeFromSuperlayer()
                self.layerAlert = nil
                
            case self.layerArrow:
                layer.removeFromSuperlayer()
                self.layerArrow = nil
                
            case self.layerMaskArrow:
                layer.removeFromSuperlayer()
                self.layerMaskArrow = nil
                
            default:
                break
            }
        })
    }
    
    // MARK: Draw Alert
    private func setDrawAlert(pointX: CGFloat, entry: BpEntry?) {
        self.setClearDrawAlert()
        guard entry?.minSystolic ?? 0 > 0 || entry?.maxSystolic ?? 0 > 0 || entry?.minDiastolic ?? 0 > 0 || entry?.maxDiastolic ?? 0 > 0 else {
            return
        }
        
        // Draw alertLayer new
        self.layerAlert = CALayer()
        self.layerAlert?.backgroundColor = UIColor.white.cgColor
        self.layerAlert?.cornerRadius = 8
        self.layerAlert?.masksToBounds = false
        self.layerAlert?.shadowOffset = CGSize(width: 0, height: 2)
        self.layerAlert?.shadowColor = UIColor.black.cgColor
        self.layerAlert?.shadowRadius = 4
        self.layerAlert?.shadowOpacity = 0.3
        
        let padding: CGFloat = 5
        let width: CGFloat = 190
        let height: CGFloat = 30 + (padding*2)
        let originX: CGFloat = pointX + (self.lineGap/2) - (width/2)
        
        if originX < 0 {
            self.layerAlert?.frame = CGRect(x: 4, y: 5, width: width, height: height)
        } else if originX > self.mainLayer.frame.width - width - 5 {
            self.layerAlert?.frame = CGRect(x: self.mainLayer.frame.width - width - 5, y: 5, width: width, height: height)
        } else {
            self.layerAlert?.frame = CGRect(x: originX, y: 5, width: width, height: height)
        }
        
        // Draw icon sys
        let sysImageLayer = CALayer()
        sysImageLayer.frame = CGRect(x: padding, y: (self.layerAlert!.frame.size.height/3)-5, width: 10, height: 10)
        sysImageLayer.contents = UIImage(named: "ic_sys")?.cgImage
        self.layerAlert?.addSublayer(sysImageLayer)
        
        // Draw icon dia
        let diaImageLayer = CALayer()
        diaImageLayer.frame = CGRect(x: padding, y: ((self.layerAlert!.frame.size.height/3)*2)-4, width: 10, height: 10)
        diaImageLayer.contents = UIImage(named: "ic_dia")?.cgImage
        self.layerAlert?.addSublayer(diaImageLayer)
        
        // Draw text sys & dia
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: padding*4, y: padding,
                                 width: self.layerAlert!.frame.size.width - (padding*5),
                                 height: self.layerAlert!.frame.size.height - (padding*2))
        textLayer.foregroundColor = UIColor.black.cgColor
        textLayer.backgroundColor = UIColor.white.cgColor
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.contentsGravity = .center
        textLayer.alignmentMode = CATextLayerAlignmentMode.left
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 12
        
        let minSystolic = entry?.minSystolic ?? 0
        let maxSystolic = entry?.maxSystolic ?? 0
        let minDiastolic = entry?.minDiastolic ?? 0
        let maxDiastolic = entry?.maxDiastolic ?? 0
        
        var textSystolic = "Tâm thu:"
        if minSystolic == maxSystolic {
            textSystolic = "Tâm thu: \(maxSystolic) mmHg"
        } else {
            textSystolic = "Tâm thu: \(minSystolic) - \(maxSystolic) mmHg"
        }
        
        var textDiastolic = "Tâm trương:"
        if minDiastolic == maxDiastolic {
            textDiastolic = "Tâm trương: \(maxDiastolic) mmHg"
        } else {
            textDiastolic = "Tâm trương: \(minDiastolic) - \(maxDiastolic) mmHg"
        }
        
        textLayer.string = "\(textSystolic)\n\(textDiastolic)"
        
        self.layerAlert?.addSublayer(textLayer)
        
        // Add Alert Layer
        self.mainLayer.addSublayer(self.layerAlert!)
        
        // Draw arrow
        let path = UIBezierPath()
        path.move(to: CGPoint(x: pointX + (self.lineGap/2), y: 70))
        path.addLine(to: CGPoint(x: self.layerAlert!.frame.origin.x + (self.layerAlert!.frame.width/2) - 10,
                                 y: self.layerAlert!.frame.height + 4))
        path.addLine(to: CGPoint(x: self.layerAlert!.frame.origin.x + (self.layerAlert!.frame.width/2) + 10,
                                 y: self.layerAlert!.frame.height + 4))
        self.layerArrow = CAShapeLayer()
        self.layerArrow?.path = path.cgPath
        self.layerArrow?.fillColor = UIColor.white.cgColor
        self.layerArrow?.shadowOffset = CGSize(width: 0, height: 2)
        self.layerArrow?.shadowColor = UIColor.black.cgColor
        self.layerArrow?.shadowRadius = 4
        self.layerArrow?.shadowOpacity = 0.3
        //self.layerArrow!.strokeColor = UIColor.red.cgColor
        //self.layerArrow!.lineWidth = 1
        self.mainLayer.addSublayer(self.layerArrow!)
        
        // Draw Mask Arrow
        self.layerMaskArrow = CALayer()
        self.layerMaskArrow?.backgroundColor = UIColor.white.cgColor
        self.layerMaskArrow?.frame = CGRect(x: self.layerAlert!.frame.origin.x + (self.layerAlert!.frame.width/2) - 50 - 5,
                                            y: self.layerAlert!.frame.height - 1, width: 100, height: 6)
        self.mainLayer.addSublayer(self.layerMaskArrow!)
    }
}
