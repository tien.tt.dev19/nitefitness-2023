//
//  HeaderTableViewHistoryMeasureBp.swift
//  1SK
//
//  Created by Thaad on 20/12/2022.
//

import UIKit

protocol HeaderTableViewHistoryMeasureBpDelegate: AnyObject {
    func onSelectDeviceAction()
    func onDidMenuChange(typeTime: TimeType)
    func onPreAction(typeTime: TimeType)
    func onNextAction(typeTime: TimeType)
}

class HeaderTableViewHistoryMeasureBp: UITableViewHeaderFooterView {
    weak var delegate: HeaderTableViewHistoryMeasureBpDelegate?
    
    @IBOutlet weak var lbl_DeviceName: UILabel!
    
    @IBOutlet weak var btn_Day: UIButton!
    @IBOutlet weak var btn_Week: UIButton!
    @IBOutlet weak var btn_Month: UIButton!
    @IBOutlet weak var btn_Year: UIButton!
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var view_NoData: UIView!
    //@IBOutlet weak var lineChart: BpLineChart!
    
    
    private var typeTime: TimeType = .day
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayout()
        self.setStateUIMenuPage(typeTime: .day)
//        self.setInitLineChart()
    }
    
    func setInitLayout() {
        self.view_NoData.isHidden = true
    }
    
    func setUpdateScalesName(name: String?) {
        if let _name = name, _name.count > 0 {
            self.lbl_DeviceName.text = _name
        } else {
            self.lbl_DeviceName.text = "Tất cả"
        }
    }
    
    func setTimeUI(fromDate: Date?, toDate: Date?) {
        switch self.typeTime {
        case .day:
            let today = Date().toString(.dmySlash)
            let day = fromDate?.toString(.dmySlash)
            
            if day == today {
                self.lbl_Time.text = "Hôm nay"
            } else {
                self.lbl_Time.text = day
            }
            
        case .week:
            let fromTime = fromDate?.toString(.dmySlash)
            let toTime = toDate?.toString(.dmySlash)
            
            self.lbl_Time.text = "\(fromTime ?? "Null") - \(toTime ?? "Null")"
            
        case .month:
            let fromTime = fromDate?.toString(.dmySlash)
            let toTime = toDate?.toString(.dmySlash)
            
            self.lbl_Time.text = "\(fromTime ?? "Null") - \(toTime ?? "Null")"
            
        case .year:
            let fromTime = fromDate?.toString(.dmySlash)
            let toTime = toDate?.toString(.dmySlash)
            
            self.lbl_Time.text = "\(fromTime ?? "Null") - \(toTime ?? "Null")"
        }
    }
    
    func setLayoutNoData(count: Int) {
        if count > 0 {
            self.view_NoData.isHidden = true
        } else {
            self.view_NoData.isHidden = false
        }
    }
    
    // MARK: - Action
    @IBAction func onSelectDeviceAction(_ sender: Any) {
        self.delegate?.onSelectDeviceAction()
    }

    @IBAction func onDayAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .day)
    }

    @IBAction func onWeekAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .week)
    }

    @IBAction func onMonthAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .month)
    }

    @IBAction func onYearAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .year)
    }

    @IBAction func onPreAction(_ sender: Any) {
        self.delegate?.onPreAction(typeTime: self.typeTime)
    }

    @IBAction func onNextAction(_ sender: Any) {
        self.delegate?.onNextAction(typeTime: self.typeTime)
    }
    
    
}

// MARK: State Menu
extension HeaderTableViewHistoryMeasureBp {
    private func setStateUIMenuPage(typeTime: TimeType) {
        self.typeTime = typeTime
        
        switch typeTime {
        case .day:
            self.btn_Day.backgroundColor = R.color.subTitle()
            self.btn_Day.setTitleColor(.white, for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)
            
        case .week:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Week.backgroundColor = R.color.subTitle()
            self.btn_Week.setTitleColor(.white, for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)
            
        case .month:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.subTitle()
            self.btn_Month.setTitleColor(.white, for: .normal)
            
            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)
            
        case .year:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Year.backgroundColor = R.color.subTitle()
            self.btn_Year.setTitleColor(.white, for: .normal)
        }
        
        self.delegate?.onDidMenuChange(typeTime: typeTime)
    }
}

// MARK: Line Chart
//extension HeaderTableViewHistoryMeasureBp {
//    func setInitLineChart() {
//        let dataEntriesDay = [BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "00"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "01"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "02"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "03"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "04"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "05"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "06"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "07"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "08"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "09"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "10"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "11"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "12"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "13"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "14"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "15"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "16"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "17"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "18"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "19"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "20"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "21"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "22"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "23"),
//                              BpEntry(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "24")
//        ]
//        self.lineChart.listDataEntries = dataEntriesDay
//    }
//}
