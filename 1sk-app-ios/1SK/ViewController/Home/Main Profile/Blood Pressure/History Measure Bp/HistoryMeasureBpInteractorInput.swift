//
//  
//  HistoryMeasureBpInteractorInput.swift
//  1SK
//
//  Created by Thaad on 20/12/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HistoryMeasureBpInteractorInputProtocol {
    func getDataBloodPressure(deviceName: String?, userId: Int, fromDate: Int, toDate: Int, page: Int, perPage: Int, accessToken: String?)
    func getDataBloodPressureAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?)
    func getListBpName(accessToken: String?)
}

// MARK: - Interactor Output Protocol
protocol HistoryMeasureBpInteractorOutputProtocol: AnyObject {
    func onGetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>, page: Int, total: Int)
    func onGetDataBloodPressureAnalysisFinished(with result: Result<[BpAnalysisModel], APIError>, type: TimeType)
    func onGetListBpNameFinished(with result: Result<[String], APIError>)
}

// MARK: - HistoryMeasureBp InteractorInput
class HistoryMeasureBpInteractorInput {
    weak var output: HistoryMeasureBpInteractorOutputProtocol?
    var connectService: ConnectServiceProtocol?
}

// MARK: - HistoryMeasureBp InteractorInputProtocol
extension HistoryMeasureBpInteractorInput: HistoryMeasureBpInteractorInputProtocol {
    func getDataBloodPressure(deviceName: String?, userId: Int, fromDate: Int, toDate: Int, page: Int, perPage: Int, accessToken: String?) {
        self.connectService?.getDataBloodPressure(deviceName: deviceName, userId: userId, fromDate: fromDate, toDate: toDate, page: page, perPage: perPage, accessToken: accessToken, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetDataBloodPressureFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
    func getDataBloodPressureAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?) {
        self.connectService?.getDataBloodPressureAnalysis(deviceName: deviceName, type: type, time: time, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetDataBloodPressureAnalysisFinished(with: result.unwrapSuccessModel(), type: type)
        })
    }
    
    func getListBpName(accessToken: String?) {
        self.connectService?.getListBpName(accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetListBpNameFinished(with: result.unwrapSuccessModel())
        })
    }
}
