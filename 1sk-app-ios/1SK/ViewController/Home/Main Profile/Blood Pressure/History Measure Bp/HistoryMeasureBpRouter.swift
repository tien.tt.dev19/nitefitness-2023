//
//  
//  HistoryMeasureBpRouter.swift
//  1SK
//
//  Created by Thaad on 20/12/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HistoryMeasureBpRouterProtocol {
    func onDismissAction()
    func presentMeasureBpResultRouter(device: DeviceRealm?, bp: BloodPressureModel?, isMeasuring: Bool?)
}

// MARK: - HistoryMeasureBp Router
class HistoryMeasureBpRouter {
    weak var viewController: HistoryMeasureBpViewController?
    
    static func setupModule(device: DeviceRealm?) -> HistoryMeasureBpViewController {
        let viewController = HistoryMeasureBpViewController()
        let router = HistoryMeasureBpRouter()
        let interactorInput = HistoryMeasureBpInteractorInput()
        let viewModel = HistoryMeasureBpViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.connectService = ConnectService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HistoryMeasureBp RouterProtocol
extension HistoryMeasureBpRouter: HistoryMeasureBpRouterProtocol {
    func onDismissAction() {
        self.viewController?.dismiss(animated: true)
    }
    
    func presentMeasureBpResultRouter(device: DeviceRealm?, bp: BloodPressureModel?, isMeasuring: Bool?) {
        let controller = MeasureBpResultRouter.setupModule(device: device, bp: bp, isMeasuring: isMeasuring)
        self.viewController?.present(controller, animated: true)
    }
}
