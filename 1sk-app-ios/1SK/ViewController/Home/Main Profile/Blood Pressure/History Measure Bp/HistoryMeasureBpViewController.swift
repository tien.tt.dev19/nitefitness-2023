//
//  
//  HistoryMeasureBpViewController.swift
//  1SK
//
//  Created by Thaad on 20/12/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol HistoryMeasureBpViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onReloadDataChart(listBpEntry: [BpEntry])
    func setTimeUI(fromDate: Date?, toDate: Date?)
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func setLayoutNoData()
    
    func onShowAlertDeviceListView()
}

// MARK: - HistoryMeasureBp ViewController
class HistoryMeasureBpViewController: BaseViewController {
    var router: HistoryMeasureBpRouterProtocol!
    var viewModel: HistoryMeasureBpViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewHistoryMeasureBp?
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    private func setInitLayout() {
        self.view_Nav.addShadow(width: 0, height: 6, color: .black, radius: 3, opacity: 0.05)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismissAction()
    }
}

// MARK: - HistoryMeasureBp ViewProtocol
extension HistoryMeasureBpViewController: HistoryMeasureBpViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        self.view_Header?.setLayoutNoData(count: self.viewModel.listBloodPressure.count)
        self.tbv_TableView.updateFrameHeaderView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.tbv_TableView.updateFrameHeaderView()
        }
    }
    
    func onReloadDataChart(listBpEntry: [BpEntry]) {
//        guard listBpEntry.count > 0 else {
//            return
//        }
//        self.view_Header?.lineChart.listDataEntries = listBpEntry
    }
    
    func setTimeUI(fromDate: Date?, toDate: Date?) {
        self.view_Header?.setTimeUI(fromDate: fromDate, toDate: toDate)
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onShowAlertDeviceListView() {
        guard self.viewModel.listDeviceName.count > 0 else {
            SKToast.shared.showToast(content: "Không có thiết bị để chọn")
            return
        }
        self.presentAlertDeviceNameListView()
    }
    
    func setLayoutNoData() {
        self.view_Header?.setLayoutNoData(count: self.viewModel.listBloodPressure.count)
        self.tbv_TableView.updateFrameHeaderView()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.tbv_TableView.updateFrameHeaderView()
        }
    }
}

// MARK: Refresh Control
extension HistoryMeasureBpViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension HistoryMeasureBpViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewBloodPressure.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listBloodPressure.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewBloodPressure.self, for: indexPath)
        cell.bloodPressure = self.viewModel.listBloodPressure[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension HistoryMeasureBpViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let bp = self.viewModel.listBloodPressure[indexPath.row]
        self.router.presentMeasureBpResultRouter(device: nil, bp: bp, isMeasuring: false)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: - UITableView Header
extension HistoryMeasureBpViewController: HeaderTableViewHistoryMeasureBpDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewHistoryMeasureBp.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // HeaderTableViewHistoryMeasureBpDelegate
    func onSelectDeviceAction() {
        self.viewModel.onSelectDeviceAction()
    }
    
    func onDidMenuChange(typeTime: TimeType) {
        self.viewModel.typeTime = typeTime
    }

    func onPreAction(typeTime: TimeType) {
        self.viewModel.onPreAction(typeTime: typeTime)
    }

    func onNextAction(typeTime: TimeType) {
        self.viewModel.onNextAction(typeTime: typeTime)
    }
}

// MARK: AlertDeviceNameListViewDelegate
extension HistoryMeasureBpViewController: AlertDeviceNameListViewDelegate {
    private func presentAlertDeviceNameListView() {
        let controller = AlertDeviceNameListViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.titleAlert = "Chọn máy đo"
        controller.currentDeviceName = self.viewModel.currentDeviceName
        controller.listDeviceName = self.viewModel.listDeviceName
        self.present(controller, animated: false)
    }
    
    func onSelectedDevice(with deviceName: DeviceName) {
        self.view_Header?.setUpdateScalesName(name: deviceName.display)
        self.viewModel.onSelectedDevice(with: deviceName)
    }
}
