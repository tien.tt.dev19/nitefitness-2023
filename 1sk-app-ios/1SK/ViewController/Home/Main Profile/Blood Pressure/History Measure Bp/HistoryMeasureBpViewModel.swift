//
//  
//  HistoryMeasureBpViewModel.swift
//  1SK
//
//  Created by Thaad on 20/12/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol HistoryMeasureBpViewModelProtocol {
    func onViewDidLoad()
    func onRefreshAction()
    func onLoadMoreAction()
    func onMeasureData(bp: BloodPressureModel)
    
    func onSelectDeviceAction()
    func onSelectedDevice(with deviceName: DeviceName?)
    
    func onPreAction(typeTime: TimeType)
    func onNextAction(typeTime: TimeType)
    
    var device: DeviceRealm? { get set }
    var typeTime: TimeType { get set }
    
    var currentDeviceName: DeviceName? { get set }
    var listDeviceName: [DeviceName] { get set }
    
    var listBloodPressure: [BloodPressureModel] { get set }
    var listDataMeasure: [BloodPressureModel] { get set }
}

// MARK: - HistoryMeasureBp ViewModel
class HistoryMeasureBpViewModel {
    weak var view: HistoryMeasureBpViewProtocol?
    private var interactor: HistoryMeasureBpInteractorInputProtocol

    init(interactor: HistoryMeasureBpInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    
    var currentDeviceName: DeviceName? = DeviceName(id: 0, name: "", display: "Tất cả")
    var listDeviceName: [DeviceName] = []
    
    var typeTime: TimeType = .day {
        didSet {
            self.getData(page: 1)
        }
    }
    
    var listBloodPressure: [BloodPressureModel] = []
    var listDataMeasure: [BloodPressureModel] = []
    var listBpAnalysis: [BpAnalysisModel] = []
    var listBpEntry: [BpEntry] = []
    
    var timeFromDay: Date? = Date().startOfDay
    var timeToDay: Date? = Date().endOfDay
    
    var timeFromWeek: Date? = Date().startOfWeek
    var timeToWeek: Date? = Date().endOfWeek
    
    var timeFromMonth: Date? = Date().startOfMonth
    var timeToMonth: Date? = Date().endOfMonth
    
    var timeFromYear: Date? = Date().startOfYear
    var timeToYear: Date? = Date().endOfYear
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 20
    private var isOutOfData = false
}

// MARK: - HistoryMeasureBp ViewModelProtocol
extension HistoryMeasureBpViewModel: HistoryMeasureBpViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.getData(page: 1)
    }
    
    func onRefreshAction() {
        self.getData(page: 1)
    }
    
    func onLoadMoreAction() {
        if self.listBloodPressure.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)

        } else {
            self.view?.onLoadingSuccess()
        }
    }
    
    func onMeasureData(bp: BloodPressureModel) {
        self.listDataMeasure.append(bp)
    }
    
    func onSelectDeviceAction() {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.view?.showHud()
        self.interactor.getListBpName(accessToken: accessToken)
    }
    
    func onSelectedDevice(with deviceName: DeviceName?) {
        self.currentDeviceName = deviceName
        self.getData(page: 1)
    }
    
    func onPreAction(typeTime: TimeType) {
        switch typeTime {
        case .day:
            guard let timeFrom = self.timeFromDay else { return }
            self.timeFromDay = timeFrom.dayBefore.startOfDay
            self.timeToDay = self.timeFromDay?.endOfDay
            
            self.getData(page: 1)
            
        case .week:
            guard let timeFrom = self.timeFromWeek else { return }
            self.timeFromWeek = timeFrom.weekBefore.startOfDay
            self.timeToWeek = self.timeFromWeek?.endOfWeek.endOfDay
            
            self.getData(page: 1)
            
        case .month:
            guard let timeFrom = self.timeFromMonth else { return }
            self.timeFromMonth = timeFrom.monthBefore.startOfMonth.startOfDay
            self.timeToMonth = self.timeFromMonth?.endOfMonth.endOfDay
            
            self.getData(page: 1)
            
        case .year:
            guard let timeFrom = self.timeFromYear else { return }
            self.timeFromYear = timeFrom.yearBefore.startOfYear.startOfDay
            self.timeToYear = self.timeFromYear?.endOfYear.endOfDay
            
            self.getData(page: 1)
        }
    }
    
    func onNextAction(typeTime: TimeType) {
        switch typeTime {
        case .day:
            guard let timeFrom = self.timeFromDay else { return }
            let timeToday = Date().toString(.dmySlash)
            let timeCurrent = timeFrom.toString(.dmySlash)
            guard timeCurrent != timeToday else { return }
            
            self.timeFromDay = timeFrom.dayAfter.startOfDay
            self.timeToDay = self.timeFromDay?.endOfDay
            
            self.getData(page: 1)
            
        case .week:
            guard let timeFrom = self.timeFromWeek, let timeTo = self.timeToWeek else { return }
            let timeToday = Date().toTimestamp()
            let timeCurrent = timeTo.toTimestamp()
            guard timeCurrent < timeToday else { return }
            
            self.timeFromWeek = timeFrom.weekAfter.startOfDay
            self.timeToWeek = self.timeFromWeek?.endOfWeek.endOfDay
            
            self.getData(page: 1)
            
        case .month:
            guard let timeFrom = self.timeFromMonth, let timeTo = self.timeToMonth else { return }
            let timeToday = Date().toTimestamp()
            let timeCurrent = timeTo.toTimestamp()
            guard timeCurrent < timeToday else { return }
            
            self.timeFromMonth = timeFrom.monthAfter.startOfMonth.startOfDay
            self.timeToMonth = self.timeFromMonth?.endOfMonth.endOfDay
            
            self.getData(page: 1)
            
        case .year:
            guard let timeFrom = self.timeFromYear, let timeTo = self.timeToYear else { return }
            let timeToday = Date().toTimestamp()
            let timeCurrent = timeTo.toTimestamp()
            guard timeCurrent < timeToday else { return }
            
            self.timeFromYear = timeFrom.yearAfter.startOfYear.startOfDay
            self.timeToYear = self.timeFromYear?.endOfYear.endOfDay
            
            self.getData(page: 1)
        }
    }
}

// MARK: - Handle
extension HistoryMeasureBpViewModel {
    func getData(page: Int) {
        if page == 1 {
            self.isOutOfData = false
        }
        
        guard let userId = self.device?.profile?.id else {
            return
        }
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        
        switch self.typeTime {
        case .day:
            guard let timeFrom = self.timeFromDay?.toTimestamp(), let timeTo = self.timeToDay?.toTimestamp() else {
                return
            }
            
            print("getDataBloodPressure .day timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .day timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")
            
            if page == 1 {
                self.view?.showHud()
                self.view?.setTimeUI(fromDate: self.timeFromDay, toDate: self.timeToDay)
                //self.interactor.getDataBloodPressureAnalysis(deviceName: self.currentDeviceName?.name, type: .day, time: timeTo, accessToken: accessToken)
            }
            self.interactor.getDataBloodPressure(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: timeFrom, toDate: timeTo, page: page, perPage: self.perPage, accessToken: accessToken)
            
            
        case .week:
            guard let timeFrom = self.timeFromWeek?.toTimestamp(), let timeTo = self.timeToWeek?.toTimestamp() else {
                return
            }
            
            print("getDataBloodPressure .week timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .week timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")
            
            if page == 1 {
                self.view?.showHud()
                self.view?.setTimeUI(fromDate: self.timeFromWeek, toDate: self.timeToWeek)
                //self.interactor.getDataBloodPressureAnalysis(deviceName: self.currentDeviceName?.name, type: .week, time: timeTo, accessToken: accessToken)
            }
            self.interactor.getDataBloodPressure(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: timeFrom, toDate: timeTo, page: page, perPage: self.perPage, accessToken: accessToken)
            
        case .month:
            guard let timeFrom = self.timeFromMonth?.toTimestamp(), let timeTo = self.timeToMonth?.toTimestamp() else {
                return
            }
            
            print("getDataBloodPressure .month timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .month timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")
            
            if page == 1 {
                self.view?.showHud()
                self.view?.setTimeUI(fromDate: self.timeFromMonth, toDate: self.timeToMonth)
                //self.interactor.getDataBloodPressureAnalysis(deviceName: self.currentDeviceName?.name, type: .month, time: timeTo, accessToken: accessToken)
            }
            self.interactor.getDataBloodPressure(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: timeFrom, toDate: timeTo, page: page, perPage: self.perPage, accessToken: accessToken)
            
        case .year:
            guard let timeFrom = self.timeFromYear?.toTimestamp(), let timeTo = self.timeToYear?.toTimestamp() else {
                return
            }
            
            print("getDataBloodPressure .year timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .year timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")
            
            if page == 1 {
                self.view?.showHud()
                self.view?.setTimeUI(fromDate: self.timeFromYear, toDate: self.timeToYear)
                //self.interactor.getDataBloodPressureAnalysis(deviceName: self.currentDeviceName?.name, type: .year, time: timeTo, accessToken: accessToken)
            }
            self.interactor.getDataBloodPressure(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: timeFrom, toDate: timeTo, page: page, perPage: self.perPage, accessToken: accessToken)
        }
    }
    
    func setParseDataEntry(type: TimeType) {
        self.listBpEntry.removeAll()
        self.listBpAnalysis.forEach { bp in
            var lable = ""
            switch type {
            case .day:
                lable = bp.date?.toString(.h) ?? ""
            case .week:
                lable = bp.date?.toString(.eee) ?? ""
            case .month:
                lable = bp.date?.toString(.dd) ?? ""
            case .year:
                lable = bp.date?.toString(.MM) ?? ""
            }

            let entry = BpEntry.init(minSystolic: bp.minSystolic ?? 0, maxSystolic: bp.maxSystolic ?? 0, minDiastolic: bp.minDiastolic ?? 0, maxDiastolic: bp.maxDiastolic ?? 0, label: lable)
            self.listBpEntry.append(entry)
        }

        if type == .day {
            let entryLast = BpEntry.init(minSystolic: 0, maxSystolic: 0, minDiastolic: 0, maxDiastolic: 0, label: "24")
            self.listBpEntry.append(entryLast)
        }
        self.view?.onReloadDataChart(listBpEntry: self.listBpEntry)
    }
}

// MARK: - HistoryMeasureBp InteractorOutputProtocol
extension HistoryMeasureBpViewModel: HistoryMeasureBpInteractorOutputProtocol {
    func onGetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listBloodPressure = listModel
                self.view?.onReloadData()
                
            } else {
                for object in listModel {
                    self.listBloodPressure.append(object)
                    self.view?.onInsertRow(at: self.listBloodPressure.count - 1)
                }
            }
            
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listBloodPressure.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
            self.view?.setLayoutNoData()
        }
        
        self.view?.hideHud()
    }
    
    func onGetDataBloodPressureAnalysisFinished(with result: Result<[BpAnalysisModel], APIError>, type: TimeType) {
        switch result {
        case .success(let model):
            self.listBpAnalysis = model
            self.setParseDataEntry(type: type)
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.view?.hideHud()
    }
    
    func onGetListBpNameFinished(with result: Result<[String], APIError>) {
        switch result {
        case .success(let listModel):
            var listData: [DeviceName] = [DeviceName(id: 0, name: "", display: "Tất cả")]
            
            listModel.forEach { name in
                switch name {
                case SKDevice.Name.DBP_6292B:
                    let model = DeviceName(id: 1, name: name, display: SKDevice.NameDisplay.DBP_6292B)
                    listData.append(model)
                    
                case SKDevice.Name.DBP_6277B, "DBP-6277b":
                    if listData.first(where: {$0.name == SKDevice.Name.DBP_6277B}) == nil {
                        let model = DeviceName(id: 2, name: name, display: SKDevice.NameDisplay.DBP_6277B)
                        listData.append(model)
                    }
                    
                default:
                    break
                }
            }
            
            self.listDeviceName = listData.sorted(by: {$0.id ?? 0 < $1.id ?? 0})
            self.view?.onShowAlertDeviceListView()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
