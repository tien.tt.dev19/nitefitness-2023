//
//  
//  MeasureBpGuideInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasureBpGuideInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int)
    func set_DataNameFunction(param1: String, param2: Int)
}

// MARK: - Interactor Output Protocol
protocol MeasureBpGuideInteractorOutputProtocol: AnyObject {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - MeasureBpGuide InteractorInput
class MeasureBpGuideInteractorInput {
    weak var output: MeasureBpGuideInteractorOutputProtocol?
}

// MARK: - MeasureBpGuide InteractorInputProtocol
extension MeasureBpGuideInteractorInput: MeasureBpGuideInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
    
    func set_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
}
