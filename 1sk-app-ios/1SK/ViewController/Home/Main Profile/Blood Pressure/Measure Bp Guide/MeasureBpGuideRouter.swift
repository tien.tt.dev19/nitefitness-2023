//
//  
//  MeasureBpGuideRouter.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasureBpGuideRouterProtocol {

}

// MARK: - MeasureBpGuide Router
class MeasureBpGuideRouter {
    weak var viewController: MeasureBpGuideViewController?
    
    static func setupModule() -> MeasureBpGuideViewController {
        let viewController = MeasureBpGuideViewController()
        let router = MeasureBpGuideRouter()
        let interactorInput = MeasureBpGuideInteractorInput()
        let viewModel = MeasureBpGuideViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MeasureBpGuide RouterProtocol
extension MeasureBpGuideRouter: MeasureBpGuideRouterProtocol {
    
}
