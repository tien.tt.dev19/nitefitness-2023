//
//  
//  MeasureBpGuideViewController.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol MeasureBpGuideViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
}

// MARK: - MeasureBpGuide ViewController
class MeasureBpGuideViewController: BaseViewController {
    var router: MeasureBpGuideRouterProtocol!
    var viewModel: MeasureBpGuideViewModelProtocol!
    
    @IBOutlet weak var view_Container: UIView!
    
    @IBOutlet weak var view_Step: UIView!
    @IBOutlet weak var lbl_Step: UILabel!
    @IBOutlet weak var btn_Back: UIButton!
    @IBOutlet weak var btn_Next: UIButton!
    
    var currenStep = 0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setStepStateUI(step: 1)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func onBackAction(_ sender: Any) {
        switch self.currenStep {
        case 2:
            self.setStepStateUI(step: 1)
            
        case 3:
            self.setStepStateUI(step: 2)
            
        default:
            break
        }
    }
    
    @IBAction func onNextAction(_ sender: Any) {
        switch self.currenStep {
        case 1:
            self.setStepStateUI(step: 2)
            
        case 2:
            self.setStepStateUI(step: 3)
            
        case 3:
            self.setStepStateUI(step: 4)
            
        default:
            break
        }
    }
    
}

// MARK: - MeasureBpGuide ViewProtocol
extension MeasureBpGuideViewController: MeasureBpGuideViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - Container View
extension MeasureBpGuideViewController {
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
    
    func setStepStateUI(step: Int) {
        self.currenStep = step
        
        switch step {
        case 1:
            self.btn_Back.isHidden = true
            self.btn_Next.isHidden = false
            self.lbl_Step.text = "Bước 1"
            
            let controller = StepOneViewController()
            self.addChildView(childViewController: controller)
            
        case 2:
            self.btn_Back.isHidden = false
            self.btn_Next.isHidden = false
            self.lbl_Step.text = "Bước 2"
            
            let controller = StepTwoViewController()
            self.addChildView(childViewController: controller)
            
        case 3:
            self.btn_Back.isHidden = false
            self.btn_Next.isHidden = false
            self.lbl_Step.text = "Bước 3"
            
            let controller = StepThreeViewController()
            self.addChildView(childViewController: controller)
            
        case 4:
            self.view_Step.isHidden = true
            
            let controller = StepFourViewController()
            self.addChildView(childViewController: controller)
            
        default:
            break
        }
    }
}
