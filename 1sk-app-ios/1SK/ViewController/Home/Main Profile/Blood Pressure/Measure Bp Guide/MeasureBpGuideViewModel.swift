//
//  
//  MeasureBpGuideViewModel.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MeasureBpGuideViewModelProtocol {
    func onViewDidLoad()
    
    //var variable: Int? { get set }
    //var listVariable: [Int] { get set }
}

// MARK: - MeasureBpGuide ViewModel
class MeasureBpGuideViewModel {
    weak var view: MeasureBpGuideViewProtocol?
    private var interactor: MeasureBpGuideInteractorInputProtocol

    init(interactor: MeasureBpGuideInteractorInputProtocol) {
        self.interactor = interactor
    }

    //var variable: Int?
    //var listVariable: [Int] = []
}

// MARK: - MeasureBpGuide ViewModelProtocol
extension MeasureBpGuideViewModel: MeasureBpGuideViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - MeasureBpGuide InteractorOutputProtocol
extension MeasureBpGuideViewModel: MeasureBpGuideInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
