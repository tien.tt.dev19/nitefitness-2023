//
//  HeaderTableViewMeasureBpResult.swift
//  1SK
//
//  Created by Thaad on 22/12/2022.
//

import UIKit

protocol HeaderTableViewMeasureBpResultDelegate: AnyObject {
    func onGuideAction()
}

class HeaderTableViewMeasureBpResult: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Bp: UILabel!
    @IBOutlet weak var lbl_Hr: UILabel!
    
    @IBOutlet weak var view_Status: UIView!
    @IBOutlet weak var lbl_Status: UILabel!
    
    @IBOutlet weak var view_Consult: UIView!
    @IBOutlet weak var lbl_Consult: UILabel!
    
    @IBOutlet weak var view_Advise: UIView!
    @IBOutlet weak var view_AdviseContent: UIView!
    
    weak var delegate: HeaderTableViewMeasureBpResultDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayout()
    }
    
    func setInitLayout() {
        self.view_AdviseContent.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        
        self.view_Advise.isHidden = true
        self.view_Consult.isHidden = true
    }
    
    func setLayoutBp(bp: BloodPressureModel?) {
        self.lbl_Time.text = bp?.date?.toString(.hmdmy)
        
        if let sys = bp?.systolic, let dia = bp?.diastolic {
            self.lbl_Bp.text = "\(sys)/\(dia)"
            
        } else {
            self.lbl_Bp.text = "_ _/_ _"
        }
        
        if let hr = bp?.heartRate {
            self.lbl_Hr.text = "\(hr)"
        } else {
            self.lbl_Hr.text = "_ _"
        }
        
        self.lbl_Status.text = bp?.index?.status_VN
    }
    
    func setLayoutConsult(consult: ConsultModel?) {
        if let content = consult?.content {
            self.lbl_Consult.text = content
            self.view_Consult.isHidden = false
        } else {
            self.view_Consult.isHidden = true
        }
    }
    
    @IBAction func onGuideAction(_ sender: Any) {
        self.delegate?.onGuideAction()
    }
    
}
