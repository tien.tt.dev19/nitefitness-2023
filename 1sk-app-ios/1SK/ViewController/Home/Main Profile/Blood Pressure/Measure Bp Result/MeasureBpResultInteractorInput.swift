//
//  
//  MeasureBpResultInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasureBpResultInteractorInputProtocol {
    func setDataBloodPressure(listBloodPressure: [BloodPressureModel], accessToken: String?)
    func getPostsBp(tag: String)
    func getSliderBanner(typeSlider: SliderType)
    func getConsultIndex(valueCode: String)
}

// MARK: - Interactor Output Protocol
protocol MeasureBpResultInteractorOutputProtocol: AnyObject {
    func onSetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>)
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>)
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>)
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>)
}

// MARK: - MeasureBpResult InteractorInput
class MeasureBpResultInteractorInput {
    weak var output: MeasureBpResultInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
    var connectService: ConnectServiceProtocol?
    var workPressService: WorkPressServiceProtocol?
}

// MARK: - MeasureBpResult InteractorInputProtocol
extension MeasureBpResultInteractorInput: MeasureBpResultInteractorInputProtocol {
    func setDataBloodPressure(listBloodPressure: [BloodPressureModel], accessToken: String?) {
        self.connectService?.setDataBloodPressure(listBloodPressure: listBloodPressure, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onSetDataBloodPressureFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getPostsBp(tag: String) {
        self.workPressService?.getPostsBp(tag: tag, completion: { [weak self] result in
            self?.output?.onGetPostsScalesFinished(with: result.unwrapSuccessModelWP())
        })
    }
    
    func getSliderBanner(typeSlider: SliderType) {
        self.storeService?.getSliderBanner(typeSlider: typeSlider, completion: { [weak self] result in
            self?.output?.onGetSliderBannerFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getConsultIndex(valueCode: String) {
        self.connectService?.getConsultIndexBp(valueCode: valueCode, completion: { [weak self] result in
            self?.output?.onGetConsultIndexFinished(with: result.unwrapSuccessModel())
        })
    }
}
