//
//  
//  MeasureBpResultRouter.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasureBpResultRouterProtocol {
    func onDismiss()
    func showWebKitRouter(title: String?, url: String?)
}

// MARK: - MeasureBpResult Router
class MeasureBpResultRouter {
    weak var viewController: MeasureBpResultViewController?
    
    static func setupModule(device: DeviceRealm?, bp: BloodPressureModel?, isMeasuring: Bool?) -> MeasureBpResultViewController {
        let viewController = MeasureBpResultViewController()
        let router = MeasureBpResultRouter()
        let interactorInput = MeasureBpResultInteractorInput()
        let viewModel = MeasureBpResultViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.device = device
        viewModel.bloodPressure = bp
        viewModel.isMeasuring = isMeasuring
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        interactorInput.connectService = ConnectService()
        interactorInput.workPressService = WorkPressService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MeasureBpResult RouterProtocol
extension MeasureBpResultRouter: MeasureBpResultRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
    func showWebKitRouter(title: String?, url: String?) {
        let controller = WebKitRouter.setupModule(title: title, url: url)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
}
