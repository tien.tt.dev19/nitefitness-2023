//
//  
//  MeasureBpResultViewController.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol MeasureBpResultViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func setDataLayout(bp: BloodPressureModel?)
    func setDataConsult()
}

// MARK: - MeasureBpResult ViewController
class MeasureBpResultViewController: BaseViewController {
    var router: MeasureBpResultRouterProtocol!
    var viewModel: MeasureBpResultViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewMeasureBpResult?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismiss()
    }
    
}

// MARK: - MeasureBpResult ViewProtocol
extension MeasureBpResultViewController: MeasureBpResultViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setDataLayout(bp: BloodPressureModel?) {
        self.view_Header?.setLayoutBp(bp: bp)
    }
    
    func setDataConsult() {
        self.view_Header?.setLayoutConsult(consult: self.viewModel.consult)
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: - UITableViewDataSource
extension MeasureBpResultViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerFooterNib(ofType: FooterSectionViewSmartScales.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewSmartScalesPost.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let sliderBanner = self.viewModel.sliderBanner {
            let footer = tableView.dequeueHeaderView(ofType: FooterSectionViewSmartScales.self)
            footer.sliderBanner = sliderBanner
            return footer
            
        } else {
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listPostSuggest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewSmartScalesPost.self, for: indexPath)
        cell.post = self.viewModel.listPostSuggest[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MeasureBpResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard let url = URL(string: self.viewModel.listPostSuggest[indexPath.row].link ?? "") else {
            return
        }
        UIApplication.shared.open(url)
    }
}

// MARK: UITableView Header
extension MeasureBpResultViewController: HeaderTableViewMeasureBpResultDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewMeasureBpResult.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // HeaderTableViewMeasureBpResultDelegate
    func onGuideAction() {
        guard let config = gConfigs.first(where: {$0.key == "hd_doc_ket_qua_do_huyet_ap"}) else {
            return
        }
        self.router.showWebKitRouter(title: "Hướng dẫn đọc kết quả", url: config.value)
    }
}
