//
//  
//  MeasureBpResultViewModel.swift
//  1SK
//
//  Created by Thaad on 13/10/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MeasureBpResultViewModelProtocol {
    func onViewDidLoad()
    
    var device: DeviceRealm? { get set }
    var bloodPressure: BloodPressureModel? { get set }
    
    var consult: ConsultModel? { get set }
    var sliderBanner: SliderBannerModel? { get set }
    var listPostSuggest: [PostHealthModel] { get set }
}

// MARK: - MeasureBpResult ViewModel
class MeasureBpResultViewModel {
    weak var view: MeasureBpResultViewProtocol?
    private var interactor: MeasureBpResultInteractorInputProtocol

    init(interactor: MeasureBpResultInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    var bloodPressure: BloodPressureModel?
    
    var isMeasuring: Bool?
    
    var consult: ConsultModel?
    var sliderBanner: SliderBannerModel?
    var listPostSuggest: [PostHealthModel] = []
    
}

// MARK: - MeasureBpResult ViewModelProtocol
extension MeasureBpResultViewModel: MeasureBpResultViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.setInitData()
        self.getSliderBanner()
        self.getInitDataBp()
    }
}

// MARK: - Handle
extension MeasureBpResultViewModel {
    func setInitData() {
        if self.isMeasuring == true {
            if let sys = self.bloodPressure?.systolic, let dia = self.bloodPressure?.diastolic {
                let status = BloodPressure.init(sys: sys, dia: dia)
                self.bloodPressure?.index = status
                
                self.view?.setDataLayout(bp: self.bloodPressure)
                
                guard let accessToken = self.device?.profile?.accessToken else {
                    return
                }
                guard let bpData = self.bloodPressure else {
                    return
                }
                self.view?.showHud()
                self.interactor.setDataBloodPressure(listBloodPressure: [bpData], accessToken: accessToken)
            }
            
        } else {
            self.view?.setDataLayout(bp: self.bloodPressure)
        }
    }
    
    func getSliderBanner() {
        self.interactor.getSliderBanner(typeSlider: .Consult)
    }
    
    func getInitDataBp() {
        guard let index = self.bloodPressure?.index else {
            return
        }
        self.interactor.getPostsBp(tag: index.status_VN ?? "")
        self.interactor.getConsultIndex(valueCode: index.status ?? "")
    }
}

// MARK: - MeasureBpResult InteractorOutputProtocol
extension MeasureBpResultViewModel: MeasureBpResultInteractorOutputProtocol {
    func onSetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>) {
        switch result {
        case .success:
            break
            
        case .failure(let error):
            debugPrint(error)
        }
        
        self.view?.hideHud()
    }
    
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listPostSuggest = model
            self.view?.onReloadData()
            
        case .failure:
            break
        }
    }
    
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>) {
        switch result {
        case .success(let model):
            self.sliderBanner = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>) {
        switch result {
        case .success(let model):
            self.consult = model
            self.view?.setDataConsult()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
