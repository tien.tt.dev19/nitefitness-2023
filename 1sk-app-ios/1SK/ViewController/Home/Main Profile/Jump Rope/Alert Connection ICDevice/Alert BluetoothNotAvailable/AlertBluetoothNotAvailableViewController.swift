//
//  AlertBluetoothSetupViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/06/2022.
//

import UIKit

protocol AlertBluetoothNotAvailableViewDelegate: AnyObject {
    func onCancelNotAvailableAction()
}

class AlertBluetoothNotAvailableViewController: UIViewController {

    weak var delegate: AlertBluetoothNotAvailableViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onCancelNotAvailableAction()
    }
    
    @IBAction func onSetupAction(_ sender: Any) {
        self.setOpenSettingsBluetooth()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.delegate?.onCancelNotAvailableAction()
        }
    }
}
