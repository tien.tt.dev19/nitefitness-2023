//
//  DeviceConnectFailedViewController.swift
//  1SKConnect
//
//  Created by TrungDN on 09/06/2022.
//

import UIKit

protocol AlertDeviceConnectFailedViewDelegate: AnyObject {
    func onCancelConnectFailedAction()
    func onUnpairConnectFailedAction()
}

class AlertDeviceConnectFailedViewController: UIViewController {
    
    weak var delegate: AlertDeviceConnectFailedViewDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func onCancelAction(_ sender: UIButton) {
        self.delegate?.onCancelConnectFailedAction()
    }
    
    @IBAction func onUnpairAction(_ sender: UIButton) {
        self.delegate?.onUnpairConnectFailedAction()
    }
}
