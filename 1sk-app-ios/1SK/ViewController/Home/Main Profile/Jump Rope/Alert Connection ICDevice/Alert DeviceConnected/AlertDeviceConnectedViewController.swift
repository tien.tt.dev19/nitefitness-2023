//
//  AlertDeviceConnectedViewController.swift
//  1SKConnect
//
//  Created by Thaad on 05/08/2022.
//

import UIKit

protocol AlertDeviceConnectedViewDelegate: AnyObject {
    func onDismissConnectedAction()
}

class AlertDeviceConnectedViewController: UIViewController {
    @IBOutlet weak var view_Image: UIImageView!
    
    weak var delegate: AlertDeviceConnectedViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setImageGifDataUI()
    }
    
    func setImageGifDataUI() {
        self.view_Image.image = UIImage.gifImageWithName("bleConnecting")
    }
    
    @IBAction func onDoneAction(_ sender: Any) {
        self.delegate?.onDismissConnectedAction()
    }
    
}
