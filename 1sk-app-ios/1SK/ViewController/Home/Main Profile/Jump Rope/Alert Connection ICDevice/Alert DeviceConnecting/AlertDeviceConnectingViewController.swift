//
//  AlertDeviceConnectingViewController.swift
//  1SKConnect
//
//  Created by Thaad on 05/08/2022.
//

import UIKit

protocol AlertDeviceConnectingViewDelegate: AnyObject {
    func onCancelConnectingAction()
}

class AlertDeviceConnectingViewController: UIViewController {

    @IBOutlet weak var view_Image: UIImageView!
    
    weak var delegate: AlertDeviceConnectingViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setImageGifDataUI()
    }
    
    func setImageGifDataUI() {
        self.view_Image.image = UIImage.gifImageWithName("bleConnecting")
    }
    
    // MARK: Action
    @IBAction func onCancelAction(_ sender: UIButton) {
        self.delegate?.onCancelConnectingAction()
    }
}
