//
//  AlertDeviceNotConnectedViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/06/2022.
//

import UIKit

protocol AlertDeviceDisconnectViewDelegate: AnyObject {
    func onCancelDisconnectAction()
    func onRetryConnectionAction()
}

class AlertDeviceDisconnectViewController: UIViewController {

    weak var delegate: AlertDeviceDisconnectViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onCancelDisconnectAction()
    }
    
    @IBAction func onRetryAction(_ sender: Any) {
        self.delegate?.onRetryConnectionAction()
    }

}
