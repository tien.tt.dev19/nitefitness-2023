//
//  AlertDeviceFindingViewController.swift
//  1SKConnect
//
//  Created by Thaad on 04/08/2022.
//

import UIKit

protocol AlertDeviceFindingViewDelegate: AnyObject {
    func onCancelFindingAction()
    func onTimeOutFindingAction()
}

class AlertDeviceFindingViewController: UIViewController {
    
    @IBOutlet weak var view_Image: UIImageView!
    
    weak var delegate: AlertDeviceFindingViewDelegate?
    
    private var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setImageGifDataUI()
    }


    func setImageGifDataUI() {
        self.view_Image.image = UIImage.gifImageWithName("bleConnecting")
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.timer?.invalidate()
        self.timer = nil
        self.delegate?.onCancelFindingAction()
    }
    
}

// MARK: - Timer
extension AlertDeviceFindingViewController {
    func setInitScheduledTimer(timeout: TimeInterval) {
        self.timer = Timer.scheduledTimer(timeInterval: timeout, target: self, selector: #selector(self.onTimeoutAction), userInfo: nil, repeats: false)
    }

    @objc private func onTimeoutAction() {
        self.timer?.invalidate()
        self.timer = nil
        self.delegate?.onTimeOutFindingAction()
    }
}
