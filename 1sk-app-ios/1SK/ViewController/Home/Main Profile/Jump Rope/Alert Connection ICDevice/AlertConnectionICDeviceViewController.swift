//
//  AlertConnectionICDeviceViewController.swift
//  1SKConnect
//
//  Created by Thaad on 04/08/2022.
//

import UIKit
import SnapKit
import ICDeviceManager

enum StateUIDeviceConnection {
    case NotAvailable
    case Finding
    case Connecting
    case Connected
    case Disconnect
    case ConnectFailed
}

protocol AlertConnectionICDeviceViewDelegate: AnyObject {
    func onRetryConnectionAction()
    func onCancelConnectingAction()
    func onUnpairConnectFailedAction()
}

class AlertConnectionICDeviceViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Container: UIView!
    
    lazy var view_AlertBluetoothNotAvailable = AlertBluetoothNotAvailableViewController()
    lazy var view_AlertDeviceFinding = AlertDeviceFindingViewController()
    lazy var view_AlertDeviceConnecting = AlertDeviceConnectingViewController()
    lazy var view_AlertDeviceConnected = AlertDeviceConnectedViewController()
    lazy var view_AlertDeviceDisconnect = AlertDeviceDisconnectViewController()
    lazy var view_AlertDeviceConnectFailed = AlertDeviceConnectFailedViewController()
    
    weak var delegate: AlertConnectionICDeviceViewDelegate?
    private var state: StateUIDeviceConnection?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    func onDismissAction() {
        self.hiddenAnimate()
    }
}

// MARK: - Add Subview
extension AlertConnectionICDeviceViewController {
     func setStateUI(state: StateUIDeviceConnection?) {
         self.state = state
         
         switch state {
         case .NotAvailable:
             self.view_AlertBluetoothNotAvailable.delegate = self
             self.addChildView(childViewController: self.view_AlertBluetoothNotAvailable)
             
         case .Disconnect:
             self.view_AlertDeviceDisconnect.delegate = self
             self.addChildView(childViewController: self.view_AlertDeviceDisconnect)
             
         case .Finding:
             self.view_AlertDeviceFinding.delegate = self
             self.view_AlertDeviceFinding.setInitScheduledTimer(timeout: 15.0)
             self.addChildView(childViewController: self.view_AlertDeviceFinding)
             
         case .Connecting:
             self.view_AlertDeviceConnecting.delegate = self
             self.addChildView(childViewController: self.view_AlertDeviceConnecting)
             
         case .Connected:
             self.view_AlertDeviceConnected.delegate = self
             self.addChildView(childViewController: self.view_AlertDeviceConnected)
             
         case .ConnectFailed:
             self.view_AlertDeviceConnectFailed.delegate = self
             self.addChildView(childViewController: self.view_AlertDeviceConnectFailed)
             
         default:
             break
         }
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: AlertBluetoothNotAvailableViewDelegate
extension AlertConnectionICDeviceViewController: AlertBluetoothNotAvailableViewDelegate {
    func onCancelNotAvailableAction() {
        self.hiddenAnimate()
    }
}

// MARK: AlertDeviceDisconnectViewDelegate
extension AlertConnectionICDeviceViewController: AlertDeviceDisconnectViewDelegate {
    func onCancelDisconnectAction() {
        print("<Bluetooth> onCancelDisconnectAction")
        self.hiddenAnimate()
    }
    
    func onRetryConnectionAction() {
        print("<Bluetooth> onRetryConnectionAction")
        self.delegate?.onRetryConnectionAction()
        self.setStateUI(state: .Finding)
    }
}

// MARK: AlertDeviceFindingViewDelegate
extension AlertConnectionICDeviceViewController: AlertDeviceFindingViewDelegate {
    func onCancelFindingAction() {
        print("<Bluetooth> onCancelFindingAction")
        ICDeviceManager.shared().stopScan()
        self.hiddenAnimate()
    }
    
    func onTimeOutFindingAction() {
        guard self.state == .Finding else {
            return
        }
        print("<Bluetooth> onTimeOutFindingAction")
        ICDeviceManager.shared().stopScan()
        self.setStateUI(state: .Disconnect)
    }
}

// MARK: AlertDeviceConnectingViewDelegate
extension AlertConnectionICDeviceViewController: AlertDeviceConnectingViewDelegate {
    func onCancelConnectingAction() {
        print("<Bluetooth> onCancelConnectingAction")
        self.delegate?.onCancelConnectingAction()
        self.hiddenAnimate()
    }
}

// MARK: AlertDeviceConnectedViewDelegate
extension AlertConnectionICDeviceViewController: AlertDeviceConnectedViewDelegate {
    func onDismissConnectedAction() {
        print("<Bluetooth> onDismissConnectedAction")
        self.hiddenAnimate()
    }
}

// MARK: AlertDeviceConnectFailedViewDelegate
extension AlertConnectionICDeviceViewController: AlertDeviceConnectFailedViewDelegate {
    func onCancelConnectFailedAction() {
        print("<Bluetooth> onCancelConnectFailedAction")
        self.hiddenAnimate()
    }
    
    func onUnpairConnectFailedAction() {
        print("<Bluetooth> onUnpairConnectFailedAction")
        self.delegate?.onUnpairConnectFailedAction()
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension AlertConnectionICDeviceViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
    
}
