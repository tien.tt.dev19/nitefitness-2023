//
//  
//  StartICJumpRopeViewController.swift
//  1SKConnect
//
//  Created by Valerian on 23/05/2022.
//
//

import UIKit

protocol AlertStartJumpRopeViewDelegate: AnyObject {
    func onBeginStartICJumpRope()
}

class AlertStartJumpRopeViewController: BaseViewController {
    
    @IBOutlet weak var view_CountDownParent: UIView!
    @IBOutlet weak var view_CountDown: UIView!
    @IBOutlet weak var lbl_CountDown: UILabel!
    
    weak var delegate: AlertStartJumpRopeViewDelegate?
    var view_CircleProgress = CircleProgressViewJumpRope()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitCountDownProgressView()
    }
}

// MARK: CircleProgressViewJumpRopeDelegate
extension AlertStartJumpRopeViewController: CircleProgressViewJumpRopeDelegate {
    func setInitCountDownProgressView() {
        self.view_CircleProgress.frame = self.view_CountDown.bounds
        self.view_CircleProgress.delegate = self
        self.view_CircleProgress.timeInterval = 3
        self.view_CircleProgress.createCircularPath()
        self.view_CircleProgress.startProgress()
        self.view_CountDown.addSubview(self.view_CircleProgress)
    }
    
    func onCountDownSuccess() {
        self.view_CountDownParent.isHidden = true
        self.lbl_CountDown.isHidden = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.dismiss(animated: false) {
                self.delegate?.onBeginStartICJumpRope()
            }
        }
    }
}
