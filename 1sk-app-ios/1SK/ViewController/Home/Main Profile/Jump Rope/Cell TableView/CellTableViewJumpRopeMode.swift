//
//  CellTableViewJumpRopeMode.swift
//  1SKConnect
//
//  Created by TrungDN on 19/05/2022.
//

import UIKit

class CellTableViewJumpRopeMode: UITableViewCell {
    
    @IBOutlet weak var lbl_ModeName: UILabel!
    @IBOutlet weak var img_ModeIcon: UIImageView!
    
    var model: JumpMode? {
        didSet {
            self.lbl_ModeName.text = self.model?.name
            self.img_ModeIcon.image = self.model?.icon
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
