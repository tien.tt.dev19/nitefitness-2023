//
//  FinishExerciseConfirm.swift
//  1SKConnect
//
//  Created by Valerian on 24/05/2022.
//

import UIKit

protocol AlertFinishExerciseConfirmViewDelegate: AnyObject {
    func onConfirmAlertFinishExerciseAction()
}

class AlertFinishExerciseConfirmViewController: UIViewController {
    weak var delegate: AlertFinishExerciseConfirmViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    @IBAction func onCancelAction(_ sender: UIButton) {
        self.hiddenAnimate()
    }
    
    @IBAction func onConfirmAction(_ sender: UIButton) {
        self.delegate?.onConfirmAlertFinishExerciseAction()
        self.hiddenAnimate()
    }
}

// MARK: - Animation
extension AlertFinishExerciseConfirmViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
