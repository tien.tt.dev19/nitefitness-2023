//
//  AlertInputValueCustomViewController.swift
//  1SK_Connect
//
//  Created by Valerian on 10/05/2022.
//

import UIKit

protocol AlertInputValueCustomViewDelegate: AnyObject {
    func onConfirmAlertInputValueAction(valueOther: Int)
}

class AlertInputValueCustomViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var tf_Value: UITextField!
    @IBOutlet weak var lbl_Warning: UILabel!
    
    @IBOutlet weak var btn_Confirm: UIButton!
    
    weak var delegate: AlertInputValueCustomViewDelegate?
    var jumpMode: JumpMode?
    var value: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitStateDataUI()
        self.setInitUITextField()
        self.setInitUIGestureRecognizer()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tf_Value.becomeFirstResponder()
    }
    
    func setInitStateDataUI() {
        switch self.jumpMode {
        case .timing:
            self.lbl_Title.text = "Chọn thời gian nhảy"
            self.lbl_Warning.text = "Số phút nhảy phải lớn hơn 0 và nhỏ hơn 100"
            self.tf_Value.placeholder = "Phút"
            
        case .countdown:
            self.lbl_Title.text = "Chọn số vòng nhảy"
            self.lbl_Warning.text = "Số vòng nhảy phải nhỏ hơn 10.000"
            self.tf_Value.placeholder = "Vòng nhảy"
            
        default:
            break
        }
    }
    
    func setStateConfirmButton(isSelected: Bool) {
        self.btn_Confirm.isSelected = isSelected
        if isSelected {
            self.btn_Confirm.backgroundColor = R.color.mainColor()
            
        } else {
            self.btn_Confirm.backgroundColor = R.color.mainDeselect()
        }
    }
    
    func setWarningUI(isHidden: Bool) {
        self.lbl_Warning.isHidden = isHidden
    }
    
    // MARK: Action
    @IBAction func onConfirmAction(_ sender: UIButton) {
        guard let valueOther = Int(self.tf_Value.text ?? "") else {
            SKToast.shared.showToast(content: "Định dạng dữ liệu nhập không đúng")
            return
        }
        
        switch self.jumpMode {
        case .timing:
            if valueOther > 0 && valueOther < 100 {
                self.delegate?.onConfirmAlertInputValueAction(valueOther: valueOther)
                self.hiddenAnimate()
            } else {
                SKToast.shared.showToast(content: "Số phút nhảy phải lớn hơn 0 và nhỏ hơn 100")
            }
            
        case .countdown:
            if valueOther > 0 && valueOther < 10000 {
                self.delegate?.onConfirmAlertInputValueAction(valueOther: valueOther)
                self.hiddenAnimate()
            } else {
                SKToast.shared.showToast(content: "Số vòng nhảy phải nhỏ hơn 10.000")
            }
            
        default:
            break
        }
    }
    
}

// MARK: UITextFieldDelegate
extension AlertInputValueCustomViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Value.delegate = self
        self.tf_Value.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.setStateConfirmButton(isSelected: false)
        self.setWarningUI(isHidden: false)
        
        if self.value ?? 0 > 0 {
            self.tf_Value.text = self.value?.asString
        }
    }
    
    @objc func textFieldDidChangeSelection() {
        guard let value = Int(self.tf_Value.text ?? "0") else {
            self.setStateConfirmButton(isSelected: false)
            self.setWarningUI(isHidden: false)
            return
        }
        
        switch self.jumpMode {
        case .timing:
            if value > 0 && value < 100 {
                self.setStateConfirmButton(isSelected: true)
                self.setWarningUI(isHidden: true)
                
            } else {
                self.setStateConfirmButton(isSelected: false)
                self.setWarningUI(isHidden: false)
            }
            
        case .countdown:
            if value > 0 && value < 10000 {
                self.setStateConfirmButton(isSelected: true)
                self.setWarningUI(isHidden: true)
                
            } else {
                self.setStateConfirmButton(isSelected: false)
                self.setWarningUI(isHidden: false)
            }
            
        default:
            self.setStateConfirmButton(isSelected: false)
            self.setWarningUI(isHidden: false)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        let value = Int(textField.text ?? "0") ?? 0
        
        switch self.jumpMode {
        case .timing:
            if value < 100 {
                return true
            } else {
                return false
            }
            
        case .countdown:
            if value < 10000 {
                return true
            } else {
                return false
            }
            
        default:
            return false
        }
    }
}

// MARK: UIGestureRecognizer
extension AlertInputValueCustomViewController {
    func setInitUIGestureRecognizer() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onBgAction)))
    }
    
    @objc func onBgAction() {
        guard self.tf_Value.isEditing == false else { return }
        self.hiddenAnimate()
    }
}

// MARK: - Animation
extension AlertInputValueCustomViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
