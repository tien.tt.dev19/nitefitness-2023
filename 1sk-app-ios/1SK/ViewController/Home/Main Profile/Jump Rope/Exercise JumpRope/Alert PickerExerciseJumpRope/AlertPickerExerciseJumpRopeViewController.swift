//
//  AlertPickerExerciseJumpRopeViewController.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//

import UIKit

protocol AlertPickerExerciseJumpRopeViewDelegate: AnyObject {
    func onSelectOptionsTiming(option: OptionsJumpTiming, valueOther: Int?)
    func onSelectOptionsCount(option: OptionsJumpCount, valueOther: Int?)
}

class AlertPickerExerciseJumpRopeViewController: UIViewController {

    @IBOutlet weak var tbv_Picker: UITableView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    weak var delegate: AlertPickerExerciseJumpRopeViewDelegate?
    
    var jumpMode: JumpMode?
    var listOptionsTiming: [OptionsJumpTiming] = [._30s, ._1m, ._5m, ._10m, ._others]
    var listOptionsCount: [OptionsJumpCount] = [._50, ._100, ._500, ._1000, ._others]
    
    var indexPath = IndexPath(row: 1, section: 0)
    var valueOther: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitDataStateUI()
        self.setInitUITableView()
    }
    
    func setInitDataStateUI() {
        switch self.jumpMode {
        case .timing:
            self.lbl_Title.text = "Chọn thời gian nhảy"
            
        case .countdown:
            self.lbl_Title.text = "Chọn số vòng nhảy"
            
        default:
            self.lbl_Title.text = "Title"
        }
    }

    @IBAction func onCancelOptionJumpAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func onSelectOptionJumpAction(_ sender: Any) {
        switch self.jumpMode {
        case .timing:
            let option = self.listOptionsTiming[self.indexPath.row]
            if option == ._others {
                guard let _valueOther = self.valueOther else {
                    self.presentAlertInputValueCustomView()
                    return
                }
                self.delegate?.onSelectOptionsTiming(option: option, valueOther: _valueOther)
                
            } else {
                self.delegate?.onSelectOptionsTiming(option: option, valueOther: nil)
            }
            
        case .countdown:
            let option = self.listOptionsCount[self.indexPath.row]
            if option == ._others {
                guard let _valueOther = self.valueOther else {
                    self.presentAlertInputValueCustomView()
                    return
                }
                self.delegate?.onSelectOptionsCount(option: option, valueOther: _valueOther)
                
            } else {
                self.delegate?.onSelectOptionsCount(option: option, valueOther: nil)
            }
            
        default:
            break
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.view.removeFromSuperview()
        }
    }
}

// MARK: - UITableViewDataSource
extension AlertPickerExerciseJumpRopeViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_Picker.registerNib(ofType: CellTableViewPickerOptionsJump.self)

        self.tbv_Picker.delegate = self
        self.tbv_Picker.dataSource = self
        self.tbv_Picker.separatorStyle = .none
        
        self.tbv_Picker.selectRow(at: self.indexPath, animated: true, scrollPosition: .bottom)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.jumpMode {
        case .timing:
            return self.listOptionsTiming.count
            
        case .countdown:
            return self.listOptionsCount.count
            
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewPickerOptionsJump.self, for: indexPath)
        switch self.jumpMode {
        case .timing:
            let option = self.listOptionsTiming[indexPath.row]
            if option == ._others {
                if self.valueOther ?? 0 > 0 {
                    cell.lbl_Name.text = "\(self.valueOther ?? 0) phút"
                    
                } else {
                    cell.lbl_Name.text = option.name
                }
                
            } else {
                cell.lbl_Name.text = option.name
            }
            
        case .countdown:
            let option = self.listOptionsCount[indexPath.row]
            if option == ._others {
                if self.valueOther ?? 0 > 0 {
                    cell.lbl_Name.text = self.valueOther?.formatNumber()
                    
                } else {
                    cell.lbl_Name.text = option.name
                }
                
            } else {
                cell.lbl_Name.text = option.name
            }
            
        default:
            break
        }
        
        return cell
    }
}

// MARK: UITableViewDelegate
extension AlertPickerExerciseJumpRopeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexPath = indexPath
        
        switch self.jumpMode {
        case .timing:
            let option = self.listOptionsTiming[indexPath.row]
            if option == ._others {
                self.presentAlertInputValueCustomView()
            }
            
        case .countdown:
            let option = self.listOptionsCount[self.indexPath.row]
            if option == ._others {
                self.presentAlertInputValueCustomView()
            }
            
        default:
            break
        }
    }
}

// MARK: AlertInputValueCustomViewDelegate
extension AlertPickerExerciseJumpRopeViewController: AlertInputValueCustomViewDelegate {
    func presentAlertInputValueCustomView() {
        let controller = AlertInputValueCustomViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.jumpMode = self.jumpMode
        controller.value = self.valueOther
        self.present(controller, animated: false)
    }
    
    func onConfirmAlertInputValueAction(valueOther: Int) {
        self.valueOther = valueOther
        self.tbv_Picker.reloadData()
        self.tbv_Picker.selectRow(at: self.indexPath, animated: true, scrollPosition: .bottom)
    }
}
