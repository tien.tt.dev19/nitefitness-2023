//
//  CellTableViewPickerOptionsJump.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//

import UIKit

class CellTableViewPickerOptionsJump: UITableViewCell {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lbl_Name.font = UIFont(name: "Roboto-Regular", size: 16)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected {
            self.lbl_Name.font = UIFont(name: "Roboto-Medium", size: 20)
            self.lbl_Name.textColor = R.color.mainColor()
            self.view_Bg.backgroundColor = R.color.color_bg_green_light()
            
        } else {
            self.lbl_Name.font = UIFont(name: "Roboto-Regular", size: 16)
            self.lbl_Name.textColor = UIColor(red: 0.451, green: 0.463, blue: 0.471, alpha: 1)
            self.view_Bg.backgroundColor = .white
        }
    }
}
