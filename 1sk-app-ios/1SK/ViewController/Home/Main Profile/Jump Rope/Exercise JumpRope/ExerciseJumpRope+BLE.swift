//
//  ManagerICDevice.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//

import Foundation
import ICDeviceManager

// MARK: - Init ManagerICDevice
extension ExerciseJumpRopeViewController {
    func setInitManagerICDevice() {
        print("<Bluetooth> IC JumpRope  setInitManagerICDevice")
        
        BluetoothManager.shared.delegate_ICDeviceManager = self
        
        // user info
        let icUserInfo = ICUserInfo()
        icUserInfo.height = UInt(gUser?.height ?? 0)
        icUserInfo.userIndex = UInt(gUser?.id ?? 0)
        icUserInfo.weightUnit = .kg
        icUserInfo.rulerUnit = .CM
        icUserInfo.kitchenUnit = .G
        icUserInfo.peopleType = ICPeopleTypeNormal
        if let year = Int(gUser?.birthday?.split(separator: "/").last ?? "0") {
            let age = Date().year - year
            icUserInfo.age = UInt(age)
        }
        if let gender = gUser?.gender {
            if gender == .female {
                icUserInfo.sex = .femal
            } else {
                icUserInfo.sex = .male
            }
        }
        ICDeviceManager.shared().update(icUserInfo)
    }
    
    func setStarSkip(icSkipMode: ICSkipMode, params: UInt) {
        ICDeviceManager.shared().getSettingManager().startSkip(self.viewModel.icDevice, mode: icSkipMode, param: params) { code in
            if code == .success {
                print("<Bluetooth> IC JumpRope  setStarSkip Success code:", code.rawValue)
                
            } else {
                print("<Bluetooth> IC JumpRope  setStarSkip Failed code:", code.rawValue)
            }
        }
    }
    
    func setStopSkip() {
        ICDeviceManager.shared().getSettingManager().stopSkip(self.viewModel.icDevice) { code in
            if code == .success {
                print("<Bluetooth> IC JumpRope  setStopSkip Success code:", code.rawValue)
                
            } else {
                print("<Bluetooth> IC JumpRope  setStopSkip Failed code:", code.rawValue)
            }
        }
        
        if self.view_ConnectError.isHidden == false || self.viewModel.icSkipData?.skip_count ?? 0 == 0 {
            self.dismiss(animated: true)
            
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
                if self.isFinishExercise {
                    self.dismiss(animated: false) {
                        self.delegate?.onFinishExerciseJumpRope(icSkipData: self.viewModel.icSkipData, device: self.viewModel.device)
                    }
                }
            }
        }
    }
}

// MARK: ICDeviceManagerDelegate
extension ExerciseJumpRopeViewController: SKICDeviceManagerDelegate {
    func onInitFinish(_ bSuccess: Bool) {}
    func onScanResult(_ deviceInfo: ICScanDeviceInfo!) {}
    func onDidConnectedICDeviceSuccess(icScanDevice: ICScanDeviceInfo) {}
    func onDidTimeoutScanICDevice() {}
    
    func onBleState(_ state: ICBleState) {
        print("<Bluetooth> IC JumpRope  onBleState state:", state)
        
        if state == .poweredOn {
            self.view_ConnectError.isHidden = true
            
        } else if state == .unauthorized {
            self.view_ConnectError.isHidden = false
            
        } else if state == .poweredOff {
            self.view_ConnectError.isHidden = false
        }
    }
    
    func onDeviceConnectionChanged(_ device: ICDevice!, state: ICDeviceConnectState) {
        print("<Bluetooth> IC JumpRope  onDeviceConnectionChanged macAddr:", device.macAddr ?? "NULL")
        
        guard device.macAddr == self.viewModel.device?.mac else {
            self.alertDefault(title: "Cảnh báo", message: "Đã kết nối thiết bị nhảy dây mới \n\(device.macAddr ?? "device unknown")")
            return
        }
        
        switch state {
        case .connected:
            print("<Bluetooth> IC JumpRope  onDeviceConnectionChanged - connected macAddr:", device.macAddr ?? "NULL")
            self.view_ConnectError.isHidden = true
            
        case .disconnected:
            print("<Bluetooth> IC JumpRope  onDeviceConnectionChanged - disconnected macAddr:", device.macAddr ?? "NULL")
            self.view_ConnectError.isHidden = false
            
        default:
            print("<Bluetooth> IC JumpRope  onDeviceConnectionChanged - unknown macAddr:", device.macAddr ?? "NULL")
            self.view_ConnectError.isHidden = false
        }
    }
    
    func onReceiveSkipData(_ device: ICDevice!, data: ICSkipData!) {
        print("<Bluetooth> IC JumpRope  onReceiveSkipData macAddr:", device.macAddr ?? "NULL")
        
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data ===========================")
        
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - device: (\(device.macAddr ?? "null")")
        
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.isStabilized = \(data.mode.rawValue)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.time = \(data.time)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.mode = \(data.mode.rawValue)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.setting = \(data.setting)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.elapsed_time = \(data.elapsed_time)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.actual_time = \(data.actual_time)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.skip_count = \(data.skip_count)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.avg_freq = \(data.avg_freq)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.fastest_freq = \(data.fastest_freq)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.calories_burned = \(data.calories_burned)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.fat_burn_efficiency = \(data.fat_burn_efficiency)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.freq_count = \(data.freq_count)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.most_jump = \(data.most_jump)")
        print("<Bluetooth> IC JumpRope  onReceiveSkipData - data.freqs = \(data.freqs.count)")
        
        self.viewModel.icSkipData = data
        self.optionValue = Int(data.setting)
        
        self.setUpdateDataStateUI(data: data)
        
        if data.freqs.count > 0 && self.isFinishExercise {
            self.isFinishExercise = false
            self.dismiss(animated: false) {
                self.delegate?.onFinishExerciseJumpRope(icSkipData: self.viewModel.icSkipData, device: self.viewModel.device)
            }
        } else {
            if data.mode.rawValue == self.viewModel.jumpMode?.rawValue ?? -1 {
                // Continue Jump Rope
            } else {
                // Change Mode
                switch Int(data.mode.rawValue) {
                case JumpMode.freedom.rawValue:
                    self.viewModel.jumpMode = .freedom
                    self.setInitJumpModeStateUI()
                    
                case JumpMode.timing.rawValue:
                    self.viewModel.jumpMode = .timing
                    self.setInitJumpModeStateUI()
                    
                case JumpMode.countdown.rawValue:
                    self.viewModel.jumpMode = .countdown
                    self.setInitJumpModeStateUI()
                    
                default:
                    SKToast.shared.showToast(content: "Lỗi chế độ nhảy, bạn vui lòng chọn chế độ để tiếp tục")
                    self.dismiss(animated: true)
                }
            }
        }
    }
}
