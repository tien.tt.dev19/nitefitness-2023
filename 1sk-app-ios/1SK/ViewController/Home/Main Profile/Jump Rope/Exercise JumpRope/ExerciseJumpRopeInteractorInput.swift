//
//  
//  ExerciseJumpRopeInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ExerciseJumpRopeInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol ExerciseJumpRopeInteractorOutputProtocol: AnyObject {
    
}

// MARK: - ExerciseJumpRope InteractorInput
class ExerciseJumpRopeInteractorInput {
    weak var output: ExerciseJumpRopeInteractorOutputProtocol?
}

// MARK: - ExerciseJumpRope InteractorInputProtocol
extension ExerciseJumpRopeInteractorInput: ExerciseJumpRopeInteractorInputProtocol {

}
