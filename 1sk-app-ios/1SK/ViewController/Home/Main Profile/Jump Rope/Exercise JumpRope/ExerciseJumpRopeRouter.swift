//
//  
//  ExerciseJumpRopeRouter.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//
//

import UIKit
import ICDeviceManager

// MARK: - RouterProtocol
protocol ExerciseJumpRopeRouterProtocol {
    func dismissViewController()
    func presentStartJumpRopeVC(delegate: AlertStartJumpRopeViewDelegate?)
    func presentAlertFinishExerciseConfirmVC(delegate: AlertFinishExerciseConfirmViewDelegate?)
}

// MARK: - ExerciseJumpRope Router
class ExerciseJumpRopeRouter {
    weak var viewController: ExerciseJumpRopeViewController?
    
    static func setupModule(jumpMode: JumpMode, icDevice: ICDevice?, icSkipData: ICSkipData?, device: DeviceRealm?, delegate: ExerciseJumpRopeViewDelegate?) -> ExerciseJumpRopeViewController {
        let viewController = ExerciseJumpRopeViewController()
        let router = ExerciseJumpRopeRouter()
        let interactorInput = ExerciseJumpRopeInteractorInput()
        let viewModel = ExerciseJumpRopeViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewController.modalPresentationStyle = .fullScreen
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.jumpMode = jumpMode
        viewModel.icDevice = icDevice
        viewModel.icSkipData = icSkipData
        viewModel.device = device
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ExerciseJumpRope RouterProtocol
extension ExerciseJumpRopeRouter: ExerciseJumpRopeRouterProtocol {
    func dismissViewController() {
        self.viewController?.dismiss(animated: true)
    }
    
    func presentStartJumpRopeVC(delegate: AlertStartJumpRopeViewDelegate?) {
        let controller = AlertStartJumpRopeViewController()
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = delegate
        self.viewController?.present(controller, animated: false)
    }
    
    func presentAlertFinishExerciseConfirmVC(delegate: AlertFinishExerciseConfirmViewDelegate?) {
        let controller = AlertFinishExerciseConfirmViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        self.viewController?.present(controller, animated: false)
    }
}
