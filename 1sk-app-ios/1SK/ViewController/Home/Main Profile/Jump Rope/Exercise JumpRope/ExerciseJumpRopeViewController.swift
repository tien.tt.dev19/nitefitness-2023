//
//  
//  ExerciseJumpRopeViewController.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//
//

import UIKit
import SnapKit
import ICDeviceManager

protocol ExerciseJumpRopeViewDelegate: AnyObject {
    func onFinishExerciseJumpRope(icSkipData: ICSkipData?, device: DeviceRealm?)
}

// MARK: - ViewProtocol
protocol ExerciseJumpRopeViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - ExerciseJumpRope ViewController
class ExerciseJumpRopeViewController: BaseViewController {
    var router: ExerciseJumpRopeRouterProtocol!
    var viewModel: ExerciseJumpRopeViewModelProtocol!
    weak var delegate: ExerciseJumpRopeViewDelegate?
    
    @IBOutlet weak var view_Navigation: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var view_ConnectError: UIView!
    
    @IBOutlet weak var view_JumpBg: UIView!
    @IBOutlet weak var view_JumpContent: UIView!
    
    @IBOutlet weak var lbl_JumpCount: UILabel!
    @IBOutlet weak var lbl_SkipCount: UILabel!
    
    @IBOutlet weak var lbl_JumpTime: UILabel!
    @IBOutlet weak var lbl_SkipTime: UILabel!
    
    @IBOutlet weak var lbl_JumpCalo: UILabel!
    @IBOutlet weak var lbl_JumpEffcient: UILabel!
    
    let alertPickerVC = AlertPickerExerciseJumpRopeViewController()
    var optionValue: Int?
    var isFinishExercise = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.isInteractivePopGestureEnable = false
        self.view_Navigation.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        
        self.setInitJumpModeStateUI()
        self.setInitManagerICDevice()
    }
    
    deinit {
        print("<ManagerICDevice> deinit")
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.presentAlertFinishExerciseConfirmVC(delegate: self)
    }
    
    @IBAction func onFinishAction(_ sender: UIButton) {
        self.router.presentAlertFinishExerciseConfirmVC(delegate: self)
    }
}

// MARK: - Container View
extension ExerciseJumpRopeViewController {
    func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: - ExerciseJumpRope ViewProtocol
extension ExerciseJumpRopeViewController: ExerciseJumpRopeViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - Init StateViewJumpMode
extension ExerciseJumpRopeViewController {
    func setInitJumpModeStateUI() {
        let jumpMode = self.viewModel.jumpMode
        self.lbl_Title.text = jumpMode?.name
        
        switch jumpMode {
        case .freedom:
            self.view_JumpBg.backgroundColor = UIColor(named: "freedomModeColor1")
            self.view_JumpContent.backgroundColor = UIColor(named: "freedomModeColor2")
            
            self.lbl_SkipCount.text = "Vòng nhảy"
            self.lbl_JumpCount.text = "--"
            
            self.lbl_SkipTime.text = "Thời gian nhảy"
            self.lbl_JumpTime.text = "--"
            
            self.setStarSkip(icSkipMode: ICSkipMode(UInt(JumpMode.freedom.rawValue)), params: 0)
            
        case .timing:
            self.view_JumpBg.backgroundColor = UIColor(named: "timingModeColor1")
            self.view_JumpContent.backgroundColor = UIColor(named: "timingModeColor2")
            
            self.lbl_SkipCount.text = "Vòng nhảy"
            self.lbl_JumpCount.text = "--"
            
            self.lbl_SkipTime.text = "Thời gian nhảy"
            self.lbl_JumpTime.text = "--"
            
            self.setInitAlertPickerView()
            
        case .countdown:
            self.view_JumpBg.backgroundColor = UIColor(named: "countModeColor1")
            self.view_JumpContent.backgroundColor = UIColor(named: "countModeColor2")
            
            self.lbl_SkipCount.text = "Vòng nhảy"
            self.lbl_JumpCount.text = "--"
            
            self.lbl_SkipTime.text = "Thời gian nhảy"
            self.lbl_JumpTime.text = "--"
            
            self.setInitAlertPickerView()
            
        default:
            break
        }
    }
    
    func setUpdateDataStateUI(data: ICSkipData) {
        self.lbl_JumpCalo.text = "\(data.calories_burned)"
        self.lbl_JumpEffcient.text = "\(data.fat_burn_efficiency)"
        
        switch self.viewModel.jumpMode {
        case .freedom:
            self.lbl_SkipCount.text =  "Vòng nhảy"
            self.lbl_JumpCount.text = "\(data.skip_count)"
            
            self.lbl_SkipTime.text = "Thời gian nhảy"
            self.lbl_JumpTime.text = TimeInterval(data.elapsed_time).asTimeFormat()
            
        case .timing:
            guard self.optionValue ?? 0 > 0 else { return }
            
            self.lbl_SkipCount.text =  "Vòng nhảy"
            self.lbl_JumpCount.text = "\(data.skip_count)"
            
            self.lbl_SkipTime.text = "Thời gian còn lại"
            let time = (self.optionValue ?? 0) - Int(data.elapsed_time)
            self.lbl_JumpTime.text = TimeInterval(time).asTimeFormat()
            
        case .countdown:
            guard self.optionValue ?? 0 > 0 else { return }
            
            self.lbl_SkipCount.text =  "Vòng nhảy\ncòn lại"
            let count = (self.optionValue ?? 0) - Int(data.skip_count)
            self.lbl_JumpCount.text = count.asString
            
            self.lbl_SkipTime.text = "Thời gian nhảy"
            self.lbl_JumpTime.text = TimeInterval(data.elapsed_time).asTimeFormat()
            
        default:
            break
        }
    }
}

// MARK: AlertPickerExerciseJumpRopeViewDelegate
extension ExerciseJumpRopeViewController: AlertPickerExerciseJumpRopeViewDelegate {
    func setInitAlertPickerView() {
        guard self.viewModel.icSkipData?.setting ?? 0 == 0 else {
            return
        }
        self.alertPickerVC.delegate = self
        self.alertPickerVC.jumpMode = self.viewModel.jumpMode
        self.addChildView(childViewController: self.alertPickerVC)
    }
    
    func onSelectOptionsTiming(option: OptionsJumpTiming, valueOther: Int?) {
        self.router.presentStartJumpRopeVC(delegate: self)
        
        if option == ._others {
            self.optionValue = valueOther?.asTimeSecond
        } else {
            self.optionValue = option.value
        }
        self.lbl_JumpTime.text = TimeInterval(self.optionValue ?? 0).asTimeFormat()
    }
    
    func onSelectOptionsCount(option: OptionsJumpCount, valueOther: Int?) {
        self.router.presentStartJumpRopeVC(delegate: self)
        
        if option == ._others {
            self.optionValue = valueOther
        } else {
            self.optionValue = option.value
        }
        self.lbl_JumpCount.text = self.optionValue?.asString ?? "Error"
    }
}

// MARK: AlertStartJumpRopeViewDelegate
extension ExerciseJumpRopeViewController: AlertStartJumpRopeViewDelegate {
    func onBeginStartICJumpRope() {
        switch self.viewModel.jumpMode {
        case .timing:
            self.setStarSkip(icSkipMode: ICSkipMode(UInt(JumpMode.timing.rawValue)), params: UInt(self.optionValue ?? 0))
            
        case .countdown:
            self.setStarSkip(icSkipMode: ICSkipMode(UInt(JumpMode.countdown.rawValue)), params: UInt(self.optionValue ?? 0))
            
        default:
            break
        }
    }
}

// MARK: - FinishExerciseConfirmDelegate
extension ExerciseJumpRopeViewController: AlertFinishExerciseConfirmViewDelegate {
    func onConfirmAlertFinishExerciseAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.setStopSkip()
        }
    }
}
