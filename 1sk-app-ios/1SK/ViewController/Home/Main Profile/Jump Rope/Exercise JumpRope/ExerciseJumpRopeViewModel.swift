//
//  
//  ExerciseJumpRopeViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 24/06/2022.
//
//

import UIKit
import ICDeviceManager

// MARK: - ViewModelProtocol
protocol ExerciseJumpRopeViewModelProtocol {
    func onViewDidLoad()
    
    var jumpMode: JumpMode? { get set }
    var icDevice: ICDevice? { get set }
    var icSkipData: ICSkipData? { get set }
    
    var device: DeviceRealm? { get set }
    
    
}

// MARK: - ExerciseJumpRope ViewModel
class ExerciseJumpRopeViewModel {
    weak var view: ExerciseJumpRopeViewProtocol?
    private var interactor: ExerciseJumpRopeInteractorInputProtocol

    init(interactor: ExerciseJumpRopeInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var jumpMode: JumpMode?
    var icDevice: ICDevice?
    var icSkipData: ICSkipData?
    
    var device: DeviceRealm?
    
    
    
}

// MARK: - ExerciseJumpRope ViewModelProtocol
extension ExerciseJumpRopeViewModel: ExerciseJumpRopeViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - ExerciseJumpRope InteractorOutputProtocol
extension ExerciseJumpRopeViewModel: ExerciseJumpRopeInteractorOutputProtocol {

}
