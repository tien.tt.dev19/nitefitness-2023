//
//  HeaderTableViewJumpRope.swift
//  1SK
//
//  Created by Thaad on 26/12/2022.
//

import UIKit
import WebKit

protocol HeaderTableViewJumpRopeDelegate: AnyObject {
    func onConnectWithDeviceAction()
    
    func onDidFinishLoadWebView()
    func onAchievementAction()
}

class HeaderTableViewJumpRope: UITableViewHeaderFooterView {
    weak var delegate: HeaderTableViewJumpRopeDelegate?
    
    @IBOutlet weak var view_Measure: UIView!
    @IBOutlet weak var view_Pairing: UIView!
    
    @IBOutlet weak var lbl_SkipCount: UILabel!
    @IBOutlet weak var lbl_Calo: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_JumpTimes: UILabel!
    
    @IBOutlet weak var web_Intro: WKWebView!
    @IBOutlet weak var constraint_height_IntroView: NSLayoutConstraint!
    
    private var codeDevice: String? = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitWKWebView()
    }
    
    func setLayoutState(isPaired: Bool) {
        if isPaired == true {
            self.view_Pairing.isHidden = true
            self.view_Measure.isHidden = false
            
        } else {
            self.view_Pairing.isHidden = false
            self.view_Measure.isHidden = true
        }
    }
    
    func setImageDeviceType(code: String?) {
        self.codeDevice = code
        switch code {
        case SKDevice.Code.RS_1949LB:
            if let config = gConfigs.first(where: {$0.key == "gt_day_nhay_RS1949LB"}) {
                self.setWebKitData(url: config.value)
            }
            
        case SKDevice.Code.RS_2047LB:
            if let config = gConfigs.first(where: {$0.key == "gt_day_nhay_RS2047LB"}) {
                self.setWebKitData(url: config.value)
            }
            
        default:
            break
        }
    }
    
    func setLayoutData(model: UserAchievementModel) {
        self.lbl_SkipCount.text = model.totalCount?.asString ?? "0"
        
        self.lbl_Calo.text = model.kcal?.asString ?? "0"
        self.lbl_Time.text = TimeInterval(model.totalTime ?? 0).asTimeFormat(false)
        self.lbl_JumpTimes.text = model.jumpTimes?.asString ?? "0"
    }

    // MARK: - Action
    @IBAction func onConnectWithDeviceAction(_ sender: Any) {
        self.delegate?.onConnectWithDeviceAction()
    }
    
    @IBAction func onSeeMoreWithDeviceAction(_ sender: Any) {
        var urlProduct: String?
        
        switch self.codeDevice {
        case SKDevice.Code.RS_1949LB:
            if let config = gConfigs.first(where: {$0.key == "mua_day_nhay_RS1949LB"}) {
                urlProduct = config.value
            }
            
        case SKDevice.Code.RS_2047LB:
            if let config = gConfigs.first(where: {$0.key == "mua_day_nhay_RS2047LB"}) {
                urlProduct = config.value
            }
            
        default:
            break
        }
        
        guard let url = URL(string: urlProduct ?? "") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func onAchievementAction(_ sender: Any) {
        self.delegate?.onAchievementAction()
    }
}

// MARK: - WKNavigationDelegate
extension HeaderTableViewJumpRope: WKNavigationDelegate, WKUIDelegate {
    func setInitWKWebView() {
        self.web_Intro.navigationDelegate = self
        
        self.web_Intro.scrollView.showsHorizontalScrollIndicator = false
        self.web_Intro.scrollView.pinchGestureRecognizer?.isEnabled = false
        self.web_Intro.scrollView.isScrollEnabled = false
    }
    
    func setWebKitData(url: String?) {
        guard let _url = URL(string: url ?? "") else {
            return
        }
        self.web_Intro.load(URLRequest(url: _url))
    }
    
    private func setHeightWebView() {
        self.web_Intro.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_Intro.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if let _height = height as? CGFloat {
                        print("web_WebView didFinish height: ", _height)
                        self.constraint_height_IntroView.constant = _height
                        self.delegate?.onDidFinishLoadWebView()
                    }
                })
            }
        })
    }
    
    // WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("web_WebView didFinish ")
        self.setHeightWebView()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            print("<webView> decidePolicyFor url:", url)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.setHeightWebView()
            }
        }
        
        decisionHandler(.allow)
    }
}
