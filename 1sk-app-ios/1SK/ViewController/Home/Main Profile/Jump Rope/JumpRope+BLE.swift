//
//  JumpRope+BLE.swift
//  1SK
//
//  Created by Thaad on 27/12/2022.
//

import Foundation
import ICDeviceManager

extension JumpRopeViewController {
    func setInitManager() {
        BluetoothManager.shared.setInitManager()
    }
    
    func setDelegateICDeviceManager() {
        print("<Bluetooth> Measure setDelegateICDeviceManager")
        BluetoothManager.shared.delegate_ICDeviceManager = self
        BluetoothManager.shared.setScanStartICDevice()
    }
    
    func setDisconnectPeripheral() {
        if let mac = self.viewModel.device?.mac {
            BluetoothManager.shared.setDisconnectICDevice(mac: mac)
        } else {
            BluetoothManager.shared.setScanStopICDevice()
        }
    }
}

// MARK: ICDeviceManagerDelegate
extension JumpRopeViewController: SKICDeviceManagerDelegate {
    func onInitFinish(_ bSuccess: Bool) {}
    func onBleState(_ state: ICBleState) {}
    func onReceiveSkipData(_ device: ICDevice!, data: ICSkipData!) {}
    func onDidConnectedICDeviceSuccess(icScanDevice: ICScanDeviceInfo) {}
    
    func onDidTimeoutScanICDevice() {
        print("<Bluetooth> Measure onDidTimeoutScanICDevice")
    }
    
    func onDeviceConnectionChanged(_ device: ICDevice!, state: ICDeviceConnectState) {
        switch state {
        case .connected:
            print("<Bluetooth> Measure onDeviceConnectionChanged - connected macAddr:", device.macAddr ?? "NULL")
            self.viewModel.icDevice = device
            self.alertConnectionICDeviceVC?.setStateUI(state: .Connected)
            
        default:
            print("<Bluetooth> Measure onDeviceConnectionChanged - disconnected macAddr:", device.macAddr ?? "NULL")
            self.viewModel.icDevice = nil
        }
    }
    
    func onScanResult(_ deviceInfo: ICScanDeviceInfo!) {
        guard self.viewModel.icDevice == nil else {
            return
        }
        guard let device = self.viewModel.device else {
            return
        }
        guard deviceInfo.macAddr == device.mac else {
            return
        }
        self.viewModel.icDevice = ICDevice()
        self.viewModel.icDevice?.macAddr = deviceInfo.macAddr

        if self.viewModel.isScanFirst {
            BluetoothManager.shared.setConnectICDevice(icScanDevice: deviceInfo)

        } else {
            self.alertConnectionICDeviceVC?.setStateUI(state: .Connecting)

            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                if self.viewModel.isCancelConnect {
                    self.viewModel.icDevice = nil
                    self.viewModel.isCancelConnect = false

                } else {
                    BluetoothManager.shared.setConnectICDevice(icScanDevice: deviceInfo)
                }
            }
        }
        
        print("<Bluetooth> Measure onScanResult deviceInfo: \(deviceInfo.macAddr ?? "NULL") - name: \(deviceInfo.name ?? "NULL")")
    }
}
