//
//  
//  JumpRopeInteractorInput.swift
//  1SK
//
//  Created by Thaad on 26/12/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol JumpRopeInteractorInputProtocol {
    func getUserAchievementToday(accessToken: String?)
}

// MARK: - Interactor Output Protocol
protocol JumpRopeInteractorOutputProtocol: AnyObject {
    func onGetUserAchievementTodayFinished(with result: Result<UserAchievementModel, APIError>)
}

// MARK: - JumpRope InteractorInput
class JumpRopeInteractorInput {
    weak var output: JumpRopeInteractorOutputProtocol?
    var jumpRopeService: JumpRopeServiceProtocol?
}

// MARK: - JumpRope InteractorInputProtocol
extension JumpRopeInteractorInput: JumpRopeInteractorInputProtocol {
    func getUserAchievementToday(accessToken: String?) {
        self.jumpRopeService?.getUserAchievementToday(accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetUserAchievementTodayFinished(with: result.unwrapSuccessModel())
        })
    }
}
