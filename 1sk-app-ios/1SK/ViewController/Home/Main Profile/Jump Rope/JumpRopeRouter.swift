//
//  
//  JumpRopeRouter.swift
//  1SK
//
//  Created by Thaad on 26/12/2022.
//
//

import UIKit
import ICDeviceManager

// MARK: - RouterProtocol
protocol JumpRopeRouterProtocol {
    func presentPairingDeviceRouter(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?)
    func presentExerciseJumpRopeRouter(jumpMode: JumpMode, icDevice: ICDevice?, icSkipData: ICSkipData?, device: DeviceRealm?, delegate: ExerciseJumpRopeViewDelegate?)
    func presentResultJumpRopeRouter(icSkipData: ICSkipData?, device: DeviceRealm?, delegate: ResultJumpRopeViewDelegate?)
    func showLeaderboardJumpRopeRouter(device: DeviceRealm?)
}

// MARK: - JumpRope Router
class JumpRopeRouter {
    weak var viewController: JumpRopeViewController?
    
    static func setupModule(device: DeviceRealm?) -> JumpRopeViewController {
        let viewController = JumpRopeViewController()
        let router = JumpRopeRouter()
        let interactorInput = JumpRopeInteractorInput()
        let viewModel = JumpRopeViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.jumpRopeService = JumpRopeService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - JumpRope RouterProtocol
extension JumpRopeRouter: JumpRopeRouterProtocol {
    func presentPairingDeviceRouter(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?) {
        let controller = PairingDeviceRouter.setupModule(device: device, delegate: delegate)
        self.viewController?.navigationController?.present(controller, animated: true)
    }
    
    func presentExerciseJumpRopeRouter(jumpMode: JumpMode, icDevice: ICDevice?, icSkipData: ICSkipData?, device: DeviceRealm?, delegate: ExerciseJumpRopeViewDelegate?) {
         let controller = ExerciseJumpRopeRouter.setupModule(jumpMode: jumpMode, icDevice: icDevice, icSkipData: icSkipData, device: device, delegate: delegate)
         self.viewController?.present(controller, animated: true)
     }
    
    func presentResultJumpRopeRouter(icSkipData: ICSkipData?, device: DeviceRealm?, delegate: ResultJumpRopeViewDelegate?) {
        let controller = ResultJumpRopeRouter.setupModule(icSkipData: icSkipData, device: device, delegate: delegate)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
    
    func showLeaderboardJumpRopeRouter(device: DeviceRealm?) {
        let controller = LeaderboardJumpRopeRouter.setupModule(device: device)
        self.viewController?.present(controller, animated: true)
    }
}
