//
//  
//  JumpRopeViewController.swift
//  1SK
//
//  Created by Thaad on 26/12/2022.
//
//

import UIKit
import ICDeviceManager

protocol JumpRopeViewDelegate: AnyObject {
    func onConnectionAction(device: DeviceRealm?)
    func onUnpairDevice(with type: TypeDevice)
}

// MARK: - ViewProtocol
protocol JumpRopeViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func setLayoutData(model: UserAchievementModel)
}

// MARK: - JumpRope ViewController
class JumpRopeViewController: BaseViewController {
    var router: JumpRopeRouterProtocol!
    var viewModel: JumpRopeViewModelProtocol!
    weak var delegate: JumpRopeViewDelegate?
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewJumpRope?
    var alertConnectionICDeviceVC: AlertConnectionICDeviceViewController?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
        self.setLayoutHeaderTableView()
        
        self.setDelegateICDeviceManager()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitManager()
        self.setInitUITableView()
        self.setLayoutHeaderTableView()
    }
    
    deinit {
        self.setDisconnectPeripheral()
        print("deinit JumpRopeViewController")
    }
    
}

// MARK: - JumpRope ViewProtocol
extension JumpRopeViewController: JumpRopeViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func setLayoutData(model: UserAchievementModel) {
        self.view_Header?.setLayoutData(model: model)
    }
}

// MARK: - UITableViewDataSource
extension JumpRopeViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewJumpRopeMode.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.isPairedDevice() == true {
            return self.viewModel.listJumpMode.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewJumpRopeMode.self, for: indexPath)
        cell.model = self.viewModel.listJumpMode[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension JumpRopeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard ICDeviceManager.shared().isBLEEnable() == true else {
            self.presentAlertConnectionICDeviceVC(state: .NotAvailable)
            return
        }
        guard self.viewModel.icDevice != nil else {
            self.presentAlertConnectionICDeviceVC(state: .Disconnect)
            return
        }
        self.onJumpModeChangeByICDevice(mode: indexPath.row)
    }
}

// MARK: UITableView Header
extension JumpRopeViewController: HeaderTableViewJumpRopeDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewJumpRope.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setLayoutHeaderTableView() {
        self.view_Header?.setLayoutState(isPaired: self.viewModel.isPairedDevice())
        self.view_Header?.setImageDeviceType(code: self.viewModel.device?.code)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // Delegate
    func onConnectWithDeviceAction() {
        self.router.presentPairingDeviceRouter(device: self.viewModel.device, delegate: self)
    }
    
    func onDidFinishLoadWebView() {
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func onAchievementAction() {
        self.router.showLeaderboardJumpRopeRouter(device: self.viewModel.device)
    }
}

// MARK: PairingDeviceViewDelegate
extension JumpRopeViewController: PairingDeviceViewDelegate {
    func onConnectionAction(device: DeviceRealm) {
        guard let profile: ProfileRealm = gRealm.objects().first(where: {$0.type == .JUMP_ROPE}) else {
            return
        }
        self.viewModel.device = device
        self.viewModel.device?.profile = profile
        self.delegate?.onConnectionAction(device: device)
        
        self.setLayoutHeaderTableView()
        self.onReloadData()
    }
}

// MARK: - AlertConnectionICDeviceViewDelegate
extension JumpRopeViewController: AlertConnectionICDeviceViewDelegate {
    func presentAlertConnectionICDeviceVC(state: StateUIDeviceConnection) {
        self.viewModel.isScanFirst = false
        
        self.alertConnectionICDeviceVC = AlertConnectionICDeviceViewController()
        self.alertConnectionICDeviceVC?.modalPresentationStyle = .custom
        self.alertConnectionICDeviceVC?.delegate = self
        
        self.present(self.alertConnectionICDeviceVC!, animated: false)
        self.alertConnectionICDeviceVC?.setStateUI(state: state)
    }
    
    func onRetryConnectionAction() {
        BluetoothManager.shared.setScanStartICDevice()
    }
    
    func onCancelConnectingAction() {
        self.viewModel.isCancelConnect = true
    }
    
    func onUnpairConnectFailedAction() {
        guard let device = self.viewModel.device else {
            return
        }
        gRealm.remove(device)
        self.viewModel.device = nil
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            self.presentConnectDeviceRouter()
//        }
    }
}

// MARK: AlertStartJumpRopeViewDelegate
extension JumpRopeViewController: AlertStartJumpRopeViewDelegate {
    func presentStartJumpRopeVC() {
        let controller = AlertStartJumpRopeViewController()
        controller.modalPresentationStyle = .fullScreen
        controller.delegate = self
        self.present(controller, animated: false)
    }
    
    func onBeginStartICJumpRope() {
        self.router.presentExerciseJumpRopeRouter(jumpMode: JumpMode.freedom,
                                                  icDevice: self.viewModel.icDevice,
                                                  icSkipData: nil,
                                                  device: self.viewModel.device,
                                                  delegate: self)
    }
}

// MARK: ResultJumpRopeViewDelegate
extension JumpRopeViewController: ResultJumpRopeViewDelegate {
    func onJumpModeChangeByICDevice(icSkipData: ICSkipData?) {
        let mode = Int(icSkipData?.mode.rawValue ?? 3)
        switch mode {
        case JumpMode.freedom.rawValue:
            self.router.presentExerciseJumpRopeRouter(
                jumpMode: JumpMode.freedom,
                icDevice: self.viewModel.icDevice,
                icSkipData: icSkipData,
                device: self.viewModel.device,
                delegate: self)

        case JumpMode.timing.rawValue:
            self.router.presentExerciseJumpRopeRouter(
                jumpMode: JumpMode.timing,
                icDevice: self.viewModel.icDevice,
                icSkipData: icSkipData,
                device: self.viewModel.device,
                delegate: self)
            
        case JumpMode.countdown.rawValue:
            self.router.presentExerciseJumpRopeRouter(
                jumpMode: JumpMode.countdown,
                icDevice: self.viewModel.icDevice,
                icSkipData: icSkipData,
                device: self.viewModel.device,
                delegate: self)
            
        default:
            break
        }
    }
    
    func onJumpModeChangeByICDevice(mode: Int?) {
        switch mode {
        case JumpMode.freedom.rawValue:
            self.presentStartJumpRopeVC()

        case JumpMode.timing.rawValue:
            self.router.presentExerciseJumpRopeRouter(jumpMode: JumpMode.timing,
                                                      icDevice: self.viewModel.icDevice,
                                                      icSkipData: nil,
                                                      device: self.viewModel.device,
                                                      delegate: self)
        case JumpMode.countdown.rawValue:
            self.router.presentExerciseJumpRopeRouter(jumpMode: JumpMode.countdown,
                                                      icDevice: self.viewModel.icDevice,
                                                      icSkipData: nil,
                                                      device: self.viewModel.device,
                                                      delegate: self)
        default:
            break
        }
    }
}

// MARK: ExerciseJumpRopeViewDelegate
extension JumpRopeViewController: ExerciseJumpRopeViewDelegate {
    func onFinishExerciseJumpRope(icSkipData: ICSkipData?, device: DeviceRealm?) {
        self.router.presentResultJumpRopeRouter(icSkipData: icSkipData, device: device, delegate: self)
    }
}
