//
//  
//  JumpRopeViewModel.swift
//  1SK
//
//  Created by Thaad on 26/12/2022.
//
//

import UIKit
import ICDeviceManager

// MARK: - ViewModelProtocol
protocol JumpRopeViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func isPairedDevice() -> Bool
    
    var device: DeviceRealm? { get set }
    var icDevice: ICDevice? { get set }
    var listJumpMode: [JumpMode] { get }
    var isCancelConnect: Bool { get set }
    var isScanFirst: Bool { get set }
}

// MARK: - JumpRope ViewModel
class JumpRopeViewModel {
    weak var view: JumpRopeViewProtocol?
    private var interactor: JumpRopeInteractorInputProtocol

    init(interactor: JumpRopeInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    var icDevice: ICDevice?
    let listJumpMode: [JumpMode] = [JumpMode.freedom, JumpMode.timing, JumpMode.countdown]
    var isCancelConnect = false
    var isScanFirst = true
}

// MARK: - JumpRope ViewModelProtocol
extension JumpRopeViewModel: JumpRopeViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        
    }
    
    func onViewDidAppear() {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.interactor.getUserAchievementToday(accessToken: accessToken)
    }
    
    func isPairedDevice() -> Bool {
        if self.device?.uuid != nil {
            return true
            
        } else {
            return false
        }
    }
}

// MARK: - JumpRope InteractorOutputProtocol
extension JumpRopeViewModel: JumpRopeInteractorOutputProtocol {
    func onGetUserAchievementTodayFinished(with result: Result<UserAchievementModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.setLayoutData(model: model)
            
        case .failure:
            break
        }
    }
}
