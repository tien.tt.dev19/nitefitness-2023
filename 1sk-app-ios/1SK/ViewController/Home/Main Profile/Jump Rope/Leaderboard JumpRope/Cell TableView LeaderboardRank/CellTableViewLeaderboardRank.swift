//
//  CellTableViewLeaderboardJumpRopeRate.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//

import UIKit

class CellTableViewLeaderboardRank: UITableViewCell {
    
    @IBOutlet weak var lbl_userIndex: UILabel!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_jumpCounts: UILabel!
    @IBOutlet weak var img_icTopChart: UIImageView!
    
    var model: UserRankingModel? {
        didSet {
            if let data = model {
                self.lbl_userName.text = data.fullName == nil ? "Chưa có tên" : data.fullName
                self.lbl_jumpCounts.text = data.totalCount
                self.lbl_userIndex.text = "\(data.rank ?? 0)"
                
                switch model?.rank {
                case 1:
                    self.lbl_userIndex.isHidden = true
                    self.img_icTopChart.isHidden = false
                    self.img_icTopChart.image = UIImage(named: "ic_topOne")
                    
                case 2:
                    self.lbl_userIndex.isHidden = true
                    self.img_icTopChart.isHidden = false
                    self.img_icTopChart.image = UIImage(named: "ic_topTwo")
                    
                case 3:
                    self.lbl_userIndex.isHidden = true
                    self.img_icTopChart.isHidden = false
                    self.img_icTopChart.image = UIImage(named: "ic_topThree")
                    
                default:
                    self.lbl_userIndex.isHidden = false
                    self.img_icTopChart.isHidden = true
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
