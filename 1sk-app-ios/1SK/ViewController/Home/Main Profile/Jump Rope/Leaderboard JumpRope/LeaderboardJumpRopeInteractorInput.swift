//
//  
//  LeaderboardJumpRopeInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol LeaderboardJumpRopeInteractorInputProtocol {
    func getUserStatistic(accessToken: String?)
    func getUserRanking(accessToken: String?, timeFilter: TimeFilter)
}

// MARK: - Interactor Output Protocol
protocol LeaderboardJumpRopeInteractorOutputProtocol: AnyObject {
    func onGetUserStatisticFinished(with result: Result<UserStatisticModel, APIError>)
    func onGetUserRankingFinished(with result: Result<[UserRankingModel], APIError>)
}

// MARK: - LeaderboardJumpRope InteractorInput
class LeaderboardJumpRopeInteractorInput {
    weak var output: LeaderboardJumpRopeInteractorOutputProtocol?
    var jumpRopeService: JumpRopeServiceProtocol?
}

// MARK: - LeaderboardJumpRope InteractorInputProtocol
extension LeaderboardJumpRopeInteractorInput: LeaderboardJumpRopeInteractorInputProtocol {
    func getUserStatistic(accessToken: String?) {
        self.jumpRopeService?.getUserStatistic(accessToken: accessToken, completion: { [weak self] (result) in
            self?.output?.onGetUserStatisticFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getUserRanking(accessToken: String?, timeFilter: TimeFilter) {
        self.jumpRopeService?.getUserRanking(accessToken: accessToken, timeFilter: timeFilter, completion: { [weak self] (result) in
            self?.output?.onGetUserRankingFinished(with: result.unwrapSuccessModel())
        })
    }
}
