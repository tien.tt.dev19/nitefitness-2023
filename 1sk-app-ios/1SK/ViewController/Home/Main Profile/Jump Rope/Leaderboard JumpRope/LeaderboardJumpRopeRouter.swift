//
//  
//  LeaderboardJumpRopeRouter.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol LeaderboardJumpRopeRouterProtocol {
    func onDismissAction()
}

// MARK: - LeaderboardJumpRope Router
class LeaderboardJumpRopeRouter {
    weak var viewController: LeaderboardJumpRopeViewController?
    
    static func setupModule(device: DeviceRealm?) -> LeaderboardJumpRopeViewController {
        let viewController = LeaderboardJumpRopeViewController()
        let router = LeaderboardJumpRopeRouter()
        let interactorInput = LeaderboardJumpRopeInteractorInput()
        let viewModel = LeaderboardJumpRopeViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.jumpRopeService = JumpRopeService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - LeaderboardJumpRope RouterProtocol
extension LeaderboardJumpRopeRouter: LeaderboardJumpRopeRouterProtocol {
    func onDismissAction() {
        self.viewController?.dismiss(animated: true)
    }
    
}
