//
//  
//  LeaderboardJumpRopeViewController.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol LeaderboardJumpRopeViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setLayoutStatistic(userStatistic: UserStatisticModel?)
    func onReloadData()
}

// MARK: - ViewController
class LeaderboardJumpRopeViewController: BaseViewController {
    var router: LeaderboardJumpRopeRouterProtocol!
    var viewModel: LeaderboardJumpRopeViewModelProtocol!
    
    @IBOutlet weak var lbl_TotalJumps: UILabel!
    @IBOutlet weak var lbl_TotalTime: UILabel!
    @IBOutlet weak var lbl_HighestAchievement: UILabel!
    @IBOutlet weak var lbl_NumberReachTop: UILabel!
    
    @IBOutlet weak var btn_Today: UIButton!
    @IBOutlet weak var btn_Week: UIButton!
    @IBOutlet weak var btn_Month: UIButton!
    
    @IBOutlet weak var view_Day: UIStackView!
    @IBOutlet weak var view_DayIndicator: UIView!
    
    @IBOutlet weak var lbl_Day: UILabel!
    @IBOutlet weak var lbl_Hour: UILabel!
    @IBOutlet weak var lbl_Minute: UILabel!
    @IBOutlet weak var lbl_Second: UILabel!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var cons_TableView: NSLayoutConstraint!
    
    var viewMode = 0
    var timer: Timer?
    var timeCountdown = 0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.timer?.invalidate()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Thành tích của tôi"
        self.setStatePageBar(index: -1)
        self.setInitUITableView()
        self.setInitScheduledTimer()
    }
    
    deinit {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismissAction()
    }
    
    @IBAction func onTodayAction(_ sender: Any) {
        self.setDataTimer(type: .day)
        self.setStatePageBar(index: 0)
        self.viewMode = 0
        self.viewModel.onGetUserRanking(timeFilter: TimeFilter.day)
    }
    
    @IBAction func onWeekAction(_ sender: Any) {
        self.setDataTimer(type: .week)
        self.setStatePageBar(index: 1)
        self.viewMode = 1
        self.viewModel.onGetUserRanking(timeFilter: TimeFilter.week)
    }
    
    @IBAction func onMonthAction(_ sender: Any) {
        self.setDataTimer(type: .month)
        self.setStatePageBar(index: 2)
        self.viewMode = 2
        self.viewModel.onGetUserRanking(timeFilter: TimeFilter.month)
    }
    
}

// MARK: - ViewProtocol
extension LeaderboardJumpRopeViewController: LeaderboardJumpRopeViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setLayoutStatistic(userStatistic: UserStatisticModel?) {
        let hightestSkipCount = userStatistic?.highestScore?.skipCount ?? 0
        var hightestElapsedTime = ((userStatistic?.highestScore?.elapsedTime ?? 0) / 60)
        if hightestElapsedTime < 1 {
            hightestElapsedTime = 1
        }
        
        self.lbl_TotalTime.text = TimeInterval(userStatistic?.totalTime ?? 0).asTimeFormat(false)
        
        self.lbl_TotalJumps.text = userStatistic?.totalCount?.asString
        self.lbl_HighestAchievement.text = "\(hightestSkipCount) vòng/\n\(hightestElapsedTime) phút"
        self.lbl_NumberReachTop.text = String(describing: userStatistic?.topCount ?? 0)
    }
    
    func onReloadData() {
        if self.viewModel.listUserRanking.count > 0 {
            let constant = CGFloat((self.viewModel.listUserRanking.count * 48) + 20)
            self.cons_TableView.constant = constant
            
        } else {
            self.cons_TableView.constant = 150
        }
        
        self.tbv_TableView.reloadData()
    }
}

// MARK: Action Handler
extension LeaderboardJumpRopeViewController {
    func setStatePageBar(index: Int) {
        switch index {
        case 0:
            UIView.animate(withDuration: 0.1) {
                self.btn_Today.backgroundColor = R.color.mainColor()
                self.btn_Today.setTitleColor(.white, for: .normal)
                
                self.btn_Week.backgroundColor = R.color.line()
                self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
                
                self.btn_Month.backgroundColor = R.color.line()
                self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
                
                self.view.layoutIfNeeded()
                
            } completion: { _ in
                self.view_Day.isHidden = true
                self.view_DayIndicator.isHidden = true
            }
            
        case 1:
            UIView.animate(withDuration: 0.1) {
                self.btn_Week.backgroundColor = R.color.mainColor()
                self.btn_Week.setTitleColor(.white, for: .normal)
                
                self.btn_Today.backgroundColor = R.color.line()
                self.btn_Today.setTitleColor(R.color.subTitle(), for: .normal)
                
                self.btn_Month.backgroundColor = R.color.line()
                self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
                
                self.view.layoutIfNeeded()
                
            } completion: { _ in
                self.view_Day.isHidden = false
                self.view_DayIndicator.isHidden = false
            }
            
        case 2:
            UIView.animate(withDuration: 0.1) {
                self.btn_Month.backgroundColor = R.color.mainColor()
                self.btn_Month.setTitleColor(.white, for: .normal)
                
                self.btn_Week.backgroundColor = R.color.line()
                self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
                
                self.btn_Today.backgroundColor = R.color.line()
                self.btn_Today.setTitleColor(R.color.subTitle(), for: .normal)
                
                self.view.layoutIfNeeded()
                
            } completion: { _ in
                self.view_Day.isHidden = false
                self.view_DayIndicator.isHidden = false
            }
            
        default:
            self.btn_Today.backgroundColor = R.color.mainColor()
            self.btn_Today.setTitleColor(.white, for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.view_Day.isHidden = true
            self.view_DayIndicator.isHidden = true
        }
    }
    
    func calNumberOfDayInMonth() -> Int {
        let calendar = Calendar.current
        let date = Date()
        let interval = calendar.dateInterval(of: .month, for: date)!
        let days = calendar.dateComponents([.day], from: interval.start, to: interval.end).day!
        return days
    }
    
    func currentDayOfMonth() -> Int {
        return Calendar.current.ordinality(of: .day, in: .month, for: Date())!
    }
}

// MARK: - Scheduled Timer
extension LeaderboardJumpRopeViewController {
    func setInitScheduledTimer() {
        self.setDataTimer(type: .day)
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onUpdateTimeAction), userInfo: nil, repeats: true)
    }
    
    @objc func onUpdateTimeAction() {
        self.timeCountdown -= 1
        let watch = StopWatch(totalSeconds: self.timeCountdown)
        
        self.lbl_Day.text = String(format: "%02d", watch.days)
        self.lbl_Hour.text = String(format: "%02d", watch.hours)
        self.lbl_Minute.text = String(format: "%02d", watch.minutes)
        self.lbl_Second.text = String(format: "%02d", watch.seconds)
    }
    
    func setDataTimer(type: TimeFilter) {
        let timeCurrent = Date().toTimestamp()
        
        switch type {
        case .day:
            let timeEnd = Date().dayCurrent.toTimestamp()
            self.timeCountdown = timeEnd - timeCurrent
            
        case .week:
            let timeEnd = (Date().endOfWeekCurrent?.toTimestamp() ?? 0) + 86400
            self.timeCountdown = timeEnd - timeCurrent
            
        case .month:
            let timeEnd = (Date().endOfMonthCurrent?.toTimestamp() ?? 0) + 86400
            self.timeCountdown = timeEnd - timeCurrent
        }
    }
}

// MARK: UITableViewDataSource
extension LeaderboardJumpRopeViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionViewLeaderboardJumpRope.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewLeaderboardRank.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        self.tbv_TableView.separatorStyle = .none
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.viewModel.listUserRanking.count > 0 {
            return 0
            
        } else {
            return 150
        }
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderSectionViewLeaderboardJumpRope.self)
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 48
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listUserRanking.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewLeaderboardRank.self, for: indexPath)
        let userRanking = self.viewModel.listUserRanking[indexPath.row]
        
        cell.model = userRanking
        
        if gUser?.id == userRanking.customerId {
            cell.backgroundColor = R.color.mainColor()
            cell.lbl_jumpCounts.textColor = .white
            cell.lbl_userName.textColor = .white
            cell.lbl_userIndex.textColor = .white
            
        } else {
            cell.backgroundColor = .white
            cell.lbl_jumpCounts.textColor = .black
            cell.lbl_userName.textColor = .black
            cell.lbl_userIndex.textColor = .black
        }
        return cell
    }
}
