//
//  
//  LeaderboardJumpRopeViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 06/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol LeaderboardJumpRopeViewModelProtocol {
    func onViewDidLoad()
    func onGetUserRanking(timeFilter: TimeFilter)
    
    var listUserRanking: [UserRankingModel] { get set }
}

// MARK: - LeaderboardJumpRope ViewModel
class LeaderboardJumpRopeViewModel {
    weak var view: LeaderboardJumpRopeViewProtocol?
    private var interactor: LeaderboardJumpRopeInteractorInputProtocol
    
    init(interactor: LeaderboardJumpRopeInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var device: DeviceRealm?
    var userStatistic: UserStatisticModel?
    var listUserRanking: [UserRankingModel] = []
    
}

// MARK: - LeaderboardJumpRope ViewModelProtocol
extension LeaderboardJumpRopeViewModel: LeaderboardJumpRopeViewModelProtocol {
    func onViewDidLoad() {
        self.getInitData()
    }
    
    func onGetUserRanking(timeFilter: TimeFilter) {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.interactor.getUserRanking(accessToken: accessToken, timeFilter: timeFilter)
    }
}

// MARK: LeaderboardJumpRope Hander
extension LeaderboardJumpRopeViewModel {
    func getInitData() {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.view?.showHud()
        self.interactor.getUserStatistic(accessToken: accessToken)
        self.interactor.getUserRanking(accessToken: accessToken, timeFilter: .day)
    }
}

// MARK: - LeaderboardJumpRope InteractorOutputProtocol
extension LeaderboardJumpRopeViewModel: LeaderboardJumpRopeInteractorOutputProtocol {
    func onGetUserStatisticFinished(with result: Result<UserStatisticModel, APIError>) {
        switch result {
        case .success(let model):
            self.userStatistic = model
            self.view?.setLayoutStatistic(userStatistic: self.userStatistic)
            
        case .failure(let error):
            debugPrint(error.description)
        }
        self.view?.hideHud()
    }
    
    func onGetUserRankingFinished(with result: Result<[UserRankingModel], APIError>) {
        switch result {
        case .success(let model):
            self.listUserRanking = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error.description)
        }
        self.view?.hideHud()
    }
}
