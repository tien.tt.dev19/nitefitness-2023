//
//  CellResultTableViewBody.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//

import UIKit
import ICDeviceManager

class CellResultTableViewBody: UITableViewCell {
    
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    var model: ICSkipData?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitUICollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setAttributedString(with valueString: String, accurrency: String) -> NSMutableAttributedString {
        let attrs1 = [NSAttributedString.Key.font: UIFont(name: "Roboto-Medium", size: 18), NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let attrs2 = [NSAttributedString.Key.font: UIFont(name: "Roboto-Regular", size: 16), NSAttributedString.Key.foregroundColor: UIColor.init(hex: "737678")]
        
        let attributedString1 = NSMutableAttributedString(string: valueString, attributes: attrs1 as [NSAttributedString.Key: Any])
        let attributedString2 = NSMutableAttributedString(string: accurrency, attributes: attrs2 as [NSAttributedString.Key: Any])
        
        attributedString1.append(attributedString2)
        return attributedString1
    }
}

// MARK: - UICollectionViewDataSource
extension CellResultTableViewBody: UICollectionViewDataSource, UICollectionViewDelegate {
    func setInitUICollectionView() {
        self.selectionStyle = .none
        self.coll_CollectionView.registerNib(ofType: CellResultColletionView.self)

        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 16
        layout.minimumLineSpacing = 16

        print("UIScreen.main.bounds.width: CellResultTableViewBody", UIScreen.main.bounds.width)
        
        let widthColl: CGFloat = UIScreen.main.bounds.width - 32
        let widthCell = (widthColl - 33) / 3
        
        layout.itemSize = CGSize(width: widthCell, height: widthCell)
        self.coll_CollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeuCell(ofType: CellResultColletionView.self, for: indexPath)
        
        if let `model` = model {
            switch indexPath.row {
            case 0:
                cell.lbl_jumpResult_title.text = ResultDataTypes.calo.name
                cell.lbl_jumpResult.attributedText = setAttributedString(with: String(describing: model.calories_burned), accurrency: "")
            case 1:
                cell.lbl_jumpResult_title.text = ResultDataTypes.jump_time.name
                cell.lbl_jumpResult.text = TimeInterval(model.elapsed_time).asTimeFormat(false) //actual_time
                
            case 2:
                cell.lbl_jumpResult_title.text = ResultDataTypes.freq_count.name
                cell.lbl_jumpResult.text = String(describing: model.freqs.count)
                
            case 3:
                cell.lbl_jumpResult_title.text = ResultDataTypes.freq_most_jump.name
                cell.lbl_jumpResult.attributedText = setAttributedString(with: String(describing: model.fastest_freq), accurrency: "")
                
            case 4:
                cell.lbl_jumpResult_title.text = ResultDataTypes.freq_avg_jump.name
                cell.lbl_jumpResult.attributedText = setAttributedString(with: String(describing: model.avg_freq), accurrency: "")
                
            case 5:
                cell.lbl_jumpResult_title.text = ResultDataTypes.most_jump.name
                cell.lbl_jumpResult.text = String(describing: model.most_jump)
                
            default:
                break
            }
        }
        return cell
    }
}
