//
//  CellListeExercercise.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//

import UIKit
import ICDeviceManager

class CellListeExercercise: UITableViewCell {
    
    @IBOutlet weak var lbl_index: UILabel!
    @IBOutlet weak var lbl_duration: UILabel!
    @IBOutlet weak var lbl_skipTime: UILabel!
    
    var model: ICSkipFreqData? {
        didSet {
            self.config(with: model)
        }
    }
    
    var index: Int = 0 {
        didSet {
            self.lbl_index.text = "\(index + 1)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func config(with data: ICSkipFreqData?) {
        if let data = data {
            let formatTime = self.secondsToMinutesSeconds(seconds: Int(data.duration))
            self.lbl_duration.text = self.makeTimeString(minutes: formatTime.0, seconds: formatTime.1)
            self.lbl_skipTime.text = String(describing: data.skip_count)
        }
    }
    
    private func secondsToMinutesSeconds(seconds: Int) -> (Int, Int) {
        return ((seconds / 60), (seconds % 60))
    }

    private func makeTimeString(minutes: Int, seconds: Int) -> String {
        var timeString = ""
        timeString += String(format: "%02d", minutes)
        timeString += ":"
        timeString += String(format: "%02d", seconds)
        return timeString
    }
}
