//
//  CellResultTableViewFooter.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//

import UIKit
import ICDeviceManager

class CellResultTableViewFooter: UITableViewCell {
    
    @IBOutlet weak var tbv_resultTableView: UITableView!
    
    var model: ICSkipData? {
        didSet {
            self.tbv_resultTableView.reloadData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.setTbvInit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: - UITableViewDataSource
extension CellResultTableViewFooter: UITableViewDataSource {

    func setTbvInit() {
        self.tbv_resultTableView.registerNib(ofType: CellListeExercercise.self)
        self.selectionStyle = .none
        
        self.tbv_resultTableView.delegate = self
        self.tbv_resultTableView.dataSource = self
        self.tbv_resultTableView.separatorStyle = .none
        
        self.tbv_resultTableView.estimatedRowHeight = 54
        self.tbv_resultTableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model?.freqs.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellListeExercercise.self, for: indexPath)
        cell.model = self.model?.freqs[indexPath.row]
        cell.index = indexPath.row
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CellResultTableViewFooter: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 54
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
