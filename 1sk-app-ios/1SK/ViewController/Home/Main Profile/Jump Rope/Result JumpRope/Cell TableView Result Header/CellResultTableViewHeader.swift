//
//  CellResultTableViewHeader.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//

import UIKit
import ICDeviceManager

class CellResultTableViewHeader: UITableViewCell {
    
    @IBOutlet weak var view_jumpCycle1: UIView!
    @IBOutlet weak var view_jumpCycle2: UIView!
    @IBOutlet weak var lbl_skipCount: UILabel!
    
    var model: ICSkipData? {
        didSet {
            self.lbl_skipCount.text = "\(model?.skip_count ?? 0)"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        view_jumpCycle1.layer.masksToBounds = true
        view_jumpCycle2.layer.masksToBounds = true
        view_jumpCycle1.layer.cornerRadius = view_jumpCycle1.frame.height / 2
        view_jumpCycle2.layer.cornerRadius = view_jumpCycle2.frame.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
}
