//
//  
//  JumpRopeResultConstract.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//
//

import UIKit
import ICDeviceManager

// MARK: - View
protocol ResultJumpRopeViewProtocol: AnyObject {

}

// MARK: - Presenter
protocol ResultJumpRopePresenterProtocol {
    func onViewDidLoad()
    func getICSkipDataValue() -> ICSkipData?
}

// MARK: - Interactor Input
protocol ResultJumpRopeInteractorInputProtocol {
    func setSyncDataICSkipJumpRope(accessToken: String?, icSkipDataRealm: ICSkipDataRealm)
}

// MARK: - Interactor Output
protocol ResultJumpRopeInteractorOutputProtocol: AnyObject {
    func onSetSyncDataICSkipJumpRopeFinished(with result: Result<EmptyModel, APIError>, icSkipDataRealm: ICSkipDataRealm)
}

// MARK: - Router
protocol ResultJumpRopeRouterProtocol: BaseRouterProtocol {

}
