//
//  
//  JumpRopeResultInteractorInput.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//
//

import UIKit
import ICDeviceManager

class ResultJumpRopeInteractorInput {
    weak var output: ResultJumpRopeInteractorOutputProtocol?
    var jumpRopeService: JumpRopeServiceProtocol?
}

// MARK: - JumpRopeResultInteractorInputProtocol
extension ResultJumpRopeInteractorInput: ResultJumpRopeInteractorInputProtocol {
    func setSyncDataICSkipJumpRope(accessToken: String?, icSkipDataRealm: ICSkipDataRealm) {
        self.jumpRopeService?.setSyncDataICSkipJumpRope(accessToken: accessToken, icSkipDataRealm: icSkipDataRealm, completion: { [weak self] (result) in
            self?.output?.onSetSyncDataICSkipJumpRopeFinished(with: result.unwrapSuccessModel(), icSkipDataRealm: icSkipDataRealm)
        })
    }
}
