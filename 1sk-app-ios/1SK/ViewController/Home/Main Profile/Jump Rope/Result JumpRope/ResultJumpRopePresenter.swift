//
//  
//  JumpRopeResultPresenter.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//
//

import UIKit
import ICDeviceManager

class ResultJumpRopePresenter {

    weak var view: ResultJumpRopeViewProtocol?
    private var interactor: ResultJumpRopeInteractorInputProtocol
    private var router: ResultJumpRopeRouterProtocol
    
    init(interactor: ResultJumpRopeInteractorInputProtocol,
         router: ResultJumpRopeRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var icSkipData: ICSkipData?
    var device: DeviceRealm?
}

// MARK: - JumpRopeResultPresenterProtocol
extension ResultJumpRopePresenter: ResultJumpRopePresenterProtocol {
    func onViewDidLoad() {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        guard let _icSkipData = self.icSkipData, let _device = self.device else {
            return
        }
        let data: ICSkipDataRealm = ICSkipDataRealm.init(icSkipData: _icSkipData)
        data.device = _device
        self.interactor.setSyncDataICSkipJumpRope(accessToken: accessToken, icSkipDataRealm: data)
    }
    
    func getICSkipDataValue() -> ICSkipData? {
        return self.icSkipData
    }
}

// MARK: - JumpRopeResultInteractorOutput 
extension ResultJumpRopePresenter: ResultJumpRopeInteractorOutputProtocol {
    func onSetSyncDataICSkipJumpRopeFinished(with result: Result<EmptyModel, APIError>, icSkipDataRealm: ICSkipDataRealm) {
        switch result {
        case .success:
            break
            
        case .failure:
            gRealm.write(icSkipDataRealm)
        }
        
        self.router.hideHud()
    }
}
