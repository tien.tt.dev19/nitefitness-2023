//
//  
//  ResultJumpRopeRouter.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//
//

import UIKit
import ICDeviceManager

class ResultJumpRopeRouter: BaseRouter {
    
    static func setupModule(icSkipData: ICSkipData?, device: DeviceRealm?, delegate: ResultJumpRopeViewDelegate?) -> ResultJumpRopeViewController {
        let viewController = ResultJumpRopeViewController()
        let router = ResultJumpRopeRouter()
        let interactorInput = ResultJumpRopeInteractorInput()
        let presenter = ResultJumpRopePresenter(interactor: interactorInput, router: router)
        
        viewController.presenter = presenter
        viewController.delegate = delegate
        presenter.view = viewController
        presenter.icSkipData = icSkipData
        presenter.device = device
        
        interactorInput.output = presenter
        interactorInput.jumpRopeService = JumpRopeService()
        
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - JumpRopeResultRouterProtocol
extension ResultJumpRopeRouter: ResultJumpRopeRouterProtocol {
    
}
