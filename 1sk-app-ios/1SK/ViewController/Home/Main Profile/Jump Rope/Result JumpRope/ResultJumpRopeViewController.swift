//
//  
//  JumpRopeResultViewController.swift
//  1SKConnect
//
//  Created by Valerian on 25/05/2022.
//
//

import UIKit
import ICDeviceManager

protocol ResultJumpRopeViewDelegate: AnyObject {
    func onJumpModeChangeByICDevice(icSkipData: ICSkipData?)
}

class ResultJumpRopeViewController: BaseViewController {
    var presenter: ResultJumpRopePresenterProtocol!
    weak var delegate: ResultJumpRopeViewDelegate?
    
    @IBOutlet weak var view_Navigation: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_Close: UIView!
    
    var icSkipDataChangeMode: ICSkipData?
    var isDismissResult = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Setup
    private func setupInit() {
        self.setInitLayout()
        self.setInitUITableView()
        self.setInitGestureRecognizer()
        
        //self.setInitManagerICDevice()
    }
    
    private func setInitLayout() {
        self.isInteractivePopGestureEnable = false
        self.view_Navigation.addShadow(width: 0, height: 6, color: .black, radius: 3, opacity: 0.05)
    }
    
    func getImageScreenshot() -> UIImage? {
        self.tbv_TableView.removeFromSuperview()
        let image = self.tbv_TableView.screenshot()
        
        self.view_Content.insertSubview(self.tbv_TableView, at: 0)
        self.tbv_TableView.anchor(top: self.view_Navigation.bottomAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor)
        return image
    }
    
    // MARK: - Action
    @IBAction func onShareAction(_ sender: UIButton) {
        let bounds = UIScreen.main.bounds
        UIGraphicsBeginImageContextWithOptions(bounds.size, true, 0.0)
        self.view.drawHierarchy(in: bounds, afterScreenUpdates: false)
        
        guard let snapShot = self.getImageScreenshot() else { return }
        
        let activityViewController = UIActivityViewController(activityItems: [snapShot], applicationActivities: nil)
        let fakeViewController = UIViewController()
        fakeViewController.view.backgroundColor = .clear
        fakeViewController.modalPresentationStyle = .overFullScreen

        activityViewController.completionWithItemsHandler = { [weak fakeViewController] _, _, _, _ in
            if let presentingViewController = fakeViewController?.presentingViewController {
                presentingViewController.dismiss(animated: false, completion: nil)
            } else {
                fakeViewController?.dismiss(animated: false, completion: nil)
            }
        }
        self.present(fakeViewController, animated: true) { [weak fakeViewController] in
            fakeViewController?.present(activityViewController, animated: true, completion: nil)
        }
    }
}

// MARK: - JumpRopeResultViewProtocol
extension ResultJumpRopeViewController: ResultJumpRopeViewProtocol {
    //
}

// MARK: - UITableViewDataSource
extension ResultJumpRopeViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellResultTableViewHeader.self)
        self.tbv_TableView.registerNib(ofType: CellResultTableViewBody.self)
        self.tbv_TableView.registerNib(ofType: CellResultTableViewFooter.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 240
            
        case 1:
            let widthColl = UIScreen.main.bounds.width - 32
            let widthCell = (widthColl - 32) / 3
            let heightCell = widthCell * 2
            return heightCell + 32
            
        case 2:
            let icSkipData = self.presenter.getICSkipDataValue()
            return CGFloat((((icSkipData?.freqs.count ?? 0) * 54) + 62) + 80)
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueCell(ofType: CellResultTableViewHeader.self, for: indexPath)
            cell.model = self.presenter.getICSkipDataValue()
            return cell
            
        case 1:
            let cell = tableView.dequeueCell(ofType: CellResultTableViewBody.self, for: indexPath)
            cell.model = self.presenter.getICSkipDataValue()
            return cell
            
        case 2:
            let cell = tableView.dequeueCell(ofType: CellResultTableViewFooter.self, for: indexPath)
            cell.model = self.presenter.getICSkipDataValue()
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: GestureRecognizer
extension ResultJumpRopeViewController {
    func setInitGestureRecognizer() {
        self.view_Close.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onDismissAction)))
    }
    
    @objc func onDismissAction() {
        if self.icSkipDataChangeMode == nil {
            self.dismiss(animated: true)
            
        } else {
            self.dismiss(animated: true) {
                self.delegate?.onJumpModeChangeByICDevice(icSkipData: self.icSkipDataChangeMode)
            }
        }
    }
}

// MARK: BluetoothManagerICDeviceDelegate
//extension ResultJumpRopeViewController: BLEManagerICDeviceDelegate {
//    func setInitManagerICDevice() {
//        BluetoothManager.shared.delegate_IcDevice = self
//    }
//
//    func onDeviceConnectionChanged(_ device: ICDevice!, state: ICDeviceConnectState) {
//        print("<ManagerICDevice> Result onDeviceConnectionChanged - device.macAddr: \(device.macAddr ?? "NULL") - state: \(state)")
//    }
//
//    func onBleState(_ state: ICBleState) {
//        print("<ManagerICDevice> onBleState: ", state)
//    }
//
//    func onReceiveSkipData(_ device: ICDevice!, data: ICSkipData!) {
//        print("<ManagerICDevice> Result onReceiveSkipData - data ===========================")
//
//        print("<ManagerICDevice> Result onReceiveSkipData - device: (\(device.macAddr ?? "null")")
//
//        print("<ManagerICDevice> Result onReceiveSkipData - data.isStabilized = \(data.mode.rawValue)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.time = \(data.time)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.mode = \(data.mode.rawValue)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.setting = \(data.setting)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.elapsed_time = \(data.elapsed_time)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.actual_time = \(data.actual_time)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.skip_count = \(data.skip_count)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.avg_freq = \(data.avg_freq)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.fastest_freq = \(data.fastest_freq)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.calories_burned = \(data.calories_burned)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.fat_burn_efficiency = \(data.fat_burn_efficiency)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.freq_count = \(data.freq_count)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.most_jump = \(data.most_jump)")
//        print("<ManagerICDevice> Result onReceiveSkipData - data.freqs = \(data.freqs.count)")
//
//        self.icSkipDataChangeMode = data
//        if data.skip_count > 0 && self.isDismissResult {
//            self.isDismissResult = false
//            self.dismiss(animated: true) {
//                print("<ManagerICDevice> Result dismiss")
//                self.delegate?.onJumpModeChangeByICDevice(icSkipData: self.icSkipDataChangeMode)
//            }
//        }
//    }
//}
