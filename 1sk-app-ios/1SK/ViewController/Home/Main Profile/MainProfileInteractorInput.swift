//
//  
//  MainProfileInteractorInput.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MainProfileInteractorInputProtocol {
    func getUserInfo(accessToken: String?)
    func getSubAccounts()
}

// MARK: - Interactor Output Protocol
protocol MainProfileInteractorOutputProtocol: AnyObject {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>)
    func onGetSubAccountsFinished(with result: Result<[AuthModel], APIError>)
}

// MARK: - MainProfile InteractorInput
class MainProfileInteractorInput {
    weak var output: MainProfileInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - MainProfile InteractorInputProtocol
extension MainProfileInteractorInput: MainProfileInteractorInputProtocol {
    func getUserInfo(accessToken: String?) {
        self.authService?.getUserInfo(accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getSubAccounts() {
        self.authService?.getSubAccounts(completion: { [weak self] result in
            self?.output?.onGetSubAccountsFinished(with: result.unwrapSuccessModel())
        })
    }
}
