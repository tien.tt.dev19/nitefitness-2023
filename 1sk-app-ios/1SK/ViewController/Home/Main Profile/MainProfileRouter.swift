//
//  
//  MainProfileRouter.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MainProfileRouterProtocol {
    func popViewController()
    func presentCreateHealthProfileRouter(userParent: UserModel?, delegate: CreateHealthProfileViewDelegate?)
}

// MARK: - MainProfile Router
class MainProfileRouter {
    weak var viewController: MainProfileViewController?
    
    static func setupModule(device: DeviceRealm?) -> MainProfileViewController {
        let viewController = MainProfileViewController()
        let router = MainProfileRouter()
        let interactorInput = MainProfileInteractorInput()
        let viewModel = MainProfileViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MainProfile RouterProtocol
extension MainProfileRouter: MainProfileRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func presentCreateHealthProfileRouter(userParent: UserModel?, delegate: CreateHealthProfileViewDelegate?) {
        let controller = CreateHealthProfileRouter.setupModule(userParent: userParent, delegate: delegate)
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
