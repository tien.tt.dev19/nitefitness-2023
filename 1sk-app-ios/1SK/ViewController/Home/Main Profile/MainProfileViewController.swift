//
//  
//  MainProfileViewController.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol MainProfileViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setLayoutAvatarProfile()
    func setLayoutNameDevice()
    
    func onSwitchProfileSuccess()
    func showAlertSwitchAccountView()
}

// MARK: - MainProfile ViewController
class MainProfileViewController: BaseViewController {
    var router: MainProfileRouterProtocol!
    var viewModel: MainProfileViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Avatar: UIImageView!
    
    @IBOutlet weak var btn_EditName: UIButton!
    @IBOutlet weak var view_Container: UIView!
    
    var childController: UIViewController?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.onViewDidLoad()
        self.setupInit()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setLayoutNameDevice()
        self.setLayoutAvatarProfile()
        self.setChildViewController()
        self.setInitGestureRecognizer()
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.popViewController()
    }
    
    @IBAction func onEditNameAction(_ sender: Any) {
        self.presentAlertEditDeviceNameView()
    }
    
}

// MARK: - Main ViewProtocol
extension MainProfileViewController: MainProfileViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func showAlertSwitchAccountView() {
        self.presentAlertSwitchAccountView()
    }
    
    func onSwitchProfileSuccess() {
        self.setDisconnectICDevice()
        
        self.setLayoutAvatarProfile()
        self.setChildViewController()
    }
    
    func setLayoutAvatarProfile() {
        self.img_Avatar.setImageWith(imageUrl: self.viewModel.device?.profile?.avatar ?? "", placeHolder: R.image.ic_default_avatar())
    }
    
    func setLayoutNameDevice() {
        switch self.viewModel.device?.type {
        case .SCALES:
            self.lbl_Title.text = self.viewModel.device?.name_display?.trimmingCondenseWhitespace() ?? self.viewModel.device?.name?.trimmingCondenseWhitespace() ?? "Cân 1SK"
            
        case .BP:
            self.lbl_Title.text = self.viewModel.device?.name_display?.trimmingCondenseWhitespace() ?? self.viewModel.device?.name?.trimmingCondenseWhitespace() ?? "Máy đo huyết áp"
            
        case .JUMP_ROPE:
            self.lbl_Title.text = self.viewModel.device?.name_display?.trimmingCondenseWhitespace() ?? self.viewModel.device?.name?.trimmingCondenseWhitespace() ?? "Dây nhảy"
            
        case .WATCH:
            self.lbl_Title.text = self.viewModel.device?.name_display?.trimmingCondenseWhitespace() ?? self.viewModel.device?.name?.trimmingCondenseWhitespace() ?? "Đồng hồ"
            
        default:
            self.lbl_Title.text = ""
        }
        
        if self.viewModel.device?.uuid?.count ?? 0 > 0 {
            self.btn_EditName.isHidden = false
            
        } else {
            self.btn_EditName.isHidden = true
        }
    }
}

// MARK: Handler
extension MainProfileViewController {
    func setDisconnectICDevice() {
        guard let icDevice = BluetoothManager.shared.icDevice else { return }
        BluetoothManager.shared.setDisconnectICDevice(mac: icDevice.macAddr)
    }
}

// MARK: Container View
extension MainProfileViewController {
    private func setChildViewController() {
        switch self.viewModel.device?.type {
        case .SCALES:
            self.childController = SmartScalesRouter.setupModule(device: self.viewModel.device, delegate: self)
            
        case .BP:
            self.childController = BloodPressureRouter.setupModule(device: self.viewModel.device, delegate: self)
            
        case .JUMP_ROPE:
            self.childController = JumpRopeRouter.setupModule(device: self.viewModel.device)
            
        case .WATCH:
            self.childController = SmartWatchRouter.setupModule(device: self.viewModel.device)
            
        default:
            self.childController = UIViewController()
        }
        
        self.addChild(self.childController!)
        self.view_Container.addSubview(self.childController!.view)
        
        self.childController!.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        self.childController!.didMove(toParent: self)
    }
}

// MARK: UIGestureRecognizer
extension MainProfileViewController {
    func setInitGestureRecognizer() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.img_Avatar.isUserInteractionEnabled = true
        self.img_Avatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onAvatarAction)))
    }
    
    @objc func onAvatarAction() {
        self.viewModel.onAvatarAction()
    }
}

// MARK: AlertSwitchAccountViewDelegate
extension MainProfileViewController: AlertSwitchAccountViewDelegate {
    func presentAlertSwitchAccountView() {
        let controller = AlertSwitchAccountViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.currentProfile = self.viewModel.device?.profile
        controller.listSubAccount = self.viewModel.listSubAccount
        self.present(controller, animated: false)
    }
    
    func onAddProfileAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let userParent = self.viewModel.listSubAccount.first(where: {$0.accountType == .Account})?.customer else {
                return
            }
            self.router.presentCreateHealthProfileRouter(userParent: userParent, delegate: self)
        }
    }
    
    func onSwitchProfileAction(account: AuthModel) {
        self.viewModel.onSwitchProfileAction(account: account)
    }
}

// MARK: CreateHealthProfileViewDelegate
extension MainProfileViewController: CreateHealthProfileViewDelegate {
    func onCreateSubAccountSuccess(auth: AuthModel?) {
        guard let account = auth else {
            return
        }
        self.viewModel.onSwitchProfileAction(account: account)
    }
}

// MARK: SmartScalesViewDelegate, BloodPressureViewDelegate
extension MainProfileViewController: SmartScalesViewDelegate, BloodPressureViewDelegate {
    func onConnectionAction(device: DeviceRealm?) {
        self.viewModel.device = device
        self.setLayoutNameDevice()
    }
}

// MARK: AlertEditDeviceNameViewDelegate
extension MainProfileViewController: AlertEditDeviceNameViewDelegate {
    func presentAlertEditDeviceNameView() {
        let controller = AlertEditDeviceNameViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.device = self.viewModel.device
        self.present(controller, animated: false)
    }
    
    func onEditDeviceNameSuccess() {
        self.setLayoutNameDevice()
    }
}
