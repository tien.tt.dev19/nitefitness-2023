//
//  
//  MainProfileViewModel.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MainProfileViewModelProtocol {
    func onViewDidLoad()
    func onAvatarAction()
    
    func onSwitchProfileAction(account: AuthModel)
    
    var listSubAccount: [AuthModel] { get set }
    var device: DeviceRealm? { get set }
    
}

// MARK: - MainProfile ViewModel
class MainProfileViewModel {
    weak var view: MainProfileViewProtocol?
    private var interactor: MainProfileInteractorInputProtocol

    init(interactor: MainProfileInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listSubAccount: [AuthModel] = []
    var device: DeviceRealm?
    
    
}

// MARK: - MainProfile ViewModelProtocol
extension MainProfileViewModel: MainProfileViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.onCreateProfileDevice()
        self.getUpdateDeviceProfile()
    }
    
    func onAvatarAction() {
        self.view?.showHud()
        self.interactor.getSubAccounts()
    }
    
    func onSwitchProfileAction(account: AuthModel) {
        guard let customer = account.customer else {
            SKToast.shared.showToast(content: "Hồ sơ người dùng lỗi...")
            return
        }
        
        gRealm.update {
            self.device?.profile?.id = customer.id
            self.device?.profile?.accessToken = account.accessToken
            self.device?.profile?.avatar = customer.avatar
            self.device?.profile?.fullName = customer.fullName
            self.device?.profile?.birthday = customer.birthday
            self.device?.profile?.gender = customer.gender
            self.device?.profile?.height = customer.height
            self.device?.profile?.weight = customer.weight
            self.device?.profile?.blood = customer.blood
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.view?.onSwitchProfileSuccess()
        }
    }
    
    func onCreateProfileDevice() {
        guard self.device?.profile == nil, let type = self.device?.type else {
            return
        }
        if let profile: ProfileRealm = gRealm.objects().first(where: {$0.type == type}) {
            self.device?.profile = profile
            
        } else {
            let profile = ProfileRealm()
            profile.id = gUser?.id
            profile.accessToken = KeyChainManager.shared.accessToken
            profile.fullName = gUser?.fullName
            profile.birthday = gUser?.birthday
            profile.avatar = gUser?.avatar
            profile.gender = gUser?.gender
            profile.height = gUser?.height
            profile.weight = gUser?.weight
            profile.blood = gUser?.blood
            profile.type = type
            
            gRealm.write(profile)
            self.device?.profile = profile
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.view?.setLayoutAvatarProfile()
        }
    }
    
}

// MARK: - SmartScales Handlers
extension MainProfileViewModel {
    func getUpdateDeviceProfile() {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.interactor.getUserInfo(accessToken: accessToken)
    }
}

// MARK: - MainProfile InteractorOutputProtocol
extension MainProfileViewModel: MainProfileInteractorOutputProtocol {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let model):
            gRealm.update {
                self.device?.profile?.avatar = model.avatar
                self.device?.profile?.fullName = model.fullName
                self.device?.profile?.birthday = model.birthday
                self.device?.profile?.gender = model.gender
                self.device?.profile?.height = model.height
                self.device?.profile?.weight = model.weight
                self.device?.profile?.blood = model.blood
            }
            print("gRealm.update MainProfileViewModel profile: \(self.device?.profile?.fullName ?? "NULL") - type: \(self.device?.profile?.type?.rawValue ?? -1)")
            
        case .failure:
            break
        }
    }
    
    func onGetSubAccountsFinished(with result: Result<[AuthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listSubAccount = model
            self.view?.showAlertSwitchAccountView()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
