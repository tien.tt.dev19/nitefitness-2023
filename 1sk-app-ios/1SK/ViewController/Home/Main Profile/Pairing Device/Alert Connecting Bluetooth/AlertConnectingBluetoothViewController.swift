//
//  AlertConnectingBluetoothViewController.swift
//  1SKConnect
//
//  Created by Thaad on 21/07/2022.
//

import UIKit
import CoreBluetooth

protocol AlertConnectingBluetoothViewDelegate: AnyObject {
    func onCancelConnectingAction()
}

class AlertConnectingBluetoothViewController: UIViewController {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_DeviceName: UILabel!
    
    weak var delegate: AlertConnectingBluetoothViewDelegate?
    
    var peripheral: CBPeripheral?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if self.peripheral?.name == SKDevice.Name.CF_516 {
            self.lbl_DeviceName.text = "\(self.peripheral?.name ?? "")BLE"
        } else {
            self.lbl_DeviceName.text = self.peripheral?.name
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    func onDismiss() {
        self.hiddenAnimate()
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onCancelConnectingAction()
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension AlertConnectingBluetoothViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
