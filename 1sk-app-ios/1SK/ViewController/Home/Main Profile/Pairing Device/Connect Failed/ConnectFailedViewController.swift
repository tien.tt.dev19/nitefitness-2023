//
//  ConnectFailedViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import UIKit
import CoreBluetooth

protocol ConnectFailedViewDelegate: AnyObject {
    func onDismissConnectFailedAction()
    func onReconnectAction(peripheral: CBPeripheral?)
}

class ConnectFailedViewController: UIViewController {

    @IBOutlet weak var lbl_Title: UILabel!
    
    weak var delegate: ConnectFailedViewDelegate?
    
    var typeDevice: TypeDevice? {
        didSet {
            self.setInitDataUI()
        }
    }
    
    var peripheral: CBPeripheral?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onDismissAction(_ sender: Any) {
        self.delegate?.onDismissConnectFailedAction()
    }
    
    @IBAction func onReconnectAction(_ sender: Any) {
        switch self.typeDevice {
        case .JUMP_ROPE:
            //self.delegate?.onReconnectAction(icScanDevice: self.icScanDevice)
            break
            
        default:
            self.delegate?.onReconnectAction(peripheral: self.peripheral)
        }
    }
    
    func setInitDataUI() {
        switch self.typeDevice {
        case .SCALES:
            self.lbl_Title.text = "Cân thông minh"
            
        case .SPO2:
            self.lbl_Title.text = "SPO2"
            
        case .BP:
            self.lbl_Title.text = "Máy đo huyết áp"
            
        case .WATCH:
            self.lbl_Title.text = "Vòng đeo"
            
        case .JUMP_ROPE:
            self.lbl_Title.text = "Dây nhảy"
            
        default:
            break
        }
    }
}
