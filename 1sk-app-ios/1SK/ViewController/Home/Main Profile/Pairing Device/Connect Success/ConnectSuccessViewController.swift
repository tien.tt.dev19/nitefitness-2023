//
//  ConnectSuccessViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import UIKit
import CoreBluetooth
import ICDeviceManager

protocol ConnectSuccessViewDelegate: AnyObject {
    func onDismissConnectSuccessAction()
    func onStartMeasureAction(peripheral: CBPeripheral?)
}

class ConnectSuccessViewController: UIViewController {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Finding: UIImageView!
    @IBOutlet weak var lbl_DeviceName: UILabel!
    @IBOutlet weak var lbl_DeviceMAC: UILabel!
    
    weak var delegate: ConnectSuccessViewDelegate?
    
    var typeDevice: TypeDevice?
    
    var peripheral: CBPeripheral?
    var icScanDevice: ICScanDeviceInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.img_Finding.image = UIImage.gifImageWithName("search")
    }
    
    @IBAction func onDismissAction(_ sender: Any) {
        self.delegate?.onDismissConnectSuccessAction()
    }
    
    @IBAction func onStartMeasureAction(_ sender: Any) {
        self.delegate?.onStartMeasureAction(peripheral: peripheral)
    }
    
    func onReloadData() {
        if self.peripheral?.name == SKDevice.Name.CF_516 {
            self.lbl_DeviceName.text = "\(self.peripheral?.name ?? "")BLE"
        } else {
            self.lbl_DeviceName.text = self.peripheral?.name
        }
        
        self.lbl_DeviceMAC.text = self.peripheral?.mac
        
        switch self.typeDevice {
        case .SCALES:
            self.lbl_Title.text = "Cân thông minh"
            
        case .BP:
            self.lbl_Title.text = "Máy đo huyết áp"
            
        case .JUMP_ROPE:
            self.lbl_Title.text = "Dây nhảy"
            self.lbl_DeviceMAC.text = self.icScanDevice?.macAddr
            
        case .WATCH:
            self.lbl_Title.text = "Đồng hồ"
            //self.lbl_DeviceMAC.text = self.zhJBTDevice?.mac
            
        default:
            break
        }
    }
}
