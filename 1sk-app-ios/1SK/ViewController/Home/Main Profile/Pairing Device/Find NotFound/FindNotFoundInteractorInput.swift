//
//  
//  FindNotFoundInteractorInput.swift
//  1SK
//
//  Created by Thaad on 19/12/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol FindNotFoundInteractorInputProtocol {
//    func getDataScalesMeasual(userId: Int, page: Int, perPage: Int, accessToken: String?)
//    func getDataBloodPressure(deviceName: String?, userId: Int, fromDate: Int, toDate: Int, page: Int, perPage: Int, accessToken: String?)
}

// MARK: - Interactor Output Protocol
protocol FindNotFoundInteractorOutputProtocol: AnyObject {
//    func onGetDataScalesMeasualFinished(with result: Result<[BodyFatModel], APIError>)
//    func onGetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>, page: Int, total: Int)
}

// MARK: - FindNotFound InteractorInput
class FindNotFoundInteractorInput {
    weak var output: FindNotFoundInteractorOutputProtocol?
//    var connectService: ConnectServiceProtocol?
}

// MARK: - FindNotFound InteractorInputProtocol
extension FindNotFoundInteractorInput: FindNotFoundInteractorInputProtocol {
//    func getDataScalesMeasual(userId: Int, page: Int, perPage: Int, accessToken: String?) {
//        self.connectService?.getDataScalesRecent(userId: userId, page: page, perPage: perPage, accessToken: accessToken, completion: { [weak self] result in
//            self?.output?.onGetDataScalesMeasualFinished(with: result.unwrapSuccessModel())
//        })
//    }
//
//    func getDataBloodPressure(deviceName: String?, userId: Int, fromDate: Int, toDate: Int, page: Int, perPage: Int, accessToken: String?) {
//        self.connectService?.getDataBloodPressure(deviceName: deviceName, userId: userId, fromDate: fromDate, toDate: toDate, page: page, perPage: perPage, accessToken: accessToken, completion: { [weak self] result in
//            let total = result.getTotal() ?? 0
//            self?.output?.onGetDataBloodPressureFinished(with: result.unwrapSuccessModel(), page: page, total: total)
//        })
//    }
}
