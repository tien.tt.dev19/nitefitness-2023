//
//  
//  FindNotFoundRouter.swift
//  1SK
//
//  Created by Thaad on 19/12/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol FindNotFoundRouterProtocol {
    func showMeasureHistoryRouter(device: DeviceRealm?)
    func showHistoryMeasureBpRouter(device: DeviceRealm?)
    func showLeaderboardJumpRopeRouter(device: DeviceRealm?)
}

// MARK: - FindNotFound Router
class FindNotFoundRouter {
    weak var viewController: FindNotFoundViewController?
    
    static func setupModule() -> FindNotFoundViewController {
        let viewController = FindNotFoundViewController()
        let router = FindNotFoundRouter()
        let interactorInput = FindNotFoundInteractorInput()
        let viewModel = FindNotFoundViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        //interactorInput.connectService = ConnectService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - FindNotFound RouterProtocol
extension FindNotFoundRouter: FindNotFoundRouterProtocol {
    func showMeasureHistoryRouter(device: DeviceRealm?) {
        let controller = MeasureHistoryRouter.setupModule(device: device)
        self.viewController?.present(controller, animated: true)
    }
    
    func showHistoryMeasureBpRouter(device: DeviceRealm?) {
        let controller = HistoryMeasureBpRouter.setupModule(device: device)
        self.viewController?.present(controller, animated: true)
    }
    
    func showLeaderboardJumpRopeRouter(device: DeviceRealm?) {
        let controller = LeaderboardJumpRopeRouter.setupModule(device: device)
        self.viewController?.present(controller, animated: true)
    }
}
