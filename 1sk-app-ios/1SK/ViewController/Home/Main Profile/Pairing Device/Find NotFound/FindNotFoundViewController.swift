//
//  FindNotFoundViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import UIKit

protocol FindNotFoundViewDelegate: AnyObject {
    func onDismissFindNotFoundAction()
    func onReFindingAction()
}

// MARK: - ViewProtocol
protocol FindNotFoundViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
//    func setInitDataUI()
//    func setBtnHistory(isHidden: Bool)
}

class FindNotFoundViewController: BaseViewController {
    var router: FindNotFoundRouterProtocol!
    var viewModel: FindNotFoundViewModelProtocol!
    weak var delegate: FindNotFoundViewDelegate?
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var btn_History: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Init
    private func setupInit() {
        //self.setBtnHistory(isHidden: true)
    }
    
    func setLayoutInit() {
        self.lbl_Title.text = ""
        self.lbl_Content.text = "Không tìm thấy thiết bị\n\(self.viewModel.device?.name_display ?? "")"
        
        switch self.viewModel.device?.type {
        case .SCALES:
            self.btn_History.setTitle("Xem lịch sử đo", for: .normal)
            
        case .BP:
            self.btn_History.setTitle("Xem lịch sử đo", for: .normal)
            
        case .JUMP_ROPE:
            self.btn_History.setTitle("Xem thành tích tập luyện", for: .normal)
            
        default:
            self.btn_History.setTitle("Không xác định", for: .normal)
        }
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.delegate?.onDismissFindNotFoundAction()
    }
    
    @IBAction func onReconnectAction(_ sender: Any) {
        self.delegate?.onReFindingAction()
    }
    
    @IBAction func onHistoryAction(_ sender: Any) {
        switch self.viewModel.device?.type {
        case .SCALES:
            self.router.showMeasureHistoryRouter(device: self.viewModel.device)

        case .BP:
            self.router.showHistoryMeasureBpRouter(device: self.viewModel.device)
            
        case .JUMP_ROPE:
            self.router.showLeaderboardJumpRopeRouter(device: self.viewModel.device)
            
        case .WATCH:
            break
            
        case .SPO2:
            break

        default:
            break
        }
    }
    
}

// MARK: - FindNotFound ViewProtocol
extension FindNotFoundViewController: FindNotFoundViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
//    func setInitDataUI() {
//        self.lbl_Title.text = ""
//        self.lbl_Content.text = "Không tìm thấy thiết bị\n\(self.viewModel.device?.name_display ?? "")"
//
//        switch self.viewModel.device?.type {
//        case .SCALES:
//            self.btn_History.setTitle("Xem lịch sử đo", for: .normal)
//
//        case .BP:
//            self.btn_History.setTitle("Xem lịch sử đo", for: .normal)
//
//        case .JUMP_ROPE:
//            self.btn_History.setTitle("Xem thành tích tập luyện", for: .normal)
//
//        default:
//            self.btn_History.setTitle("Không xác định", for: .normal)
//        }
//    }
    
//    func setBtnHistory(isHidden: Bool) {
//        self.btn_History.isHidden = isHidden
//    }
}
