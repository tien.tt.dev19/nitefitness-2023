//
//  
//  FindNotFoundViewModel.swift
//  1SK
//
//  Created by Thaad on 19/12/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol FindNotFoundViewModelProtocol {
    func onViewDidLoad()
//    func onInitData()
    
    var device: DeviceRealm? { get set }
    var listBodyFat: [BodyFatModel] { get set }
}

// MARK: - FindNotFound ViewModel
class FindNotFoundViewModel {
    weak var view: FindNotFoundViewProtocol?
    private var interactor: FindNotFoundInteractorInputProtocol

    init(interactor: FindNotFoundInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    
    var listBodyFat: [BodyFatModel] = []
    var listBloodPressure: [BloodPressureModel] = []
}

// MARK: - FindNotFound ViewModelProtocol
extension FindNotFoundViewModel: FindNotFoundViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
    
//    func onInitData() {
//        self.view?.setInitDataUI()
        
//        guard let userId = self.device?.profile?.id else {
//            return
//        }
//        guard let accessToken = self.device?.profile?.accessToken else {
//            return
//        }
//
//        switch self.device?.type {
//        case .SCALES:
//            self.interactor.getDataScalesMeasual(userId: userId, page: 1, perPage: 1, accessToken: accessToken)
//
//        case .BP:
//            self.interactor.getDataScalesMeasual(userId: userId, page: 1, perPage: 1, accessToken: accessToken)
//
//        case .JUMP_ROPE:
//            break
//            
//        case .WATCH:
//            break
//
//        case .SPO2:
//            break
//
//        default:
//            break
//        }
//    }
}

// MARK: - FindNotFound InteractorOutputProtocol
extension FindNotFoundViewModel: FindNotFoundInteractorOutputProtocol {
//    func onGetDataScalesMeasualFinished(with result: Result<[BodyFatModel], APIError>) {
//        switch result {
//        case .success(let listModel):
//            self.listBodyFat = listModel
//
//            if self.listBodyFat.count > 0 {
//                self.view?.setBtnHistory(isHidden: false)
//
//            } else {
//                self.view?.setBtnHistory(isHidden: true)
//            }
//
//        case .failure:
//            self.view?.setBtnHistory(isHidden: true)
//        }
//        self.view?.hideHud()
//    }
//
//    func onGetDataBloodPressureFinished(with result: Result<[BloodPressureModel], APIError>, page: Int, total: Int) {
//        switch result {
//        case .success(let listModel):
//            self.listBloodPressure = listModel
//
//            if self.listBloodPressure.count > 0 {
//                self.view?.setBtnHistory(isHidden: false)
//
//            } else {
//                self.view?.setBtnHistory(isHidden: true)
//            }
//
//        case .failure:
//            self.view?.setBtnHistory(isHidden: true)
//        }
//    }
}
