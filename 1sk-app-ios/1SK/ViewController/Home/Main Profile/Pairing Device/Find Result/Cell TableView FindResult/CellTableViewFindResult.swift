//
//  CellTableViewFindResult.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import UIKit
import CoreBluetooth
import ICDeviceManager

class CellTableViewFindResult: UITableViewCell {

    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Mac: UILabel!
    
    @IBOutlet weak var view_StatusPair: UIView!
    
    var typeDevice: TypeDevice?
    
    var peripheral: CBPeripheral? {
        didSet {
            if self.peripheral?.name == SKDevice.Name.CF_516 {
                self.lbl_Name.text = "\(self.peripheral?.name ?? "")BLE"
            } else {
                self.lbl_Name.text = self.peripheral?.name
            }
            
            self.lbl_Mac.text = self.peripheral?.mac
            if self.peripheral?.mac.count == 0 {
                self.lbl_Mac.text = self.peripheral?.uuid.suffix(12).pairs.joined(separator: ":")
            }
            
            self.setIconTypeDevice()
        }
    }
    
    var icScanDevice: ICScanDeviceInfo? {
        didSet {
            self.lbl_Name.text = self.icScanDevice?.name
            self.lbl_Mac.text = self.icScanDevice?.macAddr
            
            self.setIconTypeDevice()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setIconTypeDevice() {
        switch self.typeDevice {
        case .SCALES:
            self.img_Icon.image = R.image.ic_find_scales()

        case .SPO2:
            self.img_Icon.image = R.image.ic_find_bp()

        case .BP:
            self.img_Icon.image = R.image.ic_find_bp()
            
        case .WATCH:
            self.img_Icon.image = R.image.ic_find_watch()

        case .JUMP_ROPE:
            self.img_Icon.image = R.image.ic_find_jump_rope()

        default:
            break
        }
    }
}
