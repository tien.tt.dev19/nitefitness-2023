//
//  FindResultViewController.swift
//  1SK
//
//  Created by Thaad on 20/07/2022.
//

import UIKit
import CoreBluetooth
import ICDeviceManager

protocol FindResultViewDelegate: AnyObject {
    func onDismissFindResultAction()
    func onConnectionAction(device: DeviceRealm)
}

class FindResultViewController: UIViewController {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    weak var delegate: FindResultViewDelegate?
    
    var typeDevice: TypeDevice?
    
    var listPeripherals: [CBPeripheral] = []
    var listICScanDevice: [ICScanDeviceInfo] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setInitUITableView()
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.delegate?.onDismissFindResultAction()
    }
}

// MARK: UITableViewDataSource
extension FindResultViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewFindResult.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        print("<Bluetooth> Connect onDidDiscover reloadData")
    }
    
    // UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 8
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
        
    // TablView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.typeDevice {
        case .JUMP_ROPE:
            return self.listICScanDevice.count
            
        default:
            return self.listPeripherals.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewFindResult.self, for: indexPath)
        cell.typeDevice = self.typeDevice
        
        switch self.typeDevice {
        case .JUMP_ROPE:
            cell.icScanDevice = self.listICScanDevice[indexPath.row]
            
        default:
            cell.peripheral = self.listPeripherals[indexPath.row]
        }
        
        return cell
    }
}

// MARK: UITableViewDelegate
extension FindResultViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.typeDevice {
        case .JUMP_ROPE:
            let peripheral = self.listICScanDevice[indexPath.row]
            guard let device: DeviceRealm = gRealm.objects().first(where: {$0.mac == peripheral.macAddr}) else {
                SKToast.shared.showToast(content: "Lỗi\nThiết bị không tồn tại")
                return
            }
            self.delegate?.onConnectionAction(device: device)
            
        default:
            let peripheral = self.listPeripherals[indexPath.row]
            guard let device: DeviceRealm = gRealm.objects().first(where: {$0.uuid == peripheral.uuid}) else {
                SKToast.shared.showToast(content: "Lỗi\nThiết bị không tồn tại")
                return
            }
            self.delegate?.onConnectionAction(device: device)
        }
    }
}
