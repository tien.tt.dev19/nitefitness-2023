//
//  FindingDeviceViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import UIKit

protocol FindingDeviceViewDelegate: AnyObject {
    func onDismissFindingDeviceAction()
}

class FindingDeviceViewController: UIViewController {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var img_Finding: UIImageView!
    
    weak var delegate: FindingDeviceViewDelegate?
    var typeDevice: TypeDevice? {
        didSet {
            self.setInitDataUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.img_Finding.image = UIImage.gifImageWithName("search")
    }
    
    @IBAction func onDismissAction(_ sender: Any) {
        self.delegate?.onDismissFindingDeviceAction()
    }

    func setInitDataUI() {
        self.lbl_Title.text = "Lựa chọn thiết bị kết nối"
        
//        switch self.typeDevice {
//        case .SCALES:
//            self.lbl_Title.text = "Cân thông minh"
//
//        case .SPO2:
//            self.lbl_Title.text = "SPO2"
//
//        case .BP:
//            self.lbl_Title.text = "Máy đo huyết áp"
//
//        case .WATCH:
//            self.lbl_Title.text = "Vòng đeo"
//
//        case .JUMP_ROPE:
//            self.lbl_Title.text = "Dây nhảy"
//
//        default:
//            break
//        }
    }

}
