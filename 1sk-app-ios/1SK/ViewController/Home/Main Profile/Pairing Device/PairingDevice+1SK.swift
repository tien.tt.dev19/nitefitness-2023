//
//  PairingDevice+BLE.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import Foundation
import CoreBluetooth

// MARK: - BluetoothManager Handle
extension PairingDeviceViewController {
    func setDelegateBluetoothManager() {
        print("<Bluetooth> 1SK Pairing setDelegateBluetoothManager")
        
        if let name = self.viewModel.device?.name {
            BluetoothManager.shared.delegate_BluetoothManager = self
            BluetoothManager.shared.listDeviceName = [name]
            
        } else {
            BluetoothManager.shared.delegate_BluetoothManager = nil
            BluetoothManager.shared.listDeviceName = []
        }
    }
    
    func setScanPeripherals() {
        print("<Bluetooth> 1SK Pairing setScanPeripherals")
        
        BluetoothManager.shared.setScanStartPeripherals()
        BluetoothManager.shared.setStartTimer(timeout: 10.0)
    }
}

// MARK: - BluetoothManagerDelegate
extension PairingDeviceViewController: BluetoothManagerDelegate {
    func onDidUpdateState(central: CBCentralManager) {
        print("<Bluetooth> 1SK Pairing onDidUpdateState")
    }
    
    func onDidTimeoutScanPeripheralDevice() {
        print("<Bluetooth> 1SK Pairing onDidTimeoutScanDevice")
        BluetoothManager.shared.setScanStopPeripherals()
        
        let countPeripheral = self.viewModel.listPeripherals.count
        switch countPeripheral {
        case 0:
            self.setStateUIView(state: .NotFound)
            
        case 1:
            guard let peripheral = self.viewModel.listPeripherals.first else {
                self.setStateUIView(state: .NotFound)
                return
            }
            guard let device: DeviceRealm = gRealm.objects().first(where: {$0.uuid == peripheral.uuid}) else {
                self.setStateUIView(state: .NotFound)
                return
            }
            self.onConnectionAction(device: device)
            
        default:
            self.viewFindResult.listPeripherals = self.viewModel.listPeripherals
            if self.state != .Result {
                self.setStateUIView(state: .Result)
            }
        }
    }
    
    func onDidDiscover(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Pairing onDidDiscover peripheral: \(peripheral.mac) - \(peripheral.name ?? "NULL") - uuid:\(peripheral.uuid) - id: \(peripheral.identifier)")
        
        guard self.viewModel.listPeripherals.first(where: {$0.uuid == peripheral.uuid}) == nil else {
            return
        }
        self.viewModel.listPeripherals.append(peripheral)
        self.savePeripheralDevice(peripheral: peripheral)
    }
    
    func onDidFailToConnect(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Pairing onDidFailToConnect")
    }
    
    func onDidConnected(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Pairing onDidConnected")
    }
    
    func onDidDisconnect(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Pairing onDidDisconnect")
    }
    
    func savePeripheralDevice(peripheral: CBPeripheral) {
        guard peripheral.paired == false else {
            return
        }
        
        let object = DeviceRealm()
        object.uuid = peripheral.uuid
        object.mac = peripheral.mac
        object.name = peripheral.name
        object.type = self.viewModel.device?.type
        
        switch self.viewModel.device?.type {
        case .SCALES:
            switch object.name {
            case SKDevice.Name.CF_398:
                object.code = SKDevice.Code.CF_398
                
                let listScale: [DeviceRealm] = gRealm.objects().filter({$0.name == SKDevice.Name.CF_398})
                if listScale.count < 1 {
                    object.name_display = SKDevice.NameDisplay.CF_398
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.CF_398) (\(listScale.count))"
                }
                
            case SKDevice.Name.CF_539:
                object.code = SKDevice.Code.CF_539
                
                let listScale: [DeviceRealm] = gRealm.objects().filter({$0.name == SKDevice.Name.CF_539})
                if listScale.count < 1 {
                    object.name_display = SKDevice.NameDisplay.CF_539
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.CF_539) (\(listScale.count))"
                }
                
            case SKDevice.Name.CF_516:
                object.code = SKDevice.Code.CF_516
                object.name = "\(SKDevice.Name.CF_516)BLE"
                
                let listScale: [DeviceRealm] = gRealm.objects().filter({$0.name == SKDevice.Name.CF_516 || $0.name == "Smart scale CF516BLE"})
                if listScale.count < 1 {
                    object.name_display = SKDevice.NameDisplay.CF_516
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.CF_516) (\(listScale.count))"
                }
                
            default:
                break
            }
            
        case .BP:
            object.name = peripheral.name?.uppercased()
            
            switch object.name {
            case SKDevice.Name.DBP_6292B:
                object.code = SKDevice.Code.DBP_6292B
                
                let listBp: [DeviceRealm] = gRealm.objects().filter({$0.name == SKDevice.Name.DBP_6292B})
                if listBp.count < 1 {
                    object.name_display = SKDevice.NameDisplay.DBP_6292B
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.DBP_6292B) (\(listBp.count))"
                }
                
            case SKDevice.Name.DBP_6277B:
                object.code = SKDevice.Code.DBP_6277B
                
                let listBp: [DeviceRealm] = gRealm.objects().filter({$0.name == SKDevice.Name.DBP_6277B})
                if listBp.count < 1 {
                    object.name_display = SKDevice.NameDisplay.DBP_6277B
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.DBP_6277B) (\(listBp.count))"
                }
                
            default:
                break
            }
            
        case .WATCH:
            switch object.name {
            case "S5":
                object.code = object.name
                
                let listWatch: [DeviceRealm] = gRealm.objects().filter({$0.name == "S5"})
                if listWatch.count < 1 {
                    object.name_display = "S5"
                } else {
                    object.name_display = "S5 (\(listWatch.count))"
                }
                
            default:
                break
            }
            
        default:
            break
        }
        
        gRealm.write(object)
    }
}
