//
//  PairingDevice+ICDevice.swift
//  1SK
//
//  Created by Thaad on 27/12/2022.
//

import Foundation
import ICDeviceManager

// MARK: ICDeviceManager
extension PairingDeviceViewController {
    func setDelegateICDeviceManager() {
        print("<Bluetooth> IC Pairing setDelegateICDeviceManager")
        BluetoothManager.shared.delegate_ICDeviceManager = self
        
        guard let icDevice = BluetoothManager.shared.icDevice else { return }
        BluetoothManager.shared.setDisconnectICDevice(mac: icDevice.macAddr)
    }
    
    func setScanICDevice() {
        print("<Bluetooth> IC Pairing setScanICDevice")
        BluetoothManager.shared.setScanStartICDevice()
        BluetoothManager.shared.setStartTimer(timeout: 10.0)
    }
    
}

// MARK: ICDeviceManagerDelegate
extension PairingDeviceViewController: SKICDeviceManagerDelegate {
    func onInitFinish(_ bSuccess: Bool) {}
    
    func onBleState(_ state: ICBleState) {}
    
    func onDeviceConnectionChanged(_ device: ICDevice!, state: ICDeviceConnectState) {}
    
    func onReceiveSkipData(_ device: ICDevice!, data: ICSkipData!) {}
    
    func onDidTimeoutScanICDevice() {
        print("<Bluetooth> IC Pairing onDidTimeoutScanICDevice")
        BluetoothManager.shared.setScanStopICDevice()
        
        let countPeripheral = self.viewModel.listICScanDevice.count
        switch countPeripheral {
        case 0:
            self.setStateUIView(state: .NotFound)
            
        case 1:
            guard let peripheral = self.viewModel.listICScanDevice.first else {
                self.setStateUIView(state: .NotFound)
                return
            }
            guard let device: DeviceRealm = gRealm.objects().first(where: {$0.mac == peripheral.macAddr}) else {
                self.setStateUIView(state: .NotFound)
                return
            }
            self.onConnectionAction(device: device)
            
        default:
            self.viewFindResult.listICScanDevice = self.viewModel.listICScanDevice
            if self.state != .Result {
                self.setStateUIView(state: .Result)
            }
        }
    }
    
    func onScanResult(_ deviceInfo: ICScanDeviceInfo!) {
        guard deviceInfo.name == self.viewModel.device?.name else {
            return
        }
        guard self.viewModel.listICScanDevice.first(where: {$0.macAddr == deviceInfo.macAddr}) == nil else {
            return
        }
        print("<Bluetooth> IC Pairing onScanResult deviceInfo: \(deviceInfo.macAddr ?? "NULL") - name: \(deviceInfo.name ?? "NULL")")
        
        self.viewModel.listICScanDevice.append(deviceInfo)
        self.savePeripheralDevice(deviceInfo: deviceInfo)
    }
    
    func onDidConnectedICDeviceSuccess(icScanDevice: ICScanDeviceInfo) {
        print("<Bluetooth> IC Pairing onDidConnectedICDeviceSuccess")
    }
    
    func savePeripheralDevice(deviceInfo: ICScanDeviceInfo) {
        let device: DeviceRealm? = gRealm.objects().first(where: {$0.mac == deviceInfo.macAddr})
        if device == nil {
            let object = DeviceRealm()
            object.uuid = deviceInfo.macAddr
            object.mac = deviceInfo.macAddr
            object.name = deviceInfo.name
            object.code = deviceInfo.name
            object.type = self.viewModel.device?.type
            
            switch deviceInfo.name {
            case SKDevice.Name.RS_1949LB:
                object.code = SKDevice.Code.RS_1949LB
                
                let listDevice: [DeviceRealm] = gRealm.objects().filter({$0.name == SKDevice.Name.RS_1949LB})
                if listDevice.count < 1 {
                    object.name_display = SKDevice.NameDisplay.RS_1949LB
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.RS_1949LB) (\(listDevice.count))"
                }
                
            case SKDevice.Name.RS_2047LB:
                object.code = SKDevice.Code.RS_2047LB
                
                let listDevice: [DeviceRealm] = gRealm.objects().filter({$0.name ==  SKDevice.Name.RS_2047LB})
                if listDevice.count < 1 {
                    object.name_display = SKDevice.NameDisplay.RS_2047LB
                } else {
                    object.name_display = "\(SKDevice.NameDisplay.RS_2047LB) (\(listDevice.count))"
                }
                
            default:
                break
            }

            gRealm.write(object)
            
            print("<Bluetooth> IC Pairing savePeripheralDevice mac: \(deviceInfo.macAddr ?? "NULL") - name: \(deviceInfo.name ?? "NULL")")
        }
    }
}
