//
//  
//  PairingDeviceInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol PairingDeviceInteractorInputProtocol {
    
}

// MARK: - Interactor Output Protocol
protocol PairingDeviceInteractorOutputProtocol: AnyObject {
    
}

// MARK: - PairingDevice InteractorInput
class PairingDeviceInteractorInput {
    weak var output: PairingDeviceInteractorOutputProtocol?
}

// MARK: - PairingDevice InteractorInputProtocol
extension PairingDeviceInteractorInput: PairingDeviceInteractorInputProtocol {
    
}
