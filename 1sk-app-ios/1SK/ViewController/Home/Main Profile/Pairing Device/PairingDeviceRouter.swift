//
//  
//  PairingDeviceRouter.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol PairingDeviceRouterProtocol {
    func onDismiss()
}

// MARK: - PairingDevice Router
class PairingDeviceRouter {
    weak var viewController: PairingDeviceViewController?
    
    static func setupModule(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?) -> PairingDeviceViewController {
        let viewController = PairingDeviceViewController()
        let router = PairingDeviceRouter()
        let interactorInput = PairingDeviceInteractorInput()
        let viewModel = PairingDeviceViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - PairingDevice RouterProtocol
extension PairingDeviceRouter: PairingDeviceRouterProtocol {
    func onDismiss() {
        BluetoothManager.shared.setScanStopPeripherals()
        
        self.viewController?.dismiss(animated: true)
    }
}
