//
//  
//  PairingDeviceViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//
//

import UIKit
import SnapKit
import CoreBluetooth

enum StateViewConnectDevice {
    case Pair
    case Finding
    case Result
    case NotFound
    case Success
    case Failed
}

protocol PairingDeviceViewDelegate: AnyObject {
    func onConnectionAction(device: DeviceRealm)
}

// MARK: - ViewProtocol
protocol PairingDeviceViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - ConnectDevice ViewController
class PairingDeviceViewController: BaseViewController {
    var router: PairingDeviceRouterProtocol!
    var viewModel: PairingDeviceViewModelProtocol!
    weak var delegate: PairingDeviceViewDelegate?
    
    lazy var viewStartConnect = StartConnectViewController()
    lazy var viewFindingDevice = FindingDeviceViewController()
    lazy var viewFindNotFound = FindNotFoundRouter.setupModule()
    lazy var viewFindResult = FindResultViewController()
    lazy var viewConnectSuccess = ConnectSuccessViewController()
    lazy var viewConnectFailed = ConnectFailedViewController()
    
    var alertConnectingBluetooth: AlertConnectingBluetoothViewController?
    var isConnecting = true
    
    var state: StateViewConnectDevice = .Pair
    
    var timer: Timer?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.showStartConnectView()
        self.setInitBluetoothManager()
    }
    
    private func setInitBluetoothManager() {
        switch self.viewModel.device?.type {
        case .JUMP_ROPE:
            self.setDelegateICDeviceManager()
            
        default:
            self.setDelegateBluetoothManager()
        }
    }
    
}

// MARK: - PairingDevice ViewProtocol
extension PairingDeviceViewController: PairingDeviceViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - Add Subview
extension PairingDeviceViewController {
    func setStateUIView(state: StateViewConnectDevice) {
        
        self.hideHud()
        self.state = state
        
        switch state {
        case .Pair:
            self.addChildView(childViewController: self.viewStartConnect)
            self.viewStartConnect.device = self.viewModel.device
            self.viewStartConnect.delegate = self
            
        case .Finding:
            self.addChildView(childViewController: self.viewFindingDevice)
            self.viewFindingDevice.typeDevice = self.viewModel.device?.type
            self.viewFindingDevice.delegate = self
            
            switch self.viewModel.device?.type {
            case .JUMP_ROPE:
                self.setScanICDevice()
                
            default:
                self.setScanPeripherals()
            }
            
            
        case .Result:
            self.addChildView(childViewController: self.viewFindResult)
            self.viewFindResult.typeDevice = self.viewModel.device?.type
            self.viewFindResult.delegate = self
        
        case .NotFound:
            self.addChildView(childViewController: self.viewFindNotFound)
            self.viewFindNotFound.viewModel.device = self.viewModel.device
            self.viewFindNotFound.setLayoutInit()
            self.viewFindNotFound.delegate = self
            
        case .Success:
            self.addChildView(childViewController: self.viewConnectSuccess)
            self.viewConnectSuccess.typeDevice = self.viewModel.device?.type
            self.viewConnectSuccess.delegate = self
            self.viewConnectSuccess.peripheral = BluetoothManager.shared.peripheral
            self.viewConnectSuccess.onReloadData()
            
        case .Failed:
            self.addChildView(childViewController: self.viewConnectFailed)
            self.viewConnectFailed.typeDevice = self.viewModel.device?.type
            self.viewConnectFailed.delegate = self
        }
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: StartConnectViewDelegate
extension PairingDeviceViewController: StartConnectViewDelegate {
    func showStartConnectView() {
        self.setStateUIView(state: .Pair)
    }
    
    func onDismissStartConnectAction() {
        self.dismiss(animated: true)
    }
    
    func onStartConnectAction() {
        self.onCheckBluetoothState()
    }
}

// MARK: ConnectFailedViewDelegate
extension PairingDeviceViewController: ConnectFailedViewDelegate {
    func onDismissConnectFailedAction() {
        self.router.onDismiss()
    }
    
    func onReconnectAction(peripheral: CBPeripheral?) {
        self.presentAlertConnectingBluetoothVC(peripheral: peripheral)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            if self.isConnecting == true && peripheral != nil {
                BluetoothManager.shared.setConnectDevice(peripheral: peripheral!)
            }
        }
    }
}

// MARK: FindingDeviceViewDelegate
extension PairingDeviceViewController: FindingDeviceViewDelegate {
    func onDismissFindingDeviceAction() {
        self.router.onDismiss()
    }
    
}

// MARK: FindResultViewDelegate
extension PairingDeviceViewController: FindResultViewDelegate {
    func onDismissFindResultAction() {
        self.router.onDismiss()
    }
    
    func onConnectionAction(device: DeviceRealm) {
        self.delegate?.onConnectionAction(device: device)
        self.router.onDismiss()
    }
}

// MARK: FindNotFoundViewDelegate
extension PairingDeviceViewController: FindNotFoundViewDelegate {
    func onDismissFindNotFoundAction() {
        self.router.onDismiss()
    }
    
    func onReFindingAction() {
        self.setStateUIView(state: .Finding)
    }
}

// MARK: ConnectSuccessViewDelegate
extension PairingDeviceViewController: ConnectSuccessViewDelegate {
    func onDismissConnectSuccessAction() {
        self.router.onDismiss()
    }
    
    func onStartMeasureAction(peripheral: CBPeripheral?) {
        self.dismiss(animated: true) {
            //
        }
    }
}

// MARK: AlertSettingBluetoothView
extension PairingDeviceViewController {
    func presentAlertSettingBluetoothVC() {
        let controller = AlertSettingBluetoothViewController()
        controller.modalPresentationStyle = .custom
        self.present(controller, animated: false)
    }
}

// MARK: AlertConnectingBluetoothViewDelegate
extension PairingDeviceViewController: AlertConnectingBluetoothViewDelegate {
    func presentAlertConnectingBluetoothVC(peripheral: CBPeripheral?) {
        self.alertConnectingBluetooth = AlertConnectingBluetoothViewController()
        self.alertConnectingBluetooth?.modalPresentationStyle = .custom
        self.alertConnectingBluetooth?.delegate = self
        self.alertConnectingBluetooth?.peripheral = peripheral
        
        self.present(self.alertConnectingBluetooth!, animated: false)
    }
    
    func onCancelConnectingAction() {
        self.isConnecting = false
    }
}

// MARK: Bluetooth State
extension PairingDeviceViewController {
    func onCheckBluetoothState() {
        switch BluetoothManager.shared.stateCBManager() {
        case .poweredOn:
            print("<Bluetooth> Pairing onCheckBluetoothState central.state is .poweredOn")
            self.setStateUIView(state: .Finding)

        case .poweredOff:
            print("<Bluetooth> Pairing onCheckBluetoothState central.state is .poweredOff")
            self.presentAlertSettingBluetoothVC()
            
        case .resetting:
            print("<Bluetooth> Pairing onCheckBluetoothState central.state is .resetting")
            self.alertDefault(title: "Bluetooth", message: "Đang kết nối thiết bị")

        case .unsupported:
            print("<Bluetooth> Pairing onCheckBluetoothState central.state is .unsupported")
            self.alertDefault(title: "Bluetooth", message: "Thiết bị này không hỗ trợ")

        case .unauthorized:
            print("<Bluetooth> Connect onCheckBluetoothState central.state is .unauthorized")
            self.alertOpenSettingsURLString(title: "\"1SK\" muốn sử dụng Bluetooth", message: "1SK cần cho phép truy cập Bluetooth để có thể tiếp tục với tính năng này, nó sẽ giúp bạn có thể kết nối với các thiết bị thông minh. Nếu đồng ý, bạn vui lòng đến Cài đặt và bấm cho phép")
            
        case .unknown:
            print("<Bluetooth> Pairing onCheckBluetoothState central.state is .unknown")
            self.alertDefault(title: "Bluetooth", message: "Không xác định")
            
        default:
            print("<Bluetooth> Pairing onCheckBluetoothState central.state is .default")
            self.alertDefault(title: "Bluetooth", message: "Chưa khởi tạo")
        }
    }
}
