//
//  
//  PairingDeviceViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//
//

import UIKit
import CoreBluetooth
import ICDeviceManager

// MARK: - ViewModelProtocol
protocol PairingDeviceViewModelProtocol {
    func onViewDidLoad()
    
    var device: DeviceRealm? { get set }
    var listPeripherals: [CBPeripheral] { get set }
    var listICScanDevice: [ICScanDeviceInfo] { get set }}

// MARK: - PairingDevice ViewModel
class PairingDeviceViewModel {
    weak var view: PairingDeviceViewProtocol?
    private var interactor: PairingDeviceInteractorInputProtocol

    init(interactor: PairingDeviceInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    var listPeripherals: [CBPeripheral] = []
    var listICScanDevice: [ICScanDeviceInfo] = []
}

// MARK: - PairingDevice ViewModelProtocol
extension PairingDeviceViewModel: PairingDeviceViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - PairingDevice InteractorOutputProtocol
extension PairingDeviceViewModel: PairingDeviceInteractorOutputProtocol {
    
}
