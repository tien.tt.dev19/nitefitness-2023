//
//  StartConnectViewController.swift
//  1SKConnect
//
//  Created by Thaad on 20/07/2022.
//

import UIKit

protocol StartConnectViewDelegate: AnyObject {
    func onDismissStartConnectAction()
    func onStartConnectAction()
}

class StartConnectViewController: UIViewController {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Note: UILabel!
    @IBOutlet weak var img_Image: UIImageView!
    
    weak var delegate: StartConnectViewDelegate?
    
    var device: DeviceRealm? {
        didSet {
            self.setInitDataUI()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func onDismissAction(_ sender: Any) {
        self.delegate?.onDismissStartConnectAction()
    }
    
    @IBAction func onStartConnectAction(_ sender: Any) {
        self.delegate?.onStartConnectAction()
    }
    
    func setInitDataUI() {
        switch self.device?.type {
        case .SCALES:
            self.lbl_Title.text = "Ghép nối với cân"
            self.lbl_Note.text = "Vui lòng đứng lên cân"
            
            switch self.device?.code {
            case "CF398":
                self.img_Image.image = R.image.img_scales_CF398()

            case "CF516":
                self.img_Image.image = R.image.img_scales_CF516()

            case "CF539":
                self.img_Image.image = R.image.img_scales_CF539()

            default:
                self.img_Image.image = R.image.img_scales_CF398()
            }
            
        case .BP:
            self.lbl_Title.text = "Ghép nối với máy đo huyết áp"
            self.lbl_Note.text = "Nhấn và giữ nút \"MEM\" trên máy đo huyết áp cho tới khi chữ \"Boo\" nhấp nháy trên màn hình hiển thị"
            self.img_Image.image = R.image.img_bp_start_connect()
            
        case .JUMP_ROPE:
            self.lbl_Title.text = "Ghép nối với dây nhảy"
            self.lbl_Note.text = "Bật nút công tắc trên dây nhảy"
            self.img_Image.image = R.image.img_jump_rope()
            
        case .WATCH:
            self.lbl_Title.text = "Ghép nối với vòng đeo"
            self.lbl_Note.text = "Bật nguồn vòng đeo"
            self.img_Image.image = R.image.img_bp_start_connect()
            
            
        default:
            break
        }
    }
}
