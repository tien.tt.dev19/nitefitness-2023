//
//  CellTableViewSmartScalesPost.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//

import UIKit

class CellTableViewSmartScalesPost: UITableViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    var post: PostHealthModel? {
        didSet {
            self.img_Image.setImageWith(imageUrl: self.post?.postImages?.medium ?? "")
            self.lbl_Title.text = self.post?.postTitle
            
            if let date = self.post?.postDate?.toDate(.ymdhms) {
                self.lbl_Time.text = date.toString(.dmySlash)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
