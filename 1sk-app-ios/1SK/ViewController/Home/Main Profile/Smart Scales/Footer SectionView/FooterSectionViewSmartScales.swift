//
//  FooterSectionViewSmartScales.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//

import UIKit
import ImageSlideshow

class FooterSectionViewSmartScales: UITableViewHeaderFooterView {

    @IBOutlet weak var view_SlideShow: ImageSlideshow!

    var sliderBanner: SliderBannerModel? {
        didSet {
            self.setInitDataSlideShow()
        }
    }
    
    var slidePage = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

// MARK: ImageSlideshowDelegate
extension FooterSectionViewSmartScales: ImageSlideshowDelegate {
    private func setInitDataSlideShow() {
        guard let sliderModel = self.sliderBanner else {
            return
        }
        guard let listBanners = sliderModel.items, listBanners.count > 0 else {
            return
        }
        
        var imageSource: [InputSource] = []
        for banner in listBanners {
            if let image = banner.image {
                if let url = URL(string: image) {
                    let kingfisherSource = KingfisherSource(url: url, placeholder: R.image.img_default(), options: nil)
                    imageSource.append(kingfisherSource)
                }
            }
        }
        
        self.setInitSlideShow(imageSource: imageSource)
    }
    
    private func setInitSlideShow(imageSource: [InputSource]) {
        self.view_SlideShow.slideshowInterval = 3.0
        
        self.view_SlideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        self.view_SlideShow.contentScaleMode = UIViewContentMode.scaleAspectFill

        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = R.color.mainColor()
        pageControl.pageIndicatorTintColor = .white
        self.view_SlideShow.pageIndicator = pageControl

        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        self.view_SlideShow.activityIndicator = DefaultActivityIndicator()
        self.view_SlideShow.delegate = self

        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.view_SlideShow.setImageInputs(imageSource)

        self.view_SlideShow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureSlideshow)))
    }
    
    @objc func onTapGestureSlideshow() {
        guard let banner = self.sliderBanner?.items?[self.slidePage] else {
            return
        }
        
        switch banner.typeBanner {
        case .link:
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                guard let url = URL(string: banner.link ?? "") else {
                    return
                }
                UIApplication.shared.open(url)
            }

//        case .product:
//            break
//            guard let productId = banner.productId else {
//                return
//            }
//
//            self.viewModel.onDidSelectBanner(productId: productId)

        default:
            break
        }
    }
    
    // ImageSlideshowDelegate
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        self.slidePage = page
    }
}
