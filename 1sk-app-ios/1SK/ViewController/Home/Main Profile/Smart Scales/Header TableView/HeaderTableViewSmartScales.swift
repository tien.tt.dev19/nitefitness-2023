//
//  HeaderTableViewSmartScales.swift
//  1SK
//
//  Created by Thaad on 27/10/2022.
//

import Foundation
import UIKit
import WebKit

protocol HeaderTableViewSmartScalesDelegate: AnyObject {
    func onScalseMeasualAction()
    func onConnectWithDeviceAction()
    
    func onHistoryAction()
    func onAdviseAction()
    
    func onDidFinishLoadWebView()
}

class HeaderTableViewSmartScales: UIView {
    weak var delegate: HeaderTableViewSmartScalesDelegate?
    
    @IBOutlet weak var img_Scales: UIImageView!
    
    @IBOutlet weak var btn_Measure: UIButton!
    @IBOutlet weak var view_NotPair: UIStackView!
    
    @IBOutlet weak var btn_History: UIButton!
    
    @IBOutlet weak var view_History: UIView!
    @IBOutlet weak var view_Intro: UIView!
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    
    @IBOutlet weak var view_Consult: UIView!
    @IBOutlet weak var lbl_Consult: UILabel!
    
    @IBOutlet weak var view_Advise: UIView!
    @IBOutlet weak var view_AdviseContent: UIView!
    
    @IBOutlet weak var web_Intro: WKWebView!
    @IBOutlet weak var constraint_height_IntroView: NSLayoutConstraint!
    
    private var codeDevice: String? = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayout()
        self.setInitWKWebView()
    }
    
    func setInitLayout() {
        self.view_AdviseContent.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        
        self.view_Advise.isHidden = true
        self.view_Consult.isHidden = true
        self.btn_History.isHidden = true
        
    }
    
    func setLayoutState(isPaired: Bool) {
        if isPaired == true {
            self.view_NotPair.isHidden = true
            self.btn_Measure.isHidden = false
            
            self.view_History.isHidden = false
            self.view_Intro.isHidden = true
            
        } else {
            self.view_NotPair.isHidden = false
            self.btn_Measure.isHidden = true
            
            self.view_History.isHidden = true
            self.view_Intro.isHidden = false
        }
    }
    
    func setImageDeviceType(code: String?) {
        self.codeDevice = code
        
        switch code {
        case "CF398":
            self.img_Scales.image = R.image.img_scales_CF398()
            
            if let config = gConfigs.first(where: {$0.key == "gt_can_1SK_SmartScale68"}) {
                self.setWebKitData(url: config.value)
            }
            
        case "CF516":
            self.img_Scales.image = R.image.img_scales_CF516()
            
            if let config = gConfigs.first(where: {$0.key == "gt_can_Smart_scale_CF516"}) {
                self.setWebKitData(url: config.value)
            }
            
        case "CF539":
            self.img_Scales.image = R.image.img_scales_CF539()
            
            if let config = gConfigs.first(where: {$0.key == "gt_can_Smart_scale_CF539"}) {
                self.setWebKitData(url: config.value)
            }
            
        default:
            self.img_Scales.image = R.image.img_scales_CF398()
        }
    }
    
    func setDataRecentResults(bodyFat: BodyFatModel) {
        self.lbl_Weight.text = "\(bodyFat.weight ?? 0) kg"
        self.lbl_Status.text = bodyFat.bodyFatSDK?.bmiIndex?.status_VN ?? "Không xác định"
        
        if let createdAt = bodyFat.createdAt {
            if let date = createdAt.toDate(.ymdhms) {
                self.lbl_Time.text = date.toString(.hmdmy)
            }
        }
        
        //self.view_Advise.isHidden = false
        self.btn_History.isHidden = false
    }
    
    func setUpdateDataConsult(consult: ConsultModel?) {
        if let content = consult?.content {
            self.lbl_Consult.text = content
            self.view_Consult.isHidden = false
        } else {
            self.view_Consult.isHidden = true
        }
    }
    
    // MARK: - Action
    @IBAction func onMeasureAction(_ sender: Any) {
        self.delegate?.onScalseMeasualAction()
    }
    
    @IBAction func onConnectWithDeviceAction(_ sender: Any) {
        self.delegate?.onConnectWithDeviceAction()
    }
    
    @IBAction func onSeeMoreWithDeviceAction(_ sender: Any) {
        var urlProduct: String?
        
        switch self.codeDevice {
        case "CF398":
            if let config = gConfigs.first(where: {$0.key == "mua_can_1SK_SmartScale68"}) {
                urlProduct = config.value
            }
            
        case "CF516":
            if let config = gConfigs.first(where: {$0.key == "mua_can_Smart_scale_CF516"}) {
                urlProduct = config.value
            }
            
        case "CF539":
            if let config = gConfigs.first(where: {$0.key == "mua_can_Smart_scale_CF539"}) {
                urlProduct = config.value
            }
            
        default:
            break
        }
        
        guard let url = URL(string: urlProduct ?? "") else { return }
        UIApplication.shared.open(url)
    }
    
    @IBAction func onHistoryAction(_ sender: Any) {
        self.delegate?.onHistoryAction()
    }
    
    @IBAction func onAdviseAction(_ sender: Any) {
        self.delegate?.onAdviseAction()
    }
}

// MARK: - WKNavigationDelegate
extension HeaderTableViewSmartScales: WKNavigationDelegate {
    func setInitWKWebView() {
        self.web_Intro.navigationDelegate = self
        
        self.web_Intro.scrollView.showsHorizontalScrollIndicator = false
        self.web_Intro.scrollView.pinchGestureRecognizer?.isEnabled = false
        self.web_Intro.scrollView.isScrollEnabled = false
    }
    
    func setWebKitData(url: String?) {
        guard let _url = URL(string: url ?? "") else {
            return
        }
        self.web_Intro.load(URLRequest(url: _url))
    }
    
    private func setHeightWebView() {
        self.web_Intro.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_Intro.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if let _height = height as? CGFloat {
                        print("web_WebView didFinish height: ", _height)
                        self.constraint_height_IntroView.constant = _height
                        self.delegate?.onDidFinishLoadWebView()
                    }
                })
            }
        })
    }
    
    // WKNavigationDelegate
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("web_WebView didFinish ")
        self.setHeightWebView()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            print("<webView> decidePolicyFor url:", url)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.setHeightWebView()
            }
        }
        
        decisionHandler(.allow)
    }
}
