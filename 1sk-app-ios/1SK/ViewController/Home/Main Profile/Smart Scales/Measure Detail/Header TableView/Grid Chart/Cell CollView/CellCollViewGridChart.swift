//
//  CellCollViewGridChart.swift
//  1SKConnect
//
//  Created by Elcom Corp on 05/11/2021.
//

import UIKit

class CellCollViewGridChart: UICollectionViewCell {
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var lbl_Value: UILabel!

    var bodyType: BodyTypeModel? {
        didSet {
            self.setData()
        }
    }

    override var isSelected: Bool {
        didSet {
            self.setSelected(isSelected: self.isSelected)
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

// MARK: Helpers
extension CellCollViewGridChart {
    private func setData() {
        guard let model = self.bodyType else {
            return
        }
        self.lbl_Value.text = model.title
        self.setSelected(isSelected: model.isSelected)
    }

    private func setSelected(isSelected: Bool) {
        self.view_Content.backgroundColor = isSelected ? UIColor(hex: "#00C2C5") : UIColor(hex: "#F0F0F0")
        self.lbl_Value.font = isSelected ? R.font.robotoMedium(size: 14) : R.font.robotoRegular(size: 14)
        self.lbl_Value.textColor = isSelected ? .white : R.color.title()
    }
}
