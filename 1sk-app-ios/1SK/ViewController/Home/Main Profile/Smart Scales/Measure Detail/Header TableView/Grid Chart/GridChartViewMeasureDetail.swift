//
//  ScaleResultCollectionChart.swift
//  1SKConnect
//
//  Created by Elcom Corp on 05/11/2021.
//

import UIKit

class GridChartViewMeasureDetail: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var fatLabel: UILabel!
    @IBOutlet weak var lowLabel: UILabel!
    @IBOutlet weak var highLabel: UILabel!
    @IBOutlet weak var standarLabel: UILabel!

    @IBOutlet weak var firstVerticalView: UIView!
    @IBOutlet weak var lastVerticalView: UIView!
    @IBOutlet weak var firstHorizontalView: UIView!
    @IBOutlet weak var lastHorizontalView: UIView!

    @IBOutlet weak var coll_Result: UICollectionView!

    var listBodyType = [BodyTypeModel(title: "Béo ẩn", bodyType: .lFatMuscle),
                        BodyTypeModel(title: "Béo phì", bodyType: .obesFat),
                        BodyTypeModel(title: "Béo chắc", bodyType: .muscleFat),
                        BodyTypeModel(title: "Thiếu vận động", bodyType: .lackofexercise),
                        BodyTypeModel(title: "Cân đối", bodyType: .standard),
                        BodyTypeModel(title: "Cơ bắp", bodyType: .muscular),
                        BodyTypeModel(title: "Gầy", bodyType: .thin),
                        BodyTypeModel(title: "Vận động viên điền kinh", bodyType: .standardMuscle),
                        BodyTypeModel(title: "Vận động viên thể hình", bodyType: .lThinMuscle)]
    
    var bodyTypeSelected: BodyTypeModel?
    
    // MARK: Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    /// Setup default value
    func commonInit() {
        let nib = UINib(nibName: "GridChartViewMeasureDetail", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        self.contentView.frame = bounds
        self.addSubview(self.contentView)
        
        self.setInitUICollectionView()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        [self.fatLabel, highLabel, standarLabel, lowLabel].forEach { $0?.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2) }
        self.firstVerticalView.roundCorner(corners: [.topLeft, .topRight], radius: 3)
        self.lastVerticalView.roundCorner(corners: [.bottomLeft, .bottomRight], radius: 3)

        self.firstHorizontalView.roundCorner(corners: [.topLeft, .bottomLeft], radius: 3)
        self.lastHorizontalView.roundCorner(corners: [.topRight, .bottomRight], radius: 3)
    }
    
    func setUpView(bodyFat: BodyFatSDK) {
        self.listBodyType.forEach { body in
            if body.bodyType == bodyFat.bodyType {
                body.isSelected = true
                self.bodyTypeSelected = body
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.coll_Result.reloadData()
        }

    }
}

// MARK: UICollectionViewDataSource
extension GridChartViewMeasureDetail: UICollectionViewDataSource, UICollectionViewDelegate {
    private func setInitUICollectionView() {
        self.coll_Result.registerNib(ofType: CellCollViewGridChart.self)
        
        self.coll_Result.dataSource = self
        self.coll_Result.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listBodyType.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollViewGridChart.self, for: indexPath)
        cell.bodyType = self.listBodyType[indexPath.item]
        return cell
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension GridChartViewMeasureDetail: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 4
        let width: CGFloat = (collectionView.frame.width - (padding * 2)) / 3
        let height: CGFloat = (collectionView.frame.height - (padding * 2)) / 3 //78
        
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
}
