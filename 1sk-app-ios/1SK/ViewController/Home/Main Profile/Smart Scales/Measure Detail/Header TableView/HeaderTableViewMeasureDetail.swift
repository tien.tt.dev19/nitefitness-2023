//
//  HeaderTableViewMeasureDetail.swift
//  1SK
//
//  Created by Thaad on 28/11/2022.
//

import Foundation
import UIKit

protocol HeaderTableViewMeasureDetailDelegate: AnyObject {
    func onAdviseAction()
}

class HeaderTableViewMeasureDetail: UIView {
    weak var delegate: HeaderTableViewMeasureDetailDelegate?
    
    @IBOutlet weak var view_Ruler: UIView!
    @IBOutlet weak var view_RulerChart: RulerChartViewMeasureDetail!
    
    @IBOutlet weak var view_Grid: UIView!
    @IBOutlet weak var view_GridChart: GridChartViewMeasureDetail!
    
    @IBOutlet weak var view_Index: UIView!
    @IBOutlet weak var lbl_Index: UILabel!
    
    @IBOutlet weak var view_Bulkhead: UIView!
    
    @IBOutlet weak var view_Status: UIView!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_StatusTitle: UILabel!
    
    @IBOutlet weak var view_Consult: UIView!
    @IBOutlet weak var lbl_Consult: UILabel!
    
    @IBOutlet weak var view_Advise: UIView!
    @IBOutlet weak var view_AdviseContent: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayout()
    }
    
    func setInitLayout() {
        self.view_AdviseContent.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
    }
    
    func setUpdateDataCharts(index: IndexProtocol?, bodyFat: BodyFatSDK?) {
        guard let index = index, let bodyFat = bodyFat else {
            self.view_Ruler.isHidden = true
            self.view_Grid.isHidden = true
            return
        }
        
        self.view_GridChart.setUpView(bodyFat: bodyFat)
        self.view_RulerChart.setUpView(with: index, bodyFat: bodyFat)

        // Index UI
        if let value = index.value {
            switch index {
            case is BodyType:
                self.lbl_Status.text = index.status_VN
                
            case is BMR, is Fat:
                if let val = value as? (Double, Gender, Int) {
                    var valueString = "\(val.0.toString())"
                    if !String.isNilOrEmpty(index.unit) {
                        valueString = "\(val.0.toString()) (\(index.unit))"
                    }
                    self.lbl_Index.text = valueString
                }
                
            case is Muscle, is Bone:
                if let val = value as? (Double, Gender) {
                    var valueString = "\(val.0.toString())"
                    if !String.isNilOrEmpty(index.unit) {
                        valueString = "\(val.0.toString()) (\(index.unit))"
                    }
                    self.lbl_Index.text = valueString
                }
                
            case is Water:
                if let val = value as? (Double, Int) {
                    var valueString = "\(val.0.toString())"
                    if !String.isNilOrEmpty(index.unit) {
                        valueString = "\(val.0.toString()) (\(index.unit))"
                    }
                    self.lbl_Index.text = valueString
                }
                
            default:
                if let val = value as? Double {
                    var valueString = "\(val.toString())"
                    if !String.isNilOrEmpty(index.unit) {
                        valueString = "\(val.toString()) (\(index.unit))"
                    }
                    self.lbl_Index.text = valueString
                }
            }
        }
        
        // Status UI
        self.lbl_Status.text = index.status_VN
        
        // Grid UI
        if index is BodyType {
            self.view_Ruler.isHidden = true
            self.view_Grid.isHidden = false
            self.view_Index.isHidden = true
            self.view_Status.isHidden = false

            self.lbl_Status.text = self.view_GridChart.bodyTypeSelected?.title
            self.lbl_StatusTitle.text = "Vóc dáng:"

        } else if index is LeanBodyMass || index is BodyStandard {
            self.view_Ruler.isHidden = true
            self.view_Grid.isHidden = true
            self.view_Status.isHidden = true
            self.lbl_StatusTitle.text = "Kết quả:"
            
        } else {
            self.view_Ruler.isHidden = false
            self.view_Grid.isHidden = true
            self.lbl_StatusTitle.text = "Kết quả:"
        }
    }
    
    func setUpdateDataConsult(consult: ConsultModel?) {
        if let content = consult?.content {
            self.lbl_Consult.text = content
            self.view_Consult.isHidden = false
        } else {
            self.view_Consult.isHidden = true
        }
    }
    
    @IBAction func onAdviseAction(_ sender: Any) {
        self.delegate?.onAdviseAction()
    }
}
