//
//  RulerChartViewMeasureDetail.swift
//  1SKConnect
//
//  Created by Thaad on 26/07/2022.
//

import UIKit

class RulerChartViewMeasureDetail: UIView {
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var valueSlider: UISlider!

    // first view
    @IBOutlet weak var view_First: UIView!
    @IBOutlet weak var view_FirstLine: UIView!
    @IBOutlet weak var lbl_FirstValue: UILabel!
    @IBOutlet weak var lbl_FirstDesc: UILabel!
    @IBOutlet weak var constraint_width_FirstView: NSLayoutConstraint!

    // second view
    @IBOutlet weak var view_Second: UIView!
    @IBOutlet weak var view_SecondLine: UIView!
    @IBOutlet weak var lbl_SecondValue: UILabel!
    @IBOutlet weak var lbl_SecondDesc: UILabel!
    @IBOutlet weak var constraint_width_SecondView: NSLayoutConstraint!

    // third view
    @IBOutlet weak var view_Third: UIView!
    @IBOutlet weak var view_ThirdLine: UIView!
    @IBOutlet weak var lbl_ThirdValue: UILabel!
    @IBOutlet weak var lbl_ThirdDesc: UILabel!
    @IBOutlet weak var constraint_width_ThirdView: NSLayoutConstraint!

    // fourth view
    @IBOutlet weak var view_Fourth: UIView!
    @IBOutlet weak var view_FourthLine: UIView!
    @IBOutlet weak var lbl_FourthValue: UILabel!
    @IBOutlet weak var lbl_FourthDesc: UILabel!
    @IBOutlet weak var constraint_width_FourthView: NSLayoutConstraint!

    // last view
    @IBOutlet weak var view_Last: UIView!
    @IBOutlet weak var view_LastLine: UIView!
    @IBOutlet weak var lbl_LastDesc: UILabel!
    @IBOutlet weak var constraint_width_LastView: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    /// Setup default value
    func commonInit() {
        let nib = UINib(nibName: "RulerChartViewMeasureDetail", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        self.contentView.frame = bounds
        self.addSubview(self.contentView)
    }

    // MARK: Lifecycles
    override func layoutSubviews() {
        super.layoutSubviews()
        self.valueSlider.setThumbImage(R.image.ic_thumb_image()!, for: .normal)
        self.view_FirstLine.roundCorner(corners: [.topLeft, .bottomLeft], radius: 3)
        self.view_LastLine.roundCorner(corners: [.topRight, .bottomRight], radius: 3)
    }

}

// MARK: Helpers BodyFatRealm
extension RulerChartViewMeasureDetail {
    func setUpView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            switch index {
            case is BMI:
                self.setBmiView(with: index, bodyFat: bodyFat)

            case is BMR:
                self.setBmrView(with: index, bodyFat: bodyFat)

            case is Muscle:
                self.setMuscleView(with: index, bodyFat: bodyFat)

            case is Bone:
                self.setBoneView(with: index, bodyFat: bodyFat)

            case is Water:
                self.setWaterView(with: index, bodyFat: bodyFat)

            case is Protein:
                self.setProteinView(with: index, bodyFat: bodyFat)

            case is Fat:
                self.setFatView(with: index, bodyFat: bodyFat)

            case is ObesityFatLevel:
                self.setObesityFatLevelView(with: index, bodyFat: bodyFat)

            case is VisceralFatLevel:
                self.setVisceralFatLevelView(with: index, bodyFat: bodyFat)

            case is SubcutaneousFat:
                self.setSubcutaneousFatView(with: index, bodyFat: bodyFat)

            default:
                break
            }
        }
    }

    // MARK: BMI
    func setBmiView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_SecondLine.backgroundColor = R.color.standard()
        self.view_ThirdLine.backgroundColor = R.color.overweight()
        self.view_FourthLine.backgroundColor = R.color.overweight1()
        self.view_LastLine.backgroundColor = R.color.overweight2()

        [self.lbl_FirstDesc, self.lbl_SecondDesc, self.lbl_ThirdDesc, self.lbl_FourthDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.bmiRatingList, ratingList.count >= 4 else { return }
        
        [self.lbl_FirstValue, self.lbl_SecondValue, self.lbl_ThirdValue, self.lbl_FourthValue].enumerated().forEach { (i, label) in
            label?.text = index.ratingsValue[i].toString()
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentSecond = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentThird = ((ratingList[2] - ratingList[1]) / total) * 100
        let percentFourth = ((ratingList[3] - ratingList[2]) / total) * 100
        let percentLast = ((max - ratingList[3]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let secondConstant = percentSecond * percentWidth
        let thirdConstant = percentThird * percentWidth
        let fourthConstant = percentFourth * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_SecondView.constant = secondConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_FourthView.constant = fourthConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? Double
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value ?? 0)
    }
    
    // MARK: BMR
    func setBmrView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance > 0 else {
            self.isHidden = true
            return
        }

        self.constraint_width_SecondView.constant = 0
        self.constraint_width_ThirdView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Third.isHidden = true
        self.view_Fourth.isHidden = true
        
        [self.constraint_width_FirstView, self.constraint_width_LastView].forEach { $0?.constant = self.frame.width / 2 }

        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_LastLine.backgroundColor = R.color.standard()
        
        [self.lbl_FirstDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let rating = bodyFat.bmrRatingList?.first else {
            return
        }
        self.lbl_FirstValue.text = rating.toString()

        self.valueSlider.minimumValue = Float(0)
        self.valueSlider.maximumValue = Float(rating * 2)
        if let val = index.value as? (Double, Gender, Int) {
            self.valueSlider.value = Float(val.0)
        }
        
    }
    
    // MARK: Muscle
    func setMuscleView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance > 0 else {
            self.isHidden = true
            return
        }

        self.constraint_width_SecondView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Fourth.isHidden = true
        
        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_ThirdLine.backgroundColor = R.color.standard()
        self.view_LastLine.backgroundColor = R.color.good()

        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.muscleRatingList, ratingList.count >= 2 else {
            return
        }
        [self.lbl_FirstValue, self.lbl_ThirdValue].enumerated().forEach { (i, lable) in
            lable.text = ratingList[i].toString()
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentLast = ((max - ratingList[1]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? (Double, Gender)
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value?.0 ?? 0)
    }
    
    // MARK: Bone
    func setBoneView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance != 0 else {
            self.isHidden = true
            return
        }

        self.constraint_width_SecondView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Fourth.isHidden = true
        
        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_ThirdLine.backgroundColor = R.color.standard()
        self.view_LastLine.backgroundColor = R.color.good()

        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.boneRatingList, ratingList.count >= 2 else {
            return
        }
        [self.lbl_FirstValue, self.lbl_ThirdValue].enumerated().forEach { (i, lable) in
            lable.text = ratingList[i].toString()
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentLast = ((max - ratingList[1]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? (Double, Gender)
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value?.0 ?? 0)
    }

    // MARK: Water
    func setWaterView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance != 0 else {
            self.isHidden = true
            return
        }
        
        self.constraint_width_SecondView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Fourth.isHidden = true

        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_ThirdLine.backgroundColor = R.color.standard()
        self.view_LastLine.backgroundColor = R.color.good()

        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.waterRatingList, ratingList.count >= 2 else {
            return
        }
        [self.lbl_FirstValue, self.lbl_ThirdValue].enumerated().forEach { (i, lable) in
            lable.text = "\(ratingList[i].toString())%"
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentLast = ((max - ratingList[1]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? (Double, Int)
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value?.0 ?? 0)
    }

    // MARK: Protein
    func setProteinView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance != 0 else {
            self.isHidden = true
            return
        }

        self.constraint_width_SecondView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Fourth.isHidden = true

        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_ThirdLine.backgroundColor = R.color.standard()
        self.view_LastLine.backgroundColor = R.color.high()

        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.proteinRatingList, ratingList.count >= 2 else {
            return
        }
        [self.lbl_FirstValue, self.lbl_ThirdValue].enumerated().forEach { (i, lable) in
            lable.text = "\(ratingList[i].toString())%"
        }

        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentLast = ((max - ratingList[1]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? Double
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value ?? 0)
    }

    // MARK: Fat
    func setFatView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance != 0 else {
            self.isHidden = true
            return
        }

        self.constraint_width_SecondView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Fourth.isHidden = true

        self.view_FirstLine.backgroundColor = R.color.overweight()
        self.view_ThirdLine.backgroundColor = R.color.standard()
        self.view_LastLine.backgroundColor = R.color.overweight2()
        
        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingListFull = bodyFat.fatRatingList, ratingListFull.count == 4 else { return }
        let ratingList = [ratingListFull[0], ratingListFull[2]]
        
        [self.lbl_FirstValue, self.lbl_ThirdValue].enumerated().forEach { (i, lable) in
            lable.text = "\(ratingList[i].toString())%"
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentLast = ((max - ratingList[1]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? (Double, Gender, Int)
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value?.0 ?? 0)
    }

    // MARK: Obesity Fat Level
    func setObesityFatLevelView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_SecondLine.backgroundColor = R.color.standard()
        self.view_ThirdLine.backgroundColor = R.color.overweight()
        self.view_FourthLine.backgroundColor = R.color.overweight1()
        self.view_LastLine.backgroundColor = R.color.overweight2()

        [self.lbl_FirstDesc, self.lbl_SecondDesc, self.lbl_ThirdDesc, self.lbl_FourthDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.bmiRatingList, ratingList.count >= 4 else { return }
        
        [self.lbl_FirstValue, self.lbl_SecondValue, self.lbl_ThirdValue, self.lbl_FourthValue].enumerated().forEach { (i, label) in
            label?.text = index.ratingsValue[i].toString()
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentSecond = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentThird = ((ratingList[2] - ratingList[1]) / total) * 100
        let percentFourth = ((ratingList[3] - ratingList[2]) / total) * 100
        let percentLast = ((max - ratingList[3]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let secondConstant = percentSecond * percentWidth
        let thirdConstant = percentThird * percentWidth
        let fourthConstant = percentFourth * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_SecondView.constant = secondConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_FourthView.constant = fourthConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? Double
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value ?? 0)
    }

    // MARK: Visceral Fat Level
    func setVisceralFatLevelView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        self.constraint_width_SecondView.constant = 0
        self.constraint_width_FourthView.constant = 0
        
        self.view_Second.isHidden = true
        self.view_Fourth.isHidden = true
        
        self.view_FirstLine.backgroundColor = R.color.standard()
        self.view_ThirdLine.backgroundColor = R.color.overweight1()
        self.view_LastLine.backgroundColor = R.color.overweight2()
        
        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.visceralFatLevelRatingList, ratingList.count >= 2 else {
            return
        }
        [self.lbl_FirstValue, self.lbl_ThirdValue].enumerated().forEach { (i, lable) in
            lable.text = ratingList[i].toString()
        }
        
        guard
            //let rattingFirst = ratingList.first,
            let rattingLast = ratingList.last
        else { return }
        
        //let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        //let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = 1.0 // rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentLast = ((max - ratingList[1]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? Double
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value ?? 0)
    }

    // MARK: SubcutaneousFat
    func setSubcutaneousFatView(with index: IndexProtocol, bodyFat: BodyFatSDK) {
        guard let impedance = bodyFat.impedance, impedance != 0 else {
            self.isHidden = true
            return
        }

        self.constraint_width_SecondView.constant = 0
        self.view_Second.isHidden = true

        self.view_FirstLine.backgroundColor = R.color.thin()
        self.view_ThirdLine.backgroundColor = R.color.standard()
        self.view_FourthLine.backgroundColor = R.color.overweight1()
        self.view_LastLine.backgroundColor = R.color.overweight2()
        
        [self.lbl_FirstDesc, self.lbl_ThirdDesc, self.lbl_FourthDesc, self.lbl_LastDesc].enumerated().forEach { (i, lable) in
            lable.text = index.ratingsStatus[i]
        }
        
        guard let ratingList = bodyFat.subcutaneousFatRatingList, ratingList.count == 3 else { return }
        
        [self.lbl_FirstValue, self.lbl_ThirdValue, self.lbl_FourthValue].enumerated().forEach { (i, label) in
            label?.text = ratingList[i].toString()
        }
        
        guard let rattingFirst = ratingList.first, let rattingLast = ratingList.last else { return }
        
        let indexAfterFirst = 1
        let indexBeforeLast = (ratingList.count - 2)
        
        let spaceFisrt = ratingList[indexAfterFirst] - rattingFirst
        let spaceLast = rattingLast - ratingList[indexBeforeLast]
        
        let min = rattingFirst - spaceFisrt
        let max = rattingLast + spaceLast
        
        let percentWidth = self.frame.width/100
        let total = (max - min)
        
        let percentFirst = ((ratingList[0] - min) / total) * 100
        let percentThird = ((ratingList[1] - ratingList[0]) / total) * 100
        let percentFourth = ((ratingList[2] - ratingList[1]) / total) * 100
        let percentLast = ((max - ratingList[2]) / total) * 100
        
        let firstConstant = percentFirst * percentWidth
        let thirdConstant = percentThird * percentWidth
        let fourthConstant = percentFourth * percentWidth
        let lastConstant = percentLast * percentWidth
        
        self.constraint_width_FirstView.constant = firstConstant
        self.constraint_width_ThirdView.constant = thirdConstant
        self.constraint_width_FourthView.constant = fourthConstant
        self.constraint_width_LastView.constant = lastConstant
        
        let value = index.value as? Double
        
        self.valueSlider.minimumValue = Float(min)
        self.valueSlider.maximumValue = Float(max)
        self.valueSlider.value = Float(value ?? 0)
        
    }
}
