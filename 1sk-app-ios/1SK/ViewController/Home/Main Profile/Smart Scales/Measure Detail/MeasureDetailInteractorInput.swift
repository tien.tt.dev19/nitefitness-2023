//
//  
//  MeasureDetailInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasureDetailInteractorInputProtocol {
    func getPostsScales(tag: String, index: String)
    func getSliderBanner(typeSlider: SliderType)
    func getConsultIndex(index: String, status: String, gender: Gender, age: Int, weight: Double)
}

// MARK: - Interactor Output Protocol
protocol MeasureDetailInteractorOutputProtocol: AnyObject {
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>)
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>)
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>)
}

// MARK: - MeasureDetail InteractorInput
class MeasureDetailInteractorInput {
    weak var output: MeasureDetailInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
    var connectService: ConnectServiceProtocol?
    var workPressService: WorkPressServiceProtocol?
}

// MARK: - MeasureDetail InteractorInputProtocol
extension MeasureDetailInteractorInput: MeasureDetailInteractorInputProtocol {
    func getPostsScales(tag: String, index: String) {
        self.workPressService?.getPostsScales(tag: tag, index: index, completion: { [weak self] result in
            self?.output?.onGetPostsScalesFinished(with: result.unwrapSuccessModelWP())
        })
    }
    
    func getSliderBanner(typeSlider: SliderType) {
        self.storeService?.getSliderBanner(typeSlider: typeSlider, completion: { [weak self] result in
            self?.output?.onGetSliderBannerFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getConsultIndex(index: String, status: String, gender: Gender, age: Int, weight: Double) {
        self.connectService?.getConsultIndexScales(index: index, status: status, gender: gender, age: age, weight: weight, completion: { [weak self] result in
            self?.output?.onGetConsultIndexFinished(with: result.unwrapSuccessModel())
        })
    }
}
