//
//  
//  MeasureDetailRouter.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasureDetailRouterProtocol {
    func onDismiss()
    func showChatMessageRouter()
}

// MARK: - MeasureDetail Router
class MeasureDetailRouter {
    weak var viewController: MeasureDetailViewController?
    
    static func setupModule(index: IndexProtocol?, bodyFat: BodyFatSDK?) -> MeasureDetailViewController {
        let viewController = MeasureDetailViewController()
        let router = MeasureDetailRouter()
        let interactorInput = MeasureDetailInteractorInput()
        let viewModel = MeasureDetailViewModel(interactor: interactorInput)

        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.index = index
        viewModel.bodyFat = bodyFat
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        interactorInput.connectService = ConnectService()
        interactorInput.workPressService = WorkPressService()
        router.viewController = viewController

        return viewController
    }
}

// MARK: - MeasureDetail RouterProtocol
extension MeasureDetailRouter: MeasureDetailRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
    func showChatMessageRouter() {
        let controller = ChatMessageRouter.setupModule()
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
}
