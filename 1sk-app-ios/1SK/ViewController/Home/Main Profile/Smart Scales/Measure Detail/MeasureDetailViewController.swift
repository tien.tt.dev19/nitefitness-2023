//
//  
//  MeasureDetailViewController.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol MeasureDetailViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setInitDataUI()
    
    func onReloadData()
    func setDataConsult()
}

// MARK: - MeasureDetail ViewController
class MeasureDetailViewController: BaseViewController {
    var router: MeasureDetailRouterProtocol!
    var viewModel: MeasureDetailViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewMeasureDetail?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
        self.setInitUITableView()
    }
    
    private func setInitLayout() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismiss()
    }
}

// MARK: - MeasureDetail ViewProtocol
extension MeasureDetailViewController: MeasureDetailViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setInitDataUI() {
        self.lbl_Title.text = self.viewModel.index?.title
        self.view_Header?.setUpdateDataCharts(index: self.viewModel.index, bodyFat: self.viewModel.bodyFat)
        //self.view_Header?.setUpdateDataCharts(index: self.viewModel.index, bodyFat: nil)
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setDataConsult() {
        self.view_Header?.setUpdateDataConsult(consult: self.viewModel.consult)
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: - UITableViewDataSource
extension MeasureDetailViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerFooterNib(ofType: FooterSectionViewSmartScales.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewSmartScalesPost.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if let sliderBanner = self.viewModel.sliderBanner {
            let footer = tableView.dequeueHeaderView(ofType: FooterSectionViewSmartScales.self)
            footer.sliderBanner = sliderBanner
            return footer
            
        } else {
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listPostSuggest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewSmartScalesPost.self, for: indexPath)
        cell.post = self.viewModel.listPostSuggest[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MeasureDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: self.viewModel.listPostSuggest[indexPath.row].link ?? "") else {
            return
        }
        UIApplication.shared.open(url)
    }
}

// MARK: UITableView Header
extension MeasureDetailViewController: HeaderTableViewMeasureDetailDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewMeasureDetail.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setLayoutHeaderTableView() {
        
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // MARK: HeaderTableViewMeasureDetailDelegate
    func onAdviseAction() {
        self.router.showChatMessageRouter()
    }
}
