//
//  
//  MeasureDetailViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MeasureDetailViewModelProtocol {
    func onViewDidLoad()
    
    var index: IndexProtocol? { get set }
    var bodyFat: BodyFatSDK? { get set }
    
    var sliderBanner: SliderBannerModel? { get }
    var consult: ConsultModel? { get set }
    var listPostSuggest: [PostHealthModel] { get set }
}

// MARK: - MeasureDetail ViewModel
class MeasureDetailViewModel {
    weak var view: MeasureDetailViewProtocol?
    private var interactor: MeasureDetailInteractorInputProtocol

    init(interactor: MeasureDetailInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var index: IndexProtocol?
    var bodyFat: BodyFatSDK?
    
    var sliderBanner: SliderBannerModel?
    var consult: ConsultModel?
    var listPostSuggest: [PostHealthModel] = []
}

// MARK: - MeasureDetail ViewModelProtocol
extension MeasureDetailViewModel: MeasureDetailViewModelProtocol {
    func onViewDidLoad() {
        self.view?.setInitDataUI()
        self.getInitDataScales()
        self.getSliderBanner()
    }
}

// MARK: - SmartScales Handlers
extension MeasureDetailViewModel {
    func getSliderBanner() {
        self.interactor.getSliderBanner(typeSlider: .Consult)
    }
    
    func getInitDataScales() {
        guard let code = self.index?.code else {
            return
        }
        guard let status_VN = self.index?.status_VN else {
            return
        }
        self.interactor.getPostsScales(tag: status_VN, index: code)
        
        guard let gender = self.bodyFat?.gender else {
            return
        }
        guard let weight = self.bodyFat?.weight else {
            return
        }
        guard let age = self.bodyFat?.age else {
            return
        }
        self.interactor.getConsultIndex(index: code, status: status_VN, gender: gender, age: age, weight: weight)
    }
}

// MARK: - MeasureDetail InteractorOutputProtocol
extension MeasureDetailViewModel: MeasureDetailInteractorOutputProtocol {
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listPostSuggest = model
            self.view?.onReloadData()
            
        case .failure:
            break
        }
    }
    
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>) {
        switch result {
        case .success(let model):
            self.sliderBanner = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>) {
        switch result {
        case .success(let model):
            self.consult = model
            self.view?.setDataConsult()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
