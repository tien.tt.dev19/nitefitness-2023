//
//  CellTableViewMeasureHistoryBodyFat.swift
//  1SKConnect
//
//  Created by Thaad on 28/07/2022.
//

import UIKit

class CellTableViewMeasureHistoryBodyFat: UITableViewCell {

    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    
    var bodyFat: BodyFatModel? {
        didSet {
            self.lbl_Weight.text = self.bodyFat?.weight?.toString()
            
            self.lbl_Status.text = self.bodyFat?.bodyFatSDK?.bmiIndex?.status_VN ?? "Không xác định"
            self.lbl_Status.textColor = self.bodyFat?.bodyFatSDK?.bmiIndex?.color ?? UIColor.lightGray
            
            if let createdAt = self.bodyFat?.createdAt {
                if let date = createdAt.toDate(.ymdhms) {
                    self.lbl_Date.text = date.toString(.dmySlash)
                    self.lbl_Time.text = date.toString(.hm)
                }
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.view_Content.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
