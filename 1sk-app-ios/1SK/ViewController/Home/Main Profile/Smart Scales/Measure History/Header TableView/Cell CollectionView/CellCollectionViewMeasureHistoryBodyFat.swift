//
//  CellCollectionViewMeasureHistoryBodyFat.swift
//  1SKConnect
//
//  Created by Thaad on 28/07/2022.
//

import UIKit

class CellCollectionViewMeasureHistoryBodyFat: UICollectionViewCell {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    var bodyFatType: BodyFatType? {
        didSet {
            self.lbl_Title.text = self.bodyFatType?.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.view_Bg.cornerRadius = self.view_Bg.frame.height/2
    }

}
