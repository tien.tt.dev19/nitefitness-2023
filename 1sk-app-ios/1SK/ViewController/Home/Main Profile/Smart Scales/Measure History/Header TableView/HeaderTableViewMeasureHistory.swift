//
//  HeaderTableViewMeasureHistory.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//

import UIKit

protocol HeaderTableViewMeasureHistoryDelegate: AnyObject {
    func onSelectDeviceAction()
    func onDidMenuChange(typeTime: TimeType)
    func onPreAction(typeTime: TimeType)
    func onNextAction(typeTime: TimeType)
}

class HeaderTableViewMeasureHistory: UITableViewHeaderFooterView {
    weak var delegate: HeaderTableViewMeasureHistoryDelegate?
    
    
    @IBOutlet weak var lbl_ScalesName: UILabel!
    
    @IBOutlet weak var btn_Day: UIButton!
    @IBOutlet weak var btn_Week: UIButton!
    @IBOutlet weak var btn_Month: UIButton!
    @IBOutlet weak var btn_Year: UIButton!

    @IBOutlet weak var lbl_Time: UILabel!

    @IBOutlet weak var lineChart: ScalesLineChart!
    @IBOutlet weak var coll_BodyFatType: UICollectionView!
    
    private var typeTime: TimeType = .day
    private var listBodyFatType: [BodyFatType] = [.weight, .muscle, .bone, .water, .protein, .fat, .subcutaneousFat]
    
    var currentBodyFatType: BodyFatType? = .weight
    var bodyFatAnalysis: BodyFatAnalysisModel? {
        didSet {
            self.setBodyFatType()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setStateUIMenuPage(typeTime: .day)
        self.setInitUICollectionView()
        self.setInitLineChart()
    }
    
    func setUpdateScalesName(name: String?) {
        if let _name = name, _name.count > 0 {
            self.lbl_ScalesName.text = _name
        } else {
            self.lbl_ScalesName.text = "Tất cả"
        }
    }
    
    func setTimeUI(fromDate: Date?, toDate: Date?) {
        switch self.typeTime {
        case .day:
            let today = Date().toString(.dmySlash)
            let day = fromDate?.toString(.dmySlash)

            if day == today {
                self.lbl_Time.text = "Hôm nay"
            } else {
                self.lbl_Time.text = day
            }

        case .week:
            let fromTime = fromDate?.toString(.dmySlash)
            let toTime = toDate?.toString(.dmySlash)

            self.lbl_Time.text = "\(fromTime ?? "Null") - \(toTime ?? "Null")"

        case .month:
            let fromTime = fromDate?.toString(.dmySlash)
            let toTime = toDate?.toString(.dmySlash)

            self.lbl_Time.text = "\(fromTime ?? "Null") - \(toTime ?? "Null")"

        case .year:
            let fromTime = fromDate?.toString(.dmySlash)
            let toTime = toDate?.toString(.dmySlash)

            self.lbl_Time.text = "\(fromTime ?? "Null") - \(toTime ?? "Null")"
        }
    }
    
    // MARK: - Action
    @IBAction func onSelectDeviceAction(_ sender: Any) {
        self.delegate?.onSelectDeviceAction()
    }
    
    @IBAction func onDayAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .day)
    }

    @IBAction func onWeekAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .week)
    }

    @IBAction func onMonthAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .month)
    }

    @IBAction func onYearAction(_ sender: Any) {
        self.setStateUIMenuPage(typeTime: .year)
    }

    @IBAction func onPreAction(_ sender: Any) {
        self.delegate?.onPreAction(typeTime: self.typeTime)
    }

    @IBAction func onNextAction(_ sender: Any) {
        self.delegate?.onNextAction(typeTime: self.typeTime)
    }
}

// MARK: State Menu
extension HeaderTableViewMeasureHistory {
    private func setStateUIMenuPage(typeTime: TimeType) {
        self.typeTime = typeTime

        switch typeTime {
        case .day:
            self.btn_Day.backgroundColor = R.color.subTitle()
            self.btn_Day.setTitleColor(.white, for: .normal)

            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)

        case .week:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Week.backgroundColor = R.color.subTitle()
            self.btn_Week.setTitleColor(.white, for: .normal)

            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)

        case .month:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Month.backgroundColor = R.color.subTitle()
            self.btn_Month.setTitleColor(.white, for: .normal)

            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)

        case .year:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)

            self.btn_Year.backgroundColor = R.color.subTitle()
            self.btn_Year.setTitleColor(.white, for: .normal)
        }

        self.delegate?.onDidMenuChange(typeTime: typeTime)
    }
}

// MARK: - UICollectionViewDataSource
extension HeaderTableViewMeasureHistory: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_BodyFatType.registerNib(ofType: CellCollectionViewMeasureHistoryBodyFat.self)

        self.coll_BodyFatType.dataSource = self
        self.coll_BodyFatType.delegate = self

        let layoutMeasureBodyFat = UICollectionViewFlowLayout()
        layoutMeasureBodyFat.scrollDirection = .horizontal
        layoutMeasureBodyFat.minimumLineSpacing = 8
        layoutMeasureBodyFat.minimumInteritemSpacing = 8
        layoutMeasureBodyFat.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layoutMeasureBodyFat.itemSize = CGSize(width: 100, height: self.coll_BodyFatType.frame.height)
        self.coll_BodyFatType.collectionViewLayout = layoutMeasureBodyFat
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listBodyFatType.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewMeasureHistoryBodyFat.self, for: indexPath)
        let bodyFatType = self.listBodyFatType[indexPath.row]
        cell.bodyFatType = bodyFatType

        if bodyFatType.id == self.currentBodyFatType?.id {
            cell.view_Bg.backgroundColor = R.color.mainColor()
            cell.view_Bg.borderWidth = 0
            cell.lbl_Title.textColor = .white

        } else {
            cell.view_Bg.backgroundColor = .white
            cell.view_Bg.borderWidth = 1
            cell.lbl_Title.textColor = .darkGray
        }

        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension HeaderTableViewMeasureHistory: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard collectionView == self.coll_BodyFatType else {
            return
        }
        self.currentBodyFatType = self.listBodyFatType[indexPath.row]
        self.coll_BodyFatType.reloadData()

        self.setBodyFatType()
    }

    func setBodyFatType() {
        switch self.currentBodyFatType {
        case .weight:
            //print("setBodyFatType .weight:", self.bodyFatAnalysis?.weight.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.weight

        case .muscle:
            //print("setBodyFatType .muscle:", self.bodyFatAnalysis?.muscleMass.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.muscleMass

        case .bone:
            //print("setBodyFatType .bone:", self.bodyFatAnalysis?.boneMass.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.boneMass

        case .water:
            //print("setBodyFatType .water:", self.bodyFatAnalysis?.bodyWater.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.bodyWater

        case .protein:
            //print("setBodyFatType .protein:", self.bodyFatAnalysis?.protein.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.protein

        case .fat:
            //print("setBodyFatType .fat:", self.bodyFatAnalysis?.fat.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.fat

        case .subcutaneousFat:
            //print("setBodyFatType .subcutaneousFat:", self.bodyFatAnalysis?.subcutaneousFat.jsonString ?? "Null")
            self.lineChart.listDataEntries = self.bodyFatAnalysis?.subcutaneousFat

        default:
            break
        }
    }
}

// MARK: Line Chart
extension HeaderTableViewMeasureHistory: ScalesLineChartDelegate {
    func setInitLineChart() {
        self.lineChart.delegate = self

        let dataEntriesDay = [BodyFatEntry(avg: 0, unit: "", label: "0"),
                              BodyFatEntry(avg: 0, unit: "", label: "1"),
                              BodyFatEntry(avg: 0, unit: "", label: "2"),
                              BodyFatEntry(avg: 0, unit: "", label: "3"),
                              BodyFatEntry(avg: 0, unit: "", label: "4"),
                              BodyFatEntry(avg: 0, unit: "", label: "5"),
                              BodyFatEntry(avg: 0, unit: "", label: "6"),
                              BodyFatEntry(avg: 0, unit: "", label: "7"),
                              BodyFatEntry(avg: 0, unit: "", label: "8"),
                              BodyFatEntry(avg: 0, unit: "", label: "9"),
                              BodyFatEntry(avg: 0, unit: "", label: "10"),
                              BodyFatEntry(avg: 0, unit: "", label: "11"),
                              BodyFatEntry(avg: 0, unit: "", label: "12"),
                              BodyFatEntry(avg: 0, unit: "", label: "13"),
                              BodyFatEntry(avg: 0, unit: "", label: "14"),
                              BodyFatEntry(avg: 0, unit: "", label: "15"),
                              BodyFatEntry(avg: 0, unit: "", label: "16"),
                              BodyFatEntry(avg: 0, unit: "", label: "17"),
                              BodyFatEntry(avg: 0, unit: "", label: "18"),
                              BodyFatEntry(avg: 0, unit: "", label: "19"),
                              BodyFatEntry(avg: 0, unit: "", label: "20"),
                              BodyFatEntry(avg: 0, unit: "", label: "21"),
                              BodyFatEntry(avg: 0, unit: "", label: "22"),
                              BodyFatEntry(avg: 0, unit: "", label: "23"),
                              BodyFatEntry(avg: 0, unit: "", label: "24")
        ]

        self.lineChart.listDataEntries = dataEntriesDay
    }

    func setEmptyDataUI(isHidden: Bool) {
        self.coll_BodyFatType.isHidden = isHidden
    }
}
