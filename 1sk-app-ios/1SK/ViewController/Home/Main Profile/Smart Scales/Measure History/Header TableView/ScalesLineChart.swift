//
//  ScalesLineChart.swift
//  1SK
//
//  Created by Thaad on 21/10/2022.
//

import Foundation
import UIKit
import Network

protocol ScalesLineChartDelegate: AnyObject {
    func setEmptyDataUI(isHidden: Bool)
}

// MARK: ScalesLineChart
class ScalesLineChart: UIView {
    weak var delegate: ScalesLineChartDelegate?

    /// Color Line
    let colorChar = UIColor.gray

    /// gap between each point
    /// khoảng cách giữa mỗi điểm
    var lineGap: CGFloat = 60.0

    /// preseved space at top of the chart
    /// khoảng trống được chèn sẵn ở trên đầu biểu đồ
    let topSpace: CGFloat = 40.0

    /// preserved space at bottom of the chart to show labels along the Y axis
    /// không gian được bảo toàn ở cuối biểu đồ để hiển thị các nhãn dọc theo trục Y.
    let bottomSpace: CGFloat = 40.0

    /// preseeded space on the left side of the chart
    /// khoảng trống được chèn sẵn ở bên trái biểu đồ
    let leftSpace: CGFloat = 30.0

    /// The top most horizontal line in the chart will be 10% higher than the highest value in the chart
    /// Dòng nằm ngang trên cùng trong biểu đồ sẽ cao hơn 10% so với giá trị cao nhất trong biểu đồ
    let topHorizontalLine: CGFloat = 120.0 / 100.0

    /// Dot inner Radius
    /// Bán kính chấm bên trong
    var innerRadius: CGFloat = 6

    /// Dot outer Radius
    /// Bán kính chấm bên ngoài
    var outerRadius: CGFloat = 12

    /// Contains mainLayer and label for each data entry
    /// Chứa mainLayer và nhãn cho mỗi mục nhập dữ liệu
    private let scrollView: UIScrollView = UIScrollView()

    /// Contains dataLayer and gradientLayer
    /// Chứa dataLayer và gradientLayer
    private let mainLayer: CALayer = CALayer()

    /// Contains horizontal lines
    /// Chứa các đường ngang
    private let gridLayer: CALayer = CALayer()

    /// Contains the main line which represents the data
    /// Chứa dòng chính đại diện cho dữ liệu
    private let dataLayer: CALayer = CALayer()

    /// An array of CGPoint on dataLayer coordinate system that the main line will go through. These points will be calculated from dataEntries array
    /// Một mảng CGPoint trên hệ tọa độ dataLayer mà đường chính sẽ đi qua. Các điểm này sẽ được tính từ mảng dataEntries
    private var listDataPoint: [CGPoint]?
    
    private var listAreaGesture: [AreaGesture] = []
    
    private var minValueY: Double?
    private var maxValueY: Double?
    private var range: Double = 0

    private var isEmptyData = true
    private var isScrollEnabled = false

    var listDataEntries: [BodyFatEntry]? {
        didSet {
            self.setNeedsLayout()
        }
    }

    // MARK: Init Chart
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }

    convenience init() {
        self.init(frame: CGRect.zero)
        self.setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupView()
    }

    private func setupView() {
        //self.scrollView.backgroundColor = UIColor.lightGray
        //self.mainLayer.backgroundColor = UIColor.yellow.cgColor
        //self.gridLayer.backgroundColor = UIColor.purple.cgColor
        //self.dataLayer.backgroundColor = UIColor.orange.cgColor

        ////
        self.mainLayer.addSublayer(self.dataLayer)
        self.scrollView.layer.addSublayer(self.mainLayer)

        self.layer.addSublayer(self.gridLayer)
        self.addSubview(self.scrollView)
        //self.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.6941176471, blue: 0.1058823529, alpha: 1)
    }

    // MARK: Layout Subviews
    override func layoutSubviews() {
        self.setClearDrawDotSelected()
        self.setClearDrawAlert()
        self.checkEmptyData()

        self.scrollView.frame = CGRect(x: self.leftSpace, y: 0, width: self.frame.size.width - self.leftSpace, height: self.frame.size.height)
        self.scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapAction(recognizer:))))

        if let dataEntries = self.listDataEntries {
            // Set Data Min - Max
            let listData = dataEntries.filter({ $0.avg ?? 0 > 0 })
            if let min = listData.min()?.avg, let max = listData.max()?.avg {
                self.maxValueY = max
                self.range = max - min
                if self.range > 5 {
                    self.minValueY = min - 5
                } else if self.range > 1 {
                    self.minValueY = min - 1
                } else {
                    self.minValueY = min - 0.1
                }
                
                print("layoutSubviews range:", range)
                print("layoutSubviews min:", min)
                print("layoutSubviews max:", max)

            } else {
                self.minValueY = 0
                self.maxValueY = 100
            }

            // Scroll Enabled
            if self.isScrollEnabled == false {
                self.scrollView.isScrollEnabled = false
                self.lineGap = CGFloat(Int(self.scrollView.frame.size.width) / (dataEntries.count))
            }
            
            // Set Layout
            let width = CGFloat(dataEntries.count) * self.lineGap
            let height = self.frame.size.height
            
            self.scrollView.contentSize = CGSize(width: width, height: height)
            self.mainLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)

            self.dataLayer.frame = CGRect(x: 0, y: self.topSpace, width: self.mainLayer.frame.width, height: self.mainLayer.frame.height - self.topSpace - self.bottomSpace)
            self.gridLayer.frame = CGRect(x: 0, y: self.topSpace, width: self.frame.width, height: self.mainLayer.frame.height - self.topSpace - self.bottomSpace)

            self.listDataPoint = self.setConvertEntriesToPoints(entries: dataEntries)
            //print("layoutSubviews points:", self.listDataPoint!)
            
            self.setCleanLayer()

            self.setDrawVertical()
            self.setDrawHorizontal()

            self.setDrawChart(points: self.listDataPoint, color: R.color.mainColor() ?? UIColor.blue)
            self.setDrawDots(points: self.listDataPoint, color: R.color.mainColor() ?? UIColor.blue)

            self.setDrawEmptyLayer()
        }
    }

    // MARK: Check Empty Data
    private func checkEmptyData() {
        self.isEmptyData = true
        self.listDataEntries?.enumerated().forEach { (index, entry) in
            if entry.avg ?? 0 > 0 {
                self.isEmptyData = false
            }
        }
        
        self.delegate?.setEmptyDataUI(isHidden: self.isEmptyData)
    }

    // MARK: Draw Empty Layer
    private func setDrawEmptyLayer() {
        if self.isEmptyData == true {
            let size = CGSize(width: self.mainLayer.frame.width, height: 20)
            let point = CGPoint(x: 0, y: (self.mainLayer.frame.height/2)-32)

            let textLayer = CATextLayer()
            textLayer.name = "EmptyLayer"
            textLayer.frame = CGRect(origin: point, size: size)
            textLayer.foregroundColor = self.colorChar.cgColor
            textLayer.backgroundColor = UIColor.clear.cgColor
            textLayer.alignmentMode = CATextLayerAlignmentMode.center
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
            textLayer.fontSize = 16
            textLayer.string = "Chưa có dữ liệu"
            self.mainLayer.addSublayer(textLayer)
        }
    }

    /**
     Convert an array of PointEntry to an array of CGPoint on dataLayer coordinate system
     Chuyển đổi một mảng PointEntry thành một mảng CGPoint trên hệ tọa độ dataLayer
     */
    // MARK: Convert Entries To Points
    private func setConvertEntriesToPoints(entries: [BodyFatEntry]) -> [CGPoint] {
        guard let max = self.maxValueY else {
            return []
        }
        guard let min = self.minValueY else {
            return []
        }

        var result: [CGPoint] = []
        let minMaxRange: CGFloat = CGFloat(max - min) * self.topHorizontalLine

        entries.enumerated().forEach { (index, entry) in
            let avg = entry.avg ?? 0
            if avg > 0 {
                let value = (CGFloat(avg) - CGFloat(min)) / minMaxRange
                let pointX = CGFloat(index)*self.lineGap + (40 - self.leftSpace)
                let pointY = self.dataLayer.frame.height * (1 - value)

                let point = CGPoint(x: pointX, y: pointY)
                result.append(point)

            } else {
                let point = CGPoint(x: -1, y: -1)
                result.append(point)
            }
        }

        return result
    }

    // MARK:  Clean Layer
    private func setCleanLayer() {
        self.mainLayer.sublayers?.forEach({
            if $0 is CATextLayer {
                $0.removeFromSuperlayer()
            }
        })
        self.dataLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
        self.gridLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
    }

    /**
     Create titles at the bottom for all entries showed in the chart
     Tạo tiêu đề ở dưới cùng cho tất cả các mục nhập được hiển thị trong biểu đồ
     */
    // MARK: Draw Vertical
    private func setDrawVertical() {
        guard let dataEntries = self.listDataEntries, dataEntries.count > 0 else {
            return
        }
        self.listAreaGesture.removeAll()

        if self.isEmptyData == true {
            self.setDrawVerticalLineRoot()
        }

        dataEntries.enumerated().forEach { (index, entry) in
            let point = CGPoint(x: self.lineGap*CGFloat(index) - self.lineGap/2 + (40 - self.leftSpace),
                                y: self.mainLayer.frame.size.height - self.bottomSpace/2 - 8)

            // set Gesture Area
            let width = (self.dataLayer.frame.width / CGFloat(self.listDataEntries?.count ?? 0)) - 0.5
            let size = CGSize(width: width, height: self.mainLayer.frame.size.height)
            self.listAreaGesture.append(AreaGesture(point: point, size: size))

            // Set Draw Vertical Line
            if dataEntries.count > 12 {
                if index.isEven {
                    if self.scrollView.isScrollEnabled == true {
                        self.setDrawVerticalLable(point: point, entry: entry, width: width)
                        self.setDrawVerticalLine(point: point, entry: entry, index: index, isEmptyData: self.isEmptyData)

                    } else {
                        self.setDrawVerticalLable(point: point, entry: entry, width: width + (self.lineGap/4))
                        self.setDrawVerticalLine(point: point, entry: entry, index: index, isEmptyData: self.isEmptyData)
                    }

                } else {
                    // Not Draw Line
                }

            } else {
                self.setDrawVerticalLable(point: point, entry: entry, width: width)
                self.setDrawVerticalLine(point: point, entry: entry, index: index, isEmptyData: self.isEmptyData)
            }
        }
    }

    // MARK: Draw Vertical Line
    private func setDrawVerticalLine(point: CGPoint, entry: BodyFatEntry, index: Int, isEmptyData: Bool) {
        guard isEmptyData == false else {
            return
        }
        let path = UIBezierPath()
        path.move(to: CGPoint(x: point.x  + self.lineGap/2, y: -self.topSpace + 20))
        path.addLine(to: CGPoint(x: point.x + self.lineGap/2, y: self.dataLayer.frame.size.height))
        let lineLayer = CAShapeLayer()
        lineLayer.path = path.cgPath
        lineLayer.lineWidth = 0.3
        lineLayer.fillColor = UIColor.clear.cgColor
        if index == 0 {
            lineLayer.strokeColor = self.colorChar.cgColor
        } else {
            lineLayer.strokeColor = self.colorChar.withAlphaComponent(0.7).cgColor
        }
        self.dataLayer.addSublayer(lineLayer)
    }

    // MARK: Draw Vertical Lable
    private func setDrawVerticalLable(point: CGPoint, entry: BodyFatEntry, width: CGFloat) {
        let size = CGSize(width: width, height: 16)
        let pointText = CGPoint(x: point.x, y: point.y)

        let textLayer = CATextLayer()
        textLayer.frame = CGRect(origin: pointText, size: size)
        textLayer.foregroundColor = self.colorChar.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = 11
        textLayer.string = entry.label
        self.mainLayer.addSublayer(textLayer)
    }

    // MARK: Draw Vertical Line Root
    private func setDrawVerticalLineRoot() {
        let height = self.mainLayer.frame.size.height - self.bottomSpace - self.bottomSpace
        let path = UIBezierPath()
        path.move(to: CGPoint(x: self.leftSpace, y: -self.bottomSpace + 20))
        path.addLine(to: CGPoint(x: self.leftSpace, y: height))
        let lineLayer = CAShapeLayer()
        lineLayer.path = path.cgPath
        lineLayer.fillColor = UIColor.clear.cgColor
        lineLayer.strokeColor = self.colorChar.cgColor
        lineLayer.lineWidth = 0.5
        self.gridLayer.addSublayer(lineLayer)
    }

    /**
     Create horizontal lines (grid lines) and show the value of each line
     Tạo các đường ngang (đường lưới) và hiển thị giá trị của mỗi dòng
     */
    // MARK: Draw Horizontal
    private func setDrawHorizontal() {
        let gridValues: [CGFloat] = [0, 0.25, 0.5, 0.75, 1]
        //let gridValues: [CGFloat] = [0, 0.2, 0.4, 0.6, 0.8, 1]
        //let gridValues: [CGFloat] = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]

        gridValues.forEach({ value in
            let height = value * self.gridLayer.frame.size.height

            let path = UIBezierPath()
            path.move(to: CGPoint(x: 0, y: height))
            path.addLine(to: CGPoint(x: self.gridLayer.frame.size.width, y: height))

            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.fillColor = UIColor.clear.cgColor
            lineLayer.strokeColor = self.colorChar.cgColor
            lineLayer.lineWidth = 0.5
            if value < 1.0 {
                lineLayer.lineDashPattern = [4, 4]
                lineLayer.strokeColor = self.colorChar.withAlphaComponent(0.7).cgColor
                
                if self.isEmptyData {
                    //lineLayer.strokeColor = UIColor.clear.cgColor
                }
            }
            self.gridLayer.addSublayer(lineLayer)

            // Draw Lable Horizontal
            var minMaxGap: CGFloat = 0
            var lineValue: Double = 0
            if let max = self.maxValueY, let min = self.minValueY {
                minMaxGap = CGFloat(max - min) * self.topHorizontalLine
                if self.range > 1 {
                    lineValue = ((1-value) * minMaxGap + min).rounded(toPlaces: 1)
                } else {
                    lineValue = ((1-value) * minMaxGap + min).rounded(toPlaces: 2)
                }
            }
            let textLayer = CATextLayer()
            textLayer.frame = CGRect(x: 0, y: height, width: 50, height: 16)
            textLayer.foregroundColor = self.colorChar.cgColor
            textLayer.backgroundColor = UIColor.clear.cgColor
            textLayer.contentsScale = UIScreen.main.scale
            textLayer.alignmentMode = CATextLayerAlignmentMode.left
            textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
            textLayer.fontSize = 12
            textLayer.string = "\(lineValue)"
            self.gridLayer.addSublayer(textLayer)
        })
    }

    /**
     Draw a zigzag line connecting all points in dataPoints
     Vẽ một đường zigzag nối tất cả các điểm trong dataPoints
     */
    // MARK: Draw Chart
    private func setDrawChart(points: [CGPoint]?, color: UIColor) {
        if let listPoint = points, listPoint.count > 0 {
            let count = listPoint.filter({ $0.x > -1 && $0.y > -1 }).count
            if count > 1, let path = self.setCreatePath(for: listPoint) {
                let lineLayer = CAShapeLayer()
                lineLayer.path = path.cgPath
                lineLayer.strokeColor = color.cgColor
                lineLayer.fillColor = UIColor.clear.cgColor
                lineLayer.lineWidth = 1.5
                self.dataLayer.addSublayer(lineLayer)
            }
        }
    }

    /**
     Create a zigzag bezier path that connects all points in dataPoints
     Tạo một đường viền zigzag kết nối tất cả các điểm trong dataPoints
     */
    // MARK: Create Path
    private func setCreatePath(for points: [CGPoint]) -> UIBezierPath? {
        guard points.count > 0 else {
            return nil
        }
        //print("setCreatePath points:", points)
        guard let pointBegin = points.first(where: { $0.x > -1 && $0.y > -1 }) else {
            return nil
        }
        let path = UIBezierPath()
        path.move(to: pointBegin)
        points.enumerated().forEach { (index, point) in
            if point.x > -1 && point.y > -1 {
                path.addLine(to: point)
            }
        }
        return path
    }

    /**
     Create Dots on line points
     Tạo dấu chấm trên các điểm dòng
     */
    // MARK: Draw Dots
    private func setDrawDots(points: [CGPoint]?, color: UIColor) {
        points?.enumerated().forEach({ (index, point) in
            let avg = self.listDataEntries?[index].avg ?? 0
            if avg > 0 {
                let xValue = point.x - (self.outerRadius/2)
                let yValue = point.y - (self.outerRadius/2)

                // drawDots
                let dotLayer = DotCALayer()
                dotLayer.dotInnerColor = color
                dotLayer.innerRadius = self.innerRadius
                dotLayer.backgroundColor = UIColor.clear.cgColor
                dotLayer.cornerRadius = self.outerRadius / 2
                dotLayer.frame = CGRect(x: xValue, y: yValue, width: self.outerRadius, height: self.outerRadius)
                self.dataLayer.addSublayer(dotLayer)
            }
        })
    }

    // MARK: Draw Dot Selected
    private func setDrawDotSelected(point: CGPoint?) {
        guard let _point = point, _point.x >= 0, _point.y >= 0 else {
            return
        }
        let width: CGFloat = self.outerRadius
        let x = _point.x - (width/2)
        let y = _point.y - (width/2)

        let layer = CALayer()
        layer.name = "layerDotSelected"
        layer.frame = CGRect(x: x, y: y, width: width, height: width + 1)
        layer.contents = UIImage(named: "ic_dot_scales_select")?.cgImage
        self.dataLayer.addSublayer(layer)
    }
}

extension ScalesLineChart {
    // MARK: Tap Action
    @objc func onTapAction(recognizer: UIGestureRecognizer) {
        let pointTap = recognizer.location(in: self.scrollView)
        
        self.listAreaGesture.enumerated().forEach { (index, area) in
            if pointTap.x >= area.point.x && pointTap.x <= area.point.x + area.size.width {
                self.setDrawDotSelected(index: index)
                self.setDrawAlert(area: area, index: index, entry: self.listDataEntries?[index])
            }
        }
    }

    private func setClearDrawDotSelected() {
        let sublayers = self.dataLayer.sublayers
        sublayers?.forEach({ layer in
            if layer.name == "layerDotSelected" {
                layer.removeFromSuperlayer()
            }
        })
    }

    // MARK: Draw Dot Selected
    private func setDrawDotSelected(index: Int) {
        self.setClearDrawDotSelected()
        self.setDrawDotSelected(point: self.listDataPoint?[index])
    }

    private func setClearDrawAlert() { // Clear alertLayer old
        let sublayers = self.mainLayer.sublayers
        sublayers?.forEach({ layer in
            switch layer.name {
            case "layerMaskArrow":
                layer.removeFromSuperlayer()
                
            case "layerArrow":
                layer.removeFromSuperlayer()
                
            case "layerAlert":
                layer.removeFromSuperlayer()
                
            default:
                break
            }
        })
    }
    
    // MARK: Draw Alert
    private func setDrawAlert(area: AreaGesture, index: Int, entry: BodyFatEntry?) {
        self.setClearDrawAlert()
        guard entry?.avg ?? 0 > 0 else {
            return
        }

        // Draw alertLayer new
        let layerAlert = CALayer()
        layerAlert.name = "layerAlert"
        layerAlert.backgroundColor = UIColor.white.cgColor
        layerAlert.cornerRadius = 6
        layerAlert.masksToBounds = false
        layerAlert.shadowOffset = CGSize(width: 0, height: 2)
        layerAlert.shadowColor = UIColor.black.cgColor
        layerAlert.shadowRadius = 4
        layerAlert.shadowOpacity = 0.3

        let point = self.listDataPoint?[index] ?? CGPoint(x: 0, y: 0)
        
        let padding: CGFloat = 5
        let width: CGFloat = 90
        let height: CGFloat = 30 + (padding*2)
        let originX: CGFloat = area.point.x + (self.lineGap/2) - (width/2)
        let originArrowY: CGFloat = (point.y + self.topSpace) - height + 20

        if originX < 0 {
            layerAlert.frame = CGRect(x: 4,
                                      y: originArrowY - height + 1 ,
                                      width: width,
                                      height: height)
            
        } else if originX > self.mainLayer.frame.width - width - 5 {
            layerAlert.frame = CGRect(x: self.mainLayer.frame.width - width - 5,
                                      y: originArrowY - height + 1,
                                      width: width,
                                      height: height)
            
        } else {
            layerAlert.frame = CGRect(x: originX,
                                      y: originArrowY - height + 1,
                                      width: width,
                                      height: height)
        }
        
        // Draw text detail title
        let textLayerTitle = CATextLayer()
        textLayerTitle.frame = CGRect(x: padding, y: padding,
                                      width: layerAlert.frame.size.width - (padding*2),
                                      height: 16)
        textLayerTitle.foregroundColor = UIColor.gray.cgColor
        textLayerTitle.backgroundColor = UIColor.white.cgColor
        textLayerTitle.contentsScale = UIScreen.main.scale
        textLayerTitle.contentsGravity = .center
        textLayerTitle.alignmentMode = CATextLayerAlignmentMode.center
        textLayerTitle.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayerTitle.fontSize = 12
        textLayerTitle.string = "\(entry?.avg ?? 0) \(entry?.unit ?? "")"
        layerAlert.addSublayer(textLayerTitle)
        
        // Draw text detail content
        let textLayerContent = CATextLayer()
        textLayerContent.frame = CGRect(x: padding, y: padding + textLayerTitle.frame.height,
                                        width: layerAlert.frame.size.width - (padding*2),
                                        height: (layerAlert.frame.size.height/2) - (padding*2))
        textLayerContent.foregroundColor = UIColor.gray.cgColor
        textLayerContent.backgroundColor = UIColor.white.cgColor
        textLayerContent.contentsScale = UIScreen.main.scale
        textLayerContent.contentsGravity = .center
        textLayerContent.alignmentMode = CATextLayerAlignmentMode.center
        textLayerContent.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayerContent.fontSize = 10
        textLayerContent.string = "\(entry?.timeString ?? "")"
        layerAlert.addSublayer(textLayerContent)

        // Add Alert Layer
        self.mainLayer.addSublayer(layerAlert)

        // Draw arrow
        let path = UIBezierPath()
        path.move(to: CGPoint(x: point.x, y: point.y + self.topSpace))
        
        path.addLine(to: CGPoint(x: layerAlert.frame.origin.x + (layerAlert.frame.width/2) - 10,
                                 y: originArrowY))
        path.addLine(to: CGPoint(x: layerAlert.frame.origin.x + (layerAlert.frame.width/2) + 10,
                                 y: originArrowY))
        
        
        let layerArrow = CAShapeLayer()
        layerArrow.name = "layerArrow"
        layerArrow.path = path.cgPath
        layerArrow.fillColor = UIColor.white.cgColor
        layerArrow.shadowOffset = CGSize(width: 0, height: 2)
        layerArrow.shadowColor = UIColor.black.cgColor
        layerArrow.shadowRadius = 4
        layerArrow.shadowOpacity = 0.3
        self.mainLayer.addSublayer(layerArrow)

        // Draw Mask Arrow
        let heightMaskArrow: CGFloat = 6
        let layerMaskArrow = CALayer()
        layerMaskArrow.name = "layerMaskArrow"
        layerMaskArrow.backgroundColor = UIColor.white.cgColor
        layerMaskArrow.cornerRadius = 6
        layerMaskArrow.masksToBounds = false
        layerMaskArrow.frame = CGRect(x: layerAlert.frame.origin.x,
                                      y: originArrowY - heightMaskArrow + 1,
                                      width: layerAlert.frame.width,
                                      height: heightMaskArrow)
        self.mainLayer.addSublayer(layerMaskArrow)
    }
}
