//
//  
//  MeasureHistoryInteractorInput.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasureHistoryInteractorInputProtocol {
    func getDataScalesAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?)
    func getDataScalesMeasual(deviceName: String?, userId: Int, fromDate: String, toDate: String, page: Int, perPage: Int, accessToken: String?)
    func getListScalesName(accessToken: String?)
}

// MARK: - Interactor Output Protocol
protocol MeasureHistoryInteractorOutputProtocol: AnyObject {
    func onGetDataScalesAnalysisFinished(with result: Result<BodyFatAnalysisModel, APIError>)
    func onGetDataScalesMeasualFinished(with result: Result<[BodyFatModel], APIError>, page: Int, total: Int)
    func onGetListScalesNameFinished(with result: Result<[String], APIError>)
}

// MARK: - MeasureHistory InteractorInput
class MeasureHistoryInteractorInput {
    weak var output: MeasureHistoryInteractorOutputProtocol?
    var connectService: ConnectServiceProtocol?
}

// MARK: - MeasureHistory InteractorInputProtocol
extension MeasureHistoryInteractorInput: MeasureHistoryInteractorInputProtocol {
    func getDataScalesAnalysis(deviceName: String?, type: TimeType, time: Int, accessToken: String?) {
        self.connectService?.getDataScalesAnalysis(deviceName: deviceName, type: type, time: time, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetDataScalesAnalysisFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getDataScalesMeasual(deviceName: String?, userId: Int, fromDate: String, toDate: String, page: Int, perPage: Int, accessToken: String?) {
        self.connectService?.getDataScalesHistory(deviceName: deviceName, userId: userId, fromDate: fromDate, toDate: toDate, page: page, perPage: perPage, accessToken: accessToken, completion: { [weak self] result in
            let total = result.getTotal() ?? 0
            self?.output?.onGetDataScalesMeasualFinished(with: result.unwrapSuccessModel(), page: page, total: total)
        })
    }
    
    func getListScalesName(accessToken: String?) {
        self.connectService?.getListScalesName(accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetListScalesNameFinished(with: result.unwrapSuccessModel())
        })
    }
}
