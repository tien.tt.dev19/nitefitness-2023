//
//  
//  MeasureHistoryRouter.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasureHistoryRouterProtocol {
    func onDismissAction()
    func presentUpdateSubProfileRouter(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?)
    func presentResultMeasureHistoryRouter(bodyFat: BodyFatModel?)
}

// MARK: - MeasureHistory Router
class MeasureHistoryRouter {
    weak var viewController: MeasureHistoryViewController?
    
    static func setupModule(device: DeviceRealm?) -> MeasureHistoryViewController {
        let viewController = MeasureHistoryViewController()
        let router = MeasureHistoryRouter()
        let interactorInput = MeasureHistoryInteractorInput()
        let viewModel = MeasureHistoryViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.connectService = ConnectService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MeasureHistory RouterProtocol
extension MeasureHistoryRouter: MeasureHistoryRouterProtocol {
    func onDismissAction() {
        self.viewController?.dismiss(animated: true)
    }
    
    func presentUpdateSubProfileRouter(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?) {
        let controller = UpdateSubProfileRouter.setupModule(authModel: authModel, delegate: delegate)
        controller.isHiddenDeleteButton = true
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func presentResultMeasureHistoryRouter(bodyFat: BodyFatModel?) {
        let controller = ResultMeasureHistoryRouter.setupModule(bodyFat: bodyFat)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
}
