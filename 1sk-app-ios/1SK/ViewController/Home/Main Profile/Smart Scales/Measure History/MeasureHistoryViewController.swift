//
//  
//  MeasureHistoryViewController.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol MeasureHistoryViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
    func onReloadLineChart()
    func setTimeUI(fromDate: Date?, toDate: Date?)
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    
    func onShowAlertScaleListView()
}

// MARK: - MeasureHistory ViewController
class MeasureHistoryViewController: BaseViewController {
    var router: MeasureHistoryRouterProtocol!
    var viewModel: MeasureHistoryViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewMeasureHistory?
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.view_Nav.addShadow(width: 0, height: 6, color: .black, radius: 3, opacity: 0.05)
        
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismissAction()
    }
}

// MARK: - MeasureHistory ViewProtocol
extension MeasureHistoryViewController: MeasureHistoryViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.updateFrameHeaderView()
        self.tbv_TableView.reloadData()
    }
    
    func onReloadLineChart() {
        self.view_Header?.bodyFatAnalysis = self.viewModel.bodyFatAnalysis
    }
    
    func setTimeUI(fromDate: Date?, toDate: Date?) {
        self.view_Header?.setTimeUI(fromDate: fromDate, toDate: toDate)
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onShowAlertScaleListView() {
        guard self.viewModel.listDeviceName.count > 0 else {
            SKToast.shared.showToast(content: "Không có thiết bị để chọn")
            return
        }
        self.presentAlertDeviceNameListView()
    }
}

// MARK: Refresh Control
extension MeasureHistoryViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }

    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension MeasureHistoryViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewMeasureHistoryBodyFat.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listBodyFat.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewMeasureHistoryBodyFat.self, for: indexPath)
        cell.bodyFat = self.viewModel.listBodyFat[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MeasureHistoryViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard self.checkValidateFullProfile() else {
            return
        }
        let bodyFat = self.viewModel.listBodyFat[indexPath.row]
        self.router.presentResultMeasureHistoryRouter(bodyFat: bodyFat)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: HeaderTableBloodPressureViewDelegate
extension MeasureHistoryViewController: HeaderTableViewMeasureHistoryDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewMeasureHistory.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // HeaderTableViewMeasureHistoryDelegate
    func onSelectDeviceAction() {
        self.viewModel.onSelectDeviceAction()
    }
    
    func onDidMenuChange(typeTime: TimeType) {
        self.viewModel.typeTime = typeTime
    }
    
    func onPreAction(typeTime: TimeType) {
        self.viewModel.onPreAction(typeTime: typeTime)
    }
    
    func onNextAction(typeTime: TimeType) {
        self.viewModel.onNextAction(typeTime: typeTime)
    }
}

// MARK: Validate Full Profile
extension MeasureHistoryViewController {
    private func checkValidateFullProfile() -> Bool {
        guard self.viewModel.device?.profile?.fullName?.count ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.birthday?.count ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.gender != nil  else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.weight ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.height ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }

        return true
    }
}

// MARK: AlertPopupConfirmTitleViewDelegate
extension MeasureHistoryViewController: AlertPopupConfirmTitleViewDelegate {
    func presentAlertPopupConfirmTitleView() {
        let controller = AlertPopupConfirmTitleViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        controller.lblTitle = "Cập nhật thông tin"
        controller.lblContent = "Bạn cần cập nhật thêm thông tin chiều cao và cân nặng"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xác nhận"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirmCancelAction(object: Any?) {
        //
    }
    
    func onAlertPopupConfirmAgreeAction(object: Any?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let profile = self.viewModel.device?.profile else {
                //SKToast.shared.showToast(content: "profile == nil")
                return
            }
            
            let authModel = AuthModel()
            authModel.accessToken = profile.accessToken
            authModel.customer?.fullName = profile.fullName
            authModel.customer?.birthday = profile.birthday
            authModel.customer?.avatar = profile.avatar
            authModel.customer?.gender = profile.gender
            authModel.customer?.height = profile.height
            authModel.customer?.weight = profile.weight
            authModel.customer?.blood = profile.blood
            
            guard authModel.accessToken != nil else {
                //SKToast.shared.showToast(content: "authModel.accessToken == nil")
                return
            }
            
            self.router.presentUpdateSubProfileRouter(authModel: authModel, delegate: self)
        }
    }
}

// MARK: UpdateSubProfilePresenterDelegate
extension MeasureHistoryViewController: UpdateSubProfileDelegate {
    func onUpdateProfileSuccess() {
        SKToast.shared.showToast(content: "Cập nhật hồ sơ thành công")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewModel.device?.profile = gRealm.objects().first(where: {$0.type == .SCALES})
        }
    }
    
    func onDeleteProfileSuccess() { }
}

// MARK: AlertDeviceNameListViewDelegate
extension MeasureHistoryViewController: AlertDeviceNameListViewDelegate {
    private func presentAlertDeviceNameListView() {
        let controller = AlertDeviceNameListViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.titleAlert = "Loại cân"
        controller.currentDeviceName = self.viewModel.currentDeviceName
        controller.listDeviceName = self.viewModel.listDeviceName
        self.present(controller, animated: false)
    }
    
    func onSelectedDevice(with deviceName: DeviceName) {
        self.view_Header?.setUpdateScalesName(name: deviceName.display)
        self.viewModel.onSelectedDevice(with: deviceName)
    }
}
