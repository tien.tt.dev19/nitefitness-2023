//
//  
//  MeasureHistoryViewModel.swift
//  1SK
//
//  Created by Thaad on 23/11/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MeasureHistoryViewModelProtocol {
    func onViewDidLoad()
    
    func onSelectDeviceAction()
    func onSelectedDevice(with deviceName: DeviceName?)
    
    func onRefreshAction()
    func onLoadMoreAction()
    
    func onPreAction(typeTime: TimeType)
    func onNextAction(typeTime: TimeType)
    
    var currentDeviceName: DeviceName? { get set }
    var listDeviceName: [DeviceName] { get set }
    
    var device: DeviceRealm? { get set }
    var typeTime: TimeType { get set }
    var listBodyFat: [BodyFatModel] { get set }
    var bodyFatAnalysis: BodyFatAnalysisModel? { get set }
}

// MARK: - MeasureHistory ViewModel
class MeasureHistoryViewModel {
    weak var view: MeasureHistoryViewProtocol?
    private var interactor: MeasureHistoryInteractorInputProtocol

    init(interactor: MeasureHistoryInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var device: DeviceRealm?
    
    var typeTime: TimeType = .day {
        didSet {
            self.getData(page: 1)
        }
    }
    
    var currentDeviceName: DeviceName? = DeviceName(id: 0, name: "", display: "Tất cả")
    var listDeviceName: [DeviceName] = []
    
    var listBodyFat: [BodyFatModel] = []
    var bodyFatAnalysis: BodyFatAnalysisModel?
    
    // Time Filter
    var timeFromDay: Date? = Date().startOfDay
    var timeToDay: Date? = Date().endOfDay
    
    var timeFromWeek: Date? = Date().startOfWeek
    var timeToWeek: Date? = Date().endOfWeek
    
    var timeFromMonth: Date? = Date().startOfMonth
    var timeToMonth: Date? = Date().endOfMonth
    
    var timeFromYear: Date? = Date().startOfYear
    var timeToYear: Date? = Date().endOfYear
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 20
    private var isOutOfData = false
}

// MARK: - MeasureHistory ViewModelProtocol
extension MeasureHistoryViewModel: MeasureHistoryViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.getData(page: 1, showHud: false)
    }
    
    func onSelectDeviceAction() {
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.view?.showHud()
        self.interactor.getListScalesName(accessToken: accessToken)
    }
    
    func onSelectedDevice(with deviceName: DeviceName?) {
        self.currentDeviceName = deviceName
        self.getData(page: 1)
    }
    
    func onRefreshAction() {
        self.getData(page: 1)
    }

    func onLoadMoreAction() {
        if self.listBodyFat.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)

        } else {
            self.view?.onLoadingSuccess()
        }
    }

    func onPreAction(typeTime: TimeType) {
        switch typeTime {
        case .day:
            guard let timeFrom = self.timeFromDay else { return }
            self.timeFromDay = timeFrom.dayBefore.startOfDay
            self.timeToDay = self.timeFromDay?.endOfDay

            self.getData(page: 1)

        case .week:
            guard let timeFrom = self.timeFromWeek else { return }
            self.timeFromWeek = timeFrom.weekBefore.startOfDay
            self.timeToWeek = self.timeFromWeek?.endOfWeek.endOfDay

            self.getData(page: 1)

        case .month:
            guard let timeFrom = self.timeFromMonth else { return }
            self.timeFromMonth = timeFrom.monthBefore.startOfMonth.startOfDay
            self.timeToMonth = self.timeFromMonth?.endOfMonth.endOfDay

            self.getData(page: 1)

        case .year:
            guard let timeFrom = self.timeFromYear else { return }
            self.timeFromYear = timeFrom.yearBefore.startOfYear.startOfDay
            self.timeToYear = self.timeFromYear?.endOfYear.endOfDay

            self.getData(page: 1)
        }
    }

    func onNextAction(typeTime: TimeType) {
        switch typeTime {
        case .day:
            guard let timeFrom = self.timeFromDay else { return }
            let timeToday = Date().toString(.dmySlash)
            let timeCurrent = timeFrom.toString(.dmySlash)
            guard timeCurrent != timeToday else { return }

            self.timeFromDay = timeFrom.dayAfter.startOfDay
            self.timeToDay = self.timeFromDay?.endOfDay

            self.getData(page: 1)

        case .week:
            guard let timeFrom = self.timeFromWeek, let timeTo = self.timeToWeek else { return }
            let timeToday = Date().toTimestamp()
            let timeCurrent = timeTo.toTimestamp()
            guard timeCurrent < timeToday else { return }

            self.timeFromWeek = timeFrom.weekAfter.startOfDay
            self.timeToWeek = self.timeFromWeek?.endOfWeek.endOfDay

            self.getData(page: 1)

        case .month:
            guard let timeFrom = self.timeFromMonth, let timeTo = self.timeToMonth else { return }
            let timeToday = Date().toTimestamp()
            let timeCurrent = timeTo.toTimestamp()
            guard timeCurrent < timeToday else { return }

            self.timeFromMonth = timeFrom.monthAfter.startOfMonth.startOfDay
            self.timeToMonth = self.timeFromMonth?.endOfMonth.endOfDay

            self.getData(page: 1)

        case .year:
            guard let timeFrom = self.timeFromYear, let timeTo = self.timeToYear else { return }
            let timeToday = Date().toTimestamp()
            let timeCurrent = timeTo.toTimestamp()
            guard timeCurrent < timeToday else { return }

            self.timeFromYear = timeFrom.yearAfter.startOfYear.startOfDay
            self.timeToYear = self.timeFromYear?.endOfYear.endOfDay

            self.getData(page: 1)
        }
    }
}

// MARK: - MeasureHistory Handler
extension MeasureHistoryViewModel {
    func getData(page: Int, showHud: Bool = true) {
        if page == 1 {
            self.isOutOfData = false
        }

        guard let userId = self.device?.profile?.id else {
            return
        }
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }

        switch self.typeTime {
        case .day:
            guard let timeFrom = self.timeFromDay?.toTimestamp(), let timeTo = self.timeToDay?.toTimestamp() else {
                return
            }
            print("getDataBloodPressure .day timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .day timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")

            if page == 1 {
                if showHud {
                    self.view?.showHud()
                }
                self.view?.setTimeUI(fromDate: self.timeFromDay, toDate: self.timeToDay)
                self.interactor.getDataScalesAnalysis(deviceName: self.currentDeviceName?.name, type: .day, time: timeTo, accessToken: accessToken)
            }

            guard let startTime = self.timeFromDay?.toString(.ymd), let endTime = self.timeToDay?.toString(.ymd) else {
                return
            }
            self.interactor.getDataScalesMeasual(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: startTime, toDate: endTime, page: page, perPage: self.perPage, accessToken: accessToken)

        case .week:
            guard let timeFrom = self.timeFromWeek?.toTimestamp(), let timeTo = self.timeToWeek?.toTimestamp() else {
                return
            }
            print("getDataBloodPressure .week timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .week timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")

            if page == 1 {
                if showHud {
                    self.view?.showHud()
                }
                self.view?.setTimeUI(fromDate: self.timeFromWeek, toDate: self.timeToWeek)
                self.interactor.getDataScalesAnalysis(deviceName: self.currentDeviceName?.name, type: .week, time: timeTo, accessToken: accessToken)
            }

            guard let startTime = self.timeFromWeek?.toString(.ymd), let endTime = self.timeToWeek?.toString(.ymd) else {
                return
            }
            self.interactor.getDataScalesMeasual(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: startTime, toDate: endTime, page: page, perPage: self.perPage, accessToken: accessToken)

        case .month:
            guard let timeFrom = self.timeFromMonth?.toTimestamp(), let timeTo = self.timeToMonth?.toTimestamp() else {
                return
            }
            print("getDataBloodPressure .month timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .month timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")

            if page == 1 {
                if showHud {
                    self.view?.showHud()
                }
                self.view?.setTimeUI(fromDate: self.timeFromMonth, toDate: self.timeToMonth)
                self.interactor.getDataScalesAnalysis(deviceName: self.currentDeviceName?.name, type: .month, time: timeTo, accessToken: accessToken)
            }

            guard let startTime = self.timeFromMonth?.toString(.ymd), let endTime = self.timeToMonth?.toString(.ymd) else {
                return
            }
            self.interactor.getDataScalesMeasual(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: startTime, toDate: endTime, page: page, perPage: self.perPage, accessToken: accessToken)

        case .year:
            guard let timeFrom = self.timeFromYear?.toTimestamp(), let timeTo = self.timeToYear?.toTimestamp() else {
                return
            }
            print("getDataBloodPressure .year timeFrom:", TimeInterval(timeFrom).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure .year timeTo:", TimeInterval(timeTo).toDate().toString(.dmyhmsSlash))
            print("getDataBloodPressure -------------------")

            if page == 1 {
                if showHud {
                    self.view?.showHud()
                }
                self.view?.setTimeUI(fromDate: self.timeFromYear, toDate: self.timeToYear)
                self.interactor.getDataScalesAnalysis(deviceName: self.currentDeviceName?.name, type: .year, time: timeTo, accessToken: accessToken)
            }

            guard let startTime = self.timeFromYear?.toString(.ymd), let endTime = self.timeToYear?.toString(.ymd) else {
                return
            }
            self.interactor.getDataScalesMeasual(deviceName: self.currentDeviceName?.name, userId: userId, fromDate: startTime, toDate: endTime, page: page, perPage: self.perPage, accessToken: accessToken)
        }
    }
}

// MARK: - MeasureHistory InteractorOutputProtocol
extension MeasureHistoryViewModel: MeasureHistoryInteractorOutputProtocol {
    func onGetDataScalesAnalysisFinished(with result: Result<BodyFatAnalysisModel, APIError>) {
        switch result {
        case .success(let model):
            self.bodyFatAnalysis = model
            self.view?.onReloadLineChart()

        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onGetDataScalesMeasualFinished(with result: Result<[BodyFatModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            listModel.forEach { model in
                model.setBodyFatSDK(profile: self.device?.profile)
            }
            
            if page <= 1 {
                self.listBodyFat = listModel
                self.view?.onReloadData()

            } else {
                for object in listModel {
                    self.listBodyFat.append(object)
                    self.view?.onInsertRow(at: self.listBodyFat.count - 1)
                }
            }

            self.total = total
            self.page = page

            // Set Out Of Data
            if self.listBodyFat.count >= self.total {
                self.isOutOfData = true
            }

            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }

        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
    }
    
    func onGetListScalesNameFinished(with result: Result<[String], APIError>) {
        switch result {
        case .success(let listModel):
            var listData: [DeviceName] = [DeviceName(id: 0, name: "", display: "Tất cả")]
            
            listModel.forEach { name in
                switch name {
                case SKDevice.Name.CF_398:
                    let model = DeviceName(id: 1,
                                           name: name,
                                           display: SKDevice.NameDisplay.CF_398)
                    listData.append(model)
                    
                case SKDevice.Name.CF_539:
                    let model = DeviceName(id: 2,
                                           name: name,
                                           display: SKDevice.NameDisplay.CF_539)
                    listData.append(model)
                    
                case SKDevice.Name.CF_516, "Smart scale CF516BLE" :
                    if listData.first(where: {$0.name == SKDevice.Name.CF_516}) == nil {
                        let model = DeviceName(id: 3,
                                               name: SKDevice.Name.CF_516,
                                               display: SKDevice.NameDisplay.CF_516)
                        listData.append(model)
                    }
                    
                default:
                    break
                }
            }
            
            self.listDeviceName = listData.sorted(by: {$0.id ?? 0 < $1.id ?? 0})
            
            self.view?.onShowAlertScaleListView()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
