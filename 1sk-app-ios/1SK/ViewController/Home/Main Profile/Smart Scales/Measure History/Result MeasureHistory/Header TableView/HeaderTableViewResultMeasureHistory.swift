//
//  HeaderTableViewResultMeasureHistory.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//

import Foundation

class HeaderTableViewResultMeasureHistory: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var constraint_center_Kg: NSLayoutConstraint!
    @IBOutlet weak var lbl_Status: UILabel!
    
    @IBOutlet weak var view_HeartRate: UIView!
    @IBOutlet weak var lbl_HeartRate: UILabel!
    @IBOutlet weak var lbl_StatusHeartRate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    
    func setData(bodyFat: BodyFatModel?) {
        self.lbl_Time.text = bodyFat?.date?.toString(.hmdmy)
        self.lbl_Weight.text = bodyFat?.weight?.toString()
        self.constraint_center_Kg.constant = 10

        self.lbl_Status.text = bodyFat?.bodyFatSDK?.bmiIndex?.status_VN ?? "Không xác định"
        self.lbl_Status.superview?.backgroundColor = bodyFat?.bodyFatSDK?.bmiIndex?.color ?? UIColor.lightGray

        if let hrValue = bodyFat?.heartRate, hrValue > 0 {
            if let hrIndex = bodyFat?.bodyFatSDK?.heartRateIndex {
                self.lbl_HeartRate.text = "\(hrIndex.value ?? 0)"
                self.lbl_StatusHeartRate.text = hrIndex.status_VN
                self.lbl_StatusHeartRate.backgroundColor = hrIndex.color
                
                self.lbl_StatusHeartRate.isHidden = false
                
            } else {
                if let age = bodyFat?.age, age > 0 {
                    let hrIndex = HeartRate(value: hrValue, age: age)

                    self.lbl_HeartRate.text = "\(hrIndex.value ?? 0)"
                    self.lbl_StatusHeartRate.text = hrIndex.status_VN
                    self.lbl_StatusHeartRate.backgroundColor = hrIndex.color

                    self.lbl_StatusHeartRate.isHidden = false
                    
                } else {
                    let profile: ProfileRealm? = gRealm.objects().first(where: {$0.type == .SCALES})
                    if let birthday = profile?.birthday?.toDate(.dmySlash) {
                        let age = Date().year - birthday.year
                        let hr = HeartRate(value: hrValue, age: age)

                        self.lbl_HeartRate.text = "\(hr.value ?? 0)"
                        self.lbl_StatusHeartRate.text = hr.status_VN
                        self.lbl_StatusHeartRate.backgroundColor = hr.color
                        
                        self.lbl_StatusHeartRate.isHidden = false
                        
                    } else {
                        self.lbl_HeartRate.text = "\(hrValue)"
                        
                        self.lbl_StatusHeartRate.isHidden = true
                    }
                }
            }

        } else {
            if bodyFat?.device?.contains("CF516") == true {
                self.lbl_HeartRate.text = "_ _"
                self.lbl_StatusHeartRate.isHidden = true
                
            } else {
                self.lbl_HeartRate.text = "_ _"
                self.view_HeartRate.isHidden = true
                self.lbl_StatusHeartRate.isHidden = true
            }
        }

    }
}
