//
//  
//  ResultMeasureHistoryInteractorInput.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ResultMeasureHistoryInteractorInputProtocol {
    //
}

// MARK: - Interactor Output Protocol
protocol ResultMeasureHistoryInteractorOutputProtocol: AnyObject {
    //
}

// MARK: - ResultMeasureHistory InteractorInput
class ResultMeasureHistoryInteractorInput {
    weak var output: ResultMeasureHistoryInteractorOutputProtocol?
}

// MARK: - ResultMeasureHistory InteractorInputProtocol
extension ResultMeasureHistoryInteractorInput: ResultMeasureHistoryInteractorInputProtocol {
    //
}
