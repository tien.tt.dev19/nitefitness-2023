//
//  
//  ResultMeasureHistoryRouter.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ResultMeasureHistoryRouterProtocol {
    func onDismiss()
    func presentMeasureDetailRouter(index: IndexProtocol, bodyFat: BodyFatSDK?)
}

// MARK: - ResultMeasureHistory Router
class ResultMeasureHistoryRouter {
    weak var viewController: ResultMeasureHistoryViewController?
    
    static func setupModule(bodyFat: BodyFatModel?) -> ResultMeasureHistoryViewController {
        let viewController = ResultMeasureHistoryViewController()
        let router = ResultMeasureHistoryRouter()
        let interactorInput = ResultMeasureHistoryInteractorInput()
        let viewModel = ResultMeasureHistoryViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.bodyFat = bodyFat
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ResultMeasureHistory RouterProtocol
extension ResultMeasureHistoryRouter: ResultMeasureHistoryRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
    func presentMeasureDetailRouter(index: IndexProtocol, bodyFat: BodyFatSDK?) {
        let controller = MeasureDetailRouter.setupModule(index: index, bodyFat: bodyFat)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
}
