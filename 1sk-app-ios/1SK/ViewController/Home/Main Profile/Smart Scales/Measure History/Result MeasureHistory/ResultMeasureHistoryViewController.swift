//
//  
//  ResultMeasureHistoryViewController.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//
// 

import UIKit

// MARK: - ViewProtocol
protocol ResultMeasureHistoryViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - ResultMeasureHistory ViewController
class ResultMeasureHistoryViewController: BaseViewController {
    var router: ResultMeasureHistoryRouterProtocol!
    var viewModel: ResultMeasureHistoryViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header = HeaderTableViewResultMeasureHistory()
    var heightTableView: CGFloat = 0
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
        self.setInitUITableView()
        self.setInitHeaderTableView()
    }
    
    private func setInitLayout() {
        self.view_Nav.addShadow(width: 0, height: 6, color: .black, radius: 3, opacity: 0.05)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismiss()
    }
}

// MARK: - ResultMeasureHistory ViewProtocol
extension ResultMeasureHistoryViewController: ResultMeasureHistoryViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - UITableViewDataSource
extension ResultMeasureHistoryViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewMeasureResult.self)

        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self

        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension

        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension

    }

    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightTableView
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewMeasureResult.self, for: indexPath)
        cell.delegate = self
        cell.listIndex = self.viewModel.bodyFat?.bodyFatSDK?.indexs ?? []
        cell.onReloadData()

        return cell
    }
}

// MARK: - UITableView - Header
extension ResultMeasureHistoryViewController {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewResultMeasureHistory.loadFromNib()
        self.view_Header.setData(bodyFat: self.viewModel.bodyFat)
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: CellTableViewMeasureResultDelegate
extension ResultMeasureHistoryViewController: CellTableViewMeasureResultDelegate {
    func onUpdateHeightCell(height: CGFloat) {
        if height != self.heightTableView {
            self.heightTableView = height
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.tbv_TableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func onDidSelectItem(index: IndexProtocol) {
        guard let bodyFatSDK = self.viewModel.bodyFat?.bodyFatSDK else {
            return
        }
        self.router.presentMeasureDetailRouter(index: index, bodyFat: bodyFatSDK)
    }
}
