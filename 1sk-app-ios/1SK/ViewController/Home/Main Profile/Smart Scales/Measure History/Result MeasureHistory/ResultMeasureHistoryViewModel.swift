//
//  
//  ResultMeasureHistoryViewModel.swift
//  1SK
//
//  Created by Thaad on 05/12/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ResultMeasureHistoryViewModelProtocol {
    func onViewDidLoad()
    
    var bodyFat: BodyFatModel? { get set }
}

// MARK: - ResultMeasureHistory ViewModel
class ResultMeasureHistoryViewModel {
    weak var view: ResultMeasureHistoryViewProtocol?
    private var interactor: ResultMeasureHistoryInteractorInputProtocol

    init(interactor: ResultMeasureHistoryInteractorInputProtocol) {
        self.interactor = interactor
    }

    var bodyFat: BodyFatModel?
    
}

// MARK: - ResultMeasureHistory ViewModelProtocol
extension ResultMeasureHistoryViewModel: ResultMeasureHistoryViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - ResultMeasureHistory InteractorOutputProtocol
extension ResultMeasureHistoryViewModel: ResultMeasureHistoryInteractorOutputProtocol {
    //
}
