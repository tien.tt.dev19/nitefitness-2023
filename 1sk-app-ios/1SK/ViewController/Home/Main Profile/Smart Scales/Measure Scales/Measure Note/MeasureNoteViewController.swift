//
//  MeasureNoteViewController.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//

import UIKit

class MeasureNoteViewController: UIViewController {

    @IBOutlet weak var img_Scales: UIImageView!
    
    var device: DeviceRealm?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitUI()
    }

    private func setInitUI() {
        switch self.device?.code {
        case "CF398":
            self.img_Scales.image = R.image.img_scales_CF398()

        case "CF516":
            self.img_Scales.image = R.image.img_scales_CF516()

        case "CF539":
            self.img_Scales.image = R.image.img_scales_CF539()

        default:
            self.img_Scales.image = nil
        }
    }

}
