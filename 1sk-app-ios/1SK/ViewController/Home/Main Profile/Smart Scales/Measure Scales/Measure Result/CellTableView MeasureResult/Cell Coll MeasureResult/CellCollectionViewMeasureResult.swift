//
//  CellCollectionViewMeasureResult.swift
//  1SKConnect
//
//  Created by Thaad on 22/07/2022.
//

import UIKit

class CellCollectionViewMeasureResult: UICollectionViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Value: UILabel!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var view_Status: UIView!
    
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(index: IndexProtocol) {
        self.lbl_Name.text = index.name
        self.lbl_Status.text = index.status_VN
        self.lbl_Status.backgroundColor = index.color
        
        if self.indexPath?.row == 12 {
            self.view_Status.isHidden = true
        } else {
            self.view_Status.isHidden = false
        }
        
        if let value = index.value {
            switch index {
            case is BodyType:
                self.lbl_Value.text = index.status_VN
                
            case is BMR, is Fat:
                if let val = value as? (Double, Gender, Int) {
                    self.lbl_Value.text = val.0.toString()
                }
                
            case is Muscle, is Bone:
                if let val = value as? (Double, Gender) {
                    self.lbl_Value.text = val.0.toString()
                }
                
            case is Water:
                if let val = value as? (Double, Int) {
                    self.lbl_Value.text = val.0.toString()
                }
                
            default:
                if let val = value as? Double {
                    self.lbl_Value.text = val.toString()
                }
            }
        }
    }
}
