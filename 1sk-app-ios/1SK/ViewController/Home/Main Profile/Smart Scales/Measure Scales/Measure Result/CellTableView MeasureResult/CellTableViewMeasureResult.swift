//
//  CellTableViewMeasureResult.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//

import UIKit

protocol CellTableViewMeasureResultDelegate: AnyObject {
    func onUpdateHeightCell(height: CGFloat)
    func onDidSelectItem(index: IndexProtocol)
}

class CellTableViewMeasureResult: UITableViewCell {
    weak var delegate: CellTableViewMeasureResultDelegate?
    
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    var listIndex: [IndexProtocol] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUICollectionView()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func onReloadData() {
        self.coll_CollectionView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.delegate?.onUpdateHeightCell(height: self.coll_CollectionView.contentSize.height)
        }
    }
    
}

// MARK: - UICollectionViewDataSource
extension CellTableViewMeasureResult: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewMeasureResult.self)

        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 8
        layout.minimumLineSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        
        let widthColl: CGFloat = UIScreen.main.bounds.width - 32
        let widthCell = (widthColl - 16) / 3

        layout.itemSize = CGSize(width: widthCell, height: widthCell + 16)
        self.coll_CollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listIndex.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewMeasureResult.self, for: indexPath)
        cell.indexPath = indexPath
        cell.config(index: self.listIndex[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegate
extension CellTableViewMeasureResult: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.delegate?.onDidSelectItem(index: self.listIndex[indexPath.row])
    }
}
