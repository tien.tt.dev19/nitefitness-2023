//
//  FooterSectionMeasureResult.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//

import UIKit

protocol FooterSectionMeasureResultDelegate: AnyObject {
    func onCancelReweighedAction()
    func onAgreeReweighedAction()
}

class FooterSectionMeasureResult: UITableViewHeaderFooterView {

    weak var delegate: FooterSectionMeasureResultDelegate?
    
    @IBAction func onCancelReweighedAction(_ sender: Any) {
        self.delegate?.onCancelReweighedAction()
    }
    
    @IBAction func onAgreeReweighedAction(_ sender: Any) {
        self.delegate?.onAgreeReweighedAction()
    }

}
