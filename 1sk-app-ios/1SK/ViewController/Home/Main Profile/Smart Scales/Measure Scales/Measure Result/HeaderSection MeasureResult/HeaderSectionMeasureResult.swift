//
//  HeaderSectionMeasureResult.swift
//  1SKConnect
//
//  Created by Thaad on 25/07/2022.
//

import UIKit

protocol HeaderSectionMeasureResultDelegate: AnyObject {
    func setStopProgress()
}

class HeaderSectionMeasureResult: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var constraint_center_Kg: NSLayoutConstraint!
    @IBOutlet weak var lbl_Status: UILabel!
    
    @IBOutlet weak var view_HeartRate: UIView!
    @IBOutlet weak var view_Progress: UIView!
    
    @IBOutlet weak var lbl_HeartRate: UILabel!
    @IBOutlet weak var lbl_StatusHeartRate: UILabel!
    
    weak var delegate: HeaderSectionMeasureResultDelegate?
    
    var view_CircleProgress = CircleProgressViewHeartRate()
    var isStopProgress = false
    
    var device: DeviceRealm?
    var bodyFat: BodyFatSDK? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //
    }

    func setDataUI() {
        self.lbl_Time.text = self.bodyFat?.createAt?.toDate(.ymdhms)?.toString(.hmdmy)
        self.lbl_Weight.text = self.bodyFat?.weight?.toString()
        self.constraint_center_Kg.constant = 10
        
        if let bmi = self.bodyFat?.bmiIndex {
            self.lbl_Status.text = bmi.status_VN
            self.lbl_Status.superview?.backgroundColor = bmi.color
        } else {
            switch self.bodyFat?.errorType {
            case .impedance:
                self.lbl_Status.text = "Lỗi kháng trở"
                
            case .age:
                self.lbl_Status.text = "Lỗi tuổi"
                
            case .weight:
                self.lbl_Status.text = "Lỗi cân nặng"
                
            case .height:
                self.lbl_Status.text = "Lỗi chiều cao"
                
            default:
                self.lbl_Status.text = "Lỗi không xác định"
            }
        }
        
        if self.isStopProgress == false {
            self.setInitProgressMeasuringView()
        } else {
            self.view_Progress.backgroundColor = UIColor.init(hex: "7EC344")
        }
        
        if self.device?.code == "CF516" {
            self.view_HeartRate.isHidden = false
            
            if let hr = self.bodyFat?.heartRate, hr > 0 {
                self.isStopProgress = true
                guard let age = self.bodyFat?.age else {
                    return
                }
                let hrIndex = HeartRate(value: hr, age: age)
                self.lbl_StatusHeartRate.text = hrIndex.status_VN
                self.lbl_StatusHeartRate.backgroundColor = hrIndex.color
                self.lbl_StatusHeartRate.isHidden = false
                self.lbl_HeartRate.text = hr.asString

            } else {
                self.lbl_HeartRate.text = "_ _"
                self.lbl_StatusHeartRate.isHidden = true
            }
            
        } else {
            self.view_HeartRate.isHidden = true
        }
        
    }
    
}

// MARK: CircleProgressViewHeartRateDelegate
extension HeaderSectionMeasureResult: CircleProgressViewHeartRateDelegate {
    func setInitProgressMeasuringView() {
        self.view_CircleProgress.frame = self.view_Progress.bounds
        self.view_CircleProgress.delegate = self
        self.view_CircleProgress.createCircularPath()
        self.view_Progress.addSubview(self.view_CircleProgress)
        
        self.setStartProgressMeasuringView()
        
        Timer.scheduledTimer(withTimeInterval: 15.0, repeats: false) { timer in
            self.isStopProgress = true
        }
    }
    
    func setStartProgressMeasuringView() {
        guard self.view_CircleProgress.timeInterval == 0 else {
            return
        }
        self.view_CircleProgress.timeInterval = 5
        self.view_CircleProgress.startProgress()
    }
    
    func onCountDownSuccess() {
        if self.isStopProgress {
            self.delegate?.setStopProgress()
            
        } else {
            self.setStartProgressMeasuringView()
        }
    }
}
