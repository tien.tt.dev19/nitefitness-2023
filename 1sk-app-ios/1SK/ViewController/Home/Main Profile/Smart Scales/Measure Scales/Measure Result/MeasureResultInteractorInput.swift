//
//  
//  MeasureResultInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasureResultInteractorInputProtocol {
    func setDataScalesRecord(listSKBodyFat: [BodyFatSDK], device: DeviceRealm?, accessToken: String?)
}

// MARK: - Interactor Output Protocol
protocol MeasureResultInteractorOutputProtocol: AnyObject {
    func onSetScalesDataSyncFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - MeasureResult InteractorInput
class MeasureResultInteractorInput {
    weak var output: MeasureResultInteractorOutputProtocol?
    var connectService: ConnectServiceProtocol?
}

// MARK: - MeasureResult InteractorInputProtocol
extension MeasureResultInteractorInput: MeasureResultInteractorInputProtocol {
    func setDataScalesRecord(listSKBodyFat: [BodyFatSDK], device: DeviceRealm?, accessToken: String?) {
        self.connectService?.setDataScalesRecord(listBodyFat: listSKBodyFat, device: device, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onSetScalesDataSyncFinished(with: result.unwrapSuccessModel())
        })
    }
}
