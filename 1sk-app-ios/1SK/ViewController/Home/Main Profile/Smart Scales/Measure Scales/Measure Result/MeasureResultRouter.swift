//
//  
//  MeasureResultRouter.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasureResultRouterProtocol {
    func onDismiss()
    func presentMeasureDetailRouter(index: IndexProtocol, bodyFat: BodyFatSDK?)
}

// MARK: - MeasureResult Router
class MeasureResultRouter {
    weak var viewController: MeasureResultViewController?
    
    static func setupModule(delegate: MeasureResultViewDelegate?) -> MeasureResultViewController {
        let viewController = MeasureResultViewController()
        let router = MeasureResultRouter()
        let interactorInput = MeasureResultInteractorInput()
        let viewModel = MeasureResultViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.connectService = ConnectService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MeasureResult RouterProtocol
extension MeasureResultRouter: MeasureResultRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
    func presentMeasureDetailRouter(index: IndexProtocol, bodyFat: BodyFatSDK?) {
        let controller = MeasureDetailRouter.setupModule(index: index, bodyFat: bodyFat)
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
    
}
