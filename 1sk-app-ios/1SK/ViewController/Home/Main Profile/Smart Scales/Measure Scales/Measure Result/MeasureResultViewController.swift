//
//  
//  MeasureResultViewController.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit

protocol MeasureResultViewDelegate: AnyObject {
    func onCancelReweighedAction()
    func onAgreeReweighedAction()
}

// MARK: - ViewProtocol
protocol MeasureResultViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
}

// MARK: - MeasureResult ViewController
class MeasureResultViewController: BaseViewController {
    var router: MeasureResultRouterProtocol!
    var viewModel: MeasureResultViewModelProtocol!
    weak var delegate: MeasureResultViewDelegate?
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var heightTableView: CGFloat = 0
    var isHiddenCancelReweighed = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
    }
}

// MARK: - MeasureResult ViewProtocol
extension MeasureResultViewController: MeasureResultViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension MeasureResultViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionMeasureResult.self)
        self.tbv_TableView.registerFooterNib(ofType: FooterSectionMeasureResult.self)
        
        self.tbv_TableView.registerNib(ofType: CellTableViewMeasureResult.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderSectionMeasureResult.self)
        header.delegate = self
        header.isStopProgress = self.viewModel.isStopProgress
        header.device = self.viewModel.device
        header.bodyFat = self.viewModel.bodyFat
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        guard self.heightTableView > 0 else {
            return nil
        }
        guard self.viewModel.bodyFat?.indexs?.count ?? 0 < 13 else {
            return nil
        }
        guard self.isHiddenCancelReweighed == false else {
            return nil
        }
        let header = tableView.dequeueHeaderView(ofType: FooterSectionMeasureResult.self)
        header.delegate = self
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.heightTableView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewMeasureResult.self, for: indexPath)
        cell.delegate = self
        cell.listIndex = self.viewModel.bodyFat?.indexs ?? []
        cell.onReloadData()
        
        return cell
    }
}

// MARK: FooterSectionMeasureResultDelegate
extension MeasureResultViewController: FooterSectionMeasureResultDelegate {
    func onCancelReweighedAction() {
        //self.delegate?.onCancelReweighedAction()
        
        self.isHiddenCancelReweighed = true
        self.onReloadData()
    }
    
    func onAgreeReweighedAction() {
        self.delegate?.onAgreeReweighedAction()
    }
}

// MARK: CellTableViewMeasureResultDelegate
extension MeasureResultViewController: CellTableViewMeasureResultDelegate {
    func onUpdateHeightCell(height: CGFloat) {
        if height != self.heightTableView {
            self.heightTableView = height
            
            let indexPath = IndexPath(row: 0, section: 0)
            self.tbv_TableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    func onDidSelectItem(index: IndexProtocol) {
        self.router.presentMeasureDetailRouter(index: index, bodyFat: self.viewModel.bodyFat)
    }
}

// MARK: HeaderSectionMeasureResultDelegate
extension MeasureResultViewController: HeaderSectionMeasureResultDelegate {
    func setStopProgress() {
        self.viewModel.isStopProgress = true
    }
}
