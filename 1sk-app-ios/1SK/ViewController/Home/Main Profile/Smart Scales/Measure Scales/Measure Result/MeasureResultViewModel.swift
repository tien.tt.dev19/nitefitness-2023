//
//  
//  MeasureResultViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MeasureResultViewModelProtocol {
    func onViewDidLoad()
    func onDismissAction()
    
    var device: DeviceRealm? { get set }
    var bodyFat: BodyFatSDK? { get set }
    var heartRate: Int? { get set }
    
    var isStopProgress: Bool { get set }
}

// MARK: - MeasureResult ViewModel
class MeasureResultViewModel {
    weak var view: MeasureResultViewProtocol?
    private var interactor: MeasureResultInteractorInputProtocol

    init(interactor: MeasureResultInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var isStopProgress: Bool = false
    var isSyncDataBodyFat = true
    
    var device: DeviceRealm?
    var bodyFat: BodyFatSDK? {
        didSet {
            if self.bodyFat?.indexs?.count ?? 0 < 4 {
                self.isStopProgress = true
            }
            
            self.view?.onReloadData()
            
            if self.device?.code != "CF516" {
                self.setScalesDataSync()
            }
        }
    }
    
    var heartRate: Int? {
        didSet {
            if let hr = self.heartRate, hr > 0, let age = self.bodyFat?.age {
                self.bodyFat?.heartRate = hr
                self.bodyFat?.heartRateIndex = HeartRate(value: hr, age: age)

                self.setScalesDataSync()
            }
            
            self.view?.onReloadData()
        }
    }
}

// MARK: - MeasureResult ViewModelProtocol
extension MeasureResultViewModel: MeasureResultViewModelProtocol {
    func onViewDidLoad() {
        //
    }
    
    func onDismissAction() {
        self.setScalesDataSync()
    }
}

// MARK: - MeasureResult Handle Data
extension MeasureResultViewModel {
    func setScalesDataSync() {
        guard let device = self.device else {
            print("setScalesDataSync 0")
            return
        }
        guard let profile = device.profile else {
            print("setScalesDataSync 1")
            return
        }
        guard let accessToken = profile.accessToken else {
            print("setScalesDataSync 2")
            return
        }
        guard let userId = profile.id else {
            print("setScalesDataSync 3")
            return
        }
        self.bodyFat?.userId = userId
        
        guard let bodyFat = self.bodyFat else {
            print("setScalesDataSync 4")
            return
        }
        guard bodyFat.weight ?? 0 > 0 else {
            print("setScalesDataSync 5")
            return
        }
        guard bodyFat.bmiIndex != nil else {
            print("setScalesDataSync 9")
            return
        }
        
        print("setScalesDataSync 6: ", self.isSyncDataBodyFat)
        guard self.isSyncDataBodyFat == true else {
            print("setScalesDataSync 7")
            return
        }
        
        print("setScalesDataSync 8")
        self.isSyncDataBodyFat = false
        self.interactor.setDataScalesRecord(listSKBodyFat: [bodyFat], device: device, accessToken: accessToken)
    }
}

// MARK: - MeasureResult InteractorOutputProtocol
extension MeasureResultViewModel: MeasureResultInteractorOutputProtocol {
    func onSetScalesDataSyncFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
