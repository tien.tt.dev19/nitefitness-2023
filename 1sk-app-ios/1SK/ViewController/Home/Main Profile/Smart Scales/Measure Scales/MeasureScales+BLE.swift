//
//  MeasureScales+BLE.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//

import Foundation
import CoreBluetooth

// MARK: - BluetoothManager
extension MeasureScalesViewController {
    func setInitBluetoothManager() {
        print("<Bluetooth> 1SK Scales setInitBluetoothManager")
        
        if let peripheral = BluetoothManager.shared.peripheral {
            BluetoothManager.shared.setDisconnectDevice(peripheral: peripheral)
        }
        
        BluetoothManager.shared.delegate_BluetoothManager = self
        BluetoothManager.shared.listDeviceName = []
        if let name = self.viewModel.device?.name {
            BluetoothManager.shared.listDeviceName.append(name.replaceCharacter(target: "BLE", withString: ""))
        }
        
        self.setScanPeripherals()
    }
    
    func setConnectPeripheral() {
        guard let peripheral = BluetoothManager.shared.peripheral else { return }
        BluetoothManager.shared.setConnectDevice(peripheral: peripheral)
    }
    
    func setDisconnectPeripheral() {
        guard let peripheral = BluetoothManager.shared.peripheral else { return }
        BluetoothManager.shared.setDisconnectDevice(peripheral: peripheral)
    }
    
    func setScanPeripherals() {
        BluetoothManager.shared.peripheral?.delegate = nil
        BluetoothManager.shared.peripheral = nil
        BluetoothManager.shared.setScanStartPeripherals()
    }
}

// MARK: - BluetoothManagerDelegate
extension MeasureScalesViewController: BluetoothManagerDelegate {
    func onDidUpdateState(central: CBCentralManager) {
        print("<Bluetooth> 1SK Scales onDidUpdateState central.state:", central.state.rawValue)
    }
    
    func onDidTimeoutScanPeripheralDevice() { }
    
    func onDidDiscover(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Scales onDidDiscover device: \(self.viewModel.device?.name ?? "NULL") - \(self.viewModel.device?.uuid ?? "NULL") - \(self.viewModel.device?.mac ?? "NULL")")
        print("<Bluetooth> 1SK Scales onDidDiscover peripheral: \(peripheral.name ?? "NULL") - \(peripheral.uuid) - \(peripheral.mac)")
        print("<Bluetooth> 1SK Scales onDidDiscover ===================\n")
        
        guard BluetoothManager.shared.peripheral == nil else {
            return
        }
        guard peripheral.uuid == self.viewModel.device?.uuid else {
            return
        }
        
        BluetoothManager.shared.setScanStopPeripherals()
        BluetoothManager.shared.peripheral = peripheral
        
        self.setConnectPeripheral()
    }
    
    func onDidConnected(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Scales onDidConnected peripheral.name:", peripheral.name ?? "NULL")
        self.setInitCBPeripheral(peripheral: peripheral)
    }
    
    func onDidFailToConnect(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Scales onDidFailToConnect peripheral.name:", peripheral.name ?? "NULL")
        peripheral.delegate = nil
        self.setScanPeripherals()
    }
    
    func onDidDisconnect(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Scales onDidDisconnect peripheral.name:", peripheral.name ?? "NULL")
        peripheral.delegate = nil
        self.setScanPeripherals()
    }
}

// MARK: - CBPeripheralDelegate
extension MeasureScalesViewController: CBPeripheralDelegate {
    func setInitCBPeripheral(peripheral: CBPeripheral) {
        print("<Bluetooth> 1SK Scales setInitCBPeripheral")
        peripheral.delegate = self
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        peripheral.services?.forEach({ (service) in
            peripheral.discoverCharacteristics(nil, for: service)
            print("<Bluetooth> 1SK Scales didDiscoverServices service:", service.debugDescription)
        })
    }

    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        print("<Bluetooth> 1SK Scales didDiscoverCharacteristicsFor")
        
        guard error == nil else {
            print("<Bluetooth> 1SK Scales didDiscoverCharacteristicsFor error: ", error.debugDescription)
            return
        }
        guard let characteristics = service.characteristics else {
            return
        }
        characteristics.forEach { characteristic in
            print("<Bluetooth> 1SK Scales didDiscoverCharacteristicsFor characteristic.uuid: ", characteristic.uuid)
            
            if BluetoothManager.shared.scalesNotifyUUIDs.contains(characteristic.uuid) && characteristic.properties.contains(.notify) {
                peripheral.setNotifyValue(true, for: characteristic)
            }
            
            if BluetoothManager.shared.scalesWriteUUIDs.contains(characteristic.uuid) && characteristic.properties.contains(.write) {
                //
            }
        }
    }

    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        guard error == nil else {
            print("<Bluetooth> 1SK Scales didUpdateValueFor error: ", error.debugDescription)
            return
        }
        guard let value = characteristic.value else {
            return
        }
        print("<Bluetooth> 1SK Scales didUpdateValueFor characteristic.value: ", [UInt8](value))
        
        if self.stateUI == .result {
            guard let scaleData = SKScaleData(bytes: [UInt8](value)) else {
                return
            }
            
            let weight = Float(scaleData.weight)
            self.controllerMeasuringScalesVC.setUpdateWeight(weight: weight)
            
            // Heart Rate
            switch scaleData.heartRateMeasureState {
            case .measuring:
                print("didUpdateValueFor heartRate measuring scaleData.heartRate:", scaleData.heartRate)
                
            case .finished:
                print("didUpdateValueFor heartRate finished scaleData.heartRate:", scaleData.heartRate)
                self.controllerMeasureResultVC.viewModel.heartRate = Int(scaleData.heartRate)
                
            case .none:
                print("didUpdateValueFor heartRate none scaleData.heartRate:", scaleData.heartRate)
            }
            
        } else {
            guard let scaleData = SKScaleData(bytes: [UInt8](value)) else {
                return
            }
            print("<Bluetooth> 1SK Scales didUpdateValueFor scaleData: ", scaleData)
            print("<Bluetooth> 1SK Scales didUpdateValueFor encryptionImpedance: ", scaleData.encryptionImpedance)
            
            let weight = Float(scaleData.weight)
            
            switch scaleData.dataType {
            case .transmittingData:
                if weight == 0 {
                    self.setStateUIMeasureScales(state: .warning)
                    BluetoothManager.shared.setConnectDevice(peripheral: peripheral)
                    
                } else {
                    if self.stateUI == .warning {
                        self.setStateUIMeasureScales(state: .measuring)
                    }
                    
                    self.controllerMeasuringScalesVC.setUpdateWeight(weight: weight)
                }
                
            case .lockData:
                self.viewModel.bodyFat = self.getCaculateBodyFatData(weight: weight, impedance: scaleData.encryptionImpedance)

                self.controllerMeasuringScalesVC.setUpdateWeight(weight: weight)
                self.controllerMeasuringScalesVC.isStopProgress = true
                
            case .overloaded:
                SKToast.shared.showToast(content: "Bạn đã đạt quá giới hạn cân")
                
            default:
                break
            }
        }
    }
    
    private func getCaculateBodyFatData(weight: Float, impedance: Int) -> BodyFatSDK? {
        guard let profile = self.viewModel.device?.profile else {
            return nil
        }
        guard
            let height = profile.height,
            let birthday = profile.birthday?.toDate(.dmySlash),
            let gender = profile.gender else {
            SKToast.shared.showToast(content: "Lỗi hồ sơ người dùng không đầy đủ")
            return nil
        }
        let age = Date().year - birthday.year
        let bodyFat = BodyFatSDK(impedance: impedance, weight: weight, height: height, age: age, gender: gender, isMeasure: true)
        return bodyFat
    }
    
}

// MARK: MeasuringScalesViewDelegate
extension MeasureScalesViewController: MeasuringScalesViewDelegate {
    func onStopMeasuringScales() {
        self.setStateUIMeasureScales(state: .result)
    }
}

// MARK: MeasureResultViewDelegate
extension MeasureScalesViewController: MeasureResultViewDelegate {
    func onCancelReweighedAction() {
        self.router.dismiss()
    }
    
    func onAgreeReweighedAction() {
        self.setStateUIMeasureScales(state: .warning)
    }
}
