//
//  
//  MeasureScalesInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasureScalesInteractorInputProtocol {
    
}

// MARK: - Interactor Output Protocol
protocol MeasureScalesInteractorOutputProtocol: AnyObject {
    
}

// MARK: - MeasureScales InteractorInput
class MeasureScalesInteractorInput {
    weak var output: MeasureScalesInteractorOutputProtocol?
}

// MARK: - MeasureScales InteractorInputProtocol
extension MeasureScalesInteractorInput: MeasureScalesInteractorInputProtocol {
    
}
