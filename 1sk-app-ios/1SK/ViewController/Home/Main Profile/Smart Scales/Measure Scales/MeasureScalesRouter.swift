//
//  
//  MeasureScalesRouter.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasureScalesRouterProtocol {
    func dismiss()
}

// MARK: - MeasureScales Router
class MeasureScalesRouter {
    weak var viewController: MeasureScalesViewController?
    
    static func setupModule(device: DeviceRealm?, delegate: MeasureScalesViewDelegate?) -> MeasureScalesViewController {
        let viewController = MeasureScalesViewController()
        let router = MeasureScalesRouter()
        let interactorInput = MeasureScalesInteractorInput()
        let viewModel = MeasureScalesViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MeasureScales RouterProtocol
extension MeasureScalesRouter: MeasureScalesRouterProtocol {
    func dismiss() {
        self.viewController?.dismiss(animated: true)
    }
}
