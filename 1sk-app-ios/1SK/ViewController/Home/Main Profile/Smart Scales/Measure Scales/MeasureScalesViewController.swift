//
//  
//  MeasureScalesViewController.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit
import SnapKit

// MARK: StatesViewMeasureScales
enum StatesViewMeasureScales {
    case warning
    case measuring
    case result
}

protocol MeasureScalesViewDelegate: AnyObject {
    func setRefreshDataMeasureHistory()
}

// MARK: - ViewProtocol
protocol MeasureScalesViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
}

// MARK: - MeasureScales ViewController
class MeasureScalesViewController: BaseViewController {
    var router: MeasureScalesRouterProtocol!
    var viewModel: MeasureScalesViewModelProtocol!
    weak var delegate: MeasureScalesViewDelegate?
    
    lazy var controllerMeasureNoteVC = MeasureNoteViewController()
    lazy var controllerMeasuringScalesVC = MeasuringScalesRouter.setupModule(delegate: self)
    lazy var controllerMeasureResultVC = MeasureResultRouter.setupModule(delegate: self)
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var view_Container: UIView!
    
    var stateUI: StatesViewMeasureScales?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        
        self.setStateUIMeasureScales(state: .warning)
        self.setInitBluetoothManager()
    }
    
    deinit {
        self.setDisconnectPeripheral()
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        if self.stateUI == .result {
            self.controllerMeasureResultVC.viewModel.onDismissAction()
            self.delegate?.setRefreshDataMeasureHistory()
        }
        self.router.dismiss()
    }
}

// MARK: - MeasureScales ViewProtocol
extension MeasureScalesViewController: MeasureScalesViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - ChildView
extension MeasureScalesViewController {
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
    
    func setStateUIMeasureScales(state: StatesViewMeasureScales) {
        switch state {
        case .warning:
            self.lbl_Title.text = "Đo cân nặng"
            self.stateUI = .warning
            self.controllerMeasuringScalesVC.isStopProgress = false
            self.controllerMeasureNoteVC.device = self.viewModel.device
            self.addChildView(childViewController: self.controllerMeasureNoteVC)
            
        case .measuring:
            self.lbl_Title.text = "Đo cân nặng"
            self.stateUI = .measuring
            self.controllerMeasuringScalesVC.viewModel.device = self.viewModel.device
            self.addChildView(childViewController: self.controllerMeasuringScalesVC)
            self.controllerMeasuringScalesVC.setDescHeartRateUI(code: self.viewModel.device?.code)
            self.controllerMeasuringScalesVC.setStartProgressMeasuringView()
            
        case .result:
            self.lbl_Title.text = "Kết quả đo"
            self.stateUI = .result
            self.addChildView(childViewController: self.controllerMeasureResultVC)
            self.controllerMeasureResultVC.isHiddenCancelReweighed = false
            self.controllerMeasureResultVC.viewModel.device = self.viewModel.device
            self.controllerMeasureResultVC.viewModel.bodyFat = self.viewModel.bodyFat
        }
    }
}
