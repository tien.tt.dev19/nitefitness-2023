//
//  
//  MeasureScalesViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 19/07/2022.
//
//

import UIKit
import CoreBluetooth

// MARK: - ViewModelProtocol
protocol MeasureScalesViewModelProtocol {
    func onViewDidLoad()
    
    var device: DeviceRealm? { get set }
    var bodyFat: BodyFatSDK? { get set }
}

// MARK: - MeasureScales ViewModel
class MeasureScalesViewModel {
    weak var view: MeasureScalesViewProtocol?
    private var interactor: MeasureScalesInteractorInputProtocol

    init(interactor: MeasureScalesInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    var bodyFat: BodyFatSDK?
    
}

// MARK: - MeasureScales ViewModelProtocol
extension MeasureScalesViewModel: MeasureScalesViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - MeasureScales InteractorOutputProtocol
extension MeasureScalesViewModel: MeasureScalesInteractorOutputProtocol {
    
}
