//
//  
//  MeasuringScalesInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 18/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MeasuringScalesInteractorInputProtocol {
    
}

// MARK: - Interactor Output Protocol
protocol MeasuringScalesInteractorOutputProtocol: AnyObject {
    
}

// MARK: - MeasuringScales InteractorInput
class MeasuringScalesInteractorInput {
    weak var output: MeasuringScalesInteractorOutputProtocol?
}

// MARK: - MeasuringScales InteractorInputProtocol
extension MeasuringScalesInteractorInput: MeasuringScalesInteractorInputProtocol {
    
}
