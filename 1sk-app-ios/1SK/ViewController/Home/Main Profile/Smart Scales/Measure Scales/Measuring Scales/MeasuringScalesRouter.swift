//
//  
//  MeasuringScalesRouter.swift
//  1SKConnect
//
//  Created by Thaad on 18/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MeasuringScalesRouterProtocol {
    
}

// MARK: - MeasuringScales Router
class MeasuringScalesRouter {
    weak var viewController: MeasuringScalesViewController?
    
    static func setupModule(delegate: MeasuringScalesViewDelegate?) -> MeasuringScalesViewController {
        let viewController = MeasuringScalesViewController()
        let router = MeasuringScalesRouter()
        let interactorInput = MeasuringScalesInteractorInput()
        let viewModel = MeasuringScalesViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MeasuringScales RouterProtocol
extension MeasuringScalesRouter: MeasuringScalesRouterProtocol {
    
}
