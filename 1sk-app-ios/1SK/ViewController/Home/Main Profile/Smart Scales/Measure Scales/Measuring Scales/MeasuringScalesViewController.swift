//
//  
//  MeasuringScalesViewController.swift
//  1SKConnect
//
//  Created by Thaad on 18/07/2022.
//
//

import UIKit

protocol MeasuringScalesViewDelegate: AnyObject {
    func onStopMeasuringScales()
}

// MARK: - ViewProtocol
protocol MeasuringScalesViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
}

// MARK: - MeasuringScales ViewController
class MeasuringScalesViewController: BaseViewController {
    var router: MeasuringScalesRouterProtocol!
    var viewModel: MeasuringScalesViewModelProtocol!
    weak var delegate: MeasuringScalesViewDelegate?
    
    @IBOutlet weak var view_Progress: UIView!
    
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var constraint_center_Kg: NSLayoutConstraint!
    
    @IBOutlet weak var view_DescHeartRate: UIView!
    
    var view_CircleProgress = CircleProgressViewScales()
    var isStopProgress = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitProgressMeasuringView()
    }
    
    func setDescHeartRateUI(code: String?) {
        self.view_DescHeartRate.isHidden = true
        if code == "CF516" {
            Timer.scheduledTimer(withTimeInterval: 2.5, repeats: false) { timer in
                self.view_DescHeartRate.isHidden = false
            }
        }
    }
    
    func setUpdateWeight(weight: Float) {
        guard self.lbl_Weight != nil else {
            return
        }
        self.lbl_Weight.text = "\(weight)"
        if self.constraint_center_Kg.constant == 0 {
            constraint_center_Kg.constant = 13
        }
    }
    
    // MARK: - Action
    @IBAction func onCloseDescHeartRateAction(_ sender: Any) {
        self.view_DescHeartRate.isHidden = true
    }
    
}

// MARK: - MeasuringScales ViewProtocol
extension MeasuringScalesViewController: MeasuringScalesViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
}

// MARK: CircleProgressViewScalesDelegate
extension MeasuringScalesViewController: CircleProgressViewScalesDelegate {
    func setInitProgressMeasuringView() {
        self.view_CircleProgress.frame = self.view_Progress.bounds
        self.view_CircleProgress.delegate = self
        self.view_CircleProgress.createCircularPath()
        self.view_Progress.addSubview(self.view_CircleProgress)
    }
    
    func setStartProgressMeasuringView() {
        guard self.view_CircleProgress.timeInterval == 0 else {
            return
        }
        self.view_CircleProgress.timeInterval = 5
        self.view_CircleProgress.startProgress()
    }
    
    func onCountDownSuccess() {
        if self.isStopProgress {
            self.delegate?.onStopMeasuringScales()
            
        } else {
            self.setStartProgressMeasuringView()
        }
    }
}
