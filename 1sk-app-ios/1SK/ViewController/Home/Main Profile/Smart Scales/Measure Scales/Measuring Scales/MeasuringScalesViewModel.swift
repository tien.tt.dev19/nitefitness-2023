//
//  
//  MeasuringScalesViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 18/07/2022.
//
//

import UIKit

// MARK: - State
extension MeasuringScalesViewModel {
    enum State {
        case waiting
        case measuring
        case finish
    }
}

// MARK: - ViewModelProtocol
protocol MeasuringScalesViewModelProtocol {
    func onViewDidLoad()
    
    var device: DeviceRealm? { get set }
}

// MARK: - MeasuringScales ViewModel
class MeasuringScalesViewModel {
    weak var view: MeasuringScalesViewProtocol?
    private var interactor: MeasuringScalesInteractorInputProtocol

    init(interactor: MeasuringScalesInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
    var state: State = .waiting
}

// MARK: - MeasuringScales ViewModelProtocol
extension MeasuringScalesViewModel: MeasuringScalesViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - MeasuringScales InteractorOutputProtocol
extension MeasuringScalesViewModel: MeasuringScalesInteractorOutputProtocol {
    
}
