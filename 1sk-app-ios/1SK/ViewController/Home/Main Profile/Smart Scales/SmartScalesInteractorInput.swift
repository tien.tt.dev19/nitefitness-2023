//
//  
//  SmartScalesInteractorInput.swift
//  1SK
//
//  Created by Thaad on 27/10/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol SmartScalesInteractorInputProtocol {
    func getPostsScales(tag: String, index: String)
    func getSliderBanner(typeSlider: SliderType)
    func getDataScalesMeasual(userId: Int, page: Int, perPage: Int, accessToken: String?)
    func getConsultIndex(index: String, status: String, gender: Gender, age: Int, weight: Double)
}

// MARK: - Interactor Output Protocol
protocol SmartScalesInteractorOutputProtocol: AnyObject {
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>)
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>)
    func onGetDataScalesMeasualFinished(with result: Result<[BodyFatModel], APIError>)
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>)
}

// MARK: - SmartScales InteractorInput
class SmartScalesInteractorInput {
    weak var output: SmartScalesInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
    var connectService: ConnectServiceProtocol?
    var workPressService: WorkPressServiceProtocol?
}

// MARK: - SmartScales InteractorInputProtocol
extension SmartScalesInteractorInput: SmartScalesInteractorInputProtocol {
    func getPostsScales(tag: String, index: String) {
        self.workPressService?.getPostsScales(tag: tag, index: index, completion: { [weak self] result in
            self?.output?.onGetPostsScalesFinished(with: result.unwrapSuccessModelWP())
        })
    }
    
    func getSliderBanner(typeSlider: SliderType) {
        self.storeService?.getSliderBanner(typeSlider: typeSlider, completion: { [weak self] result in
            self?.output?.onGetSliderBannerFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getDataScalesMeasual(userId: Int, page: Int, perPage: Int, accessToken: String?) {
        self.connectService?.getDataScalesRecent(userId: userId, page: page, perPage: perPage, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetDataScalesMeasualFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getConsultIndex(index: String, status: String, gender: Gender, age: Int, weight: Double) {
        self.connectService?.getConsultIndexScales(index: index, status: status, gender: gender, age: age, weight: weight, completion: { [weak self] result in
            self?.output?.onGetConsultIndexFinished(with: result.unwrapSuccessModel())
        })
    }
}
