//
//  
//  SmartScalesRouter.swift
//  1SK
//
//  Created by Thaad on 27/10/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol SmartScalesRouterProtocol {
    func presentPairingDeviceRouter(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?)
    func presentUpdateSubProfileRouter(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?)
    func presentMeasureScalesRouter(device: DeviceRealm?, delegate: MeasureScalesViewDelegate?)
    func presentAlertSettingBluetoothVC()
    func showMeasureHistoryRouter(device: DeviceRealm?)
    func showChatMessageRouter()
}

// MARK: - SmartScales Router
class SmartScalesRouter {
    weak var viewController: SmartScalesViewController?
    
    static func setupModule(device: DeviceRealm?, delegate: SmartScalesViewDelegate?) -> SmartScalesViewController {
        let viewController = SmartScalesViewController()
        let router = SmartScalesRouter()
        let interactorInput = SmartScalesInteractorInput()
        let viewModel = SmartScalesViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        interactorInput.connectService = ConnectService()
        interactorInput.workPressService = WorkPressService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - SmartScales RouterProtocol
extension SmartScalesRouter: SmartScalesRouterProtocol {
    func presentPairingDeviceRouter(device: DeviceRealm?, delegate: PairingDeviceViewDelegate?) {
        let controller = PairingDeviceRouter.setupModule(device: device, delegate: delegate)
        self.viewController?.navigationController?.present(controller, animated: true)
    }
    
    func presentUpdateSubProfileRouter(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?) {
        let controller = UpdateSubProfileRouter.setupModule(authModel: authModel, delegate: delegate)
        controller.isHiddenDeleteButton = true
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func presentMeasureScalesRouter(device: DeviceRealm?, delegate: MeasureScalesViewDelegate?) {
        let controller = MeasureScalesRouter.setupModule(device: device, delegate: delegate)
        self.viewController?.present(controller, animated: true)
    }
    
    func presentAlertSettingBluetoothVC() {
        let controller = AlertSettingBluetoothViewController()
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: false)
    }
    
    func showMeasureHistoryRouter(device: DeviceRealm?) {
        let controller = MeasureHistoryRouter.setupModule(device: device)
        self.viewController?.present(controller, animated: true)
    }
    
    func showChatMessageRouter() {
        let controller = ChatMessageRouter.setupModule()
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
}
