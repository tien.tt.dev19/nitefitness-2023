//
//  
//  SmartScalesViewController.swift
//  1SK
//
//  Created by Thaad on 27/10/2022.
//
//

import UIKit

protocol SmartScalesViewDelegate: AnyObject {
    func onConnectionAction(device: DeviceRealm?)
}

// MARK: - ViewProtocol
protocol SmartScalesViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func setDataBodyFat()
    func setDataConsult()
}

// MARK: - SmartScales ViewController
class SmartScalesViewController: BaseViewController {
    var router: SmartScalesRouterProtocol!
    var viewModel: SmartScalesViewModelProtocol!
    weak var delegate: SmartScalesViewDelegate?
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var view_Header: HeaderTableViewSmartScales?
    
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
        self.setLayoutHeaderTableView()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitRefreshControl()
        self.setLayoutHeaderTableView()
        
        BluetoothManager.shared.setInitManager()
    }
}

// MARK: - SmartScales ViewProtocol
extension SmartScalesViewController: SmartScalesViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setDataBodyFat() {
        guard let bodyFat = self.viewModel.listBodyFat.first else {
            return
        }
        self.view_Header?.setDataRecentResults(bodyFat: bodyFat)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setDataConsult() {
        self.view_Header?.setUpdateDataConsult(consult: self.viewModel.consult)
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: Refresh Control
extension SmartScalesViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }

    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension SmartScalesViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerFooterNib(ofType: FooterSectionViewSmartScales.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewSmartScalesPost.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if self.viewModel.isPairedDevice(), let sliderBanner = self.viewModel.sliderBanner {
            let footer = tableView.dequeueHeaderView(ofType: FooterSectionViewSmartScales.self)
            footer.sliderBanner = sliderBanner
            return footer
            
        } else {
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.isPairedDevice() {
            return self.viewModel.listPostSuggest.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewSmartScalesPost.self, for: indexPath)
        cell.post = self.viewModel.listPostSuggest[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SmartScalesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let url = URL(string: self.viewModel.listPostSuggest[indexPath.row].link ?? "") else {
            return
        }
        UIApplication.shared.open(url)
    }
}

// MARK: UITableView Header
extension SmartScalesViewController: HeaderTableViewSmartScalesDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewSmartScales.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setLayoutHeaderTableView() {
        self.view_Header?.setLayoutState(isPaired: self.viewModel.isPairedDevice())
        self.view_Header?.setImageDeviceType(code: self.viewModel.device?.code)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // MARK: HeaderTableSmartScalesViewDelegate
    
    func onScalseMeasualAction() {
        guard self.checkValidateFullProfile() else {
            return
        }
        
        switch BluetoothManager.shared.stateCBManager() {
        case .poweredOn:
            print("<Bluetooth> Scalse central.state is .poweredOn")
            self.router.presentMeasureScalesRouter(device: self.viewModel.device, delegate: self)

        case .poweredOff:
            print("<Bluetooth> Scalse central.state is .poweredOff")
            self.router.presentAlertSettingBluetoothVC()
            
        case .resetting:
            print("<Bluetooth> Scalse central.state is .resetting")
            self.alertDefault(title: "Bluetooth", message: "Đang kết nối thiết bị")

        case .unsupported:
            print("<Bluetooth> Scalse central.state is .unsupported")
            self.alertDefault(title: "Bluetooth", message: "Thiết bị này không hỗ trợ")

        case .unauthorized:
            print("<Bluetooth> Scalse central.state is .unauthorized")
            self.alertOpenSettingsURLString(title: "\"1SK\" muốn sử dụng Bluetooth", message: "1SK cần cho phép truy cập Bluetooth để có thể tiếp tục với tính năng này, nó sẽ giúp bạn có thể kết nối với các thiết bị thông minh. Nếu đồng ý, bạn vui lòng đến Cài đặt và bấm cho phép")
            
        case .unknown:
            print("<Bluetooth> Scalse central.state is .unknown")
            self.alertDefault(title: "Bluetooth", message: "Không xác định")
            
        default:
            print("<Bluetooth> Scalse central.state is .default")
            self.alertDefault(title: "Bluetooth", message: "Chưa khởi tạo")
        }
    }
    
    func onConnectWithDeviceAction() {
        self.router.presentPairingDeviceRouter(device: self.viewModel.device, delegate: self)
    }
    
    func onHistoryAction() {
        guard self.checkValidateFullProfile() else {
            return
        }
        self.router.showMeasureHistoryRouter(device: self.viewModel.device)
    }
    
    func onAdviseAction() {
        self.router.showChatMessageRouter()
    }
    
    func onDidFinishLoadWebView() {
        self.tbv_TableView.updateFrameHeaderView()
    }
}

// MARK: Validate Full Profile
extension SmartScalesViewController {
    private func checkValidateFullProfile() -> Bool {
        guard self.viewModel.device?.profile?.fullName?.count ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.birthday?.count ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.gender != nil  else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.weight ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }
        guard self.viewModel.device?.profile?.height ?? 0 > 0 else {
            self.presentAlertPopupConfirmTitleView()
            return false
        }

        return true
    }
}

// MARK: PairingDeviceViewDelegate
extension SmartScalesViewController: PairingDeviceViewDelegate {
    func onConnectionAction(device: DeviceRealm) {
        guard let profile: ProfileRealm = gRealm.objects().first(where: {$0.type == .SCALES}) else {
            return
        }
        self.viewModel.device = device
        self.viewModel.device?.profile = profile
        self.delegate?.onConnectionAction(device: device)
        
        self.setLayoutHeaderTableView()
        self.onReloadData()
    }
}

// MARK: AlertPopupConfirmTitleViewDelegate
extension SmartScalesViewController: AlertPopupConfirmTitleViewDelegate {
    func presentAlertPopupConfirmTitleView() {
        let controller = AlertPopupConfirmTitleViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        controller.lblTitle = "Cập nhật thông tin"
        controller.lblContent = "Bạn cần cập nhật thêm thông tin chiều cao và cân nặng"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xác nhận"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirmCancelAction(object: Any?) { }
    
    func onAlertPopupConfirmAgreeAction(object: Any?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            guard let profile = self.viewModel.device?.profile else {
                return
            }
            
            let authModel = AuthModel()
            authModel.accessToken = profile.accessToken
            authModel.customer?.fullName = profile.fullName
            authModel.customer?.birthday = profile.birthday
            authModel.customer?.avatar = profile.avatar
            authModel.customer?.gender = profile.gender
            authModel.customer?.height = profile.height
            authModel.customer?.weight = profile.weight
            authModel.customer?.blood = profile.blood
            
            guard authModel.accessToken != nil else {
                return
            }
            
            self.router.presentUpdateSubProfileRouter(authModel: authModel, delegate: self)
        }
    }
}

// MARK: MeasureScalesViewDelegate
extension SmartScalesViewController: MeasureScalesViewDelegate {
    func setRefreshDataMeasureHistory() {
        self.viewModel.onRefreshAction()
    }
}

// MARK: UpdateSubProfilePresenterDelegate
extension SmartScalesViewController: UpdateSubProfileDelegate {
    func onUpdateProfileSuccess() {
        SKToast.shared.showToast(content: "Cập nhật hồ sơ thành công")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewModel.device?.profile = gRealm.objects().first(where: {$0.type == .SCALES})
        }
    }
    
    func onDeleteProfileSuccess() { }
}
