//
//  
//  SmartScalesViewModel.swift
//  1SK
//
//  Created by Thaad on 27/10/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol SmartScalesViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onRefreshAction()
    
    func isPairedDevice() -> Bool
    
    var device: DeviceRealm? { get set }
    var sliderBanner: SliderBannerModel? { get set }
    var consult: ConsultModel? { get set }
    var listPostSuggest: [PostHealthModel] { get set }
    var listBodyFat: [BodyFatModel] { get set }
}

// MARK: - SmartScales ViewModel
class SmartScalesViewModel {
    weak var view: SmartScalesViewProtocol?
    private var interactor: SmartScalesInteractorInputProtocol

    init(interactor: SmartScalesInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var isViewDidAppear = false

    var device: DeviceRealm?
    var sliderBanner: SliderBannerModel?
    var consult: ConsultModel?
    var listPostSuggest: [PostHealthModel] = []
    var listBodyFat: [BodyFatModel] = []
}

// MARK: - SmartScales ViewModelProtocol
extension SmartScalesViewModel: SmartScalesViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.getDataRecent()
        self.getSliderBanner()
    }
    
    func onViewDidAppear() {
        if self.isViewDidAppear == true {
            self.getDataRecent()
        }
        self.isViewDidAppear = true
    }
    
    func onRefreshAction() {
        self.getDataRecent()
    }
    
    func isPairedDevice() -> Bool {
        if self.device?.uuid != nil {
            return true
            
        } else {
            return false
        }
    }
}

// MARK: - SmartScales Handlers
extension SmartScalesViewModel {
    func getSliderBanner() {
        self.interactor.getSliderBanner(typeSlider: .Consult)
    }
    
    func getDataRecent() {
        guard self.device?.uuid != nil else {
            return
        }
        guard let userId = self.device?.profile?.id else {
            return
        }
        guard let accessToken = self.device?.profile?.accessToken else {
            return
        }
        self.interactor.getDataScalesMeasual(userId: userId, page: 1, perPage: 1, accessToken: accessToken)
    }
    
    func getInitDataScales() {
        guard self.device?.uuid != nil else {
            return
        }
        guard let bodyFat = self.listBodyFat.first, let bmiIndex = bodyFat.bodyFatSDK?.bmiIndex else {
            return
        }
        self.interactor.getPostsScales(tag: bmiIndex.status_VN ?? "", index: bmiIndex.code ?? "")
        
        guard let status = bmiIndex.status_VN else {
            return
        }
        guard let weight = self.listBodyFat.first?.weight else {
            return
        }
        guard let gender = self.device?.profile?.gender else {
            return
        }
        guard let birthday = self.device?.profile?.birthday?.toDate(.dmySlash) else {
            return
        }
        let age = Date().year - birthday.year
        self.interactor.getConsultIndex(index: "BMI", status: status, gender: gender, age: age, weight: weight)
    }
}

// MARK: - SmartScales InteractorOutputProtocol
extension SmartScalesViewModel: SmartScalesInteractorOutputProtocol {
    func onGetPostsScalesFinished(with result: Result<[PostHealthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listPostSuggest = model
            self.view?.onReloadData()
            
        case .failure:
            break
        }
    }
    
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>) {
        switch result {
        case .success(let model):
            self.sliderBanner = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetDataScalesMeasualFinished(with result: Result<[BodyFatModel], APIError>) {
        switch result {
        case .success(let listModel):
            listModel.forEach { model in
                model.setBodyFatSDK(profile: self.device?.profile)
            }
            self.listBodyFat = listModel
            self.view?.setDataBodyFat()
            
            self.getInitDataScales()
            
        case .failure:
            break
        }
    }
    
    func onGetConsultIndexFinished(with result: Result<ConsultModel, APIError>) {
        switch result {
        case .success(let model):
            self.consult = model
            self.view?.setDataConsult()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
