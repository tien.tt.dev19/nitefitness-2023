//
//  
//  ActivitySmartWatchInteractorInput.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ActivitySmartWatchInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int)
    func set_DataNameFunction(param1: String, param2: Int)
}

// MARK: - Interactor Output Protocol
protocol ActivitySmartWatchInteractorOutputProtocol: AnyObject {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - ActivitySmartWatch InteractorInput
class ActivitySmartWatchInteractorInput {
    weak var output: ActivitySmartWatchInteractorOutputProtocol?
}

// MARK: - ActivitySmartWatch InteractorInputProtocol
extension ActivitySmartWatchInteractorInput: ActivitySmartWatchInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
    
    func set_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
}
