//
//  
//  ActivitySmartWatchRouter.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ActivitySmartWatchRouterProtocol {

}

// MARK: - ActivitySmartWatch Router
class ActivitySmartWatchRouter {
    weak var viewController: ActivitySmartWatchViewController?
    
    static func setupModule() -> ActivitySmartWatchViewController {
        let viewController = ActivitySmartWatchViewController()
        let router = ActivitySmartWatchRouter()
        let interactorInput = ActivitySmartWatchInteractorInput()
        let viewModel = ActivitySmartWatchViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ActivitySmartWatch RouterProtocol
extension ActivitySmartWatchRouter: ActivitySmartWatchRouterProtocol {
    
}
