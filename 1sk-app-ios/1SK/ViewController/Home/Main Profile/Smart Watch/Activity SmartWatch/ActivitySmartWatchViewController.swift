//
//  
//  ActivitySmartWatchViewController.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ActivitySmartWatchViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - ActivitySmartWatch ViewController
class ActivitySmartWatchViewController: BaseViewController {
    var router: ActivitySmartWatchRouterProtocol!
    var viewModel: ActivitySmartWatchViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    private var view_Header: HeaderTableViewActivitySmartWatch!
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
        self.setInitUITableView()
    }
    
    private func setInitLayout() {
        self.title = "Vận động"
    }
    
    // MARK: - Action
    
}

// MARK: - ActivitySmartWatch ViewProtocol
extension ActivitySmartWatchViewController: ActivitySmartWatchViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - UITableViewDataSource
extension ActivitySmartWatchViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewActivitySmartWatch.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionViewActivitySmartWatch.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.setInitHeaderTableView()
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderSectionViewActivitySmartWatch.self)
        
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10 //self.viewModel.listData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewActivitySmartWatch.self, for: indexPath)
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ActivitySmartWatchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            //self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: HeaderTableViewActivitySmartWatchDelegate
extension ActivitySmartWatchViewController: HeaderTableViewActivitySmartWatchDelegate {
    func setInitHeaderTableView() {
        self.view_Header = HeaderTableViewActivitySmartWatch.loadFromNib()
        self.view_Header?.delegate = self
        
        // 1. Set table header view programmatically
        self.tbv_TableView.setTableHeaderView(headerView: self.view_Header)

        // 2. Set initial frame
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    // View size is changed (e.g., device is rotated.)
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        self.tbv_TableView.updateFrameHeaderView()
    }
    
    func setStateUI(typeTime: TimeType) {
        self.tbv_TableView.updateFrameHeaderView()
        //self.viewModel.typeTime = typeTime
    }
    
    func onPreAction(typeTime: TimeType) {
        //self.viewModel.onPreAction(typeTime: typeTime)
    }
    
    func onNextAction(typeTime: TimeType) {
        //self.viewModel.onNextAction(typeTime: typeTime)
    }
}
