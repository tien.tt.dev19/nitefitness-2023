//
//  
//  ActivitySmartWatchViewModel.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ActivitySmartWatchViewModelProtocol {
    func onViewDidLoad()
    
    var listData: [Int] { get set }
}

// MARK: - ActivitySmartWatch ViewModel
class ActivitySmartWatchViewModel {
    weak var view: ActivitySmartWatchViewProtocol?
    private var interactor: ActivitySmartWatchInteractorInputProtocol

    init(interactor: ActivitySmartWatchInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listData: [Int] = []
}

// MARK: - ActivitySmartWatch ViewModelProtocol
extension ActivitySmartWatchViewModel: ActivitySmartWatchViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - ActivitySmartWatch InteractorOutputProtocol
extension ActivitySmartWatchViewModel: ActivitySmartWatchInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
