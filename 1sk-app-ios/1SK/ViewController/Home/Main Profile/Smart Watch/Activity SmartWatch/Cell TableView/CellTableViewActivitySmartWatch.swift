//
//  CellTableViewActivitySmartWatch.swift
//  1SK
//
//  Created by Thaad on 04/11/2022.
//

import UIKit

class CellTableViewActivitySmartWatch: UITableViewCell {

    @IBOutlet weak var view_Content: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.view_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
