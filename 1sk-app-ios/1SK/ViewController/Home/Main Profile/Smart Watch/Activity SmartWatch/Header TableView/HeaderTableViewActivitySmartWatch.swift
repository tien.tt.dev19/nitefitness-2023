//
//  HeaderTableViewActivitySmartWatch.swift
//  1SK
//
//  Created by Thaad on 04/11/2022.
//

import UIKit
import MBCircularProgressBar

protocol HeaderTableViewActivitySmartWatchDelegate: AnyObject {
    func setStateUI(typeTime: TimeType)
    func onPreAction(typeTime: TimeType)
    func onNextAction(typeTime: TimeType)
}

class HeaderTableViewActivitySmartWatch: UIView {
    @IBOutlet weak var view_Progress: MBCircularProgressBarView!
    
    @IBOutlet weak var btn_Day: UIButton!
    @IBOutlet weak var btn_Week: UIButton!
    @IBOutlet weak var btn_Month: UIButton!
    @IBOutlet weak var btn_Year: UIButton!
    
    @IBOutlet weak var view_ContentDay: UIView!
    @IBOutlet weak var view_ContentOther: UIView!
    
    @IBOutlet weak var view_Activity: UIView!
    @IBOutlet weak var view_ChartDay: UIView!
    @IBOutlet weak var view_ChartOther: UIView!
    
    weak var delegate: HeaderTableViewActivitySmartWatchDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitLayout()
        self.setStateUI(typeTime: .day)
        
        
    }
    
    private func setInitLayout() {
        self.view_Activity.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_ChartDay.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_ChartOther.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        
        self.view_Progress.value = 30
        self.view_Progress.maxValue = 100
        self.view_Progress.layoutSubviews()
    }

    @IBAction func onDayAction(_ sender: Any) {
        self.setStateUI(typeTime: .day)
    }
    
    @IBAction func onWeekAction(_ sender: Any) {
        self.setStateUI(typeTime: .week)
    }
    
    @IBAction func onMonthAction(_ sender: Any) {
        self.setStateUI(typeTime: .month)
    }
    
    @IBAction func onYearAction(_ sender: Any) {
        self.setStateUI(typeTime: .year)
    }
}

// MARK: State Menu
extension HeaderTableViewActivitySmartWatch {
    private func setStateUI(typeTime: TimeType) {
        switch typeTime {
        case .day:
            self.btn_Day.backgroundColor = R.color.subTitle()
            self.btn_Day.setTitleColor(.white, for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.view_ContentDay.isHidden = false
            self.view_ContentOther.isHidden = true
            
        case .week:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Week.backgroundColor = R.color.subTitle()
            self.btn_Week.setTitleColor(.white, for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.view_ContentDay.isHidden = true
            self.view_ContentOther.isHidden = false
            
        case .month:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.subTitle()
            self.btn_Month.setTitleColor(.white, for: .normal)
            
            self.btn_Year.backgroundColor = R.color.line()
            self.btn_Year.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.view_ContentDay.isHidden = true
            self.view_ContentOther.isHidden = false
            
        case .year:
            self.btn_Day.backgroundColor = R.color.line()
            self.btn_Day.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Week.backgroundColor = R.color.line()
            self.btn_Week.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Month.backgroundColor = R.color.line()
            self.btn_Month.setTitleColor(R.color.subTitle(), for: .normal)
            
            self.btn_Year.backgroundColor = R.color.subTitle()
            self.btn_Year.setTitleColor(.white, for: .normal)
            
            self.view_ContentDay.isHidden = true
            self.view_ContentOther.isHidden = false
        }
        
        self.delegate?.setStateUI(typeTime: typeTime)
    }
}
