//
//  
//  SmartWatchInteractorInput.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol SmartWatchInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int)
    func set_DataNameFunction(param1: String, param2: Int)
}

// MARK: - Interactor Output Protocol
protocol SmartWatchInteractorOutputProtocol: AnyObject {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - SmartWatch InteractorInput
class SmartWatchInteractorInput {
    weak var output: SmartWatchInteractorOutputProtocol?
}

// MARK: - SmartWatch InteractorInputProtocol
extension SmartWatchInteractorInput: SmartWatchInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
    
    func set_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
}
