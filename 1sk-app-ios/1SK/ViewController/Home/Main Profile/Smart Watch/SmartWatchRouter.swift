//
//  
//  SmartWatchRouter.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol SmartWatchRouterProtocol {
    func showActivitySmartWatchRouter()
    func showStepGoalsSmartWatchRouter()
}

// MARK: - SmartWatch Router
class SmartWatchRouter {
    weak var viewController: SmartWatchViewController?
    
    static func setupModule(device: DeviceRealm?) -> SmartWatchViewController {
        let viewController = SmartWatchViewController()
        let router = SmartWatchRouter()
        let interactorInput = SmartWatchInteractorInput()
        let viewModel = SmartWatchViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.device = device
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - SmartWatch RouterProtocol
extension SmartWatchRouter: SmartWatchRouterProtocol {
    func showActivitySmartWatchRouter() {
        let controller = ActivitySmartWatchRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showStepGoalsSmartWatchRouter() {
        let controller = StepGoalsSmartWatchRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
