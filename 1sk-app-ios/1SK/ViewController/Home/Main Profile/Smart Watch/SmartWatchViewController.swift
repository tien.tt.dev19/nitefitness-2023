//
//  
//  SmartWatchViewController.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit
import MBCircularProgressBar

// MARK: - ViewProtocol
protocol SmartWatchViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - SmartWatch ViewController
class SmartWatchViewController: BaseViewController {
    var router: SmartWatchRouterProtocol!
    var viewModel: SmartWatchViewModelProtocol!
    
    @IBOutlet weak var view_Watch_Content: UIView!
    @IBOutlet weak var view_Progress: MBCircularProgressBarView!
    
    @IBOutlet weak var view_Option: UIStackView!
    @IBOutlet weak var lbl_Option: UILabel!
    @IBOutlet weak var img_Option: UIImageView!
    @IBOutlet weak var view_ActivityChart: UIStackView!
    
    @IBOutlet weak var view_Hr_Activity: UIView!
    @IBOutlet weak var view_Hr_Content: UIView!
    @IBOutlet weak var lbl_Hr_Time: UILabel!
    
    @IBOutlet weak var view_Oxy_Activity: UIView!
    @IBOutlet weak var view_Oxy_Content: UIView!
    @IBOutlet weak var lbl_Oxy_Time: UILabel!
    
    
    @IBOutlet weak var view_Bp_Activity: UIView!
    @IBOutlet weak var view_Bp_Content: UIView!
    @IBOutlet weak var lbl_Bp_Time: UILabel!
    
    
    @IBOutlet weak var view_Temp_Activity: UIView!
    @IBOutlet weak var view_Temp_Content: UIView!
    @IBOutlet weak var lbl_Temp_Time: UILabel!
    
    
    @IBOutlet weak var view_Stress_Activity: UIView!
    @IBOutlet weak var view_Stress_Content: UIView!
    @IBOutlet weak var lbl_Stress_Time: UILabel!
    
    
    @IBOutlet weak var view_Sleep_Activity: UIView!
    @IBOutlet weak var view_Sleep_Content: UIView!
    @IBOutlet weak var lbl_Sleep_Time: UILabel!
    
    
    @IBOutlet weak var view_Week_Activity: UIView!
    @IBOutlet weak var view_Week_Content: UIView!
    @IBOutlet weak var tbv_History: UITableView!
    @IBOutlet weak var constraint_height_TableView: NSLayoutConstraint!
    
    var isCollapse = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayoutUI()
        self.setInitUITableView()
        self.setInitUIGestureRecognizer()
        
        
        self.view_Progress.value = 30
        self.view_Progress.maxValue = 100
        self.view_Progress.layoutSubviews()
        
    }
    
    private func setInitLayoutUI() {
        self.view_Watch_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        
        self.view_Hr_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_Oxy_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_Bp_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_Temp_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_Stress_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        self.view_Sleep_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
        
        self.view_Week_Content.addShadow(width: 0, height: 2, color: .black, radius: 4, opacity: 0.1)
    }
    
    // MARK: - Action
    @IBAction func onActivityDetailAction(_ sender: Any) {
        self.router.showActivitySmartWatchRouter()
    }
    
    @IBAction func onHeartRateDetailAction(_ sender: Any) {
        self.router.showActivitySmartWatchRouter()
    }
    
}

// MARK: - SmartWatch ViewProtocol
extension SmartWatchViewController: SmartWatchViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - UIGestureRecognizer
extension SmartWatchViewController {
    private func setInitUIGestureRecognizer() {
        self.view_Option.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onOptionViewAction)))
        self.view_Progress.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onProgressViewAction)))
    }
    
    @objc func onOptionViewAction() {
        self.isCollapse = !self.isCollapse
        self.view_ActivityChart.isHidden = self.isCollapse
        
        if self.isCollapse {
            self.lbl_Option.text = "Mở rộng"
            self.img_Option.image = R.image.ic_expand_watch()
            
        } else {
            self.lbl_Option.text = "Thu gọn"
            self.img_Option.image = R.image.ic_collap_watch()
        }
    }
    
    @objc func onProgressViewAction() {
        self.router.showStepGoalsSmartWatchRouter()
    }
}

// MARK: - UITableViewDataSource
extension SmartWatchViewController: UITableViewDataSource, UITableViewDelegate {
    private func setInitUITableView() {
        self.tbv_History.registerNib(ofType: CellHistorySmartWatch.self)
        self.tbv_History.dataSource = self
        self.tbv_History.delegate = self
        
        self.constraint_height_TableView.constant = 10 * 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellHistorySmartWatch.self, for: indexPath)
        
        return cell
    }
}
