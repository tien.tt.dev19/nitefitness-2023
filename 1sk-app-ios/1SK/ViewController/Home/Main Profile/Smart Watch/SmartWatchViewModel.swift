//
//  
//  SmartWatchViewModel.swift
//  1SK
//
//  Created by Thaad on 15/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol SmartWatchViewModelProtocol {
    func onViewDidLoad()
    
    var device: DeviceRealm? { get set }
}

// MARK: - SmartWatch ViewModel
class SmartWatchViewModel {
    weak var view: SmartWatchViewProtocol?
    private var interactor: SmartWatchInteractorInputProtocol

    init(interactor: SmartWatchInteractorInputProtocol) {
        self.interactor = interactor
    }

    var device: DeviceRealm?
}

// MARK: - SmartWatch ViewModelProtocol
extension SmartWatchViewModel: SmartWatchViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - SmartWatch InteractorOutputProtocol
extension SmartWatchViewModel: SmartWatchInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
