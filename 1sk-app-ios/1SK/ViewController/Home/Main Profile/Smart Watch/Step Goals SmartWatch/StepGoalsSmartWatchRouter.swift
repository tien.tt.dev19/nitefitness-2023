//
//  
//  StepGoalsSmartWatchRouter.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol StepGoalsSmartWatchRouterProtocol {

}

// MARK: - StepGoalsSmartWatch Router
class StepGoalsSmartWatchRouter {
    weak var viewController: StepGoalsSmartWatchViewController?
    
    static func setupModule() -> StepGoalsSmartWatchViewController {
        let viewController = StepGoalsSmartWatchViewController()
        let router = StepGoalsSmartWatchRouter()
        let interactorInput = StepGoalsSmartWatchInteractorInput()
        let viewModel = StepGoalsSmartWatchViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - StepGoalsSmartWatch RouterProtocol
extension StepGoalsSmartWatchRouter: StepGoalsSmartWatchRouterProtocol {
    
}
