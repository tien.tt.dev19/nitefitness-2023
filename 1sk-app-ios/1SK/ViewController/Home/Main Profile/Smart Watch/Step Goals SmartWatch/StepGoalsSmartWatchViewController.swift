//
//  
//  StepGoalsSmartWatchViewController.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol StepGoalsSmartWatchViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - StepGoalsSmartWatch ViewController
class StepGoalsSmartWatchViewController: BaseViewController {
    var router: StepGoalsSmartWatchRouterProtocol!
    var viewModel: StepGoalsSmartWatchViewModelProtocol!
    
    @IBOutlet weak var lbl_StepGoal: UILabel!
    @IBOutlet weak var sld_Slider: UISlider!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Mục tiêu"
        self.setInitUISlider()
    }
    
    // MARK: - Action
    
}

// MARK: - StepGoalsSmartWatch ViewProtocol
extension StepGoalsSmartWatchViewController: StepGoalsSmartWatchViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - UIGestureRecognizer
extension StepGoalsSmartWatchViewController {
    private func setInitUISlider() {
        self.sld_Slider.minimumValue = 100
        self.sld_Slider.maximumValue = 10000
        
        self.sld_Slider.setThumbImage(R.image.ic_slider_thumb(), for: .normal)
        self.sld_Slider.setThumbImage(R.image.ic_slider_thumb(), for: .highlighted)
        
        self.sld_Slider.addTarget(self, action: #selector(self.onSliderValueChanged(slider:event:)), for: .valueChanged)
        self.sld_Slider.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSliderDidTapAction(gestureRecognizer:))))
    }
    
    @objc func onSliderValueChanged(slider: UISlider, event: UIEvent) {
        self.lbl_StepGoal.text = "\(Int(slider.value))"
        
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                // handle drag began
                print("onSlider onSliderValChanged began")
                
            case .moved:
                // handle drag moved
                print("onSlider onSliderValChanged moved")
                
            case .ended:
                // handle drag ended
                print("onSlider onSliderValChanged ended")
                
            default:
                break
            }
        }
    }
    
    @objc func onSliderDidTapAction(gestureRecognizer: UIGestureRecognizer) {
        print("onSlider onSliderDidTapAction")
        
        let pointTapped: CGPoint = gestureRecognizer.location(in: self.sld_Slider)
        let positionOfSlider: CGPoint = self.sld_Slider.frame.origin
        let widthOfSlider: CGFloat = self.sld_Slider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(self.sld_Slider.maximumValue) / widthOfSlider)

        self.sld_Slider.setValue(Float(newValue), animated: true)
        self.lbl_StepGoal.text = "\(Int(newValue))"
    }
}
