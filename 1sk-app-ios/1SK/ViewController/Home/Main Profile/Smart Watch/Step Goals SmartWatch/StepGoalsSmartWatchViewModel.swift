//
//  
//  StepGoalsSmartWatchViewModel.swift
//  1SK
//
//  Created by Thaad on 03/11/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol StepGoalsSmartWatchViewModelProtocol {
    func onViewDidLoad()
    
    //var variable: Int? { get set }
    //var listVariable: [Int] { get set }
}

// MARK: - StepGoalsSmartWatch ViewModel
class StepGoalsSmartWatchViewModel {
    weak var view: StepGoalsSmartWatchViewProtocol?
    private var interactor: StepGoalsSmartWatchInteractorInputProtocol

    init(interactor: StepGoalsSmartWatchInteractorInputProtocol) {
        self.interactor = interactor
    }

    //var variable: Int?
    //var listVariable: [Int] = []
}

// MARK: - StepGoalsSmartWatch ViewModelProtocol
extension StepGoalsSmartWatchViewModel: StepGoalsSmartWatchViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - StepGoalsSmartWatch InteractorOutputProtocol
extension StepGoalsSmartWatchViewModel: StepGoalsSmartWatchInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
