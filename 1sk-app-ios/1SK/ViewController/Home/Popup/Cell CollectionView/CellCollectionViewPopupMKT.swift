//
//  CellCollectionViewPopupMKT.swift
//  1SK
//
//  Created by Thaad on 10/02/2022.
//

import UIKit

protocol CellCollectionViewPopupMKTDelegate: AnyObject {
    func onClosePopupAction(popup: PopupModel)
    func onSelectPopupAction(popup: PopupModel)
}

class CellCollectionViewPopupMKT: UICollectionViewCell {
    
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var img_Image: UIImageView!
    
    weak var delegate: CellCollectionViewPopupMKTDelegate?
    var popup: PopupModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitGestureRecognizer()
    }
    
    func config(popup: PopupModel) {
        self.popup = popup
        
        guard let `popup` = self.popup else {
            return
        }
        guard let imageUrl = popup.imageUrl else {
            return
        }
        self.img_Image.setImageWith(imageUrl: imageUrl, placeHolder: R.image.img_place_holder())
    }
    
    @IBAction func onCloseAction(_ sender: Any) {
        self.delegate?.onClosePopupAction(popup: self.popup!)
    }
    
}

// MARK: GestureRecognizer
extension CellCollectionViewPopupMKT {
    func setInitGestureRecognizer() {
        self.img_Image.isUserInteractionEnabled = true
        self.img_Image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onSelectAction)))
    }
    
    @objc func onSelectAction() {
        self.delegate?.onSelectPopupAction(popup: self.popup!)
    }
}
