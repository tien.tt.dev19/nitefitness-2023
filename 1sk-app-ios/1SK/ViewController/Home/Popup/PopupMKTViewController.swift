//
//  PopupMKTViewController.swift
//  1SK
//
//  Created by Thaad on 10/02/2022.
//

import UIKit

protocol PopupMKTViewDelegate: AnyObject {
    func onSelectPopupAction(popup: PopupModel)
}

class PopupMKTViewController: UIViewController {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    
    @IBOutlet weak var page_Control: UIPageControl!
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    weak var delegate: PopupMKTViewDelegate?
    var listPopup: [PopupModel] = []
    
    let marketingService = MarketingService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitUICollectionView()
        self.setInitUIPageControl()
        //self.setInitGestureRecognizer()
        
        self.setConfig()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    private func setConfig() {
        gIsShowMarketingPopup = true
        
        self.coll_CollectionView.reloadData()
        self.setDataPageControl()
        self.setReadedPopup()
    }
    
    @IBAction func onClosePopupAction(_ sender: Any) {
        self.hiddenAnimate()
    }
    
    func setReadedPopup() {
        self.marketingService.setPopupReadByListId(listPopup: self.listPopup) { result in
            print("setReadData")
        }
    }
}

// MARK: UIPageControl
extension PopupMKTViewController {
    func setInitUIPageControl() {
        self.page_Control.hidesForSinglePage = true
    }
    
    func setDataPageControl() {
        self.page_Control.numberOfPages = self.listPopup.count
    }
}

// MARK: GestureRecognizer
extension PopupMKTViewController {
    func setInitGestureRecognizer() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onViewBgAction)))
    }
    
    @objc func onViewBgAction() {
        self.hiddenAnimate()
    }
}

// MARK: - Init Animation
extension PopupMKTViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
            
        }, completion: { (finished) -> Void in
            //
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            gIsShowMarketingPopup = false
            self.dismiss(animated: false, completion: nil)
        })
    }
    
//    func showInView(_ aView: UIViewController, animated: Bool) {
//        aView.view.addSubview(self.view)
//        aView.addChild(self)
//
//
//
//        if animated == true {
//            self.showAnimate()
//        }
//    }
    
//    func showAnimate() {
//        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//        self.view.alpha = 0
//        UIView.animate(withDuration: 0.25, animations: { () -> Void in
//            self.view.alpha = 1
//            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
//        })
//    }
//
//    func removeAnimate() {
//        UIView.animate(withDuration: 0.25, animations: { () -> Void in
//            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
//            self.view.alpha = 0
//
//        }, completion: { (finished) -> Void in
//            if finished == true {
//                gIsShowMarketingPopup = false
//                self.view.removeFromSuperview()
//
//                if (self.tabBarController?.viewControllers?.last as? PopupMKTViewController) != nil {
//                    self.tabBarController?.viewControllers?.removeLast()
//                }
//            }
//        })
//    }
}

// MARK: UICollectionViewDataSource
extension PopupMKTViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewPopupMKT.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listPopup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewPopupMKT.self, for: indexPath)
        cell.delegate = self
        cell.config(popup: self.listPopup[indexPath.row])
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension PopupMKTViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.coll_CollectionView.frame.width, height: self.coll_CollectionView.frame.height)
    }
}

// MARK: - UIScrollViewDelegate
extension PopupMKTViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let OFFSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2
        self.page_Control.currentPage = Int(OFFSet + horizontalCenter) / Int(width)
    }
}

// MARK: CellCollectionViewPopupMKTDelegate
extension PopupMKTViewController: CellCollectionViewPopupMKTDelegate {
    func onClosePopupAction(popup: PopupModel) {
        self.hiddenAnimate()
    }
    
    func onSelectPopupAction(popup: PopupModel) {
        self.delegate?.onSelectPopupAction(popup: popup)
        self.hiddenAnimate()
    }
}
