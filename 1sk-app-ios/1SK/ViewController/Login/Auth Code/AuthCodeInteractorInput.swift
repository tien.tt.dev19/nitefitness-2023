//
//  
//  AuthCodeInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol AuthCodeInteractorInputProtocol {
    func setVerifyCodeOtp(phone: String, codeOtp: String, state: StateAuthPhone)
    func setVerifyPhoneNumber(phone: String)
}

// MARK: - Interactor Output Protocol
protocol AuthCodeInteractorOutputProtocol: AnyObject {
    func onSetVerifyCodeOtpFinished(with result: Result<EmptyModel, APIError>, phone: String, code: String)
    func onSetVerifyPhoneNumberFinished(with result: Result<EmptyModel, APIError>, phone: String)
}

// MARK: - AuthCode InteractorInput
class AuthCodeInteractorInput {
    weak var output: AuthCodeInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - AuthCode InteractorInputProtocol
extension AuthCodeInteractorInput: AuthCodeInteractorInputProtocol {
    func setVerifyCodeOtp(phone: String, codeOtp: String, state: StateAuthPhone) {
        self.authService?.setVerifyCodeOtp(phone: phone, codeOtp: codeOtp, state: state, completion: { [weak self] (result) in
            self?.output?.onSetVerifyCodeOtpFinished(with: result.unwrapSuccessModel(), phone: phone, code: codeOtp)
        })
    }
    
    func setVerifyPhoneNumber(phone: String) {
        self.authService?.setVerifyPhoneNumber(phone: phone) { [weak self] (result) in
            self?.output?.onSetVerifyPhoneNumberFinished(with: result.unwrapSuccessModel(), phone: phone)
        }
    }
}
