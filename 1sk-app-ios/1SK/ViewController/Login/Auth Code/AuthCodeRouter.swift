//
//  
//  AuthCodeRouter.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol AuthCodeRouterProtocol {
    
}

// MARK: - AuthCode Router
class AuthCodeRouter {
    weak var viewController: AuthCodeViewController?
    
    static func setupModule(delegate: AuthCodeViewDelegate?) -> AuthCodeViewController {
        let viewController = AuthCodeViewController()
        let router = AuthCodeRouter()
        let interactorInput = AuthCodeInteractorInput()
        let viewModel = AuthCodeViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - AuthCode RouterProtocol
extension AuthCodeRouter: AuthCodeRouterProtocol {
    
}
