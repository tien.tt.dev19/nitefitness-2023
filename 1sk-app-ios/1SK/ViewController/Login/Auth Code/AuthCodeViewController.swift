//
//  
//  AuthCodeViewController.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit
import IQKeyboardManagerSwift

protocol AuthCodeViewDelegate: AnyObject {
    func onAuthCodeViewDismiss()
    func onAuthCodeSuccess(phoneNumber: String?, codeOtp: String?)
}

// MARK: - ViewProtocol
protocol AuthCodeViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onAuthCodeSuccess()
    func setErrorUI(message: String)
    func setResendSuccess()
    func setDataPhoneNumber()
}

// MARK: - AuthCode ViewController
class AuthCodeViewController: BaseViewController {
    var router: AuthCodeRouterProtocol!
    var viewModel: AuthCodeViewModelProtocol!
    weak var delegate: AuthCodeViewDelegate?
    
    @IBOutlet weak var tf_Code_1: TextFieldBackward!
    @IBOutlet weak var tf_Code_2: TextFieldBackward!
    @IBOutlet weak var tf_Code_3: TextFieldBackward!
    @IBOutlet weak var tf_Code_4: TextFieldBackward!
    @IBOutlet weak var tf_Code_5: TextFieldBackward!
    
    @IBOutlet weak var lbl_Note: UILabel!
    @IBOutlet weak var lbl_Error: UILabel!
    @IBOutlet weak var btn_Confirm: UIButton!
    
    @IBOutlet weak var lbl_Countdown: UILabel!
    
    @IBOutlet weak var stack_Countdown: UIStackView!
    @IBOutlet weak var stack_ResendCode: UIStackView!
    
    private var timeCount: Int = 0
    private var timer: Timer?
    
    private var isDidLoad = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tf_Code_1.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    // MARK: - Init
    private func setupInit() {
        self.isDidLoad = true
        self.setDataPhoneNumber()
        self.setInitUITextField()
        self.setStateConfirmButton(isSelected: false)
        self.setInitTimerCountdown()
    }
    
    func setDataPhoneNumber() {
        if self.isDidLoad {
            self.lbl_Note.text = "Vui lòng nhập mã gồm 5 chữ số vừa được gửi đến \(self.viewModel.phoneNumber ?? "null")"
        }
    }
    
    func setStateConfirmButton(isSelected: Bool) {
        self.btn_Confirm.isSelected = isSelected
        if isSelected {
            self.btn_Confirm.backgroundColor = R.color.mainColor()
        } else {
            self.btn_Confirm.backgroundColor = R.color.mainDeselect()
        }
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.view.endEditing(true)
        self.delegate?.onAuthCodeViewDismiss()
    }
    
    @IBAction func onConfirmAction(_ sender: Any) {
        guard self.btn_Confirm.isSelected else {
            //SKToast.shared.showToast(content: "Vui lòng nhập mã xác thực để tiếp tục")
            return
        }
        let code: String = String(format: "%@%@%@%@%@",
                           self.tf_Code_1.text ?? "",
                           self.tf_Code_2.text ?? "",
                           self.tf_Code_3.text ?? "",
                           self.tf_Code_4.text ?? "",
                           self.tf_Code_5.text ?? "")
        
        guard code.count == 5 else {
            SKToast.shared.showToast(content: "Vui lòng nhập mã xác thực để tiếp tục")
            return
        }
        self.view.endEditing(true)
        self.viewModel.onConfirmOtpCode(code: code)
    }
    
    @IBAction func onResendCodeAction(_ sender: Any) {
        self.tf_Code_1.text = ""
        self.tf_Code_2.text = ""
        self.tf_Code_3.text = ""
        self.tf_Code_4.text = ""
        self.tf_Code_5.text = ""
        
        self.lbl_Error.isHidden = true
        self.tf_Code_1.becomeFirstResponder()
        
        self.viewModel.onResendCodeAction()
    }
}

// MARK: - AuthCode ViewProtocol
extension AuthCodeViewController: AuthCodeViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onAuthCodeSuccess() {
        self.delegate?.onAuthCodeSuccess(phoneNumber: self.viewModel.phoneNumber, codeOtp: self.viewModel.codeOtp)
    }
    
    func setErrorUI(message: String) {
        self.lbl_Error.text = message
        self.lbl_Error.isHidden = false
    }
    
    func setResendSuccess() {
        self.setCountdownUI(isCount: true)
        self.setInitTimerCountdown()
    }
}

// MARK: Timer Countdown
extension AuthCodeViewController {
    func setInitTimerCountdown() {
        self.timeCount = 60
        self.setCountdownUI(isCount: true)
        
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.onTimerChangeValue), userInfo: nil, repeats: true)
    }
    
    @objc func onTimerChangeValue() {
        if self.timeCount > 0 {
            self.timeCount -= 1
            self.lbl_Countdown.text = "\(Int(self.timeCount))"
            
        } else {
            self.lbl_Countdown.text = "0"
            self.timer?.invalidate()
            self.timer = nil
            
            self.setCountdownUI(isCount: false)
        }
    }
    
    func setCountdownUI(isCount: Bool) {
        if isCount {
            self.lbl_Countdown.text = self.timeCount.asString
            self.stack_Countdown.isHidden = false
            self.stack_ResendCode.isHidden = true
            
        } else {
            self.lbl_Countdown.text = "0"
            self.stack_Countdown.isHidden = true
            self.stack_ResendCode.isHidden = false
        }
    }
}

// MARK: UITextFieldDelegate
extension AuthCodeViewController: UITextFieldDelegate, TextFieldBackwardDelegate {
    func setInitUITextField() {
        self.tf_Code_1.delegate = self
        self.tf_Code_2.delegate = self
        self.tf_Code_3.delegate = self
        self.tf_Code_4.delegate = self
        self.tf_Code_5.delegate = self
        
        self.tf_Code_1.textContentType = .oneTimeCode
        self.tf_Code_2.textContentType = .oneTimeCode
        self.tf_Code_3.textContentType = .oneTimeCode
        self.tf_Code_4.textContentType = .oneTimeCode
        self.tf_Code_5.textContentType = .oneTimeCode
        
        self.tf_Code_1.delegateBackward = self
        self.tf_Code_2.delegateBackward = self
        self.tf_Code_3.delegateBackward = self
        self.tf_Code_4.delegateBackward = self
        self.tf_Code_5.delegateBackward = self
        
        self.tf_Code_1.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.tf_Code_2.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.tf_Code_3.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.tf_Code_4.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.tf_Code_5.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.tf_Code_5.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
    }
    
    @objc func textFieldDidChangeSelection() {
        self.lbl_Error.isHidden = true
        if self.tf_Code_1.text?.count ?? 0 == 1
            && self.tf_Code_2.text?.count ?? 0 == 1
            && self.tf_Code_3.text?.count ?? 0 == 1
            && self.tf_Code_4.text?.count ?? 0 == 1
            && self.tf_Code_5.text?.count ?? 0 == 1 {
            self.setStateConfirmButton(isSelected: true)

        } else {
            self.setStateConfirmButton(isSelected: false)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case self.tf_Code_1:
            self.tf_Code_1.text = ""
            
        case self.tf_Code_2:
            self.tf_Code_2.text = ""
            
        case self.tf_Code_3:
            self.tf_Code_3.text = ""
            
        case self.tf_Code_4:
            self.tf_Code_4.text = ""
            
        case self.tf_Code_5:
            self.tf_Code_5.text = ""
            
        default:
            break
        }
        self.textFieldDidChangeSelection()
        return true
    }
    
    func textFieldDidDelete(textField: UITextField) {
        switch textField {
        case self.tf_Code_1:
            self.tf_Code_1.text = ""
            
        case self.tf_Code_2:
            self.tf_Code_2.text = ""
            self.tf_Code_1.becomeFirstResponder()
            
        case self.tf_Code_3:
            self.tf_Code_3.text = ""
            self.tf_Code_2.becomeFirstResponder()
            
        case self.tf_Code_4:
            self.tf_Code_4.text = ""
            self.tf_Code_3.becomeFirstResponder()
            
        case self.tf_Code_5:
            self.tf_Code_5.text = ""
            self.tf_Code_4.becomeFirstResponder()
            
        default:
            break
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !string.isBackSpace {
            switch textField {
            case self.tf_Code_1:
                guard textField.text?.count ?? 0 == 0 else { return false }
                self.tf_Code_1.text = string
                self.tf_Code_2.becomeFirstResponder()
                
            case self.tf_Code_2:
                guard textField.text?.count ?? 0 == 0 else { return false }
                self.tf_Code_2.text = string
                self.tf_Code_3.becomeFirstResponder()
                
            case self.tf_Code_3:
                guard textField.text?.count ?? 0 == 0 else { return false }
                self.tf_Code_3.text = string
                self.tf_Code_4.becomeFirstResponder()
                
            case self.tf_Code_4:
                guard textField.text?.count ?? 0 == 0 else { return false }
                self.tf_Code_4.text = string
                self.tf_Code_5.becomeFirstResponder()
                
            case self.tf_Code_5:
                guard textField.text?.count ?? 0 == 0 else { return false }
                
            default:
                break
            }
        }
        
        return true
    }
}
