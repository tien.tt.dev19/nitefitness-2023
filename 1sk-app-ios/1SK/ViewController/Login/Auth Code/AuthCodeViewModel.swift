//
//  
//  AuthCodeViewModel.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol AuthCodeViewModelProtocol {
    func onViewDidLoad()
    func onConfirmOtpCode(code: String)
    func onResendCodeAction()
    
    var stateAuthPhone: StateAuthPhone { get set }
    var phoneNumber: String? {get set}
    var codeOtp: String? {get set}
}

// MARK: - AuthCode ViewModel
class AuthCodeViewModel {
    weak var view: AuthCodeViewProtocol?
    private var interactor: AuthCodeInteractorInputProtocol

    init(interactor: AuthCodeInteractorInputProtocol) {
        self.interactor = interactor
    }

    var stateAuthPhone: StateAuthPhone = .None
    var codeOtp: String?
    var phoneNumber: String? {
        didSet {
            self.view?.setDataPhoneNumber()
        }
    }
}

// MARK: - AuthCode ViewModelProtocol
extension AuthCodeViewModel: AuthCodeViewModelProtocol {
    func onViewDidLoad() {
        //
    }
    
    func onConfirmOtpCode(code: String) {
        guard let phone = self.phoneNumber else {
            return
        }
        self.view?.showHud()
        self.interactor.setVerifyCodeOtp(phone: phone, codeOtp: code, state: self.stateAuthPhone)
    }
    
    func onResendCodeAction() {
        guard let phone = self.phoneNumber else {
            return
        }
        self.view?.showHud()
        self.interactor.setVerifyPhoneNumber(phone: phone)
    }
}

// MARK: - AuthCode InteractorOutputProtocol
extension AuthCodeViewModel: AuthCodeInteractorOutputProtocol {
    func onSetVerifyCodeOtpFinished(with result: Result<EmptyModel, APIError>, phone: String, code: String) {
        switch result {
        case .success:
            gUser?.phoneNumber = phone
            gUser?.mobileVerifiedAt = Date().toString()
            
            self.phoneNumber = phone
            self.codeOtp = code
            self.view?.onAuthCodeSuccess()
            
        case .failure(let error):
            self.view?.setErrorUI(message: error.message)
        }
        self.view?.hideHud()
    }
    
    func onSetVerifyPhoneNumberFinished(with result: Result<EmptyModel, APIError>, phone: String) {
        switch result {
        case .success:
            self.view?.setResendSuccess()
            
        case .failure(let error):
            self.view?.setErrorUI(message: error.message)
        }
        self.view?.hideHud()
    }
}
