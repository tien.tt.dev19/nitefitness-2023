//
//  
//  AuthPhoneInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol AuthPhoneInteractorInputProtocol {
    func setValidatePhoneNumber(phone: String)
    func setVerifyPhoneNumber(phone: String)
}

// MARK: - Interactor Output Protocol
protocol AuthPhoneInteractorOutputProtocol: AnyObject {
    func onSetValidatePhoneNumberFinished(with result: Result<EmptyModel, APIError>, phone: String)
    func onSetVerifyPhoneNumberFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - AuthPhone InteractorInput
class AuthPhoneInteractorInput {
    weak var output: AuthPhoneInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - AuthPhone InteractorInputProtocol
extension AuthPhoneInteractorInput: AuthPhoneInteractorInputProtocol {
    func setValidatePhoneNumber(phone: String) {
        self.authService?.setValidatePhoneNumber(phone: phone, completion: { [weak self] (result) in
            self?.output?.onSetValidatePhoneNumberFinished(with: result.unwrapSuccessModel(), phone: phone)
        })
    }
    
    func setVerifyPhoneNumber(phone: String) {
        self.authService?.setVerifyPhoneNumber(phone: phone) { [weak self] (result) in
            self?.output?.onSetVerifyPhoneNumberFinished(with: result.unwrapSuccessModel())
        }
    }
    
    
}
