//
//  
//  AuthPhoneRouter.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol AuthPhoneRouterProtocol {
    
}

// MARK: - AuthPhone Router
class AuthPhoneRouter {
    weak var viewController: AuthPhoneViewController?
    
    static func setupModule(delegate: AuthPhoneViewDelegate?) -> AuthPhoneViewController {
        let viewController = AuthPhoneViewController()
        let router = AuthPhoneRouter()
        let interactorInput = AuthPhoneInteractorInput()
        let viewModel = AuthPhoneViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - AuthPhone RouterProtocol
extension AuthPhoneRouter: AuthPhoneRouterProtocol {
    
}
