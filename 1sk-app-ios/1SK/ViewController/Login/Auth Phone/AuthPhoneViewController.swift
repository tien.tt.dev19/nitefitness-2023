//
//  
//  AuthPhoneViewController.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

protocol AuthPhoneViewDelegate: AnyObject {
    func onAuthPhoneDismiss()
    func onAuthPhoneLogin()
    func onAuthPhoneRegister()
    func onAuthPhoneSuccess(phoneNumber: String?)
}

// MARK: - ViewProtocol
protocol AuthPhoneViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onAuthPhoneSuccess()
    func setErrorUI(message: String)
    func setValidateUI(error: Int, message: String)
}

// MARK: - AuthPhone ViewController
class AuthPhoneViewController: BaseViewController {
    var router: AuthPhoneRouterProtocol!
    var viewModel: AuthPhoneViewModelProtocol!
    weak var delegate: AuthPhoneViewDelegate?
    
    @IBOutlet weak var lbl_TitleState: UILabel!
    
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var lbl_Error: UILabel!
    
    @IBOutlet weak var view_Login: UIView!
    @IBOutlet weak var lbl_Login: UILabel!
    
    @IBOutlet weak var view_Register: UIView!
    @IBOutlet weak var lbl_Register: UILabel!
    
    @IBOutlet weak var btn_Close: UIButton!
    @IBOutlet weak var btn_Confirm: UIButton!
    
    private var titleState = "Đăng ký tài khoản"
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tf_Phone.becomeFirstResponder()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUIStateAuth()
        self.setInitUITextField()
        self.setInitUIErrorValidate()
        self.setStateConfirmButton(isSelected: false)
        
        //self.tf_Phone.text = "0100100100"
    }
    
    private func setInitUIStateAuth() {
        switch self.viewModel.stateAuthPhoneValue {
        case .Register:
            self.lbl_TitleState.text = "Đăng ký tài khoản"
            
        case .ForgotPassword:
            self.lbl_TitleState.text = "Quên mật khẩu?"
            
        case .Verify:
            self.lbl_TitleState.text = "Xác thực số điện thoại"
            if gAppSettings.isRequiredVerifyPhone == true {
                self.btn_Close.isHidden = true
            }
            
        case .None:
            self.lbl_TitleState.text = "Error"
        }
    }
    
    private func setInitUIErrorValidate() {
        let atributeLogin_0 = NSAttributedString(string: "Số điện thoại này đã được đăng ký. ", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black])
        let atributeLogin_1 = NSAttributedString(string: "Đăng nhập", attributes: [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .semibold),
            NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.black])
        let atributeLogin_2 = NSAttributedString(string: " để tiếp tục", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black])
        
        let atributeLogin = NSMutableAttributedString(attributedString: atributeLogin_0)
        atributeLogin.append(atributeLogin_1)
        atributeLogin.append(atributeLogin_2)
        self.lbl_Login.attributedText = atributeLogin
        
        let atributeRegister_0 = NSAttributedString(string: "Số điện thoại này chưa được đăng ký. ", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black])
        let atributeRegister_1 = NSAttributedString(string: "Đăng ký", attributes: [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 14, weight: .semibold),
            NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.black])
        let atributeRegister_2 = NSAttributedString(string: " để tiếp tục", attributes: [
            NSAttributedString.Key.foregroundColor: UIColor.black])
        
        let atributeRegister = NSMutableAttributedString(attributedString: atributeRegister_0)
        atributeRegister.append(atributeRegister_1)
        atributeRegister.append(atributeRegister_2)
        self.lbl_Register.attributedText = atributeRegister
    }
    
    private func setStateConfirmButton(isSelected: Bool) {
        self.btn_Confirm.isSelected = isSelected
        if isSelected {
            self.btn_Confirm.backgroundColor = R.color.mainColor()
        } else {
            self.btn_Confirm.backgroundColor = R.color.mainDeselect()
        }
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.delegate?.onAuthPhoneDismiss()
    }
    
    @IBAction func onLoginAction(_ sender: Any) {
        self.delegate?.onAuthPhoneLogin()
    }
    
    @IBAction func onRegisterAction(_ sender: Any) {
        self.delegate?.onAuthPhoneRegister()
    }
    
    @IBAction func onConfirmAction(_ sender: Any) {
        guard self.tf_Phone.text?.count ?? 0 == 10 else {
            SKToast.shared.showToast(content: "Số điện thoại không đúng (gợi ý: 0912345678)")
            self.tf_Phone.becomeFirstResponder()
            return
        }
        guard self.tf_Phone.text?.first == "0" else {
            SKToast.shared.showToast(content: "Số điện thoại không đúng (gợi ý: 0912345678)")
            self.tf_Phone.becomeFirstResponder()
            return
        }
        guard let phone = self.tf_Phone.text else {
            return
        }
        self.lbl_Error.isHidden = true
        self.view.endEditing(true)
        self.viewModel.onValidatePhoneNumberAction(phone: phone)
    }
}

// MARK: - AuthPhone ViewProtocol
extension AuthPhoneViewController: AuthPhoneViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onAuthPhoneSuccess() {
        guard let phoneNumber = self.tf_Phone.text else {
            return
        }
        self.delegate?.onAuthPhoneSuccess(phoneNumber: phoneNumber)
    }
    
    func setErrorUI(message: String) {
        self.lbl_Error.text = message
        self.lbl_Error.isHidden = false
    }
    
    func setValidateUI(error: Int, message: String) {
        switch error {
        case 0:
            switch self.viewModel.stateAuthPhoneValue {
            case .Register:
                self.view_Login.isHidden = false
                self.view_Register.isHidden = true
                
            case .ForgotPassword:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = true
                
            case .Verify:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = true
                
            case .None:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = true
            }
            
        case 404:
            switch self.viewModel.stateAuthPhoneValue {
            case .Register:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = true
                
            case .ForgotPassword:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = false
                
            case .Verify:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = true
                
            case .None:
                self.view_Login.isHidden = true
                self.view_Register.isHidden = true
            }
            
        default:
            self.view_Login.isHidden = true
            self.view_Register.isHidden = true
        }
    }
}

// MARK: - UITextFieldDelegate
extension AuthPhoneViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Phone.delegate = self
        self.tf_Phone.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
    }
    
    @objc func textFieldDidChangeSelection() {
        self.lbl_Error.isHidden = true
        if self.tf_Phone.text?.count == 10 {
            self.setStateConfirmButton(isSelected: true)
        } else {
            self.setStateConfirmButton(isSelected: false)
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.isBackSpace == false else { return true }
        
        let text0 = textField.text?.first
        switch text0 {
        case "0":
            if textField.text?.count ?? 0 < 10 {
                return true
            } else {
                return false
            }
            
        default:
            if textField.text?.count ?? 0 < 10 {
                return true
            } else {
                return false
            }
        }
    }
}
