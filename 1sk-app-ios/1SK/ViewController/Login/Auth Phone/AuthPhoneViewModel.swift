//
//  
//  AuthPhoneViewModel.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

enum StateAuthPhone {
    case Register
    case ForgotPassword
    case Verify
    case None
    
    var value: Int {
        switch self {
        case .Register:
            return 3
            
        case .ForgotPassword:
            return 2
            
        case .Verify:
            return 1
            
        case .None:
            return 0
        }
    }
}

// MARK: - ViewModelProtocol
protocol AuthPhoneViewModelProtocol {
    func onViewDidLoad()
    func onValidatePhoneNumberAction(phone: String)
    
    var stateAuthPhoneValue: StateAuthPhone { get set }
}

// MARK: - AuthPhone ViewModel
class AuthPhoneViewModel {
    weak var view: AuthPhoneViewProtocol?
    private var interactor: AuthPhoneInteractorInputProtocol

    init(interactor: AuthPhoneInteractorInputProtocol) {
        self.interactor = interactor
    }

    var stateAuthPhone: StateAuthPhone = .None
}

// MARK: - AuthPhone ViewModelProtocol
extension AuthPhoneViewModel: AuthPhoneViewModelProtocol {
    func onViewDidLoad() {
        //
    }
    
    func onValidatePhoneNumberAction(phone: String) {
        self.view?.showHud()
        self.interactor.setValidatePhoneNumber(phone: phone)
    }
    
    var stateAuthPhoneValue: StateAuthPhone {
        get {
            return self.stateAuthPhone
        }
        set {
            self.stateAuthPhone = newValue
        }
    }
}

// MARK: - AuthPhone InteractorOutputProtocol
extension AuthPhoneViewModel: AuthPhoneInteractorOutputProtocol {
    func onSetValidatePhoneNumberFinished(with result: Result<EmptyModel, APIError>, phone: String) {
        switch result {
        case .success:
            self.view?.setValidateUI(error: 0, message: "Số điện thoại này đã tồn tại trên hệ thống. \nVui lòng sử dụng số điện thoại khác để xác thực")
            
            switch self.stateAuthPhone {
            case .ForgotPassword:
                self.interactor.setVerifyPhoneNumber(phone: phone)
                
            case .Verify:
                self.view?.setErrorUI(message: "Số điện thoại này đã tồn tại trên hệ thống. \nVui lòng sử dụng số điện thoại khác để xác thực")
                
            default:
                break
            }
            
        case .failure(let error):
            self.view?.setValidateUI(error: error.statusCode, message: "Số điện thoại này chưa được đăng ký.")
            if error.statusCode == 404 {
                switch self.stateAuthPhone {
                case .Register:
                    self.interactor.setVerifyPhoneNumber(phone: phone)
                    
                case .Verify:
                    self.interactor.setVerifyPhoneNumber(phone: phone)
                    
                default:
                    break
                }
                
            } else {
                self.view?.setErrorUI(message: error.message)
            }
        }
        self.view?.hideHud()
    }
    
    func onSetVerifyPhoneNumberFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            self.view?.onAuthPhoneSuccess()
            
        case .failure(let error):
            self.view?.setErrorUI(message: error.message)
            self.view?.hideHud()
        }
    }
}
