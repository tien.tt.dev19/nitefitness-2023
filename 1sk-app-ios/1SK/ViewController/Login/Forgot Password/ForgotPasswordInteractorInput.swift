//
//  
//  ForgotPasswordInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ForgotPasswordInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol ForgotPasswordInteractorOutputProtocol: AnyObject {
    
}

// MARK: - ForgotPassword InteractorInput
class ForgotPasswordInteractorInput {
    weak var output: ForgotPasswordInteractorOutputProtocol?
}

// MARK: - ForgotPassword InteractorInputProtocol
extension ForgotPasswordInteractorInput: ForgotPasswordInteractorInputProtocol {

}
