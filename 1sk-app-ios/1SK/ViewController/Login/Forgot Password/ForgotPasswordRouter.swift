//
//  
//  ForgotPasswordRouter.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ForgotPasswordRouterProtocol {
    func dismiss()
}

// MARK: - ForgotPassword Router
class ForgotPasswordRouter {
    weak var viewController: ForgotPasswordViewController?
    
    static func setupModule(delegate: ForgotPasswordViewDelegate?) -> ForgotPasswordViewController {
        let viewController = ForgotPasswordViewController()
        let router = ForgotPasswordRouter()
        let interactorInput = ForgotPasswordInteractorInput()
        let viewModel = ForgotPasswordViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ForgotPassword RouterProtocol
extension ForgotPasswordRouter: ForgotPasswordRouterProtocol {
    func dismiss() {
        self.viewController?.dismiss(animated: true)
    }
}
