//
//  
//  ForgotPasswordViewController.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit
import SnapKit

protocol ForgotPasswordViewDelegate: AnyObject {
    func onRegisterAccount()
    func onForgotPasswordSuccess(phoneNumber: String?, password: String?)
}

// MARK: - ViewProtocol
protocol ForgotPasswordViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - ForgotPassword ViewController
class ForgotPasswordViewController: BaseViewController {
    var router: ForgotPasswordRouterProtocol!
    var viewModel: ForgotPasswordViewModelProtocol!
    weak var delegate: ForgotPasswordViewDelegate?
    
    @IBOutlet weak var view_Container: UIView!
    
    // Page tab
    private lazy var pageViewController: VXPageViewController = VXPageViewController()
    
    private lazy var authPhoneRouter = AuthPhoneRouter.setupModule(delegate: self)
    private lazy var authCodeRouter = AuthCodeRouter.setupModule(delegate: self)
    private lazy var resetPasswordRouter = ResetPasswordRouter.setupModule(delegate: self)
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setStateAuthPhoneRouter()
    }
    
    // MARK: - Action
    
}

// MARK: - Page Menu
extension ForgotPasswordViewController {
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: - ForgotPassword ViewProtocol
extension ForgotPasswordViewController: ForgotPasswordViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: AuthPhoneViewDelegate
extension ForgotPasswordViewController: AuthPhoneViewDelegate {
    func setStateAuthPhoneRouter() {
        self.authPhoneRouter.viewModel.stateAuthPhoneValue = .ForgotPassword
        self.addChildView(childViewController: self.authPhoneRouter)
    }
    
    func onAuthPhoneDismiss() {
        self.router.dismiss()
    }
    
    func onAuthPhoneLogin() {
        self.router.dismiss()
    }
    
    func onAuthPhoneRegister() {
        self.delegate?.onRegisterAccount()
    }
    
    func onAuthPhoneSuccess(phoneNumber: String?) {
        self.setStateAuthCodeRouter(phoneNumber: phoneNumber)
    }
}

// MARK: AuthCodeViewDelegate
extension ForgotPasswordViewController: AuthCodeViewDelegate {
    func setStateAuthCodeRouter(phoneNumber: String?) {
        self.authCodeRouter.viewModel.stateAuthPhone = .ForgotPassword
        self.authCodeRouter.viewModel.phoneNumber = phoneNumber
        self.addChildView(childViewController: self.authCodeRouter)
    }
    
    func onAuthCodeViewDismiss() {
        self.addChildView(childViewController: self.authPhoneRouter)
    }
    
    func onAuthCodeSuccess(phoneNumber: String?, codeOtp: String?) {
        self.setStateResetPasswordRouter(phone: phoneNumber, code: codeOtp)
    }
}

// MARK: ResetPasswordViewDelegate
extension ForgotPasswordViewController: ResetPasswordViewDelegate {
    func setStateResetPasswordRouter(phone: String?, code: String?) {
        self.resetPasswordRouter.viewModel.phoneNumberValue = phone
        self.resetPasswordRouter.viewModel.codeOtpValue = code
        self.addChildView(childViewController: self.resetPasswordRouter)
    }
    
    func onResetPasswordDismiss() {
        self.router.dismiss()
    }
    
    func onResetPasswordSuccess(phoneNumber: String?, password: String?) {
        self.delegate?.onForgotPasswordSuccess(phoneNumber: phoneNumber, password: password)
        self.router.dismiss()
    }
}
