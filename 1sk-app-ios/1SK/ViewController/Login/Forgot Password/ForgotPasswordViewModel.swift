//
//  
//  ForgotPasswordViewModel.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ForgotPasswordViewModelProtocol {
    func onViewDidLoad()
}

// MARK: - ForgotPassword ViewModel
class ForgotPasswordViewModel {
    weak var view: ForgotPasswordViewProtocol?
    private var interactor: ForgotPasswordInteractorInputProtocol

    init(interactor: ForgotPasswordInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - ForgotPassword ViewModelProtocol
extension ForgotPasswordViewModel: ForgotPasswordViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - ForgotPassword InteractorOutputProtocol
extension ForgotPasswordViewModel: ForgotPasswordInteractorOutputProtocol {
    
}
