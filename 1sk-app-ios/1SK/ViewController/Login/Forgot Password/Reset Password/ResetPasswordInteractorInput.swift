//
//  
//  ResetPasswordInteractorInput.swift
//  1SK
//
//  Created by Thaad on 19/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ResetPasswordInteractorInputProtocol {
    func setResetPassword(phoneNumber: String, otpCode: String, newPassword: String, reNewPassword: String)
}

// MARK: - Interactor Output Protocol
protocol ResetPasswordInteractorOutputProtocol: AnyObject {
    func onSetResetPasswordFinished(with result: Result<EmptyModel, APIError>, password: String)
}

// MARK: - ResetPassword InteractorInput
class ResetPasswordInteractorInput {
    weak var output: ResetPasswordInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - ResetPassword InteractorInputProtocol
extension ResetPasswordInteractorInput: ResetPasswordInteractorInputProtocol {
    func setResetPassword(phoneNumber: String, otpCode: String, newPassword: String, reNewPassword: String) {
        self.authService?.setResetPassword(phoneNumber: phoneNumber, otpCode: otpCode, newPassword: newPassword, reNewPassword: reNewPassword, completion: { [weak self] (result) in
            self?.output?.onSetResetPasswordFinished(with: result.unwrapSuccessModel(), password: newPassword)
        })
    }

}
