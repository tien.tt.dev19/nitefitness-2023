//
//  
//  ResetPasswordRouter.swift
//  1SK
//
//  Created by Thaad on 19/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ResetPasswordRouterProtocol {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?)
}

// MARK: - ResetPassword Router
class ResetPasswordRouter {
    weak var viewController: ResetPasswordViewController?
    
    static func setupModule(delegate: ResetPasswordViewDelegate?) -> ResetPasswordViewController {
        let viewController = ResetPasswordViewController()
        let router = ResetPasswordRouter()
        let interactorInput = ResetPasswordInteractorInput()
        let viewModel = ResetPasswordViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ResetPassword RouterProtocol
extension ResetPasswordRouter: ResetPasswordRouterProtocol {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?) {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.titleAlert = "Đổi mật khẩu thành công"
        self.viewController?.present(controller, animated: false)
    }
}
