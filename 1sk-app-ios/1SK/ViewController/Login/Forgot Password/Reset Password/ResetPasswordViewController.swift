//
//  
//  ResetPasswordViewController.swift
//  1SK
//
//  Created by Thaad on 19/05/2022.
//
//

import UIKit

protocol ResetPasswordViewDelegate: AnyObject {
    func onResetPasswordDismiss()
    func onResetPasswordSuccess(phoneNumber: String?, password: String?)
}

// MARK: - ViewProtocol
protocol ResetPasswordViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setErrorUI(message: String)
    func setShowAlertResetPasswordSuccess()
}

// MARK: - ResetPassword ViewController
class ResetPasswordViewController: BaseViewController {
    var router: ResetPasswordRouterProtocol!
    var viewModel: ResetPasswordViewModelProtocol!
    weak var delegate: ResetPasswordViewDelegate?
    
    @IBOutlet weak var scroll_ScrollView: UIScrollView!
    
    @IBOutlet weak var tf_Password: UITextField!
    @IBOutlet weak var tf_PasswordConfirm: UITextField!
    
    @IBOutlet weak var btn_ShowNewPassword: UIButton!
    @IBOutlet weak var btn_ShowReNewPassword: UIButton!
    
    @IBOutlet weak var lbl_Error: UILabel!
    @IBOutlet weak var btn_Confirm: UIButton!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tf_Password.becomeFirstResponder()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITextField()
        self.setInitPasswordUI()
        self.setStateConfirmButton(isSelected: false)
        self.setInitGestureRecognizer()
    }
    
    func setInitPasswordUI() {
        self.btn_ShowNewPassword.setImage(R.image.ic_password_hidden(), for: .normal)
        self.btn_ShowNewPassword.setImage(R.image.ic_password_show(), for: .selected)
        
        self.btn_ShowReNewPassword.setImage(R.image.ic_password_hidden(), for: .normal)
        self.btn_ShowReNewPassword.setImage(R.image.ic_password_show(), for: .selected)
    }
    
    func setStateConfirmButton(isSelected: Bool) {
        self.btn_Confirm.isSelected = isSelected
        if isSelected {
            self.btn_Confirm.backgroundColor = R.color.mainColor()
        } else {
            self.btn_Confirm.backgroundColor = R.color.mainDeselect()
        }
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.delegate?.onResetPasswordDismiss()
    }
    
    @IBAction func onShowNewPasswordAction(_ sender: Any) {
        self.btn_ShowNewPassword.isSelected = !self.btn_ShowNewPassword.isSelected
        self.tf_Password.isSecureTextEntry = !self.btn_ShowNewPassword.isSelected
    }
    
    @IBAction func onShowReNewPasswordAction(_ sender: Any) {
        self.btn_ShowReNewPassword.isSelected = !self.btn_ShowReNewPassword.isSelected
        self.tf_PasswordConfirm.isSecureTextEntry = !self.btn_ShowReNewPassword.isSelected
    }
    
    @IBAction func onConfirmAction(_ sender: Any) {
        guard let password = self.tf_Password.text, password.count >= 6 else {
            SKToast.shared.showToast(content: "Mật khẩu phải trong khoảng từ 6 đến 32 ký tự")
            self.tf_Password.becomeFirstResponder()
            return
        }
        guard let password = self.tf_Password.text, password.count <= 32 else {
            SKToast.shared.showToast(content: "Mật khẩu phải trong khoảng từ 6 đến 32 ký tự")
            self.tf_Password.becomeFirstResponder()
            return
        }
        guard let passwordConfirm = self.tf_PasswordConfirm.text, passwordConfirm.count >= 6 else {
            SKToast.shared.showToast(content: "Mật khẩu phải trong khoảng từ 6 đến 32 ký tự")
            self.tf_PasswordConfirm.becomeFirstResponder()
            return
        }
        guard password == passwordConfirm else {
            SKToast.shared.showToast(content: "Xác nhận mật khẩu không khớp")
            self.tf_PasswordConfirm.becomeFirstResponder()
            return
        }
        self.view.endEditing(true)
        self.viewModel.onConfirmAction(newPassword: password, reNewPassword: passwordConfirm)
    }
    
}

// MARK: - ResetPassword ViewProtocol
extension ResetPasswordViewController: ResetPasswordViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setErrorUI(message: String) {
        self.lbl_Error.text = message
        self.lbl_Error.isHidden = false
    }
}

// MARK: - UITapGestureRecognizer
extension ResetPasswordViewController {
    func setInitGestureRecognizer() {
        self.scroll_ScrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.view.endEditing(true)
    }
}

// MARK: UITextFieldDelegate
extension ResetPasswordViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Password.delegate = self
        self.tf_PasswordConfirm.delegate = self
        
        self.tf_Password.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.tf_PasswordConfirm.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func onTextFieldDidChange(_ textField: UITextField) {
        var isPasswordValidate = false
        
        if let password = self.tf_Password.text, password.count >= 6, password.count < 32 {
            if let passwordConfirm = self.tf_PasswordConfirm.text, passwordConfirm == password {
                isPasswordValidate = true
            }
        }
        
        if isPasswordValidate {
            self.setStateConfirmButton(isSelected: true)
        } else {
            self.setStateConfirmButton(isSelected: false)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.isBackSpace == false else { return true }
        
        switch textField {
        case self.tf_Password, self.tf_PasswordConfirm:
            if textField.text?.count ?? 0 < 32 {
                return true
                
            } else {
                return false
            }
            
        default:
            return true
        }
    }
}

// MARK: AlertConfirmSuccessViewDelegate
extension ResetPasswordViewController: AlertConfirmSuccessViewDelegate {
    func setShowAlertResetPasswordSuccess() {
        self.router.presentAlertConfirmSuccessVC(delegate: self)
    }
    
    func onDidDismissAlertConfirmSuccessView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            self.delegate?.onResetPasswordSuccess(phoneNumber: self.viewModel.phoneNumberValue, password: self.viewModel.passwordValue)
        }
    }
}
