//
//  
//  ResetPasswordViewModel.swift
//  1SK
//
//  Created by Thaad on 19/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ResetPasswordViewModelProtocol {
    func onViewDidLoad()
    func onConfirmAction(newPassword: String, reNewPassword: String)
    
    var phoneNumberValue: String? { get set }
    var codeOtpValue: String? { get set }
    var passwordValue: String? { get set}
}

// MARK: - ResetPassword ViewModel
class ResetPasswordViewModel {
    weak var view: ResetPasswordViewProtocol?
    private var interactor: ResetPasswordInteractorInputProtocol

    init(interactor: ResetPasswordInteractorInputProtocol) {
        self.interactor = interactor
    }

    var phoneNumber: String?
    var codeOtp: String?
    var password: String?
    
}

// MARK: - ResetPassword ViewModelProtocol
extension ResetPasswordViewModel: ResetPasswordViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onConfirmAction(newPassword: String, reNewPassword: String) {
        guard let _phoneNumber = self.phoneNumber, let _codeOtp = self.codeOtp else {
            return
        }
        self.view?.showHud()
        self.interactor.setResetPassword(phoneNumber: _phoneNumber, otpCode: _codeOtp, newPassword: newPassword, reNewPassword: reNewPassword)
    }
    
    var phoneNumberValue: String? {
        get {
            return self.phoneNumber
        }
        set {
            self.phoneNumber = newValue
        }
    }
    
    var codeOtpValue: String? {
        get {
            return self.codeOtp
        }
        set {
            self.codeOtp = newValue
        }
    }
    
    var passwordValue: String? {
        get {
            return self.password
        }
        set {
            self.password = newValue
        }
    }
}

// MARK: - ResetPassword InteractorOutputProtocol
extension ResetPasswordViewModel: ResetPasswordInteractorOutputProtocol {
    func onSetResetPasswordFinished(with result: Result<EmptyModel, APIError>, password: String) {
        switch result {
        case .success:
            self.password = password
            self.view?.setShowAlertResetPasswordSuccess()
            
        case .failure(let error):
            self.view?.setErrorUI(message: error.message)
            debugPrint(error.description)
        }
        self.view?.hideHud()
    }
}
