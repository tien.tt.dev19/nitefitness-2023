//
//  
//  LoginInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol LoginInteractorInputProtocol {
    func setLoginWithPhoneNumber(phone: String, password: String)
    func setLoginWithFacebook(accessToken: String)
    func setLoginWithGoogle(accessToken: String)
    func setLoginWithApple(accessToken: String)
}

// MARK: - Interactor Output Protocol
protocol LoginInteractorOutputProtocol: AnyObject {
    func onSetLoginWithPhoneNumberFinished(with result: Result<AuthModel, APIError>)
    func onSetLoginWithFacebookFinished(with result: Result<AuthModel, APIError>)
    func onSetLoginWithGoogleFinished(with result: Result<AuthModel, APIError>)
    func onSetLoginWithAppleFinished(with result: Result<AuthModel, APIError>)
}

// MARK: - Login InteractorInput
class LoginInteractorInput {
    weak var output: LoginInteractorOutputProtocol?
    let authService = AuthService()
}

// MARK: - Login InteractorInputProtocol
extension LoginInteractorInput: LoginInteractorInputProtocol {
    func setLoginWithPhoneNumber(phone: String, password: String) {
        self.authService.setLoginWithPhoneNumber(phone: phone, password: password) { [weak self] (result) in
            self?.output?.onSetLoginWithPhoneNumberFinished(with: result.unwrapSuccessModel())
        }
    }
    
    func setLoginWithFacebook(accessToken: String) {
        self.authService.loginWithFacebook(accessToken: accessToken) { [weak self] result in
            self?.output?.onSetLoginWithFacebookFinished(with: result.unwrapSuccessModel())
        }
    }
    
    func setLoginWithGoogle(accessToken: String) {
        self.authService.loginWithGoogle(accessToken: accessToken) { [weak self] (result) in
            self?.output?.onSetLoginWithGoogleFinished(with: result.unwrapSuccessModel())
        }
    }
    
    func setLoginWithApple(accessToken: String) {
        self.authService.loginWithApple(accessToken: accessToken) { [weak self] result in
            self?.output?.onSetLoginWithAppleFinished(with: result.unwrapSuccessModel())
        }
    }
}
