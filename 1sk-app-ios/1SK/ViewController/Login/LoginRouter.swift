//
//  
//  LoginRouter.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol LoginRouterProtocol {
    func dismiss()
    func showRegisterVC(delegate: RegisterViewDelegate?)
    func showForgotPasswordVC(delegate: ForgotPasswordViewDelegate?)
}

// MARK: - Login Router
class LoginRouter {
    weak var viewController: LoginViewController?
    
    static func setupModule(delegate: LoginViewDelegate) -> LoginViewController {
        let viewController = LoginViewController()
        let router = LoginRouter()
        let interactorInput = LoginInteractorInput()
        let viewModel = LoginViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - Login RouterProtocol
extension LoginRouter: LoginRouterProtocol {
    func dismiss() {
        self.viewController?.dismiss(animated: true, completion: nil)
    }
    
    func showRegisterVC(delegate: RegisterViewDelegate?) {
        let controller = RegisterRouter.setupModule(delegate: delegate)
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: true)
    }
    
    func showForgotPasswordVC(delegate: ForgotPasswordViewDelegate?) {
        let controller = ForgotPasswordRouter.setupModule(delegate: delegate)
        controller.modalPresentationStyle = .custom
        self.viewController?.present(controller, animated: true)
    }
}
