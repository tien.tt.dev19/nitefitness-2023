//
//  
//  LoginViewController.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit
import Firebase
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

protocol LoginViewDelegate: AnyObject {
    func onLoginSuccess()
}

// MARK: - ViewProtocol
protocol LoginViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func dismiss()
    func onLoginSuccess()
    func setErrorUI(message: String)
}

// MARK: - Login ViewController
class LoginViewController: BaseViewController {
    var router: LoginRouterProtocol!
    var viewModel: LoginViewModelProtocol!
    weak var delegate: LoginViewDelegate?
    
    @IBOutlet weak var scroll_ScrollView: UIScrollView!
    
    @IBOutlet weak var view_ReLogin: UIView!
    @IBOutlet weak var view_NewLogin: UIView!
    @IBOutlet weak var view_PhoneNumber: UIView!
    
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_FullName: UILabel!
    
    @IBOutlet weak var view_LoginWithFacebook: UIView!
    @IBOutlet weak var view_LoginWithGoogle: UIView!
    @IBOutlet weak var view_SignInWithApple: UIView!
    
    @IBOutlet weak var tf_Phone: UITextField!
    @IBOutlet weak var tf_Password: UITextField!
    
    @IBOutlet weak var lbl_Error: UILabel!
    
    @IBOutlet weak var btn_Login: UIButton!
    @IBOutlet weak var btn_LoginOtherAccount: UIButton!
    @IBOutlet weak var btn_Password: UIButton!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitStateLoginUI()
        self.setInitPasswordUI()
        self.setInitUITextField()
        self.setInitGestureRecognizer()
        self.setSignInWithAppleUI()
        self.setBtnLoginStateUI(isSelected: false)
    }
    
    func setInitStateLoginUI() {
        if self.viewModel.stateLoginValue == .ReLogin {
            self.view_ReLogin.isHidden = false
            self.view_NewLogin.isHidden = true
            self.view_PhoneNumber.isHidden = true
            self.btn_LoginOtherAccount.isHidden = false
            
            self.tf_Phone.text = SKUserDefaults.shared.phoneNumber
            self.lbl_FullName.text = SKUserDefaults.shared.fullName
            self.img_Avatar.setImageWith(imageUrl: SKUserDefaults.shared.avatar ?? "", placeHolder: R.image.default_avatar_2())
            
        } else {
            self.view_ReLogin.isHidden = true
            self.view_NewLogin.isHidden = false
            self.view_PhoneNumber.isHidden = false
            self.btn_LoginOtherAccount.isHidden = true
        }
    }
    
    func setInitPasswordUI() {
        self.btn_Password.setImage(R.image.ic_password_show(), for: .selected)
        self.btn_Password.setImage(R.image.ic_password_hidden(), for: .normal)
    }
    
    func setSignInWithAppleUI() {
        if #available(iOS 13.0, *) {
            self.view_SignInWithApple.isHidden = false
            
        } else {
            self.view_SignInWithApple.isHidden = true
        }
    }
    
    func setBtnLoginStateUI(isSelected: Bool) {
        self.btn_Login.isSelected = isSelected
        if isSelected {
            self.btn_Login.backgroundColor = R.color.mainColor()
        } else {
            self.btn_Login.backgroundColor = R.color.mainDeselect()
        }
    }
    
    func setLoginWithOtherAccountUI() {
        self.viewModel.stateLoginValue = .NewLogin
        self.setInitStateLoginUI()
    }
    
    func onValidateDataLogin() {
        guard self.tf_Phone.text?.count ?? 0 == 10 else {
            SKToast.shared.showToast(content: "Số điện thoại không đúng (gợi ý: 0912345678)")
            self.tf_Phone.becomeFirstResponder()
            self.setLoginWithOtherAccountUI()
            return
        }
        guard self.tf_Phone.text?.first == "0" else {
            SKToast.shared.showToast(content: "Số điện thoại không đúng (gợi ý: 0912345678)")
            self.tf_Phone.becomeFirstResponder()
            self.setLoginWithOtherAccountUI()
            return
        }
        
        guard self.tf_Password.text?.count ?? 0 >= 6 else {
            SKToast.shared.showToast(content: "Mật khẩu phải lớn hơn 6 ký tự")
            self.tf_Password.becomeFirstResponder()
            return
        }
        guard let phone = self.tf_Phone.text, let password = self.tf_Password.text else {
            return
        }
        self.view.endEditing(true)
        self.viewModel.onLoginAction(phone: phone, password: password)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func onLoginAction(_ sender: Any) {
        self.onValidateDataLogin()
    }
    
    @IBAction func onShowPasswordAction(_ sender: Any) {
        self.btn_Password.isSelected = !self.btn_Password.isSelected
        self.tf_Password.isSecureTextEntry = !self.btn_Password.isSelected
    }
    
    @IBAction func onLoginWithOtherAccountAction(_ sender: Any) {
        self.tf_Phone.text = ""
        self.tf_Password.text = ""
        self.btn_LoginOtherAccount.isHidden = true
        self.lbl_Error.isHidden = true
        self.setLoginWithOtherAccountUI()
    }
    
    @IBAction func onForgotPasswordAction(_ sender: Any) {
        self.router.showForgotPasswordVC(delegate: self)
    }
    
    @IBAction func onRegisterAction(_ sender: Any) {
        self.router.showRegisterVC(delegate: self)
    }
    
    @IBAction func onLoginWithFacebookAction(_ sender: Any) {
        self.setInitLoginWithFacebook()
    }
    
    @IBAction func onLoginWithGoogleAction(_ sender: Any) {
        self.setInitLoginWithGoogle()
    }
    
    @IBAction func onSignInWithAppleAction(_ sender: Any) {
        if #available(iOS 13.0, *) {
            self.setInitSignInWithApple()
        } else {
            // Fallback on earlier versions
        }
    }
}

// MARK: - Login ViewProtocol
extension LoginViewController: LoginViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func dismiss() {
        self.router.dismiss()
    }
    
    func onLoginSuccess() {
        self.delegate?.onLoginSuccess()
    }
    
    func setErrorUI(message: String) {
        self.lbl_Error.text = message
        self.lbl_Error.isHidden = false
    }
}

// MARK: - UITapGestureRecognizer
extension LoginViewController {
    func setInitGestureRecognizer() {
        self.scroll_ScrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.view.endEditing(true)
    }
}

// MARK: - UITextFieldDelegate
extension LoginViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Phone.delegate = self
        self.tf_Password.delegate = self
        
        self.tf_Phone.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
        self.tf_Password.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
    }
    
    @objc func textFieldDidChangeSelection() {
        self.lbl_Error.isHidden = true
        
        if self.view_PhoneNumber.isHidden == true {
            if self.tf_Password.text?.count ?? 0 >= 6 {
                self.setBtnLoginStateUI(isSelected: true)
            } else {
                self.setBtnLoginStateUI(isSelected: false)
            }
            
        } else {
            if self.tf_Phone.text?.count == 10 && self.tf_Password.text?.count ?? 0 >= 6 {
                self.setBtnLoginStateUI(isSelected: true)
            } else {
                self.setBtnLoginStateUI(isSelected: false)
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.onValidateDataLogin()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.tf_Phone:
            let text0 = textField.text?.first
            switch text0 {
            case "0":
                if textField.text?.count ?? 0 < 10 {
                    return true
                } else {
                    return false
                }
                
            default:
                if textField.text?.count ?? 0 < 10 {
                    return true
                } else {
                    return false
                }
            }
            
        default:
            return true
        }
    }
}

// MARK: RegisterViewDelegate
extension LoginViewController: RegisterViewDelegate {
    func onRegisterAccountSuccess(phoneNumber: String?, password: String?) {
        guard let _phone = phoneNumber, let _password = password else {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.view.endEditing(true)
            self.viewModel.onLoginAction(phone: _phone, password: _password)
        }
    }
}

// MARK: ForgotPasswordViewDelegate
extension LoginViewController: ForgotPasswordViewDelegate {
    func onRegisterAccount() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.router.showRegisterVC(delegate: self)
        }
    }
    
    func onForgotPasswordSuccess(phoneNumber: String?, password: String?) {
//        guard let _phone = phoneNumber, let _password = password else {
//            return
//        }
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
//            self.view.endEditing(true)
//            self.viewModel.onLoginAction(phone: _phone, password: _password)
//        }
    }
}

// MARK: - FBSDKLoginKit
extension LoginViewController {
    func setInitLoginWithFacebook() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["public_profile", "email"], from: self) { [weak self] (result, _) in
            guard let token = result?.token?.tokenString else {
                print("<Login> Facebook error ")
                return
            }
            print("<Login> Facebook success token:", token)
            self?.viewModel.onLoginWithFacebookSuccess(accessToken: token)
        }
    }
}

// MARK: - GIDSignInDelegate
extension LoginViewController {
    func setInitLoginWithGoogle() {
        guard let clientID = FirebaseApp.app()?.options.clientID else {
            return
        }
        let signInConfig = GIDConfiguration.init(clientID: clientID)
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            if let err = error {
                print("<Login> Google error: ", error.debugDescription)
                self.alertDefault(title: "Đăng nhập với Google", message: err.localizedDescription)
                
            } else {
                if let accessToken = user?.authentication.idToken {
                    print("<Login> Google success accessToken:", accessToken)
                    self.viewModel.onLoginWithGoogleSuccess(accessToken: accessToken)
                    
                } else {
                    print("<Login> Google success accessToken: nil")
                    self.alertDefault(title: "Đăng nhập với Google", message: "Có lỗi xảy ra")
                }
            }
        }
    }
    
}

// MARK: - ASAuthorizationControllerDelegate
@available(iOS 13.0, *)
extension LoginViewController: ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    func setInitSignInWithApple() {
        let appleIDProvider = ASAuthorizationAppleIDProvider()
        let request = appleIDProvider.createRequest()
        request.requestedScopes = [.fullName, .email]

        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }

    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        switch authorization.credential {
        case let appleIDCredential as ASAuthorizationAppleIDCredential:
            // Create an account in your system.
            
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("<Login> Apple Unable to fetch identity token")
                return
            }
            
            let authorizationCode = String(data: appleIDCredential.authorizationCode!, encoding: .utf8)!
            print("<Login> Apple authorizationCode: ", authorizationCode)
            
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("<Login> Apple Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                return
            }
            
            let userIdentifier = appleIDCredential.user
            let fullName = appleIDCredential.fullName
            let email = appleIDCredential.email
            
            print("<Login> Apple userIdentifier: ", userIdentifier)
            print("<Login> Apple fullName: ", fullName ?? "")
            print("<Login> Apple email: ", email ?? "")
            print("<Login> Apple idTokenString: ", idTokenString)
            
            self.viewModel.onLoginWithAppleSuccess(accessToken: authorizationCode)
            
        case let passwordCredential as ASPasswordCredential:
            // Sign in using an existing iCloud Keychain credential.
            let username = passwordCredential.user
            let password = passwordCredential.password
            
            print("<Login> Apple username: ", username)
            print("<Login> Apple password: ", password)
            
        default:
            print("<Login> Apple failure")
        }
    }

    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print("<Login> Apple error: ", error.localizedDescription)
    }
}
