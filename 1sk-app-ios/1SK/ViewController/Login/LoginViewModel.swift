//
//  
//  LoginViewModel.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit
import WebKit
import FirebaseAnalytics

enum StateLogin {
    case ReLogin
    case NewLogin
}

// MARK: - ViewModelProtocol
protocol LoginViewModelProtocol {
    func onViewDidLoad()
    
    func onLoginAction(phone: String, password: String)
    
    func onLoginWithGoogleSuccess(accessToken: String)
    func onLoginWithFacebookSuccess(accessToken: String)
    func onLoginWithAppleSuccess(accessToken: String)
    
    var stateLoginValue: StateLogin { get set }
}

// MARK: - Login ViewModel
class LoginViewModel {
    weak var view: LoginViewProtocol?
    private var interactor: LoginInteractorInputProtocol

    init(interactor: LoginInteractorInputProtocol) {
        self.interactor = interactor
        self.checkStateLogin()
    }
    
    var stateLogin: StateLogin = .NewLogin

    func checkStateLogin() {
        guard SKUserDefaults.shared.phoneNumber?.count ?? 0 == 10 && SKUserDefaults.shared.authType == AuthType.Phone.value else {
            self.stateLogin = .NewLogin
            return
        }
        self.stateLogin = .ReLogin
    }
    
    func getUserAgent() {
        if let userAgent = WKWebView().value(forKey: "userAgent") as? String {
            KeyChainManager.shared.userAgent = userAgent
            print("userAgent: ", userAgent)
        }
    }
}

// MARK: - Login ViewModelProtocol
extension LoginViewModel: LoginViewModelProtocol {
    func onViewDidLoad() {
        self.getUserAgent()
    }
    
    func onLoginAction(phone: String, password: String) {
        self.view?.showHud()
        self.interactor.setLoginWithPhoneNumber(phone: phone, password: password)
    }
    
    func onLoginWithGoogleSuccess(accessToken: String) {
        self.view?.showHud()
        self.interactor.setLoginWithGoogle(accessToken: accessToken)
    }

    func onLoginWithFacebookSuccess(accessToken: String) {
        self.view?.showHud()
        self.interactor.setLoginWithFacebook(accessToken: accessToken)
    }
    
    func onLoginWithAppleSuccess(accessToken: String) {
        self.view?.showHud()
        self.interactor.setLoginWithApple(accessToken: accessToken)
    }
    
    var stateLoginValue: StateLogin {
        get {
            return self.stateLogin
        }
        set {
            self.stateLogin = newValue
        }
    }
}

// MARK: - Login InteractorOutputProtocol
extension LoginViewModel: LoginInteractorOutputProtocol {
    func onSetLoginWithPhoneNumberFinished(with result: Result<AuthModel, APIError>) {
        self.onHandlerAPIResponse(result, authType: .Phone)
    }
    
    func onSetLoginWithFacebookFinished(with result: Result<AuthModel, APIError>) {
        self.onHandlerAPIResponse(result, authType: .Facebook)
    }
    
    func onSetLoginWithGoogleFinished(with result: Result<AuthModel, APIError>) {
        self.onHandlerAPIResponse(result, authType: .Google)
    }

    func onSetLoginWithAppleFinished(with result: Result<AuthModel, APIError>) {
        self.onHandlerAPIResponse(result, authType: .Apple)
    }
    
    private func onHandlerAPIResponse(_ result: Result<AuthModel, APIError>, authType: AuthType) {
        switch result {
        case .success(let model):
            model.authType = authType
            Configure.shared.setDataAuth(authModel: model)
            if let customer = model.customer {
                Configure.shared.setDataUser(userModel: customer)
            }
            
            self.view?.onLoginSuccess()
            self.view?.dismiss()
            
            AnalyticsHelper.setEvent(with: .USER_SIGN_IN)
            AnalyticsHelper.setUserID(with: model.customer?.id)
            
        case .failure(let error):
            if authType == .Phone {
                self.view?.setErrorUI(message: error.message)
                
            } else {
                SKToast.shared.showToast(content: "Đăng nhập thất bại")
            }
        }
        self.view?.hideHud()
    }
}
