//
//  
//  RegisterAccountInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol RegisterAccountInteractorInputProtocol {
    func setRegisterUser(codeOtp: String, fullname: String, email: String, phoneNumber: String, password: String, passwordConfirm: String)
}

// MARK: - Interactor Output Protocol
protocol RegisterAccountInteractorOutputProtocol: AnyObject {
    func onSetRegisterUserFinished(with result: Result<UserModel, APIError>, password: String)
}

// MARK: - RegisterAccount InteractorInput
class RegisterAccountInteractorInput {
    weak var output: RegisterAccountInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - RegisterAccount InteractorInputProtocol
extension RegisterAccountInteractorInput: RegisterAccountInteractorInputProtocol {
    func setRegisterUser(codeOtp: String, fullname: String, email: String, phoneNumber: String, password: String, passwordConfirm: String) {
        self.authService?.setRegisterUser(codeOtp: codeOtp, fullname: fullname, email: email, phoneNumber: phoneNumber, password: password, passwordConfirm: passwordConfirm, completion: { [weak self] (result) in
            self?.output?.onSetRegisterUserFinished(with: result.unwrapSuccessModel(), password: password)
        })
    }
}
