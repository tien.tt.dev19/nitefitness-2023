//
//  
//  RegisterAccountRouter.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol RegisterAccountRouterProtocol {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?)
}

// MARK: - RegisterAccount Router
class RegisterAccountRouter {
    weak var viewController: RegisterAccountViewController?
    
    static func setupModule(delegate: RegisterAccountViewDelegate?) -> RegisterAccountViewController {
        let viewController = RegisterAccountViewController()
        let router = RegisterAccountRouter()
        let interactorInput = RegisterAccountInteractorInput()
        let viewModel = RegisterAccountViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - RegisterAccount RouterProtocol
extension RegisterAccountRouter: RegisterAccountRouterProtocol {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?) {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.titleAlert = "Đăng ký thành công"
        self.viewController?.present(controller, animated: false)
    }
}
