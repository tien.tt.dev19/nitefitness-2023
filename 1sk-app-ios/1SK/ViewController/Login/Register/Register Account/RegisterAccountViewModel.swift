//
//  
//  RegisterAccountViewModel.swift
//  1SK
//
//  Created by Thaad on 17/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol RegisterAccountViewModelProtocol {
    func onViewDidLoad()
    func onRegisterAction(fullname: String, email: String, password: String, passwordConfirm: String)
    
    var phoneNumberValue: String? { get set }
    var passwordValue: String? { get }
    var codeOtpValue: String? { get set }
}

// MARK: - RegisterAccount ViewModel
class RegisterAccountViewModel {
    weak var view: RegisterAccountViewProtocol?
    private var interactor: RegisterAccountInteractorInputProtocol

    init(interactor: RegisterAccountInteractorInputProtocol) {
        self.interactor = interactor
    }

    var phoneNumber: String?
    var codeOtp: String?
    var password: String?
}

// MARK: - RegisterAccount ViewModelProtocol
extension RegisterAccountViewModel: RegisterAccountViewModelProtocol {
    func onViewDidLoad() {
        //
    }
    
    func onRegisterAction(fullname: String, email: String, password: String, passwordConfirm: String) {
        guard let phone = self.phoneNumber, let code = self.codeOtp else {
            return
        }
        self.view?.showHud()
        self.interactor.setRegisterUser(codeOtp: code, fullname: fullname, email: email, phoneNumber: phone, password: password, passwordConfirm: passwordConfirm)
    }
    
    var phoneNumberValue: String? {
        get {
            return self.phoneNumber
        }
        set {
            self.phoneNumber = newValue
        }
    }
    
    var codeOtpValue: String? {
        get {
            return self.codeOtp
        }
        set {
            self.codeOtp = newValue
        }
    }
    
    var passwordValue: String? {
        return self.password
    }
}

// MARK: - RegisterAccount InteractorOutputProtocol
extension RegisterAccountViewModel: RegisterAccountInteractorOutputProtocol {
    func onSetRegisterUserFinished(with result: Result<UserModel, APIError>, password: String) {
        switch result {
        case .success:
            self.password = password
            self.view?.setShowAlertConfirmSuccess()
            
            AnalyticsHelper.setEvent(with: .USER_SIGN_UP)
            
        case .failure(let error):
            self.view?.setErrorUI(message: error.message)
            debugPrint(error.description)
        }
        self.view?.hideHud()
    }
}
