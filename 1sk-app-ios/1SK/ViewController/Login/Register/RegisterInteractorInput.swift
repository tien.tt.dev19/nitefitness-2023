//
//  
//  RegisterInteractorInput.swift
//  1SK
//
//  Created by Thaad on 18/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol RegisterInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol RegisterInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Register InteractorInput
class RegisterInteractorInput {
    weak var output: RegisterInteractorOutputProtocol?
}

// MARK: - Register InteractorInputProtocol
extension RegisterInteractorInput: RegisterInteractorInputProtocol {

}
