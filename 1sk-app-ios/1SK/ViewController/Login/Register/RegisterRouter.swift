//
//  
//  RegisterRouter.swift
//  1SK
//
//  Created by Thaad on 18/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol RegisterRouterProtocol {
    func dismiss()
}

// MARK: - Register Router
class RegisterRouter {
    weak var viewController: RegisterViewController?
    
    static func setupModule(delegate: RegisterViewDelegate?) -> RegisterViewController {
        let viewController = RegisterViewController()
        let router = RegisterRouter()
        let interactorInput = RegisterInteractorInput()
        let viewModel = RegisterViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - Register RouterProtocol
extension RegisterRouter: RegisterRouterProtocol {
    func dismiss() {
        self.viewController?.dismiss(animated: true)
    }
}
