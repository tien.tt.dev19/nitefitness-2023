//
//  
//  RegisterViewController.swift
//  1SK
//
//  Created by Thaad on 18/05/2022.
//
//

import UIKit
import SnapKit

protocol RegisterViewDelegate: AnyObject {
    func onRegisterAccountSuccess(phoneNumber: String?, password: String?)
}

// MARK: - ViewProtocol
protocol RegisterViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - Register ViewController
class RegisterViewController: BaseViewController {
    var router: RegisterRouterProtocol!
    var viewModel: RegisterViewModelProtocol!
    weak var delegate: RegisterViewDelegate?
    
    @IBOutlet weak var view_Container: UIView!
    
    private lazy var authPhoneRouter = AuthPhoneRouter.setupModule(delegate: self)
    private lazy var authCodeRouter = AuthCodeRouter.setupModule(delegate: self)
    private lazy var registerAccountRouter = RegisterAccountRouter.setupModule(delegate: self)
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setStateAuthPhoneRouter()
    }
    
    // MARK: - Action
    
}

// MARK: - Register ViewProtocol
extension RegisterViewController: RegisterViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - Container View
extension RegisterViewController {
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: AuthPhoneViewDelegate
extension RegisterViewController: AuthPhoneViewDelegate {
    func setStateAuthPhoneRouter() {
        self.authPhoneRouter.viewModel.stateAuthPhoneValue = .Register
        self.addChildView(childViewController: self.authPhoneRouter)
    }
    
    func onAuthPhoneDismiss() {
        self.router.dismiss()
    }
    
    func onAuthPhoneLogin() {
        self.router.dismiss()
    }
    
    func onAuthPhoneRegister() {
        self.router.dismiss()
    }
    
    func onAuthPhoneSuccess(phoneNumber: String?) {
        self.setStateAuthCodeRouter(phoneNumber: phoneNumber)
    }
}

// MARK: AuthCodeViewDelegate
extension RegisterViewController: AuthCodeViewDelegate {
    func setStateAuthCodeRouter(phoneNumber: String?) {
        self.authCodeRouter.viewModel.stateAuthPhone = .Register
        self.authCodeRouter.viewModel.phoneNumber = phoneNumber
        self.addChildView(childViewController: self.authCodeRouter)
    }
    
    func onAuthCodeViewDismiss() {
        self.addChildView(childViewController: self.authPhoneRouter)
    }
    
    func onAuthCodeSuccess(phoneNumber: String?, codeOtp: String?) {
        self.registerAccountRouter.viewModel.phoneNumberValue = phoneNumber
        self.registerAccountRouter.viewModel.codeOtpValue = codeOtp
        self.addChildView(childViewController: self.registerAccountRouter)
    }
}

// MARK: RegisterAccountViewDelegate
extension RegisterViewController: RegisterAccountViewDelegate {
    func onRegisterAccountViewDismiss() {
        self.router.dismiss()
    }
    
    func onRegisterAccountSuccess(phoneNumber: String?, password: String?) {
        self.delegate?.onRegisterAccountSuccess(phoneNumber: phoneNumber, password: password)
        self.router.dismiss()
    }
}
