//
//  
//  RegisterViewModel.swift
//  1SK
//
//  Created by Thaad on 18/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol RegisterViewModelProtocol {
    func onViewDidLoad()
}

// MARK: - Register ViewModel
class RegisterViewModel {
    weak var view: RegisterViewProtocol?
    private var interactor: RegisterInteractorInputProtocol

    init(interactor: RegisterInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - Register ViewModelProtocol
extension RegisterViewModel: RegisterViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - Register InteractorOutputProtocol
extension RegisterViewModel: RegisterInteractorOutputProtocol {

}
