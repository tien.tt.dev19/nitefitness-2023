//
//  
//  VerifyPhoneInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol VerifyPhoneInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol VerifyPhoneInteractorOutputProtocol: AnyObject {
    
}

// MARK: - VerifyPhone InteractorInput
class VerifyPhoneInteractorInput {
    weak var output: VerifyPhoneInteractorOutputProtocol?
}

// MARK: - VerifyPhone InteractorInputProtocol
extension VerifyPhoneInteractorInput: VerifyPhoneInteractorInputProtocol {

}
