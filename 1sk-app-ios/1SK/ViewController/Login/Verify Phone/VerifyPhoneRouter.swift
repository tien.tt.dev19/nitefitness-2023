//
//  
//  VerifyPhoneRouter.swift
//  1SK
//
//  Created by Thaad on 09/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol VerifyPhoneRouterProtocol {
    func dismiss()
}

// MARK: - VerifyPhone Router
class VerifyPhoneRouter {
    weak var viewController: VerifyPhoneViewController?
    
    static func setupModule() -> VerifyPhoneViewController {
        let viewController = VerifyPhoneViewController()
        let router = VerifyPhoneRouter()
        let interactorInput = VerifyPhoneInteractorInput()
        let viewModel = VerifyPhoneViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - VerifyPhone RouterProtocol
extension VerifyPhoneRouter: VerifyPhoneRouterProtocol {
    func dismiss() {
        self.viewController?.dismiss(animated: true)
    }
}
