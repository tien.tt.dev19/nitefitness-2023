//
//  
//  VerifyPhoneViewController.swift
//  1SK
//
//  Created by Thaad on 09/06/2022.
//
//

import UIKit
import SnapKit

// MARK: - ViewProtocol
protocol VerifyPhoneViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
}

// MARK: - VerifyPhone ViewController
class VerifyPhoneViewController: BaseViewController {
    var router: VerifyPhoneRouterProtocol!
    var viewModel: VerifyPhoneViewModelProtocol!
    
    @IBOutlet weak var view_Container: UIView!
    
    private lazy var authPhoneRouter = AuthPhoneRouter.setupModule(delegate: self)
    private lazy var authCodeRouter = AuthCodeRouter.setupModule(delegate: self)
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setStateAuthPhoneRouter()
    }
    
    // MARK: - Action
    
}

// MARK: - VerifyPhone ViewProtocol
extension VerifyPhoneViewController: VerifyPhoneViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - Container View
extension VerifyPhoneViewController {
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: AuthPhoneViewDelegate
extension VerifyPhoneViewController: AuthPhoneViewDelegate {
    func setStateAuthPhoneRouter() {
        self.authPhoneRouter.viewModel.stateAuthPhoneValue = .Verify
        self.addChildView(childViewController: self.authPhoneRouter)
    }
    
    func onAuthPhoneDismiss() {
        self.router.dismiss()
    }
    
    func onAuthPhoneLogin() {
        // not use
    }
    
    func onAuthPhoneRegister() {
        // not use
    }
    
    func onAuthPhoneSuccess(phoneNumber: String?) {
        self.setStateAuthCodeRouter(phoneNumber: phoneNumber)
    }
}

// MARK: AuthCodeViewDelegate
extension VerifyPhoneViewController: AuthCodeViewDelegate {
    func setStateAuthCodeRouter(phoneNumber: String?) {
        self.authCodeRouter.viewModel.stateAuthPhone = .Verify
        self.authCodeRouter.viewModel.phoneNumber = phoneNumber
        self.addChildView(childViewController: self.authCodeRouter)
    }
    
    func onAuthCodeViewDismiss() {
        self.addChildView(childViewController: self.authPhoneRouter)
    }
    
    func onAuthCodeSuccess(phoneNumber: String?, codeOtp: String?) {
        self.presentAlertConfirmSuccessVC(delegate: self)
    }
}

// MARK: - AlertConfirmSuccessViewDelegate
extension VerifyPhoneViewController: AlertConfirmSuccessViewDelegate {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?) {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.titleAlert = "Xác thực thành công"
        self.present(controller, animated: false)
    }
    
    func onDidDismissAlertConfirmSuccessView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.router.dismiss()
        }
    }
}
