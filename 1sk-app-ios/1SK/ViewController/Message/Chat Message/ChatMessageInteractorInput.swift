//
//  
//  ChatMessageInteractorInput.swift
//  1SK
//
//  Created by Thaad on 25/11/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ChatMessageInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int)
    func set_DataNameFunction(param1: String, param2: Int)
}

// MARK: - Interactor Output Protocol
protocol ChatMessageInteractorOutputProtocol: AnyObject {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - ChatMessage InteractorInput
class ChatMessageInteractorInput {
    weak var output: ChatMessageInteractorOutputProtocol?
}

// MARK: - ChatMessage InteractorInputProtocol
extension ChatMessageInteractorInput: ChatMessageInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
    
    func set_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
}
