//
//  
//  ChatMessageRouter.swift
//  1SK
//
//  Created by Thaad on 25/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ChatMessageRouterProtocol {
    func onDismiss()
}

// MARK: - ChatMessage Router
class ChatMessageRouter {
    weak var viewController: ChatMessageViewController?
    
    static func setupModule() -> ChatMessageViewController {
        let viewController = ChatMessageViewController()
        let router = ChatMessageRouter()
        let interactorInput = ChatMessageInteractorInput()
        let viewModel = ChatMessageViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ChatMessage RouterProtocol
extension ChatMessageRouter: ChatMessageRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
}
