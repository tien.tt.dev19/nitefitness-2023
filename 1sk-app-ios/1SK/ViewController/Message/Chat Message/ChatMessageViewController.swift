//
//  
//  ChatMessageViewController.swift
//  1SK
//
//  Created by Thaad on 25/11/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ChatMessageViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - ChatMessage ViewController
class ChatMessageViewController: BaseViewController {
    var router: ChatMessageRouterProtocol!
    var viewModel: ChatMessageViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
    }
    
    private func setInitLayout() {
        self.view_Nav.addShadow(width: 0, height: 7, color: .black, radius: 3, opacity: 0.05)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismiss()
    }
    
}

// MARK: - ChatMessage ViewProtocol
extension ChatMessageViewController: ChatMessageViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}
