//
//  
//  ChatMessageViewModel.swift
//  1SK
//
//  Created by Thaad on 25/11/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ChatMessageViewModelProtocol {
    func onViewDidLoad()
    
    //var variable: Int? { get set }
    //var listVariable: [Int] { get set }
}

// MARK: - ChatMessage ViewModel
class ChatMessageViewModel {
    weak var view: ChatMessageViewProtocol?
    private var interactor: ChatMessageInteractorInputProtocol

    init(interactor: ChatMessageInteractorInputProtocol) {
        self.interactor = interactor
    }

    //var variable: Int?
    //var listVariable: [Int] = []
}

// MARK: - ChatMessage ViewModelProtocol
extension ChatMessageViewModel: ChatMessageViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - ChatMessage InteractorOutputProtocol
extension ChatMessageViewModel: ChatMessageInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
