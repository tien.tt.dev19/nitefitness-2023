//
//  CellTableViewHomeMessage.swift
//  1SK
//
//  Created by Tiến Trần on 14/09/2022.
//

import UIKit

class CellTableViewHomeMessage: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var view_badge: UIView!
    
    var model: AnnouncementModels? {
        didSet {
            if let data = model {
                self.lbl_title.text = data.title ?? ""
                self.lbl_description.text = data.description ?? ""
                
                if let createDate = data.createdAt {
                    self.lbl_time.text = Date(timeIntervalSince1970: Double(createDate)).toString(.hm)
                }
                
                let toDayString = Date().toDay.toString(.dmySlash)
                let dateFromSource = data.createdAt?.toDatedmySlash()
                
                if toDayString != dateFromSource {
                    self.lbl_time.text = data.createdAt?.toDate()
                }
                
                if let notiIsRead = data.isRead {
                    if notiIsRead {
                        self.lbl_title.textColor = UIColor(red: 0.451, green: 0.463, blue: 0.471, alpha: 1)
                        self.lbl_description.textColor = UIColor(red: 0.451, green: 0.463, blue: 0.471, alpha: 1)
                        self.lbl_time.textColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1)
                        self.view_badge.isHidden = true
                        return
                    }
                    
                    self.lbl_title.textColor = UIColor(red: 0.137, green: 0.161, blue: 0.188, alpha: 1)
                    self.lbl_description.textColor = UIColor(red: 0.451, green: 0.463, blue: 0.471, alpha: 1)
                    self.lbl_time.textColor = UIColor(red: 0.137, green: 0.161, blue: 0.188, alpha: 1)
                    self.view_badge.isHidden = false
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
