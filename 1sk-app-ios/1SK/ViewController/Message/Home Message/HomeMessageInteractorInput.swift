//
//  
//  HomeMessageInteractorInput.swift
//  1SK
//
//  Created by Thaad on 13/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HomeMessageInteractorInputProtocol {
    func get_Announcement(page: Int, per_page: Int)
    func set_Announcement(notiID: [Int])
    func set_AllAnnouncement(markType: String)
}

// MARK: - Interactor Output Protocol
protocol HomeMessageInteractorOutputProtocol: AnyObject {
    func onGet_Announcement_Finished(with result: Result<[AnnouncementModels], APIError>, page: Int, total: Int)
    func onSet_Announcement_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_AllAnnouncement_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - HomeMessage InteractorInput
class HomeMessageInteractorInput {
    weak var output: HomeMessageInteractorOutputProtocol?
    var AnnouncementService: AnnouncementServiceProtocol!
}

// MARK: - HomeMessage InteractorInputProtocol
extension HomeMessageInteractorInput: HomeMessageInteractorInputProtocol {
    func get_Announcement(page: Int, per_page: Int) {
        self.AnnouncementService.getAnnouncementList(page: page, per_page: per_page) { [weak self] result in
            self?.output?.onGet_Announcement_Finished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        }
    }
    
    func set_Announcement(notiID: [Int]) {
        self.AnnouncementService.setSingleAnnouncementStatus(notiID: notiID) { [weak self] result in
            self?.output?.onSet_Announcement_Finished(with: result.unwrapSuccessModel())
        }
    }
    
    func set_AllAnnouncement(markType: String) {
        self.AnnouncementService.setAnnouncementStatus(markType: markType) { [weak self] result in
            self?.output?.onSet_AllAnnouncement_Finished(with: result.unwrapSuccessModel())
        }
    }
}
