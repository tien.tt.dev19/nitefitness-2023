//
//  
//  HomeMessageRouter.swift
//  1SK
//
//  Created by Thaad on 13/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HomeMessageRouterProtocol {
    func goToDetail(model: AnnouncementModels)
}

// MARK: - HomeMessage Router
class HomeMessageRouter {
    weak var viewController: HomeMessageViewController?
    
    static func setupModule() -> HomeMessageViewController {
        let viewController = HomeMessageViewController()
        let router = HomeMessageRouter()
        let interactorInput = HomeMessageInteractorInput()
        let viewModel = HomeMessageViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.AnnouncementService = AnnouncementService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HomeMessage RouterProtocol
extension HomeMessageRouter: HomeMessageRouterProtocol {
    func goToDetail(model: AnnouncementModels) {
        let controller = MessageDetailRouter.setupModule(model: model)
        controller.modalPresentationStyle = .fullScreen
        controller.hidesBottomBarWhenPushed = true
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
}
