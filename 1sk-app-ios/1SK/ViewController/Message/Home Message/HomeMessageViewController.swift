//
//  
//  HomeMessageViewController.swift
//  1SK
//
//  Created by Thaad on 13/09/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol HomeMessageViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    func onInsertRowTop(at index: Int)
    func onChangeStateNoti()
    func onHidenBagde()
    func onEnableMarkAtReadAll()
}

// MARK: - HomeMessage ViewController
class HomeMessageViewController: BaseViewController {
    var router: HomeMessageRouterProtocol!
    var viewModel: HomeMessageViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var btn_MarkAtReadAll: UIButton!
    
    private var refreshControl: UIRefreshControl?
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Action
    @IBAction func onReadAllNotificationAction(_ sender: UIButton) {
        self.viewModel.onReadAllMessage()
    }
    
    @objc func onHandleNotisocketEvent(notification: NSNotification) {
        guard let notificationList = notification.object as? AnnouncementsSocketModel else {
            return
        }
        
        self.viewModel.onHandleSocketMessage(model: notificationList)
    }
}

//MARK: -UITableViewDataSource
extension HomeMessageViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewHomeMessage.self)
        self.tbv_TableView.delegate = self
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.emptyDataSetSource = self
        self.tbv_TableView.separatorStyle = .none
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleNotisocketEvent), name: .NewAnnouncementToAllEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onHandleNotisocketEvent), name: .NewAnnouncementToSingleUserEvent, object: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.anouncementList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewHomeMessage.self, for: indexPath)
        cell.model = self.viewModel.anouncementList[indexPath.row]
        return cell
    }
}

// MARK: Refresh Control
extension HomeMessageViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

//MARK: -UITableViewDelegate
extension HomeMessageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let notiState = self.viewModel.anouncementList[indexPath.row].isRead {
            if !notiState {
                if let notiID = self.viewModel.anouncementList[indexPath.row].id {
                    self.viewModel.notiChoose = indexPath.row
                    self.viewModel.onMarkAsRead(with: [notiID])
                    return
                }
            }
            self.router.goToDetail(model: self.viewModel.anouncementList[indexPath.row])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: - HomeMessage ViewProtocol
extension HomeMessageViewController: HomeMessageViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .bottom)
    }
    
    func onInsertRowTop(at index: Int) {
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .top)
    }
    
    func onChangeStateNoti() {
        self.router.goToDetail(model: self.viewModel.anouncementList[self.viewModel.notiChoose!])
    }
    
    func onHidenBagde() {
        gTabBarController?.tabBar.removeBadge(index: 3)
        self.btn_MarkAtReadAll.isUserInteractionEnabled = false
        self.btn_MarkAtReadAll.titleLabel?.textColor = UIColor(hex: "C4C4C4")
    }
    
    func onEnableMarkAtReadAll() {
        self.btn_MarkAtReadAll.isUserInteractionEnabled = true
        self.btn_MarkAtReadAll.titleLabel?.textColor = .black
    }
}

// MARK: - EmptyDataSetSource
extension HomeMessageViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có thông báo nào")
    }
    
    func image(forEmptyDataSet scrollView: UIScrollView) -> UIImage? {
        return UIImage(named: "ic_noNotification")
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
}
