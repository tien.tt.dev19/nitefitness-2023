//
//  
//  HomeMessageViewModel.swift
//  1SK
//
//  Created by Thaad on 13/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol HomeMessageViewModelProtocol {
    var anouncementList: [AnnouncementModels] { get set }
    var notiChoose: Int? { get set }
    
    func onViewDidLoad()
    func onViewWillAppear()
    func onLoadMoreAction()
    func onMarkAsRead(with notiIDs: [Int])
    func onHandleSocketMessage(model: AnnouncementsSocketModel)
    func onReadAllMessage()
    func onRefreshAction()
}

// MARK: - HomeMessage ViewModel
class HomeMessageViewModel {
    weak var view: HomeMessageViewProtocol?
    private var interactor: HomeMessageInteractorInputProtocol
    
    var anouncementList: [AnnouncementModels] = []
    var notiChoose: Int?
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 20
    private var isOutOfData = false
    
    init(interactor: HomeMessageInteractorInputProtocol) {
        self.interactor = interactor
    }
}

// MARK: - HomeMessage ViewModelProtocol
extension HomeMessageViewModel: HomeMessageViewModelProtocol {
    func onViewDidLoad() {
        self.view?.showHud()
    }
    
    func onViewWillAppear() {
        self.page = 1
        self.interactor.get_Announcement(page: self.page, per_page: self.perPage)
    }
    
    func onLoadMoreAction() {
        if self.anouncementList.count >= self.perPage && !self.isOutOfData {
            self.view?.showHud()
            self.interactor.get_Announcement(page: self.page + 1, per_page: self.perPage)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
        }
    }
    
    func onMarkAsRead(with notiIDs: [Int]) {
        self.view?.showHud()
        self.interactor.set_Announcement(notiID: notiIDs)
    }
    
    func onHandleSocketMessage(model: AnnouncementsSocketModel) {
        self.interactor.get_Announcement(page: 1, per_page: self.perPage)
    }
    
    func onReadAllMessage() {
        self.view?.showHud()
        self.interactor.set_AllAnnouncement(markType: "all")
    }
    
    func onRefreshAction() {
        self.view?.showHud()
        self.page = 1
        self.interactor.get_Announcement(page: self.page, per_page: self.perPage)
    }
    
    func onCheckAllMessageIsRead() -> Bool {
        var allRead = false
        for item in self.anouncementList {
            guard let isRead = item.isRead else { return false }
            if !isRead {
                allRead = false
                return false
            }
            allRead = true
        }
        return allRead
    }
    
    func onMarkReadAll() {
        for item in self.anouncementList {
            guard var isRead = item.isRead else { return }
            if !isRead {
                isRead = true
            }
        }
        
        if self.onCheckAllMessageIsRead() {
            self.view?.onHidenBagde()
            
        } else {
            self.view?.onEnableMarkAtReadAll()
        }
        
        self.view?.onReloadData()
    }
}

// MARK: - HomeMessage InteractorOutputProtocol
extension HomeMessageViewModel: HomeMessageInteractorOutputProtocol {
    func onGet_Announcement_Finished(with result: Result<[AnnouncementModels], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.anouncementList = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.anouncementList.append(object)
                    self.view?.onInsertRow(at: self.anouncementList.count - 1)
                }
            }
            
            self.page = page
            self.total = total
            
            // Set Out Of Data
            if self.anouncementList.count >= self.total {
                self.isOutOfData = true
            }
            
            if self.onCheckAllMessageIsRead() {
                self.view?.onHidenBagde()
                
            } else {
                self.view?.onEnableMarkAtReadAll()
            }
            
            if self.anouncementList.isEmpty {
                self.view?.onHidenBagde()
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure(let error):
            debugPrint(error)
            SKToast.shared.showToast(content: error.message)
        }
        
        self.view?.hideHud()
    }
    
    func onSet_Announcement_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(_):
            self.view?.onChangeStateNoti()
            
        case .failure(let error):
            debugPrint(error)
            SKToast.shared.showToast(content: error.message)
        }
        
        self.view?.hideHud()
    }
    
    func onSet_AllAnnouncement_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(_):
            self.interactor.get_Announcement(page: 1, per_page: self.perPage)

            
        case .failure(let error):
            debugPrint(error)
            SKToast.shared.showToast(content: error.message)
        }
        
    }
}
