//
//  
//  MessageDetailInteractorInput.swift
//  1SK
//
//  Created by Tiến Trần on 22/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol MessageDetailInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int)
    func set_DataNameFunction(param1: String, param2: Int)
}

// MARK: - Interactor Output Protocol
protocol MessageDetailInteractorOutputProtocol: AnyObject {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - MessageDetail InteractorInput
class MessageDetailInteractorInput {
    weak var output: MessageDetailInteractorOutputProtocol?
}

// MARK: - MessageDetail InteractorInputProtocol
extension MessageDetailInteractorInput: MessageDetailInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
    
    func set_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
}
