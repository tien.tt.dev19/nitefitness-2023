//
//  
//  MessageDetailRouter.swift
//  1SK
//
//  Created by Tiến Trần on 22/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol MessageDetailRouterProtocol {

}

// MARK: - MessageDetail Router
class MessageDetailRouter {
    weak var viewController: MessageDetailViewController?
    
    static func setupModule(model: AnnouncementModels) -> MessageDetailViewController {
        let viewController = MessageDetailViewController()
        let router = MessageDetailRouter()
        let interactorInput = MessageDetailInteractorInput()
        let viewModel = MessageDetailViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.notiModel = model
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - MessageDetail RouterProtocol
extension MessageDetailRouter: MessageDetailRouterProtocol {
    
}
