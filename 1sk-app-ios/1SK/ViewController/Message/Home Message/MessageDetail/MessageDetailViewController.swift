//
//  
//  MessageDetailViewController.swift
//  1SK
//
//  Created by Tiến Trần on 22/09/2022.
//
//

import UIKit
import WebKit

// MARK: - ViewProtocol
protocol MessageDetailViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - MessageDetail ViewController
class MessageDetailViewController: BaseViewController {
    var router: MessageDetailRouterProtocol!
    var viewModel: MessageDetailViewModelProtocol!
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var web_WebView_info: WKWebView!
    @IBOutlet weak var contraint_webView: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Thông báo"
        self.setInitWKWebView()
        self.lbl_title.text = self.viewModel.notiModel?.title ?? ""
        
        if let HHmm = self.viewModel.notiModel?.createdAt,
            let days = self.viewModel.notiModel?.createdAt?.toDatedmySlash() {
            let date = Date(timeIntervalSince1970: Double(HHmm)).toString(.hm)
            self.lbl_time.text = "\(date)  \(days)"
        }
        
        self.web_WebView_info.loadHTMLString(self.viewModel.notiModel?.content?.asHtmlStoreProductInfo ?? "", baseURL: nil)
    }
    
    // MARK: - Action
    
}

// MARK: - MessageDetail ViewProtocol
extension MessageDetailViewController: MessageDetailViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - WKNavigationDelegate
extension MessageDetailViewController: WKNavigationDelegate {
    func setInitWKWebView() {
        self.web_WebView_info?.navigationDelegate = self
        self.web_WebView_info?.scrollView.isScrollEnabled = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.web_WebView_info.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_WebView_info.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if let `height` = height as? CGFloat {
                        self.contraint_webView.constant = height
                    }
                })
            }
        })
    }
}
