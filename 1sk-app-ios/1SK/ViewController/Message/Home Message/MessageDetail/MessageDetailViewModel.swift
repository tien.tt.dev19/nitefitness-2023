//
//  
//  MessageDetailViewModel.swift
//  1SK
//
//  Created by Tiến Trần on 22/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol MessageDetailViewModelProtocol {
    func onViewDidLoad()
    
    var notiModel: AnnouncementModels? { get set }
    //var listVariable: [Int] { get set }
}

// MARK: - MessageDetail ViewModel
class MessageDetailViewModel {
    weak var view: MessageDetailViewProtocol?
    private var interactor: MessageDetailInteractorInputProtocol

    init(interactor: MessageDetailInteractorInputProtocol) {
        self.interactor = interactor
    }

    var notiModel: AnnouncementModels?
    //var listVariable: [Int] = []
}

// MARK: - MessageDetail ViewModelProtocol
extension MessageDetailViewModel: MessageDetailViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - MessageDetail InteractorOutputProtocol
extension MessageDetailViewModel: MessageDetailInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
