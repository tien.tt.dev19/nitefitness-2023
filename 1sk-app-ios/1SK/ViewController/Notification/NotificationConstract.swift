//
//  
//  NotificationConstract.swift
//  1SK
//
//  Created by tuyenvx on 25/02/2021.
//
//

import UIKit

// MARK: View
protocol NotificationViewProtocol: AnyObject {
    func onReloadData()
    func onUpdateView(with canReceiveNotification: Bool)
    
    func onInsertRow(at index: Int)
    func onLoadingSuccess()
    func onSetNotFoundView(isHidden: Bool)
}

// MARK: Interactor - Input
protocol NotificationInteractorInputProtocol {
    func getListNotification(page: Int, limit: Int)
    func setNotificationRead(announcementId: Int)
    
    func getAppointmentDetail(id: Int)
}

// MARK: Interactor - Output
protocol NotificationInteractorOutputProtocol: AnyObject {
    func onGetListNotificationFinished(with result: Result<[NotificationModel], APIError>, page: Int, total: Int)
    func onSetNotificationReadFinished(with result: Result<EmptyModel, APIError>)
    func onGetAppointmentDetailFinished(with result: Result<AppointmentModel, APIError>)
}

// MARK: Presenter
protocol NotificationPresenterProtocol {
    func onViewDidLoad()
    func onViewWillAppear()
    func onViewDidAppear()
    
    /// Action
    func onEnableAuthNotificationAction()
    func onRefreshAction()
    
    /// UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> NotificationModel?
    func onDidSelectRow(at indexPath: IndexPath)
    func onDidEndDisplayingRow(at indexPath: IndexPath)
    func onLoadMoreAction()
}

// MARK: Router -
protocol NotificationRouterProtocol: BaseRouterProtocol {
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func gotoDetailConsultingVC(appointment: AppointmentModel)
}
