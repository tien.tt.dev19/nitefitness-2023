//
//  
//  NotificationInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 25/02/2021.
//
//

import UIKit

class NotificationInteractorInput {
    weak var output: NotificationInteractorOutputProtocol?
    
    var configService: ConfigServiceProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - NotificationInteractorInput - NotificationInteractorInputProtocol -
extension NotificationInteractorInput: NotificationInteractorInputProtocol {
    func getListNotification(page: Int, limit: Int) {
        self.careService?.getListNotification(page: page, limit: limit, completion: { [weak self] result in
            self?.output?.onGetListNotificationFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
    
    func setNotificationRead(announcementId: Int) {
        self.careService?.setNotificationRead(announcementId: announcementId, completion: { [weak self] result in
            self?.output?.onSetNotificationReadFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getAppointmentDetail(id: Int) {
        self.careService?.getAppointmentDetail(appointmentId: id, completion: { [weak self] result in
            self?.output?.onGetAppointmentDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
