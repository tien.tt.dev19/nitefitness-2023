//
//  
//  NotificationPresenter.swift
//  1SK
//
//  Created by tuyenvx on 25/02/2021.
//
//

import UIKit

class NotificationPresenter {

    weak var view: NotificationViewProtocol?
    private var interactor: NotificationInteractorInputProtocol
    private var router: NotificationRouterProtocol

    init(interactor: NotificationInteractorInputProtocol,
         router: NotificationRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
    var pushNotiModel: PushNotiModel?
    
    // Loadmore
    var listNotification: [NotificationModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let limit = 10
    private var isOutOfData = false
    
    func getData(in page: Int) {
        self.router.showHud()
        self.interactor.getListNotification(page: page, limit: self.limit)
    }
    
    func onGetAppointmentDetailSuccess(appointment: AppointmentModel) {
        guard let statusId = appointment.status?.id else {
            return
        }
        switch statusId {
        case AppointmentStatus.complete.id, AppointmentStatus.finish.id:
            self.router.gotoDetailConsultingVC(appointment: appointment)
            
        default:
            self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: nil)
        }
    }
}

// MARK: - NotificationPresenterProtocol
extension NotificationPresenter: NotificationPresenterProtocol {
    func onViewDidLoad() {
        self.setInitUIApplication()
        self.getData(in: 1)
    }

    func onViewWillAppear() {
        self.onCheckUpdateViewNotificationAuthorization()
    }
    
    func onViewDidAppear() {
        if let push = self.pushNotiModel {
            switch push.screen {
            case .appointment:
                guard let appointmentId = push.data?.id else {
                    return
                }
                
                self.router.showHud()
                self.interactor.getAppointmentDetail(id: appointmentId)
                
            default:
                break
            }
            self.pushNotiModel = nil
        }
    }

    /// Action
    func onEnableAuthNotificationAction() {
        self.onCheckEnableNotificationAuthorization()
    }
    
    func onRefreshAction() {
        self.getData(in: 1)
    }
    
    /// UITableView
    func numberOfRows() -> Int {
        return self.listNotification.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> NotificationModel? {
        return self.listNotification[indexPath.row]
    }
    
    func onLoadMoreAction() {
        if self.listNotification.count >= self.limit && !self.isOutOfData {
            self.getData(in: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        let notification = self.listNotification[indexPath.row]
        guard let appointmentId = notification.extends?.appointmentId else {
            return
        }
        
        self.router.showHud()
        self.interactor.getAppointmentDetail(id: appointmentId)
    }
    
    func onDidEndDisplayingRow(at indexPath: IndexPath) {
        let notification = self.listNotification[indexPath.row]
        guard notification.isRead == false else {
            return
        }
        if let announcementId = notification.id {
            self.interactor.setNotificationRead(announcementId: announcementId)
            notification.isRead = true
        }
    }
}

// MARK: - NotificationInteractorOutput
extension NotificationPresenter: NotificationInteractorOutputProtocol {
    func onGetListNotificationFinished(with result: Result<[NotificationModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listNotification = listModel
                self.view?.onReloadData()
                
            } else {
                for object in listModel {
                    self.listNotification.append(object)
                    self.view?.onInsertRow(at: self.listNotification.count - 1)
                }
            }
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listNotification.count >= self.total {
                self.isOutOfData = true
            }
            
            // Set Status View Data Not Found
            if self.listNotification.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            if self.listNotification.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.router.hideHud()
    }
    
    func onSetNotificationReadFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            gBadgeCare -= 1
//            UIApplication.shared.applicationIconBadgeNumber = gBadgeCare
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetAppointmentDetailFinished(with result: Result<AppointmentModel, APIError>) {
        switch result {
        case .success(let model):
            self.onGetAppointmentDetailSuccess(appointment: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
}

// MARK: - Notification Authorization
extension NotificationPresenter {
    func onCheckEnableNotificationAuthorization() {
        UNUserNotificationCenter.current().getNotificationSettings { (setting) in
            DispatchQueue.main.async {
                switch setting.authorizationStatus {
                case .notDetermined:
                    self.requestNotificationAuthorization()
                    
                case .authorized, .ephemeral:
                    break
                    
                default:
                    self.openAppSetting()
                }
            }
        }
    }
    
    private func onCheckUpdateViewNotificationAuthorization() {
        UNUserNotificationCenter.current().getNotificationSettings { (setting) in
            DispatchQueue.main.async {
                switch setting.authorizationStatus {
                case .notDetermined:
                    self.view?.onUpdateView(with: false)
                    
                case .denied:
                    self.view?.onUpdateView(with: false)
                    
                case .authorized:
                    self.view?.onUpdateView(with: true)
                    
                case .provisional:
                    self.view?.onUpdateView(with: true)
                    
                case .ephemeral:
                    self.view?.onUpdateView(with: false)
                    
                @unknown default:
                    self.view?.onUpdateView(with: false)
                }
            }
        }
    }

    private func requestNotificationAuthorization() {
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (isSuccess, _) in
            DispatchQueue.main.async {
                self.view?.onUpdateView(with: isSuccess)
            }
        }
    }
    
    private func openAppSetting() {
        guard let settingURL = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settingURL) else {
            return
        }
        UIApplication.shared.open(settingURL, options: [:], completionHandler: nil)
    }
}

// MARK: - UIApplication
extension NotificationPresenter {
    func setInitUIApplication() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppWillEnterForeground), name: .applicationWillEnterForeground, object: nil)
    }
    
    @objc func onAppWillEnterForeground() {
        self.onCheckUpdateViewNotificationAuthorization()
    }
}
