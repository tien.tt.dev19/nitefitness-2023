//
//  
//  NotificationRouter.swift
//  1SK
//
//  Created by tuyenvx on 25/02/2021.
//
//

import UIKit

class NotificationRouter: BaseRouter {
    static func setupModule(pushNotiModel: PushNotiModel?) -> NotificationViewController {
        let viewController = NotificationViewController()
        let router = NotificationRouter()
        let interactorInput = NotificationInteractorInput()
        let presenter = NotificationPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        presenter.pushNotiModel = pushNotiModel
        interactorInput.output = presenter
        interactorInput.configService = ConfigService()
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - NotificationRouter: NotificationRouterProtocol -
extension NotificationRouter: NotificationRouterProtocol {
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailConsultingVC(appointment: AppointmentModel) {
        let controller = DetailConsultingRouter.setupModule(with: appointment)
        self.viewController?.show(controller, sender: nil)
    }
}
