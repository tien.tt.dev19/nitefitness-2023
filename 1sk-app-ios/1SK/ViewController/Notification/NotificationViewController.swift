//
//  
//  NotificationViewController.swift
//  1SK
//
//  Created by tuyenvx on 25/02/2021.
//
//

import UIKit
import Photos
import PhotosUI

class NotificationViewController: BaseViewController {
    var presenter: NotificationPresenterProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    @IBOutlet weak var constraint_height_WarningNotification: NSLayoutConstraint!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.onViewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.onViewDidAppear()
    }

    private func setupDefaults() {
        self.navigationItem.title = "Thông báo"
        self.setInitTableView()
        self.setInitRefreshControl()
    }

    // MARK: Action
    @IBAction func onEnableAuthNotificationAction(_ sender: Any) {
        self.presenter.onEnableAuthNotificationAction()
    }
}

// MARK: NotificationViewController - NotificationViewProtocol -
extension NotificationViewController: NotificationViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }

    func onUpdateView(with canReceiveNotification: Bool) {
        self.constraint_height_WarningNotification.constant = canReceiveNotification ? 0 : 80
        self.tbv_TableView.contentInset.top = canReceiveNotification ? 0 : 6
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
}

// MARK: Refresh Control
extension NotificationViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension NotificationViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewNotification.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewNotification.self, for: indexPath)
        cell.config(notification: self.presenter.cellForRow(at: indexPath))
        return cell
    }
}

// MARK: - UITableViewDataSource
extension NotificationViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRow(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.presenter.onDidEndDisplayingRow(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.presenter.onLoadMoreAction()
        }
    }
}
