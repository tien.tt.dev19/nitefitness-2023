//
//  AlbumTableViewCell.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//

import UIKit
import Photos

class AlbumTableViewCell: UITableViewCell {
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var nameLabelTraillingConstraint: NSLayoutConstraint!
    private lazy var imageWidth: CGFloat = {
        return 52 * UIScreen.main.nativeScale
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        albumImageView.isSkeletonable = true
        nameLabel.isSkeletonable = true
        countLabel.isSkeletonable = true
        checkImageView.isSkeletonable = true
        albumImageView.superview?.isSkeletonable = true
    }

    deinit {
        reset()
    }

    func config(with album: AlbumModel?, isSelected: Bool) {
        showSkeleton()
        guard let `album` = album else {
            return
        }
        if let asset = album.imageFetchResult?.firstObject {
            PHImageManager.default().requestImage(for: asset,
                                                  targetSize: CGSize(width: imageWidth, height: imageWidth),
                                                  contentMode: .aspectFill,
                                                  options: nil) { (image, _) in
                self.albumImageView.image = image
            }
        }
        nameLabel.text = album.name
        countLabel.text = album.count.toStringWithCommas()
        checkImageView.isHidden = !isSelected
        hideSkeleton()
    }

    func reset() {
        albumImageView.image = nil
        nameLabel.text = ""
        countLabel.text = ""
        checkImageView.isHidden = true
        nameLabelTraillingConstraint.constant = 16
    }
}
