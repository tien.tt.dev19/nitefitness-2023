//
//  PhotoCollectionViewCell.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//

import UIKit
import Photos

class PhotoPickerCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var unSelectedView: UIView!
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var indexLabel: UILabel!
    var assetIdentifier: String?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
    }

    func config(with asset: PHAsset?, selectedIndex: Int?, isCamera: Bool = false) {
        guard !isCamera else {
            imageView.isHidden = true
            unSelectedView.isHidden = true
            selectedView.isHidden = true
            return
        }
        assetIdentifier = asset?.localIdentifier
        imageView.isHidden = false
        unSelectedView.isHidden = selectedIndex != nil
        selectedView.isHidden = selectedIndex == nil
        indexLabel.text = String(selectedIndex ?? 0)
    }

    func updateSelectedLabel(with selectedIndex: Int?) {
        unSelectedView.isHidden = selectedIndex != nil
        selectedView.isHidden = selectedIndex == nil
        indexLabel.text = String(selectedIndex ?? 0)
    }

    func updateImage(_ image: UIImage?) {
        imageView.image = image
    }

    private func reset() {
        imageView.image = nil
        unSelectedView.isHidden = true
        selectedView.isHidden = true
    }
}
