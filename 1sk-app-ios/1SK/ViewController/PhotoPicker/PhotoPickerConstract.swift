//
//  
//  PhotoPickerConstract.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//
//

import UIKit
import Photos

// MARK: - View
protocol PhotoPickerViewProtocol: AnyObject {
    func reloadData()
    func updateUploadButtonState(isEnable: Bool)
    func setTableViewHidden(_ isHidden: Bool)
    func getCollectionViewVisibleRow() -> [IndexPath]
    func collectionViewReloadRow(at indexPaths: [IndexPath])
    func updateTitle(with title: String?)
    func updateSelectedNumber()
    func updateImage(at index: IndexPath, image: UIImage?)
    func updateCollectionView(removedIndexs: [IndexPath], insertedIndexs: [IndexPath], changedIndexs: [IndexPath], enumerateMoves: (@escaping (Int, Int) -> Void) -> Void)
    func tableViewReloadItem(at index: IndexPath)
}

// MARK: - Presenter
protocol PhotoPickerPresenterProtocol {
    var cachingImageManager: PHCachingImageManager! { get }
    
    func onViewDidLoad()
    
    // Action
    func onSelectAction()
    func onCloseAction()
    func onAlbumAction()
    func onBackgroundAction()
    
    func onPickerViewDidPickupImage(_ image: UIImage)
    
    // TableView
    func numberOfRowTable(in section: Int) -> Int
    func itemForRowTable(at index: IndexPath) -> AlbumModel?
    func isItemSelectedTable(at index: IndexPath) -> Bool
    
    func onDidSelectedRowTable(at index: IndexPath)
    
    // Collection
    func numberOfRowColl(in section: Int) -> Int
    func itemForRowColl(at index: IndexPath) -> PHAsset?
    func selectedIndexNumberColl(at index: IndexPath) -> Int?
    
    func onPrefetchItemColl(at indexPaths: [IndexPath])
    func onDidSelectItemColl(at index: IndexPath)
    
}

// MARK: - Interactor - Input
protocol PhotoPickerInteractorInputProtocol {
    func fetchAlbums()
    func fetchAllPhoto()
//    func uploadSelectedPhotos(_ selectedPhotos: [SelectedPhotoItem], cachingImageManager: PHCachingImageManager)
//    func uploadNewFiles(files: [FileUploadModel], to folder: FolderModel?, profileModel: ProfileModel?)
    func cancleUpload()
}

// MARK: - Interactor - Output
protocol PhotoPickerInteractorOutputProtocol: AnyObject {
    func onFetchAlbumsFinish(with result: PHFetchResult<PHAssetCollection>)
    func onFetchAllPhotoFinish(with result: PHFetchResult<PHAsset>)
//    func onUploadImageFinish(with result: Result<FileUploadModel, APIError>)
//    func onUploadSelectedImageComplete()
//    func onAddNewFileFinished(with result: Result<FileModel, APIError>)
//    func onAddAllFileCompleted()
}



// MARK: - Router
protocol PhotoPickerRouterProtocol: BaseRouterProtocol {
    func dismiss()
    func dismissSuperView()
    func showAlertViewController(type: AlertType, delegate: AlertViewControllerDelegate?)
    func showCameraImagePicker()
}
