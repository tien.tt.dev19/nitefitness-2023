//
//  
//  PhotoPickerInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//
//

import UIKit
import Photos

class PhotoPickerInteractorInput {
    weak var output: PhotoPickerInteractorOutputProtocol?
    var uploadService: UploadServiceProtocol?

    var options: PHFetchOptions {
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        return options
    }

}

// MARK: - PhotoPickerInteractorInput - PhotoPickerInteractorInputProtocol -
extension PhotoPickerInteractorInput: PhotoPickerInteractorInputProtocol {
    func fetchAlbums() {
        let albumsFetchResult = PHAssetCollection.fetchAssetCollections(with: .smartAlbum, subtype: .any, options: nil)
        self.output?.onFetchAlbumsFinish(with: albumsFetchResult)
    }

    func fetchAllPhoto() {
        let option = PHFetchOptions()
        option.sortDescriptors = [
            NSSortDescriptor(key: "creationDate", ascending: false)
        ]
        let allPhoto = PHAsset.fetchAssets(with: option)
        self.output?.onFetchAllPhotoFinish(with: allPhoto)
    }

    func cancleUpload() {
    }
}
