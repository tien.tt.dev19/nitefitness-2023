//
//  
//  PhotoPickerPresenter.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//
//

import UIKit
import Photos
//import RealmSwift

protocol PhotoPickerPresenterDelegate: AnyObject {
    func didSelectedPhotos(_ photos: [PHAsset])
}

class PhotoPickerPresenter: NSObject, CameraUsage, PhotoUsage {
    
    weak var view: PhotoPickerViewProtocol?
    private var interactor: PhotoPickerInteractorInputProtocol
    private var router: PhotoPickerRouterProtocol
    
    var listAlbums: [AlbumModel] = []
    var listPhotoSelected: [PHAsset] = [] {
        didSet {
            self.view?.updateUploadButtonState(isEnable: !self.listPhotoSelected.isEmpty)
        }
    }
    
    private var dispatchGroup = DispatchGroup()
    var currentPhotosNumber: Int = 0
    var isDismissSuperViewWhenClose: Bool!

    private var currentAlbumIndex = 0
    private var isShowingAlbumTableView = false
    private var maxSelectedImages: Int {
        return 8 - self.currentPhotosNumber
    }
    
    private lazy var cellWidth: CGFloat = {
        let numberOfItem = round(Constant.Screen.width / 118)
        let nativeScale = UIScreen.main.nativeScale
        return Constant.Screen.width * nativeScale / numberOfItem
    }()

    var cachingImageManager: PHCachingImageManager!
    var albumsFetchResult: PHFetchResult<PHAssetCollection>!
    var allPhotoFetchResult: PHFetchResult<PHAsset>!
    var albumAssetFetchResult: [PHFetchResult<PHAsset>] = []

    var options: PHFetchOptions {
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        return options
    }
    weak var delegate: PhotoPickerPresenterDelegate?

    init(interactor: PhotoPickerInteractorInputProtocol,
         router: PhotoPickerRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    // MARK: - Action
    private func fetchImage() {
        self.listAlbums = []
        
        self.dispatchGroup.enter()
        self.interactor.fetchAlbums()
        
        self.dispatchGroup.enter()
        self.interactor.fetchAllPhoto()
        
        self.dispatchGroup.notify(queue: .main) {
            self.onFetchImageFinish()
        }
    }

    private func onFetchImageFinish() {
        self.view?.reloadData()
    }

    private func savePhoto(image: UIImage) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.imageDidSaved(_:didFinishSavingWith:context:)), nil)
    }

    @objc func imageDidSaved(_ image: UIImage, didFinishSavingWith error: Error?, context: UnsafeRawPointer) {
    }

    private func registerPhotoObserver() {
        PHPhotoLibrary.shared().register(self)
    }
}

// MARK: - PhotoPickerPresenter
extension PhotoPickerPresenter: PhotoPickerPresenterProtocol {
    func onViewDidLoad() {
        self.fetchImage()
        self.registerPhotoObserver()
        self.view?.updateUploadButtonState(isEnable: !self.listPhotoSelected.isEmpty)
    }

    // MARK: Action
    func onSelectAction() {
        self.delegate?.didSelectedPhotos(self.listPhotoSelected)
        self.router.dismiss()
    }

    func onCloseAction() {
        !self.isDismissSuperViewWhenClose ? self.router.dismiss() : self.router.dismissSuperView()
    }

    func onAlbumAction() {
        self.isShowingAlbumTableView = !isShowingAlbumTableView
        self.view?.setTableViewHidden(!isShowingAlbumTableView)
    }

    func onBackgroundAction() {
        self.isShowingAlbumTableView = false
        self.view?.setTableViewHidden(!isShowingAlbumTableView)
    }

    func onPickerViewDidPickupImage(_ image: UIImage) {
        self.checkAbilityForPhotoUsage(grantedHandler: { [weak self] in
            self?.savePhoto(image: image)
        }, notGrantedHandler: { [weak self] in
            self?.router.showAlertViewController(type: .photoUsage, delegate: nil)
        })
    }
    
    // MARK: UITableView
    func numberOfRowTable(in section: Int) -> Int {
        return self.listAlbums.count
    }

    func itemForRowTable(at index: IndexPath) -> AlbumModel? {
        return self.listAlbums[index.row]
    }

    func isItemSelectedTable(at index: IndexPath) -> Bool {
        return index.row == self.currentAlbumIndex
    }

    func onDidSelectedRowTable(at index: IndexPath) {
        print("xxxx currentAlbumIndex:", currentAlbumIndex)
        self.currentAlbumIndex = index.row
        self.view?.reloadData()
        self.view?.setTableViewHidden(true)
        self.view?.updateTitle(with: self.listAlbums[currentAlbumIndex].name)
    }

    // MARK: UICollectionView
    func numberOfRowColl(in section: Int) -> Int {
        if self.listAlbums.count > self.currentAlbumIndex {
            return self.listAlbums[self.currentAlbumIndex].count + 1
            
        } else {
            return 0
        }
    }

    func itemForRowColl(at index: IndexPath) -> PHAsset? {
        guard index.row != 0 else {
            return nil
        }
        print("xxxx ccc currentAlbumIndex:", currentAlbumIndex)
        let asset = self.listAlbums[currentAlbumIndex].imageFetchResult?[index.row - 1]
        return asset
    }

    func selectedIndexNumberColl(at index: IndexPath) -> Int? {
        guard let asset = self.itemForRowColl(at: index) else {
            return nil
        }
        if let index = self.listPhotoSelected.firstIndex(where: { $0 == asset }) {
            return index + 1
        } else {
            return nil
        }
    }

    func onPrefetchItemColl(at indexPaths: [IndexPath]) {
        let assets = indexPaths.compactMap { (indexPath) -> PHAsset? in
            guard indexPath.row != 0 else {
                return nil
            }
            return self.listAlbums[currentAlbumIndex].imageFetchResult?[indexPath.row - 1]
        }
        self.cachingImageManager.startCachingImages(for: assets, targetSize: CGSize(width: self.cellWidth, height: self.cellWidth), contentMode: .aspectFill, options: nil)
    }

    func onDidSelectItemColl(at index: IndexPath) {
        if index.row == 0 { // Select Camera cell
            self.checkAbilityForUsageCamera(grantedHandler: { [weak self] in
                self?.router.showCameraImagePicker()
                
            }, notGrantedHandler: { [weak self] authorization in
                self?.router.showAlertViewController(type: .cameraUsage, delegate: nil)
            })
            
        } else {
            let currentAlbum = self.listAlbums[self.currentAlbumIndex]
            guard let asset = currentAlbum.imageFetchResult?[index.row - 1] else {
                print("onDidSelectItemColl return asset nil 1")
                return
            }

            if let index = self.listPhotoSelected.firstIndex(where: { $0 == asset}) {
                self.listPhotoSelected.remove(at: index)
                print("onDidSelectItemColl index: ", index)
                
            } else {
                guard self.listPhotoSelected.count < self.maxSelectedImages else {
                    SKToast.shared.showToast(content: "Bạn chỉ được chọn tối đa 8 ảnh")
                    return
                }
                guard let asset = currentAlbum.imageFetchResult?[index.row - 1] else {
                    print("onDidSelectItemColl return asset nil 2")
                    return
                }
                
                self.listPhotoSelected.append(asset)
                
                let options = PHImage.optionsPhotoImageRequest()
                
                self.cachingImageManager.allowsCachingHighQualityImages = true
                
                self.cachingImageManager.startCachingImages(for: [asset], targetSize: PHImage.targetSize, contentMode: PHImage.contentMode, options: options)
                
                print("onDidSelectItemColl picker asset:", asset.description)
            }
            
            self.view?.updateSelectedNumber()
        }
    }
}

// MARK: - PhotoPickerInteractorOutput
extension PhotoPickerPresenter: PhotoPickerInteractorOutputProtocol {
    
    /// Get Image In System
    func onFetchAlbumsFinish(with result: PHFetchResult<PHAssetCollection>) {
        self.albumsFetchResult = result
        result.enumerateObjects { [weak self] (album, _, _) in
            let fetchResult = PHAsset.fetchAssets(in: album, options: self?.options)
            if fetchResult.count > 0 {
                let albumModel = AlbumModel(album: album, fetchResult: fetchResult)
                self?.listAlbums.append(albumModel)
            }
        }
        self.dispatchGroup.leave()
        
        print("onFetchAllPhotoFinish xx listAlbums.count:", listAlbums.count)
    }

    func onFetchAllPhotoFinish(with result: PHFetchResult<PHAsset>) {
        
        self.allPhotoFetchResult = result
        let allPhotoAlbum = AlbumModel(album: nil, fetchResult: result)
        self.listAlbums.insert(allPhotoAlbum, at: 0)
        self.dispatchGroup.leave()
        
        print("onFetchAllPhotoFinish listAlbums.count:", listAlbums.count)
    }
    
    /// API - Backend
    
//
//    func onUploadImageFinish(with result: Result<FileUploadModel, APIError>) {
//        switch result {
//        case .success(let fileUpload):
//            fileUploads.append(fileUpload)
//        case .failure:
//            SKToast.shared.showToast()
//            router.hideHud()
//            interactor.cancleUpload()
//        }
//    }
//
//    func onUploadSelectedImageComplete() {
//        interactor.uploadNewFiles(files: fileUploads, to: uploadedFolder, profileModel: profile)
//    }
//
//    func onAddNewFileFinished(with result: Result<FileModel, APIError>) {
//        switch result {
//        case .success(let file):
//            if let fileListObject = fileListDAO.getObject(with: uploadedFolder?.id ?? "") {
//                let fileList = FileListModel(value: fileListObject)
//                fileList.files.append(file)
//                fileListDAO.add(fileList)
//            } else {
//                let fileList = FileListModel(profielID: profile?.id ?? "", folderID: uploadedFolder?.id ?? "", files: [file])
//                fileListDAO.add(fileList)
//            }
//            fileUploads.removeAll(where: { $0.fileDownloadUri == file.url })
//        case .failure:
//            SKToast.shared.showToast()
//            router.hideHud()
//            interactor.cancleUpload()
//        }
//    }
//
//    func onAddAllFileCompleted() {
//        SKToast.shared.showToast(content: "Đã cập nhật hồ sơ")
//        router.hideHud()
//        router.dismiss()
//    }
}

// MARK: - PHPhotoLibraryChangeObserver
extension PhotoPickerPresenter: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        DispatchQueue.main.async {
            if let albumChange = changeInstance.changeDetails(for: self.albumsFetchResult) {
                self.albumsFetchResult = albumChange.fetchResultAfterChanges
                self.fetchImage()
            }

            if let changes = changeInstance.changeDetails(for: self.allPhotoFetchResult) {
                self.allPhotoFetchResult = changes.fetchResultAfterChanges
                let allPhotoAlbum = AlbumModel(album: nil, fetchResult: self.allPhotoFetchResult)
                self.listAlbums[0] = allPhotoAlbum
                let allPhotoIndexPath = IndexPath(row: 0, section: 0)
                self.view?.tableViewReloadItem(at: allPhotoIndexPath)
                guard self.currentAlbumIndex == 0 else {
                    return
                }
                self.updateCollectionView(with: changes)
            }

            for (index, fetchResult) in self.albumAssetFetchResult.enumerated() {
                if let changes = changeInstance.changeDetails(for: fetchResult) {
                    self.albumAssetFetchResult[index] = changes.fetchResultAfterChanges
                    self.listAlbums[index + 1].imageFetchResult = changes.fetchResultAfterChanges
                    let albumIndexPath = IndexPath(row: index + 1, section: 0)
                    self.view?.tableViewReloadItem(at: albumIndexPath)
                    guard self.currentAlbumIndex == index + 1 else {
                        return
                    }
                    self.updateCollectionView(with: changes)
                }
            }
        }
    }

    private func updateCollectionView(with changes: PHFetchResultChangeDetails<PHAsset>) {
        var removeIndexs: [IndexPath] = []
        var insertedIndexs: [IndexPath] = []
        var changedIndexs: [IndexPath] = []
        if let removedIndexSet = changes.removedIndexes {
            removeIndexs = removedIndexSet.map({ return IndexPath(row: $0 + 1, section: 0)})
        }

        if let insertedIndexSet = changes.insertedIndexes {
            insertedIndexs = insertedIndexSet.map({ return IndexPath(row: $0 + 1, section: 0)})
        }

        if let changedIndexSet = changes.changedIndexes {
            changedIndexs = changedIndexSet.map({ return IndexPath(row: $0 + 1, section: 0)})
        }
        self.view?.updateCollectionView(removedIndexs: removeIndexs,
                                        insertedIndexs: insertedIndexs,
                                        changedIndexs: changedIndexs,
                                        enumerateMoves: changes.enumerateMoves)
    }

}
