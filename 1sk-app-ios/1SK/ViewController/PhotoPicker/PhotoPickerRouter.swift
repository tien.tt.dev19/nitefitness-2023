//
//  
//  PhotoPickerRouter.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//
//

import UIKit
import Photos

class PhotoPickerRouter: BaseRouter {
    static func setupModule(with delegate: PhotoPickerPresenterDelegate?,
                            cachingImageManager: PHCachingImageManager,
                            selectedPhotos: [PHAsset],
                            currentPhotoNumber: Int = 0,
                            isDismissSuperViewWhenClose: Bool = false) -> PhotoPickerViewController {
        let viewController = PhotoPickerViewController()
        let router = PhotoPickerRouter()
        let interactorInput = PhotoPickerInteractorInput()
        let presenter = PhotoPickerPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.delegate = delegate
        presenter.cachingImageManager = cachingImageManager
        presenter.isDismissSuperViewWhenClose = isDismissSuperViewWhenClose
        presenter.listPhotoSelected = selectedPhotos
        presenter.currentPhotosNumber = currentPhotoNumber
        interactorInput.output = presenter
        interactorInput.uploadService = UploadService()
        router.viewController = viewController
        viewController.modalPresentationStyle = .fullScreen
        return viewController
    }
}

// MARK: - PhotoPickerRouter: PhotoPickerRouterProtocol -
extension PhotoPickerRouter: PhotoPickerRouterProtocol {
    func dismiss() {
        viewController?.dismiss(animated: true, completion: nil)
    }

    func dismissSuperView() {
        viewController?.presentingViewController?.dismiss(animated: true, completion: nil)
    }

    func showAlertViewController(type: AlertType, delegate: AlertViewControllerDelegate?) {
        let alertVC = AlertViewController.loadFromNib()
        alertVC.modalPresentationStyle = .overFullScreen
        alertVC.delegate = delegate
        alertVC.type = type
        viewController?.present(alertVC, animated: false, completion: nil)
    }

    func showCameraImagePicker() {
        let pickerController = UIImagePickerController()
        if let `viewController` = viewController as? PhotoPickerViewController {
            pickerController.delegate = viewController
        }
        pickerController.allowsEditing = false
        pickerController.sourceType = .camera
        viewController?.present(pickerController, animated: true, completion: nil)
    }
}
