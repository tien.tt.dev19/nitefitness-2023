//
//  
//  PhotoPickerViewController.swift
//  1SK
//
//  Created by tuyenvx on 11/03/2021.
//
//

import UIKit

class PhotoPickerViewController: BaseViewController {
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var tbv_TableViewAlbum: UITableView!
    @IBOutlet weak var coll_CollectionViewPhoto: UICollectionView!
    
    @IBOutlet weak var btn_Upload: UIButton!
    
    @IBOutlet weak var view_HideAlbum: UIView!
    @IBOutlet weak var constraint_height_TableViewAlbum: NSLayoutConstraint!
    
    var presenter: PhotoPickerPresenterProtocol!
    
    private let tableViewCellHeight: CGFloat = 73
    
    private lazy var collectionViewCellSize: CGSize = {
        let screenWidth = Constant.Screen.width
        let numberOfItemPerRow = (screenWidth / 118).rounded(.down)
        let width = ((screenWidth - 5.5 * 2 - 5.5 * (numberOfItemPerRow - 1)) / numberOfItemPerRow).rounded(.down)
        return CGSize(width: width, height: width)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupDefaults() {
        self.setInitView()
        self.setInitUITableView()
        self.setInitUICollectionView()
        
        self.updateUploadButtonState(isEnable: false)
    }

    func setInitView() {
        self.btn_Upload.superview?.addShadow(width: 0, height: -5, color: R.color.black()!, radius: 4, opacity: 0.09)
    }
    
    private func caculateTableViewHeight() -> CGFloat {
        let tableViewHeight = CGFloat(presenter.numberOfRowTable(in: 0)) * tableViewCellHeight
        let maxHeight = view.safeAreaLayoutGuide.layoutFrame.height - 44 - 64
        return min(tableViewHeight, maxHeight)
    }
    
    // MARK: - IBAction
    @IBAction func onCloseAction(_ sender: Any) {
        self.presenter.onCloseAction()
    }

    @IBAction func onAlbumAction(_ sender: Any) {
        self.presenter.onAlbumAction()
    }

    @IBAction func onUploadAction(_ sender: Any) {
        self.presenter.onSelectAction()
    }

    @IBAction func onTapBackgroundAction(_ sender: Any) {
        self.presenter.onBackgroundAction()
    }
}

// MARK: PhotoPickerViewController - PhotoPickerViewProtocol -
extension PhotoPickerViewController: PhotoPickerViewProtocol {
    func reloadData() {
        self.tbv_TableViewAlbum.reloadData()
        self.coll_CollectionViewPhoto.reloadData()
    }

    func updateUploadButtonState(isEnable: Bool) {
        self.btn_Upload?.isEnabled = isEnable
        self.btn_Upload?.backgroundColor = isEnable ? R.color.mainColor() : UIColor(hex: "C4C4C4")
    }

    func setTableViewHidden(_ isHidden: Bool) {
        self.constraint_height_TableViewAlbum.constant = isHidden ? 0 : caculateTableViewHeight()
        self.view_HideAlbum.isHidden = isHidden
        UIView.animate(withDuration: Constant.Number.animationTime) {
            self.view.layoutIfNeeded()
        }
    }

    func getCollectionViewVisibleRow() -> [IndexPath] {
        return self.coll_CollectionViewPhoto.indexPathsForVisibleItems
    }

    func collectionViewReloadRow(at indexPaths: [IndexPath]) {
        self.coll_CollectionViewPhoto.reloadItems(at: indexPaths)
    }

    func updateSelectedNumber() {
        for indexPath in self.coll_CollectionViewPhoto.indexPathsForVisibleItems {
            let selectedIndex = self.presenter.selectedIndexNumberColl(at: indexPath)
            if let cell = self.coll_CollectionViewPhoto.cellForItem(at: indexPath) as? PhotoPickerCollectionViewCell {
                cell.updateSelectedLabel(with: selectedIndex)
            }
        }
    }

    func updateImage(at index: IndexPath, image: UIImage?) {
            if let cell = self.coll_CollectionViewPhoto.cellForItem(at: index) as? PhotoPickerCollectionViewCell {
                cell.updateImage(image)
            }
    }

    func updateTitle(with title: String?) {
        self.lbl_Title.text = title
    }

    func updateCollectionView(removedIndexs: [IndexPath], insertedIndexs: [IndexPath], changedIndexs: [IndexPath], enumerateMoves: (@escaping (Int, Int) -> Void) -> Void) {
        self.coll_CollectionViewPhoto.performBatchUpdates({ [weak self] in
            self?.coll_CollectionViewPhoto.deleteItems(at: removedIndexs)
            self?.coll_CollectionViewPhoto.insertItems(at: insertedIndexs)
            self?.coll_CollectionViewPhoto.reloadItems(at: changedIndexs)
            enumerateMoves { (fromIndex, toIndex) in
                self?.coll_CollectionViewPhoto.moveItem(at: IndexPath(row: fromIndex + 1, section: 0), to: IndexPath(row: toIndex + 1, section: 0))
            }
        }, completion: { _ in

        })
    }

    func tableViewReloadItem(at index: IndexPath) {
        self.tbv_TableViewAlbum.reloadRows(at: [index], with: .none)
    }

}

// MARK: - UITableView DataSource
extension PhotoPickerViewController: UITableViewDataSource {
    private func setInitUITableView() {
        self.tbv_TableViewAlbum.registerNib(ofType: AlbumTableViewCell.self)
        self.tbv_TableViewAlbum.delegate = self
        self.tbv_TableViewAlbum.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRowTable(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: AlbumTableViewCell.self, for: indexPath)
        let item = self.presenter.itemForRowTable(at: indexPath)
        cell.config(with: item, isSelected: self.presenter.isItemSelectedTable(at: indexPath))
        return cell
    }
}

// MARK: - UITableView Delegate
extension PhotoPickerViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.onDidSelectedRowTable(at: indexPath)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableViewCellHeight
    }
}

// MARK: - UICollectionView DataSource
extension PhotoPickerViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_CollectionViewPhoto.registerNib(ofType: PhotoPickerCollectionViewCell.self)
        self.coll_CollectionViewPhoto.delegate = self
        self.coll_CollectionViewPhoto.dataSource = self
        self.coll_CollectionViewPhoto.contentInset = UIEdgeInsets(top: 5.5, left: 5.5, bottom: 155.5, right: 5.5)
        self.coll_CollectionViewPhoto.prefetchDataSource = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.presenter.numberOfRowColl(in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: PhotoPickerCollectionViewCell.self, for: indexPath)
        
        let item = self.presenter.itemForRowColl(at: indexPath)
        cell.config(with: item, selectedIndex: self.presenter.selectedIndexNumberColl(at: indexPath), isCamera: indexPath.row == 0)
        
        if let asset = item {
            let numberOfItem = round(Constant.Screen.width / 118)
            let nativeScale = UIScreen.main.nativeScale
            let imageWidth = Constant.Screen.width * nativeScale / numberOfItem
            let size = CGSize(width: imageWidth, height: imageWidth)

            self.presenter.cachingImageManager.requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: nil) { (image, _) in
                if cell.assetIdentifier == asset.localIdentifier {
                    cell.updateImage(image)
                }
            }
        }
        return cell
    }

}

// MARK: - UICollectionView FlowLayout
extension PhotoPickerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter.onDidSelectItemColl(at: indexPath)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.collectionViewCellSize
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5.5
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5.5
    }
}

// MARK: - UICollectionView PrefetchDataSource
extension PhotoPickerViewController: UICollectionViewDataSourcePrefetching {
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        self.presenter.onPrefetchItemColl(at: indexPaths)
    }
}

/// Khi chụp ảnh
// MARK: - UIImagePickerControllerDelegate
extension PhotoPickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("imagePickerController return")
            return
        }
        print("imagePickerController selectedImage", selectedImage.description)
        self.presenter.onPickerViewDidPickupImage(selectedImage)
    }
}
