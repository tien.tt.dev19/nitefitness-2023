//
//  
//  HealthActivityConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/19/21.
//
//

import UIKit

// MARK: - View
protocol HealthActivityViewProtocol: AnyObject {
    func onReloadData()
    func onJoinRoomActive(videoCall: VideoCallModel?)
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?)
}

// MARK: - Presenter
protocol HealthActivityPresenterProtocol {
    func onViewDidLoad()
    
    /// Action
    func onRefreshAction()
    func onSeeMoreMyAppointment()
    func onJoinRoomCallAction(appointment: AppointmentModel)
    
    /// UITableView
    func numberOfRows() -> Int
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel
    func onDidSelectRow(at indexPath: IndexPath)
    
    /// Get Value
    func getTotalAppointment() -> Int
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?)
}

// MARK: - Interactor Input
protocol HealthActivityInteractorInputProtocol {
    func getListAppointment()
    func getTokenAppointmentRoom(appointment: AppointmentModel)
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel)
}

// MARK: - Interactor Output
protocol HealthActivityInteractorOutputProtocol: AnyObject {
    func onGetListAppointmentFinished(with result: Result<[AppointmentModel], APIError>, total: Int?)
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel)
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?)
}

// MARK: - Router
protocol HealthActivityRouterProtocol: BaseRouterProtocol {
    func gotoMyAppointmentVC()
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?)
    func alertDefault(title: String, message: String)
}
