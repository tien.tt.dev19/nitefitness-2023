//
//  
//  HealthActivityInteractorInput.swift
//  1SK
//
//  Created by vuongbachthu on 7/19/21.
//
//

import UIKit

class HealthActivityInteractorInput {
    weak var output: HealthActivityInteractorOutputProtocol?
    var careService: CareServiceProtocol?
}

// MARK: - HealthActivityInteractorInputProtocol
extension HealthActivityInteractorInput: HealthActivityInteractorInputProtocol {
    func getListAppointment() {
        let listStatus = [AppointmentStatus.paying, AppointmentStatus.confirming, AppointmentStatus.confirmed]
        self.careService?.getListAppointments(isComing: true,
                                              isPats: false,
                                              page: 1,
                                              pageLimit: 2,
                                              orderByTime: "ASC",
                                              listStatus: listStatus,
                                              completion: { [weak self] result in
            let total = result.getTotal()
            self?.output?.onGetListAppointmentFinished(with: result.unwrapSuccessModel(), total: total)
        })
    }
    
    func getTokenAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta, appointment: appointment)
        })
    }
    
    func getTokenRenewAppointmentRoom(appointment: AppointmentModel) {
        self.careService?.getTokenRenewAppointmentRoom(appointment: appointment, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onGetTokenRenewAppointmentRoomFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
}
