//
//  
//  HealthActivityPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/19/21.
//
//

import UIKit

class HealthActivityPresenter {

    weak var view: HealthActivityViewProtocol?
    private var interactor: HealthActivityInteractorInputProtocol
    private var router: HealthActivityRouterProtocol

    init(interactor: HealthActivityInteractorInputProtocol,
         router: HealthActivityRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

    var listAppointment: [AppointmentModel] = []
    var totalAppointment = 0
    
    func getDataListAppointment() {
        self.router.showHud()
        self.interactor.getListAppointment()
    }
}

// MARK: - HealthActivityPresenterProtocol
extension HealthActivityPresenter: HealthActivityPresenterProtocol {
    func onViewDidLoad() {
        self.getDataListAppointment()
        self.setInitHandleSocketEvent()
    }
    
    /// Action
    func onRefreshAction() {
        self.getDataListAppointment()
    }
    
    func onSeeMoreMyAppointment() {
        self.router.gotoMyAppointmentVC()
    }
    
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.router.showHud()
        self.interactor.getTokenAppointmentRoom(appointment: appointment)
    }
    
    //MARK: UITableView
    func numberOfRows() -> Int {
        return self.listAppointment.count
    }
    
    func cellForRow(at indexPath: IndexPath) -> AppointmentModel {
        return self.listAppointment[indexPath.row]
    }
    
    func onDidSelectRow(at indexPath: IndexPath) {
        guard self.listAppointment.count > 0 else {
            return
        }
        let appointment = self.listAppointment[indexPath.row]
        self.router.gotoDetailAppointmentVC(appointment: appointment, delegate: self)
    }
    
    /// Get Value
    func getTotalAppointment() -> Int {
        return self.totalAppointment
    }
    
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        guard let `appointment` = appointment else {
            return
        }
        self.router.showHud()
        self.interactor.getTokenRenewAppointmentRoom(appointment: appointment)
    }
}

// MARK: - HealthActivityInteractorOutput 
extension HealthActivityPresenter: HealthActivityInteractorOutputProtocol {
    func onGetListAppointmentFinished(with result: Result<[AppointmentModel], APIError>, total: Int?) {
        switch result {
        case .success(let listModel):
            self.listAppointment = listModel
            self.totalAppointment = total ?? 0
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.router.hideHud()
    }
    
    func onGetTokenAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?, appointment: AppointmentModel) {
        if meta?.code == 10002 {
            self.view?.onShowAlertRenewToken(meta: meta, appointment: appointment)
            
        } else {
            switch result {
            case .success(let model):
                let room = model
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
            case .failure(let error):
                let room = VideoCallModel()
                room.meta = meta
                self.view?.onJoinRoomActive(videoCall: room)
                
                self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
            }
        }
        self.router.hideHud()
    }
    
    func onGetTokenRenewAppointmentRoomFinished(with result: Result<VideoCallModel, APIError>, meta: Meta?) {
        switch result {
        case .success(let model):
            let room = model
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
        case .failure(let error):
            let room = VideoCallModel()
            room.meta = meta
            self.view?.onJoinRoomActive(videoCall: room)
            
            self.router.alertDefault(title: "Có lỗi xảy ra", message: "Mã lỗi: \(error.statusCode) \n\(error.message)")
        }
        self.router.hideHud()
    }
}

// MARK: - DetailAppointmentPresenterDelegate
extension HealthActivityPresenter: DetailAppointmentPresenterDelegate {
    func onCancelAppointmentSuccess(appointment: AppointmentModel) {
        self.view?.onReloadData()
    }
    
    func onAppointmentChangeStatus(appointment: AppointmentModel) {
        self.view?.onReloadData()
    }
}


// MARK:
extension HealthActivityPresenter {
    func setInitHandleSocketEvent() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentCreatedEvent), name: .AppointmentCreatedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentStatusChangedEvent), name: .AppointmentStatusChangedEvent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onAppointmentShowButtonJoinEvent), name: .AppointmentShowButtonJoinEvent, object: nil)
    }
    
    @objc func onAppointmentCreatedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentCreatedEvent HealthActivityPresenter")
        self.getDataListAppointment()
    }
    
    @objc func onAppointmentStatusChangedEvent(notification: NSNotification) {
        print("socket.on care: AppointmentCreatedEvent HealthActivityPresenter")
        self.getDataListAppointment()
    }
    
    @objc func onAppointmentShowButtonJoinEvent(notification: NSNotification) {
        print("socket.on care: AppointmentShowButtonJoinEvent HealthActivityPresenter")
        
    }
}
