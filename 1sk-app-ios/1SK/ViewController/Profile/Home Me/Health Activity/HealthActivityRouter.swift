//
//  
//  HealthActivityRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/19/21.
//
//

import UIKit

class HealthActivityRouter: BaseRouter {
//    weak var viewController: HealthActivityViewController?
    static func setupModule() -> HealthActivityViewController {
        let viewController = HealthActivityViewController()
        let router = HealthActivityRouter()
        let interactorInput = HealthActivityInteractorInput()
        let presenter = HealthActivityPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        interactorInput.careService = CareService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - HealthActivityRouterProtocol
extension HealthActivityRouter: HealthActivityRouterProtocol {
    func gotoMyAppointmentVC() {
        let controller = MyAppointmentRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func gotoDetailAppointmentVC(appointment: AppointmentModel?, delegate: DetailAppointmentPresenterDelegate?) {
        let controller = DetailAppointmentRouter.setupModule(width: appointment, delegate: delegate, isFromBooking: nil)
        self.viewController?.show(controller, sender: nil)
    }
    
    func alertDefault(title: String, message: String) {
        self.viewController?.alertDefault(title: title, message: message)
    }
}
