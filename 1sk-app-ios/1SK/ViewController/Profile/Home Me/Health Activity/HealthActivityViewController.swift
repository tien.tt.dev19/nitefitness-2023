//
//  
//  HealthActivityViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/19/21.
//
//

import UIKit
import Photos

class HealthActivityViewController: BaseViewController {

    @IBOutlet weak var tbv_TableView: UITableView!
    
    var presenter: HealthActivityPresenterProtocol!
    private var refreshControl: UIRefreshControl?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaults()
        presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.onReloadData()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitTableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - HealthActivityViewProtocol
extension HealthActivityViewController: HealthActivityViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onJoinRoomActive(videoCall: VideoCallModel?) {
        if videoCall?.appId != nil && videoCall?.channelName != nil && videoCall?.token != nil {
            self.showJoinRoomVideoCallView(videoCall: videoCall!)
            
        } else {
            self.onShowAlertWarningAppointment(videoCall: videoCall)
        }
    }
    
    func onShowAlertRenewToken(meta: Meta?, appointment: AppointmentModel?) {
        self.showAlertRenewTokenCallViewController(title: "Cuộc gọi tư vấn", content: meta?.message ?? "Bạn đang tham gia tư vấn online bằng thiết bị khác", appointment: appointment, delegate: self)
    }
}

// MARK: AlertRenewTokenCallViewDelegate
extension HealthActivityViewController: AlertRenewTokenCallViewDelegate {
    func onAgreeAlertRenewTokenCallAction(appointment: AppointmentModel?) {
        self.presenter.onAgreeAlertRenewTokenCallAction(appointment: appointment)
    }
}

// MARK: Refresh Control
extension HealthActivityViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.presenter.onRefreshAction()
        return
    }
}

// MARK: - UITableViewDataSource
extension HealthActivityViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewAppointment.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderCareAppointment.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewNoData.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let count = self.presenter.getTotalAppointment()
        let header = tableView.dequeueHeaderView(ofType: SectionHeaderCareAppointment.self)
        header.delegate = self
        header.setStateHeader(countAppointment: count)
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.presenter.numberOfRows()
        if count > 0 {
            return count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let count = self.presenter.numberOfRows()
        if count > 0 {
            let cell = tableView.dequeueCell(ofType: CellTableViewAppointment.self, for: indexPath)
            cell.delegate = self
            cell.config(appointment: self.presenter.cellForRow(at: indexPath))
            return cell
        } else {
            let cell = tableView.dequeueCell(ofType: CellTableViewNoData.self, for: indexPath)
            return cell
        }
    }
}

// MARK: - UITableViewDataSource
extension HealthActivityViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.onDidSelectRow(at: indexPath)
    }
}

// MARK: - SectionHeaderCareAppointmentDelegate
extension HealthActivityViewController: SectionHeaderCareAppointmentDelegate {
    func onSeeMoreMyAppointment() {
        self.presenter.onSeeMoreMyAppointment()
    }
}

// MARK: - CellTableViewAppointmentDelegate
extension HealthActivityViewController: CellTableViewAppointmentDelegate {
    func onJoinRoomCallAction(appointment: AppointmentModel) {
        self.onCheckAuthorizationStatus(appointment: appointment)
    }
}

// MARK: - AVCaptureDevice
extension HealthActivityViewController {
    func onCheckAuthorizationStatus(appointment: AppointmentModel) {
        guard AVCaptureDevice.authorizationStatus(for: .video) == .authorized &&  AVCaptureDevice.authorizationStatus(for: .audio) == .authorized else {
            self.onCheckAuthorizationVideo(appointment: appointment)
            return
        }
        self.presenter.onJoinRoomCallAction(appointment: appointment)
    }
    
    func onCheckAuthorizationVideo(appointment: AppointmentModel) {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.onCheckAuthorizationAudio(appointment: appointment)
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    //access allowed
                    self.onCheckAuthorizationAudio(appointment: appointment)
                    
                } else {
                    //access denied
                    if AVCaptureDevice.authorizationStatus(for: .video) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Camera để có thể tiếp tục")
                        }
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
                    }
                }
            })
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Camera",
                message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi video trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Camera")
        }
    }
    
    func onCheckAuthorizationAudio(appointment: AppointmentModel) {
        switch AVCaptureDevice.authorizationStatus(for: .audio) {
        case .authorized:
            DispatchQueue.main.async {
                self.presenter.onJoinRoomCallAction(appointment: appointment)
            }
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .audio) { granted in
                if granted == true {
                   // User granted
                    DispatchQueue.main.async {
                        self.presenter.onJoinRoomCallAction(appointment: appointment)
                    }
                    
                } else {
                    // User denied
                    if AVCaptureDevice.authorizationStatus(for: .audio) == .denied {
                        DispatchQueue.main.async {
                            SKToast.shared.showToast(content: "Chức năng này yêu cầu quyền truy cập Microphone để có thể tiếp tục")
                        }
                        
                    } else {
                        self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
                        message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
                    }
                }
            }
            
        default:
            self.alertOpenSettingsURLString(title: "1SK chưa được cấp quyền truy cập Microphone",
            message: "\nNó sẽ giúp bạn có thể thực hiện cuộc gọi trong ứng dụng. \nVui lòng vào Cài đặt > 1SK > Microphone")
        }
    }
}

// MARK: - VideoCallViewDelegate
extension HealthActivityViewController: AgoraVideoCallViewDelegate, TwilioVideoCallViewDelegate {
    func showJoinRoomVideoCallView(videoCall: VideoCallModel) {
        switch videoCall.typeVideoCall {
        case .agora:
            let controller = AgoraVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        case .twilio:
            let controller = TwilioVideoCallViewController()
            controller.modalPresentationStyle = .overFullScreen
            controller.delegate = self
            controller.videoCall = videoCall
            self.navigationController?.present(controller, animated: true, completion: nil)
            
        default:
            SKToast.shared.showToast(content: "Chức năng Video Call không hoạt động")
        }
    }
    
    func onEndCallAction(videoCall: VideoCallModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if let id = videoCall?.appointment?.id {
                self.onShowAlertRateConsultationView(appointmentId: id)
            }
        }
    }
}
