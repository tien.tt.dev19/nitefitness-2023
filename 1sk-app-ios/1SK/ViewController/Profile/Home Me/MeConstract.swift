//
//  
//  MeConstract.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

// MARK: View -
protocol MeViewProtocol: AnyObject {
    func onSetDataUser(user: UserModel?)
}

// MARK: Interactor -
protocol MeInteractorInputProtocol {
    
}

protocol MeInteractorOutputProtocol: AnyObject {
    
}

// MARK: Presenter -
protocol MePresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func onSettingAction()
    func onTapGestureUserAction()
}

// MARK: Router -
protocol MeRouterProtocol: BaseRouterProtocol {
    func showSettingViewController()
    func gotoAccountViewController(delegate: UpdateProfileViewDelegate?)
}
