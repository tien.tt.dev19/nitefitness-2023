//
//  
//  MePresenter.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class MePresenter {

    weak var view: MeViewProtocol?
    private var interactor: MeInteractorInputProtocol
    private var router: MeRouterProtocol
    
    init(interactor: MeInteractorInputProtocol,
         router: MeRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
}

// MARK: - MePresenterProtocol
extension MePresenter: MePresenterProtocol {
    
    func onViewDidLoad() {
        
    }

    func onViewDidAppear() {
        self.view?.onSetDataUser(user: gUser)
    }
    
    // Action
    func onSettingAction() {
        self.router.showSettingViewController()
    }
    
    func onTapGestureUserAction() {
        self.router.gotoAccountViewController(delegate: self)
    }
    
}

// MARK: - MePresenter: MeInteractorOutput -
extension MePresenter: MeInteractorOutputProtocol {
    
}

// MARK: UpdateProfilePresenterDelegate
extension MePresenter: UpdateProfileViewDelegate {
    func onSaveUserInfoChangeSuccess() {
        self.view?.onSetDataUser(user: gUser)
    }
}
