//
//  
//  MeRouter.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class MeRouter: BaseRouter {
    static func setupModule() -> MeViewController {
        let viewController = MeViewController()
        let router = MeRouter()
        let interactorInput = MeInteractorInput()
        let presenter = MePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - MeRouterProtocol
extension MeRouter: MeRouterProtocol {
    func showSettingViewController() {
        let setingVC = SettingRouter.setupModule()
        self.viewController?.navigationController?.pushViewController(setingVC, animated: true)
    }
    
    func gotoAccountViewController(delegate: UpdateProfileViewDelegate?) {
        let controller = UpdateProfileRouter.setupModule(with: delegate)
        self.viewController?.present(controller, animated: true)
    }
}
