//
//  
//  MeViewController.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class MeViewController: BaseViewController {
    
    
    @IBOutlet weak var view_User: UIView!
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var view_PageMenu: UIView!
    
    var presenter: MePresenterProtocol!
    
    private lazy var pageViewController: VXPageViewController = VXPageViewController()
    
    var pageStoreActivity: StoreActivityViewController!
    var pageHealthActivity: HealthActivityViewController!
    var pageStravaActivity: StravaActivityViewController!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitPageViewController()
        self.setInitGestureView()
    }
    
    // MARK: - Action
    @IBAction func onSettingAction(_ sender: Any) {
        self.presenter.onSettingAction()
    }
}

// MARK: - Gesture Recognizer
extension MeViewController {
    func setInitGestureView() {
        self.view_User.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureUserAction)))
    }
    
    @objc func onTapGestureUserAction() {
        self.presenter.onTapGestureUserAction()
    }
}

// MARK: - MeViewProtocol
extension MeViewController: MeViewProtocol {
    func onSetDataUser(user: UserModel?) {
        self.img_Avatar.setImageWith(imageUrl: user?.avatar ?? "", placeHolder: R.image.ic_default_avatar())
        if let fullName = user?.fullName, fullName != "" {
            self.lbl_Name.text = fullName
            self.lbl_Name.textColor = .black
            
        } else {
            self.lbl_Name.text = "Họ tên chưa cập nhật"
            self.lbl_Name.textColor = .darkGray
        }
    }
}

// MARK: VX Page Bar
extension MeViewController {
    private func setInitPageViewController() {
        let barConfig = VXPageBarConfig(height: 48,
                                      selectedColor: R.color.darkText(),
                                      unSelectedColor: R.color.subTitle(),
                                      selectedFont: R.font.robotoMedium(size: 16),
                                      unSelectedFont: R.font.robotoRegular(size: 16),
                                      underLineHeight: 3,
                                      underLineWidth: 60,
                                      underLineColor: R.color.mainColor(),
                                      backgroundColor: .white)
        
        self.pageStravaActivity = StravaActivityRouter.setupModule()
        let pageStrava = VXPageItem(viewController: self.pageStravaActivity, title: "Tập luyện")
        
        self.pageStoreActivity = StoreActivityRouter.setupModule()
        let pageStore = VXPageItem(viewController: self.pageStoreActivity, title: "Cửa hàng")
        
        self.pageHealthActivity = HealthActivityRouter.setupModule()
        let pageHealth = VXPageItem(viewController: self.pageHealthActivity, title: "Tư vấn")
        
//        self.pageViewController.setPageItems([pageStrava, pageStore, pageHealth])
        self.pageViewController.setPageItems([pageStore])
        self.pageViewController.setPageBarConfig(barConfig)
        
        self.addChild(self.pageViewController)
        self.view_PageMenu.addSubview(self.pageViewController.view)
        self.pageViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        self.pageViewController.didMove(toParent: self)
    }
}
