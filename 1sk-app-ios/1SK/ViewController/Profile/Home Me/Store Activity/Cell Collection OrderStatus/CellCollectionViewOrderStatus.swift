//
//  CellCollectionViewOrderStatus.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//

import UIKit

class CellCollectionViewOrderStatus: UICollectionViewCell {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var view_Badge: UIView!
    @IBOutlet weak var lbl_BadgeCount: UILabel!
    
    var model: OrderStatusModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    override var isSelected: Bool {
        didSet {
            self.setSelected(isSelected: self.isSelected)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    private func setDataUI() {
        guard let model = self.model else { return }
        self.lbl_Name.text = model.status?.name
        self.setSelected(isSelected: model.isSelected ?? false)
        
        if model.ordersCount ?? 0 > 0 {
            self.view_Badge.isHidden = false
            self.lbl_BadgeCount.text = model.ordersCount?.asString
            
        } else {
            self.view_Badge.isHidden = true
            self.lbl_BadgeCount.text = "0"
        }
    }
    
    func setSelected(isSelected: Bool) {
        if isSelected {
            self.lbl_Name.textColor = .white
            self.lbl_Name.font = R.font.robotoRegular(size: 14)
            self.view_Bg.borderWidth = 0
            self.view_Bg.borderColor = .none
            self.view_Bg.backgroundColor = R.color.mainColor()
            
        } else {
            self.lbl_Name.textColor = .lightGray
            self.lbl_Name.font = R.font.robotoRegular(size: 14)
            self.view_Bg.borderWidth = 0.5
            self.view_Bg.borderColor = .lightGray
            self.view_Bg.backgroundColor = .white
        }
    }
}
