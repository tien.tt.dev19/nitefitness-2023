//
//  FooterSectionOrderView.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//

import UIKit

protocol FooterSectionOrderViewDelegate: AnyObject {
    func onRatetingAction(order: OrderModel?)
    func onRatetedAction(order: OrderModel?)
    func onDetailAction(order: OrderModel?)
}

class FooterSectionOrderView: UITableViewHeaderFooterView {

    @IBOutlet weak var btn_Rateting: UIButton_0!
    @IBOutlet weak var btn_Rateted: UIButton_0!
    @IBOutlet weak var view_None: UIView!
    
    weak var delegate: FooterSectionOrderViewDelegate?
    
    var model: OrderModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func setDataUI() {
        if self.model?.statusOrder == OrderStatus.completed {
            if (self.model?.items?.first(where: {$0.isReview == true})) != nil {
                self.btn_Rateting.isHidden = true
                self.btn_Rateted.isHidden = false
                self.view_None.isHidden = true
                
            } else {
                self.btn_Rateting.isHidden = false
                self.btn_Rateted.isHidden = true
                self.view_None.isHidden = true
            }
            
        } else {
            self.btn_Rateting.isHidden = true
            self.btn_Rateted.isHidden = true
            self.view_None.isHidden = false
        }
    }
    
    @IBAction func onRatetingAction(_ sender: Any) {
        self.delegate?.onRatetingAction(order: self.model)
    }
    
    @IBAction func onRatetedAction(_ sender: Any) {
        self.delegate?.onRatetedAction(order: self.model)
    }

    @IBAction func onDetailAction(_ sender: Any) {
        self.delegate?.onDetailAction(order: self.model)
    }
    
}
