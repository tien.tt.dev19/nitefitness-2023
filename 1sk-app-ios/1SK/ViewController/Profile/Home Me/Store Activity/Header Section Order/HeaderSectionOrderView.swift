//
//  HeaderSectionOrderView.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//

import UIKit

class HeaderSectionOrderView: UITableViewHeaderFooterView {

    @IBOutlet weak var view_Status: UIView!
    @IBOutlet weak var lbl_Status: UILabel!
    
    @IBOutlet weak var lbl_Ordercode: UILabel!
    
    var model: OrderModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    private func setDataUI() {
        guard let model = self.model else { return }
        self.lbl_Ordercode.text = "#\(model.code ?? "")"
        self.lbl_Status.text = model.statusOrder?.name ?? "null"
        
        switch model.statusOrder {
        case .pending:
            self.lbl_Status.textColor = UIColor(hex: "FEBF07")
            self.view_Status.backgroundColor = UIColor(hex: "FEBF07").withAlphaComponent(0.1)
            
        case .processing:
            self.lbl_Status.textColor = UIColor(hex: "046FBC")
            self.view_Status.backgroundColor = UIColor(hex: "046FBC").withAlphaComponent(0.1)
            
        case .completed:
            self.lbl_Status.textColor = UIColor(hex: "27C500")
            self.view_Status.backgroundColor = UIColor(hex: "27C500").withAlphaComponent(0.1)
            
        case .canceled:
            self.lbl_Status.textColor = UIColor(hex: "F22323")
            self.view_Status.backgroundColor = UIColor(hex: "F22323").withAlphaComponent(0.1)
            
        default:
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = R.color.mainColor()
        }
    }
}
