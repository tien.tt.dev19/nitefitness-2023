//
//  HeaderSectionViewOrderDelivery.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//

import UIKit

class HeaderSectionViewOrderDelivery: UITableViewHeaderFooterView {
    @IBOutlet weak var view_Status: UIView!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_OrderCode: UILabel!
    
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_Email: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    
    var model: OrderModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    private func setDataUI() {
        guard let model = self.model else { return }
        self.lbl_OrderCode.text = "#\(model.code ?? "")"
        self.lbl_Status.text = model.statusOrder?.name ?? "null"
        
        self.lbl_UserName.text = model.address?.name
        self.lbl_Phone.text = model.address?.phone
        self.lbl_Email.text = model.address?.email ?? "Không có email"
        self.lbl_Address.text = model.address?.address
        if let name = model.address?.district?.name {
            self.lbl_Address.text = "\(self.lbl_Address.text ?? ""), \(name)"
        }
        if let name = model.address?.city?.name {
            self.lbl_Address.text = "\(self.lbl_Address.text ?? ""), \(name)"
        }
        if let name = model.address?.state?.name {
            self.lbl_Address.text = "\(self.lbl_Address.text ?? ""), \(name)"
        }
        
        switch model.statusOrder {
        case .pending:
            self.lbl_Status.textColor = UIColor(hex: "FEBF07")
            self.view_Status.backgroundColor = UIColor(hex: "FEBF07").withAlphaComponent(0.1)
            
        case .processing:
            self.lbl_Status.textColor = UIColor(hex: "046FBC")
            self.view_Status.backgroundColor = UIColor(hex: "046FBC").withAlphaComponent(0.1)
            
        case .completed:
            self.lbl_Status.textColor = UIColor(hex: "27C500")
            self.view_Status.backgroundColor = UIColor(hex: "27C500").withAlphaComponent(0.1)
            
        case .canceled:
            self.lbl_Status.textColor = UIColor(hex: "F22323")
            self.view_Status.backgroundColor = UIColor(hex: "F22323").withAlphaComponent(0.1)
            
        default:
            self.lbl_Status.textColor = .white
            self.view_Status.backgroundColor = R.color.mainColor()
        }
        
    }
}
