//
//  HeaderSectionViewOrderPayment.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//

import UIKit

protocol HeaderSectionViewOrderPaymentDelegate: AnyObject {
    func onCancelOrderAction()
}

class HeaderSectionViewOrderPayment: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_PriceCost: UILabel!
    @IBOutlet weak var lbl_PriceShipping: UILabel!
    @IBOutlet weak var lbl_PriceDiscount: UILabel!
    @IBOutlet weak var lbl_PriceTotal: UILabel!
    
    @IBOutlet weak var view_CancelOrder: UIView!
    @IBOutlet weak var btn_CancelOrder: UIButton!
    
    weak var delegate: HeaderSectionViewOrderPaymentDelegate?
    var model: OrderModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    private func setDataUI() {
        guard let model = self.model else { return }
        
        self.lbl_PriceCost.text = "\(model.subTotal?.formatNumber() ?? "0")đ"
        self.lbl_PriceShipping.text = "\(model.shippingAmount?.formatNumber() ?? "0")đ"
        self.lbl_PriceDiscount.text = "-\(model.discountAmount?.formatNumber() ?? "0")đ"
        self.lbl_PriceTotal.text = "\(model.amount?.formatNumber() ?? "0")đ"
        
        if model.statusOrder == .pending {
            self.view_CancelOrder.isHidden = false
            
        } else {
            self.view_CancelOrder.isHidden = true
        }
    }
    
    @IBAction func onCancelOrderAction(_ sender: Any) {
        self.delegate?.onCancelOrderAction()
    }
}
