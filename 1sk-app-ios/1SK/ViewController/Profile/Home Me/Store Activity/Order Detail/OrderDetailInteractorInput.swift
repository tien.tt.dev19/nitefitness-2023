//
//  
//  OrderDetailInteractorInput.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol OrderDetailInteractorInputProtocol {
    func setCancelOrder(orderId: Int)
}

// MARK: - Interactor Output Protocol
protocol OrderDetailInteractorOutputProtocol: AnyObject {
    func onSetCancelOrderFinished(with result: Result<OrderModel, APIError>)
}

// MARK: - OrderDetail InteractorInput
class OrderDetailInteractorInput {
    weak var output: OrderDetailInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - OrderDetail InteractorInputProtocol
extension OrderDetailInteractorInput: OrderDetailInteractorInputProtocol {
    func setCancelOrder(orderId: Int) {
        self.storeService?.setCancelOrder(orderId: orderId, completion: { [weak self] result in
            self?.output?.onSetCancelOrderFinished(with: result.unwrapSuccessModel())
        })
    }
    

}
