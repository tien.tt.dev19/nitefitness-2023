//
//  
//  OrderDetailRouter.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol OrderDetailRouterProtocol {
    func onPopToViewController()
    func onPopToRootViewController()
}

// MARK: - OrderDetail Router
class OrderDetailRouter {
    weak var viewController: OrderDetailViewController?
    
    static func setupModule(order: OrderModel?, delegate: OrderDetailViewDelegate?, isCreateOrder: Bool?) -> OrderDetailViewController {
        let viewController = OrderDetailViewController()
        let router = OrderDetailRouter()
        let interactorInput = OrderDetailInteractorInput()
        let viewModel = OrderDetailViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.order = order
        viewModel.isCreateOrder = isCreateOrder
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - OrderDetail RouterProtocol
extension OrderDetailRouter: OrderDetailRouterProtocol {
    func onPopToViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func onPopToRootViewController() {
        self.viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
