//
//  
//  OrderDetailViewController.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

protocol OrderDetailViewDelegate: AnyObject {
    func onCancelOrderSuccess(order: OrderModel?)
}

// MARK: - ViewProtocol
protocol OrderDetailViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onCancelOrderSuccess(order: OrderModel?)
}

// MARK: - OrderDetail ViewController
class OrderDetailViewController: BaseViewController {
    var router: OrderDetailRouterProtocol!
    var viewModel: OrderDetailViewModelProtocol!
    weak var delegate: OrderDetailViewDelegate?
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.setInitUITableView()
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        let isCreateOrder = self.viewModel.isCreateOrderValue
        if isCreateOrder == true {
            self.router.onPopToRootViewController()
        } else {
            self.router.onPopToViewController()
        }
    }
}

// MARK: - OrderDetail ViewProtocol
extension OrderDetailViewController: OrderDetailViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onCancelOrderSuccess(order: OrderModel?) {
        self.delegate?.onCancelOrderSuccess(order: order)
    }
}

// MARK: UITableViewDataSource
extension OrderDetailViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionViewOrderDelivery.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionViewOrderProduct.self)
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionViewOrderPayment.self)
        
        self.tbv_TableView.registerNib(ofType: CellTableViewOrderProduct.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionViewOrderDelivery.self)
            header.model = self.viewModel.getOrder()
            return header
            
        case 1:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionViewOrderProduct.self)
            return header
            
        case 2:
            let header = tableView.dequeueHeaderView(ofType: HeaderSectionViewOrderPayment.self)
            header.delegate = self
            header.model = self.viewModel.getOrder()
            return header
            
        default:
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return self.viewModel.numberOfRows(in: section)
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewOrderProduct.self, for: indexPath)
        cell.config(item: self.viewModel.cellForRow(at: indexPath))
        
        let count = self.viewModel.numberOfRows(in: indexPath.section) - 1
        if indexPath.row == count {
            cell.view_LineBottom.isHidden = true
        } else {
            cell.view_LineBottom.isHidden = false
        }
        
        return cell
    }
    
}

// MARK: HeaderSectionViewOrderPaymentDelegate
extension OrderDetailViewController: HeaderSectionViewOrderPaymentDelegate {
    func onCancelOrderAction() {
        self.presentAlertPopupConfirmTitleView()
    }
}

// MARK: AlertPopupConfirmTitleViewDelegate
extension OrderDetailViewController: AlertPopupConfirmTitleViewDelegate {
    func presentAlertPopupConfirmTitleView() {
        let controller = AlertPopupConfirmTitleViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        controller.lblTitle = "Hủy đơn hàng"
        controller.lblContent = "Bạn có chắc chắn muốn hủy đơn?"
        controller.btnTitleCancel = "Không hủy"
        controller.btnTitleAgree = "Đồng ý"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirmCancelAction(object: Any?) {
        //
    }
    
    func onAlertPopupConfirmAgreeAction(object: Any?) {
        self.viewModel.onCancelOrderAction()
    }
}

// MARK: - AlertConfirmActionViewDelegate
//extension OrderDetailViewController: AlertSheetConfirmIconViewDelegate {
//    func showAlertConfirmActionView() {
//        let controller = AlertSheetConfirmIconViewController()
//        controller.alertContent = "Bạn có chắc chắn muốn hủy đơn?"
//        controller.alertImage = R.image.ic_calendar_cancel()
//        controller.btnTitleCancel = "Trở lại"
//        controller.btnTitleAgree = "Đồng ý"
//
//        controller.modalPresentationStyle = .custom
//        controller.delegate = self
//        self.navigationController?.present(controller, animated: true)
//    }
//
//    func onAlertConfirmCancelAction() {
//        //
//    }
//
//    func onAlertConfirmAgreeAction() {
//        self.viewModel.onCancelOrderAction()
//    }
//}
