//
//  
//  OrderDetailViewModel.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol OrderDetailViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onCancelOrderAction()
    
    // UITableView
    func numberOfRows(in section: Int) -> Int
    func cellForRow(at indexPath: IndexPath) -> ItemOrder?
    func getOrder() -> OrderModel?
    
    // Value
    var isCreateOrderValue: Bool { get }
}

// MARK: - OrderDetail ViewModel
class OrderDetailViewModel {
    weak var view: OrderDetailViewProtocol?
    private var interactor: OrderDetailInteractorInputProtocol

    init(interactor: OrderDetailInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var order: OrderModel?
    var isCreateOrder: Bool?
}

// MARK: - OrderDetail ViewModelProtocol
extension OrderDetailViewModel: OrderDetailViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    // Action
    func onCancelOrderAction() {
        guard let orderId = self.order?.id else {
            return
        }
        self.view?.showHud()
        self.interactor.setCancelOrder(orderId: orderId)
    }
    
    // UITableView
    func numberOfRows(in section: Int) -> Int {
        return self.order?.items?.count ?? 0
    }
    
    func cellForRow(at indexPath: IndexPath) -> ItemOrder? {
        return self.order?.items?[indexPath.row]
    }
    
    func getOrder() -> OrderModel? {
        return self.order
    }
    
    // Value
    var isCreateOrderValue: Bool {
        return self.isCreateOrder ?? false
    }
}

// MARK: - OrderDetail InteractorOutputProtocol
extension OrderDetailViewModel: OrderDetailInteractorOutputProtocol {
    func onSetCancelOrderFinished(with result: Result<OrderModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Hủy đơn thành công")
            self.order = model
            self.view?.onReloadData()
            self.view?.onCancelOrderSuccess(order: self.order)
            
        case .failure(let error):
            SKToast.shared.showToast(content: "Không thể hủy đơn hàng")
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
