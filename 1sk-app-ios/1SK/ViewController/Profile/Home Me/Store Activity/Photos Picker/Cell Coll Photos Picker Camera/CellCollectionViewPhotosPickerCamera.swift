//
//  CellCollectionViewPhotosPickerCamera.swift
//  1SK
//
//  Created by Thaad on 23/05/2022.
//

import UIKit

protocol CellCollectionViewPhotosPickerCameraDelegate: AnyObject {
    func onCaptureAction()
}

class CellCollectionViewPhotosPickerCamera: UICollectionViewCell {

    weak var delegate: CellCollectionViewPhotosPickerCameraDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func onCaptureAction(_ sender: Any) {
        self.delegate?.onCaptureAction()
    }
    
}
