//
//  CellCollectionViewPhotosPickerImage.swift
//  1SK
//
//  Created by Thaad on 23/05/2022.
//

import UIKit

protocol CellCollectionViewPhotosPickerImageDelegate: AnyObject {
    func onSelectImageAction()
}

class CellCollectionViewPhotosPickerImage: UICollectionViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    
    @IBOutlet weak var view_Selected: UIView!
    @IBOutlet weak var lbl_Index: UILabel!
    
    @IBOutlet weak var btn_Selected: UIButton!
    
    weak var delegate: CellCollectionViewPhotosPickerImageDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        self.view_Selected.isHidden = true
    }

    @IBAction func onSelectImageAction(_ sender: Any) {
        self.btn_Selected.isSelected = !self.btn_Selected.isSelected
        
        if self.btn_Selected.isSelected == true {
            self.view_Selected.isHidden = false
        } else {
            self.view_Selected.isHidden = true
        }
        
        self.delegate?.onSelectImageAction()
    }
}
