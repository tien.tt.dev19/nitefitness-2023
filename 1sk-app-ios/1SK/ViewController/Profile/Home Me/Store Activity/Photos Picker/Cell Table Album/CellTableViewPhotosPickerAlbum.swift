//
//  CellTableViewPhotosPickerAlbum.swift
//  1SK
//
//  Created by Thaad on 23/05/2022.
//

import UIKit

protocol CellTableViewPhotosPickerAlbumDelegate: AnyObject {
    
}

class CellTableViewPhotosPickerAlbum: UITableViewCell {

    weak var delegate: CellTableViewPhotosPickerAlbumDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
