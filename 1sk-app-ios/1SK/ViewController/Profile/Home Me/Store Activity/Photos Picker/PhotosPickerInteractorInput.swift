//
//  
//  PhotosPickerInteractorInput.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol PhotosPickerInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol PhotosPickerInteractorOutputProtocol: AnyObject {
    
}

// MARK: - PhotosPicker InteractorInput
class PhotosPickerInteractorInput {
    weak var output: PhotosPickerInteractorOutputProtocol?
}

// MARK: - PhotosPicker InteractorInputProtocol
extension PhotosPickerInteractorInput: PhotosPickerInteractorInputProtocol {

}
