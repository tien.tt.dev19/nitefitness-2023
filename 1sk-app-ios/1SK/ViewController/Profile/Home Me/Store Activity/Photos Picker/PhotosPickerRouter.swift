//
//  
//  PhotosPickerRouter.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol PhotosPickerRouterProtocol {

}

// MARK: - PhotosPicker Router
class PhotosPickerRouter {
    weak var viewController: PhotosPickerViewController?
    
    static func setupModule() -> PhotosPickerViewController {
        let viewController = PhotosPickerViewController()
        let router = PhotosPickerRouter()
        let interactorInput = PhotosPickerInteractorInput()
        let viewModel = PhotosPickerViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - PhotosPicker RouterProtocol
extension PhotosPickerRouter: PhotosPickerRouterProtocol {
    
}
