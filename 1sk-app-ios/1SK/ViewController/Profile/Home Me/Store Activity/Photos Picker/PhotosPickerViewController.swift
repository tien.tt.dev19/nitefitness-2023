//
//  
//  PhotosPickerViewController.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol PhotosPickerViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    //func onReloadData()
}

// MARK: - PhotosPicker ViewController
class PhotosPickerViewController: BaseViewController {
    var router: PhotosPickerRouterProtocol!
    var viewModel: PhotosPickerViewModelProtocol!
    
    @IBOutlet weak var btn_Close: UIButton!
    @IBOutlet weak var btn_Select: UIButton!
    @IBOutlet weak var btn_Album: UIButton!
    
    @IBOutlet weak var tbv_Album: UITableView!
    @IBOutlet weak var coll_Photos: UICollectionView!
    
    @IBOutlet weak var constraint_height_TableAlbum: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitUICollectionView()
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func onSelectAction(_ sender: Any) {
        
    }
    
    @IBAction func onAlbumAction(_ sender: Any) {
        
    }
}

// MARK: - PhotosPicker ViewProtocol
extension PhotosPickerViewController: PhotosPickerViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: UICollectionViewDataSource
extension PhotosPickerViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_Album.registerNib(ofType: CellTableViewPhotosPickerAlbum.self)
        
        self.tbv_Album.delegate = self
        self.tbv_Album.dataSource = self
        
        self.constraint_height_TableAlbum.constant = 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewPhotosPickerAlbum.self, for: indexPath)
        
        return cell
    }
}

// MARK: UICollectionViewDataSource
extension PhotosPickerViewController: UITableViewDelegate {
    
}

// MARK: UICollectionViewDataSource
extension PhotosPickerViewController: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_Photos.registerNib(ofType: CellCollectionViewPhotosPickerCamera.self)
        self.coll_Photos.registerNib(ofType: CellCollectionViewPhotosPickerImage.self)
        
        self.coll_Photos.delegate = self
        self.coll_Photos.dataSource = self
        
        let width: CGFloat = UIScreen.main.bounds.size.width/3 - 1
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        self.coll_Photos.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewPhotosPickerCamera.self, for: indexPath)
            return cell
            
        default:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewPhotosPickerImage.self, for: indexPath)
            
            
            
            return cell
        }
    }
}

// MARK: UICollectionViewDataSource
extension PhotosPickerViewController: UICollectionViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
}
