//
//  
//  PhotosPickerViewModel.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol PhotosPickerViewModelProtocol {
    func onViewDidLoad()
    //func onViewDidAppear()
    
    // UITableView
    //func numberOfSections() -> Int
    //func numberOfRows() -> Int
    //func cellForRow(at indexPath: IndexPath) -> Any?
    //func didSelectRow(at indexPath: IndexPath)
    
    // UICollectionView
    //func numberOfItems() -> Int
    //func cellForItem(at indexPath: IndexPath) -> Any
    //func didSelectItem(at indexPath: IndexPath)
}

// MARK: - PhotosPicker ViewModel
class PhotosPickerViewModel {
    weak var view: PhotosPickerViewProtocol?
    private var interactor: PhotosPickerInteractorInputProtocol

    init(interactor: PhotosPickerInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - PhotosPicker ViewModelProtocol
extension PhotosPickerViewModel: PhotosPickerViewModelProtocol {
    func onViewDidLoad() {
        
    }
}

// MARK: - PhotosPicker InteractorOutputProtocol
extension PhotosPickerViewModel: PhotosPickerInteractorOutputProtocol {

}
