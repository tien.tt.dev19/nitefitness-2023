//
//  CellTableViewReviewsComment.swift
//  1SK
//
//  Created by Thaad on 25/05/2022.
//

import UIKit
import GrowingTextView

class CellTableViewReviewsComment: UITableViewCell {

    @IBOutlet weak var tv_Comment: GrowingTextView!
    
    var model: ItemOrder? {
        didSet {
            self.setInitDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUITextView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setInitDataUI() {
        self.tv_Comment.placeholder = self.model?.rateType?.placeholderText
        self.tv_Comment.text = self.model?.review?.comment
        //self.tv_Comment.text = self.model?.comment ?? self.model?.product?.reviews?.first?.comment
        
        if self.model?.isReview == true {
            self.tv_Comment.isEditable = false
        }
    }
}

// MARK: UITextViewDelegate
extension CellTableViewReviewsComment: UITextViewDelegate {
    func setInitUITextView() {
        self.tv_Comment.delegate = self
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.model?.comment = self.tv_Comment.text
    }
}
