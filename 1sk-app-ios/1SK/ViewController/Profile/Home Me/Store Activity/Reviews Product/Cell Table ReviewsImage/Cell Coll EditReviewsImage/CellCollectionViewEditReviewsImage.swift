//
//  CellCollectionViewEditReviewsImage.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//

import UIKit

protocol CellCollectionViewEditReviewsImageDelegate: AnyObject {
    func onRemoveImageAction(indexPath: IndexPath?)
    func onAddImageAction()
}

class CellCollectionViewEditReviewsImage: UICollectionViewCell {

    @IBOutlet weak var view_Image: UIView!
    @IBOutlet weak var view_Camera: UIView!
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Count: UILabel!
    
    weak var delegate: CellCollectionViewEditReviewsImageDelegate?
    var indexPath: IndexPath?
    
    var isStateCamera = false {
        didSet {
            self.setUIStateView()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setUIStateView()
    }

    func setUIStateView() {
        if self.isStateCamera == true {
            self.view_Image.isHidden = true
            self.view_Camera.isHidden = false
            
        } else {
            self.view_Image.isHidden = false
            self.view_Camera.isHidden = true
        }
        
    }
    
    @IBAction func onRemoveAction(_ sender: Any) {
        self.delegate?.onRemoveImageAction(indexPath: self.indexPath)
    }
    
    @IBAction func onAddAction(_ sender: Any) {
        self.delegate?.onAddImageAction()
    }
}
