//
//  CellCollectionViewShowReviewsImage.swift
//  1SK
//
//  Created by Thaad on 24/05/2022.
//

import UIKit

class CellCollectionViewShowReviewsImage: UICollectionViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    
    var imageUrl: String? {
        didSet {
            self.img_Image.setImageWith(imageUrl: self.imageUrl ?? "")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
