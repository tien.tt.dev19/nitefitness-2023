//
//  CellTableViewReviewsImage.swift
//  1SK
//
//  Created by Thaad on 26/05/2022.
//

import UIKit

protocol CellTableViewReviewsImageDelegate: AnyObject {
    func onAddImageAction(indexPath: IndexPath?)
}

class CellTableViewReviewsImage: UITableViewCell {

    @IBOutlet weak var view_AddImage: UIView!
    @IBOutlet weak var view_SelectImage: UIView!
    
    @IBOutlet weak var coll_Photos: UICollectionView!
    @IBOutlet weak var constraint_height_CollPhotos: NSLayoutConstraint!
    
    weak var delegate: CellTableViewReviewsImageDelegate?
    
    var indexPath: IndexPath?
    var model: ItemOrder? {
        didSet {
            self.setInitDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUICollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setInitDataUI() {
        if self.model?.isReview == true {
            let width: CGFloat = (UIScreen.main.bounds.size.width - 64) / 3
            let layout = UICollectionViewFlowLayout()
            layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
            layout.itemSize = CGSize(width: width, height: width)
            layout.minimumLineSpacing = 8
            layout.minimumInteritemSpacing = 8
            self.coll_Photos.collectionViewLayout = layout
            self.constraint_height_CollPhotos.constant = width + 8
        }
        
        self.setUIImageState()
        self.coll_Photos.reloadData()
    }
    
    func setUIImageState() {
        if self.model?.isReview == true {
            self.view_AddImage.isHidden = true
            self.view_SelectImage.isHidden = false

        } else {
            if self.model?.imageReviews?.count ?? 0 > 1 {
                self.view_AddImage.isHidden = true
                self.view_SelectImage.isHidden = false

            } else {
                self.view_AddImage.isHidden = false
                self.view_SelectImage.isHidden = true
            }
        }
    }
    
    @IBAction func onAddImageAction(_ sender: Any) {
        self.delegate?.onAddImageAction(indexPath: self.indexPath)
    }
}

// MARK: UICollectionViewDataSource
extension CellTableViewReviewsImage: UICollectionViewDataSource, UICollectionViewDelegate {
    func setInitUICollectionView() {
        self.coll_Photos.registerNib(ofType: CellCollectionViewEditReviewsImage.self)
        self.coll_Photos.registerNib(ofType: CellCollectionViewShowReviewsImage.self)
        
        self.coll_Photos.delegate = self
        self.coll_Photos.dataSource = self
        
        var width: CGFloat = 60
        if self.model?.isReview == true {
            width = (UIScreen.main.bounds.size.width - 64) / 3
        }
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.itemSize = CGSize(width: width, height: width)
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        self.coll_Photos.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.model?.isReview == true {
            return self.model?.review?.images?.count ?? 0
            //return self.model?.product?.reviews?.first?.images?.count ?? 0
            
        } else {
            return self.model?.imageReviews?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.model?.isReview == true {
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewShowReviewsImage.self, for: indexPath)
            cell.imageUrl = self.model?.review?.images?[indexPath.row]
            //cell.imageUrl = self.model?.product?.reviews?.first?.images?[indexPath.row]
            return cell
            
        } else {
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewEditReviewsImage.self, for: indexPath)
            cell.delegate = self
            cell.indexPath = indexPath
            
            if let image = self.model?.imageReviews?[indexPath.row] {
                cell.img_Image.image = image
            }
            
            if let count = self.model?.imageReviews?.count, count > 1 {
                if indexPath.row == count - 1 {
                    cell.isStateCamera = true
                    cell.lbl_Count.text = "\(count - 1)/3"
                } else {
                    cell.isStateCamera = false
                    cell.lbl_Count.text = "0/3"
                }
            } else {
                cell.isStateCamera = false
                cell.lbl_Count.text = "0/3"
            }
            
            return cell
        }
    }
}

// MARK: CellCollectionViewEditReviewsImageDelegate
extension CellTableViewReviewsImage: CellCollectionViewEditReviewsImageDelegate {
    func onRemoveImageAction(indexPath: IndexPath?) {
        guard let index = indexPath?.row else {
            return
        }
        self.model?.imageReviews?.remove(at: index)
        self.coll_Photos.reloadData()
        
        self.setUIImageState()
    }
    
    func onAddImageAction() {
        guard let count = self.model?.imageReviews?.count, count < 4 else {
            return
        }
        self.delegate?.onAddImageAction(indexPath: self.indexPath)
    }
}
