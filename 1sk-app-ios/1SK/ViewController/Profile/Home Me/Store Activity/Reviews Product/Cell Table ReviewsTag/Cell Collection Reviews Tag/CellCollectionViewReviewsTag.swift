//
//  CellCollectionViewReviewsTag.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//

import UIKit

class CellCollectionViewReviewsTag: UICollectionViewCell {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_Content: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUIStateView(isSelected: Bool) {
        if isSelected {
            self.view_Bg.backgroundColor = R.color.color_Bg_Green()
            self.view_Bg.borderColor = R.color.mainColor()
            self.lbl_Content.textColor = R.color.mainColor()
            
        } else {
            self.view_Bg.backgroundColor = R.color.line_gray_01()?.withAlphaComponent(0.5)
            self.view_Bg.borderColor = .lightGray.withAlphaComponent(0.5)
            self.lbl_Content.textColor = R.color.subTitle()
        }
    }

}
