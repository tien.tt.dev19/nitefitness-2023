//
//  CellTableViewReviewsProduct.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//

import UIKit

protocol CellTableViewReviewsTagDelegate: AnyObject {
    func setHeightForCellReviewsTag(height: CGFloat, indexPath: IndexPath?)
}

class CellTableViewReviewsTag: UITableViewCell {

    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    weak var delegate: CellTableViewReviewsTagDelegate?
    
    var indexPath: IndexPath?
    var model: ItemOrder? {
        didSet {
            self.coll_CollectionView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let heightColl = self.coll_CollectionView.contentSize.height
                self.delegate?.setHeightForCellReviewsTag(height: heightColl, indexPath: self.indexPath)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUICollectionView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

// MARK: UICollectionViewDataSource
extension CellTableViewReviewsTag: UICollectionViewDataSource {
    func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewReviewsTag.self)
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
        
        let layout = UICollectionViewLayoutCustom()
        layout.alignment = .left
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.estimatedItemSize = CGSize(width: 128, height: 36)
        self.coll_CollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.model?.rateComments?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewReviewsTag.self, for: indexPath)
        cell.lbl_Content.text = self.model?.rateComments?[indexPath.row].comment
        cell.setUIStateView(isSelected: self.model?.rateComments?[indexPath.row].isSelected ?? false)
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension CellTableViewReviewsTag: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isSelected = self.model?.rateComments?[indexPath.row].isSelected ?? false
        self.model?.rateComments?[indexPath.row].isSelected = !isSelected
        
        self.coll_CollectionView.reloadData()
    }
}
