//
//  
//  ReviewsProductInteractorInput.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ReviewsProductInteractorInputProtocol {
    func setReviewProduct(itemOrder: ItemOrder, orderId: Int?)
    func getMyReviewProduct(listProductId: [Int], orderId: Int)
}

// MARK: - Interactor Output Protocol
protocol ReviewsProductInteractorOutputProtocol: AnyObject {
    func onSetReviewProductFinished(with result: Result<ItemOrder, APIError>, itemId: Int?)
    func onGetMyReviewProductFinished(with result: Result<[ProductOrderModel], APIError>)
}

// MARK: - ReviewsProduct InteractorInput
class ReviewsProductInteractorInput {
    weak var output: ReviewsProductInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - ReviewsProduct InteractorInputProtocol
extension ReviewsProductInteractorInput: ReviewsProductInteractorInputProtocol {
    func setReviewProduct(itemOrder: ItemOrder, orderId: Int?) {
        self.storeService?.setReviewProduct(itemOrder: itemOrder, orderId: orderId, completion: { [weak self] result in
            self?.output?.onSetReviewProductFinished(with: result.unwrapSuccessModel(), itemId: itemOrder.id)
        })
    }
    
    func getMyReviewProduct(listProductId: [Int], orderId: Int) {
        self.storeService?.getMyReviewProduct(listProductId: listProductId, orderId: orderId, completion: { [weak self] result in
            self?.output?.onGetMyReviewProductFinished(with: result.unwrapSuccessModel())
        })
    }
}
