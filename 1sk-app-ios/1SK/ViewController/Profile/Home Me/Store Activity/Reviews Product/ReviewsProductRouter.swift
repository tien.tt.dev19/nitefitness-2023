//
//  
//  ReviewsProductRouter.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//
//

import UIKit
import Photos

// MARK: - RouterProtocol
protocol ReviewsProductRouterProtocol {
    func popViewController()
    func showPhotoPickerViewController(delegate: PhotoPickerPresenterDelegate?, cachingManager: PHCachingImageManager, selectedPhotos: [PHAsset], isDismissSuperViewOnClose: Bool, currentImageNumber: Int)
    func showPhotosPickerRouter()
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?)
}

// MARK: - ReviewsProduct Router
class ReviewsProductRouter {
    weak var viewController: ReviewsProductViewController?
    
    static func setupModule(order: OrderModel?) -> ReviewsProductViewController {
        let viewController = ReviewsProductViewController()
        let router = ReviewsProductRouter()
        let interactorInput = ReviewsProductInteractorInput()
        let viewModel = ReviewsProductViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        viewModel.order = order
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ReviewsProduct RouterProtocol
extension ReviewsProductRouter: ReviewsProductRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showPhotoPickerViewController(delegate: PhotoPickerPresenterDelegate?, cachingManager: PHCachingImageManager, selectedPhotos: [PHAsset], isDismissSuperViewOnClose: Bool, currentImageNumber: Int) {
        let controlller = PhotoPickerRouter.setupModule(with: delegate, cachingImageManager: cachingManager, selectedPhotos: selectedPhotos, currentPhotoNumber: currentImageNumber, isDismissSuperViewWhenClose: isDismissSuperViewOnClose)
        self.viewController?.present(controlller, animated: true, completion: nil)
    }
    
    func showPhotosPickerRouter() {
        let controller = PhotosPickerRouter.setupModule()
        controller.modalPresentationStyle = .fullScreen
        self.viewController?.present(controller, animated: true)
    }
    
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?) {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.image = R.image.ic_success_review()
        controller.titleAlert = "Gửi thành công!"
        self.viewController?.present(controller, animated: false)
    }
}
