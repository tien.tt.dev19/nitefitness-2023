//
//  
//  ReviewsProductViewController.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//
//

import UIKit
import Photos

// MARK: - ViewProtocol
protocol ReviewsProductViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onReloadData(indexPath: IndexPath?)
    func setUIStateView()
    
    func setShowAlertConfirmSuccess()
}

// MARK: - ReviewsProduct ViewController
class ReviewsProductViewController: BaseViewController {
    var router: ReviewsProductRouterProtocol!
    var viewModel: ReviewsProductViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_ReviewAction: UIView!
    
    var indexPathSelect: IndexPath?
    let imagePicker = UIImagePickerController()
    
    var isReloadData = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Đánh giá sản phẩm"
        self.setInitUITableView()
        self.setUIStateView()
    }
    
    // MARK: - Action
    @IBAction func onSendReviewAction(_ sender: Any) {
        self.viewModel.onSendReviewAction()
    }
}

// MARK: - ReviewsProduct ViewProtocol
extension ReviewsProductViewController: ReviewsProductViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onReloadData(indexPath: IndexPath?) {
        guard let _indexPath = indexPath else { return }
        self.tbv_TableView.reloadRows(at: [_indexPath], with: .automatic)
    }
    
    func setUIStateView() {
        if (self.viewModel.orderValue?.items?.first(where: {$0.isReview == true})) != nil {
            self.view_ReviewAction.isHidden = true
            
        } else {
            self.view_ReviewAction.isHidden = false
        }
    }
    
    func setShowAlertConfirmSuccess() {
        self.router.presentAlertConfirmSuccessVC(delegate: self)
    }
}

// MARK: UITableViewDataSource
extension ReviewsProductViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderReviewsProduct.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewReviewsImage.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewReviewsTag.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewReviewsComment.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: SectionHeaderReviewsProduct.self)
        header.delegate = self
        header.section = section
        header.model = self.viewModel.itemForSection(at: section)
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView.init(frame: CGRect(x: 0, y: 0, width: self.tbv_TableView.frame.width, height: 8))
        footer.backgroundColor = R.color.line_gray_01()
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 8
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let isReview = self.viewModel.itemForSection(at: section)?.isReview
        if isReview == true {
            return 2
        } else {
            return 3
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            let item = self.viewModel.itemForSection(at: indexPath.section)
            if item?.isReview == true {
                //let count = item?.product?.reviews?.first?.images?.count ?? 0
                let count = item?.review?.images?.count ?? 0
                
                if count == 0 {
                    return 0
                } else {
                    return UITableView.automaticDimension
                }
            } else {
                return UITableView.automaticDimension
            }
            
        case 1:
            let itemOrder = self.viewModel.itemForSection(at: indexPath.section)
            let isReview = itemOrder?.isReview
            if isReview == true {
                if itemOrder?.review?.comment?.count ?? 0 > 0 {
                    return 168
                } else {
                    return 0
                }
                
//                if itemOrder?.product?.reviews?.first?.comment?.count ?? 0 > 0 {
//                    return 168
//                } else {
//                    return 0
//                }
            } else {
                return 168
            }
            
        case 2:
            let heightForCell = self.viewModel.getHeightForCell(at: indexPath)?.heightForCell ?? 0
            if heightForCell > 0 {
                return heightForCell + 8
            } else {
                return 0
            }
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueCell(ofType: CellTableViewReviewsImage.self, for: indexPath)
            cell.delegate = self
            cell.indexPath = indexPath
            cell.model = self.viewModel.itemForSection(at: indexPath.section)
            return cell
            
        case 1:
            let cell = tableView.dequeueCell(ofType: CellTableViewReviewsComment.self, for: indexPath)
            cell.model = self.viewModel.itemForSection(at: indexPath.section)
            return cell
            
        case 2:
            let cell = tableView.dequeueCell(ofType: CellTableViewReviewsTag.self, for: indexPath)
            cell.delegate = self
            cell.indexPath = indexPath
            cell.model = self.viewModel.itemForSection(at: indexPath.section)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
}

// MARK: SectionHeaderReviewsProductDelegate
extension ReviewsProductViewController: SectionHeaderReviewsProductDelegate {
    func onSetRateCosmosAction(section: Int?) {
        guard let _section = section else { return }
        self.isReloadData = true
        
        //let indexPath_0 = IndexPath(row: 0, section: _section)
        let indexPath_1 = IndexPath(row: 1, section: _section)
        let indexPath_2 = IndexPath(row: 2, section: _section)
        self.tbv_TableView.reloadRows(at: [indexPath_1, indexPath_2], with: .automatic)
    }
}

// MARK: CellTableViewReviewsImageDelegate
extension ReviewsProductViewController: CellTableViewReviewsImageDelegate {
    func onAddImageAction(indexPath: IndexPath?) {
        guard let _indexPath = indexPath else { return }
        self.indexPathSelect = _indexPath
        self.presentAlertActionSheet()
    }
    
    func presentAlertActionSheet() {
        let controller = UIAlertController(title: "Chọn ảnh để tiếp tục", message: nil, preferredStyle: .actionSheet)
        controller.addAction(UIAlertAction(title: "Chụp ảnh với Camera", style: .default, handler: self.onCameraAction(action:)))
        controller.addAction(UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default, handler: self.onPhotoAction(action:)))
        controller.addAction(UIAlertAction(title: "Hủy bỏ", style: .cancel))
        self.present(controller, animated: true)
    }
    
    func onCameraAction(action: UIAlertAction) {
        self.openCamera(imgPicker: self.imagePicker)
    }
    
    func onPhotoAction(action: UIAlertAction) {
        self.openPhotos(imgPicker: self.imagePicker)
    }
}

// MARK: CellTableViewReviewsTagDelegate
extension ReviewsProductViewController: CellTableViewReviewsTagDelegate {
    func setHeightForCellReviewsTag(height: CGFloat, indexPath: IndexPath?) {
        guard let _indexPath = indexPath  else { return}
        self.viewModel.setHeightForCell(at: _indexPath, height: height)
        
        if self.isReloadData == true {
            self.isReloadData = false
            self.tbv_TableView.reloadRows(at: [_indexPath], with: .automatic)
        }
    }
}

//MARK: UIImagePickerControllerDelegate
extension ReviewsProductViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func openCamera(imgPicker: UIImagePickerController) {
        imgPicker.sourceType = .camera
        imgPicker.delegate = self
        imgPicker.allowsEditing = false
        
        self.present(imgPicker, animated: true, completion: nil)
    }
    
    func openPhotos(imgPicker: UIImagePickerController) {
        imgPicker.sourceType = .photoLibrary
        imgPicker.delegate = self
        imgPicker.allowsEditing = false
        
        self.present(imgPicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        self.imagePicker.dismiss(animated: true, completion: nil)
        if let imageData = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.viewModel.onDidSelectImage(image: imageData, indexPath: self.indexPathSelect)
        }
    }
}

// MARK: AlertConfirmSuccessViewDelegate
extension ReviewsProductViewController: AlertConfirmSuccessViewDelegate {
    func onDidDismissAlertConfirmSuccessView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.router.popViewController()
        }
    }
}
