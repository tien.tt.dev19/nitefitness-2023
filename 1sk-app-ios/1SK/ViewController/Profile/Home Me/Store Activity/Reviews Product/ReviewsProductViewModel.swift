//
//  
//  ReviewsProductViewModel.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ReviewsProductViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onSendReviewAction()
    
    // UITableView
    func numberOfSections() -> Int
    func itemForSection(at section: Int) -> ItemOrder?
    func getHeightForCell(at indexPath: IndexPath) -> ItemOrder?
    func setHeightForCell(at indexPath: IndexPath, height: CGFloat)
    
    // ImagePicker
    func onDidSelectImage(image: UIImage, indexPath: IndexPath?)
    
    // Value
    var orderValue: OrderModel? { get }
}

// MARK: - ReviewsProduct ViewModel
class ReviewsProductViewModel {
    weak var view: ReviewsProductViewProtocol?
    private var interactor: ReviewsProductInteractorInputProtocol

    init(interactor: ReviewsProductInteractorInputProtocol) {
        self.interactor = interactor
    }

    var order: OrderModel?
    var countReviewFinished = 0
}

// MARK: - ReviewsProduct ViewModelProtocol
extension ReviewsProductViewModel: ReviewsProductViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    // Action
    func onSendReviewAction() {
        guard let items = self.order?.items, items.count > 0, let orderId = self.order?.id else { return }
        self.countReviewFinished = 0
        self.view?.showHud()
        for item in items {
            self.interactor.setReviewProduct(itemOrder: item, orderId: orderId)
        }
    }
    
    // UITableView
    func numberOfSections() -> Int {
        return self.order?.items?.count ?? 0
    }
    
    func itemForSection(at section: Int) -> ItemOrder? {
        return self.order?.items?[section]
    }
    
    func getHeightForCell(at indexPath: IndexPath) -> ItemOrder? {
        return self.order?.items?[indexPath.section]
    }
    
    func setHeightForCell(at indexPath: IndexPath, height: CGFloat) {
        self.order?.items?[indexPath.section].heightForCell = height
    }
    
    // ImagePicker
    func onDidSelectImage(image: UIImage, indexPath: IndexPath?) {
        guard let _indexPath = indexPath else { return }
        let endIndex = self.order?.items?[_indexPath.section].imageReviews?.endIndex ?? 1
        self.order?.items?[_indexPath.section].imageReviews?.insert(image, at: endIndex - 1)
        self.view?.onReloadData(indexPath: _indexPath)
    }
    
    // Value
    var orderValue: OrderModel? {
        return self.order
    }
}

// MARK: - ReviewsProduct InteractorOutputProtocol
extension ReviewsProductViewModel: ReviewsProductInteractorOutputProtocol {
    func onSetReviewProductFinished(with result: Result<ItemOrder, APIError>, itemId: Int?) {
        switch result {
        case .success:
            self.countReviewFinished += 1
            
        case .failure(let error):
            self.view?.hideHud()
            SKToast.shared.showToast(content: error.message)
        }
        
        if self.countReviewFinished == self.order?.items?.count {
            guard let items = self.order?.items, let orderId = self.order?.id else { return }
            let listProductId: [Int] = items.map({ $0.product?.originalProduct ?? 0 })
            self.interactor.getMyReviewProduct(listProductId: listProductId, orderId: orderId)
        }
    }
    
    func onGetMyReviewProductFinished(with result: Result<[ProductOrderModel], APIError>) {
        switch result {
        case .success(let model):
            for product in model {
                if let index = self.order?.items?.firstIndex(where: {$0.product?.originalProduct == product.originalProduct}) {
                    self.order?.items?[index].isReview = true
                    self.order?.items?[index].product?.reviews = product.reviews

                    let star = Double(product.reviews?.first?.star ?? 5)
                    switch star {
                    case RateType.one.value:
                        self.order?.items?[index].rateType = .one
                        
                    case RateType.two.value:
                        self.order?.items?[index].rateType = .two
                        
                    case RateType.three.value:
                        self.order?.items?[index].rateType = .three
                        
                    case RateType.four.value:
                        self.order?.items?[index].rateType = .four
                        
                    case RateType.five.value:
                        self.order?.items?[index].rateType = .five
                        
                    default:
                        break
                    }
                }
            }
            self.view?.onReloadData()
            self.view?.setUIStateView()
            self.view?.setShowAlertConfirmSuccess()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
