//
//  SectionHeaderReviewsProduct.swift
//  1SK
//
//  Created by Thaad on 20/05/2022.
//

import UIKit
import Cosmos

protocol SectionHeaderReviewsProductDelegate: AnyObject {
    func onSetRateCosmosAction(section: Int?)
}

class SectionHeaderReviewsProduct: UITableViewHeaderFooterView {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    
    @IBOutlet weak var view_CosmosRate: CosmosView!
    
    weak var delegate: SectionHeaderReviewsProductDelegate?
    var section: Int?
    var model: ItemOrder? {
        didSet {
            self.setInitDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setRateCosmosAction()
    }
    
    func setInitDataUI() {
        self.lbl_Name.text = self.model?.productName
        self.img_Image.setImageWith(imageUrl: self.model?.productImage ?? "")
        
        self.view_CosmosRate.rating = self.model?.rateType?.value ?? RateType.five.value
        
        if self.model?.isReview == true {
            self.view_CosmosRate.settings.updateOnTouch = false
        }
    }
}

// MARK: Cosmos Action
extension SectionHeaderReviewsProduct {
    func setRateCosmosAction() {
        self.view_CosmosRate.didFinishTouchingCosmos = { rating in
            guard self.model?.isReview != true else { return }
            
            switch rating {
            case RateType.one.value:
                self.model?.rateType = .one
                
            case RateType.two.value:
                self.model?.rateType = .two
                
            case RateType.three.value:
                self.model?.rateType = .three
                
            case RateType.four.value:
                self.model?.rateType = .four
                
            case RateType.five.value:
                self.model?.rateType = .five
                
            default:
                self.model?.rateType = .five
            }
            
            self.model?.rateComments = DataConfig.shared.getRateComments(type: self.model?.rateType)
            self.delegate?.onSetRateCosmosAction(section: self.section)
        }
    }
}
