//
//  
//  StoreActivityInteractorInput.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol StoreActivityInteractorInputProtocol {
    func getListOrder(with status: OrderStatus, page: Int, perPage: Int)
    func getListOrderStatus()
    func getMyReviewProduct(listProductId: [Int], order: OrderModel?)
}

// MARK: - Interactor Output Protocol
protocol StoreActivityInteractorOutputProtocol: AnyObject {
    func onGetListOrderFinished(with result: Result<[OrderModel], APIError>, page: Int, total: Int)
    func onGetListOrderStatusFinished(with result: Result<[OrderStatusModel], APIError>)
    func onGetMyReviewProductFinished(with result: Result<[ProductOrderModel], APIError>, order: OrderModel?)
}

// MARK: - StoreActivity InteractorInput
class StoreActivityInteractorInput {
    weak var output: StoreActivityInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - StoreActivity InteractorInputProtocol
extension StoreActivityInteractorInput: StoreActivityInteractorInputProtocol {
    func getListOrder(with status: OrderStatus, page: Int, perPage: Int) {
        self.storeService?.getListOrder(with: status, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListOrderFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
    
    func getListOrderStatus() {
        self.storeService?.getListOrderStatus(completion: { [weak self] result in
            self?.output?.onGetListOrderStatusFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getMyReviewProduct(listProductId: [Int], order: OrderModel?) {
        self.storeService?.getMyReviewProduct(listProductId: listProductId, orderId: order?.id, completion: { [weak self] result in
            self?.output?.onGetMyReviewProductFinished(with: result.unwrapSuccessModel(), order: order)
        })
    }
}
