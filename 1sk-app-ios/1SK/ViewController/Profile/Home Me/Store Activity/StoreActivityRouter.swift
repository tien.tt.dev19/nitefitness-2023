//
//  
//  StoreActivityRouter.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol StoreActivityRouterProtocol {
    func showOrderDetail(order: OrderModel?, delegate: OrderDetailViewDelegate?)
    func showReviewsProductVC(order: OrderModel?)
}

// MARK: - StoreActivity Router
class StoreActivityRouter {
    weak var viewController: StoreActivityViewController?
    
    static func setupModule() -> StoreActivityViewController {
        let viewController = StoreActivityViewController()
        let router = StoreActivityRouter()
        let interactorInput = StoreActivityInteractorInput()
        let viewModel = StoreActivityViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - StoreActivity RouterProtocol
extension StoreActivityRouter: StoreActivityRouterProtocol {
    func showOrderDetail(order: OrderModel?, delegate: OrderDetailViewDelegate?) {
        let controller = OrderDetailRouter.setupModule(order: order, delegate: delegate, isCreateOrder: nil)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showReviewsProductVC(order: OrderModel?) {
        let controller = ReviewsProductRouter.setupModule(order: order)
        self.viewController?.navigationController?.show(controller, sender: true)
    }
}
