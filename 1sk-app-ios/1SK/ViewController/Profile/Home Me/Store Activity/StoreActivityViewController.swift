//
//  
//  StoreActivityViewController.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol StoreActivityViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    // UICollectionView
    func onReloadDataColl()
    
    // UITableView
    func onReloadDataTable()
    func onLoadingSuccess()
    func onInsertSection(at section: Int)
    func onSetNotFoundView(isHidden: Bool)
    
    // Action
    func onShowReviewsDetail(order: OrderModel?)
}

// MARK: - StoreActivity ViewController
class StoreActivityViewController: BaseViewController {
    var router: StoreActivityRouterProtocol!
    var viewModel: StoreActivityViewModelProtocol!
    
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotFound: UIView!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUICollectionView()
        self.setInitUITableView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    
}

// MARK: - StoreActivity ViewProtocol
extension StoreActivityViewController: StoreActivityViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    // UICollectionView
    func onReloadDataColl() {
        self.coll_CollectionView.reloadData()
    }
    
    // UITableView
    func onReloadDataTable() {
        self.tbv_TableView.reloadData()
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertSection(at section: Int) {
        self.tbv_TableView.beginUpdates()
        self.tbv_TableView.insertSections(IndexSet(integer: section), with: .automatic)
        self.tbv_TableView.endUpdates()
    }
    
    func onSetNotFoundView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 0
            } completion: { _ in
                self.view_NotFound.isHidden = true
            }

        } else {
            self.view_NotFound.isHidden = false
            UIView.animate(withDuration: 0.5) {
                self.view_NotFound.alpha = 1
            }
        }
    }
    
    // Action
    func onShowReviewsDetail(order: OrderModel?) {
        self.router.showReviewsProductVC(order: order)
    }
}

// MARK: Refresh Control
extension StoreActivityViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.tbv_TableView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: UICollectionViewDataSource
extension StoreActivityViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewOrderStatus.self)
        
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems(in: section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewOrderStatus.self, for: indexPath)
        cell.model = self.viewModel.cellForItem(at: indexPath)
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension StoreActivityViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let index = self.viewModel.getIndexOrderStatusSelected() {
            if let cell = self.coll_CollectionView.cellForItem(at: IndexPath(row: index, section: 0)) as? CellCollectionViewOrderStatus {
                cell.setSelected(isSelected: false)
            }
        }
        
        self.viewModel.didSelectItem(at: indexPath)
    }
}

// MARK: UITableViewDataSource
extension StoreActivityViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionOrderView.self)
        self.tbv_TableView.registerHeaderNib(ofType: FooterSectionOrderView.self)
        
        self.tbv_TableView.registerNib(ofType: CellTableViewOrderProduct.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderSectionOrderView.self)
        header.model = self.viewModel.itemForSection(in: section)
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: FooterSectionOrderView.self)
        header.delegate = self
        header.model = self.viewModel.itemForSection(in: section)
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows(in: section)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewOrderProduct.self, for: indexPath)
        cell.config(item: self.viewModel.cellForRow(at: indexPath))
        
        let count = self.viewModel.numberOfRows(in: indexPath.section) - 1
        if indexPath.row == count {
            cell.view_LineBottom.isHidden = true
        } else {
            cell.view_LineBottom.isHidden = false
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.subviews.first as? CellTableViewOrderProduct) != nil {
            let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
            
            if isReachingEnd && self.isLoading == false {
                self.isLoading = true
                self.viewModel.onLoadMoreAction()
            }
        }
    }
    
}

// MARK: FooterSectionOrderViewDelegate
extension StoreActivityViewController: FooterSectionOrderViewDelegate {
    func onRatetingAction(order: OrderModel?) {
        self.router.showReviewsProductVC(order: order)
    }
    
    func onRatetedAction(order: OrderModel?) {
        self.viewModel.onRatetedAction(order: order)
    }
    
    func onDetailAction(order: OrderModel?) {
        self.router.showOrderDetail(order: order, delegate: self)
    }
}

// MARK: OrderDetailViewDelegate
extension StoreActivityViewController: OrderDetailViewDelegate {
    func onCancelOrderSuccess(order: OrderModel?) {
        self.viewModel.onCancelOrderSuccess(order: order)
    }
}
