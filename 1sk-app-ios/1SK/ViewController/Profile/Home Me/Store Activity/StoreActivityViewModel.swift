//
//  
//  StoreActivityViewModel.swift
//  1SK
//
//  Created by Thaad on 23/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol StoreActivityViewModelProtocol {
    func onViewDidLoad()
    func onViewWillAppear()
    func onViewDidAppear()
    
    // UICollectionView
    func numberOfItems(in section: Int) -> Int
    func cellForItem(at indexPath: IndexPath) -> OrderStatusModel?
    func didSelectItem(at indexPath: IndexPath)
    func getIndexOrderStatusSelected() -> Int?
    
    // UITableView
    func numberOfSections() -> Int
    func itemForSection(in section: Int) -> OrderModel?
    func numberOfRows(in section: Int) -> Int
    func cellForRow(at indexPath: IndexPath) -> ItemOrder?
    func didSelectRow(at indexPath: IndexPath)
    func onDetailAction(order: OrderModel?)
    func onRefreshAction()
    func onLoadMoreAction()
    
    // Action
    func onCancelOrderSuccess(order: OrderModel?)
    func onRatetedAction(order: OrderModel?)
}

// MARK: - StoreActivity ViewModel
class StoreActivityViewModel {
    weak var view: StoreActivityViewProtocol?
    private var interactor: StoreActivityInteractorInputProtocol

    init(interactor: StoreActivityInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listOrderStatus: [OrderStatusModel] = []
    var listOrder: [OrderModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 10
    private var isOutOfData = false
    
    func setInitDataOrderStatus() {
        self.listOrderStatus.append(OrderStatusModel(status: .all, isSelected: true))
        self.listOrderStatus.append(OrderStatusModel(status: .pending, isSelected: false))
        self.listOrderStatus.append(OrderStatusModel(status: .processing, isSelected: false))
        self.listOrderStatus.append(OrderStatusModel(status: .completed, isSelected: false))
        self.listOrderStatus.append(OrderStatusModel(status: .canceled, isSelected: false))
        self.view?.onReloadDataColl()
        
        self.getDataOrderStatus()
    }
    
    private func getData(page: Int) {
        self.page = page
        if page == 1 {
            self.page = 1
            self.isOutOfData = false
        }
        
        guard let status = self.listOrderStatus.first(where: {$0.isSelected == true})?.status else {
            return
        }
        self.view?.showHud()
        self.interactor.getListOrder(with: status, page: page, perPage: self.perPage)
    }
    
    private func getDataOrderStatus() {
        self.interactor.getListOrderStatus()
    }
}

// MARK: - StoreActivity ViewModelProtocol
extension StoreActivityViewModel: StoreActivityViewModelProtocol {
    func onViewDidLoad() {
        self.setInitDataOrderStatus()
        self.getData(page: 1)
    }
    
    func onViewWillAppear() {
        //
    }
    
    func onViewDidAppear() {
        self.getData(page: 1)
        self.getDataOrderStatus()
    }
    
    // UICollectionView
    func numberOfItems(in section: Int) -> Int {
        return self.listOrderStatus.count
    }
    
    func cellForItem(at indexPath: IndexPath) -> OrderStatusModel? {
        return self.listOrderStatus[indexPath.row]
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        self.listOrderStatus.first(where: {$0.isSelected == true})?.isSelected = false
        self.listOrderStatus[indexPath.row].isSelected = true
        
        self.getData(page: 1)
        self.getDataOrderStatus()
    }
    
    func getIndexOrderStatusSelected() -> Int? {
        return self.listOrderStatus.firstIndex(where: {$0.isSelected == true})
    }
    
    // UITableView
    func numberOfSections() -> Int {
        return self.listOrder.count
    }
    
    func itemForSection(in section: Int) -> OrderModel? {
        return self.listOrder[section]
    }
    
    func numberOfRows(in section: Int) -> Int {
        return self.listOrder[section].items?.count ?? 0
    }
    
    func cellForRow(at indexPath: IndexPath) -> ItemOrder? {
        return self.listOrder[indexPath.section].items?[indexPath.row]
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        print("didSelectRow \(indexPath.section) - \(indexPath.row)")
    }
    
    func onDetailAction(order: OrderModel?) {
        SKToast.shared.showToast(content: "Đang làm chưa xong")
    }
    
    func onRefreshAction() {
        self.getData(page: 1)
        self.getDataOrderStatus()
    }
    
    func onLoadMoreAction() {
        if self.listOrder.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)
            
        } else {
            //SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    // Action
    func onCancelOrderSuccess(order: OrderModel?) {
        if let index = self.listOrder.firstIndex(where: {$0.id == order?.id}) {
            
            let status = self.listOrderStatus.first(where: {$0.isSelected == true})?.status
            switch status {
            case .all:
                self.listOrder[index] = order!
                
            default:
                self.listOrder.remove(at: index)
            }
            
            self.view?.onReloadDataTable()
        }
    }
    
    func onRatetedAction(order: OrderModel?) {
        self.view?.onShowReviewsDetail(order: order)
        
//        guard let items = order?.items else {
//            return
//        }
//
//        let listProductId: [Int] = items.map({ $0.product?.id ?? 0 })
//        self.view?.showHud()
//        self.interactor.getMyReviewProduct(listProductId: listProductId, order: order)
    }
}

// MARK: - StoreActivity InteractorOutputProtocol
extension StoreActivityViewModel: StoreActivityInteractorOutputProtocol {
    func onGetListOrderFinished(with result: Result<[OrderModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listOrder = listModel
                self.view?.onReloadDataTable()
                
            } else {
                for object in listModel {
                    self.listOrder.append(object)
                    self.view?.onInsertSection(at: self.listOrder.count - 1)
                }
            }
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listOrder.count >= self.total {
                self.isOutOfData = true
            }
            
            // Set Status View Data Not Found
            if self.listOrder.count > 0 {
                self.view?.onSetNotFoundView(isHidden: true)
            } else {
                self.view?.onSetNotFoundView(isHidden: false)
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetListOrderStatusFinished(with result: Result<[OrderStatusModel], APIError>) {
        switch result {
        case .success(let listModel):
            for item in self.listOrderStatus {
                var allCount = 0
                for status in listModel {
                    if item.status?.id == status.value {
                        item.ordersCount = status.ordersCount
                    }
                    allCount += status.ordersCount ?? 0
                }
                
                if item.status?.id == OrderStatus.all.id {
                    item.ordersCount = allCount
                }
            }
            
            self.view?.onReloadDataColl()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetMyReviewProductFinished(with result: Result<[ProductOrderModel], APIError>, order: OrderModel?) {
        switch result {
        case .success(let model):
            for product in model {
                if let index = order?.items?.firstIndex(where: {$0.product?.originalProduct == product.originalProduct}) {
                    order?.items?[index].product?.reviews = product.reviews

                    let star = Double(product.reviews?.first?.star ?? 5)
                    switch star {
                    case RateType.one.value:
                        order?.items?[index].rateType = .one
                        
                    case RateType.two.value:
                        order?.items?[index].rateType = .two
                        
                    case RateType.three.value:
                        order?.items?[index].rateType = .three
                        
                    case RateType.four.value:
                        order?.items?[index].rateType = .four
                        
                    case RateType.five.value:
                        order?.items?[index].rateType = .five
                        
                    default:
                        break
                    }
                }
            }
            self.view?.onShowReviewsDetail(order: order)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
