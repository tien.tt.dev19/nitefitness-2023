//
//  TypeActivityCell.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//

import UIKit

class TypeActivityCell: UICollectionViewCell {
    
    @IBOutlet weak var view_main: UIView!
    @IBOutlet weak var lbl_TypeCategory: UILabel!
    
    override var isSelected: Bool {
        didSet {
            self.view_main.backgroundColor = isSelected ? R.color.mainColor() : UIColor(hex: "EEEEEE")
            self.lbl_TypeCategory.textColor = isSelected ? UIColor.white : UIColor(hex: "737678")
        }
    }
    
    var model: String? {
        didSet {
            self.setData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}

// MARK: - helpers
extension TypeActivityCell {
    private func setData() {
        guard let model = self.model else { return }
        self.lbl_TypeCategory.text = model
    }
}
