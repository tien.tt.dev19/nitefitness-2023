//
//  CellTableViewTracking.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//

import UIKit
import Accelerate

class CellTableViewTracking: UITableViewCell {

    @IBOutlet weak var img_UnitData: UIImageView!
    @IBOutlet weak var lbl_UnitData: UILabel!
    @IBOutlet weak var lbl_Distance: UILabel!
    @IBOutlet weak var lbl_time: UILabel!
    @IBOutlet weak var lbl_Today: UILabel!
    
    @IBOutlet weak var stack_WarningInvalid: UIStackView!
    @IBOutlet weak var lbl_WarningInvalid: UILabel!
    
    var model: StravaAthleteActivityModel? {
        didSet {
            self.setDataUI()
        }
    }
    
    var type: TypeActivity? {
        didSet {
            self.img_UnitData.image = type?.unitDataImage
            self.lbl_UnitData.text = type?.unitDataTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setDataUI() {
        if let data = self.model {
            self.lbl_Distance.text = data.distance?.toKilometers()
            self.lbl_Today.text = data.startDateLocal?.toDate(.ymdhms)?.toString(.hmdmy)
            self.lbl_time.text = data.movingTime?.toHoursMinutesSeconds()
        }
        
        if self.model?.illegal == 1 {
            self.lbl_WarningInvalid.text = self.model?.illegalLabel
            self.stack_WarningInvalid.isHidden = false
            
        } else {
            self.lbl_WarningInvalid.text = ""
            self.stack_WarningInvalid.isHidden = true
            
        }
    }
}
