//
//  
//  StravaActivityInteractorInput.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol StravaActivityInteractorInputProtocol {
    func onGetAthleteActivity(per_page: Int, vr_sport_id: Int, before: String, after: String)
    func onGetVRSports()
    func onGetMeIdentifyProviders()
    func getStravaActivitiesTotal(vrSportId: Int, timeBefore: String, timeAfter: String)
}

// MARK: - Interactor Output Protocol
protocol StravaActivityInteractorOutputProtocol: AnyObject {
    func didGetAthleteActivityFinished(with result: Result<[StravaAthleteActivityModel], APIError>)
    func didGetVRSports(with result: Result<[VRSportModel], APIError>)
    func didGetMeIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>)
    func onGetStravaActivitiesTotalFinised(with result: Result<ActivitiesTotal, APIError>)
}

// MARK: - StravaActivity InteractorInput
class StravaActivityInteractorInput {
    weak var output: StravaActivityInteractorOutputProtocol?
    
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - StravaActivity InteractorInputProtocol
extension StravaActivityInteractorInput: StravaActivityInteractorInputProtocol {
    func onGetAthleteActivity(per_page: Int, vr_sport_id: Int, before: String, after: String) {
        self.fitRaceSevice?.onGetAthleteActivities(per_page: per_page, vr_sport_id: vr_sport_id, before: before, after: after, completion: { [weak self] result in
            self?.output?.didGetAthleteActivityFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetVRSports() {
        self.fitRaceSevice?.onGetVRSports(completion: { [weak self] result in
            self?.output?.didGetVRSports(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetMeIdentifyProviders() {
        self.fitRaceSevice?.onGetMeIdentifyProvider(completion: { [weak self] result in
            self?.output?.didGetMeIdentifyProvidersStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func getStravaActivitiesTotal(vrSportId: Int, timeBefore: String, timeAfter: String) {
        self.fitRaceSevice?.onGetStravaActivitiesTotal(vrSportId: vrSportId, timeBefore: timeBefore, timeAfter: timeAfter, completion: { [weak self] result in
            self?.output?.onGetStravaActivitiesTotalFinised(with: result.unwrapSuccessModel())
        })
    }
}
