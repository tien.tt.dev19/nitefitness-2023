//
//  
//  StravaActivityRouter.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol StravaActivityRouterProtocol {
    func showAppTrackingConnectRouter()
}

// MARK: - StravaActivity Router
class StravaActivityRouter {
    weak var viewController: StravaActivityViewController?
    
    static func setupModule() -> StravaActivityViewController {
        let viewController = StravaActivityViewController()
        let router = StravaActivityRouter()
        let interactorInput = StravaActivityInteractorInput()
        let viewModel = StravaActivityViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - StravaActivity RouterProtocol
extension StravaActivityRouter: StravaActivityRouterProtocol {
    func showAppTrackingConnectRouter() {
        let controller = AppTrackingConnectRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
}
