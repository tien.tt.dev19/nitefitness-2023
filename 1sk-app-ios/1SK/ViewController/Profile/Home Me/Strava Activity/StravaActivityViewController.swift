//
//  
//  StravaActivityViewController.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//
//

import UIKit

// MARK: TypeActivity
enum TypeActivity: Int, CaseIterable {
    case run
    case walk
    case ride
    
    var typeString: String {
        switch self {
        case .run:
            return "Run"
            
        case .walk:
            return "Walk"
            
        case .ride:
            return "Ride"
        }
    }
    
    var title: String {
        switch self {
        case .run:
            return "Chạy bộ"
            
        case .walk:
            return "Đi bộ"
            
        case .ride:
            return "Đạp xe"
        }
    }
    
    var unitDataImage: UIImage {
        switch self {
        case .run:
            return R.image.ic_strava_distance_unselected()!
            
        case .walk:
            return R.image.ic_strava_walk_selected()!
            
        case .ride:
            return R.image.ic_strava_bike_selected()!
        }
    }
    
    var unitDataTitle: String {
        switch self {
        case .run:
            return "Quãng đường"
        case .walk:
            return "Số bước"
        case .ride:
            return "Quãng đường"
        }
    }
}

// MARK: - ViewProtocol
protocol StravaActivityViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onReloadVRSportsData()
    func onReloadData()
    func onShowNoActivityView()
    func setUpdateStatusUI(isConnect: Bool)
    
    func setDataUITotal(activitiesTotal: ActivitiesTotal)
}

// MARK: - StravaActivity ViewController
class StravaActivityViewController: BaseViewController {
    var router: StravaActivityRouterProtocol!
    var viewModel: StravaActivityViewModelProtocol!
    
    @IBOutlet weak var constraint_TableView: NSLayoutConstraint!
    @IBOutlet weak var coll_TypeActivity: UICollectionView!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var img_UnitData: UIImageView!
    @IBOutlet weak var lbl_UnitDistance: UILabel!
    @IBOutlet weak var view_noActivity: UIView!
    @IBOutlet weak var view_contentView: UIView!
    
    @IBOutlet weak var lbl_StatusConnect: UILabel!
    
    @IBOutlet weak var view_DistanceTotal: UIView!
    @IBOutlet weak var lbl_DistanceTotal: UILabel!
    
    @IBOutlet weak var view_TimeTotal: UIView!
    @IBOutlet weak var lbl_TimeToltal: UILabel!
    
    var type = TypeActivity.init(rawValue: 0)
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillAppear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.img_UnitData.image = self.type?.unitDataImage
        self.setInitCollectioView()
        self.setInitTableView()
        
        self.view_DistanceTotal.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.15)
        self.view_TimeTotal.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.15)
    }
    
    private func getMessageNoData(message: String) -> NSAttributedString {
        let font = R.font.robotoRegular(size: 16)!
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message, attributes: attributes)
        return attributedMessage
    }
    
    // MARK: - Action
    @IBAction func onStravaAction(_ sender: Any) {
        self.router.showAppTrackingConnectRouter()
    }
}

// MARK: - StravaActivity ViewProtocol
extension StravaActivityViewController: StravaActivityViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        if self.viewModel.onGetAthleteData().isEmpty {
            self.onShowNoActivityView()
            
        } else {
            self.constraint_TableView.constant = CGFloat(self.viewModel.onGetAthleteData().count * 121)
            self.view_noActivity.isHidden = true
            self.view_contentView.isHidden = false
            self.tbv_TableView.reloadData()
        }
    }
    
    func onReloadVRSportsData() {
        self.coll_TypeActivity.reloadData()
        let indexPathForFirstRow = IndexPath(row: 0, section: 0)
        self.coll_TypeActivity.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: [.centeredHorizontally])
        self.collectionView(self.coll_TypeActivity, didSelectItemAt: indexPathForFirstRow)
        self.hideHud()
    }
    
    func onShowNoActivityView() {
        self.view_noActivity.isHidden = false
        self.view_contentView.isHidden = true
        self.constraint_TableView.constant = 94
    }
    
    func setUpdateStatusUI(isConnect: Bool) {
        if isConnect {
            self.lbl_StatusConnect.text = "Đã kết nối"
        } else {
            self.lbl_StatusConnect.text = "Kết nối"
        }
    }
    
    func setDataUITotal(activitiesTotal: ActivitiesTotal) {
        if let totalDistance = activitiesTotal.totalDistance {
            let distance: Double = totalDistance / 1000
            self.lbl_DistanceTotal.text = "\(distance.rounded(toPlaces: 2)) Km"
        }
        
        if let totalTime = activitiesTotal.totalMovingTime, let timeInt = Int(totalTime) {
            self.lbl_TimeToltal.text = timeInt.toHoursMinutesSeconds()
        }
    }
}

//MARK: - UICollectionViewDataSource
extension StravaActivityViewController: UICollectionViewDataSource {
    func setInitCollectioView() {
        self.coll_TypeActivity.registerNib(ofType: TypeActivityCell.self)

        self.coll_TypeActivity.delegate = self
        self.coll_TypeActivity.dataSource = self
        self.coll_TypeActivity.emptyDataSetSource = self

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 8
        layout.minimumInteritemSpacing = 8
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        self.coll_TypeActivity.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.onGetVrSportsData().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: TypeActivityCell.self, for: indexPath)
        cell.model = self.viewModel.onGetVrSportsData()[indexPath.row].name ?? ""
        return cell
    }
}

//MARK: -UICollectionViewDelegateFlowLayout
extension StravaActivityViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.viewModel.collectionView(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath)
    }
}

//MARK: - UICollectionViewDelegate
extension StravaActivityViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.coll_TypeActivity.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
        self.viewModel.collectionView(collectionView, didSelectItemAt: indexPath)
        self.onReloadData()
    }
}

//MARK: - UITableViewDataSource
extension StravaActivityViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitTableView() {
        self.constraint_TableView.constant = 10 * 94
        self.tbv_TableView.registerNib(ofType: CellTableViewTracking.self)
        self.tbv_TableView.delegate = self
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.separatorStyle = .none
        
        self.view_noActivity.isHidden = false
        self.view_contentView.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.onGetAthleteData().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewTracking.self, for: indexPath)
        cell.type = self.type
        cell.model = self.viewModel.cellForItem(at: indexPath)
        return cell
    }
}

//MARK: - EmptyDataSetSource
extension StravaActivityViewController: EmptyDataSetSource {
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return self.getMessageNoData(message: "Chưa có dữ liệu")
    }
}
