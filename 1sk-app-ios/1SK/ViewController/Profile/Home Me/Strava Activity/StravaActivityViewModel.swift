//
//  
//  StravaActivityViewModel.swift
//  1SK
//
//  Created by Tiến Trần on 26/07/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol StravaActivityViewModelProtocol {
    func onViewDidLoad()
    func onViewWillAppear()
    func onGetVrSportsData() -> [VRSportModel]
    func onGetAthleteData() -> [StravaAthleteActivityModel]
    func onGetTotalData(type: TypeActivity) -> String
    func onGetTotalElapsedTime(type: TypeActivity) -> String
    
    func numberOfItems() -> Int
    func cellForItem(at indexPath: IndexPath) -> StravaAthleteActivityModel?
    func didSelectItem(at indexPath: IndexPath)
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
}

// MARK: - StravaActivity ViewModel
class StravaActivityViewModel {
    weak var view: StravaActivityViewProtocol?
    private var interactor: StravaActivityInteractorInputProtocol
    
    init(interactor: StravaActivityInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var stravaAthleteData = [StravaAthleteActivityModel]()
    var vrSportsData = [VRSportModel]()
    
    var startTime = Date().startOfWeek.toString(Date.Format.ymdhms)
    var endTime = Date().endOfWeek.toString(Date.Format.ymdhms)
    
    func setInitDataStravaActivity() {
        self.view?.showHud()
        self.interactor.onGetVRSports()
        self.interactor.onGetMeIdentifyProviders()
    }
}

// MARK: - StravaActivity ViewModelProtocol
extension StravaActivityViewModel: StravaActivityViewModelProtocol {
    func onViewDidLoad() {
        self.setInitDataStravaActivity()
    }
    
    func onViewWillAppear() {
        self.setInitDataStravaActivity()
    }

    func numberOfItems() -> Int {
        return 0
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 12
        let textSize = self.onGetVrSportsData()[indexPath.item].name?.width(withConstrainedHeight: 17, font: R.font.robotoRegular(size: 14)!) ?? 0
        let cellWidth = padding * 2 + textSize
        return CGSize(width: cellWidth, height: 32)
    }
    
    func onGetAthleteData() -> [StravaAthleteActivityModel] {
        return self.stravaAthleteData
    }
    
    func onGetVrSportsData() -> [VRSportModel] {
        return self.vrSportsData
    }
    
    func cellForItem(at indexPath: IndexPath) -> StravaAthleteActivityModel? {
        return self.onGetAthleteData()[indexPath.row]
    }
    
    func onGetTotalData(type: TypeActivity) -> String {
        let totalDistance = self.onGetAthleteData().map({$0.distance ?? 0}).reduce(0, +)
        return totalDistance.toKilometers()
    }
    
    func onGetTotalElapsedTime(type: TypeActivity) -> String {
        let totalElapsedTime = self.onGetAthleteData().map({$0.movingTime ?? 0}).reduce(0, +)
        return "\(totalElapsedTime.toHoursMinutesSeconds())"
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let vrSportId = self.vrSportsData[indexPath.row].id else {
            return
        }
        self.interactor.onGetAthleteActivity(per_page: 200, vr_sport_id: vrSportId, before: self.endTime, after: self.startTime)
        self.interactor.getStravaActivitiesTotal(vrSportId: vrSportId, timeBefore: self.endTime, timeAfter: self.startTime)
    }
}

// MARK: - StravaActivity InteractorOutputProtocol
extension StravaActivityViewModel: StravaActivityInteractorOutputProtocol {
    func didGetAthleteActivityFinished(with result: Result<[StravaAthleteActivityModel], APIError>) {
        switch result {
        case .success(let model):
            self.stravaAthleteData = model
            self.view?.hideHud()
            self.view?.onReloadData()
            
        case .failure:
            self.stravaAthleteData.removeAll()
            self.view?.hideHud()
            self.view?.onShowNoActivityView()
        }        
    }
    
    func didGetVRSports(with result: Result<[VRSportModel], APIError>) {
        switch result {
        case .success(let model):
            self.vrSportsData = model
            self.view?.hideHud()
            self.view?.onReloadVRSportsData()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func didGetMeIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>) {
        switch result {
        case .success(let model):
            guard model.first(where: {$0.code == "strava"}) != nil else {
                self.view?.setUpdateStatusUI(isConnect: false)
                return
            }
            self.view?.setUpdateStatusUI(isConnect: true)
            
        case .failure:
            self.view?.setUpdateStatusUI(isConnect: false)
        }
        self.view?.hideHud()
    }
    
    func onGetStravaActivitiesTotalFinised(with result: Result<ActivitiesTotal, APIError>) {
        switch result {
        case .success(let model):
            self.view?.setDataUITotal(activitiesTotal: model)
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
