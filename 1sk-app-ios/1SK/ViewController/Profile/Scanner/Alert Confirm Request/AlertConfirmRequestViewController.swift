//
//  AlertConfirmRequestViewController.swift
//  1SK
//
//  Created by Thaad on 10/05/2022.
//

import UIKit

protocol AlertConfirmRequestViewDelegate: AnyObject {
    func onAlertPopupConfirmCancelAction(model: LoginRequestModel?)
    func onAlertPopupConfirmAgreeAction(model: LoginRequestModel?)
}

class AlertConfirmRequestViewController: UIViewController {

    @IBOutlet weak var lbl_DeviceName: UILabel!
    @IBOutlet weak var lbl_OsName: UILabel!
    @IBOutlet weak var lbl_OsVersion: UILabel!
    @IBOutlet weak var lbl_BrowserName: UILabel!
    @IBOutlet weak var lbl_BrowserVersion: UILabel!
    
    weak var delegate: AlertConfirmRequestViewDelegate?
    var model: LoginRequestModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitDataUI(agent: self.model?.agent)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    func setInitDataUI(agent: AgentLoginRequest?) {
        self.lbl_DeviceName.text = agent?.deviceName
        self.lbl_OsName.text = agent?.osName
        self.lbl_OsVersion.text = agent?.osVersion
        self.lbl_BrowserName.text = agent?.browserName
        self.lbl_BrowserVersion.text = agent?.browserVersion
    }

    @IBAction func onCancelAction(_ sender: Any) {
        self.delegate?.onAlertPopupConfirmCancelAction(model: self.model)
        self.hiddenAnimate()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onAlertPopupConfirmAgreeAction(model: self.model)
        self.hiddenAnimate()
    }
    
}

// MARK: - Init Animation
extension AlertConfirmRequestViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}
