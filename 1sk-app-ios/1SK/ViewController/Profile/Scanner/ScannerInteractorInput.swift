//
//  
//  ScannerInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/05/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ScannerInteractorInputProtocol {
    func getInfoRequestLogin(token: String)
    func setConfirmRequestLogin(token: String, action: TypeConfirmRequest)
}

// MARK: - Interactor Output Protocol
protocol ScannerInteractorOutputProtocol: AnyObject {
    func onGetInfoRequestLoginFinished(with result: Result<LoginRequestModel, APIError>)
    func onSetConfirmRequestLoginFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - Scanner InteractorInput
class ScannerInteractorInput {
    weak var output: ScannerInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - Scanner InteractorInputProtocol
extension ScannerInteractorInput: ScannerInteractorInputProtocol {
    func getInfoRequestLogin(token: String) {
        self.authService?.getInfoRequestLogin(token: token, completion: { [weak self] result in
            self?.output?.onGetInfoRequestLoginFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setConfirmRequestLogin(token: String, action: TypeConfirmRequest) {
        self.authService?.setConfirmRequestLogin(token: token, action: action, completion: { [weak self] result in
            self?.output?.onSetConfirmRequestLoginFinished(with: result.unwrapSuccessModel())
        })
    }
}
