//
//  
//  ScannerRouter.swift
//  1SK
//
//  Created by Thaad on 09/05/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ScannerRouterProtocol {
    func dismissViewController()
    func presentAlertConfirmRequest(model: LoginRequestModel?, delegate: AlertConfirmRequestViewDelegate?)
}

// MARK: - Scanner Router
class ScannerRouter {
    weak var viewController: ScannerViewController?
    
    static func setupModule() -> ScannerViewController {
        let viewController = ScannerViewController()
        let router = ScannerRouter()
        let interactorInput = ScannerInteractorInput()
        let viewModel = ScannerViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - Scanner RouterProtocol
extension ScannerRouter: ScannerRouterProtocol {
    func dismissViewController() {
        self.viewController?.dismiss(animated: true)
    }
    
    func presentAlertConfirmRequest(model: LoginRequestModel?, delegate: AlertConfirmRequestViewDelegate?) {
        let controller = AlertConfirmRequestViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.model = model
        self.viewController?.present(controller, animated: false)
    }
}
