//
//  
//  ScannerViewController.swift
//  1SK
//
//  Created by Thaad on 09/05/2022.
//
//

import UIKit
import AVFoundation

// MARK: - ViewProtocol
protocol ScannerViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onShowPopupConfirmLogin(model: LoginRequestModel?)
    func onDismissViewController()
    func onResetScanner()
}

// MARK: - Scanner ViewController
class ScannerViewController: BaseViewController {
    var router: ScannerRouterProtocol!
    var viewModel: ScannerViewModelProtocol!
    
    @IBOutlet weak var view_Camera: UIView!
    
    @IBOutlet weak var view_Overlay: UIView!
    @IBOutlet weak var view_Scanner: UIView!
    @IBOutlet weak var img_Scanner: UIImageView!
    @IBOutlet weak var img_QrCode: UIImageView!
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    
    private var boundingBox = CAShapeLayer()
    private var resetTimer: Timer?
    
    private var isScanner = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.captureSession?.isRunning == false {
            self.captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.captureSession?.isRunning == true {
            self.captureSession.stopRunning()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitAVCapture()
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.router.dismissViewController()
    }
    
}

// MARK: - Scanner ViewProtocol
extension ScannerViewController: ScannerViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onShowPopupConfirmLogin(model: LoginRequestModel?) {
        self.router.presentAlertConfirmRequest(model: model, delegate: self)
    }
    
    func onDismissViewController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.router.dismissViewController()
        }
    }
    
    func onResetScanner() {
        self.img_Scanner.backgroundColor = .clear
        self.img_Scanner.alpha = 1
        self.img_QrCode.image = nil
        self.isScanner = false
    }
}

// MARK: AlertConfirmRequestViewDelegate
extension ScannerViewController: AlertConfirmRequestViewDelegate {
    func onAlertPopupConfirmCancelAction(model: LoginRequestModel?) {
        guard let token = model?.token else {
            return
        }
        self.viewModel.onSetConfirmRequestLogin(token: token, action: .deny)
    }
    
    func onAlertPopupConfirmAgreeAction(model: LoginRequestModel?) {
        guard let token = model?.token else {
            return
        }
        self.viewModel.onSetConfirmRequestLogin(token: token, action: .accept)
    }
}

// MARK: AVCaptureMetadataOutputObjectsDelegate
extension ScannerViewController: AVCaptureMetadataOutputObjectsDelegate {
    func setInitAVCapture() {
        self.captureSession = AVCaptureSession()
        self.captureSession.beginConfiguration()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        guard self.captureSession.canAddInput(videoInput) == true else {
            self.onFailed()
            return
        }
        self.captureSession.addInput(videoInput)
        
        let metadataOutput = AVCaptureMetadataOutput()
        guard self.captureSession.canAddOutput(metadataOutput) else {
            self.onFailed()
            return
        }
        self.captureSession.addOutput(metadataOutput)
        
        metadataOutput.metadataObjectTypes = [.qr]
        metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        
        self.captureSession.commitConfiguration()
        
        self.previewLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        self.previewLayer.frame = self.view_Camera.layer.bounds
        self.previewLayer.videoGravity = .resizeAspectFill
        self.view_Camera.layer.addSublayer(self.previewLayer)
        
        self.captureSession.startRunning()
        
        self.setupBoundingBox()
    }
    
    // MARK: Metadata Output
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        guard self.isScanner == false else { return }
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            
            if let transformedObject = self.previewLayer.transformedMetadataObject(for: metadataObject) as? AVMetadataMachineReadableCodeObject {
                self.updateBoundingBox(transformedObject.corners)
                self.hideBoundingBox(after: 0.25)
                self.onCheckFrameScanner(transformedObject: transformedObject)
            }
            
            guard self.isScanner else { return }
            guard let stringValue = readableObject.stringValue else { return }
            //AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            //self.captureSession.stopRunning()
            self.onFound(code: stringValue)
        }
    }
    
    func onFailed() {
        let controller = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(controller, animated: true)
        self.captureSession = nil
    }
    
    func onFound(code: String) {
        print("QR Code:", code)
        
        guard let data = code.toDictionary(), let app = data["app"] as? String, app == "1sk" else {
            self.isScanner = false
            return
        }
        guard let type = data["type"] as? String else {
            self.isScanner = false
            return
        }
        guard let token = data["token"] as? String else {
            self.isScanner = false
            return
        }
        
        switch type {
        case "login":
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            self.setGeneratingQRCode(code: code)
            
            self.viewModel.onGetInfoRequestLogin(token: token)
            
        default:
            self.isScanner = false
        }
    }
    
    func onCheckFrameScanner(transformedObject: AVMetadataMachineReadableCodeObject) {
        let pointCode_1 = transformedObject.bounds.origin
        let pointCode_2 = CGPoint(x: pointCode_1.x + transformedObject.bounds.size.width,
                                  y: pointCode_1.y + transformedObject.bounds.size.height)
        
        let pointScanner_1 = self.view_Scanner.frame.origin
        let pointScanner_2 = CGPoint(x: self.view_Scanner.frame.maxX,
                                     y: self.view_Scanner.frame.maxY)
        
        var point_1 = false
        var point_2 = false
        
        /// Check Point 1
        if pointCode_1.x > pointScanner_1.x && pointCode_1.y > pointScanner_1.y {
            point_1 = true
            
        } else {
            point_1 = false
        }
        
        /// Check Point 2
        if pointCode_2.x < pointScanner_2.x && pointCode_2.y < pointScanner_2.y {
            point_2 = true
            
        } else {
            point_2 = false
        }
        
        if point_1 == true && point_2 == true {
            self.isScanner = true
        }
    }
}

// MARK: Generating QR Code
extension ScannerViewController {
    func setGeneratingQRCode(code: String) {
        // black foreground and white background
        // Get define string to encode
        let myString = code //"https://pennlabs.org"
        
        // Get data from the string
        let data = myString.data(using: String.Encoding.ascii)
        
        // Get a QR CIFilter
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        
        // Input the data
        qrFilter.setValue(data, forKey: "inputMessage")
        
        // Get the output image
        guard let qrImage = qrFilter.outputImage else { return }
        
        // Scale the image
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        // Do some processing to get the UIImage
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        
        let processedImage = UIImage(cgImage: cgImage)
        
        self.img_Scanner.backgroundColor = UIColor.black
        self.img_Scanner.alpha = 0.6
        self.img_QrCode.image = processedImage
        
    }
}

// MARK: Bounding Box
extension ScannerViewController {
    // MARK - Bounding Box
    private func setupBoundingBox() {
        self.boundingBox.frame = self.view_Camera.layer.bounds
        self.boundingBox.strokeColor = UIColor.green.cgColor
        self.boundingBox.lineWidth = 2.0
        self.boundingBox.fillColor = UIColor.clear.cgColor
        
        self.view_Camera.layer.addSublayer(self.boundingBox)
    }
    
    fileprivate func updateBoundingBox(_ points: [CGPoint]) {
        guard let firstPoint = points.first else {
            return
        }
        
        let path = UIBezierPath()
        path.move(to: firstPoint)
        
        var newPoints = points
        newPoints.removeFirst()
        newPoints.append(firstPoint)
        
        newPoints.forEach { path.addLine(to: $0) }
        
        self.boundingBox.path = path.cgPath
        self.boundingBox.isHidden = false
    }
    
    fileprivate func hideBoundingBox(after: Double) {
        self.resetTimer?.invalidate()
        self.resetTimer = Timer.scheduledTimer(withTimeInterval: TimeInterval() + after, repeats: false) { [weak self] (timer) in
            self?.resetViews()
        }
    }
    
    private func resetViews() {
        self.boundingBox.isHidden = true
    }
}
