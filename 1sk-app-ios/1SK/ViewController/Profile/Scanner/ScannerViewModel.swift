//
//  
//  ScannerViewModel.swift
//  1SK
//
//  Created by Thaad on 09/05/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ScannerViewModelProtocol {
    func onViewDidLoad()
    func onGetInfoRequestLogin(token: String)
    func onSetConfirmRequestLogin(token: String, action: TypeConfirmRequest)
}

// MARK: - Scanner ViewModel
class ScannerViewModel {
    weak var view: ScannerViewProtocol?
    private var interactor: ScannerInteractorInputProtocol

    init(interactor: ScannerInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - Scanner ViewModelProtocol
extension ScannerViewModel: ScannerViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onGetInfoRequestLogin(token: String) {
        self.view?.showHud()
        self.interactor.getInfoRequestLogin(token: token)
    }
    
    func onSetConfirmRequestLogin(token: String, action: TypeConfirmRequest) {
        self.view?.showHud()
        self.interactor.setConfirmRequestLogin(token: token, action: action)
    }
}

// MARK: - Scanner InteractorOutputProtocol
extension ScannerViewModel: ScannerInteractorOutputProtocol {
    func onGetInfoRequestLoginFinished(with result: Result<LoginRequestModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onShowPopupConfirmLogin(model: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onSetConfirmRequestLoginFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            SKToast.shared.showToast(content: "Xác nhận đăng nhập thành công")
            self.view?.onDismissViewController()
            
        case .failure:
            SKToast.shared.showToast(content: "Xác nhận đăng nhập thất bại")
            self.view?.onResetScanner()
            
        }
        self.view?.hideHud()
    }
}
