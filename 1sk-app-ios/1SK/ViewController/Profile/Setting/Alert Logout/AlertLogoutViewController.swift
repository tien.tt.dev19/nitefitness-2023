//
//  AlertLogoutViewController.swift
//  1SK
//
//  Created by Thaad on 14/11/2022.
//

import UIKit

protocol AlertLogoutViewDelegate: AnyObject {
    func onLogoutAgreeAction()
}

class AlertLogoutViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    weak var delegate: AlertLogoutViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    @IBAction func onCancelAction(_ sender: Any) {
        self.setAnimationHidden()
    }
    
    @IBAction func onAgreeAction(_ sender: Any) {
        self.delegate?.onLogoutAgreeAction()
        self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertLogoutViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn) {
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ContentView.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertLogoutViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
}
