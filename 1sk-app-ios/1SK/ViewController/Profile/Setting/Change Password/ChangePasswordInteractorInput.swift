//
//  
//  ChangePasswordInteractorInput.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ChangePasswordInteractorInputProtocol {
    func setChangePassword(oldPassword: String, newPassword: String, reNewPassword: String)
}

// MARK: - Interactor Output Protocol
protocol ChangePasswordInteractorOutputProtocol: AnyObject {
    func onSetChangePasswordFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - ChangePassword InteractorInput
class ChangePasswordInteractorInput {
    weak var output: ChangePasswordInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - ChangePassword InteractorInputProtocol
extension ChangePasswordInteractorInput: ChangePasswordInteractorInputProtocol {
    func setChangePassword(oldPassword: String, newPassword: String, reNewPassword: String) {
        self.authService?.setChangePassword(oldPassword: oldPassword, newPassword: newPassword, reNewPassword: reNewPassword, completion: { [weak self] result in
            self?.output?.onSetChangePasswordFinished(with: result.unwrapSuccessModel())
        })
    }
}
