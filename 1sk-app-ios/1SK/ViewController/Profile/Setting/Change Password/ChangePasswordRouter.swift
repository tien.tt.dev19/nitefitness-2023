//
//  
//  ChangePasswordRouter.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ChangePasswordRouterProtocol {
    func popViewController()
}

// MARK: - ChangePassword Router
class ChangePasswordRouter {
    weak var viewController: ChangePasswordViewController?
    
    static func setupModule() -> ChangePasswordViewController {
        let viewController = ChangePasswordViewController()
        let router = ChangePasswordRouter()
        let interactorInput = ChangePasswordInteractorInput()
        let viewModel = ChangePasswordViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ChangePassword RouterProtocol
extension ChangePasswordRouter: ChangePasswordRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
