//
//  
//  ChangePasswordViewController.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ChangePasswordViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setErrorUI(message: String)
    func onAlertConfirmSuccess()
}

// MARK: - ChangePassword ViewController
class ChangePasswordViewController: BaseViewController {
    var router: ChangePasswordRouterProtocol!
    var viewModel: ChangePasswordViewModelProtocol!
    
    @IBOutlet weak var scroll_ScrollView: UIScrollView!
    
    @IBOutlet weak var tf_OldPassword: UITextField!
    @IBOutlet weak var tf_NewPassword: UITextField!
    @IBOutlet weak var tf_NewPasswordConfirm: UITextField!
    
    @IBOutlet weak var btn_ShowOldPassword: UIButton!
    @IBOutlet weak var btn_ShowNewPassword: UIButton!
    @IBOutlet weak var btn_ShowReNewPassword: UIButton!
    
    @IBOutlet weak var lbl_Error: UILabel!
    @IBOutlet weak var btn_Confirm: UIButton!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Cập nhật mật khẩu"
        
        self.setInitUITextField()
        self.setInitPasswordUI()
        self.setStateConfirmButton(isSelected: false)
        self.setInitGestureRecognizer()
    }
    
    func setInitPasswordUI() {
        self.btn_ShowOldPassword.setImage(R.image.ic_password_hidden(), for: .normal)
        self.btn_ShowOldPassword.setImage(R.image.ic_password_show(), for: .selected)
        
        self.btn_ShowNewPassword.setImage(R.image.ic_password_hidden(), for: .normal)
        self.btn_ShowNewPassword.setImage(R.image.ic_password_show(), for: .selected)
        
        self.btn_ShowReNewPassword.setImage(R.image.ic_password_hidden(), for: .normal)
        self.btn_ShowReNewPassword.setImage(R.image.ic_password_show(), for: .selected)
    }
    
    func setStateConfirmButton(isSelected: Bool) {
        if isSelected {
            self.btn_Confirm.backgroundColor = R.color.mainColor()
        } else {
            self.btn_Confirm.backgroundColor = R.color.mainDeselect()
        }
    }
    
    // MARK: - Action
    @IBAction func onShowOldPasswordAction(_ sender: Any) {
        self.btn_ShowOldPassword.isSelected = !self.btn_ShowOldPassword.isSelected
        self.tf_OldPassword.isSecureTextEntry = !self.btn_ShowOldPassword.isSelected
    }
    
    @IBAction func onShowNewPasswordAction(_ sender: Any) {
        self.btn_ShowNewPassword.isSelected = !self.btn_ShowNewPassword.isSelected
        self.tf_NewPassword.isSecureTextEntry = !self.btn_ShowNewPassword.isSelected
    }
    
    @IBAction func onShowReNewPasswordAction(_ sender: Any) {
        self.btn_ShowReNewPassword.isSelected = !self.btn_ShowReNewPassword.isSelected
        self.tf_NewPasswordConfirm.isSecureTextEntry = !self.btn_ShowReNewPassword.isSelected
    }
    
    @IBAction func onConfirmAction(_ sender: Any) {
        guard let oldPassword = self.tf_OldPassword.text, oldPassword.count >= 6, oldPassword.count <= 32 else {
            SKToast.shared.showToast(content: "Mật khẩu phải trong khoảng từ 6 đến 32 ký tự")
            self.tf_OldPassword.becomeFirstResponder()
            return
        }
        
        guard let newPassword = self.tf_NewPassword.text, newPassword.count >= 6, newPassword.count <= 32 else {
            SKToast.shared.showToast(content: "Mật khẩu phải trong khoảng từ 6 đến 32 ký tự")
            self.tf_NewPassword.becomeFirstResponder()
            return
        }
        guard let passwordConfirm = self.tf_NewPasswordConfirm.text, passwordConfirm.count >= 6, passwordConfirm.count <= 32 else {
            SKToast.shared.showToast(content: "Mật khẩu phải trong khoảng từ 6 đến 32 ký tự")
            self.tf_NewPasswordConfirm.becomeFirstResponder()
            return
        }
        guard newPassword == passwordConfirm else {
            SKToast.shared.showToast(content: "Xác nhận mật khẩu không khớp")
            self.tf_NewPasswordConfirm.becomeFirstResponder()
            return
        }
        
        self.lbl_Error.isHidden = true
        self.view.endEditing(true)
        self.viewModel.onConfirmAction(oldPassword: oldPassword, newPassword: newPassword, reNewPassword: passwordConfirm)
    }
    
}

// MARK: - ChangePassword ViewProtocol
extension ChangePasswordViewController: ChangePasswordViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setErrorUI(message: String) {
        self.lbl_Error.text = message
        self.lbl_Error.isHidden = false
    }
    
    func onAlertConfirmSuccess() {
        self.presentAlertConfirmSuccessVC(delegate: self)
    }
}

// MARK: - UITapGestureRecognizer
extension ChangePasswordViewController {
    func setInitGestureRecognizer() {
        self.scroll_ScrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.view.endEditing(true)
    }
}

// MARK: UITextFieldDelegate
extension ChangePasswordViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_OldPassword.delegate = self
        self.tf_NewPassword.delegate = self
        self.tf_NewPasswordConfirm.delegate = self
        
        self.tf_OldPassword.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.tf_NewPassword.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.tf_NewPasswordConfirm.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
    }
    
    @objc func onTextFieldDidChange(_ textField: UITextField) {
        self.lbl_Error.isHidden = true
        
        guard let password = self.tf_OldPassword.text, password.count >= 6, password.count < 32 else {
            self.setStateConfirmButton(isSelected: false)
            return
        }
        
        var isPasswordValidate = false
        if let password = self.tf_NewPassword.text, password.count >= 6, password.count < 32 {
            if let passwordConfirm = self.tf_NewPasswordConfirm.text, passwordConfirm == password {
                isPasswordValidate = true
            }
        }
        
        if isPasswordValidate {
            self.setStateConfirmButton(isSelected: true)
        } else {
            self.setStateConfirmButton(isSelected: false)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string.isBackSpace == false else { return true }
        
        switch textField {
        case self.tf_NewPassword, self.tf_NewPasswordConfirm:
            if textField.text?.count ?? 0 < 32 {
                return true
                
            } else {
                return false
            }
            
        default:
            return true
        }
    }
}

// MARK: - AlertConfirmSuccessViewDelegate
extension ChangePasswordViewController: AlertConfirmSuccessViewDelegate {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?) {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.titleAlert = "Đổi mật khẩu thành công"
        self.present(controller, animated: false)
    }
    
    func onDidDismissAlertConfirmSuccessView() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.router.popViewController()
        }
    }
}
