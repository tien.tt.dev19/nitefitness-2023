//
//  
//  ChangePasswordViewModel.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ChangePasswordViewModelProtocol {
    func onViewDidLoad()
    func onConfirmAction(oldPassword: String, newPassword: String, reNewPassword: String)
}

// MARK: - ChangePassword ViewModel
class ChangePasswordViewModel {
    weak var view: ChangePasswordViewProtocol?
    private var interactor: ChangePasswordInteractorInputProtocol

    init(interactor: ChangePasswordInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - ChangePassword ViewModelProtocol
extension ChangePasswordViewModel: ChangePasswordViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onConfirmAction(oldPassword: String, newPassword: String, reNewPassword: String) {
        self.view?.showHud()
        self.interactor.setChangePassword(oldPassword: oldPassword, newPassword: newPassword, reNewPassword: reNewPassword)
    }
}

// MARK: - ChangePassword InteractorOutputProtocol
extension ChangePasswordViewModel: ChangePasswordInteractorOutputProtocol {
    func onSetChangePasswordFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            self.view?.onAlertConfirmSuccess()
            
        case .failure(let error):
            self.view?.setErrorUI(message: error.message)
        }
        self.view?.hideHud()
    }
}
