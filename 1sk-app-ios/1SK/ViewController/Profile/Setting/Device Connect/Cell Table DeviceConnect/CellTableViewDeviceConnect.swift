//
//  CellTableViewDeviceConnect.swift
//  1SKConnect
//
//  Created by Thaad on 02/08/2022.
//

import UIKit

protocol CellTableViewDeviceConnectDelegate: AnyObject {
    func onDisconnectDeviceAction(indexPath: IndexPath?)
}

class CellTableViewDeviceConnect: UITableViewCell {

    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Mac: UILabel!
    
    @IBOutlet weak var btn_Connect: UIButton!
    
    weak var delegate: CellTableViewDeviceConnectDelegate?
    var indexPath: IndexPath?
    
    var device: DeviceRealm? {
        didSet {
            self.setDataUI()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func setDataUI() {
        self.lbl_Name.text = self.device?.name_display
        self.lbl_Mac.text = self.device?.mac
        
        switch self.device?.type {
        case .SCALES:
            self.img_Icon.image = R.image.ic_scale_device_connected()
            
        case .BP:
            self.img_Icon.image = R.image.ic_bp_device_connected()
            
        case .JUMP_ROPE:
            self.img_Icon.image = R.image.ic_jump_rope_device_connected()
            
        case .WATCH:
            self.img_Icon.image = R.image.ic_watch_device_connected()
            
        default:
            self.img_Icon.image = nil
        }
    }
    
    @IBAction func onDisconnectAction(_ sender: Any) {
        self.delegate?.onDisconnectDeviceAction(indexPath: self.indexPath)
    }
    
}
