//
//  
//  DeviceConnectInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 02/08/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol DeviceConnectInteractorInputProtocol {
    //
}

// MARK: - Interactor Output Protocol
protocol DeviceConnectInteractorOutputProtocol: AnyObject {
    //
}

// MARK: - DeviceConnect InteractorInput
class DeviceConnectInteractorInput {
    weak var output: DeviceConnectInteractorOutputProtocol?
}

// MARK: - DeviceConnect InteractorInputProtocol
extension DeviceConnectInteractorInput: DeviceConnectInteractorInputProtocol {
    //
}
