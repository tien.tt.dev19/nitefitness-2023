//
//  
//  DeviceConnectRouter.swift
//  1SKConnect
//
//  Created by Thaad on 02/08/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol DeviceConnectRouterProtocol {

}

// MARK: - DeviceConnect Router
class DeviceConnectRouter {
    weak var viewController: DeviceConnectViewController?
    
    static func setupModule() -> DeviceConnectViewController {
        let viewController = DeviceConnectViewController()
        let router = DeviceConnectRouter()
        let interactorInput = DeviceConnectInteractorInput()
        let viewModel = DeviceConnectViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - DeviceConnect RouterProtocol
extension DeviceConnectRouter: DeviceConnectRouterProtocol {
    
}
