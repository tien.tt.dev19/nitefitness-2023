//
//  
//  DeviceConnectViewController.swift
//  1SKConnect
//
//  Created by Thaad on 02/08/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol DeviceConnectViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
}

// MARK: - DeviceConnect ViewController
class DeviceConnectViewController: BaseViewController {
    var router: DeviceConnectRouterProtocol!
    var viewModel: DeviceConnectViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Thiết bị"
        self.setInitUITableView()
    }
    
    // MARK: - Action
    
}

// MARK: - DeviceConnect ViewProtocol
extension DeviceConnectViewController: DeviceConnectViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
}

// MARK: - UITableViewDataSource
extension DeviceConnectViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionDeviceConnect.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewDeviceConnect.self)
        self.tbv_TableView.registerNib(ofType: CellUITableViewNotFound.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionFooterHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionFooterHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderSectionDeviceConnect.self)
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.viewModel.listDeviceConnect.count > 0 {
            return self.viewModel.listDeviceConnect.count
            
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.viewModel.listDeviceConnect.count > 0 {
            let cell = tableView.dequeueCell(ofType: CellTableViewDeviceConnect.self, for: indexPath)
            cell.delegate = self
            cell.indexPath = indexPath
            cell.device = self.viewModel.listDeviceConnect[indexPath.row]
            return cell
            
        } else {
            let cell = tableView.dequeueCell(ofType: CellUITableViewNotFound.self, for: indexPath)
            return cell
        }
    }
}

// MARK: - UITableViewDelegate
extension DeviceConnectViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        guard self.viewModel.listDeviceConnect.count > 0 else {
            return
        }
        self.presentAlertDisconnectDeviceView(indexPath: indexPath)
    }
}

// MARK: CellTableViewDeviceConnectDelegate
extension DeviceConnectViewController: CellTableViewDeviceConnectDelegate {
    func onDisconnectDeviceAction(indexPath: IndexPath?) {
        self.presentAlertDisconnectDeviceView(indexPath: indexPath)
    }
}

// MARK: AlertDisconnectDeviceViewDelegate
extension DeviceConnectViewController: AlertDisconnectDeviceViewDelegate {
    func presentAlertDisconnectDeviceView(indexPath: IndexPath?) {
        let controller = AlertDisconnectDeviceViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.indexPath = indexPath
        self.present(controller, animated: false)
    }
    
    func onAgreeDisconnectAction(indexPath: IndexPath?) {
        self.viewModel.onAgreeDisconnectAction(indexPath: indexPath)
    }
    
}
