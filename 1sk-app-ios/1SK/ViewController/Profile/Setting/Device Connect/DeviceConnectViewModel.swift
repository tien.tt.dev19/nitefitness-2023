//
//  
//  DeviceConnectViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 02/08/2022.
//
//

import UIKit
import CoreBluetooth

// MARK: - ViewModelProtocol
protocol DeviceConnectViewModelProtocol {
    func onViewDidLoad()
    func onAgreeDisconnectAction(indexPath: IndexPath?)
    
    var listDeviceConnect: [DeviceRealm] { get set }
}

// MARK: - DeviceConnect ViewModel
class DeviceConnectViewModel {
    weak var view: DeviceConnectViewProtocol?
    private var interactor: DeviceConnectInteractorInputProtocol

    init(interactor: DeviceConnectInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listDeviceConnect: [DeviceRealm] = []
    
    func getDataDeviceConnect() {
        self.listDeviceConnect = gRealm.objects()
    }
}

// MARK: - DeviceConnect ViewModelProtocol
extension DeviceConnectViewModel: DeviceConnectViewModelProtocol {
    func onViewDidLoad() {
        self.getDataDeviceConnect()
    }
    
    func onAgreeDisconnectAction(indexPath: IndexPath?) {
        guard let index = indexPath?.row else { return }
        let device = self.listDeviceConnect[index]
        
        switch device.type {
        case .SCALES:
            if let peripheral = BluetoothManager.shared.peripheral {
                BluetoothManager.shared.setDisconnectDevice(peripheral: peripheral)
            }
        default:
            break
        }
        
        self.listDeviceConnect.remove(at: index)
        gRealm.remove(device)
        
        self.view?.onReloadData()
    }
}

// MARK: - DeviceConnect InteractorOutputProtocol
extension DeviceConnectViewModel: DeviceConnectInteractorOutputProtocol {
    //
}
