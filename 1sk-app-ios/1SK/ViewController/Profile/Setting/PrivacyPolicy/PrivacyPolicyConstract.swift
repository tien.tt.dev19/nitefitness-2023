//
//  
//  PrivacyPolicyConstract.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

// MARK: View -
protocol PrivacyPolicyViewProtocol: class {

}

// MARK: Interactor -
protocol PrivacyPolicyInteractorInputProtocol {

}

protocol PrivacyPolicyInteractorOutputProtocol: class {
}
// MARK: Presenter -
protocol PrivacyPolicyPresenterProtocol {
    func onViewDidLoad()
}

// MARK: Router -
protocol PrivacyPolicyRouterProtocol {

}
