//
//  
//  PrivacyPolicyInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class PrivacyPolicyInteractorInput {
    weak var output: PrivacyPolicyInteractorOutputProtocol?
}

// MARK: - PrivacyPolicyInteractorInput - PrivacyPolicyInteractorInputProtocol -
extension PrivacyPolicyInteractorInput: PrivacyPolicyInteractorInputProtocol {

}
