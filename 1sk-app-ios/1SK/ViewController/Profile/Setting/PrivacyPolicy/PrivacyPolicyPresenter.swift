//
//  
//  PrivacyPolicyPresenter.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class PrivacyPolicyPresenter {

    weak var view: PrivacyPolicyViewProtocol?
    private var interactor: PrivacyPolicyInteractorInputProtocol
    private var router: PrivacyPolicyRouterProtocol

    init(interactor: PrivacyPolicyInteractorInputProtocol,
         router: PrivacyPolicyRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - extending PrivacyPolicyPresenter: PrivacyPolicyPresenterProtocol -
extension PrivacyPolicyPresenter: PrivacyPolicyPresenterProtocol {
    func onViewDidLoad() {
    }
}

// MARK: - PrivacyPolicyPresenter: PrivacyPolicyInteractorOutput -
extension PrivacyPolicyPresenter: PrivacyPolicyInteractorOutputProtocol {

}
