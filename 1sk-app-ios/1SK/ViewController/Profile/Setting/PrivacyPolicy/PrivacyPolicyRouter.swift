//
//  
//  PrivacyPolicyRouter.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class PrivacyPolicyRouter {
    weak var viewController: UIViewController?
    static func setupModule() -> PrivacyPolicyViewController {
        let viewController = PrivacyPolicyViewController()
        viewController.hidesBottomBarWhenPushed = true
        let router = PrivacyPolicyRouter()
        let interactorInput = PrivacyPolicyInteractorInput()
        let presenter = PrivacyPolicyPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - PrivacyPolicyRouter: PrivacyPolicyRouterProtocol -
extension PrivacyPolicyRouter: PrivacyPolicyRouterProtocol {
}
