//
//  
//  PrivacyPolicyViewController.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class PrivacyPolicyViewController: BaseViewController {

    var presenter: PrivacyPolicyPresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.onViewDidLoad()
    }

    private func setupUI() {
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        navigationItem.title = "Chính sách bảo mật"
    }
}

// MARK: PrivacyPolicyViewController - PrivacyPolicyViewProtocol -
extension PrivacyPolicyViewController: PrivacyPolicyViewProtocol {
}
