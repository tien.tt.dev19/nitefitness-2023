//
//  CellTableViewProfileList.swift
//  1SKConnect
//
//  Created by Thaad on 09/09/2022.
//

import UIKit

class CellTableViewProfileList: UITableViewCell {
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var img_Avatar: UIImageView!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    
    var model: AuthModel? {
        didSet {
            self.lbl_Name.text = self.model?.customer?.fullName
            self.img_Avatar.setImageWith(imageUrl: self.model?.customer?.avatar ?? "", placeHolder: R.image.default_avatar_2())
            
            if self.model?.accountType == .Account {
                self.lbl_Phone.text = "Tôi"
            } else {
                self.lbl_Phone.text = ""
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if selected {
            self.view_Bg.backgroundColor = UIColor.init(hex: "F1F1F1")
        } else {
            self.view_Bg.backgroundColor = .white
        }
    }
    
}
