//
//  
//  CreateHealthProfileInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 27/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol CreateHealthProfileInteractorInputProtocol {
    func setCreateSubAccount(newUser: UserModel, avatar: UIImage?, createdBy: Int)
}

// MARK: - Interactor Output Protocol
protocol CreateHealthProfileInteractorOutputProtocol: AnyObject {
    func onSetCreateSubAccountFinished(with result: Result<AuthModel, APIError>)
}

// MARK: - CreateHealthProfile InteractorInput
class CreateHealthProfileInteractorInput {
    weak var output: CreateHealthProfileInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - CreateHealthProfile InteractorInputProtocol
extension CreateHealthProfileInteractorInput: CreateHealthProfileInteractorInputProtocol {
    func setCreateSubAccount(newUser: UserModel, avatar: UIImage?, createdBy: Int) {
        self.authService?.setSubAccount(newUser: newUser, avatar: avatar, createdBy: createdBy, completion: { [weak self] result in
            self?.output?.onSetCreateSubAccountFinished(with: result.unwrapSuccessModel())
        })
    }
}
