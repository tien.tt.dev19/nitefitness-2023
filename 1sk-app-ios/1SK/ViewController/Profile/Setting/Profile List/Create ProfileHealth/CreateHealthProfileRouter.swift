//
//  
//  CreateHealthProfileRouter.swift
//  1SKConnect
//
//  Created by Thaad on 27/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol CreateHealthProfileRouterProtocol {
    func setRootTabbarViewController()
}

// MARK: - CreateHealthProfile Router
class CreateHealthProfileRouter {
    weak var viewController: CreateHealthProfileViewController?
    
    static func setupModule(userParent: UserModel?, delegate: CreateHealthProfileViewDelegate?) -> CreateHealthProfileViewController {
        let viewController = CreateHealthProfileViewController()
        let router = CreateHealthProfileRouter()
        let interactorInput = CreateHealthProfileInteractorInput()
        let viewModel = CreateHealthProfileViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.userParent = userParent
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - CreateHealthProfile RouterProtocol
extension CreateHealthProfileRouter: CreateHealthProfileRouterProtocol {
    func setRootTabbarViewController() {
//        UIApplication.shared.keyWindow?.rootViewController = TabBarViewController()
//        UIApplication.shared.keyWindow?.makeKeyAndVisible()
    }
}
