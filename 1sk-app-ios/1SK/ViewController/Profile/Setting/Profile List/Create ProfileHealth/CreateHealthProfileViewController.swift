//
//  
//  CreateHealthProfileViewController.swift
//  1SKConnect
//
//  Created by Thaad on 27/06/2022.
//
//

import UIKit
import DLRadioButton
import IQKeyboardManagerSwift

protocol CreateHealthProfileViewDelegate: AnyObject {
    func onCreateSubAccountSuccess(auth: AuthModel?)
}

// MARK: - ViewProtocol
protocol CreateHealthProfileViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setDataUser(user: UserModel?)
    func setBtnLoginStateUI(isSelected: Bool)
    
    func onCreateSubAccountSuccess(auth: AuthModel?)
    func setRootTabbarViewController()
}

// MARK: - CreateHealthProfile ViewController
class CreateHealthProfileViewController: BaseViewController, ImagePickable {
    var router: CreateHealthProfileRouterProtocol!
    var viewModel: CreateHealthProfileViewModelProtocol!
    weak var delegate: CreateHealthProfileViewDelegate?
    
    @IBOutlet weak var view_Scroll: UIScrollView!
    
    @IBOutlet weak var view_Avatar: UIView!
    @IBOutlet weak var img_Avatar: UIImageView!
    
    @IBOutlet weak var tf_Name: SKTextField!
    @IBOutlet weak var tf_Birthday: SKTextField!
    @IBOutlet weak var tf_Height: SKTextField!
    @IBOutlet weak var tf_Weight: SKTextField!
    @IBOutlet weak var tf_Blood: UITextField!
    
    @IBOutlet weak var btn_Male: DLRadioButton!
    @IBOutlet weak var btn_Female: DLRadioButton!
    @IBOutlet weak var btn_Save: UIButton!
    
    private var sk_tf_Name: UITextField {
        return self.tf_Name.titleTextField
    }
    
    private var sk_tf_Birthday: UITextField {
        return self.tf_Birthday.titleTextField
    }
    
    private var sk_tf_Height: UITextField {
        return self.tf_Height.titleTextField
    }

    private var sk_tf_Weight: UITextField {
        return self.tf_Weight.titleTextField
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUI()
        self.setInitGestureView()
        self.setInitUITextField()
        self.setInitRadioButton()
        self.setInitKeyboardToolBar()
    }
    
    func setInitUI() {
        self.btn_Save.setTitleColor(.lightGray, for: .normal)
        self.btn_Save.setTitleColor(.white, for: .selected)
    }
    
    func onValidateStateButtonSave() {
        guard self.sk_tf_Name.text?.count ?? 0 >= 2 else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        guard self.sk_tf_Name.text?.count ?? 0 <= 32 else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        guard self.sk_tf_Birthday.text?.count ?? 0 > 5 else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        guard self.btn_Male.isSelected == true || self.btn_Female.isSelected == true else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        
        guard let textHeight = self.sk_tf_Height.text?.replaceCharacter(target: " cm", withString: "") else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        guard let height = Int(textHeight), height >= 50 && height <= 300 else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        guard let textWeight = self.sk_tf_Weight.text?.replaceCharacter(target: " kg", withString: "") else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        guard let weight = Double(textWeight.replaceCharacter(target: ",", withString: ".")), weight > 0 && weight <= 200 else {
            self.setBtnLoginStateUI(isSelected: false)
            return
        }
        
        self.setBtnLoginStateUI(isSelected: true)
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func onSaveAction(_ sender: Any) {
        guard self.viewModel.isValidateForm() == true else {
            return
        }
        guard self.btn_Save.isSelected == true else {
            return
        }
        self.view.endEditing(true)
        self.viewModel.onSaveAction()
    }
}

// MARK: - CreateHealthProfile ViewProtocol
extension CreateHealthProfileViewController: CreateHealthProfileViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setDataUser(user: UserModel?) {
        self.sk_tf_Name.text = user?.fullName
        self.sk_tf_Birthday.text = user?.birthday
        self.tf_Blood.text = user?.blood
        
        if let height = user?.height {
            self.sk_tf_Height.text = "\(Int(height)) cm"
        }
        if let weight = user?.weight {
            self.sk_tf_Weight.text = "\(weight) kg"
        }
        
        // 0: Nam, 1: Nu, 2: Khong xac dinh
        switch user?.gender {
        case .male:
            self.btn_Male.isSelected = true
            self.btn_Female.isSelected = false
            
        case .female:
            self.btn_Male.isSelected = false
            self.btn_Female.isSelected = true
            
        default:
            self.btn_Male.isSelected = false
            self.btn_Female.isSelected = false
        }
        
        self.img_Avatar.setImageWith(imageUrl: user?.avatar ?? "", placeHolder: R.image.ic_default_avatar())
        
        for textField in [self.sk_tf_Name, self.sk_tf_Birthday, self.sk_tf_Height, self.sk_tf_Weight] {
            self.setStateLabelHidden(of: textField)
        }
    }
    
    func setBtnLoginStateUI(isSelected: Bool) {
        self.btn_Save.isSelected = isSelected
        if isSelected {
            self.btn_Save.backgroundColor = R.color.mainColor()
        } else {
            self.btn_Save.backgroundColor = R.color.background()
        }
    }
    
    func onCreateSubAccountSuccess(auth: AuthModel?) {
        self.dismiss(animated: true) {
            self.delegate?.onCreateSubAccountSuccess(auth: auth)
        }
    }
    
    func setRootTabbarViewController() {
        self.router.setRootTabbarViewController()
    }
}

// MARK: - UIGestureRecognizer
extension CreateHealthProfileViewController {
    func setInitGestureView() {
        self.view_Avatar.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapAvatarAction)))
    }
    
    @objc func onTapAvatarAction() {
        self.showSelectedImageSourceAlert(with: "Chọn ảnh từ", popoverRect: self.img_Avatar.frame, popoverView: self.view, allowsEditing: true)
    }
}

// MARK: - UIImagePickerViewDelegate
extension CreateHealthProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
        
        self.viewModel.onAvatarChanged(with: image)
        self.img_Avatar.image = image
        
        self.onValidateStateButtonSave()
    }
}

// MARK: - UITextFieldDelegate
extension CreateHealthProfileViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.sk_tf_Name.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Height.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Weight.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        
        self.tf_Name.setContentType(.name)
        self.tf_Name.setKeyboardType(.alphabet)
        self.tf_Height.setKeyboardType(.decimalPad)
        self.tf_Height.setKeyboardType(.decimalPad)
        self.tf_Weight.setKeyboardType(.decimalPad)
        
        self.sk_tf_Name.delegate = self
        self.sk_tf_Birthday.delegate = self
        self.sk_tf_Height.delegate = self
        self.sk_tf_Weight.delegate = self
        self.tf_Blood.delegate = self

        self.tf_Name.setEnable(isEnable: true)
        self.tf_Birthday.setEnable(isEnable: true)
        self.tf_Height.setEnable(isEnable: true)
        self.tf_Weight.setEnable(isEnable: true)
        
        self.tf_Name.updateTypeLabelHiddenState()
        self.tf_Birthday.updateTypeLabelHiddenState()
        self.tf_Height.updateTypeLabelHiddenState()
        self.tf_Weight.updateTypeLabelHiddenState()
        
        self.sk_tf_Name.clearButtonMode = .whileEditing
        self.sk_tf_Birthday.clearButtonMode = .whileEditing
        self.sk_tf_Height.clearButtonMode = .whileEditing
        self.sk_tf_Weight.clearButtonMode = .whileEditing
    }
    
    @objc func onTextFieldDidChange(_ textField: UITextField) {
        let newText = textField.text ?? ""
        print("onTextFieldDidChange newText:", newText)
        
        switch textField {
        case self.sk_tf_Name:
            self.viewModel.onUserNameChanged(with: newText)
            
        case self.sk_tf_Height:
            self.viewModel.onDidChangeHeight(value: newText)
            
        case self.sk_tf_Weight:
            self.viewModel.onDidChangeWeight(value: newText)
            
        default:
            break
        }
        
        self.onValidateStateButtonSave()
    }
    
    private func setStateLabelHidden(of textField: UITextField) {
        switch textField {
        case self.sk_tf_Name:
            self.tf_Name.updateTypeLabelHiddenState()
            
        case self.sk_tf_Birthday:
            self.tf_Birthday.updateTypeLabelHiddenState()
            
        case self.sk_tf_Height:
            self.tf_Height.updateTypeLabelHiddenState()

        case self.sk_tf_Weight:
            self.tf_Weight.updateTypeLabelHiddenState()

        default:
            break
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case self.sk_tf_Birthday:
            self.view.endEditing(true)
            self.showAlertCalendarDateViewAction()
            return false
            
        case self.sk_tf_Height:
            if textField.text?.contains(" cm") ?? false {
                textField.text?.removeLast(3)
            }
            return true
            
        case self.sk_tf_Weight:
            if textField.text?.contains(" kg") ?? false {
                textField.text?.removeLast(3)
            }
            return true
            
        case self.tf_Blood:
            self.showAlertPickerViewAction()
            return false
            
        default:
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.setStateLabelHidden(of: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.setStateLabelHidden(of: textField)
        
        textField.text = textField.text?.components(separatedBy: .whitespacesAndNewlines).filter { $0.count > 0 }.joined(separator: " ")
        let newText = textField.text ?? ""
        print("onTextFieldDidChange newText:", newText)
        
        switch textField {
        case self.sk_tf_Name:
            self.viewModel.onUserNameChanged(with: newText)
            
        case self.sk_tf_Height:
            self.viewModel.onDidChangeHeight(value: newText)
            
            if let content = self.sk_tf_Height.text, !content.contains(" cm") && !content.isEmpty {
                self.sk_tf_Height.text?.append(" cm")
            }
            
        case self.sk_tf_Weight:
            guard let valueDouble = Double(newText.replaceCharacter(target: ",", withString: ".")) else {
                return
            }
            let valueRound = valueDouble.rounded(toPlaces: 2)
            let valueText = "\(valueRound)"
            self.sk_tf_Weight.text = valueText
            self.viewModel.onDidChangeWeight(value: valueText)
            
            if let content = self.sk_tf_Weight.text, !content.contains(" kg") && !content.isEmpty {
                self.sk_tf_Weight.text?.append(" kg")
            }
            
        default:
            break
        }
        
        self.onValidateStateButtonSave()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.sk_tf_Name:
            if textField.text?.count ?? 0 <= 32 {
                return true
            } else {
                return false
            }
            
        case self.sk_tf_Height:
            guard string.isNumberInt else {
                return false
            }
            if textField.text?.count ?? 0 < 3 {
                return true
            } else {
                return false
            }
            
        case self.sk_tf_Weight:
            if textField.text?.count ?? 0 < 5 {
                return true
            } else {
                return false
            }
            
        default:
            return true
        }
    }
}

// MARK: - AlertCalendarDateViewDelegate
extension CreateHealthProfileViewController: AlertDatePickerViewDelegate {
    func showAlertCalendarDateViewAction() {
        let controller = AlertDatePickerViewController()
        controller.modalPresentationStyle = .custom
        controller.alertTitle = "Chọn ngày sinh"
        controller.dateSelect = self.sk_tf_Birthday.text ?? ""
        controller.delegate = self
        
        self.present(controller, animated: true)
    }
    
    func onAlertDatePickerCancelAction() {
        //
    }
    
    func onAlertDatePickerAgreeAction(day: String) {
        self.sk_tf_Birthday.text = day
        self.tf_Birthday.updateTypeLabelHiddenState()
        self.viewModel.onBirthDayChanged(with: day)
        
        self.onValidateStateButtonSave()
    }
}

// MARK: - InitRadioButton
extension CreateHealthProfileViewController {
    private func setInitRadioButton() {
        self.btn_Male.addTarget(self, action: #selector(self.onMaleAction), for: .touchUpInside)
        self.btn_Female.addTarget(self, action: #selector(self.onFemaleAction), for: .touchUpInside)
        
        self.onMaleAction()
    }

    @objc private func onMaleAction() {
        self.btn_Male.isSelected = true
        self.btn_Female.isSelected = false
        
        self.viewModel.onGenderChanged(with: .male)
        self.onValidateStateButtonSave()
    }

    @objc private func onFemaleAction() {
        self.btn_Male.isSelected = false
        self.btn_Female.isSelected = true
        
        self.viewModel.onGenderChanged(with: .female)
        self.onValidateStateButtonSave()
    }
}

// MARK: - AlertPickerViewDelegate
extension CreateHealthProfileViewController: AlertPickerViewDelegate {
    func showAlertPickerViewAction() {
        var listItem: [ItemPickerView] = []
        for (index, blood) in BloodGroup.allCases.enumerated() {
            let item = ItemPickerView()
            item.id = index
            item.name = blood.name
            if blood.name == self.tf_Blood.text {
                item.isSelected = true
            } else {
                item.isSelected = false
            }
            
            listItem.append(item)
        }
        
        let controller = AlertPickerViewViewController()
        controller.modalPresentationStyle = .custom
        controller.alertTitle = "Chọn nhóm máu"
        controller.delegate = self
        controller.listItemPickerView = listItem
        self.present(controller, animated: true)
    }
    
    func onAlertCancelAction() {
        //
    }
    
    func onAlertAgreeAction(item: ItemPickerView) {
        let blood = BloodGroup.allCases[item.id ?? 0]
        self.tf_Blood.text = blood.name
        self.viewModel.onDidChangeBlood(value: blood.name)
        self.onValidateStateButtonSave()
    }
}

// MARK: - InitKeyboardToolBar
extension CreateHealthProfileViewController {
    private func setInitKeyboardToolBar() {
        self.view_Scroll.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapBgAction)))
        
        let name = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onNameDoneAction(_:)))
        self.sk_tf_Name.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập họ tên người dùng", rightBarButtonConfiguration: name)
        
        let height = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onHeightDoneAction(_:)))
        self.sk_tf_Height.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số chiều cao", rightBarButtonConfiguration: height)
        
        let weight = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onWeightDoneAction(_:)))
        self.sk_tf_Weight.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập chỉ số cân nặng", rightBarButtonConfiguration: weight)
    }
    
    @objc func onTapBgAction() {
        self.view.endEditing(true)
    }
    
    @objc func onNameDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @objc func onHeightDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @objc func onWeightDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
}
