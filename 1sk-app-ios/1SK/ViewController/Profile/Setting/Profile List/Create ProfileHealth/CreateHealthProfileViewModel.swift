//
//  
//  CreateHealthProfileViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 27/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol CreateHealthProfileViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onSaveAction()
    
    // Value
    func onAvatarChanged(with avatar: UIImage?)
    
    func onUserNameChanged(with name: String)
    func onBirthDayChanged(with birthday: String)
    func onGenderChanged(with gender: Gender)
    
    func onDidChangeWeight(value: String?)
    func onDidChangeHeight(value: String?)
    func onDidChangeBlood(value: String?)
    
    func isValidateForm() -> Bool
}

// MARK: - CreateHealthProfile ViewModel
class CreateHealthProfileViewModel {
    weak var view: CreateHealthProfileViewProtocol?
    private var interactor: CreateHealthProfileInteractorInputProtocol

    init(interactor: CreateHealthProfileInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var userParent: UserModel?

    private var newUser = UserModel()
    private var newAvatar: UIImage?
    
}

// MARK: - CreateHealthProfile ViewModelProtocol
extension CreateHealthProfileViewModel: CreateHealthProfileViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    // Action
    func onSaveAction() {
        guard let parentId = userParent?.id else { return }
        print("setCreateSubAccount parent.id: \(parentId) - name: \(self.userParent?.fullName ?? "null")")
        
        self.view?.showHud()
        self.interactor.setCreateSubAccount(newUser: self.newUser, avatar: self.newAvatar, createdBy: parentId)
        
    }
    
    // Value
    func onAvatarChanged(with avatar: UIImage?) {
        self.newAvatar = avatar
    }

    func onUserNameChanged(with name: String) {
        self.newUser.fullName = name
    }

    func onBirthDayChanged(with birthday: String) {
        self.newUser.birthday = birthday
    }

    func onGenderChanged(with gender: Gender) {
        self.newUser.gender = gender
    }

    // PHR Action
    func onDidChangeWeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        guard let weight = Double(value.replaceCharacter(target: ",", withString: ".")) else {
            return
        }
        self.newUser.weight = weight
    }
    
    func onDidChangeHeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        guard let height = Double(value) else {
            return
        }
        self.newUser.height = height
    }
    
    func onDidChangeBlood(value: String?) {
        guard let `value` = value else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.newUser.blood = value
    }
    
    func isValidateForm() -> Bool {
        guard self.newUser.fullName?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Nhập họ tên không đúng\nvui lòng kiểm tra lại")
            return false
        }
        guard self.newUser.fullName?.count ?? 0 <= 32 else {
            SKToast.shared.showToast(content: "Nhập họ tên không đúng\nvui lòng kiểm tra lại")
            return false
        }
        guard self.newUser.birthday?.count ?? 0 > 5 else {
            SKToast.shared.showToast(content: "Nhập ngày sinh không đúng\nvui lòng kiểm tra lại")
            return false
        }
        guard let gender = self.newUser.gender, gender.id < 2 else {
            SKToast.shared.showToast(content: "Vui lòng chọn giới tính của bạn để tiếp tục")
            return false
        }
        guard let height = self.newUser.height, height >= 50 else {
            SKToast.shared.showToast(content: "Nhập chiều cao không đúng\nvui lòng kiểm tra lại")
            return false
        }
        guard let weight = self.newUser.weight, weight > 0 else {
            SKToast.shared.showToast(content: "Nhập cân nặng không đúng\nvui lòng kiểm tra lại")
            return false
        }
        
        return true
    }
}

// MARK: - CreateHealthProfile InteractorOutputProtocol
extension CreateHealthProfileViewModel: CreateHealthProfileInteractorOutputProtocol {
    func onSetCreateSubAccountFinished(with result: Result<AuthModel, APIError>) {
        switch result {
        case .success( let model):
            self.view?.hideHud()
            self.view?.onCreateSubAccountSuccess(auth: model)
            
        case .failure(let error):
            self.view?.hideHud()
            SKToast.shared.showToast(content: "Lỗi\n\(error.message)")
        }
    }
    
}
