//
//  
//  ProfileListInteractorInput.swift
//  1SKConnect
//
//  Created by Thaad on 09/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ProfileListInteractorInputProtocol {
    func getSubAccounts()
}

// MARK: - Interactor Output Protocol
protocol ProfileListInteractorOutputProtocol: AnyObject {
    func onGetSubAccountsFinished(with result: Result<[AuthModel], APIError>)
}

// MARK: - ProfileList InteractorInput
class ProfileListInteractorInput {
    weak var output: ProfileListInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - ProfileList InteractorInputProtocol
extension ProfileListInteractorInput: ProfileListInteractorInputProtocol {
    func getSubAccounts() {
        self.authService?.getSubAccounts(completion: { [weak self] result in
            self?.output?.onGetSubAccountsFinished(with: result.unwrapSuccessModel())
        })
    }
}
