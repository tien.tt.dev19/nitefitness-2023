//
//  
//  ProfileListRouter.swift
//  1SKConnect
//
//  Created by Thaad on 09/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ProfileListRouterProtocol {
    func presentUpdateProfileRouter(delegate: UpdateProfileViewDelegate?)
    func presentUpdateSubProfileRouter(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?)
    func presentCreateHealthProfileRouter(userParent: UserModel?, delegate: CreateHealthProfileViewDelegate?)
}

// MARK: - ProfileList Router
class ProfileListRouter {
    weak var viewController: ProfileListViewController?
    
    static func setupModule() -> ProfileListViewController {
        let viewController = ProfileListViewController()
        let router = ProfileListRouter()
        let interactorInput = ProfileListInteractorInput()
        let viewModel = ProfileListViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ProfileList RouterProtocol
extension ProfileListRouter: ProfileListRouterProtocol {
    func presentUpdateProfileRouter(delegate: UpdateProfileViewDelegate?) {
        let controller = UpdateProfileRouter.setupModule(with: delegate)
        self.viewController?.present(controller, animated: true)
    }
    
    func presentUpdateSubProfileRouter(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?) {
        let controller = UpdateSubProfileRouter.setupModule(authModel: authModel, delegate: delegate)
        self.viewController?.present(controller, animated: true, completion: nil)
    }
    
    func presentCreateHealthProfileRouter(userParent: UserModel?, delegate: CreateHealthProfileViewDelegate?) {
        let controller = CreateHealthProfileRouter.setupModule(userParent: userParent, delegate: delegate)
        self.viewController?.present(controller, animated: true, completion: nil)
    }
}
