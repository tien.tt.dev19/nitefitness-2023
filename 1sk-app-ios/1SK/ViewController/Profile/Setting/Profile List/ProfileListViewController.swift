//
//  
//  ProfileListViewController.swift
//  1SKConnect
//
//  Created by Thaad on 09/09/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ProfileListViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    //UITableView
    func onReloadData()
}

// MARK: - ProfileList ViewController
class ProfileListViewController: BaseViewController {
    var router: ProfileListRouterProtocol!
    var viewModel: ProfileListViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Hồ sơ sức khỏe"
        self.setInitUIBarButtonItem()
        self.setInitUITableView()
    }
    
}

// MARK: - UIBarButtonItem
extension ProfileListViewController {
    private func setInitUIBarButtonItem() {
        let btn_Add = UIBarButtonItem(image: R.image.ic_add_account_2(), style: .plain, target: self, action: #selector(self.onAddAccountAction))
        self.navigationItem.setRightBarButton(btn_Add, animated: true)
    }
    
    @objc func onAddAccountAction() {
//        guard self.viewModel.listSubAccount.count < 10 else {
//            SKToast.shared.showToast(content: "Chỉ tạo được tối đa 10 hồ sơ sức khỏe")
//            return
//        }
        guard let userParent = self.viewModel.listSubAccount.first(where: {$0.accountType == .Account})?.customer else {
            return
        }
        self.router.presentCreateHealthProfileRouter(userParent: userParent, delegate: self)
    }
}

// MARK: - ProfileList ViewProtocol
extension ProfileListViewController: ProfileListViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        
        if let index = self.viewModel.listSubAccount.firstIndex(where: {$0.customer?.id == gUser?.id}) {
            let indexPath = IndexPath(row: index, section: 0)
            self.tbv_TableView.selectRow(at: indexPath, animated: true, scrollPosition: .bottom)
        }
    }
}

// MARK: - UITableViewDataSource
extension ProfileListViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewProfileList.self)
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listSubAccount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewProfileList.self, for: indexPath)
        cell.model = self.viewModel.listSubAccount[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ProfileListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let authModel = self.viewModel.listSubAccount[indexPath.row]
        
//        if authModel.accountType == .Account {
//            self.router.presentUpdateProfileRouter(delegate: self)
//
//        } else {
//            self.router.presentUpdateSubProfileRouter(authModel: authModel, delegate: self)
//        }
        
        self.router.presentUpdateSubProfileRouter(authModel: authModel, delegate: self)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.onReloadData()
        }
    }
}

// MARK: - CreateHealthProfileViewDelegate
extension ProfileListViewController: CreateHealthProfileViewDelegate {
    func onCreateSubAccountSuccess(auth: AuthModel?) {
        self.viewModel.onReloadSubAccount()
    }
}

// MARK: UpdateSubProfilePresenterDelegate
extension ProfileListViewController: UpdateSubProfileDelegate {
    func onUpdateProfileSuccess() {
        self.viewModel.onReloadSubAccount()
        SKToast.shared.showToast(content: "Cập nhật hồ sơ thành công")
    }
    
    func onDeleteProfileSuccess() {
        if let authModel = self.viewModel.listSubAccount.first(where: {$0.accountType == .Account}),
            let userModel = authModel.customer {
            Configure.shared.setDataAuthSwitch(subAccount: authModel)
            Configure.shared.setDataUser(userModel: userModel)
        }
        
        self.viewModel.onReloadSubAccount()
        SKToast.shared.showToast(content: "Xóa hồ hồ sơ thành công")
    }
}

// MARK: UpdateProfilePresenterDelegate
extension ProfileListViewController: UpdateProfileViewDelegate {
    func onSaveUserInfoChangeSuccess() {
        self.viewModel.onReloadSubAccount()
        SKToast.shared.showToast(content: "Cập nhật hồ sơ thành công")
    }
}
