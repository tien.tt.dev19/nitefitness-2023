//
//  
//  ProfileListViewModel.swift
//  1SKConnect
//
//  Created by Thaad on 09/09/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ProfileListViewModelProtocol {
    func onViewDidLoad()
    func onReloadSubAccount()
    
    var listSubAccount: [AuthModel] { get set }
}

// MARK: - ProfileList ViewModel
class ProfileListViewModel {
    weak var view: ProfileListViewProtocol?
    private var interactor: ProfileListInteractorInputProtocol

    init(interactor: ProfileListInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listSubAccount: [AuthModel] = []
}

// MARK: - ProfileList ViewModelProtocol
extension ProfileListViewModel: ProfileListViewModelProtocol {
    func onViewDidLoad() {
        self.view?.showHud()
        self.interactor.getSubAccounts()
    }
    
    func onReloadSubAccount() {
        self.interactor.getSubAccounts()
    }
}

// MARK: - ProfileList InteractorOutputProtocol
extension ProfileListViewModel: ProfileListInteractorOutputProtocol {
    func onGetSubAccountsFinished(with result: Result<[AuthModel], APIError>) {
        switch result {
        case .success(let model):
            self.listSubAccount = model
            self.view?.onReloadData()

        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
