//
//  
//  UpdateSubProfileInteractorInput.swift
//  1SK
//
//  Created by Thaad on 20/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol UpdateSubProfileInteractorInputProtocol {
    func getUserInfo(accessToken: String?)
    func setUpdateUserInfo(newUser: UserModel, newAvatar: UIImage?, accessToken: String?)
    func setDeleteProfile(accessToken: String?)
}

// MARK: - Interactor Output Protocol
protocol UpdateSubProfileInteractorOutputProtocol: AnyObject {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>)
    func onUpdateUserInfoFinished(with result: Result<UserModel, APIError>)
    func onSetDeleteProfileFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - UpdateSubProfile InteractorInput
class UpdateSubProfileInteractorInput {
    weak var output: UpdateSubProfileInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - UpdateSubProfile InteractorInputProtocol
extension UpdateSubProfileInteractorInput: UpdateSubProfileInteractorInputProtocol {
    func getUserInfo(accessToken: String?) {
        self.authService?.getUserInfo(accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onGetUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setUpdateUserInfo(newUser: UserModel, newAvatar: UIImage?, accessToken: String?) {
        self.authService?.setUpdateSubProfile(newUser: newUser, image: newAvatar, accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onUpdateUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setDeleteProfile(accessToken: String?) {
        self.authService?.setDeleteProfile(accessToken: accessToken, completion: { [weak self] result in
            self?.output?.onSetDeleteProfileFinished(with: result.unwrapSuccessModel())
        })
    }
}
