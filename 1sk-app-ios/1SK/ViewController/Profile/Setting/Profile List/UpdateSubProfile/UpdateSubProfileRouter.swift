//
//  
//  UpdateSubProfileRouter.swift
//  1SK
//
//  Created by Thaad on 20/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol UpdateSubProfileRouterProtocol {
    func onDismiss()
}

// MARK: - UpdateSubProfile Router
class UpdateSubProfileRouter {
    weak var viewController: UpdateSubProfileViewController?
    
    static func setupModule(authModel: AuthModel?, delegate: UpdateSubProfileDelegate?) -> UpdateSubProfileViewController {
        let viewController = UpdateSubProfileViewController()
        let router = UpdateSubProfileRouter()
        let interactorInput = UpdateSubProfileInteractorInput()
        let viewModel = UpdateSubProfileViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.delegate = delegate
        viewModel.authModel = authModel
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - UpdateSubProfile RouterProtocol
extension UpdateSubProfileRouter: UpdateSubProfileRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
}
