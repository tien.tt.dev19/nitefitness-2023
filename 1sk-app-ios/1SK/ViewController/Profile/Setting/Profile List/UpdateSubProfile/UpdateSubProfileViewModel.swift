//
//  
//  UpdateSubProfileViewModel.swift
//  1SK
//
//  Created by Thaad on 20/09/2022.
//
//

import UIKit
protocol UpdateSubProfileDelegate: AnyObject {
    func onUpdateProfileSuccess()
    func onDeleteProfileSuccess()
}

// MARK: - ViewModelProtocol
protocol UpdateSubProfileViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onSaveAction()
    
    // Value
    func onAvatarChanged(with avatar: UIImage?)
    
    func onUserNameChanged(with name: String)
    func onBirthDayChanged(with birthday: String)
    func onPhoneChanged(with phone: String)
    func onGenderChanged(with gender: Gender)
    
    func onDidChangeWeight(value: String?)
    func onDidChangeHeight(value: String?)
    func onDidChangeBlood(value: String?)
    
    func onAgreeDeleteProfileAction()
    
    var authModel: AuthModel? { get set }
    var newUser: UserModel? { get set }
}

// MARK: - UpdateSubProfile ViewModel
class UpdateSubProfileViewModel {
    weak var view: UpdateSubProfileViewProtocol?
    private var interactor: UpdateSubProfileInteractorInputProtocol
    weak var delegate: UpdateSubProfileDelegate?
    
    init(interactor: UpdateSubProfileInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var authModel: AuthModel?

    var newUser: UserModel?
    var newAvatar: UIImage?
    
    func getInitData() {
        self.newUser = self.authModel?.customer
        self.view?.setDataUser(user: self.newUser)
        
        guard let accessToken = self.authModel?.accessToken else {
            return
        }
        self.view?.showHud()
        self.interactor.getUserInfo(accessToken: accessToken)
    }
}

// MARK: - UpdateSubProfile ViewModelProtocol
extension UpdateSubProfileViewModel: UpdateSubProfileViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.getInitData()
    }
    
    // Action
    func onSaveAction() {
        guard let _newUser = self.newUser else {
            return
        }
        guard let accessToken = self.authModel?.accessToken else {
            return
        }
        self.view?.showHud()
        self.interactor.setUpdateUserInfo(newUser: _newUser, newAvatar: self.newAvatar, accessToken: accessToken)
    }
    
    // Value
    func onAvatarChanged(with avatar: UIImage?) {
        self.newAvatar = avatar
    }

    func onUserNameChanged(with name: String) {
        self.newUser?.fullName = name
    }

    func onBirthDayChanged(with birthday: String) {
        self.newUser?.birthday = birthday
    }
    
    func onPhoneChanged(with phone: String) {
        self.newUser?.phoneNumber = phone
    }

    func onGenderChanged(with gender: Gender) {
        self.newUser?.gender = gender
    }

    // PHR Action
    func onDidChangeWeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        guard let weight = Double(value.replaceCharacter(target: ",", withString: ".")) else {
            return
        }
        self.newUser?.weight = weight
    }
    
    func onDidChangeHeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        guard let height = Double(value) else {
            return
        }
        self.newUser?.height = height
    }
    
    func onDidChangeBlood(value: String?) {
        guard let `value` = value else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.newUser?.blood = value
    }
    
    func onAgreeDeleteProfileAction() {
        guard let accessToken = self.authModel?.accessToken else {
            return
        }
        self.view?.showHud()
        self.interactor.setDeleteProfile(accessToken: accessToken)
    }
}

// MARK: - UpdateSubProfile InteractorOutputProtocol
extension UpdateSubProfileViewModel: UpdateSubProfileInteractorOutputProtocol {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let userModel):
            self.newUser = userModel
            self.view?.setDataUser(user: self.newUser)
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onUpdateUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let userModel):
            let listProfile: [ProfileRealm] = gRealm.objects().filter({$0.id == userModel.id})
            listProfile.forEach { profile in
                gRealm.update {
                    profile.avatar = userModel.avatar
                    profile.fullName = userModel.fullName
                    profile.birthday = userModel.birthday
                    profile.gender = userModel.gender
                    profile.height = userModel.height
                    profile.weight = userModel.weight
                    //profile.blood = userModel.blood
                }
                print("gRealm.update UpdateSubProfileViewModel profile \(profile.fullName ?? "NULL") - type: \(profile.type?.rawValue ?? -1)")
            }
            
            self.delegate?.onUpdateProfileSuccess()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.view?.hideHud()
                self.view?.onDismiss()
            }
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
            self.view?.hideHud()
        }
    }
    
    func onSetDeleteProfileFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            // Remove Profile
            let listProfile: [ProfileRealm] = gRealm.objects().filter({$0.id == self.authModel?.customer?.id})
            listProfile.forEach { profile in
                gRealm.remove(profile)
            }
            
            self.delegate?.onDeleteProfileSuccess()
            self.view?.onDismiss()
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
}
