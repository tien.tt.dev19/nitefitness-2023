//
//  
//  RemoveAccountInteractorInput.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol RemoveAccountInteractorInputProtocol {
    func setDeleteAccount()
}

// MARK: - Interactor Output Protocol
protocol RemoveAccountInteractorOutputProtocol: AnyObject {
    func onSetDeleteAccountFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - RemoveAccount InteractorInput
class RemoveAccountInteractorInput {
    weak var output: RemoveAccountInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - RemoveAccount InteractorInputProtocol
extension RemoveAccountInteractorInput: RemoveAccountInteractorInputProtocol {
    func setDeleteAccount() {
        self.authService?.setDeleteAccount(completion: { [weak self] result in
            self?.output?.onSetDeleteAccountFinished(with: result.unwrapSuccessModel())
        })
    }
}
