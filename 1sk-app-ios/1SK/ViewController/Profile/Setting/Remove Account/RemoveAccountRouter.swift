//
//  
//  RemoveAccountRouter.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol RemoveAccountRouterProtocol {
    func popViewController()
    func logout()
}

// MARK: - RemoveAccount Router
class RemoveAccountRouter {
    weak var viewController: RemoveAccountViewController?
    
    static func setupModule() -> RemoveAccountViewController {
        let viewController = RemoveAccountViewController()
        let router = RemoveAccountRouter()
        let interactorInput = RemoveAccountInteractorInput()
        let viewModel = RemoveAccountViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - RemoveAccount RouterProtocol
extension RemoveAccountRouter: RemoveAccountRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func logout() {
        if let tabBarController = self.viewController?.tabBarController as? TabBarController {
            tabBarController.onLogoutAction()
        }
    }
}
