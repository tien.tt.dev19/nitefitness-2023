//
//  
//  RemoveAccountViewController.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol RemoveAccountViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setShowAlertConfirmSuccessView()
}

// MARK: - RemoveAccount ViewController
class RemoveAccountViewController: BaseViewController {
    var router: RemoveAccountRouterProtocol!
    var viewModel: RemoveAccountViewModelProtocol!
    
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Xóa tài khoản"
    }
    
    // MARK: - Action
    @IBAction func onRemoveAccountAction(_ sender: Any) {
        self.setShowAlertPopupConfirmTitle()
    }
    
}

// MARK: - RemoveAccount ViewProtocol
extension RemoveAccountViewController: RemoveAccountViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setShowAlertConfirmSuccessView() {
        self.presentAlertConfirmSuccessVC(delegate: self)
    }
}

// MARK: - AlertPopupConfirmTitle_2_ViewDelegate
extension RemoveAccountViewController: AlertPopupConfirmTitle_2_ViewDelegate {
    func setShowAlertPopupConfirmTitle() {
        let controller = AlertPopupConfirmTitle_2_ViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        
        controller.lblTitle = "Xóa tài khoản"
        controller.lblContent = "Bạn có chắc chắn muốn xóa tài khoản?"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xóa"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirm_2_CancelAction(object: Any?) {
        // not use
    }
    
    func onAlertPopupConfirm_2_AgreeAction(object: Any?) {
        self.viewModel.onAgreeDeleteAccount()
    }
}

// MARK: - AlertConfirmSuccessViewDelegate
extension RemoveAccountViewController: AlertConfirmSuccessViewDelegate {
    func presentAlertConfirmSuccessVC(delegate: AlertConfirmSuccessViewDelegate?) {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        controller.titleAlert = "Xóa tài khoản thành công"
        self.present(controller, animated: false)
    }
    
    func onDidDismissAlertConfirmSuccessView() {
        self.router.logout()
    }
}
