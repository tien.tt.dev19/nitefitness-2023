//
//  
//  RemoveAccountViewModel.swift
//  1SK
//
//  Created by Thaad on 10/06/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol RemoveAccountViewModelProtocol {
    func onViewDidLoad()
    func onAgreeDeleteAccount()
}

// MARK: - RemoveAccount ViewModel
class RemoveAccountViewModel {
    weak var view: RemoveAccountViewProtocol?
    private var interactor: RemoveAccountInteractorInputProtocol

    init(interactor: RemoveAccountInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - RemoveAccount ViewModelProtocol
extension RemoveAccountViewModel: RemoveAccountViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onAgreeDeleteAccount() {
        self.view?.showHud()
        self.interactor.setDeleteAccount()
    }
}

// MARK: - RemoveAccount InteractorOutputProtocol
extension RemoveAccountViewModel: RemoveAccountInteractorOutputProtocol {
    func onSetDeleteAccountFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            SKUserDefaults.shared.deleteAccountObject()
            self.view?.setShowAlertConfirmSuccessView()
            
            AnalyticsHelper.setEvent(with: .USER_DELETE_ACCOUNT)
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
}
