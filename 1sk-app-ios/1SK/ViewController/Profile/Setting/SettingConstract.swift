//
//  
//  SettingConstract.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

// MARK: View -
protocol SettingViewProtocol: AnyObject {

}

// MARK: Interactor -
protocol SettingInteractorInputProtocol {
    func logout()
}

protocol SettingInteractorOutputProtocol: AnyObject {
    func onLogoutFinished(with result: Result<EmptyModel, APIError>)
}
// MARK: Presenter -
protocol SettingPresenterProtocol {
    func onViewDidLoad()
    
    // MARK: - Action
    func onHealthProfileAction()
    func onDeviceConnectAction()
    func onTermsConditionaction()
    func onPrivacyPolicyAction()
    func onUserInfoAction()
    func onChangePasswordAction()
    func onLogoutAction()
    func onDeleteAccountAction()
    func gotoAppTrackingViewController()
}

// MARK: Router -
protocol SettingRouterProtocol: BaseRouterProtocol {
    func showProfileListRouter()
    func gotoAccountViewController(delegate: UpdateProfileViewDelegate?)
    func gotoTermsOfServiceViewController()
    func gotoPrivacyAndPolicyViewController()
    func gotoAppTrackingViewController()
    func showRemoveAccountRouter()
    func showChangePasswordRouter()
    func gotoDeviceViewController()
    
    func presentAlertLogoutVC(delegate: AlertLogoutViewDelegate?)
    func logout()
}
