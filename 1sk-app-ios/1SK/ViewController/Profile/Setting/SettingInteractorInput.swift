//
//  
//  SettingInteractorInput.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class SettingInteractorInput {
    weak var output: SettingInteractorOutputProtocol?
}

// MARK: - SettingInteractorInputProtocol
extension SettingInteractorInput: SettingInteractorInputProtocol {
    func logout() {
        let sucess: Result<EmptyModel, APIError> = .success(EmptyModel())
        self.output?.onLogoutFinished(with: sucess)
    }
}
