//
//  
//  SettingPresenter.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class SettingPresenter {

    weak var view: SettingViewProtocol?
    private var interactor: SettingInteractorInputProtocol
    private var router: SettingRouterProtocol

    init(interactor: SettingInteractorInputProtocol, router: SettingRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - SettingPresenterProtocol
extension SettingPresenter: SettingPresenterProtocol {
    func onViewDidLoad() {

    }
    
    func onHealthProfileAction() {
        self.router.showProfileListRouter()
    }
    
    func onDeviceConnectAction() {
        self.router.gotoDeviceViewController()
    }
    
    func onTermsConditionaction() {
        self.router.gotoTermsOfServiceViewController()
    }
    
    func onPrivacyPolicyAction() {
        self.router.gotoPrivacyAndPolicyViewController()
    }
    
    func onUserInfoAction() {
        self.router.gotoAccountViewController(delegate: nil)
    }
    
    func onChangePasswordAction() {
        self.router.showChangePasswordRouter()
    }
    
    func gotoAppTrackingViewController() {
        self.router.gotoAppTrackingViewController()
    }
    
    func onLogoutAction() {
        self.router.presentAlertLogoutVC(delegate: self)
        
    }
    
    func onDeleteAccountAction() {
        self.router.showRemoveAccountRouter()
    }
}

// MARK: - SettingPresenter: SettingInteractorOutput -
extension SettingPresenter: SettingInteractorOutputProtocol {
    func onLogoutFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            self.router.logout()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
}

// MARK: AlertLogoutViewDelegate
extension SettingPresenter: AlertLogoutViewDelegate {
    func onLogoutAgreeAction() {
        self.router.showHud()
        self.interactor.logout()
        
        AnalyticsHelper.setEvent(with: .USER_SIGN_OUT)
    }
}
