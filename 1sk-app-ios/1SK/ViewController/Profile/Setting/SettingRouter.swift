//
//  
//  SettingRouter.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class SettingRouter: BaseRouter {
    static func setupModule() -> SettingViewController {
        let viewController = SettingViewController()
        let router = SettingRouter()
        let interactorInput = SettingInteractorInput()
        let presenter = SettingPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        viewController.hidesBottomBarWhenPushed = true
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - SettingRouterProtocol
extension SettingRouter: SettingRouterProtocol {
    func showProfileListRouter() {
        let controller = ProfileListRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func gotoAccountViewController(delegate: UpdateProfileViewDelegate?) {
        let controller = UpdateProfileRouter.setupModule(with: delegate)
        self.viewController?.present(controller, animated: true)
    }

    func gotoTermsOfServiceViewController() {
        let termsOfServiceVC = TermsOfServiceRouter.setupModule()
        viewController?.navigationController?.pushViewController(termsOfServiceVC, animated: true)
    }

    func gotoPrivacyAndPolicyViewController() {
        let privacyAndPolicyVC = PrivacyPolicyRouter.setupModule()
        viewController?.navigationController?.pushViewController(privacyAndPolicyVC, animated: true)
    }
    
    func gotoAppTrackingViewController() {
        let controller = AppTrackingConnectRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }

    func showRemoveAccountRouter() {
        let controller = RemoveAccountRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: true)
    }
    
    func showChangePasswordRouter() {
        let controller = ChangePasswordRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: true)
    }
    
    func gotoDeviceViewController() {
        let controller = DeviceConnectRouter.setupModule()
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func presentAlertLogoutVC(delegate: AlertLogoutViewDelegate?) {
        let controller = AlertLogoutViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = delegate
        self.viewController?.present(controller, animated: false)
    }
    
    func logout() {
        if let tabBarController = self.viewController?.tabBarController as? TabBarController {
            tabBarController.onLogoutAction()
        }
    }
}
