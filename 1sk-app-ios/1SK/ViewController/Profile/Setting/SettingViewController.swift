//
//  
//  SettingViewController.swift
//  1SK
//
//  Created by tuyenvx on 15/06/2021.
//
//

import UIKit

class SettingViewController: BaseViewController {
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var view_ChangePassword: UIView!
    
    @IBOutlet weak var view_SyncData: UIView!
    
    var presenter: SettingPresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDefaults()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func setupDefaults() {
        self.setInitLayout()
        self.setDataVersion()
    }
    
    private func setInitLayout() {
        self.navigationItem.title = "Cài đặt"
        self.view_SyncData.isHidden = gVersion.isReviewAppstore ?? false
        
        if gUser?.mobileVerifiedAt == nil {
            self.view_ChangePassword.isHidden = true
        } else {
            self.view_ChangePassword.isHidden = false
        }
    }
    
    func setDataVersion() {
        switch APP_ENV {
        case .DEV:
            self.versionLabel.text = String(format: "Version: %@  (%@)", APP_VERSION, APP_BUILD)
            
        case .PRO:
            self.versionLabel.text = String(format: "Version: %@", APP_VERSION)
        }
    }
    
    // MARK: - Action
    @IBAction func onUserInfoAction(_ sender: Any) {
        self.presenter.onUserInfoAction()
    }
    
    @IBAction func onHealthProfileAction(_ sender: Any) {
        self.presenter.onHealthProfileAction()
    }
    
    @IBAction func onDeviceConnectAction(_ sender: Any) {
        self.presenter.onDeviceConnectAction()
    }
    
    @IBAction func onStravaConnectAction(_ sender: Any) {
        self.presenter.gotoAppTrackingViewController()
    }
    
    @IBAction func onChangePasswordAction(_ sender: Any) {
        self.presenter.onChangePasswordAction()
    }
    
    @IBAction func onTermsConditionaction(_ sender: Any) {
        self.presenter.onTermsConditionaction()
    }
    
    @IBAction func onPrivacyPolicyAction(_ sender: Any) {
        self.presenter.onPrivacyPolicyAction()
    }
    
    @IBAction func onLogoutAction(_ sender: Any) {
        self.presenter.onLogoutAction()
    }

    @IBAction func onDeleteAccountAction(_ sender: Any) {
        self.presenter.onDeleteAccountAction()
    }
}

// MARK: - SettingViewProtocol
extension SettingViewController: SettingViewProtocol {
    
}
