//
//  
//  AppTrackingConnectInteractorInput.swift
//  1SK
//
//  Created by Tiến Trần on 25/07/2022.
//
//

import UIKit
import Alamofire

// MARK: - Interactor Input Protocol
protocol AppTrackingConnectInteractorInputProtocol {
    func onConnectStrava(code: String)
    func onDisconnectStrava()
    func onIdentifyProviders()
    func onGetMeIdentifyProviders()
    func onRequestStravaToken(params: [String: Any])
}

// MARK: - Interactor Output Protocol
protocol AppTrackingConnectInteractorOutputProtocol: AnyObject {
    func didConnectStravaFinised(with result: Result<EmptyModel, APIError>)
    func didDisconnectStravaFinised(with result: Result<EmptyModel, APIError>)
    func didIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>)
    func didGetMeIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>)
}

// MARK: - AppTrackingConnect InteractorInput
class AppTrackingConnectInteractorInput {
    weak var output: AppTrackingConnectInteractorOutputProtocol?
    var fitRaceSevice: FitRaceSeviceProtocol?
}

// MARK: - AppTrackingConnect InteractorInputProtocol
extension AppTrackingConnectInteractorInput: AppTrackingConnectInteractorInputProtocol {
    func onConnectStrava(code: String) {
        self.fitRaceSevice?.onConnectStravaCode(code: code, completion: { [weak self] result in
            self?.output?.didConnectStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onDisconnectStrava() {
        self.fitRaceSevice?.onDisConnectStrava(completion: { [weak self] result in
            self?.output?.didDisconnectStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onIdentifyProviders() {
        self.fitRaceSevice?.onIdentifyProviders(completion: { [weak self] result in
            self?.output?.didIdentifyProvidersStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onGetMeIdentifyProviders() {
        self.fitRaceSevice?.onGetMeIdentifyProvider(completion: { [weak self] result in
            self?.output?.didGetMeIdentifyProvidersStravaFinised(with: result.unwrapSuccessModel())
        })
    }
    
    func onRequestStravaToken(params: [String: Any]) {
        AF.request("https://www.strava.com/oauth/token", method: .post, parameters: params).response { response in
            guard let data = response.data else { return }
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                if let refresh_token = json["refresh_token"] as? String {
                    self.fitRaceSevice?.onConnectStravaCode(code: refresh_token, completion: { [weak self] result in
                        self?.output?.didConnectStravaFinised(with: result.unwrapSuccessModel())
                    })
                }
            }
        }
    }
}
