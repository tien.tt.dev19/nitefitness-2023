//
//  
//  AppTrackingConnectRouter.swift
//  1SK
//
//  Created by Tiến Trần on 25/07/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol AppTrackingConnectRouterProtocol {

}

// MARK: - AppTrackingConnect Router
class AppTrackingConnectRouter {
    weak var viewController: AppTrackingConnectViewController?
    
    static func setupModule() -> AppTrackingConnectViewController {
        let viewController = AppTrackingConnectViewController()
        let router = AppTrackingConnectRouter()
        let interactorInput = AppTrackingConnectInteractorInput()
        let viewModel = AppTrackingConnectViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.fitRaceSevice = FitRaceService()
        router.viewController = viewController
        return viewController
    }
}

// MARK: - AppTrackingConnect RouterProtocol
extension AppTrackingConnectRouter: AppTrackingConnectRouterProtocol {
    
}
