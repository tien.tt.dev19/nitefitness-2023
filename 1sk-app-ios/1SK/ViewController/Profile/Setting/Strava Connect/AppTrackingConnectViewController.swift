//
//  
//  AppTrackingConnectViewController.swift
//  1SK
//
//  Created by Tiến Trần on 25/07/2022.
//
//

import UIKit
import Alamofire
import AuthenticationServices

// MARK: - ViewProtocol
protocol AppTrackingConnectViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func didConnectStrava(with model: StravaModel?)
    func didDisconnectStrava()
    
    //UITableView
    //func onReloadData()
}

// MARK: - AppTrackingConnect ViewController
class AppTrackingConnectViewController: BaseViewController {
    var router: AppTrackingConnectRouterProtocol!
    var viewModel: AppTrackingConnectViewModelProtocol!
    
    @IBOutlet weak var stack_MainStackView: UIStackView!
    @IBOutlet weak var lbl_ConnectPaddingLabel: PaddingLabel!
    @IBOutlet weak var lbl_UserId: UILabel!
    @IBOutlet weak var lbl_UserName: UILabel!
    @IBOutlet weak var img_UserAvatar: UIImageView!
    @IBOutlet weak var btn_ConnectStrava: UIButton!
    @IBOutlet weak var lbl_Guide: UILabel!
    @IBOutlet weak var view_ProfileUser: UIView!
    @IBOutlet weak var img_Strava: UIImageView!
    @IBOutlet weak var lbl_ConnectTitle: UILabel!
    @IBOutlet weak var view_Link: UIView!
    @IBOutlet weak var view_Strava: UIView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Kết nối ứng dụng"

        guard let text = self.lbl_Guide.text else { return }
        let textRange = NSRange(location: 0, length: text.count)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(.underlineStyle,
                                    value: NSUnderlineStyle.single.rawValue,
                                    range: textRange)
        // Add other attributes if needed
        self.lbl_Guide.attributedText = attributedText

        self.lbl_Guide.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowLink(_:))))
        self.view_Link.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onShowLinkView(_:))))
        
        NotificationCenter.default.addObserver(self, selector: #selector(onConnectStrava), name: .applicationStravaUrlCallBack, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .applicationStravaUrlCallBack, object: nil)
    }
    
    // MARK: - Action
    @IBAction private func onConnectToStravaAction(_ sender: UIButton) {
        if self.btn_ConnectStrava.titleLabel?.text == "Kết nối" {
            self.viewModel.onAuthenStrava(vc: self)
        } else {
            self.viewModel.onDisconnectStrava()
        }
    }
    
    func didConnectStrava(with model: StravaModel?) {
        self.view_Strava.isHidden = false
        
        self.lbl_Guide.isHidden = true
        self.img_Strava.isHidden = true
        self.lbl_ConnectTitle.isHidden = true
        self.view_ProfileUser.isHidden = false
        self.view_Link.isHidden = false

        self.btn_ConnectStrava.backgroundColor = .white
        self.btn_ConnectStrava.setTitleColor(R.color.mainColor(), for: .normal)
        self.btn_ConnectStrava.setTitle("Ngắt kết nối", for: .normal)
        
        if let stravaInfo = model?.info {
            self.lbl_UserName.text = "\(stravaInfo.firstname ?? "") \(stravaInfo.lastname ?? "")"
            self.img_UserAvatar.setImageWith(imageUrl: stravaInfo.profile ?? "")
            self.lbl_UserId.text = "\(stravaInfo.id ?? 0)"
        } else {
            self.lbl_UserName.text = gUser?.fullName
            self.img_UserAvatar.setImageWith(imageUrl: gUser?.avatar ?? "")
            self.lbl_UserId.text = "\(gUser?.id ?? 0)"
        }
    }
    
    @objc func onConnectStrava() {
        self.showHud()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.viewModel.onConnectStrava(code: self.getCode(from: gStravaURL)!)
        }
    }
    
    private func getCode(from url: URL?) -> String? {
        guard let url = url?.absoluteString else { return nil }
        
        let urlComponents: URLComponents? = URLComponents(string: url)
        let code: String? = urlComponents?.queryItems?.filter { $0.name == "code" }.first?.value
        
        return code
    }
    
    func didDisconnectStrava() {
        self.view_Strava.isHidden = false
        self.lbl_Guide.isHidden = false
        self.img_Strava.isHidden = false
        self.lbl_ConnectTitle.isHidden = false
        self.view_ProfileUser.isHidden = true
        self.view_Link.isHidden = true

        self.btn_ConnectStrava.backgroundColor = R.color.mainColor()
        self.btn_ConnectStrava.setTitleColor(.white, for: .normal)
        self.btn_ConnectStrava.setTitle("Kết nối", for: .normal)
    }
    
    @objc private func onShowLink(_ sender: UITapGestureRecognizer) {
        self.onOpenLinkToSafari(url: GUIDE_CONNECTING_STRAVA_URL)
    }
    
    @objc private func onShowLinkView(_ sender: UITapGestureRecognizer) {
        self.onOpenLinkToSafari(url: GUIDE_TRACKING_STRAVA_URL)
    }
}

// MARK: - AppTrackingConnect ViewProtocol
extension AppTrackingConnectViewController: AppTrackingConnectViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

//MARK: - ASWebAuthenticationPresentationContextProviding
extension AppTrackingConnectViewController: ASWebAuthenticationPresentationContextProviding {
    func presentationAnchor(for session: ASWebAuthenticationSession) -> ASPresentationAnchor {
        UIApplication.shared.windows[0]
    }
}
