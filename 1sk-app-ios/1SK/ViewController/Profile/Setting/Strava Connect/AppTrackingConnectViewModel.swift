//
//  
//  AppTrackingConnectViewModel.swift
//  1SK
//
//  Created by Tiến Trần on 25/07/2022.
//
//

import UIKit
import AuthenticationServices

enum VirtualRaceConnectedApp: Int {
    case strava = 1
}

// MARK: - ViewModelProtocol
protocol AppTrackingConnectViewModelProtocol {
    func onViewDidLoad()
    func onConnectStrava(code: String)
    func onAuthenStrava(vc: AppTrackingConnectViewController)
    func onIdentifyProviders()
    func onGetMeIdentifyProviders()
    func onDisconnectStrava()
}

// MARK: - AppTrackingConnect ViewModel
class AppTrackingConnectViewModel {
    weak var view: AppTrackingConnectViewProtocol?
    private var interactor: AppTrackingConnectInteractorInputProtocol

    private let clientId: String = "89948"
    private let urlScheme: String = "myapp"
    private let redirectUri: String = "myapp://1sk.vn"
    private let clientSerect: String = "2ea5b1ae0ede66bff12e991721d63f55bce5a35d"

    private var authSession: ASWebAuthenticationSession?
    var listProviders: [StravaModel] = [StravaModel]()
    
    init(interactor: AppTrackingConnectInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - AppTrackingConnect ViewModelProtocol
extension AppTrackingConnectViewModel: AppTrackingConnectViewModelProtocol {
    func onViewDidLoad() {
        self.onGetMeIdentifyProviders()
    }
    
    func onViewWillAppear() {
        self.onGetMeIdentifyProviders()
    }
    
    func onAuthenStrava(vc: AppTrackingConnectViewController) {
        let webAuthUrl = self.createWebAuthUrl()
        let appAuthUrl = self.createAppAuthUrl()
        
        guard let webOAuthUrl = URL(string: webAuthUrl) else { return }
        guard let appOAuthUrl = URL(string: appAuthUrl) else { return }
        
        print("tien log: url \(webOAuthUrl)")
        
        if UIApplication.shared.canOpenURL(appOAuthUrl) {
            
            UIApplication.shared.open(appOAuthUrl, options: [:]) { success in
                print("Success!!!")
            }
            
            
        } else {
            self.authSession = ASWebAuthenticationSession(url: webOAuthUrl, callbackURLScheme: self.urlScheme) { [weak self] url, error in
                if error != nil {
                } else {
                    if let code = self?.getCode(from: url) {
                        self?.view?.showHud()
                        self?.interactor.onConnectStrava(code: code)
                    }
                }
            }
            
            if #available(iOS 13.0, *) {
                self.authSession?.presentationContextProvider = vc
            } else {
                // Fallback on earlier versions
            }
            self.authSession?.start()
        }
    }
    
    func onConnectStrava(code: String) {
        self.view?.showHud()
        self.interactor.onConnectStrava(code: code)
    }
    
    private func getCode(from url: URL?) -> String? {
        guard let url = url?.absoluteString else { return nil }
        
        let urlComponents: URLComponents? = URLComponents(string: url)
        let code: String? = urlComponents?.queryItems?.filter { $0.name == "code" }.first?.value
        
        return code
    }
    
    fileprivate func createWebAuthUrl() -> String {
        var url = ""
        url.append("https://www.strava.com/oauth/mobile/authorize")
        url.append("?client_id=\(self.clientId)")
        url.append("&redirect_uri=\(self.redirectUri)")
        url.append("&response_type=code")
        url.append("&approval_prompt=force")
        url.append("&grant_type=authorization_code")
        url.append("&scope=read,read_all,profile:read_all,activity:read,activity:read_all")
        return url
    }
    
    fileprivate func createAppAuthUrl() -> String {
        var url = ""
        url.append("strava://oauth/mobile/authorize")
        url.append("?client_id=\(self.clientId)")
        url.append("&redirect_uri=\(self.redirectUri)")
        url.append("&response_type=code")
        url.append("&approval_prompt=force")
        url.append("&grant_type=authorization_code")
        url.append("&scope=read,read_all,profile:read_all,activity:read,activity:read_all")
        return url
    }
    
    private func requestStravaTokens(with code: String) {
        let parameters: [String: Any] = ["client_id": self.clientId,
                                         "client_secret": self.clientSerect,
                                         "code": code,
                                         "grant_type": "authorization_code"]
        
        self.interactor.onRequestStravaToken(params: parameters)
    }
    
    func onIdentifyProviders() {
        self.interactor.onIdentifyProviders()
    }
    
    func onDisconnectStrava() {
        self.view?.showHud()
        self.interactor.onDisconnectStrava()
    }
    
    
    func onGetMeIdentifyProviders() {
        self.view?.showHud()
        self.interactor.onGetMeIdentifyProviders()
    }
}

// MARK: - AppTrackingConnect InteractorOutputProtocol
extension AppTrackingConnectViewModel: AppTrackingConnectInteractorOutputProtocol {
    func didConnectStravaFinised(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            self.onGetMeIdentifyProviders()
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
    
    func didDisconnectStravaFinised(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            self.view?.didDisconnectStrava()
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
    
    func didGetMeIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>) {
        switch result {
        case .success(let model):
            if model.isEmpty {
                self.view?.didDisconnectStrava()
            } else {
                model.forEach { stravaModel in
                    switch VirtualRaceConnectedApp(rawValue: stravaModel.id ?? 0) {
                    case .strava:
                        self.view?.didConnectStrava(with: stravaModel)
                    case .none:
                        self.view?.didDisconnectStrava()
                    case .some(_):
                        self.view?.didDisconnectStrava()
                    }
                }
            }
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
    
    func didIdentifyProvidersStravaFinised(with result: Result<[StravaModel], APIError>) {
        switch result {
        case .success(let model):
            model.forEach { stravaModel in
                switch VirtualRaceConnectedApp(rawValue: stravaModel.id ?? 0) {
                case .strava:
                    self.view?.didConnectStrava(with: stravaModel)
                case .none:
                    self.view?.didDisconnectStrava()
                case .some(_):
                    self.view?.didDisconnectStrava()
                }
            }
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
}
