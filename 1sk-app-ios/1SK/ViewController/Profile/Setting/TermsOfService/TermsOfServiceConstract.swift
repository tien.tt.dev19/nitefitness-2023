//
//  
//  TermsOfServiceConstract.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

// MARK: View -
protocol TermsOfServiceViewProtocol: class {

}

// MARK: Interactor -
protocol TermsOfServiceInteractorInputProtocol {

}

protocol TermsOfServiceInteractorOutputProtocol: class {
}
// MARK: Presenter -
protocol TermsOfServicePresenterProtocol {
    func onViewDidLoad()
}

// MARK: Router -
protocol TermsOfServiceRouterProtocol {

}
