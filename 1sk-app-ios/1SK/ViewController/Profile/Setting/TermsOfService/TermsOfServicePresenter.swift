//
//  
//  TermsOfServicePresenter.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class TermsOfServicePresenter {

    weak var view: TermsOfServiceViewProtocol?
    private var interactor: TermsOfServiceInteractorInputProtocol
    private var router: TermsOfServiceRouterProtocol

    init(interactor: TermsOfServiceInteractorInputProtocol,
         router: TermsOfServiceRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - extending TermsOfServicePresenter: TermsOfServicePresenterProtocol -
extension TermsOfServicePresenter: TermsOfServicePresenterProtocol {
    func onViewDidLoad() {
    }
}

// MARK: - TermsOfServicePresenter: TermsOfServiceInteractorOutput -
extension TermsOfServicePresenter: TermsOfServiceInteractorOutputProtocol {

}
