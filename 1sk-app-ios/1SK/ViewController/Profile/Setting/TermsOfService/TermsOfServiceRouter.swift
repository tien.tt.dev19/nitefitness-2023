//
//  
//  TermsOfServiceRouter.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class TermsOfServiceRouter {
    weak var viewController: UIViewController?
    static func setupModule() -> TermsOfServiceViewController {
        let viewController = TermsOfServiceViewController()
        viewController.hidesBottomBarWhenPushed = true
        let router = TermsOfServiceRouter()
        let interactorInput = TermsOfServiceInteractorInput()
        let presenter = TermsOfServicePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - TermsOfServiceRouter: TermsOfServiceRouterProtocol -
extension TermsOfServiceRouter: TermsOfServiceRouterProtocol {

}
