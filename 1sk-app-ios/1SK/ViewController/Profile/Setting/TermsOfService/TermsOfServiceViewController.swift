//
//  
//  TermsOfServiceViewController.swift
//  1SK
//
//  Created by tuyenvx on 22/02/2021.
//
//

import UIKit

class TermsOfServiceViewController: BaseViewController {

    var presenter: TermsOfServicePresenterProtocol!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        presenter.onViewDidLoad()
    }

    private func setupUI() {
        setupNavigation()
    }

    private func setupNavigation() {
        navigationItem.title = "Điều khoản sử dụng"
    }

}

// MARK: TermsOfServiceViewController - TermsOfServiceViewProtocol -
extension TermsOfServiceViewController: TermsOfServiceViewProtocol {

}
