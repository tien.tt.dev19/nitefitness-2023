//
//  
//  UpdateProfileInteractorInput.swift
//  1SK
//
//  Created by Thaad on 20/09/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol UpdateProfileInteractorInputProtocol {
    func getUserInfo()
    func setUpdateUserInfo(newUser: UserModel, newAvatar: UIImage?)
    func setDeleteAccount()
}

// MARK: - Interactor Output Protocol
protocol UpdateProfileInteractorOutputProtocol: AnyObject {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>)
    func onUpdateUserInfoFinished(with result: Result<UserModel, APIError>)
    func onSetDeleteAccountFinished(with result: Result<EmptyModel, APIError>)
}

// MARK: - UpdateProfile InteractorInput
class UpdateProfileInteractorInput {
    weak var output: UpdateProfileInteractorOutputProtocol?
    var authService: AuthServiceProtocol?
}

// MARK: - UpdateProfile InteractorInputProtocol
extension UpdateProfileInteractorInput: UpdateProfileInteractorInputProtocol {
    func getUserInfo() {
        self.authService?.getUserInfo(completion: { [weak self] result in
            self?.output?.onGetUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setUpdateUserInfo(newUser: UserModel, newAvatar: UIImage?) {
        self.authService?.setUpdateProfile(newUser: newUser, image: newAvatar, completion: { [weak self] result in
            self?.output?.onUpdateUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setDeleteAccount() {
        self.authService?.setDeleteAccount(completion: { [weak self] result in
            self?.output?.onSetDeleteAccountFinished(with: result.unwrapSuccessModel())
        })
    }
}
