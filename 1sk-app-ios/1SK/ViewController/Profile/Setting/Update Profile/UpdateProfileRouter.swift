//
//  
//  UpdateProfileRouter.swift
//  1SK
//
//  Created by Thaad on 20/09/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol UpdateProfileRouterProtocol {
    func onDismiss()
    func logout()
}

// MARK: - UpdateProfile Router
class UpdateProfileRouter {
    weak var viewController: UpdateProfileViewController?
    
    static func setupModule(with delegate: UpdateProfileViewDelegate?) -> UpdateProfileViewController {
        let viewController = UpdateProfileViewController()
        let router = UpdateProfileRouter()
        let interactorInput = UpdateProfileInteractorInput()
        let viewModel = UpdateProfileViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .fullScreen
        viewModel.view = viewController
        viewModel.delegate = delegate
        interactorInput.output = viewModel
        interactorInput.authService = AuthService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - UpdateProfile RouterProtocol
extension UpdateProfileRouter: UpdateProfileRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
    
    func logout() {
        if let tabBarController = gTabBarController as? TabBarController {
            tabBarController.onLogoutAction()
        }
    }
}
