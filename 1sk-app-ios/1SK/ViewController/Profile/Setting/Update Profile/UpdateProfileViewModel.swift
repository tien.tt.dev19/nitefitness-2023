//
//  
//  UpdateProfileViewModel.swift
//  1SK
//
//  Created by Thaad on 20/09/2022.
//
//

import UIKit
protocol UpdateProfileViewDelegate: AnyObject {
    func onSaveUserInfoChangeSuccess()
}

// MARK: - ViewModelProtocol
protocol UpdateProfileViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onSaveAction()
    
    // Value
    func onAvatarChanged(with avatar: UIImage?)
    
    func onUserNameChanged(with name: String)
    func onBirthDayChanged(with birthday: String)
    func onPhoneChanged(with phone: String)
    func onGenderChanged(with gender: Gender)
    
    func onDidChangeWeight(value: String?)
    func onDidChangeHeight(value: String?)
    func onDidChangeBlood(value: String?)
    
    func onAgreeDeleteProfileAction()
    
    var newUser: UserModel { get set }
}

// MARK: - UpdateProfile ViewModel
class UpdateProfileViewModel {
    weak var view: UpdateProfileViewProtocol?
    private var interactor: UpdateProfileInteractorInputProtocol
    weak var delegate: UpdateProfileViewDelegate?
    
    init(interactor: UpdateProfileInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var userParent: UserModel?

    var newUser = UserModel()
    var newAvatar: UIImage?
    
    func getInitData() {
        self.view?.showHud()
        self.interactor.getUserInfo()
    }
}

// MARK: - UpdateProfile ViewModelProtocol
extension UpdateProfileViewModel: UpdateProfileViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.getInitData()
    }
    
    // Action
    func onSaveAction() {
        self.view?.showHud()
        self.interactor.setUpdateUserInfo(newUser: self.newUser, newAvatar: self.newAvatar)
    }
    
    // Value
    func onAvatarChanged(with avatar: UIImage?) {
        self.newAvatar = avatar
    }

    func onUserNameChanged(with name: String) {
        self.newUser.fullName = name
    }

    func onBirthDayChanged(with birthday: String) {
        self.newUser.birthday = birthday
    }
    
    func onPhoneChanged(with phone: String) {
        self.newUser.phoneNumber = phone
    }

    func onGenderChanged(with gender: Gender) {
        self.newUser.gender = gender
    }

    // PHR Action
    func onDidChangeWeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        guard let weight = Double(value.replaceCharacter(target: ",", withString: ".")) else {
            return
        }
        self.newUser.weight = weight
    }
    
    func onDidChangeHeight(value: String?) {
        guard let `value` = value else {
            return
        }
        guard value.first != "-" else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        guard let height = Double(value) else {
            return
        }
        self.newUser.height = height
    }
    
    func onDidChangeBlood(value: String?) {
        guard let `value` = value else {
            SKToast.shared.showToast(content: "Dữ liệu nhập không đúng")
            return
        }
        self.newUser.blood = value
    }
    
    func onAgreeDeleteProfileAction() {
        self.view?.showHud()
        self.interactor.setDeleteAccount()
    }
}

// MARK: - UpdateProfile InteractorOutputProtocol
extension UpdateProfileViewModel: UpdateProfileInteractorOutputProtocol {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let userModel):
            Configure.shared.setDataUser(userModel: userModel)
            
            self.newUser.fullName = gUser?.fullName
            self.newUser.birthday = gUser?.birthday
            self.newUser.gender = gUser?.gender
            self.newUser.phoneNumber = gUser?.phoneNumber
            self.newUser.height = gUser?.height
            self.newUser.weight = gUser?.weight
            
            self.view?.setDataUser(user: gUser)
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
    
    func onUpdateUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let userModel):
            Configure.shared.setDataUser(userModel: userModel)
            
            self.view?.onUpdateInfoSuccess(true)
            self.delegate?.onSaveUserInfoChangeSuccess()
            SKToast.shared.showToast(content: "Cập nhật hồ sơ thành công")
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
            self.view?.onUpdateInfoSuccess(false)
        }
        self.view?.hideHud()
    }
    
    func onSetDeleteAccountFinished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success:
            SKUserDefaults.shared.deleteAccountObject()
            self.view?.setShowAlertConfirmSuccessView()
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        self.view?.hideHud()
    }
}
