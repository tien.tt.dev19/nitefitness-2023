//
//  
//  SplashInteractorInput.swift
//  1SK
//
//  Created by Thaad on 11/11/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol SplashInteractorInputProtocol {
    func getUserInfo()
}

// MARK: - Interactor Output Protocol
protocol SplashInteractorOutputProtocol: AnyObject {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>)
}

// MARK: - Splash InteractorInput
class SplashInteractorInput {
    weak var output: SplashInteractorOutputProtocol?
    
    let authService = AuthService()
}

// MARK: - Splash InteractorInputProtocol
extension SplashInteractorInput: SplashInteractorInputProtocol {
    func getUserInfo() {
        self.authService.getUserInfo(completion: { [weak self] result in
            self?.output?.onGetUserInfoFinished(with: result.unwrapSuccessModel())
        })
    }
}
