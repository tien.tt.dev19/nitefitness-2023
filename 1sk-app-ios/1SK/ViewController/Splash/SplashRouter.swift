//
//  
//  SplashRouter.swift
//  1SK
//
//  Created by Thaad on 11/11/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol SplashRouterProtocol {
    func presentTabBarViewController()
}

// MARK: - Splash Router
class SplashRouter {
    weak var viewController: SplashViewController?
    
    static func setupModule() -> SplashViewController {
        let viewController = SplashViewController()
        let router = SplashRouter()
        let interactorInput = SplashInteractorInput()
        let viewModel = SplashViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - Splash RouterProtocol
extension SplashRouter: SplashRouterProtocol {
    func presentTabBarViewController() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.7) {
            let controller = TabBarController()
            controller.modalPresentationStyle = .fullScreen
            self.viewController?.present(controller, animated: false, completion: nil)
        }
    }
}
