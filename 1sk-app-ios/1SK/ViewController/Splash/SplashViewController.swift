//
//  
//  SplashViewController.swift
//  1SK
//
//  Created by Thaad on 11/11/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol SplashViewProtocol: AnyObject {
    func showHud()
    func hideHud()
}

// MARK: - Splash ViewController
class SplashViewController: BaseViewController {
    var router: SplashRouterProtocol!
    var viewModel: SplashViewModelProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        if gIsInternetConnectionAvailable == true {
            self.router.presentTabBarViewController()
            self.viewModel.getUserInfo()
            
        } else {
            self.setInitHandleNetwork()
        }
        
//        if SKUserDefaults.shared.appOpenFirst != true {
//            AnalyticsHelper.setEvent(with: .APP_OPEN_FIRST)
//        }
    }
}

// MARK: - Internet Connection
extension SplashViewController {
    func setInitHandleNetwork() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onConnectionAvailable), name: .connectionAvailable, object: nil)
    }
    
    @objc func onConnectionAvailable() {
        NotificationCenter.default.removeObserver(self, name: .connectionAvailable, object: nil)
        self.router.presentTabBarViewController()
        self.viewModel.getUserInfo()
    }
}

// MARK: - Splash ViewProtocol
extension SplashViewController: SplashViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}
