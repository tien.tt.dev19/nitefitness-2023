//
//  
//  SplashViewModel.swift
//  1SK
//
//  Created by Thaad on 11/11/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol SplashViewModelProtocol {
    func onViewDidLoad()
    func getUserInfo()
}

// MARK: - Splash ViewModel
class SplashViewModel {
    weak var view: SplashViewProtocol?
    private var interactor: SplashInteractorInputProtocol

    init(interactor: SplashInteractorInputProtocol) {
        self.interactor = interactor
    }
    
}

// MARK: - Splash ViewModelProtocol
extension SplashViewModel: SplashViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
        self.setInitEnvironment()
    }
    
    func getUserInfo() {
        if SKUserDefaults.shared.loginAuth == true && KeyChainManager.shared.accessToken != nil {
            self.interactor.getUserInfo()
        } else {
            KeyChainManager.shared.accessToken = nil
        }
    }
}

// MARK: - Splash Handler
extension SplashViewModel {
    func setInitEnvironment() {
        if APP_ENV == .DEV {
            gTimeDurationEndCall = 900
        } else {
            gTimeDurationEndCall = 1800
        }
    }
}

// MARK: - Splash InteractorOutputProtocol
extension SplashViewModel: SplashInteractorOutputProtocol {
    func onGetUserInfoFinished(with result: Result<UserModel, APIError>) {
        switch result {
        case .success(let userModel):
            Configure.shared.setDataUser(userModel: userModel)
            
        case .failure:
            break
        }
    }
}
