//
//  
//  CartStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol CartStoreInteractorInputProtocol {
    func getListProductSuggest(page: Int, perPage: Int)
    func getProductDetail(id: Int)
    func getDeliveryAddress()
}

// MARK: - Interactor Output Protocol
protocol CartStoreInteractorOutputProtocol: AnyObject {
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
    func onGetDeliveryAddressFinished(with result: Result<DeliveryModel, APIError>)
}

// MARK: - CartStore InteractorInput
class CartStoreInteractorInput {
    weak var output: CartStoreInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - CartStore InteractorInputProtocol
extension CartStoreInteractorInput: CartStoreInteractorInputProtocol {
    func getListProductSuggest(page: Int, perPage: Int) {
        self.storeService?.getListProductSuggest(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductSuggestFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getDeliveryAddress() {
        self.storeService?.getDeliveryAddress(completion: { [weak self] result in
            self?.output?.onGetDeliveryAddressFinished(with: result.unwrapSuccessModel())
        })
    }
}
