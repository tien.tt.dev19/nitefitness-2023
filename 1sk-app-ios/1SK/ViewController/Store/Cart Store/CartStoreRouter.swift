//
//  
//  CartStoreRouter.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol CartStoreRouterProtocol {
    func showProductStore(product: ProductModel?)
    func showPaymentStore(listProduct: [CartProductSchema], delivery: DeliveryModel?)
    func showDeliveryInformationStore(delegate: DeliveryInformationViewDelegate?)
    func onPopViewController()
}

// MARK: - CartStore Router
class CartStoreRouter {
    weak var viewController: CartStoreViewController?
    static func setupModule() -> CartStoreViewController {
        let viewController = CartStoreViewController()
        let router = CartStoreRouter()
        let interactorInput = CartStoreInteractorInput()
        let viewModel = CartStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.hidesBottomBarWhenPushed = true
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - CartStore RouterProtocol
extension CartStoreRouter: CartStoreRouterProtocol {
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showPaymentStore(listProduct: [CartProductSchema], delivery: DeliveryModel?) {
        let controller = PaymentStoreRouter.setupModule(listProduct: listProduct, delivery: delivery)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showDeliveryInformationStore(delegate: DeliveryInformationViewDelegate?) {
        let controller = DeliveryInformationRouter.setupModule(state: .Create, delivery: nil, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func onPopViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
