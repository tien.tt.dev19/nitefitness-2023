//
//  
//  CartStoreViewController.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol CartStoreViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func reloadData_Cart()
    func reloadData_Suggest()
    
    func onDidSelectItem(product: ProductModel)
    func setUITotalPrice()
    
    func onGetDeliveryAddress(delivery: DeliveryModel?)
}

// MARK: - CartStore ViewController
class CartStoreViewController: BaseViewController {
    var router: CartStoreRouterProtocol!
    var viewModel: CartStoreViewModelProtocol!
    
    @IBOutlet weak var view_Data_Cart: UIView!
    
    @IBOutlet weak var tbv_Cart: UITableView!
    @IBOutlet weak var coll_Suggest: UICollectionView!
    @IBOutlet weak var constraint_height_Suggest: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_TotalPrice: UILabel!
    @IBOutlet weak var btn_Order: UIButton!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        self.view.endEditing(true)
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitUICollectionView()
        self.setUITotalPrice()
        
        self.tbv_Cart.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.view.endEditing(true)
    }
    
    // MARK: Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.onPopViewController()
    }
    
    @IBAction func onPaymentAction(_ sender: Any) {
        guard Configure.shared.isLogged() == true else {
            self.presentLoginRouter()
            return
        }
        self.viewModel.onPaymentAction()
    }
    
    // MARK: Helper
    func onUpdateProductAmount(product: CartProductSchema?, isReloadCell: Bool) {
        guard let index = gListCartProduct.firstIndex(where: {$0.id == product?.id}) else {
            return
        }

        if product?.amount == 0 {
            self.presentAlertPopupConfirmTitleView(product: product)
            
        } else {
            gListCartProduct[index].amount = product?.amount
            Shared.instance.cartProductSchema = gListCartProduct
            
            if product?.amount == 1 && isReloadCell == true {
                self.onReloadCellTable(index: index)
            }
        }

        self.setUITotalPrice()
    }
    
    func onRemoveProductInCart(product: CartProductSchema?) {
        guard let index = gListCartProduct.firstIndex(where: {$0.id == product?.id}) else {
            return
        }
        
        gListCartProduct.remove(at: index)
        Shared.instance.cartProductSchema = gListCartProduct

        self.onDeleteRowsTable(index: index)
    }
    
    func onDeleteRowsTable(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        self.tbv_Cart.deleteRows(at: [indexPath], with: .fade)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if gListCartProduct.count == 0 {
                UIView.animate(withDuration: 0.5) {
                    self.view_Data_Cart.alpha = 0
                }
            } else {
                self.tbv_Cart.reloadData()
            }
        }
    }
    
    func onReloadCellTable(index: Int) {
        let indexPath = IndexPath(row: index, section: 0)
        self.tbv_Cart.reloadRows(at: [indexPath], with: .automatic)
    }
}

// MARK: - CartStore ViewProtocol
extension CartStoreViewController: CartStoreViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func reloadData_Cart() {
        self.tbv_Cart.reloadData()
        self.setUITotalPrice()
        
        if gListCartProduct.count == 0 {
            UIView.animate(withDuration: 0.5) {
                self.view_Data_Cart.alpha = 0
            }
            
        } else {
            UIView.animate(withDuration: 0.5) {
                self.view_Data_Cart.alpha = 1
            }
        }
    }
    
    func reloadData_Suggest() {
        self.coll_Suggest.reloadData()
        
        let count = self.viewModel.numberOfItems_Suggest()
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_Suggest.constant = CGFloat(((count/2) * 220) + 16)
            
        } else {
            if count <= 2 {
                self.constraint_height_Suggest.constant = CGFloat(220 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_Suggest.constant = CGFloat(((row/2) * 220) + 16)
            }
        }
    }
    
    func setUITotalPrice() {
        var totalPrice = 0
        for item in gListCartProduct {
            if let amount = item.amount, let price = item.price, item.isSelected == true {
                totalPrice += amount * price
            }
        }
        self.lbl_TotalPrice.text = "\(totalPrice.formatNumber())đ"
        
        if totalPrice == 0 {
            self.btn_Order.backgroundColor = R.color.line_gray_01()
            self.btn_Order.setTitleColor(.black, for: .normal)
            
        } else {
            self.btn_Order.backgroundColor = R.color.mainColor()
            self.btn_Order.setTitleColor(.white, for: .normal)
        }
    }
    
    func onGetDeliveryAddress(delivery: DeliveryModel?) {
        if delivery == nil {
            self.onCreateDeliverAction()
            
        } else {
            var listProduct: [CartProductSchema] = []
            for item in gListCartProduct {
                if item.isSelected == true {
                    listProduct.append(item)
                }
            }
            guard listProduct.count > 0 else {
                return
            }
            self.router.showPaymentStore(listProduct: listProduct, delivery: delivery)
        }
    }
}

// MARK: DeliveryInformationViewDelegate
extension CartStoreViewController: DeliveryInformationViewDelegate {
    func onCreateDeliverAction() {
        self.router.showDeliveryInformationStore(delegate: self)
    }
    
    func onCreateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.viewModel.onPaymentAction()
        }
    }
    
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        // not use
    }
}

// MARK: UITableViewDataSource
extension CartStoreViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_Cart.registerNib(ofType: CellTableViewCart.self)
        self.tbv_Cart.dataSource = self
        self.tbv_Cart.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows_Table()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewCart.self, for: indexPath)
        cell.delegate = self
        cell.product = self.viewModel.cellForRow_Table(at: indexPath)
        
        let count = self.viewModel.numberOfRows_Table() - 1
        if indexPath.row == count {
            cell.view_LineBottom.isHidden = true
        } else {
            cell.view_LineBottom.isHidden = false
        }
        
        return cell
    }
    
    func onDidSelectItem(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
}

// MARK: UITableViewDelegate
extension CartStoreViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.didSelectRow_Table(at: indexPath)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
            return UISwipeActionsConfiguration(actions: [
                self.onCellDeleteAction(forRowAt: indexPath)
            ])
        }
    
    private func onCellDeleteAction(forRowAt indexPath: IndexPath) -> UIContextualAction {
        return UIContextualAction(style: .destructive, title: "Xóa") { (action, swipeButtonView, completion) in
            self.presentAlertPopupConfirmTitleView(product: gListCartProduct[indexPath.row])
            completion(true)
        }
    }
}

// MARK: CellTableViewCartDelegate
extension CartStoreViewController: CellTableViewCartDelegate {
    func onDidSelectedProduct(product: CartProductSchema?) {
        guard let index = gListCartProduct.firstIndex(where: {$0.id == product?.id}) else {
            return
        }
        gListCartProduct[index].isSelected = product?.isSelected
        Shared.instance.cartProductSchema = gListCartProduct
        self.setUITotalPrice()
    }
    
    func onDidChangeAmount(product: CartProductSchema?) {
        self.onUpdateProductAmount(product: product, isReloadCell: false)
    }
    
    func onDidTapCell(product: CartProductSchema?) {
        self.view.endEditing(true)
        
        guard let index = gListCartProduct.firstIndex(where: {$0.id == product?.id}) else {
            return
        }
        let indexPath = IndexPath(row: index, section: 0)
        self.viewModel.didSelectRow_Table(at: indexPath)
    }
}

// MARK: AlertPopupConfirmTitleViewDelegate
extension CartStoreViewController: AlertPopupConfirmTitleViewDelegate {
    func presentAlertPopupConfirmTitleView(product: CartProductSchema?) {
        let controller = AlertPopupConfirmTitleViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.object = product
        
        controller.lblTitle = "Xóa sản phẩm"
        controller.lblContent = "Bạn có xóa sản phẩm này trong giỏ hàng ?"
        controller.btnTitleCancel = "Hủy"
        controller.btnTitleAgree = "Xác nhận"
        
        self.present(controller, animated: false, completion: nil)
    }
    
    func onAlertPopupConfirmCancelAction(object: Any?) {
        guard let product = object as? CartProductSchema else {
            return
        }
        if product.amount == 0 {
            product.amount = 1
        }
        self.onUpdateProductAmount(product: product, isReloadCell: true)
    }
    
    func onAlertPopupConfirmAgreeAction(object: Any?) {
        guard let product = object as? CartProductSchema else {
            return
        }
        self.onRemoveProductInCart(product: product)
    }
}

// MARK: UICollectionViewDataSource
extension CartStoreViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_Suggest.registerNib(ofType: CellCollectionViewProduct.self)
        
        self.coll_Suggest.dataSource = self
        self.coll_Suggest.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.itemSize = CGSize(width: width, height: height)
        self.coll_Suggest.collectionViewLayout = layout
        
        self.constraint_height_Suggest.constant = 0 //(220 * 5) + 16
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems_Suggest()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
        cell.config(product: self.viewModel.cellForItem_Suggest(at: indexPath))
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension CartStoreViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.didSelectItem_Suggest(at: indexPath)
    }
}

// MARK: - LoginViewDelegate
extension CartStoreViewController: LoginViewDelegate {
    func presentLoginRouter() {
        let controller = LoginRouter.setupModule(delegate: self)
        controller.modalPresentationStyle = .custom
        self.present(controller, animated: true)
    }
    
    func onLoginSuccess() {
        SKToast.shared.showToast(content: "Đăng nhập thành công")
    }
}
