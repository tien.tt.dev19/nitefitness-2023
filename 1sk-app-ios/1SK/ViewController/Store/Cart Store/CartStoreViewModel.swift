//
//  
//  CartStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol CartStoreViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    // Action
    func onPaymentAction()
    
    // TableView
    func numberOfRows_Table() -> Int
    func cellForRow_Table(at indexPath: IndexPath) -> CartProductSchema?
    func didSelectRow_Table(at indexPath: IndexPath)
    
    // CollectionView
    func numberOfItems_Suggest() -> Int
    func cellForItem_Suggest(at indexPath: IndexPath) -> ProductModel
    func didSelectItem_Suggest(at indexPath: IndexPath)
}

// MARK: - CartStore ViewModel
class CartStoreViewModel {
    weak var view: CartStoreViewProtocol?
    private var interactor: CartStoreInteractorInputProtocol

    init(interactor: CartStoreInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listProductSuggest: [ProductModel] = []
    var deliveryAddress: DeliveryModel?

    func getDataCartProductLocal() {
        gListCartProduct = Shared.instance.cartProductSchema
    }
    
    func getDataProductSuggest() {
        self.view?.showHud()
        self.interactor.getListProductSuggest(page: 1, perPage: 6)
    }
    
    var isFirst = true
}

// MARK: - CartStore ViewModelProtocol
extension CartStoreViewModel: CartStoreViewModelProtocol {
    func onViewDidLoad() {
        self.getDataCartProductLocal()
        self.getDataProductSuggest()
    }
    
    func onViewDidAppear() {
        self.view?.reloadData_Cart()
    }
    
    // Action
    func onPaymentAction() {
        guard gListCartProduct.first(where: {$0.isSelected == true}) != nil else {
            SKToast.shared.showToast(content: "Vui lòng chọn mặt hàng để thanh toán")
            return
        }
        
        self.view?.showHud()
        self.interactor.getDeliveryAddress()
    }
    
    // TableView
    func numberOfRows_Table() -> Int {
        return gListCartProduct.count
    }
    
    func cellForRow_Table(at indexPath: IndexPath) -> CartProductSchema? {
        return gListCartProduct[indexPath.row]
    }
    
    func didSelectRow_Table(at indexPath: IndexPath) {
        let product = gListCartProduct[indexPath.row]
        guard let id = product.id else {
            return
        }
        self.view?.showHud()
        self.interactor.getProductDetail(id: id)
    }
    
    // Coll Suggest
    func numberOfItems_Suggest() -> Int {
        return self.listProductSuggest.count
    }
    
    func cellForItem_Suggest(at indexPath: IndexPath) -> ProductModel {
        return self.listProductSuggest[indexPath.row]
    }
    
    func didSelectItem_Suggest(at indexPath: IndexPath) {
        let product = self.listProductSuggest[indexPath.row]
        guard let id = product.id else {
            return
        }
        self.view?.showHud()
        self.interactor.getProductDetail(id: id)
    }
}

// MARK: - CartStore InteractorOutputProtocol
extension CartStoreViewModel: CartStoreInteractorOutputProtocol {
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>) {
        switch result {
        case .success(let model):
            self.listProductSuggest = model
            self.view?.reloadData_Suggest()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onDidSelectItem(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetDeliveryAddressFinished(with result: Result<DeliveryModel, APIError>) {
        switch result {
        case .success(let model):
            self.deliveryAddress = model
            self.view?.onGetDeliveryAddress(delivery: self.deliveryAddress)
            
            /// Tesst
//            if self.isFirst  {
//                self.isFirst = false
//                self.view?.onGetDeliveryAddress(delivery: nil)
//
//            } else {
//                self.deliveryAddress = model
//                self.view?.onGetDeliveryAddress(delivery: self.deliveryAddress)
//            }

        case .failure(let error):
            debugPrint(error)
            self.view?.onGetDeliveryAddress(delivery: nil)
        }
        self.view?.hideHud()
    }
}
