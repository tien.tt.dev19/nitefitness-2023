//
//  CellTableViewCart.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit

protocol CellTableViewCartDelegate: AnyObject {
    func onDidSelectedProduct(product: CartProductSchema?)
    func onDidChangeAmount(product: CartProductSchema?)
    func onDidTapCell(product: CartProductSchema?)
}

class CellTableViewCart: UITableViewCell {
    
    @IBOutlet weak var btn_CheckBox: UIButton!
    @IBOutlet weak var img_Image: UIImageView!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_PriceOld: UILabel!
    
    @IBOutlet weak var tf_Amount: UITextField!
    
    @IBOutlet weak var view_LineBottom: UIView!
    
    weak var delegate: CellTableViewCartDelegate?
    var product: CartProductSchema? {
        didSet {
            self.setDataUI()
        }
    }
    
    private var amount: Int = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.setInitUITextField()
        self.setInitGestureView()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setDataUI() {
        self.lbl_Title.text = self.product?.name
        self.lbl_Price.text = "\(self.product?.price?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.text = "\(self.product?.pricerOld?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.strikeThrough(true)
        self.tf_Amount.text = self.product?.amount?.asString ?? "0"
        self.btn_CheckBox.isSelected = self.product?.isSelected ?? false
        self.img_Image.setImage(with: self.product?.image ?? "", completion: nil)
        
        if self.product?.isSalePrice == true {
            self.lbl_PriceOld.isHidden = false
        } else {
            self.lbl_PriceOld.isHidden = true
        }
        
        self.amount = self.product?.amount ?? 1
    }
    
    @IBAction func onCheckBoxAction(_ sender: Any) {
        self.btn_CheckBox.isSelected = !self.btn_CheckBox.isSelected
        self.product?.isSelected = self.btn_CheckBox.isSelected
        self.delegate?.onDidSelectedProduct(product: self.product)
    }
    
    @IBAction func onMinusAction(_ sender: Any) {
        self.tf_Amount.resignFirstResponder()
        guard self.amount > 0 else {
            return
        }
        self.amount -= 1
        self.product?.amount = self.amount
        self.tf_Amount.text = self.amount.asString
        self.delegate?.onDidChangeAmount(product: self.product)
    }
    
    @IBAction func onPlusAction(_ sender: Any) {
        self.tf_Amount.resignFirstResponder()
        guard self.amount <= 999 else {
            return
        }
        self.amount += 1
        self.product?.amount = self.amount
        self.tf_Amount.text = self.amount.asString
        self.delegate?.onDidChangeAmount(product: self.product)
    }
}

extension CellTableViewCart: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Amount.delegate = self
        self.tf_Amount.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.isBackSpace {
            return true
            
        } else {
            if textField.text?.count ?? 0 < 3 {
                return true
                
            } else {
                return false
            }
        }
    }
    
    @objc func textFieldDidChangeSelection() {
        guard let text = self.tf_Amount.text else {
            return
        }
        guard let value = Int(text), value >= 0, value <= 999 else {
            return
        }

        self.amount = value
        self.product?.amount = self.amount
        self.delegate?.onDidChangeAmount(product: self.product)
    }
    
//    func textFieldDidChangeSelection(_ textField: UITextField) {
//        guard let text = textField.text else {
//            return
//        }
//        guard let value = Int(text), value >= 0, value <= 999 else {
//            return
//        }
//
//        self.amount = value
//        self.product?.amount = self.amount
//        self.delegate?.onDidChangeAmount(product: self.product)
//    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text?.count ?? 0 == 0 {
            self.amount = 1
            self.product?.amount = self.amount
            self.tf_Amount.text = self.amount.asString
            self.delegate?.onDidChangeAmount(product: self.product)
        }
    }
}

// MARK: - Init GestureRecognizer
extension CellTableViewCart {
    func setInitGestureView() {
        self.img_Image.isUserInteractionEnabled = true
        self.img_Image.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
        self.lbl_Title.isUserInteractionEnabled = true
        self.lbl_Title.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.delegate?.onDidTapCell(product: self.product)
    }
}
