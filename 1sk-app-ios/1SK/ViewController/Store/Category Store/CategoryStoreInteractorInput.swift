//
//  
//  CategoryStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol CategoryStoreInteractorInputProtocol {
    func getListFilterAttribute()
    func getListProduct(category: TypeCategoryStore, filters: [FilterStoreModel], page: Int, perPage: Int)
    func getProductDetail(id: Int)
}

// MARK: - Interactor Output Protocol
protocol CategoryStoreInteractorOutputProtocol: AnyObject {
    func onGetListFilterAttributeFinished(with result: Result<[FilterStoreModel], APIError>)
    func onGetListProductFinished(with result: Result<[ProductModel], APIError>, page: Int, total: Int)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
}

// MARK: - CategoryStore InteractorInput
class CategoryStoreInteractorInput {
    weak var output: CategoryStoreInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - CategoryStore InteractorInputProtocol
extension CategoryStoreInteractorInput: CategoryStoreInteractorInputProtocol {
    func getListFilterAttribute() {
        self.storeService?.getFilterAttribute(completion: { [weak self] result in
            self?.output?.onGetListFilterAttributeFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListProduct(category: TypeCategoryStore, filters: [FilterStoreModel], page: Int, perPage: Int) {
        self.storeService?.getListProduct(category: category, filters: filters, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductFinished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
