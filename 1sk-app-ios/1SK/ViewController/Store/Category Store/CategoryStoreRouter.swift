//
//  
//  CategoryStoreRouter.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol CategoryStoreRouterProtocol {
    func popViewController()
    func showSearchStore()
    func showCartStore()
    func showProductStore(product: ProductModel?)
    func presentFilterStore(filters: [FilterStoreModel], delegate: FilterStoreViewControllerDelegate?)
}

// MARK: - CategoryStore Router
class CategoryStoreRouter {
    weak var viewController: CategoryStoreViewController?
    static func setupModule(with type: TypeCategoryStore) -> CategoryStoreViewController {
        let viewController = CategoryStoreViewController()
        let router = CategoryStoreRouter()
        let interactorInput = CategoryStoreInteractorInput()
        let viewModel = CategoryStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        viewModel.typeCategory = type
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - CategoryStore RouterProtocol
extension CategoryStoreRouter: CategoryStoreRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showSearchStore() {
        let controller = SearchStoreRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func showCartStore() {
        let controller = CartStoreRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
    
    func presentFilterStore(filters: [FilterStoreModel], delegate: FilterStoreViewControllerDelegate?) {
        let controller = FilterStoreRouter.setupModule(filters: filters, delegate: delegate)
        self.viewController?.present(controller, animated: false)
    }
}
