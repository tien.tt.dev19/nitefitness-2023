//
//  
//  CategoryStoreViewController.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol CategoryStoreViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onDidSelectItem(product: ProductModel)
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    
    func setBadgeFilterUI(isFilter: Bool)
}

// MARK: - CategoryStore ViewController
class CategoryStoreViewController: BaseViewController {
    var router: CategoryStoreRouterProtocol!
    var viewModel: CategoryStoreViewModelProtocol!
    
    @IBOutlet weak var lbl_Title: UILabel!
    
    @IBOutlet weak var btn_Search: UIButton!
    @IBOutlet weak var btn_Filter: UIButton!
    @IBOutlet weak var view_BadgeFilter: UIView!
    
    @IBOutlet weak var coll_Category: UICollectionView!
    @IBOutlet weak var stack_NoData: UIStackView!
    
    private var refreshControl: UIRefreshControl?
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUIView()
        self.setInitUICollectionView()
        self.setInitRefreshControl()
    }
    
    private func setInitUIView() {
        self.lbl_Title.text = self.viewModel.typeCategoryValue.name.replaceCharacter(target: "\n", withString: " ")
        self.btn_Filter.isHidden = self.viewModel.typeCategoryValue != .NUTRITION
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.popViewController()
    }
    
    @IBAction func onSearchAction(_ sender: Any) {
        self.router.showSearchStore()
    }
    
    @IBAction func onFilterAction(_ sender: Any) {
        self.presentFilterStore()
    }
}

// MARK: - CategoryStore ViewProtocol
extension CategoryStoreViewController: CategoryStoreViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.coll_Category.reloadData()
        
        if self.viewModel.numberOfItems() > 0 {
            self.stack_NoData.isHidden = true
        } else {
            self.stack_NoData.isHidden = false
        }
    }
    
    func onDidSelectItem(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.coll_Category.insertItems(at: [IndexPath.init(row: index, section: 0)])
    }
    
    func setBadgeFilterUI(isFilter: Bool) {
        self.view_BadgeFilter.isHidden = !isFilter
    }
}

// MARK: Refresh Control
extension CategoryStoreViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.coll_Category.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}

// MARK: UICollectionViewDataSource
extension CategoryStoreViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_Category.registerNib(ofType: CellCollectionViewProduct.self)
        self.coll_Category.dataSource = self
        self.coll_Category.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        layout.itemSize = CGSize(width: width, height: height)
        self.coll_Category.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
        cell.config(product: self.viewModel.cellForItem(at: indexPath))
        return cell
    }
}

//// MARK: - UICollectionViewDelegateFlowLayout
//extension CategoryStoreViewController: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//        let widthArea = Constant.Screen.width
//        let width = (widthArea - 48) / 2
//        let height = width * (210/164)
//        
//        return CGSize(width: width, height: height)
//    }
//}

// MARK: UICollectionViewDelegate
extension CategoryStoreViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.didSelectItem(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}

// MARK: FilterStoreViewControllerDelegate
extension CategoryStoreViewController: FilterStoreViewControllerDelegate {
    func presentFilterStore() {
        self.router.presentFilterStore(filters: self.viewModel.listFilterAttributeValue, delegate: self)
    }
    
    func onApplyFilterData(listFilterAttribute: [FilterStoreModel]) {
        self.viewModel.onApplyFilterData(listFilterAttribute: listFilterAttribute)
    }
}
