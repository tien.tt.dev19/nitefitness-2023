//
//  
//  CategoryStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol CategoryStoreViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onApplyFilterData(listFilterAttribute: [FilterStoreModel])
    func onRefreshAction()
    func onLoadMoreAction()
    
    // CollectionView
    func numberOfItems() -> Int
    func cellForItem(at indexPath: IndexPath) -> ProductModel
    func didSelectItem(at indexPath: IndexPath)
    
    // Value
    var listFilterAttributeValue: [FilterStoreModel] { get }
    var typeCategoryValue: TypeCategoryStore { get }
}

// MARK: - CategoryStore ViewModel
class CategoryStoreViewModel {
    weak var view: CategoryStoreViewProtocol?
    private var interactor: CategoryStoreInteractorInputProtocol
    
    init(interactor: CategoryStoreInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var typeCategory: TypeCategoryStore!
    
    private var listFilterAttribute: [FilterStoreModel] = []
    private var listProduct: [ProductModel] = []
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 20
    private var isOutOfData = false
    
    func getData(page: Int) {
        if page == 1 {
            self.isOutOfData = false
        }
        
        self.view?.showHud()
        self.interactor.getListProduct(category: self.typeCategory, filters: self.listFilterAttribute, page: page, perPage: self.perPage)
    }
    
    func getDataFilterAttribute() {
        self.interactor.getListFilterAttribute()
    }
}

// MARK: - CategoryStore ViewModelProtocol
extension CategoryStoreViewModel: CategoryStoreViewModelProtocol {
    func onViewDidLoad() {
        self.getDataFilterAttribute()
        self.getData(page: 1)
    }
    
    // Action
    func onApplyFilterData(listFilterAttribute: [FilterStoreModel]) {
        self.view?.showHud()
        self.listFilterAttribute = listFilterAttribute
        
        var isFilter = false
        for filter in self.listFilterAttribute {
            if let attributes = filter.attributes {
                if attributes.first(where: {$0.isSelected == true}) != nil {
                    isFilter = true
                    break
                }
            }
        }
        self.view?.setBadgeFilterUI(isFilter: isFilter)
        self.getData(page: 1)
    }
    
    func onRefreshAction() {
        self.getData(page: 1)
    }
    
    func onLoadMoreAction() {
        if self.listProduct.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    // Coll Category
    func numberOfItems() -> Int {
        return self.listProduct.count
    }
    
    func cellForItem(at indexPath: IndexPath) -> ProductModel {
        return self.listProduct[indexPath.row]
    }
    
    func didSelectItem(at indexPath: IndexPath) {
        let product = self.listProduct[indexPath.row]
        guard let id = product.id else {
            return
        }
        self.view?.showHud()
        self.interactor.getProductDetail(id: id)
    }
    
    // Get Value
    var listFilterAttributeValue: [FilterStoreModel] {
        return self.listFilterAttribute
    }
    
    var typeCategoryValue: TypeCategoryStore {
        return self.typeCategory
    }
}

// MARK: - CategoryStore InteractorOutputProtocol
extension CategoryStoreViewModel: CategoryStoreInteractorOutputProtocol {
    func onGetListFilterAttributeFinished(with result: Result<[FilterStoreModel], APIError>) {
        switch result {
        case .success(let model):
            self.listFilterAttribute = model
            
//            for filter in self.listFilterAttribute {
//                if let listAttributes = filter.attributes, listAttributes.count > 0 {
//                    for attr in listAttributes {
//                        switch filter.displayLayout {
//                        case TypeFilterStore.text.rawValue:
//                            attr.type = TypeFilterStore.text
//
//                        case TypeFilterStore.price.rawValue:
//                            attr.type = TypeFilterStore.price
//
//                        default:
//                            break
//                        }
//                    }
//                }
//            }
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onGetListProductFinished(with result: Result<[ProductModel], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let listModel):
            if page <= 1 {
                self.listProduct = listModel
                self.view?.onReloadData()
                
            } else {
                for object in listModel {
                    self.listProduct.append(object)
                    self.view?.onInsertRow(at: self.listProduct.count - 1)
                }
            }
            
            self.total = total
            self.page = page
            
            // Set Out Of Data
            if self.listProduct.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure(let error):
            debugPrint(error)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        self.view?.hideHud()
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onDidSelectItem(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
