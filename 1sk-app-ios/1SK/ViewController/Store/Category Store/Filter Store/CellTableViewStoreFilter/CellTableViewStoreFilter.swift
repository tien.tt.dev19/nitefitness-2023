//
//  CellTableViewStoreFilter.swift
//  1SK
//
//  Created by TrungDN on 15/03/2022.
//

import UIKit

class CellTableViewStoreFilter: UITableViewCell {
    
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var btn_CheckBox: UIButton!
    @IBOutlet weak var lbl_Title: UILabel!
    
    var model: AttributeFilter? {
        didSet {
            self.btn_CheckBox.isSelected = self.model?.isSelected ?? false
            
            switch model?.type {
            case .text:
                self.lbl_Title.text = self.model?.title
                
            case .price:
                if let fromPrice = self.model?.fromPrice, let toPrice = self.model?.toPrice {
                    if fromPrice > 0 && toPrice > 0 {
                        self.lbl_Title.text = "\(fromPrice.formatNumber()) ₫ - \(toPrice.formatNumber()) ₫"
                        
                    } else if fromPrice == 0 && toPrice > 0 {
                        self.lbl_Title.text = "Dưới \(toPrice.formatNumber()) ₫"
                        
                    } else if fromPrice > 0 && toPrice == 0 {
                        self.lbl_Title.text = "Trên \(fromPrice.formatNumber()) ₫"
                        
                    } else {
                        self.lbl_Title.text = "\(fromPrice.formatNumber()) ₫ - \(toPrice.formatNumber()) ₫"
                    }
                    
                } else {
                    self.lbl_Title.text = self.model?.title
                }
                
            default:
                self.lbl_Title.text = self.model?.title
            }
            
            print("CellTableViewStoreFilter type:", model?.type?.rawValue ?? "null")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitGestureRecognizer()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.btn_CheckBox.isSelected = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setCheckBoxAction() {
        self.btn_CheckBox.isSelected = !self.btn_CheckBox.isSelected
        self.model?.isSelected = self.btn_CheckBox.isSelected
    }
    
    @IBAction func onCheckBoxAction(_ sender: Any) {
        self.setCheckBoxAction()
    }
}

extension CellTableViewStoreFilter {
    func setInitGestureRecognizer() {
        self.view_Content.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setCheckBoxAction()
    }
}
