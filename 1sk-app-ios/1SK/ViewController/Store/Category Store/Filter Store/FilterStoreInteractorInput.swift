//
//  
//  FilterStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol FilterStoreInteractorInputProtocol {
    
}

// MARK: - Interactor Output Protocol
protocol FilterStoreInteractorOutputProtocol: AnyObject {
    
}

// MARK: - FilterStore InteractorInput
class FilterStoreInteractorInput {
    weak var output: FilterStoreInteractorOutputProtocol?
}

// MARK: - FilterStore InteractorInputProtocol
extension FilterStoreInteractorInput: FilterStoreInteractorInputProtocol {
    
}
