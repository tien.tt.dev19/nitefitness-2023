//
//  
//  FilterStoreRouter.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol FilterStoreRouterProtocol {
    func dismissViewController()
}

// MARK: - FilterStore Router
class FilterStoreRouter {
    weak var viewController: FilterStoreViewController?
    static func setupModule(filters: [FilterStoreModel], delegate: FilterStoreViewControllerDelegate?) -> FilterStoreViewController {
        let viewController = FilterStoreViewController()
        viewController.delegate = delegate
        let router = FilterStoreRouter()
        let interactorInput = FilterStoreInteractorInput()
        let viewModel = FilterStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.modalPresentationStyle = .custom
        viewModel.view = viewController
        viewModel.listFilterAttributeRoot = filters
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - FilterStore RouterProtocol
extension FilterStoreRouter: FilterStoreRouterProtocol {
    func dismissViewController() {
        self.viewController?.dismiss(animated: false)
    }
}
