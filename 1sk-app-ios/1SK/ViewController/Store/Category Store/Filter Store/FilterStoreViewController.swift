//
//  
//  FilterStoreViewController.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

protocol FilterStoreViewControllerDelegate: AnyObject {
    func onApplyFilterData(listFilterAttribute: [FilterStoreModel])
}

// MARK: - ViewProtocol
protocol FilterStoreViewProtocol: AnyObject {
    func onReloadData()
    func onReloadData(section: Int)
    func onReloadData(indexPath: IndexPath)
    func onApplyFilterData(listFilterAttribute: [FilterStoreModel])
}

// MARK: - FilterStore ViewController
class FilterStoreViewController: BaseViewController {
    
    var router: FilterStoreRouterProtocol!
    var viewModel: FilterStoreViewModelProtocol!
    weak var delegate: FilterStoreViewControllerDelegate?
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ViewContent: NSLayoutConstraint!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUI()
        self.setInitUITableView()
        self.setInitGestureRecognizer()
    }
    
    func setInitUI() {
        self.constraint_bottom_ViewContent.constant = -self.view_Content.height
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: UIButton) {
        self.removeAnimate()
    }
    
    @IBAction func onClearFilterAction(_ sender: UIButton) {
        self.viewModel.onClearFilterAction()
    }
    
    @IBAction func onApplyFilterAction(_ sender: UIButton) {
        self.viewModel.onApplyFilterAction()
    }
}

// MARK: - FilterStore ViewProtocol
extension FilterStoreViewController: FilterStoreViewProtocol {
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onReloadData(section: Int) {
        self.tbv_TableView.reloadSections(IndexSet(integer: section), with: .automatic)
    }
    
    func onReloadData(indexPath: IndexPath) {
        self.tbv_TableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func onApplyFilterData(listFilterAttribute: [FilterStoreModel]) {
        self.delegate?.onApplyFilterData(listFilterAttribute: listFilterAttribute)
        self.removeAnimate()
    }
}

// MARK: - Init Animation
extension FilterStoreViewController {
    func showAnimate() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ViewContent.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ViewContent.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.router.dismissViewController()
        })
    }
}

// MARK: - FilterStore ViewProtocol
extension FilterStoreViewController {
    func setInitGestureRecognizer() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapBgAction)))
    }
    
    @objc func onTapBgAction() {
        self.removeAnimate()
    }
}

// MARK: - UITableViewDataSource
extension FilterStoreViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderStoreFilter.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewStoreFilter.self)

        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }

    // MARK: UITableView - Section
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.numberOfSections()
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: SectionHeaderStoreFilter.self)
        header.delegate = self
        header.model = self.viewModel.itemForSectionAt(in: section)
        return header
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        switch section {
        case self.viewModel.numberOfSections() - 1:
            return nil
            
        default:
            let viewFooter = UIView()
            viewFooter.backgroundColor = .white

            let seperatorView = UIView()
            seperatorView.backgroundColor = R.color.line_gray_01()
            viewFooter.addSubview(seperatorView)
            seperatorView.anchor(left: viewFooter.leftAnchor, bottom: viewFooter.bottomAnchor, right: viewFooter.rightAnchor, height: 1.0)

            return viewFooter
        }
    }

    // MARK: UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows(in: section)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewStoreFilter.self, for: indexPath)
        cell.model = self.viewModel.cellForRowAt(in: indexPath)
        return cell
    }
}

// MARK: - SectionHeaderStoreFilterDelegate
extension FilterStoreViewController: SectionHeaderStoreFilterDelegate {
    func onExpandAction(model: FilterStoreModel?) {
        self.viewModel.onExpandAction(model: model)
    }
}
