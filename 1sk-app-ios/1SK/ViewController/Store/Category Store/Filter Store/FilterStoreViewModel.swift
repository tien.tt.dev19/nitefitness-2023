//
//  
//  FilterStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol FilterStoreViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onExpandAction(model: FilterStoreModel?)
    func onClearFilterAction()
    func onApplyFilterAction()
    
    // UITableView
    func numberOfSections() -> Int
    func itemForSectionAt(in section: Int) -> FilterStoreModel?
    func numberOfRows(in section: Int) -> Int
    func cellForRowAt(in indexPath: IndexPath) -> AttributeFilter?
}

// MARK: - FilterStore ViewModel
class FilterStoreViewModel {
    weak var view: FilterStoreViewProtocol?
    private var interactor: FilterStoreInteractorInputProtocol
    
    init(interactor: FilterStoreInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listFilterAttributeRoot: [FilterStoreModel] = []
    var listFilterAttribute: [FilterStoreModel] = []
    
    
    func getCopyFilterAttribute() {
        for itemFilter in self.listFilterAttributeRoot {
            let filter = FilterStoreModel()
            filter.id = itemFilter.id
            filter.title = itemFilter.title
            filter.displayLayout = itemFilter.displayLayout
            filter.isExpand = itemFilter.isExpand
            
            if let attributes = itemFilter.attributes {
                filter.attributes = []
                for itemAttr in attributes {
                    let attr = AttributeFilter()
                    attr.id = itemAttr.id
                    attr.title = itemAttr.title
                    attr.fromPrice = itemAttr.fromPrice
                    attr.toPrice = itemAttr.toPrice
                    attr.type = itemAttr.type
                    attr.isSelected = itemAttr.isSelected
                    filter.attributes?.append(attr)
                }
            }
            
            self.listFilterAttribute.append(filter)
        }
        
        self.view?.onReloadData()
    }
}

// MARK: - FilterStore ViewModelProtocol
extension FilterStoreViewModel: FilterStoreViewModelProtocol {
    func onViewDidLoad() {
        self.getCopyFilterAttribute()
    }
    
    // Action
    func onExpandAction(model: FilterStoreModel?) {
        guard let index = self.listFilterAttribute.firstIndex(where: {$0.id == model?.id}) else {
            return
        }
        self.listFilterAttribute[index].isExpand = model?.isExpand
        self.view?.onReloadData(section: index)
    }
    
    func onClearFilterAction() {
        for filter in self.listFilterAttribute {
            filter.isExpand = true
            if let attributes = filter.attributes {
                for attr in attributes {
                    attr.isSelected = false
                }
            }
        }
        self.view?.onReloadData()
        
        //self.view?.onApplyFilterData(listFilterAttribute: self.listFilterAttribute)
    }
    
    func onApplyFilterAction() {
        self.view?.onApplyFilterData(listFilterAttribute: self.listFilterAttribute)
    }
    
    // UITableView
    func numberOfSections() -> Int {
        return self.listFilterAttribute.count
    }
    
    func itemForSectionAt(in section: Int) -> FilterStoreModel? {
        return self.listFilterAttribute[section]
    }
    
    func numberOfRows(in section: Int) -> Int {
        let filter = self.listFilterAttribute[section]
        if filter.isExpand == true {
            return filter.attributes?.count ?? 0
        } else {
            return  0
        }
    }
    
    func cellForRowAt(in indexPath: IndexPath) -> AttributeFilter? {
        return self.listFilterAttribute[indexPath.section].attributes?[indexPath.row]
    }
}

// MARK: - FilterStore InteractorOutputProtocol
extension FilterStoreViewModel: FilterStoreInteractorOutputProtocol {
    
}
