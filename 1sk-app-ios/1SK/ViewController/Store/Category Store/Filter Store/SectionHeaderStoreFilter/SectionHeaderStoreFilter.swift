//
//  SectionHeaderStoreFilter.swift
//  1SK
//
//  Created by TrungDN on 18/03/2022.
//

import UIKit

protocol SectionHeaderStoreFilterDelegate: AnyObject {
    func onExpandAction(model: FilterStoreModel?)
}

class SectionHeaderStoreFilter: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var btn_Expand: UIButton!
    
    weak var delegate: SectionHeaderStoreFilterDelegate?
    
    var model: FilterStoreModel? {
        didSet {
            self.btn_Expand.isSelected = self.model?.isExpand ?? false
            self.lbl_Title.text = self.model?.title?.uppercased()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.btn_Expand.setImage(R.image.ic_minus_filter(), for: .selected)
        self.btn_Expand.setImage(R.image.ic_plus_filter(), for: .normal)
    }
    
    @IBAction func onExpandAction(_ sender: UIButton) {
        self.btn_Expand.isSelected = !self.btn_Expand.isSelected
        self.model?.isExpand = self.btn_Expand.isSelected
        self.delegate?.onExpandAction(model: self.model)
    }
}
