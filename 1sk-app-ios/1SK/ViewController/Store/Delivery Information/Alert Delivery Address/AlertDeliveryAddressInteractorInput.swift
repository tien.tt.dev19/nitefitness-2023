//
//  
//  AlertDeliveryAddressInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol AlertDeliveryAddressInteractorInputProtocol {

}

// MARK: - Interactor Output Protocol
protocol AlertDeliveryAddressInteractorOutputProtocol: AnyObject {
    
}

// MARK: - AlertDeliveryAddress InteractorInput
class AlertDeliveryAddressInteractorInput {
    weak var output: AlertDeliveryAddressInteractorOutputProtocol?
}

// MARK: - AlertDeliveryAddress InteractorInputProtocol
extension AlertDeliveryAddressInteractorInput: AlertDeliveryAddressInteractorInputProtocol {

}
