//
//  
//  AlertDeliveryAddressRouter.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol AlertDeliveryAddressRouterProtocol {
    func dismissViewController()
}

// MARK: - AlertDeliveryAddress Router
class AlertDeliveryAddressRouter {
    weak var viewController: AlertDeliveryAddressViewController?
    
    static func setupModule(state: StateAlertAddress, listData: [Any]?, delegate: AlertDeliveryAddressViewDelegate?) -> AlertDeliveryAddressViewController {
        let viewController = AlertDeliveryAddressViewController()
        let router = AlertDeliveryAddressRouter()
        let interactorInput = AlertDeliveryAddressInteractorInput()
        let viewModel = AlertDeliveryAddressViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewController.modalPresentationStyle = .custom
        viewModel.view = viewController
        viewModel.state = state
        viewModel.listStates = listData as? [StateAddress]
        viewModel.listCities = listData as? [CityAddress]
        viewModel.listDistricts = listData as? [DistrictAddress]
        
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - AlertDeliveryAddress RouterProtocol
extension AlertDeliveryAddressRouter: AlertDeliveryAddressRouterProtocol {
    func dismissViewController() {
        self.viewController?.dismiss(animated: false)
    }
}
