//
//  
//  AlertDeliveryAddressViewController.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

enum StateAlertAddress {
    case states
    case cities
    case district
}

protocol AlertDeliveryAddressViewDelegate: AnyObject {
    func onDidChangeAddress(state: StateAlertAddress, object: Any?)
}

// MARK: - ViewProtocol
protocol AlertDeliveryAddressViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func reloadData()
    func setInitUIState(state: StateAlertAddress)
    
}

// MARK: - AlertDeliveryAddress ViewController
class AlertDeliveryAddressViewController: BaseViewController {
    var router: AlertDeliveryAddressRouterProtocol!
    var viewModel: AlertDeliveryAddressViewModelProtocol!
    weak var delegate: AlertDeliveryAddressViewDelegate?
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ViewContent: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var tbv_TableView: UITableView!
    
    var state: StateAlertAddress = .states
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUI()
        self.setInitUITapGestureRecognizer()
        self.setInitUITableView()
    }
    
    func setInitUI() {
        self.constraint_bottom_ViewContent.constant = -self.view_Content.height
    }
    
    // MARK: - Action
    
}

// MARK: - AlertDeliveryAddress ViewProtocol
extension AlertDeliveryAddressViewController: AlertDeliveryAddressViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func reloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func setInitUIState(state: StateAlertAddress) {
        switch state {
        case .states:
            self.lbl_Title.text = "Tỉnh/Thành phố"
            
        case .cities:
            self.lbl_Title.text = "Quận/Huyện"
            
        case .district:
            self.lbl_Title.text = "Phường/Xã"
        }
    }
}

// MARK: - Init Animation
extension AlertDeliveryAddressViewController {
    func showAnimate() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ViewContent.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func removeAnimate() {
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ViewContent.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.router.dismissViewController()
        })
    }
}

// MARK: - FilterStore ViewProtocol
extension AlertDeliveryAddressViewController {
    func setInitUITapGestureRecognizer() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapBgAction)))
    }
    
    @objc func onTapBgAction() {
        self.removeAnimate()
    }
}

// MARK: UITableViewDataSource
extension AlertDeliveryAddressViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewDeliveryAddress.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.state = self.viewModel.getStateAlertAddress()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewDeliveryAddress.self, for: indexPath)
        cell.delegate = self
        cell.indexPath = indexPath
        switch self.state {
        case .states:
            if let item = self.viewModel.cellForRow_States(at: indexPath) {
                cell.config(states: item)
            }
            
        case .cities:
            if let item = self.viewModel.cellForRow_Cities(at: indexPath) {
                cell.config(cities: item)
            }
            
        case .district:
            if let item = self.viewModel.cellForRow_Districts(at: indexPath) {
                cell.config(districts: item)
            }
        }
        
        return cell
    }
}

// MARK: CellTableViewDeliveryAddressDelegate
extension AlertDeliveryAddressViewController: CellTableViewDeliveryAddressDelegate {
    func onDidSelectedCell(indexPath: IndexPath?) {
        let state = self.viewModel.getStateAlertAddress()
        switch state {
        case .states:
            let object = self.viewModel.cellForRow_States(at: indexPath!)
            self.delegate?.onDidChangeAddress(state: state, object: object)
            
        case .cities:
            let object = self.viewModel.cellForRow_Cities(at: indexPath!)
            self.delegate?.onDidChangeAddress(state: state, object: object)
            
        case .district:
            let object = self.viewModel.cellForRow_Districts(at: indexPath!)
            self.delegate?.onDidChangeAddress(state: state, object: object)
        }
        
        self.tbv_TableView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.removeAnimate()
        }
    }
}
