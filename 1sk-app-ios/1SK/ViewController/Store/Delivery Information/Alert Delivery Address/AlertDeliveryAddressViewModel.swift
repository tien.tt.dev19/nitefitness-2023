//
//  
//  AlertDeliveryAddressViewModel.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol AlertDeliveryAddressViewModelProtocol {
    func onViewDidLoad()
    
    // TableView
    func numberOfRows() -> Int
    func cellForRow_States(at indexPath: IndexPath) -> StateAddress?
    func cellForRow_Cities(at indexPath: IndexPath) -> CityAddress?
    func cellForRow_Districts(at indexPath: IndexPath) -> DistrictAddress?
    
    // Get Data
    func getStateAlertAddress() -> StateAlertAddress
}

// MARK: - AlertDeliveryAddress ViewModel
class AlertDeliveryAddressViewModel {
    weak var view: AlertDeliveryAddressViewProtocol?
    private var interactor: AlertDeliveryAddressInteractorInputProtocol

    init(interactor: AlertDeliveryAddressInteractorInputProtocol) {
        self.interactor = interactor
    }

    var state: StateAlertAddress = .states
    
    var listStates: [StateAddress]?
    var listCities: [CityAddress]?
    var listDistricts: [DistrictAddress]?
    
    var indexPath: IndexPath?
    
}

// MARK: - AlertDeliveryAddress ViewModelProtocol
extension AlertDeliveryAddressViewModel: AlertDeliveryAddressViewModelProtocol {
    
    func onViewDidLoad() {
        self.view?.setInitUIState(state: self.state)
    }
    
    // TableView
    func numberOfRows() -> Int {
        switch self.state {
        case .states:
            return self.listStates?.count ?? 0
            
        case .cities:
            return self.listCities?.count ?? 0
            
        case .district:
            return self.listDistricts?.count ?? 0
        }
    }
    
    func cellForRow_States(at indexPath: IndexPath) -> StateAddress? {
        return self.listStates?[indexPath.row]
    }
    
    func cellForRow_Cities(at indexPath: IndexPath) -> CityAddress? {
        return self.listCities?[indexPath.row]
    }
    
    func cellForRow_Districts(at indexPath: IndexPath) -> DistrictAddress? {
        return self.listDistricts?[indexPath.row]
    }
    
    // Get Data
    func getStateAlertAddress() -> StateAlertAddress {
        return self.state
    }
}

// MARK: - AlertDeliveryAddress InteractorOutputProtocol
extension AlertDeliveryAddressViewModel: AlertDeliveryAddressInteractorOutputProtocol {
    
}
