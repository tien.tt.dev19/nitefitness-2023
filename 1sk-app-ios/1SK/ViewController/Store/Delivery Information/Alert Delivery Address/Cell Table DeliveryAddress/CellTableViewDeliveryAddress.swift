//
//  CellTableViewDeliveryAddress.swift
//  1SK
//
//  Created by Thaad on 18/03/2022.
//

import UIKit

protocol CellTableViewDeliveryAddressDelegate: AnyObject {
    func onDidSelectedCell(indexPath: IndexPath?)
}

class CellTableViewDeliveryAddress: UITableViewCell {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var btn_Status: UIButton!
    
    weak var delegate: CellTableViewDeliveryAddressDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onSelectedAction(_ sender: Any) {
        self.btn_Status.isSelected = true
        self.delegate?.onDidSelectedCell(indexPath: self.indexPath)
    }
    
    func config(states: StateAddress) {
        self.lbl_Name.text = states.name
        self.btn_Status.isSelected = states.isSelected ?? false
    }
    
    func config(cities: CityAddress) {
        self.lbl_Name.text = cities.name
        self.btn_Status.isSelected = cities.isSelected ?? false
    }
    
    func config(districts: DistrictAddress) {
        self.lbl_Name.text = districts.name
        self.btn_Status.isSelected = districts.isSelected ?? false
    }
    
}
