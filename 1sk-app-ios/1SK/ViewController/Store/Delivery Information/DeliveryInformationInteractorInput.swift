//
//  
//  DeliveryInformationInteractorInput.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol DeliveryInformationInteractorInputProtocol {
    func getListCountries()
    func setCreateDeliveryAddress(delivery: DeliveryModel)
    func setUpdateDeliveryAddress(delivery: DeliveryModel)
}

// MARK: - Interactor Output Protocol
protocol DeliveryInformationInteractorOutputProtocol: AnyObject {
    func onGetListCountriesFinished(with result: Result<[CountryModel], APIError>)
    
    func onSetCreateDeliveryAddressFinished(with result: Result<DeliveryModel, APIError>)
    func onSetUpdateDeliveryAddressFinished(with result: Result<DeliveryModel, APIError>)
}

// MARK: - DeliveryInformation InteractorInput
class DeliveryInformationInteractorInput {
    weak var output: DeliveryInformationInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - DeliveryInformation InteractorInputProtocol
extension DeliveryInformationInteractorInput: DeliveryInformationInteractorInputProtocol {
    func getListCountries() {
        self.storeService?.getListCountries(completion: { [weak self] result in
            self?.output?.onGetListCountriesFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setCreateDeliveryAddress(delivery: DeliveryModel) {
        self.storeService?.setCreateDeliveryAddress(delivery: delivery, completion: { [weak self] result in
            self?.output?.onSetCreateDeliveryAddressFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setUpdateDeliveryAddress(delivery: DeliveryModel) {
        self.storeService?.setUpdateDeliveryAddress(delivery: delivery, completion: { [weak self] result in
            self?.output?.onSetUpdateDeliveryAddressFinished(with: result.unwrapSuccessModel())
        })
    }
}
