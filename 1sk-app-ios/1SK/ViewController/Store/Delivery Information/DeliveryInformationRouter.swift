//
//  
//  DeliveryInformationRouter.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol DeliveryInformationRouterProtocol {
    func presentAlertDeliveryAddress(state: StateAlertAddress, listData: [Any]?, delegate: AlertDeliveryAddressViewDelegate?)
    func onPopViewController()
}

// MARK: - DeliveryInformation Router
class DeliveryInformationRouter {
    weak var viewController: DeliveryInformationViewController?
    
    static func setupModule(state: StateDeliveryView, delivery: DeliveryModel?, delegate: DeliveryInformationViewDelegate?) -> DeliveryInformationViewController {
        let viewController = DeliveryInformationViewController()
        let router = DeliveryInformationRouter()
        let interactorInput = DeliveryInformationInteractorInput()
        let viewModel = DeliveryInformationViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        viewModel.state = state
        viewModel.delivery = delivery
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - DeliveryInformation RouterProtocol
extension DeliveryInformationRouter: DeliveryInformationRouterProtocol {
    func presentAlertDeliveryAddress(state: StateAlertAddress, listData: [Any]?, delegate: AlertDeliveryAddressViewDelegate?) {
        let controller = AlertDeliveryAddressRouter.setupModule(state: state, listData: listData, delegate: delegate)
        self.viewController?.present(controller, animated: false)
    }
    
    func onPopViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
