//
//  
//  DeliveryInformationViewController.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit
import IQKeyboardManagerSwift

protocol DeliveryInformationViewDelegate: AnyObject {
    func onCreateDeliveryAddressSuccess(delivery: DeliveryModel?)
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?)
}

// MARK: - ViewProtocol
protocol DeliveryInformationViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setTextFieldState(state: StateAddress?)
    func setTextFieldCitie(citie: CityAddress?)
    func setTextFieldDistrict(district: DistrictAddress?)
    
    func setEditName()
    func setEditPhone()
    func setEditEmail()
    func setEditStates()
    func setEditCities()
    func setEditDistricts()
    func setEditAddress()
    
    func setIniUIUpdate(delivery: DeliveryModel?)
    
    func setUITypeDelivery(type: TypeDelivery)
    
    func onCreateDeliveryAddressSuccess(delivery: DeliveryModel?)
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?)
}

// MARK: - DeliveryInformation ViewController
class DeliveryInformationViewController: BaseViewController {
    var router: DeliveryInformationRouterProtocol!
    var viewModel: DeliveryInformationViewModelProtocol!
    weak var delegate: DeliveryInformationViewDelegate?
    
    @IBOutlet weak var view_Scroll: UIScrollView!
    
    @IBOutlet weak var tf_Name: SKTextField!
    @IBOutlet weak var tf_Phone: SKTextField!
    @IBOutlet weak var tf_Email: SKTextField!
    
    @IBOutlet weak var tf_State: SKTextField!
    @IBOutlet weak var tf_City: SKTextField!
    @IBOutlet weak var tf_District: SKTextField!
    @IBOutlet weak var tf_Address: SKTextField!
    
    @IBOutlet weak var btn_HomeAddress: UIButton!
    @IBOutlet weak var btn_CompanyAddress: UIButton!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUI()
        self.setInitUITextField()
        self.setInitKeyboardToolBar()
    }
    
    func setInitUI() {
        self.btn_HomeAddress.isSelected = true
    }
    
    // Contact
    private var sk_tf_Name: UITextField {
        return self.tf_Name.titleTextField
    }
    
    private var sk_tf_Phone: UITextField {
        return self.tf_Phone.titleTextField
    }
    
    private var sk_tf_Email: UITextField {
        return self.tf_Email.titleTextField
    }
    
    // Address
    private var sk_tf_State: UITextField {
        return self.tf_State.titleTextField
    }
    
    private var sk_tf_City: UITextField {
        return self.tf_City.titleTextField
    }
    
    private var sk_tf_District: UITextField {
        return self.tf_District.titleTextField
    }
    
    private var sk_tf_Address: UITextField {
        return self.tf_Address.titleTextField
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.onPopViewController()
    }
    
    @IBAction func onHomeAddressAction(_ sender: Any) {
        self.btn_HomeAddress.isSelected = true
        self.btn_CompanyAddress.isSelected = false
        
        self.viewModel.setTypeDelivery(type: .Home)
    }
    
    @IBAction func onCompanyAddressAction(_ sender: Any) {
        self.btn_HomeAddress.isSelected = false
        self.btn_CompanyAddress.isSelected = true
        
        self.viewModel.setTypeDelivery(type: .Company)
    }
    
    @IBAction func onSaveConfirmAction(_ sender: Any) {
        self.viewModel.onSaveConfirmAction()
    }
}

// MARK: - DeliveryInformation ViewProtocol
extension DeliveryInformationViewController: DeliveryInformationViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setTextFieldState(state: StateAddress?) {
        self.sk_tf_State.text = state?.name
        self.updateTypeLabelHiddenState(of: self.sk_tf_State)
    }
    
    func setTextFieldCitie(citie: CityAddress?) {
        self.sk_tf_City.text = citie?.name
        self.updateTypeLabelHiddenState(of: self.sk_tf_City)
    }
    
    func setTextFieldDistrict(district: DistrictAddress?) {
        self.sk_tf_District.text = district?.name
        self.updateTypeLabelHiddenState(of: self.sk_tf_District)
    }
    
    func setEditName() {
        self.sk_tf_Name.becomeFirstResponder()
    }
    
    func setEditPhone() {
        self.sk_tf_Phone.becomeFirstResponder()
    }
    
    func setEditEmail() {
        self.sk_tf_Email.becomeFirstResponder()
    }
    
    func setEditStates() {
        self.router.presentAlertDeliveryAddress(state: .states, listData: self.viewModel.getListStateAddress(), delegate: self)
    }
    
    func setEditCities() {
        self.router.presentAlertDeliveryAddress(state: .cities, listData: self.viewModel.getListCityAddress(), delegate: self)
    }
    
    func setEditDistricts() {
        self.router.presentAlertDeliveryAddress(state: .district, listData: self.viewModel.getListDistrictAddress(), delegate: self)
    }
    
    func setEditAddress() {
        self.sk_tf_Address.becomeFirstResponder()
    }
    
    func setIniUIUpdate(delivery: DeliveryModel?) {
        self.sk_tf_Name.text = delivery?.name
        self.sk_tf_Phone.text = delivery?.phone
        self.sk_tf_Email.text = delivery?.email
        self.sk_tf_State.text = delivery?.state?.name
        self.sk_tf_City.text = delivery?.city?.name
        self.sk_tf_District.text = delivery?.district?.name
        self.sk_tf_Address.text = delivery?.address
        
        switch delivery?.typeDelivery {
        case .Home:
            self.btn_HomeAddress.isSelected = true
            self.btn_CompanyAddress.isSelected = false
            
        case .Company:
            self.btn_HomeAddress.isSelected = false
            self.btn_CompanyAddress.isSelected = true
            
        default:
            break
        }
        
        for textField in [self.sk_tf_Name, self.sk_tf_Phone, sk_tf_Email, self.sk_tf_State, self.sk_tf_City, self.sk_tf_District, self.sk_tf_Address] {
            self.updateTypeLabelHiddenState(of: textField)
        }
    }
    
    func setUITypeDelivery(type: TypeDelivery) {
        switch type {
        case .Home:
            self.btn_HomeAddress.isSelected = true
            self.btn_CompanyAddress.isSelected = false
            
        case .Company:
            self.btn_HomeAddress.isSelected = false
            self.btn_CompanyAddress.isSelected = true
        }
    }
    
    func onCreateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        self.delegate?.onCreateDeliveryAddressSuccess(delivery: delivery)
        self.router.onPopViewController()
    }
    
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        self.delegate?.onUpdateDeliveryAddressSuccess(delivery: delivery)
        self.router.onPopViewController()
    }
}

// MARK: - UITextFieldDelegate
extension DeliveryInformationViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.sk_tf_Name.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Phone.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Email.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_State.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_City.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_District.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Address.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        
        self.tf_Name.setContentType(.name)
        self.tf_Name.setKeyboardType(.alphabet)
        self.tf_Phone.setContentType(.telephoneNumber)
        self.tf_Phone.setKeyboardType(.phonePad)
        self.tf_Email.setContentType(.emailAddress)
        self.tf_Email.setKeyboardType(.emailAddress)
        self.tf_Address.setContentType(.name)
        self.tf_Address.setKeyboardType(.alphabet)
        
        self.sk_tf_Name.delegate = self
        self.sk_tf_Phone.delegate = self
        self.sk_tf_Email.delegate = self
        self.sk_tf_State.delegate = self
        self.sk_tf_City.delegate = self
        self.sk_tf_District.delegate = self
        self.sk_tf_Address.delegate = self

        self.tf_Name.setEnable(isEnable: true)
        self.tf_Phone.setEnable(isEnable: true)
        self.tf_Email.setEnable(isEnable: true)
        self.tf_State.setEnable(isEnable: true)
        self.tf_City.setEnable(isEnable: true)
        self.tf_District.setEnable(isEnable: true)
        self.tf_Address.setEnable(isEnable: true)
        
        self.tf_Name.updateTypeLabelHiddenState()
        self.tf_Phone.updateTypeLabelHiddenState()
        self.tf_Email.updateTypeLabelHiddenState()
        self.tf_State.updateTypeLabelHiddenState()
        self.tf_City.updateTypeLabelHiddenState()
        self.tf_District.updateTypeLabelHiddenState()
        self.tf_Address.updateTypeLabelHiddenState()
        
        self.sk_tf_Name.clearButtonMode = .whileEditing
        self.sk_tf_Phone.clearButtonMode = .whileEditing
        self.sk_tf_Email.clearButtonMode = .whileEditing
        self.sk_tf_Address.clearButtonMode = .whileEditing
    }
    
    @objc func onTextFieldDidChange(_ textField: UITextField) {
        let newText = textField.text ?? ""
        
        print("onTextFieldDidChange newText:", newText)
        
        switch textField {
        case self.sk_tf_Name:
            self.viewModel.onUserNameChanged(with: newText)
            
        case self.sk_tf_Phone:
            self.viewModel.onPhoneChanged(with: newText)
            
        case self.sk_tf_Email:
            self.viewModel.onEmailChanged(with: newText)
            
        case self.sk_tf_Address:
            self.viewModel.onAddressChanged(with: newText)
            
        default:
            break
        }
    }
    
    private func updateTypeLabelHiddenState(of textField: UITextField) {
        switch textField {
        case self.sk_tf_Name:
            self.tf_Name.updateTypeLabelHiddenState()
            
        case self.sk_tf_Phone:
            self.tf_Phone.updateTypeLabelHiddenState()
            
        case self.sk_tf_Email:
            self.tf_Email.updateTypeLabelHiddenState()
            
        case self.sk_tf_State:
            self.tf_State.updateTypeLabelHiddenState()
            
        case self.sk_tf_City:
            self.tf_City.updateTypeLabelHiddenState()
            
        case self.sk_tf_District:
            self.tf_District.updateTypeLabelHiddenState()
            
        case self.sk_tf_Address:
            self.tf_Address.updateTypeLabelHiddenState()

        default:
            break
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case self.sk_tf_State:
            self.view.endEditing(true)
            self.router.presentAlertDeliveryAddress(state: .states, listData: self.viewModel.getListStateAddress(), delegate: self)
            return false

        case self.sk_tf_City:
            self.view.endEditing(true)
            self.router.presentAlertDeliveryAddress(state: .cities, listData: self.viewModel.getListCityAddress(), delegate: self)
            return false

        case self.sk_tf_District:
            self.view.endEditing(true)
            self.router.presentAlertDeliveryAddress(state: .district, listData: self.viewModel.getListDistrictAddress(), delegate: self)
            return false

        default:
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.sk_tf_Name:
            if textField.text?.count ?? 0 <= 32 {
                return true
                
            } else {
                return false
            }
            
        case self.sk_tf_Phone:
            if textField.text?.count ?? 0 < 3 {
                return true
                
            } else {
                let text0 = textField.text?.first
                switch text0 {
                case "0":
                    if textField.text?.count ?? 0 < 10 {
                        return true
                    } else {
                        return false
                    }
                    
//                case "+":
//                    if textField.text?.count ?? 0 < 12 {
//                        return true
//                    } else {
//                        return false
//                    }
                    
                default:
                    if textField.text?.count ?? 0 < 10 {
                        return true
                    } else {
                        return false
                    }
                }
            }
            
        default:
            return true
        }
    }
}

// MARK: AlertDeliveryAddressViewDelegate
extension DeliveryInformationViewController: AlertDeliveryAddressViewDelegate {
    func onDidChangeAddress(state: StateAlertAddress, object: Any?) {
        self.viewModel.onDidChangeAddress(state: state, object: object)
    }
}

// MARK: - InitKeyboardToolBar
extension DeliveryInformationViewController {
    private func setInitKeyboardToolBar() {
        self.view_Scroll.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapBgAction)))
        
        let name = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onNameDoneAction(_:)))
        self.sk_tf_Name.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập tên người dùng", rightBarButtonConfiguration: name)
        
        let phone = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onPhoneDoneAction(_:)))
        self.sk_tf_Phone.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập số điện thoại", rightBarButtonConfiguration: phone)
        
        let email = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onEmailDoneAction(_:)))
        self.sk_tf_Email.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập địa chỉ email", rightBarButtonConfiguration: email)
        
        let address = IQBarButtonItemConfiguration(title: "Xong", action: #selector(self.onAddressDoneAction(_:)))
        self.sk_tf_Address.addKeyboardToolbarWithTarget(target: self, titleText: "Nhập địa chỉ nhà", rightBarButtonConfiguration: address)
    }
    
    @objc func onTapBgAction() {
        self.view.endEditing(true)
    }
    
    @objc func onNameDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @objc func onPhoneDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @objc func onEmailDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    @objc func onAddressDoneAction(_ sender: Any) {
        self.view.endEditing(true)
    }
}
