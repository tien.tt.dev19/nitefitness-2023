//
//  
//  DeliveryInformationViewModel.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//
//

import UIKit

enum StateDeliveryView {
    case Update
    case Create
}

// MARK: - ViewModelProtocol
protocol DeliveryInformationViewModelProtocol {
    func onViewDidLoad()
    
    // UITextField
    func onUserNameChanged(with name: String)
    func onPhoneChanged(with phone: String)
    func onEmailChanged(with email: String)
    func onAddressChanged(with address: String)
    
    // Get Data
    func getListStateAddress() -> [StateAddress]
    func getListCityAddress() -> [CityAddress]
    func getListDistrictAddress() -> [DistrictAddress]
    
    // Set Data
    func onDidChangeAddress(state: StateAlertAddress, object: Any?)
    func setTypeDelivery(type: TypeDelivery)
    func onSaveConfirmAction()
    
}

// MARK: - DeliveryInformation ViewModel
class DeliveryInformationViewModel {
    weak var view: DeliveryInformationViewProtocol?
    private var interactor: DeliveryInformationInteractorInputProtocol

    init(interactor: DeliveryInformationInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var state: StateDeliveryView = .Create
    var delivery: DeliveryModel? = DeliveryModel()
    
    var listStatesCountries: [StateAddress] = []
    
    var stateAddress: StateAddress?
    var cityAddress: CityAddress?
    var districtAddress: DistrictAddress?
    
    func getDataCountries() {
        guard gListCountryModel.count > 0 else {
            self.view?.showHud()
            self.interactor.getListCountries()
            return
        }
        
        self.onParseDataCountry(model: gListCountryModel)
        //self.onParseDataCountry(model: gListCountryModel.clone())
    }
    
    func onParseDataCountry(model: [CountryModel]) {
        if let vietnam = model.first {
            self.delivery?.country?.id = vietnam.id
            self.listStatesCountries = vietnam.states ?? []
            self.setInitDataSelected()
        }
    }
    
    func setInitData() {
        if self.delivery == nil {
            self.delivery = DeliveryModel()
        }
        
        switch self.state {
        case .Create:
            self.view?.setUITypeDelivery(type: .Home)
            
        case .Update:
            self.view?.setIniUIUpdate(delivery: self.delivery)
            
            self.stateAddress = self.delivery?.state
            self.cityAddress = self.delivery?.city
            self.districtAddress = self.delivery?.district
            
            self.view?.setUITypeDelivery(type: self.delivery?.typeDelivery ?? .Home)
        }
    }
    
    func setInitDataSelected() {
        for state in self.listStatesCountries {
            if state.id == self.stateAddress?.id {
                state.isSelected = true
                self.stateAddress = state
                
                if state.cities?.count ?? 0 > 0 {
                    for city in state.cities! {
                        if city.id == self.cityAddress?.id {
                            city.isSelected = true
                            self.cityAddress = city
                            
                        } else {
                            city.isSelected = false
                        }
                        
                        if city.districts?.count ?? 0 > 0 {
                            for district in city.districts! {
                                if district.id == self.districtAddress?.id {
                                    district.isSelected = true
                                    self.districtAddress = district
                                    
                                } else {
                                    district.isSelected = false
                                }
                            }
                        }
                        //
                    }
                }
                //
                
            } else {
                state.isSelected = false
            }
        }
    }
}

// MARK: - DeliveryInformation ViewModelProtocol
extension DeliveryInformationViewModel: DeliveryInformationViewModelProtocol {
    func onViewDidLoad() {
        self.setInitData()
        self.getDataCountries()
    }
    
    // UITextField
    func onUserNameChanged(with name: String) {
        self.delivery?.name = name
    }
    
    func onPhoneChanged(with phone: String) {
        self.delivery?.phone = phone
    }
    
    func onEmailChanged(with email: String) {
        self.delivery?.email = email
    }
    
    func onAddressChanged(with address: String) {
        self.delivery?.address = address
    }
    
    // Get Data
    func getListStateAddress() -> [StateAddress] {
        return self.listStatesCountries
    }
    
    func getListCityAddress() -> [CityAddress] {
        return self.stateAddress?.cities ?? []
    }
    
    func getListDistrictAddress() -> [DistrictAddress] {
        return self.cityAddress?.districts ?? []
    }
    
    // Set Data
    func onDidChangeAddress(state: StateAlertAddress, object: Any?) {
        switch state {
        case .states:
            self.stateAddress = object as? StateAddress
            self.cityAddress = nil
            self.districtAddress = nil
            
        case .cities:
            self.cityAddress = object as? CityAddress
            self.districtAddress = nil
            
        case .district:
            self.districtAddress = object as? DistrictAddress
        }
        
        self.view?.setTextFieldState(state: self.stateAddress)
        self.view?.setTextFieldCitie(citie: self.cityAddress)
        self.view?.setTextFieldDistrict(district: self.districtAddress)
        
        self.delivery?.state = self.stateAddress
        self.delivery?.city = self.cityAddress
        self.delivery?.district = self.districtAddress
        
        for state in self.listStatesCountries {
            if state.id == self.stateAddress?.id {
                state.isSelected = true
                
                if state.cities?.count ?? 0 > 0 {
                    for city in state.cities! {
                        if city.id == self.cityAddress?.id {
                            city.isSelected = true
                        } else {
                            city.isSelected = false
                        }
                        
                        if city.districts?.count ?? 0 > 0 {
                            for district in city.districts! {
                                if district.id == self.districtAddress?.id {
                                    district.isSelected = true
                                } else {
                                    district.isSelected = false
                                }
                            }
                        }
                        
                    }
                }
                
            } else {
                state.isSelected = false
            }
        }
    }
    
    func setTypeDelivery(type: TypeDelivery) {
        self.delivery?.typeDelivery = type
    }
    
    func onSaveConfirmAction() {
        guard self.delivery?.name?.count ?? 0 >= 2 else {
            SKToast.shared.showToast(content: "Tên người dùng chưa nhập & không đúng (gợi ý: Nguyễn Văn A)")
            self.view?.setEditName()
            return
        }
        guard self.delivery?.phone?.count ?? 0 == 10 else {
            SKToast.shared.showToast(content: "Số điện thoại không đúng (gợi ý: 0912345678)")
            self.view?.setEditPhone()
            return
        }
        guard self.delivery?.phone?.first == "0" else {
            SKToast.shared.showToast(content: "Số điện thoại không đúng (gợi ý: 0912345678)")
            self.view?.setEditPhone()
            return
        }
        
        guard self.delivery?.email?.count ?? 0 == 0 || self.delivery?.email?.isValidEmail() == true else {
            SKToast.shared.showToast(content: "Địa chỉ email không đúng (gợi ý: email@domain.com)")
            self.view?.setEditEmail()
            return
        }
        
        guard self.delivery?.state?.id ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Chưa chọn Tỉnh/Thành phố")
            self.view?.setEditStates()
            return
        }
        guard self.delivery?.city?.id ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Chưa chọn Quận/Huyện")
            self.view?.setEditCities()
            return
        }
        guard self.delivery?.district?.id ?? 0 > 0 else {
            SKToast.shared.showToast(content: "Chưa chọn Phường/Xã")
            self.view?.setEditDistricts()
            return
        }
        guard self.delivery?.address?.count ?? 0 >= 6 else {
            SKToast.shared.showToast(content: "Tên đường, tòa nhà, số nhà tối thiểu phải từ 6 ký tự trở lên")
            self.view?.setEditAddress()
            return
        }
        
        switch self.state {
        case .Create:
            guard let delivery = self.delivery else {
                return
            }
            self.view?.showHud()
            self.interactor.setCreateDeliveryAddress(delivery: delivery)

        case .Update:
            guard let delivery = self.delivery, let userId = gUser?.id else {
                return
            }

            self.view?.showHud()
            self.delivery?.customerId = userId
            self.interactor.setUpdateDeliveryAddress(delivery: delivery)
        }
    }
    
    
}

// MARK: - DeliveryInformation InteractorOutputProtocol
extension DeliveryInformationViewModel: DeliveryInformationInteractorOutputProtocol {
    func onGetListCountriesFinished(with result: Result<[CountryModel], APIError>) {
        switch result {
        case .success(let model):
            self.onParseDataCountry(model: model)
            gListCountryModel = model
            
//            if let vietnam = model.first {
//                self.delivery?.country?.id = vietnam.id
//                self.listStatesCountries = vietnam.states ?? []
//                self.setInitDataSelected()
//            }
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onSetCreateDeliveryAddressFinished(with result: Result<DeliveryModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Lưu thông tin nhận hàng thành công")
            self.view?.onCreateDeliveryAddressSuccess(delivery: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onSetUpdateDeliveryAddressFinished(with result: Result<DeliveryModel, APIError>) {
        switch result {
        case .success(let model):
            SKToast.shared.showToast(content: "Cập nhât thông tin nhận hàng thành công")
            self.view?.onUpdateDeliveryAddressSuccess(delivery: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
