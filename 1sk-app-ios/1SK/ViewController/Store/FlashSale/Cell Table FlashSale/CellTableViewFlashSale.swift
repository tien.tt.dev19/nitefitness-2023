//
//  CellTableViewFlashSale.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//

import UIKit
import Cosmos

class CellTableViewFlashSale: UITableViewCell {
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_PriceOld: UILabel!
    @IBOutlet weak var lbl_SalePercent: UILabel!
    @IBOutlet weak var view_Rate: CosmosView!
    @IBOutlet weak var lbl_soldNumber: UILabel!
    @IBOutlet weak var view_soldProducts: UIView!
    @IBOutlet weak var contraint_soldWidth: NSLayoutConstraint!
    
    var model: Product? {
        didSet {
            if let data = model {
                self.lbl_Title.text = data.name ?? ""
                self.lbl_Price.text = "\(data.frontSalePrice?.formatNumber() ?? "0")đ"
                self.img_Image.setImageWith(imageUrl: data.image ?? "")
                self.view_Rate.rating = Double(data.reviewsAvg ?? 0)
                self.lbl_soldNumber.text = "Đã bán \(data.sold ?? 0)"
                self.lbl_PriceOld.text = "\(data.priceWithTaxes?.formatNumber() ?? "0")đ"
                self.lbl_PriceOld.strikeThrough(true)
                self.lbl_SalePercent.text = "-\(data.salePercentage ?? 0)%"
                let totalSale = Double(data.quantityFlashSale ?? 0)
                let soldout = Double(data.sold ?? 0)
                
                if totalSale == 0 {
                    self.contraint_soldWidth.constant = 0
                    return
                }
                
                self.contraint_soldWidth.constant = CGFloat((soldout / totalSale) * Double(self.view_soldProducts.width))
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
