//
//  
//  FlashSaleInteractorInput.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol FlashSaleInteractorInputProtocol {
    func getProductDetail(id: Int)
    func getAllProducts(page: Int, perPage: Int)
}

// MARK: - Interactor Output Protocol
protocol FlashSaleInteractorOutputProtocol: AnyObject {
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
    func onGet_All_FlashSale_Finished(with result: Result<[Product], APIError>, page: Int, total: Int)
}

// MARK: - FlashSale InteractorInput
class FlashSaleInteractorInput {
    weak var output: FlashSaleInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - FlashSale InteractorInputProtocol
extension FlashSaleInteractorInput: FlashSaleInteractorInputProtocol {
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getAllProducts(page: Int, perPage: Int) {
        self.storeService?.getAllFlashSaleProduct(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGet_All_FlashSale_Finished(with: result.unwrapSuccessModel(), page: page, total: result.getTotal() ?? 0)
        })
    }
}
