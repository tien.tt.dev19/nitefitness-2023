//
//  
//  FlashSaleRouter.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol FlashSaleRouterProtocol {
    func showProductStore(product: ProductModel?)
}

// MARK: - FlashSale Router
class FlashSaleRouter {
    weak var viewController: FlashSaleViewController?
    
    static func setupModule(endDate: String, endTime: String) -> FlashSaleViewController {
        let viewController = FlashSaleViewController()
        let router = FlashSaleRouter()
        let interactorInput = FlashSaleInteractorInput()
        let viewModel = FlashSaleViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.hidesBottomBarWhenPushed = true
        viewModel.view = viewController
        viewModel.endDate = endDate
        viewModel.endTime = endTime
        interactorInput.storeService = StoreService()
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - FlashSale RouterProtocol
extension FlashSaleRouter: FlashSaleRouterProtocol {
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
}
