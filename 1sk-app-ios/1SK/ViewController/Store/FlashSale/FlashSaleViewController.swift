//
//  
//  FlashSaleViewController.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol FlashSaleViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onLoadingSuccess()
    func onInsertRow(at index: Int)
    
    //UITableView
    func onReloadData()
    func onDidSelectItem(product: ProductModel)
}

// MARK: - FlashSale ViewController
class FlashSaleViewController: BaseViewController {
    var router: FlashSaleRouterProtocol!
    var viewModel: FlashSaleViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    private var isLoading = false
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillAppear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.title = "Flash Sale"
        self.setInitUITableView()
    }
    
    // MARK: - Action
}

// MARK: - FlashSale ViewProtocol
extension FlashSaleViewController: FlashSaleViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
    }
    
    func onDidSelectItem(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
    
    func onLoadingSuccess() {
        self.isLoading = false
    }
    
    func onInsertRow(at index: Int) {
        self.tbv_TableView.insertRows(at: [IndexPath.init(row: index, section: 0)], with: .bottom)
    }
}

// MARK: - UITableViewDataSource
extension FlashSaleViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: HeaderSectionTableFlashSale.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewFlashSale.self)
        self.tbv_TableView.separatorStyle = .none
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: HeaderSectionTableFlashSale.self)
        
        let endDate = self.viewModel.onGetEndDate()
        let endTime = self.viewModel.onGetEndTime()
        
        let endDay = endDate + " " + endTime
        let endDayToDate = endDay.toDate(.ymdSlashhms)
        let endDayTimestamp = endDayToDate?.toTimestamp() ?? 0
        let secondLeft = abs(Int(endDayTimestamp - Date().toTimestamp()))
        
        header.deinitTimer()
        header.onCountTimeAction(time: secondLeft)
        return header
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems_FlashSale()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewFlashSale.self, for: indexPath)
        cell.model = self.viewModel.cellForItem_FlashSale(at: indexPath)
        return cell
    }
}

// MARK: - UITableViewDelegate
extension FlashSaleViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.didSelectFlashSale(at: indexPath)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let isReachingEnd = scrollView.contentOffset.y >= 0 && scrollView.contentOffset.y >= (scrollView.contentSize.height - scrollView.frame.size.height)
        
        if isReachingEnd && self.isLoading == false {
            self.isLoading = true
            self.viewModel.onLoadMoreAction()
        }
    }
}
