//
//  
//  FlashSaleViewModel.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol FlashSaleViewModelProtocol {
    func onViewDidLoad()
    func onViewWillAppear()
    func onLoadMoreAction()
    func onGetEndDate() -> String
    func onGetEndTime() -> String
    
    // FlashSale
    func numberOfItems_FlashSale() -> Int
    func cellForItem_FlashSale(at indexPath: IndexPath) -> Product?
    func onGetflashSaleProducts() -> [Product]?
    func didSelectFlashSale(at indexPath: IndexPath)
}

// MARK: - FlashSale ViewModel
class FlashSaleViewModel {
    weak var view: FlashSaleViewProtocol?
    private var interactor: FlashSaleInteractorInputProtocol
    
    // Loadmore
    private var total = 0
    private var page = 1
    private let perPage = 10
    private var isOutOfData = false
    var flashSaleProducts: [Product] = []
    
    var endDate: String = ""
    var endTime: String = ""

    init(interactor: FlashSaleInteractorInputProtocol) {
        self.interactor = interactor
    }
}

// MARK: - FlashSale ViewModelProtocol
extension FlashSaleViewModel: FlashSaleViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onViewWillAppear() {
        self.getData(page: self.page)
    }
    
    func numberOfItems_FlashSale() -> Int {
        return self.flashSaleProducts.count
    }
    
    func cellForItem_FlashSale(at indexPath: IndexPath) -> Product? {
        return self.flashSaleProducts[indexPath.row]
    }
    
    func onGetflashSaleProducts() -> [Product]? {
        return self.flashSaleProducts
    }
    
    func didSelectFlashSale(at indexPath: IndexPath) {
        let product = self.flashSaleProducts[indexPath.row]
        guard let id = product.id else {
            return
        }
        
        self.view?.showHud()
        self.interactor.getProductDetail(id: id)
    }
    
    func onLoadMoreAction() {
        if self.flashSaleProducts.count >= self.perPage && !self.isOutOfData {
            self.getData(page: self.page + 1)
            
        } else {
            SKToast.shared.showToast(content: "Đã tải hết dữ liệu")
            self.view?.onLoadingSuccess()
        }
    }
    
    func getData(page: Int) {
        self.view?.showHud()

        if page == 1 {
            self.isOutOfData = false
        }
        
        self.interactor.getAllProducts(page: page, perPage: self.perPage)
    }
    
    func onGetEndDate() -> String {
        return self.endDate
    }
    
    func onGetEndTime() -> String {
        return self.endTime
    }
}

// MARK: - FlashSale InteractorOutputProtocol
extension FlashSaleViewModel: FlashSaleInteractorOutputProtocol {
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onDidSelectItem(product: model)

        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGet_All_FlashSale_Finished(with result: Result<[Product], APIError>, page: Int, total: Int) {
        switch result {
        case .success(let model):
            if page <= 1 {
                self.flashSaleProducts = model
                self.view?.onReloadData()
                
            } else {
                for object in model {
                    self.flashSaleProducts.append(object)
                    self.view?.onInsertRow(at: self.flashSaleProducts.count - 1)
                }
            }
            
            self.page = page
            self.total = total
            
            // Set Out Of Data
            if self.flashSaleProducts.count >= self.total {
                self.isOutOfData = true
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                self.view?.onLoadingSuccess()
            }
            
        case .failure:
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                self.view?.onLoadingSuccess()
            }
        }
        
        self.view?.hideHud()
    }
}
