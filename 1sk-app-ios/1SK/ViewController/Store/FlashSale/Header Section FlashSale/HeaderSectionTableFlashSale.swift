//
//  HeaderSectionTableFlashSale.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//

import UIKit

class HeaderSectionTableFlashSale: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lbl_day: UILabel!
    @IBOutlet weak var lbl_hour: UILabel!
    @IBOutlet weak var lbl_minute: UILabel!
    @IBOutlet weak var lbl_second: UILabel!
    
    var timer: Timer?
    
    var endDate: Int = 0 {
        didSet {
            let days = self.endDate / 86400
            if days / 10 > 0 {
                self.lbl_day.text = String(days)
            } else {
                self.lbl_day.text = "0\(days)"
            }
            
            let hour = self.endDate % 86400 / 3600
            
            if hour / 10 > 0 {
                self.lbl_hour.text = String(hour)
            } else {
                self.lbl_hour.text = "0\(hour)"
            }
            
            let minute = self.endDate % 3600 / 60
            if minute / 10 > 0 {
                self.lbl_minute.text = String(minute)
            } else {
                self.lbl_minute.text = "0\(minute)"
            }
            
            let second = self.endDate % 3600 % 60
            if second / 10 > 0 {
                self.lbl_second.text = String(second)
            } else {
                self.lbl_second.text = "0\(second)"
            }
            
        }
    }
    
    func onCountTimeAction(time: Int) {
        self.endDate = time
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            DispatchQueue.main.async {
                self.endDate  -= 1
            }
        })
        
        RunLoop.main.add(self.timer!, forMode: .common)
    }
    
    
    func deinitTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
}
