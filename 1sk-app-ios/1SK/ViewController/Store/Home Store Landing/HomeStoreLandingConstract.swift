//
//  
//  HomeStoreLandingConstract.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

// MARK: - View
protocol HomeStoreLandingViewProtocol: AnyObject {
    func onUpdateBadgeNotification(count: Int)
}

// MARK: - Presenter
protocol HomeStoreLandingPresenterProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onNotificationAction()
}

// MARK: - Interactor Input
protocol HomeStoreLandingInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol HomeStoreLandingInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol HomeStoreLandingRouterProtocol: BaseRouterProtocol {
    func showNotificationViewController()
}
