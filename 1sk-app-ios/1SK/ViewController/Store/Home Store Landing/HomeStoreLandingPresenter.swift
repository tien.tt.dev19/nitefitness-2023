//
//  
//  HomeStoreLandingPresenter.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

class HomeStoreLandingPresenter {

    weak var view: HomeStoreLandingViewProtocol?
    private var interactor: HomeStoreLandingInteractorInputProtocol
    private var router: HomeStoreLandingRouterProtocol

    init(interactor: HomeStoreLandingInteractorInputProtocol,
         router: HomeStoreLandingRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }
    
}

// MARK: - HomeStoreLandingPresenterProtocol
extension HomeStoreLandingPresenter: HomeStoreLandingPresenterProtocol {
    func onViewDidLoad() {
        
    }
    
    func onViewDidAppear() {
        self.view?.onUpdateBadgeNotification(count: gBadgeCare)
    }
    
    // Action
    func onNotificationAction() {
        self.router.showNotificationViewController()
    }
}

// MARK: - HomeStoreLandingInteractorOutput 
extension HomeStoreLandingPresenter: HomeStoreLandingInteractorOutputProtocol {

}
