//
//  
//  HomeStoreLandingRouter.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

class HomeStoreLandingRouter: BaseRouter {
    static func setupModule() -> HomeStoreLandingViewController {
        let viewController = HomeStoreLandingViewController()
        let router = HomeStoreLandingRouter()
        let interactorInput = HomeStoreLandingInteractorInput()
        let presenter = HomeStoreLandingPresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - HomeStoreLandingRouterProtocol
extension HomeStoreLandingRouter: HomeStoreLandingRouterProtocol {
    func showNotificationViewController() {
        let notificationVC = NotificationRouter.setupModule(pushNotiModel: nil)
        viewController?.navigationController?.pushViewController(notificationVC, animated: true)
    }
}
