//
//  
//  HomeStoreLandingViewController.swift
//  1SK
//
//  Created by vuongbachthu on 11/11/21.
//
//

import UIKit

class HomeStoreLandingViewController: BaseViewController {

    var presenter: HomeStoreLandingPresenterProtocol!
    
    @IBOutlet weak var view_Badge: UIView!
    @IBOutlet weak var lbl_Badge: EdgeInsetLabel!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.presenter.onViewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.presenter.onViewDidAppear()
    }
    
    // MARK: - Setup
    private func setupInit() {

    }
    
    // MARK: - Action
    @IBAction func onNotificationAction(_ sender: Any) {
        self.presenter.onNotificationAction()
    }
    
    @IBAction func onSeeMoreAction(_ sender: Any) {
        self.onOpenLinkToSafari(url: LANDING_STORE_URL)
    }
}

// MARK: - HomeStoreLandingViewProtocol
extension HomeStoreLandingViewController: HomeStoreLandingViewProtocol {
    
    func onUpdateBadgeNotification(count: Int) {
        self.lbl_Badge.text = count.asString
        if count > 0 {
            self.view_Badge.isHidden = false
        } else {
            self.view_Badge.isHidden = true
        }
    }
}
