//
//  CellCollectionViewCategory.swift
//  1SK
//
//  Created by Thaad on 07/03/2022.
//

import UIKit

class CellCollectionViewCategory: UICollectionViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(category: TypeCategoryStore) {
        self.img_Image.image = category.image
        self.lbl_Title.text = category.name
    }
}
