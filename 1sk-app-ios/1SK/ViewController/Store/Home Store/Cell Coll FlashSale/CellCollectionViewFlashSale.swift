//
//  CellCollectionViewFlashSale.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//

import UIKit
import Cosmos

class CellCollectionViewFlashSale: UICollectionViewCell {

    @IBOutlet weak var lbl_ProductsName: UILabel!
    @IBOutlet weak var lbl_Prices: UILabel!
    @IBOutlet weak var view_StarRate: CosmosView!
    @IBOutlet weak var img_ProductThumb: UIImageView!
    @IBOutlet weak var lbl_SalePercent: UILabel!
    @IBOutlet weak var lbl_SaledCount: UILabel!
    @IBOutlet weak var contraint_soldWidth: NSLayoutConstraint!
    @IBOutlet weak var view_PercentShowOut: UIView!
    
    var model: Product? {
        didSet {
            if let data = model {
                self.lbl_ProductsName.text = data.name ?? ""
                self.lbl_Prices.text = "\(data.frontSalePrice?.formatNumber() ?? "0")đ"
                self.img_ProductThumb.setImageWith(imageUrl: data.image ?? "")
                self.view_StarRate.rating = Double(data.reviewsAvg ?? 0)
                self.lbl_SaledCount.text = "Đã bán \(data.sold ?? 0)"
                self.lbl_SalePercent.text = "-\(data.salePercentage ?? 0)%"

                let totalSale = Double(data.quantityFlashSale ?? 0)
                let soldout = Double(data.sold ?? 0)
                self.contraint_soldWidth.constant = CGFloat((soldout / totalSale) * Double(self.view_PercentShowOut.width))
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
