//
//  CellCollectionViewProduct.swift
//  1SK
//
//  Created by Thaad on 07/03/2022.
//

import UIKit
import Cosmos

class CellCollectionViewProduct: UICollectionViewCell {

    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_PriceOld: UILabel!
    @IBOutlet weak var view_Rate: CosmosView!
    
    @IBOutlet weak var view_Sale: UIView!
    @IBOutlet weak var lbl_Sale: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(product: ProductModel) {
        self.lbl_Title.text = product.name
        self.lbl_Price.text = "\(product.frontSalePriceWithTaxes?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.text = "\(product.priceWithTaxes?.formatNumber() ?? "0")đ"
        self.view_Rate.rating = product.reviewsAvg ?? 0
        self.lbl_PriceOld.strikeThrough(true)
        self.lbl_Sale.text = "-\(product.salePercentage ?? 0)%"
        
        if product.isSalePrice == true {
            self.lbl_PriceOld.isHidden = false
            self.view_Sale.isHidden = false
        } else {
            self.lbl_PriceOld.isHidden = true
            self.view_Sale.isHidden = true
        }
        
        self.img_Image.setImage(with: product.image ?? "", completion: nil)
        
    }
}
