//
//  
//  HomeStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 07/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HomeStoreInteractorInputProtocol {
    func getSliderBanner(typeSlider: SliderType)
    func getListProductSuggest(page: Int, perPage: Int)
    func getListProductFeatured(type: TypeCategoryStore, page: Int, perPage: Int)
    func getProductDetail(id: Int)
    func getFlashSaleProducts()
    func getListCountries()
}

// MARK: - Interactor Output Protocol
protocol HomeStoreInteractorOutputProtocol: AnyObject {
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>)
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>)
    func onGetListProductFeaturedFinished(with result: Result<[ProductModel], APIError>, type: TypeCategoryStore)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
    func onGetFlashSaleProductsFinished(with result: Result<[FlashSaleModel], APIError>)
    func onGetListCountriesFinished(with result: Result<[CountryModel], APIError>)
}

// MARK: - HomeStore InteractorInput
class HomeStoreInteractorInput {
    weak var output: HomeStoreInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - HomeStore InteractorInputProtocol
extension HomeStoreInteractorInput: HomeStoreInteractorInputProtocol {
    func getSliderBanner(typeSlider: SliderType) {
        self.storeService?.getSliderBanner(typeSlider: typeSlider, completion: { [weak self] result in
            self?.output?.onGetSliderBannerFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListProductSuggest(page: Int, perPage: Int) {
        self.storeService?.getListProductSuggest(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductSuggestFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListProductFeatured(type: TypeCategoryStore, page: Int, perPage: Int) {
        self.storeService?.getListProductFeatured(type: type, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductFeaturedFinished(with: result.unwrapSuccessModel(), type: type)
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getListCountries() {
        self.storeService?.getListCountries(completion: { [weak self] result in
            self?.output?.onGetListCountriesFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getFlashSaleProducts() {
        self.storeService?.getFlashSaleProducts(completion: { [weak self] result in
            self?.output?.onGetFlashSaleProductsFinished(with: result.unwrapSuccessModel())
        })
    }
}
