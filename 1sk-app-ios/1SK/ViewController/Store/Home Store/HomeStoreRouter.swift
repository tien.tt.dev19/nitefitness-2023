//
//  
//  HomeStoreRouter.swift
//  1SK
//
//  Created by Thaad on 07/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HomeStoreRouterProtocol {
    func showCategoryStore(with type: TypeCategoryStore)
    func showFlashSaleRouter(endDate: String, endTime: String)
    func showSearchStore()
    func showCartStore()
    func showProductStore(product: ProductModel?)
}

// MARK: - HomeStore Router
class HomeStoreRouter {
    weak var viewController: HomeStoreViewController?
    static func setupModule() -> HomeStoreViewController {
        let viewController = HomeStoreViewController()
        let router = HomeStoreRouter()
        let interactorInput = HomeStoreInteractorInput()
        let viewModel = HomeStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HomeStore RouterProtocol
extension HomeStoreRouter: HomeStoreRouterProtocol {
    func showCategoryStore(with type: TypeCategoryStore) {
        let controller = CategoryStoreRouter.setupModule(with: type)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showFlashSaleRouter(endDate: String, endTime: String) {
        let controller = FlashSaleRouter.setupModule(endDate: endDate, endTime: endTime)
        self.viewController?.navigationController?.show(controller, sender: nil)
//        let controller = FlashSaleProductsRouter.setupModule(endDate: endDate, endTime: endTime)
//        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func showSearchStore() {
        let controller = SearchStoreRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func showCartStore() {
        let controller = CartStoreRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
    
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
}
