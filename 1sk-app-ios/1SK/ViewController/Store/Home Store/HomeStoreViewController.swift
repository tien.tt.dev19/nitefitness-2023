//
//  
//  HomeStoreViewController.swift
//  1SK
//
//  Created by Thaad on 07/03/2022.
//
//

import UIKit
import ImageSlideshow

// MARK: - ViewProtocol
protocol HomeStoreViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setUISliderBanner(sliderBanner: SliderBannerModel?)
    
    func reloadData_Category()
    func reloadData_Suggest()
    func reloadData_DeviceFit()
    func reloadData_DeviceCare()
    func reloadData_Nutrition()
    func reloadData_Accessory()
    func reloadData_FlashSale()
    
    func onDidSelectItem(product: ProductModel)
    
    func setAnimationBadgeCart()
}

// MARK: - HomeStore ViewController
class HomeStoreViewController: BaseViewController {
    var router: HomeStoreRouterProtocol!
    var viewModel: HomeStoreViewModelProtocol!
    
    @IBOutlet weak var view_BadgeCart: UIView!
    @IBOutlet weak var lbl_CountCart: UILabel!
    
    @IBOutlet weak var scr_ScrollView: UIScrollView!
    @IBOutlet weak var view_SlideShow: ImageSlideshow!
    
    @IBOutlet weak var coll_Category: UICollectionView!
    
    @IBOutlet weak var coll_Suggest: UICollectionView!
    @IBOutlet weak var coll_FlashSale: UICollectionView!
    @IBOutlet weak var coll_DeviceFit: UICollectionView!
    @IBOutlet weak var coll_DeviceCare: UICollectionView!
    @IBOutlet weak var coll_Nutrition: UICollectionView!
    @IBOutlet weak var coll_Accessory: UICollectionView!
    
    @IBOutlet weak var constraint_height_CollSuggest: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_CollDeviceFit: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_CollDeviceCare: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_CollNutrition: NSLayoutConstraint!
    @IBOutlet weak var constraint_height_CollAccessory: NSLayoutConstraint!
    
    @IBOutlet weak var view_Suggest: UIView!
    @IBOutlet weak var view_Device: UIView!
    @IBOutlet weak var view_Nutrition: UIView!
    @IBOutlet weak var view_Accessory: UIView!
    @IBOutlet weak var view_FlashSale: UIView!
    
    @IBOutlet weak var lbl_day: UILabel!
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    
    private var refreshControl: UIRefreshControl?
    var currentPageSlideShow = 0
    
    var timer: Timer?
    
    var countDountTime: Int = 0 {
        didSet {
            if self.countDountTime == 0 {
                self.viewModel.onViewWillAppear()
                
            } else {
                let days = self.countDountTime / 86400
                if days / 10 > 0 {
                    self.lbl_day.text = String(days)
                } else {
                    self.lbl_day.text = "0\(days)"
                }
                
                let hour = self.countDountTime % 86400 / 3600

                if hour / 10 > 0 {
                    self.hourLabel.text = String(hour)
                } else {
                    self.hourLabel.text = "0\(hour)"
                }

                let minute = self.countDountTime % 3600 / 60
                if minute / 10 > 0 {
                    self.minuteLabel.text = String(minute)
                } else {
                    self.minuteLabel.text = "0\(minute)"
                }

                let second = self.countDountTime % 3600 % 60
                if second / 10 > 0 {
                    self.secondLabel.text = String(second)
                } else {
                    self.secondLabel.text = "0\(second)"
                }
            }
        }
    }
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.viewModel.onViewWillAppear()
        self.setAnimationBadgeCart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    deinit {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUICollectionView()
        self.setInitRefreshControl()
    }
    
    // MARK: - Action
    @IBAction func onSearchAction(_ sender: Any) {
        self.router.showSearchStore()
    }
    
    @IBAction func onCartAction(_ sender: Any) {
        self.router.showCartStore()
    }
    
    @IBAction func onSeeMoreFlashSaleAction(_ sender: Any) {
        let endDate = self.viewModel.onGetflashSaleProducts()?.first?.endDate ??  ""
        let endTime = self.viewModel.onGetflashSaleProducts()?.first?.endTime ?? ""

        self.router.showFlashSaleRouter(endDate: endDate, endTime: endTime)
    }
    
    @IBAction func onSeeMoreDeviceFitAction(_ sender: Any) {
        self.router.showCategoryStore(with: .DEVICE_FIT)
    }
    
    @IBAction func onSeeMoreDeviceCareAction(_ sender: Any) {
        self.router.showCategoryStore(with: .DEVICE_CARE)
    }
    
    @IBAction func onSeeMoreNutritionAction(_ sender: Any) {
        self.router.showCategoryStore(with: .NUTRITION)
    }
    
    @IBAction func onSeeMoreAccessoryAction(_ sender: Any) {
        self.router.showCategoryStore(with: .ACCESSORY)
    }
}

// MARK: - HomeStore ViewProtocol
extension HomeStoreViewController: HomeStoreViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func setUISliderBanner(sliderBanner: SliderBannerModel?) {
        guard let sliderModel = sliderBanner else {
            return
        }
        guard let listBanners = sliderModel.items, listBanners.count > 0 else {
            return
        }
        
        var imageSource: [InputSource] = []
        for banner in listBanners {
            if let image = banner.image {
                if let url = URL(string: image) {
                    let kingfisherSource = KingfisherSource(url: url, placeholder: R.image.img_default(), options: nil)
                    imageSource.append(kingfisherSource)
                }
            }
        }
        
        self.setInitImageSlideshow(imageSource: imageSource)
    }
    
    func reloadData_Category() {
        self.coll_Category.reloadData()
    }
    
    func reloadData_Suggest() {
        self.coll_Suggest.reloadData()
        
        let count = self.viewModel.numberOfItems_Product(type: nil)
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_CollSuggest.constant = CGFloat(((count/2) * 210) + 50)
            
        } else {
            if count <= 2 {
                self.constraint_height_CollSuggest.constant = CGFloat(210 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_CollSuggest.constant = CGFloat(((row/2) * 210) + 50)
            }
        }
        
        if count > 0 {
            self.view_Suggest.isHidden = false
        } else {
            self.view_Suggest.isHidden = true
        }
    }
    
    func reloadData_DeviceFit() {
        self.coll_DeviceFit.reloadData()
        
        let count = self.viewModel.numberOfItems_Product(type: .DEVICE_FIT)
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_CollDeviceFit.constant = CGFloat(((count/2) * 210) + 50)
            
        } else {
            if count <= 2 {
                self.constraint_height_CollDeviceFit.constant = CGFloat(210 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_CollDeviceFit.constant = CGFloat(((row/2) * 210) + 50)
            }
        }
        
        if count > 0 {
            self.view_Device.isHidden = false
        } else {
            self.view_Device.isHidden = true
        }
    }
    
    func reloadData_DeviceCare() {
        self.coll_DeviceCare.reloadData()
        
        let count = self.viewModel.numberOfItems_Product(type: .DEVICE_CARE)
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_CollDeviceCare.constant = CGFloat(((count/2) * 210) + 50)
            
        } else {
            if count <= 2 {
                self.constraint_height_CollDeviceCare.constant = CGFloat(210 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_CollDeviceCare.constant = CGFloat(((row/2) * 210) + 50)
            }
        }
        
        if count > 0 {
            self.view_Device.isHidden = false
        } else {
            self.view_Device.isHidden = true
        }
    }
    
    func reloadData_Nutrition() {
        self.coll_Nutrition.reloadData()
        
        let count = self.viewModel.numberOfItems_Product(type: .NUTRITION)
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_CollNutrition.constant = CGFloat(((count/2) * 210) + 50)
            
        } else {
            if count <= 2 {
                self.constraint_height_CollNutrition.constant = CGFloat(210 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_CollNutrition.constant = CGFloat(((row/2) * 210) + 50)
            }
        }
        
        if count > 0 {
            self.view_Nutrition.isHidden = false
        } else {
            self.view_Nutrition.isHidden = true
        }
    }
    
    func reloadData_Accessory() {
        self.coll_Accessory.reloadData()
        
        let count = self.viewModel.numberOfItems_Product(type: .ACCESSORY)
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_CollAccessory.constant = CGFloat(((count/2) * 210) + 50)
            
        } else {
            if count <= 2 {
                self.constraint_height_CollAccessory.constant = CGFloat(210 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_CollAccessory.constant = CGFloat(((row/2) * 210) + 50)
            }
        }
        
        if count > 0 {
            self.view_Accessory.isHidden = false
        } else {
            self.view_Accessory.isHidden = true
        }
    }
    
    func reloadData_FlashSale() {
        // deinit the previous timer
        self.timer?.invalidate()
        self.timer = nil
        
        if self.viewModel.onGetflashSaleProducts()?.count == 0 {
            self.view_FlashSale.isHidden = true
            return
        }
        
        self.view_FlashSale.isHidden = false
        
        let endDate = self.viewModel.onGetflashSaleProducts()?.first?.endDate ??  ""
        let endTime = self.viewModel.onGetflashSaleProducts()?.first?.endTime ?? ""
        
        let endDay = endDate + " " + endTime
        let endDayToDate = endDay.toDate(.ymdSlashhms)
        let endDayTimestamp = endDayToDate?.toTimestamp() ?? 0
        
        let secondLeft = abs(Int(endDayTimestamp - Date().toTimestamp()))
        
        self.countDountTime = secondLeft
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            DispatchQueue.main.async {
                self.countDountTime  -= 1
            }
        })
        
        RunLoop.main.add(self.timer!, forMode: .common)
        
        self.coll_FlashSale.reloadData()
    }
    
    func onDidSelectItem(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
    
    func setAnimationBadgeCart() {
        guard gListCartProduct.count > 0 else {
            self.lbl_CountCart.text = "0"
            self.view_BadgeCart.isHidden = true
            return
        }
        
        var totalBadge = 0
        for item in gListCartProduct {
            if let amount = item.amount {
                totalBadge += amount
            }
        }
        if totalBadge > 9 {
            self.lbl_CountCart.text = "9+"
        } else {
            self.lbl_CountCart.text = totalBadge.asString
        }
        
        self.view_BadgeCart.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn) {
            // Scale your image
            self.view_BadgeCart.transform = CGAffineTransform.identity.scaledBy(x: 2, y: 2)
            
        } completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                // undo in 1 seconds
                self.view_BadgeCart.transform = CGAffineTransform.identity
            })
        }
    }
}

// MARK: Refresh Control
extension HomeStoreViewController {
    func setInitRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.attributedTitle = NSAttributedString(string: "Kéo xuống để tải mới", attributes: [NSAttributedString.Key.foregroundColor: R.color.mainColor() ?? UIColor.darkGray] )
        self.refreshControl?.tintColor = R.color.mainColor()
        self.refreshControl?.addTarget(self, action: #selector(self.onRefreshAction), for: .valueChanged)
        if let refresh = self.refreshControl {
            self.scr_ScrollView.addSubview(refresh)
        }
    }
    
    @objc func onRefreshAction() {
        self.refreshControl?.endRefreshing()
        self.viewModel.onRefreshAction()
        return
    }
}


// MARK: ImageSlideshowDelegate
extension HomeStoreViewController: ImageSlideshowDelegate {
    func setInitImageSlideshow(imageSource: [InputSource]) {
        self.view_SlideShow.slideshowInterval = 3.0
        
        self.view_SlideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        self.view_SlideShow.contentScaleMode = UIViewContentMode.scaleAspectFit

        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = R.color.mainColor()
        pageControl.pageIndicatorTintColor = .white
        self.view_SlideShow.pageIndicator = pageControl

        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        self.view_SlideShow.activityIndicator = DefaultActivityIndicator()
        self.view_SlideShow.delegate = self

        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.view_SlideShow.setImageInputs(imageSource)

        self.view_SlideShow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureSlideshow)))
    }
    
    @objc func onTapGestureSlideshow() {
        guard let banner = self.viewModel.getItemBanner(at: self.currentPageSlideShow) else {
            return
        }
        
        switch banner.typeBanner {
        case .link:
            self.showHud()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.hideHud()
                guard let url = URL(string: banner.link ?? "") else { return }
                UIApplication.shared.open(url)
            }
            
        case .product:
            guard let productId = banner.productId else {
                return
            }
            self.viewModel.onDidSelectBanner(productId: productId)
            
        default:
            break
        }
    }
    
    // ImageSlideshowDelegate
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        self.currentPageSlideShow = page
    }
}

// MARK: UICollectionViewDataSource
extension HomeStoreViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_Category.registerNib(ofType: CellCollectionViewCategory.self)
        self.coll_FlashSale.registerNib(ofType: CellCollectionViewFlashSale.self)
        self.coll_Suggest.registerNib(ofType: CellCollectionViewProduct.self)
        self.coll_DeviceFit.registerNib(ofType: CellCollectionViewProduct.self)
        self.coll_DeviceCare.registerNib(ofType: CellCollectionViewProduct.self)
        self.coll_Nutrition.registerNib(ofType: CellCollectionViewProduct.self)
        self.coll_Accessory.registerNib(ofType: CellCollectionViewProduct.self)
        
        self.coll_Category.dataSource = self
        self.coll_Category.delegate = self
        
        self.coll_FlashSale.dataSource = self
        self.coll_FlashSale.delegate = self
        
        self.coll_Suggest.dataSource = self
        self.coll_Suggest.delegate = self
        
        self.coll_DeviceFit.dataSource = self
        self.coll_DeviceFit.delegate = self
        
        self.coll_DeviceCare.dataSource = self
        self.coll_DeviceCare.delegate = self
        
        self.coll_Nutrition.dataSource = self
        self.coll_Nutrition.delegate = self
        
        self.coll_Accessory.dataSource = self
        self.coll_Accessory.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164)
        
        let widthEst = (self.coll_Category.frame.width - 48) / 3
        let heightCategory = self.coll_Category.frame.height
        let widthCategory = widthEst - (widthEst/8)
        
        let layout_Category = UICollectionViewFlowLayout()
        layout_Category.scrollDirection = .horizontal
        layout_Category.minimumLineSpacing = 0
        layout_Category.minimumInteritemSpacing = 0
        layout_Category.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout_Category.itemSize = CGSize(width: widthCategory, height: heightCategory)
        self.coll_Category.collectionViewLayout = layout_Category
        
        let layout_FlashSale = UICollectionViewFlowLayout()
        layout_FlashSale.scrollDirection = .horizontal
        layout_FlashSale.minimumLineSpacing = 16
        layout_FlashSale.minimumInteritemSpacing = 16
        layout_FlashSale.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_FlashSale.itemSize = CGSize(width: 145, height: self.coll_FlashSale.frame.height)
        self.coll_FlashSale.collectionViewLayout = layout_FlashSale
        
        let layout_Suggest = UICollectionViewFlowLayout()
        layout_Suggest.minimumLineSpacing = 16
        layout_Suggest.minimumInteritemSpacing = 16
        layout_Suggest.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_Suggest.itemSize = CGSize(width: width, height: height)
        self.coll_Suggest.collectionViewLayout = layout_Suggest
        
        let layout_DeviceFit = UICollectionViewFlowLayout()
        layout_DeviceFit.minimumLineSpacing = 16
        layout_DeviceFit.minimumInteritemSpacing = 16
        layout_DeviceFit.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_DeviceFit.itemSize = CGSize(width: width, height: height)
        self.coll_DeviceFit.collectionViewLayout = layout_DeviceFit
        
        let layout_DeviceCare = UICollectionViewFlowLayout()
        layout_DeviceCare.minimumLineSpacing = 16
        layout_DeviceCare.minimumInteritemSpacing = 16
        layout_DeviceCare.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_DeviceCare.itemSize = CGSize(width: width, height: height)
        self.coll_DeviceCare.collectionViewLayout = layout_DeviceCare
        
        let layout_Nutrition = UICollectionViewFlowLayout()
        layout_Nutrition.minimumLineSpacing = 16
        layout_Nutrition.minimumInteritemSpacing = 16
        layout_Nutrition.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_Nutrition.itemSize = CGSize(width: width, height: height)
        self.coll_Nutrition.collectionViewLayout = layout_Nutrition
        
        let layout_Accessory = UICollectionViewFlowLayout()
        layout_Accessory.minimumLineSpacing = 16
        layout_Accessory.minimumInteritemSpacing = 16
        layout_Accessory.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout_Accessory.itemSize = CGSize(width: width, height: height)
        self.coll_Accessory.collectionViewLayout = layout_Accessory
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case self.coll_Category:
            return self.viewModel.numberOfItems_Category()
            
        case self.coll_FlashSale:
            return self.viewModel.numberOfItems_FlashSale()
            
        case self.coll_Suggest:
            return self.viewModel.numberOfItems_Product(type: nil)
            
        case self.coll_DeviceFit:
            return self.viewModel.numberOfItems_Product(type: .DEVICE_FIT)
            
        case self.coll_DeviceCare:
            return self.viewModel.numberOfItems_Product(type: .DEVICE_CARE)
            
        case self.coll_Nutrition:
            return self.viewModel.numberOfItems_Product(type: .NUTRITION)
            
        case self.coll_Accessory:
            return self.viewModel.numberOfItems_Product(type: .ACCESSORY)
            
        default:
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView {
        case self.coll_Category:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewCategory.self, for: indexPath)
            cell.config(category: self.viewModel.cellForItem_Category(at: indexPath))
            return cell
            
        case self.coll_FlashSale:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewFlashSale.self, for: indexPath)
            cell.model = self.viewModel.cellForItem_FlashSale(at: indexPath)
            return cell
            
        case self.coll_Suggest:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
            cell.config(product: self.viewModel.cellForItem_Product(at: indexPath, type: nil))
            return cell
            
        case self.coll_DeviceFit:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
            cell.config(product: self.viewModel.cellForItem_Product(at: indexPath, type: .DEVICE_FIT))
            return cell
            
        case self.coll_DeviceCare:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
            cell.config(product: self.viewModel.cellForItem_Product(at: indexPath, type: .DEVICE_CARE))
            return cell
            
        case self.coll_Nutrition:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
            cell.config(product: self.viewModel.cellForItem_Product(at: indexPath, type: .NUTRITION))
            return cell
            
        case self.coll_Accessory:
            let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
            cell.config(product: self.viewModel.cellForItem_Product(at: indexPath, type: .ACCESSORY))
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
}

// MARK: UICollectionViewDelegate
extension HomeStoreViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView {
        case self.coll_Category:
            self.router.showCategoryStore(with: self.viewModel.cellForItem_Category(at: indexPath))
            
        case self.coll_FlashSale:
            self.viewModel.didSelectFlashSale(at: indexPath)
            
        case self.coll_Suggest:
            self.viewModel.didSelectItem_Product(at: indexPath, type: nil)
            
        case self.coll_DeviceFit:
            self.viewModel.didSelectItem_Product(at: indexPath, type: .DEVICE_FIT)
            
        case self.coll_DeviceCare:
            self.viewModel.didSelectItem_Product(at: indexPath, type: .DEVICE_CARE)
            
        case self.coll_Nutrition:
            self.viewModel.didSelectItem_Product(at: indexPath, type: .NUTRITION)
            
        case self.coll_Accessory:
            self.viewModel.didSelectItem_Product(at: indexPath, type: .ACCESSORY)
            
        default:
            break
        }
    }
}
