//
//  
//  HomeStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 07/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol HomeStoreViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    func onViewWillAppear()
    
    // ScrollView
    func onRefreshAction()
    
    // CollectionView
    func numberOfItems_Category() -> Int
    func cellForItem_Category(at indexPath: IndexPath) -> TypeCategoryStore

    
    // Coll Product
    func numberOfItems_Product(type: TypeCategoryStore?) -> Int
    func cellForItem_Product(at indexPath: IndexPath, type: TypeCategoryStore?) -> ProductModel
    func didSelectItem_Product(at indexPath: IndexPath, type: TypeCategoryStore?)
    
    // FlashSale
    func numberOfItems_FlashSale() -> Int
    func cellForItem_FlashSale(at indexPath: IndexPath) -> Product?
    func didSelectFlashSale(at indexPath: IndexPath)

    // Slide Show
    func getItemBanner(at page: Int) -> ItemBanner?
    func onDidSelectBanner(productId: Int)
    
    func onGetflashSaleProducts() -> [FlashSaleModel]?
}

// MARK: - HomeStore ViewModel
class HomeStoreViewModel {
    weak var view: HomeStoreViewProtocol?
    private var interactor: HomeStoreInteractorInputProtocol

    init(interactor: HomeStoreInteractorInputProtocol) {
        self.interactor = interactor
    }

    var sliderBanner: SliderBannerModel?
    
    var listCategory: [TypeCategoryStore] = []
    var listProductSuggest: [ProductModel] = []
    var listProductDeviceFit: [ProductModel] = []
    var listProductDeviceCare: [ProductModel] = []
    var listProductNutrition: [ProductModel] = []
    var listProductAccessory: [ProductModel] = []
    var flashSaleProducts: [FlashSaleModel] = []
    
    func getDataCartProductLocal() {
        gListCartProduct = Shared.instance.cartProductSchema
    }
    
    func setDataCategory() {
        self.listCategory.append(.DEVICE_FIT)
        self.listCategory.append(.DEVICE_CARE)
        self.listCategory.append(.NUTRITION)
        self.listCategory.append(.ACCESSORY)
        
        self.view?.reloadData_Category()
    }
    
    func getInitData() {
        self.view?.showHud()
        self.interactor.getSliderBanner(typeSlider: .HomeStore)
        self.interactor.getListProductSuggest(page: 1, perPage: 6)
        
        self.interactor.getListProductFeatured(type: .DEVICE_FIT, page: 1, perPage: 6)
        self.interactor.getListProductFeatured(type: .DEVICE_CARE, page: 1, perPage: 6)
        self.interactor.getListProductFeatured(type: .NUTRITION, page: 1, perPage: 6)
        self.interactor.getListProductFeatured(type: .ACCESSORY, page: 1, perPage: 6)
        self.interactor.getListCountries()
        self.interactor.getFlashSaleProducts()
    }
}

// MARK: - HomeStore ViewModelProtocol
extension HomeStoreViewModel: HomeStoreViewModelProtocol {
    func onViewDidLoad() {
        self.getDataCartProductLocal()
        self.setDataCategory()
        self.getInitData()
    }
    
    func onViewDidAppear() {
        //
    }
    
    func onViewWillAppear() {
        self.view?.showHud()
        self.interactor.getFlashSaleProducts()
    }
    
    func onRefreshAction() {
        self.getInitData()
    }
    
    // Coll Category
    func numberOfItems_Category() -> Int {
        return self.listCategory.count
    }
    
    func cellForItem_Category(at indexPath: IndexPath) -> TypeCategoryStore {
        return self.listCategory[indexPath.row]
    }
    
    // Coll Product
    func numberOfItems_Product(type: TypeCategoryStore?) -> Int {
        switch type {
        case .DEVICE_FIT:
            return self.listProductDeviceFit.count
            
        case .DEVICE_CARE:
            return self.listProductDeviceCare.count
            
        case .NUTRITION:
            return self.listProductNutrition.count
            
        case .ACCESSORY:
            return self.listProductAccessory.count
            
        default:
            return self.listProductSuggest.count
        }
    }
    
    func cellForItem_Product(at indexPath: IndexPath, type: TypeCategoryStore?) -> ProductModel {
        switch type {
        case .DEVICE_FIT:
            return self.listProductDeviceFit[indexPath.row]
            
        case .DEVICE_CARE:
            return self.listProductDeviceCare[indexPath.row]
            
        case .NUTRITION:
            return self.listProductNutrition[indexPath.row]
            
        case .ACCESSORY:
            return self.listProductAccessory[indexPath.row]
            
        default:
            return self.listProductSuggest[indexPath.row]
        }
    }
    
    func didSelectItem_Product(at indexPath: IndexPath, type: TypeCategoryStore?) {
        switch type {
        case .DEVICE_FIT:
            let product = self.listProductDeviceFit[indexPath.row]
            guard let id = product.id else {
                return
            }
            self.view?.showHud()
            self.interactor.getProductDetail(id: id)
            
        case .DEVICE_CARE:
            let product = self.listProductDeviceCare[indexPath.row]
            guard let id = product.id else {
                return
            }
            self.view?.showHud()
            self.interactor.getProductDetail(id: id)
            
        case .NUTRITION:
            let product = self.listProductNutrition[indexPath.row]
            guard let id = product.id else {
                return
            }
            self.view?.showHud()
            self.interactor.getProductDetail(id: id)
            
        case .ACCESSORY:
            let product = self.listProductAccessory[indexPath.row]
            guard let id = product.id else {
                return
            }
            self.view?.showHud()
            self.interactor.getProductDetail(id: id)
            
        default:
            let product = self.listProductSuggest[indexPath.row]
            guard let id = product.id else {
                return
            }
            self.view?.showHud()
            self.interactor.getProductDetail(id: id)
        }
    }
    
    func didSelectFlashSale(at indexPath: IndexPath) {
        if let product = self.flashSaleProducts.first?.products?[indexPath.row] {
            guard let id = product.id else {
                return
            }
            self.view?.showHud()
            self.interactor.getProductDetail(id: id)
        }
    }
    
    // Slide Show
    func getItemBanner(at page: Int) -> ItemBanner? {
        return self.sliderBanner?.items?[page]
    }
    
    func numberOfItems_FlashSale() -> Int {
        return self.flashSaleProducts.first?.products?.count ?? 0
    }
    
    func onDidSelectBanner(productId: Int) {
        self.view?.showHud()
        self.interactor.getProductDetail(id: productId)
    }
    
    func cellForItem_FlashSale(at indexPath: IndexPath) -> Product? {
        return self.flashSaleProducts.first?.products?[indexPath.row]
    }
    
    func onGetflashSaleProducts() -> [FlashSaleModel]? {
        return self.flashSaleProducts
    }
}

// MARK: - HomeStore InteractorOutputProtocol
extension HomeStoreViewModel: HomeStoreInteractorOutputProtocol {
    func onGetSliderBannerFinished(with result: Result<SliderBannerModel, APIError>) {
        switch result {
        case .success(let model):
            self.sliderBanner = model
            self.view?.setUISliderBanner(sliderBanner: self.sliderBanner)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>) {
        switch result {
        case .success(let model):
            self.listProductSuggest = model
            self.view?.reloadData_Suggest()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetListProductFeaturedFinished(with result: Result<[ProductModel], APIError>, type: TypeCategoryStore) {
        switch result {
        case .success(let model):
            switch type {
            case .DEVICE_FIT:
                self.listProductDeviceFit = model
                self.view?.reloadData_DeviceFit()
                
            case .DEVICE_CARE:
                self.listProductDeviceCare = model
                self.view?.reloadData_DeviceCare()
                
            case .NUTRITION:
                self.listProductNutrition = model
                self.view?.reloadData_Nutrition()
                
            case .ACCESSORY:
                self.listProductAccessory = model
                self.view?.reloadData_Accessory()
            }
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onDidSelectItem(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetListCountriesFinished(with result: Result<[CountryModel], APIError>) {
        switch result {
        case .success(let model):
            gListCountryModel = model
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetFlashSaleProductsFinished(with result: Result<[FlashSaleModel], APIError>) {
        switch result {
        case .success(let model):
            self.flashSaleProducts = model
//            self.view?.reloadData_FlashSale()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
