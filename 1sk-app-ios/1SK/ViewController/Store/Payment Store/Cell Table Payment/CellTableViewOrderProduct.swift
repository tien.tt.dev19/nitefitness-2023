//
//  CellTableViewOrderProduct.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit

class CellTableViewOrderProduct: UITableViewCell {
    
    @IBOutlet weak var img_Image: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_PriceOld: UILabel!
    @IBOutlet weak var lbl_Amount: UILabel!
    
    @IBOutlet weak var view_LineBottom: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(product: CartProductSchema?) {
        self.lbl_Title.text = product?.name
        self.lbl_Price.text = "\(product?.price?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.text = "\(product?.pricerOld?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.strikeThrough(true)
        self.lbl_Amount.text = product?.amount?.asString ?? "0"
        self.img_Image.setImage(with: product?.image ?? "", completion: nil)
        
        if product?.isSalePrice == true {
            self.lbl_PriceOld.isHidden = false
        } else {
            self.lbl_PriceOld.isHidden = true
        }
    }
    
    func config(item: ItemOrder?) {
        self.lbl_Title.text = item?.productName
        self.lbl_Price.text = "\(item?.price?.formatNumber() ?? "0")đ"
        
        self.lbl_PriceOld.attributedText = NSAttributedString.strikeThroughStyle(with: "\(item?.product?.priceWithTaxes?.formatNumber() ?? "0")đ", strikeColor: self.lbl_PriceOld.textColor)
        
        self.lbl_Amount.text = item?.quantity?.asString ?? "0"
        self.img_Image.setImage(with: item?.productImage ?? "", completion: nil)
        
        if item?.product?.isSalePrice == true {
            self.lbl_PriceOld.isHidden = false
        } else {
            self.lbl_PriceOld.isHidden = true
        }
    }
}
