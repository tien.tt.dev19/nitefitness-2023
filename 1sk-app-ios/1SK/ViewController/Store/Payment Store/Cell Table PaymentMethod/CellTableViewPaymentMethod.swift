//
//  CellTableViewPaymentMethod.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//

import UIKit

class CellTableViewPaymentMethod: UITableViewCell {

    @IBOutlet weak var lbl_OrdersCode: UILabel!
    @IBOutlet weak var lbl_BankAccountNumber: UILabel!
    @IBOutlet weak var lbl_BankAccountName: UILabel!
    @IBOutlet weak var lbl_BankName: UILabel!
    
    var bank: BankStoreModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(bank: BankStoreModel?) {
        self.bank = bank
        
        self.lbl_OrdersCode.text = bank?.codeOrder?.code
        self.lbl_BankAccountNumber.text = bank?.account
        self.lbl_BankAccountName.text = "Tên thụ hưởng: \(bank?.userReceive ?? "Không có")"
        self.lbl_BankName.text = "Ngân hàng: \(bank?.nameBank ?? "") - \(bank?.nameBankShort ?? "") - \(bank?.branch ?? "")"
    }
    
    @IBAction func onCopyOrdersCodeAction(_ sender: Any) {
        if let transferContent = self.bank?.codeOrder?.code {
            let pasteboard = UIPasteboard.general
            pasteboard.string = "\(transferContent)"
            SKToast.shared.showToast(content: "Đã sao chép mã đơn hàng: \(pasteboard.string ?? "") \n\nMở app NGÂN HÀNG và dán vào mục nội dung chuyển khoản để tiếp tục")
        }
    }
    
    @IBAction func onCopyBankAccountNumberAction(_ sender: Any) {
        if let bankAccountNumber = self.bank?.account {
            let pasteboard = UIPasteboard.general
            pasteboard.string = bankAccountNumber.replaceCharacter(target: " ", withString: "")
            SKToast.shared.showToast(content: "Đã sao chép số tài khoản: \(pasteboard.string ?? "") \n\nMở app NGÂN HÀNG và dán vào mục nhập số tài khoản để tiếp tục")
        }
    }
}
