//
//  CellTableViewVoucher.swift
//  1SK
//
//  Created by Thaad on 17/03/2022.
//

import UIKit

class CellTableViewVoucher: UITableViewCell {

    @IBOutlet weak var lbl_ValueError: UILabel!
    
    @IBOutlet weak var view_Success: UIStackView!
    @IBOutlet weak var lbl_ValueVoucher: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(value: String?, status: Bool?) {
        if status == true {
            let valueInt = Int(value ?? "0")
            self.lbl_ValueVoucher.text = "\(valueInt?.formatNumber() ?? "0")đ"
            self.lbl_ValueError.isHidden = true
            self.view_Success.isHidden = false
            
        } else {
            self.lbl_ValueError.text = value ?? ""
            self.lbl_ValueError.isHidden = false
            self.view_Success.isHidden = true
        }
    }
}
