//
//  
//  PaymentSuccessInteractorInput.swift
//  1SK
//
//  Created by Thaad on 21/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol PaymentSuccessInteractorInputProtocol {
    func getListProductSuggest(page: Int, perPage: Int)
    func getProductDetail(id: Int)
    func getOrderDetail(orderId: Int)
}

// MARK: - Interactor Output Protocol
protocol PaymentSuccessInteractorOutputProtocol: AnyObject {
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
    func onGetOrderDetailFinished(with result: Result<OrderModel, APIError>)
}

// MARK: - PaymentSuccess InteractorInput
class PaymentSuccessInteractorInput {
    weak var output: PaymentSuccessInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - PaymentSuccess InteractorInputProtocol
extension PaymentSuccessInteractorInput: PaymentSuccessInteractorInputProtocol {
    func getListProductSuggest(page: Int, perPage: Int) {
        self.storeService?.getListProductSuggest(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductSuggestFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getOrderDetail(orderId: Int) {
        self.storeService?.getOrderDetail(orderId: orderId, completion: { [weak self] result in
            self?.output?.onGetOrderDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
