//
//  
//  PaymentSuccessRouter.swift
//  1SK
//
//  Created by Thaad on 21/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol PaymentSuccessRouterProtocol {
    func showProductStore(product: ProductModel?)
    func showOrderDetail(order: OrderModel?, delegate: OrderDetailViewDelegate?)
    func onPopToRootViewController()
}

// MARK: - PaymentSuccess Router
class PaymentSuccessRouter {
    weak var viewController: PaymentSuccessViewController?
    
    static func setupModule(order: OrderModel?) -> PaymentSuccessViewController {
        let viewController = PaymentSuccessViewController()
        let router = PaymentSuccessRouter()
        let interactorInput = PaymentSuccessInteractorInput()
        let viewModel = PaymentSuccessViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.order = order
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - PaymentSuccess RouterProtocol
extension PaymentSuccessRouter: PaymentSuccessRouterProtocol {
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showOrderDetail(order: OrderModel?, delegate: OrderDetailViewDelegate?) {
        let controller = OrderDetailRouter.setupModule(order: order, delegate: delegate, isCreateOrder: true)
        self.viewController?.navigationController?.show(controller, sender: nil)
    }
    
    func onPopToRootViewController() {
        self.viewController?.navigationController?.popToRootViewController(animated: true)
    }
}
