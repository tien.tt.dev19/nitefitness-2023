//
//  
//  PaymentSuccessViewController.swift
//  1SK
//
//  Created by Thaad on 21/03/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol PaymentSuccessViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func reloadData_Suggest()
    func onDidSelectItem(product: ProductModel)
    
    func onGetOrderDetailSuccess(order: OrderModel?)
}

// MARK: - PaymentSuccess ViewController
class PaymentSuccessViewController: BaseViewController {
    var router: PaymentSuccessRouterProtocol!
    var viewModel: PaymentSuccessViewModelProtocol!
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var coll_Suggest: UICollectionView!
    @IBOutlet weak var constraint_height_Suggest: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Init
    private func setupInit() {
        //self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.setInitUICollectionView()
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.onPopToRootViewController()
    }
    
    @IBAction func onOrderDetailAction(_ sender: Any) {
        self.viewModel.onOrderDetailAction()
    }
}

// MARK: - PaymentSuccess ViewProtocol
extension PaymentSuccessViewController: PaymentSuccessViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func reloadData_Suggest() {
        self.coll_Suggest.reloadData()
        
        let count = self.viewModel.numberOfItems_Suggest()
        let divisible = count%2
        if divisible == 0 && count > 2 {
            self.constraint_height_Suggest.constant = CGFloat(((count/2) * 220) + 16)
            
        } else {
            if count <= 2 {
                self.constraint_height_Suggest.constant = CGFloat(220 + 10)
                
            } else {
                let row = count + 1
                self.constraint_height_Suggest.constant = CGFloat(((row/2) * 220) + 16)
            }
        }
    }
    
    func onDidSelectItem(product: ProductModel) {
        self.router.showProductStore(product: product)
    }
    
    func onGetOrderDetailSuccess(order: OrderModel?) {
        self.router.showOrderDetail(order: order, delegate: self)
    }
}

// MARK: UICollectionViewDataSource
extension PaymentSuccessViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_Suggest.registerNib(ofType: CellCollectionViewProduct.self)
        
        self.coll_Suggest.dataSource = self
        self.coll_Suggest.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.itemSize = CGSize(width: width, height: height)
        self.coll_Suggest.collectionViewLayout = layout
        
        self.constraint_height_Suggest.constant = 0 //(220 * 5) + 16
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.numberOfItems_Suggest()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
        cell.config(product: self.viewModel.cellForItem_Suggest(at: indexPath))
        return cell
    }
}

//// MARK: - UICollectionViewDelegateFlowLayout
//extension PaymentSuccessViewController: UICollectionViewDelegateFlowLayout {
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let widthArea = Constant.Screen.width
//        let width = (widthArea - 48) / 2
//        let height = width * (210/164)
//        
//        return CGSize(width: width, height: height)
//    }
//}

// MARK: UICollectionViewDelegate
extension PaymentSuccessViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.didSelectItem_Suggest(at: indexPath)
    }
}

// MARK: OrderDetailViewDelegate
extension PaymentSuccessViewController: OrderDetailViewDelegate {
    func onCancelOrderSuccess(order: OrderModel?) {
        //
    }
}
