//
//  
//  PaymentSuccessViewModel.swift
//  1SK
//
//  Created by Thaad on 21/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol PaymentSuccessViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onOrderDetailAction()
    
    // CollectionView
    func numberOfItems_Suggest() -> Int
    func cellForItem_Suggest(at indexPath: IndexPath) -> ProductModel
    func didSelectItem_Suggest(at indexPath: IndexPath)
}

// MARK: - PaymentSuccess ViewModel
class PaymentSuccessViewModel {
    weak var view: PaymentSuccessViewProtocol?
    private var interactor: PaymentSuccessInteractorInputProtocol

    init(interactor: PaymentSuccessInteractorInputProtocol) {
        self.interactor = interactor
    }

    var order: OrderModel?
    var listProductSuggest: [ProductModel] = []
    
    func getDataProductSuggest() {
        self.view?.showHud()
        self.interactor.getListProductSuggest(page: 1, perPage: 6)
    }
}

// MARK: - PaymentSuccess ViewModelProtocol
extension PaymentSuccessViewModel: PaymentSuccessViewModelProtocol {
    func onViewDidLoad() {
        self.getDataProductSuggest()
    }
    
    // Action
    func onOrderDetailAction() {
        guard let orderId = self.order?.id else {
            return
        }
        self.view?.showHud()
        self.interactor.getOrderDetail(orderId: orderId)
    }
    
    // Coll Suggest
    func numberOfItems_Suggest() -> Int {
        return self.listProductSuggest.count
    }
    
    func cellForItem_Suggest(at indexPath: IndexPath) -> ProductModel {
        return self.listProductSuggest[indexPath.row]
    }
    
    func didSelectItem_Suggest(at indexPath: IndexPath) {
        let product = self.listProductSuggest[indexPath.row]
        guard let id = product.id else {
            return
        }
        self.view?.showHud()
        self.interactor.getProductDetail(id: id)
    }
}

// MARK: - PaymentSuccess InteractorOutputProtocol
extension PaymentSuccessViewModel: PaymentSuccessInteractorOutputProtocol {
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>) {
        switch result {
        case .success(let model):
            self.listProductSuggest = model
            self.view?.reloadData_Suggest()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onDidSelectItem(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetOrderDetailFinished(with result: Result<OrderModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onGetOrderDetailSuccess(order: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
