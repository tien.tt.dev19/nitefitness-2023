//
//  
//  PaymentStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol PaymentStoreInteractorInputProtocol {
    func getBankStoreInfo()
    func setCreateOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], orderCode: String, paymentMethod: String?, couponCode: String?, desc: String?)
    func setVoucherOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], paymentMethod: String?, couponCode: String?, desc: String?)
}

// MARK: - Interactor Output Protocol
protocol PaymentStoreInteractorOutputProtocol: AnyObject {
    func onGetBankStoreInfoFinished(with result: Result<[BankStoreModel], APIError>)
    func onSetCreateOrderFinished(with result: Result<OrderModel, APIError>)
    func onSetVoucherOrderFinished(with result: Result<OrderModel, APIError>, meta: Meta?)
}

// MARK: - PaymentStore InteractorInput
class PaymentStoreInteractorInput {
    weak var output: PaymentStoreInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - PaymentStore InteractorInputProtocol
extension PaymentStoreInteractorInput: PaymentStoreInteractorInputProtocol {
    func getBankStoreInfo() {
        self.storeService?.getBankStoreInfo(completion: { [weak self] result in
            self?.output?.onGetBankStoreInfoFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setCreateOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], orderCode: String, paymentMethod: String?, couponCode: String?, desc: String?) {
        self.storeService?.setCreateOrder(delivery: delivery, listProduct: listProduct, orderCode: orderCode, paymentMethod: paymentMethod, couponCode: couponCode, desc: desc, completion: { [weak self] result in
            self?.output?.onSetCreateOrderFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setVoucherOrder(delivery: DeliveryModel, listProduct: [CartProductSchema], paymentMethod: String?, couponCode: String?, desc: String?) {
        self.storeService?.setVoucherOrder(delivery: delivery, listProduct: listProduct, paymentMethod: paymentMethod, couponCode: couponCode, desc: desc, completion: { [weak self] result in
            let meta = result.getMeta()
            self?.output?.onSetVoucherOrderFinished(with: result.unwrapSuccessModel(), meta: meta)
        })
    }
}
