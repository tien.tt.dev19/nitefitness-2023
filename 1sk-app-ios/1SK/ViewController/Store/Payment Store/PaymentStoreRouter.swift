//
//  
//  PaymentStoreRouter.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol PaymentStoreRouterProtocol {
    func showDeliveryInformationStore(delivery: DeliveryModel?, delegate: DeliveryInformationViewDelegate?)
    func showPaymentStoreSuccess(order: OrderModel?)
    func onPopViewController()
}

// MARK: - PaymentStore Router
class PaymentStoreRouter {
    weak var viewController: PaymentStoreViewController?
    static func setupModule(listProduct: [CartProductSchema], delivery: DeliveryModel?) -> PaymentStoreViewController {
        let viewController = PaymentStoreViewController()
        let router = PaymentStoreRouter()
        let interactorInput = PaymentStoreInteractorInput()
        let viewModel = PaymentStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.listProduct = listProduct
        viewModel.delivery = delivery
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - PaymentStore RouterProtocol
extension PaymentStoreRouter: PaymentStoreRouterProtocol {
    func showDeliveryInformationStore(delivery: DeliveryModel?, delegate: DeliveryInformationViewDelegate?) {
        let controller = DeliveryInformationRouter.setupModule(state: .Update, delivery: delivery, delegate: delegate)
        self.viewController?.show(controller, sender: nil)
    }
    
    func showPaymentStoreSuccess(order: OrderModel?) {
        let controller = PaymentSuccessRouter.setupModule(order: order)
        self.viewController?.show(controller, sender: nil)
    }
    
    func onPopViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
}
