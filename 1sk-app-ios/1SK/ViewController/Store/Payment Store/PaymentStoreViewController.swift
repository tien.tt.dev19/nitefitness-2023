//
//  
//  PaymentStoreViewController.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol PaymentStoreViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func reloadData_Cart()
    func setUIDataPayment()
    func setUITotalPrice(order: OrderModel?)
    func setUIDelivery(delivery: DeliveryModel?)
    func setUIVoucher()
    
    func onPaymentStoreSuccess(order: OrderModel?)
}

// MARK: - PaymentStore ViewController
class PaymentStoreViewController: BaseViewController {
    var router: PaymentStoreRouterProtocol!
    var viewModel: PaymentStoreViewModelProtocol!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var lbl_TotalPrice: UILabel!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setUITotalPrice(order: nil)
    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.onPopViewController()
    }
    
    @IBAction func onCreateOrderAction(_ sender: Any) {
        self.viewModel.onCreateOrderAction()
    }
}

// MARK: - PaymentStore ViewProtocol
extension PaymentStoreViewController: PaymentStoreViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func reloadData_Cart() {
        self.tbv_TableView.reloadData()
        self.setUITotalPrice(order: nil)
    }
    
    func setUIDataPayment() {
        let indexSet = IndexSet(integer: 1)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
    
    func setUITotalPrice(order: OrderModel?) {
        guard let amount = order?.amount else {
            var totalPrice = 0
            for item in self.viewModel.getListProduct() {
                if let amount = item.amount, let price = item.price, item.isSelected == true {
                    totalPrice += amount * price
                }
            }
            self.lbl_TotalPrice.text = "\(totalPrice.formatNumber())đ"
            return
        }
        
        self.lbl_TotalPrice.text = "\(amount.formatNumber())đ"
    }
    
    func setUIDelivery(delivery: DeliveryModel?) {
        let indexSet = IndexSet(integer: 0)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
    
    func setUIVoucher() {
        self.tbv_TableView.reloadRows(at: [IndexPath(row: 0, section: 2)], with: .automatic)
    }
    
    func onPaymentStoreSuccess(order: OrderModel?) {
        self.router.showPaymentStoreSuccess(order: order)
    }
}

// MARK: UITableViewDataSource
extension PaymentStoreViewController: UITableViewDataSource, UITableViewDelegate {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderPaymentAddress.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderPaymentMethod.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderPaymentVoucher.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderPaymentNote.self)
        
        self.tbv_TableView.registerNib(ofType: CellTableViewOrderProduct.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewPaymentMethod.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewVoucher.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
        
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderPaymentAddress.self)
            header.delegate = self
            header.config(delivery: self.viewModel.getDeliveryAddress())
            return header
            
        case 1:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderPaymentMethod.self)
            header.delegate = self
            header.config(paymentMethod: self.viewModel.getPaymentMethod())
            return header
            
        case 2:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderPaymentVoucher.self)
            header.delegate = self
            return header
            
        case 3:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderPaymentNote.self)
            header.delegate = self
            header.config(text: self.viewModel.getNoteOrder())
            return header
            
        default:
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return self.viewModel.numberOfRows_Table()
            
        case 1:
            switch self.viewModel.getPaymentMethod() {
            case .PaymentOnDelivery:
                return 0
                
            case .PaymentTransfer:
                return 1
            }
            
        case 2:
            return 1
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 2:
            if self.viewModel.getVoucherValue() != nil {
                return 16
                
            } else {
                return 0
            }
            
        default:
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueCell(ofType: CellTableViewOrderProduct.self, for: indexPath)
            cell.config(product: self.viewModel.cellForRow_Table(at: indexPath))
            
            let count = self.viewModel.numberOfRows_Table() - 1
            if indexPath.row == count {
                cell.view_LineBottom.isHidden = true
            } else {
                cell.view_LineBottom.isHidden = false
            }
            return cell
            
        case 1:
            let cell = tableView.dequeueCell(ofType: CellTableViewPaymentMethod.self, for: indexPath)
            cell.config(bank: self.viewModel.getBankStore())
            return cell
        
        case 2:
            let cell = tableView.dequeueCell(ofType: CellTableViewVoucher.self, for: indexPath)
            let value = self.viewModel.getVoucherValue()
            let status = self.viewModel.getVoucherStatus()
            cell.config(value: value, status: status)
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
}

// MARK: SectionHeaderPaymentAddressDelegate
extension PaymentStoreViewController: SectionHeaderPaymentAddressDelegate, DeliveryInformationViewDelegate {
    func onUpdateDeliverAction() {
        let delivery = self.viewModel.getDeliveryAddress()
        self.router.showDeliveryInformationStore(delivery: delivery, delegate: self)
    }
    
    func onCreateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        // not use
    }
    
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        self.setUIDelivery(delivery: delivery)
    }
}

// MARK: SectionHeaderPaymentMethodDelegate
extension PaymentStoreViewController: SectionHeaderPaymentMethodDelegate {
    func onDidChangePaymentMethod(paymentMethod: PaymentMethod) {
        self.viewModel.onDidChangePaymentMethod(paymentMethod: paymentMethod)
        
        let indexSet = IndexSet(integer: 1)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
}

// MARK: SectionHeaderPaymentVoucherDelegate
extension PaymentStoreViewController: SectionHeaderPaymentVoucherDelegate {
    func onConfirmCouponCodeAction(code: String) {
        self.viewModel.onConfirmCouponCodeAction(code: code)
    }
    
    func onVoucherDidChangeSelection() {
        self.viewModel.onVoucherDidChangeSelection()
        
        self.setUIVoucher()
        self.setUITotalPrice(order: nil)
    }
}

// MARK: SectionHeaderPaymentNoteDelegate
extension PaymentStoreViewController: SectionHeaderPaymentNoteDelegate {
    func onNoteDidChange(text: String?) {
        self.viewModel.onNoteDidChange(text: text)
    }
}
