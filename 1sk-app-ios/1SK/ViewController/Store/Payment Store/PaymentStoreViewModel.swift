//
//  
//  PaymentStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol PaymentStoreViewModelProtocol {
    func onViewDidLoad()
    
    // Action
    func onCreateOrderAction()
    func onConfirmCouponCodeAction(code: String)
    func onDidChangePaymentMethod(paymentMethod: PaymentMethod)
    func onNoteDidChange(text: String?)
    
    // TableView
    func numberOfRows_Table() -> Int
    func cellForRow_Table(at indexPath: IndexPath) -> CartProductSchema?
    
    // Data
    func getListProduct() -> [CartProductSchema]
    func getDeliveryAddress() -> DeliveryModel?
    func getBankStore() -> BankStoreModel?
    func getPaymentMethod() -> PaymentMethod
    func getNoteOrder() -> String?
    func getVoucherValue() -> String?
    func getVoucherStatus() -> Bool?
    
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?)
    func onVoucherDidChangeSelection()
}

// MARK: - PaymentStore ViewModel
class PaymentStoreViewModel {
    weak var view: PaymentStoreViewProtocol?
    private var interactor: PaymentStoreInteractorInputProtocol

    init(interactor: PaymentStoreInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listProduct: [CartProductSchema] = []
    var delivery: DeliveryModel?
    var bankStore: BankStoreModel?
    var paymentMethod: PaymentMethod = .PaymentOnDelivery
    var noteOrder: String?
    
    var voucherCouponCode: String?
    var voucherValue: String?
    var voucherStatus: Bool?
    
    func setInitDataBank() {
        self.view?.showHud()
        self.interactor.getBankStoreInfo()
    }
    
    func setRemoveProductInCart() {
        gListCartProduct.removeAll {$0.isSelected == true }
        Shared.instance.cartProductSchema = gListCartProduct
    }
}

// MARK: - PaymentStore ViewModelProtocol
extension PaymentStoreViewModel: PaymentStoreViewModelProtocol {
    func onViewDidLoad() {
        self.setInitDataBank()
    }
    
    // Action
    func onCreateOrderAction() {
        guard let orderCode = self.bankStore?.codeOrder?.code, orderCode.count > 0 else {
            SKToast.shared.showToast(content: "Lỗi tạo đơn hàng")
            return
        }
        
        var couponCode = ""
        if self.voucherStatus == true {
            couponCode = self.voucherCouponCode ?? ""
        }
        
        self.view?.showHud()
        self.interactor.setCreateOrder(delivery: self.delivery!, listProduct: self.listProduct, orderCode: orderCode, paymentMethod: self.paymentMethod.rawValue, couponCode: couponCode, desc: self.noteOrder)
    }
    
    func onConfirmCouponCodeAction(code: String) {
        self.voucherCouponCode = code
        
        self.view?.showHud()
        self.interactor.setVoucherOrder(delivery: self.delivery!, listProduct: self.listProduct, paymentMethod: self.paymentMethod.rawValue, couponCode: code, desc: self.noteOrder)
    }
    
    func onDidChangePaymentMethod(paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
    }
    
    func onNoteDidChange(text: String?) {
        self.noteOrder = text
    }
    
    // TableView
    func numberOfRows_Table() -> Int {
        return self.listProduct.count
    }
    
    func cellForRow_Table(at indexPath: IndexPath) -> CartProductSchema? {
        return self.listProduct[indexPath.row]
    }
    
    // Data
    func getListProduct() -> [CartProductSchema] {
        return self.listProduct
    }
    
    func getDeliveryAddress() -> DeliveryModel? {
        return self.delivery
    }
    
    func getBankStore() -> BankStoreModel? {
        return self.bankStore
    }
    
    func getPaymentMethod() -> PaymentMethod {
        return self.paymentMethod
    }
    
    func getNoteOrder() -> String? {
        return self.noteOrder
    }
    
    func getVoucherValue() -> String? {
        return self.voucherValue
    }
    
    func getVoucherStatus() -> Bool? {
        return self.voucherStatus
    }
    
    func onUpdateDeliveryAddressSuccess(delivery: DeliveryModel?) {
        self.delivery = delivery
    }
    
    func onVoucherDidChangeSelection() {
        self.voucherCouponCode = nil
        self.voucherValue = nil
        self.voucherStatus = nil
    }
}

// MARK: - PaymentStore InteractorOutputProtocol
extension PaymentStoreViewModel: PaymentStoreInteractorOutputProtocol {
    func onGetBankStoreInfoFinished(with result: Result<[BankStoreModel], APIError>) {
        switch result {
        case .success(let model):
            self.bankStore = model.first
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onSetCreateOrderFinished(with result: Result<OrderModel, APIError>) {
        switch result {
        case .success(let model):
            self.setRemoveProductInCart()
            self.view?.onPaymentStoreSuccess(order: model)
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onSetVoucherOrderFinished(with result: Result<OrderModel, APIError>, meta: Meta?) {
        switch result {
        case .success(let model):
            self.voucherValue = model.discountAmount?.asString
            self.voucherStatus = true
            
            self.view?.setUITotalPrice(order: model)
            self.view?.setUIVoucher()
            
        case .failure(let error):
            debugPrint(error)
            
            self.voucherValue = error.message
            self.voucherStatus = false
            self.view?.setUIVoucher()
        }
        
        self.view?.hideHud()
    }
}
