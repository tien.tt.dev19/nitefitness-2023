//
//  SectionHeaderPaymentAddress.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit

protocol SectionHeaderPaymentAddressDelegate: AnyObject {
    func onUpdateDeliverAction()
}

class SectionHeaderPaymentAddress: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Phone: UILabel!
    @IBOutlet weak var lbl_Address: UILabel!
    
    weak var delegate: SectionHeaderPaymentAddressDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func onUpdateDeliverAction(_ sender: Any) {
        self.delegate?.onUpdateDeliverAction()
    }
    
    func config(delivery: DeliveryModel?) {
        self.lbl_Name.text = delivery?.name
        self.lbl_Phone.text = delivery?.phone
        self.lbl_Address.text = delivery?.address
        if let name = delivery?.district?.name {
            self.lbl_Address.text = "\(self.lbl_Address.text ?? ""), \(name)"
        }
        if let name = delivery?.city?.name {
            self.lbl_Address.text = "\(self.lbl_Address.text ?? ""), \(name)"
        }
        if let name = delivery?.state?.name {
            self.lbl_Address.text = "\(self.lbl_Address.text ?? ""), \(name)"
        }
    }
}
