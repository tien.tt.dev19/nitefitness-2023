//
//  SectionHeaderPaymentMethod.swift
//  1SK
//
//  Created by Thaad on 16/03/2022.
//

import UIKit

protocol SectionHeaderPaymentMethodDelegate: AnyObject {
    func onDidChangePaymentMethod(paymentMethod: PaymentMethod)
}

class SectionHeaderPaymentMethod: UITableViewHeaderFooterView {

    @IBOutlet weak var btn_PaymentOnDelivery: UIButton!
    @IBOutlet weak var btn_PaymentTransfer: UIButton!
    
    weak var delegate: SectionHeaderPaymentMethodDelegate?
    private var paymentMethod: PaymentMethod?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    func config(paymentMethod: PaymentMethod) {
        self.paymentMethod = paymentMethod
        
        switch self.paymentMethod {
        case .PaymentOnDelivery:
            self.btn_PaymentOnDelivery.isSelected = true
            self.btn_PaymentTransfer.isSelected = false
            
        case .PaymentTransfer:
            self.btn_PaymentOnDelivery.isSelected = false
            self.btn_PaymentTransfer.isSelected = true
            
        default:
            break
        }
    }

    @IBAction func onPaymentOnDeliveryAction(_ sender: Any) {
        self.btn_PaymentOnDelivery.isSelected = true
        self.btn_PaymentTransfer.isSelected = false
        
        self.delegate?.onDidChangePaymentMethod(paymentMethod: .PaymentOnDelivery)
    }
    
    @IBAction func onPaymentTransferAction(_ sender: Any) {
        self.btn_PaymentOnDelivery.isSelected = false
        self.btn_PaymentTransfer.isSelected = true
        
        self.delegate?.onDidChangePaymentMethod(paymentMethod: .PaymentTransfer)
    }
}
