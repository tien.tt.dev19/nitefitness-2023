//
//  SectionHeaderPaymentNote.swift
//  1SK
//
//  Created by Thaad on 16/03/2022.
//

import UIKit
import GrowingTextView

protocol SectionHeaderPaymentNoteDelegate: AnyObject {
    func onNoteDidChange(text: String?)
}

class SectionHeaderPaymentNote: UITableViewHeaderFooterView {

    @IBOutlet weak var tv_Note: UITextView!
    
    weak var delegate: SectionHeaderPaymentNoteDelegate?
    let placeholderTextView = "Tin nhắn, lưu ý cho 1SK..."
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitUITextView()
    }

    func config(text: String?) {
        self.tv_Note.text = text
    }
}

// MARK: - UITextViewDelegate
extension SectionHeaderPaymentNote: UITextViewDelegate {
    func setInitUITextView() {
        self.tv_Note.delegate = self
        self.tv_Note.placeholder = self.placeholderTextView
        
//        self.tv_Note.maxLength = 140
//        self.tv_Note.minHeight = 90.0
//        self.tv_Note.maxHeight = 300.0
//        self.tv_Note.backgroundColor = .white
//        self.tv_Note.layer.cornerRadius = 4.0
//        self.tv_Note.trimWhiteSpaceWhenEndEditing = false
//        self.tv_Note.placeholder = self.placeholderTextView
//        self.tv_Note.placeholderColor = R.color.subTitle() ?? UIColor(white: 0.8, alpha: 1.0)
//        self.tv_Note.textColor = R.color.darkText()
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("textViewDidChange textView:", textView.text ?? "null")
        guard textView.text.count <= 250 else {
            SKToast.shared.showToast(content: "Ghi chú của bạn nhiều quá quy định (tối đã: 250 ký tự)")
            self.endEditing(true)
            return
        }
        
        if textView.text != "" && textView.text != self.placeholderTextView {
            self.delegate?.onNoteDidChange(text: textView.text)
            
        } else {
            self.delegate?.onNoteDidChange(text: "")
        }
    }
    
}
