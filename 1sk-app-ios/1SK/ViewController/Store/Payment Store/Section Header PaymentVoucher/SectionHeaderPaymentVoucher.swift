//
//  SectionHeaderPaymentVoucher.swift
//  1SK
//
//  Created by Thaad on 16/03/2022.
//

import UIKit

protocol SectionHeaderPaymentVoucherDelegate: AnyObject {
    func onConfirmCouponCodeAction(code: String)
    func onVoucherDidChangeSelection()
}

class SectionHeaderPaymentVoucher: UITableViewHeaderFooterView {

    @IBOutlet weak var tf_CouponCode: UITextField!
    
    weak var delegate: SectionHeaderPaymentVoucherDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitUITextField()
    }

    @IBAction func onConfirmCouponCodeAction(_ sender: Any) {
        self.endEditing(true)
        
        guard let code = self.tf_CouponCode.text, code.count > 0 else {
            SKToast.shared.showToast(content: "Vui lòng nhập mã giảm giá để tiếp tục")
            return
        }
        guard code.containsSpecialCharacter == false else {
            SKToast.shared.showToast(content: "Mã của bạn không đúng, vui lòng nhập lại để tiếp tục")
            return
        }
        
        self.delegate?.onConfirmCouponCodeAction(code: code)
    }
}

// MARK: UITextFieldDelegate
extension SectionHeaderPaymentVoucher: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_CouponCode.delegate = self
        //self.tf_CouponCode.addTarget(self, action: #selector(self.textFieldDidChangeSelection), for: .editingChanged)
    }
    
//    @objc func textFieldDidChangeSelection() {
//        self.delegate?.onVoucherDidChangeSelection()
//    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        self.delegate?.onVoucherDidChangeSelection()
    }
}
