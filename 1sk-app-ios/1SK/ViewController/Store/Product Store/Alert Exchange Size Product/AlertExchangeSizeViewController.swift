//
//  AlertExchangeSizeViewController.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//

import UIKit

class AlertExchangeSizeViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var img_ExchangeSize: UIImageView!
    
    var product: ProductModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.img_ExchangeSize.setImageWith(imageUrl: self.product?.imageChooseSize ?? "")
        self.setInitGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
}

// MARK: - Init Animation
extension AlertExchangeSizeViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertExchangeSizeViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.hiddenAnimate()
    }
}
