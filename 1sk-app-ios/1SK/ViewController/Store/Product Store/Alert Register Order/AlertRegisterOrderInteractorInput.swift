//
//  
//  AlertRegisterOrderInteractorInput.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol AlertRegisterOrderInteractorInputProtocol {
    func post_RegisterProduct(name: String, phone: String, email: String, productID: String)
}

// MARK: - Interactor Output Protocol
protocol AlertRegisterOrderInteractorOutputProtocol: AnyObject {
    func onPost_RegisterProduct_Finished(with result: Result<EmptyModel, APIError>)

}

// MARK: - AlertRegisterOrder InteractorInput
class AlertRegisterOrderInteractorInput {
    weak var output: AlertRegisterOrderInteractorOutputProtocol?
    var storeService: StoreServiceProtocol!
}

// MARK: - AlertRegisterOrder InteractorInputProtocol
extension AlertRegisterOrderInteractorInput: AlertRegisterOrderInteractorInputProtocol {
    func post_RegisterProduct(name: String, phone: String, email: String, productID: String) {
        self.storeService.postRegisterProduct(name: name, phone: phone, email: email, productId: productID) { [weak self] result in
            self?.output?.onPost_RegisterProduct_Finished(with: result.unwrapSuccessModel())
        }
    }
}
