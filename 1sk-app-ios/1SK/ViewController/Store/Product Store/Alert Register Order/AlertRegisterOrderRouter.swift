//
//  
//  AlertRegisterOrderRouter.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol AlertRegisterOrderRouterProtocol {

}

// MARK: - AlertRegisterOrder Router
class AlertRegisterOrderRouter {
    weak var viewController: AlertRegisterOrderViewController?
    
    static func setupModule() -> AlertRegisterOrderViewController {
        let viewController = AlertRegisterOrderViewController()
        let router = AlertRegisterOrderRouter()
        let interactorInput = AlertRegisterOrderInteractorInput()
        let viewModel = AlertRegisterOrderViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - AlertRegisterOrder RouterProtocol
extension AlertRegisterOrderRouter: AlertRegisterOrderRouterProtocol {
    
}
