//
//  
//  AlertRegisterOrderViewController.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

protocol AlertRegisterOrderViewDelegate: AnyObject {
    func onDidRegisterOrder()
}

struct OrderRegisterParam {
    var name: String
    var phone: String
    var email: String
    
    func toDictionary() -> [String: Any] {
        var json: [String: Any] = [:]
        json["full_name"] = self.name
        
        if !String.isNilOrEmpty(self.phone) {
            json["phone_number"] = self.phone
        }
        
        if !String.isNilOrEmpty(self.email) {
            json["email"] = self.email
        }
        
        return json
    }
}

// MARK: - ViewProtocol
protocol AlertRegisterOrderViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    func onPresentAlertConfirmSuccessVC()

    //UITableView
    //func onReloadData()
}

// MARK: - AlertRegisterOrder ViewController
class AlertRegisterOrderViewController: BaseViewController {
    var router: AlertRegisterOrderRouterProtocol!
    var viewModel: AlertRegisterOrderViewModelProtocol!
    weak var delegate: AlertRegisterOrderViewDelegate?
    
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var tf_name: SKTextField!
    @IBOutlet weak var tf_phone: SKTextField!
    @IBOutlet weak var tf_email: SKTextField!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var btn_Order: UIButton!
    @IBOutlet weak var lbl_WarmingName: UILabel!
    @IBOutlet weak var lbl_WarmingPhone: UILabel!
    
    private var sk_tf_Name: UITextField {
        return self.tf_name.titleTextField
    }
    
    private var sk_tf_Phone: UITextField {
        return self.tf_phone.titleTextField
    }
    
    private var sk_tf_Email: UITextField {
        return self.tf_email.titleTextField
    }
    
    var userRegisterParam: OrderRegisterParam = OrderRegisterParam(name: "", phone: "", email: "")
    var product: ProductModel?
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showAnimate()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitGestureView()
        self.setInitUITextField()
        
        self.btn_Order.isUserInteractionEnabled = false
        self.btn_Order.backgroundColor = UIColor(red: 0.946, green: 0.946, blue: 0.946, alpha: 1)
        self.btn_Order.setTitleColor(UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1), for: .normal)
        self.tf_name.setValue(with: gUser?.fullName)
        self.tf_phone.setValue(with: gUser?.phoneNumber)
        self.userRegisterParam.name = gUser?.fullName ?? ""
        self.userRegisterParam.phone = gUser?.phoneNumber ?? ""
        
        for textField in [self.sk_tf_Name, self.sk_tf_Phone, self.sk_tf_Email] {
            self.updateTypeLabelHiddenState(of: textField)
        }
    }
    
    // MARK: - Action
    @IBAction func onRegisterOrderAction(_ sender: Any) {
        var finalStatus = true
        
        if self.userRegisterParam.name.trimmingCharacters(in: .whitespacesAndNewlines).count < 2 || self.userRegisterParam.name.trimmingCharacters(in: .whitespacesAndNewlines).count > 32 {
            self.tf_name.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_WarmingName.isHidden = false
            self.lbl_WarmingName.text = "Thông tin không hợp lệ"
            finalStatus = false
        }
                
        if String.isNilOrEmpty(self.userRegisterParam.phone) {
            self.tf_phone.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_WarmingPhone.isHidden = false
            self.lbl_WarmingPhone.text = "Vui lòng nhập số điện thoại"
            finalStatus = false
            
        } else if self.userRegisterParam.phone.count < 10 || self.userRegisterParam.phone.first != "0" {
            self.tf_phone.bgView.layer.borderColor = UIColor(hex: "FF3030").cgColor
            self.lbl_WarmingPhone.isHidden = false
            self.lbl_WarmingPhone.text = "Số điện thoại không hợp lệ"
            finalStatus = false
        }
        
        if finalStatus {
            print("<onRegisterOrderAction> \(self.userRegisterParam.name)")
            print("<onRegisterOrderAction> \(self.userRegisterParam.phone)")
            print("<onRegisterOrderAction> \(self.userRegisterParam.email)")
            
            self.viewModel.onRegisterProduct(name: self.userRegisterParam.name,
                                             phone: self.userRegisterParam.phone,
                                             email: self.userRegisterParam.email,
                                             productID: self.product?.id?.asString ?? "-1")
        }
    }
}

//MARK: - UITextViewDelegate
extension AlertRegisterOrderViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.sk_tf_Name.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Phone.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        self.sk_tf_Email.addTarget(self, action: #selector(self.onTextFieldDidChange(_:)), for: .editingChanged)
        
        self.tf_name.setContentType(.name)
        self.tf_name.setKeyboardType(.alphabet)
        self.tf_phone.setContentType(.telephoneNumber)
        self.tf_phone.setKeyboardType(.phonePad)
        
        self.sk_tf_Name.delegate = self
        self.sk_tf_Phone.delegate = self
        self.sk_tf_Email.delegate = self

        self.tf_name.setEnable(isEnable: true)
        self.tf_phone.setEnable(isEnable: true)
        self.tf_email.setEnable(isEnable: true)
        
        self.tf_name.updateTypeLabelHiddenState()
        self.tf_phone.updateTypeLabelHiddenState()
        self.tf_email.updateTypeLabelHiddenState()
        
        self.sk_tf_Name.clearButtonMode = .whileEditing
        self.sk_tf_Email.clearButtonMode = .whileEditing
        self.sk_tf_Phone.clearButtonMode = .whileEditing
    }
    
    @objc func onTextFieldDidChange(_ textField: UITextField) {
        let newText = textField.text ?? ""
        
        print("onTextFieldDidChange newText:", newText)
        
        switch textField {
        case self.sk_tf_Name:
            if !String.isNilOrEmpty(newText) {
                self.tf_name.bgView.borderColor = UIColor(hex: "D3DBE3")
            }
            self.userRegisterParam.name = newText
            
        case self.sk_tf_Phone:
            if !String.isNilOrEmpty(newText) {
                self.tf_phone.bgView.borderColor = UIColor(hex: "D3DBE3")
            }
            self.userRegisterParam.phone = newText
            
        case self.sk_tf_Email:
            if !String.isNilOrEmpty(newText) {
                self.tf_email.bgView.borderColor = UIColor(hex: "D3DBE3")
            }
            self.userRegisterParam.email = newText
        default:
            break
        }
    }
    
    private func updateTypeLabelHiddenState(of textField: UITextField) {
        switch textField {
        case self.sk_tf_Name:
            self.tf_name.updateTypeLabelHiddenState()
            
        case self.sk_tf_Email:
            self.tf_email.updateTypeLabelHiddenState()

        case self.sk_tf_Phone:
            self.tf_phone.updateTypeLabelHiddenState()

        default:
            break
        }
        
        if self.tf_name.getValue() != "" && self.tf_phone.getValue() != "" {
            self.btn_Order.isUserInteractionEnabled = true
            self.btn_Order.backgroundColor = UIColor(red: 0, green: 0.761, blue: 0.773, alpha: 1)
            self.btn_Order.setTitleColor(.white, for: .normal)
            
        } else {
            self.btn_Order.isUserInteractionEnabled = false
            self.btn_Order.backgroundColor = UIColor(red: 0.946, green: 0.946, blue: 0.946, alpha: 1)
            self.btn_Order.setTitleColor(UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 1), for: .normal)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
        self.lbl_WarmingName.isHidden = true
        self.lbl_WarmingPhone.isHidden = true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.updateTypeLabelHiddenState(of: textField)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard string.isBackSpace == false else {
            return true
        }
        
        switch textField {
        case self.sk_tf_Phone:
            if textField.text?.count ?? 0 < 3 {
                return true
                
            } else {
                let text0 = textField.text?.first
                switch text0 {
                case "0":
                    if textField.text?.count ?? 0 < 10 {
                        return true
                    } else {
                        return false
                    }
                    
                case "+":
                    if textField.text?.count ?? 0 < 12 {
                        return true
                        
                    } else {
                        return false
                    }
                    
                default:
                    if textField.text?.count ?? 0 < 12 {
                        return true
                    } else {
                        return false
                    }
                }
            }
            
        case self.sk_tf_Name:
            if textField.text?.count ?? 0 < 32 {
                return true
            } else {
                return false
            }
            
        default:
            return true
        }
    }
}

// MARK: - AlertRegisterOrder ViewProtocol
extension AlertRegisterOrderViewController: AlertRegisterOrderViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onPresentAlertConfirmSuccessVC() {
        self.presentAlertConfirmSuccessVC()
    }
}

// MARK: - Init Animation
extension AlertRegisterOrderViewController {
    func showAnimate() {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0
        
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.alpha = 1
            self.view.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func hiddenAnimate() {
        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0
            self.view.layoutIfNeeded()
            
        }, completion: { (finished) -> Void in
            self.dismiss(animated: false, completion: nil)
        })
    }
}

// MARK: - Init GestureRecognizer
extension AlertRegisterOrderViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.hiddenAnimate()
    }
}

// MARK: AlertConfirmSuccessViewDelegate
extension AlertRegisterOrderViewController: AlertConfirmSuccessViewDelegate {
    func presentAlertConfirmSuccessVC() {
        let controller = AlertConfirmSuccessViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.titleAlert = "Đăng ký thành công"
        controller.contentAlert = "Chúng tôi sẽ liên hệ lại sớm với bạn"
        self.view_Content.isHidden = true
        self.present(controller, animated: false)
    }
    
    func onDidDismissAlertConfirmSuccessView() {
        self.hiddenAnimate()
    }
}
