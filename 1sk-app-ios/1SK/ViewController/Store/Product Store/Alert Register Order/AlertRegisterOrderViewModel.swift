//
//  
//  AlertRegisterOrderViewModel.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol AlertRegisterOrderViewModelProtocol {
    func onViewDidLoad()
    func onRegisterProduct(name: String, phone: String, email: String, productID: String)
    
    //func onViewDidAppear()
    
    // UITableView
    //func numberOfSections() -> Int
    //func numberOfRows() -> Int
    //func cellForRow(at indexPath: IndexPath) -> Any?
    //func didSelectRow(at indexPath: IndexPath)
    
    // UICollectionView
    //func numberOfItems() -> Int
    //func cellForItem(at indexPath: IndexPath) -> Any
    //func didSelectItem(at indexPath: IndexPath)
}

// MARK: - AlertRegisterOrder ViewModel
class AlertRegisterOrderViewModel {
    weak var view: AlertRegisterOrderViewProtocol?
    private var interactor: AlertRegisterOrderInteractorInputProtocol

    init(interactor: AlertRegisterOrderInteractorInputProtocol) {
        self.interactor = interactor
    }

}

// MARK: - AlertRegisterOrder ViewModelProtocol
extension AlertRegisterOrderViewModel: AlertRegisterOrderViewModelProtocol {
    func onViewDidLoad() {
        
    }
    
    func onRegisterProduct(name: String, phone: String, email: String, productID: String) {
        self.view?.showHud()
        self.interactor.post_RegisterProduct(name: name, phone: phone, email: email, productID: productID)
    }
}

// MARK: - AlertRegisterOrder InteractorOutputProtocol
extension AlertRegisterOrderViewModel: AlertRegisterOrderInteractorOutputProtocol {
    func onPost_RegisterProduct_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(_):
            self.view?.onPresentAlertConfirmSuccessVC()
            break
            
        case .failure(let error):
            SKToast.shared.showToast(content: error.message)
        }
        
        self.view?.hideHud()
    }

}
