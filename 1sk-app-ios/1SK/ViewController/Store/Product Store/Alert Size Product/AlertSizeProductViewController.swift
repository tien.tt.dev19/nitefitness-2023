//
//  AlertSizeProductViewController.swift
//  1SK
//
//  Created by Thaad on 08/08/2022.
//

import UIKit

protocol AlertSizeProductViewDelegate: AnyObject {
    func onAddCartByAlertAction(listAttributes: [Int])
    func onBuyNowByAlertAction(listAttributes: [Int])
}

class AlertSizeProductViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var img_productImage: UIImageView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    @IBOutlet weak var view_ExchangeSizeTable: UIView!
    @IBOutlet weak var tbv_MaintableView: UITableView!
    @IBOutlet weak var btn_ConfirmChooseSize: UIButton!
    
    var storeService = StoreService()
    
    weak var delegate: AlertSizeProductViewDelegate?
    
    var product: ProductModel?
    var listAttributes = [Int]()
    var indexSection = [Int]()
    var type = -1
    var isReloadData = true
    var indexNotChooseAttribute: [Int] = []
    var listAllAttributeID: [Int] = []
    var listAttributeUserChoose: [IndexPath] = []
    var currentIndex = -1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInit()
        self.setInitGestureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    private func setInit() {
        self.setInitTableView()
        self.img_productImage.setImageWith(imageUrl: self.product?.image ?? "")
        
        if let _ = self.product?.imageChooseSize {
            
        } else {
            self.view_ExchangeSizeTable.isHidden = true
        }
        
        if let listFullAttributes = self.product?.fullAttributes {
            for _ in listFullAttributes {
                self.listAttributes.append(-1)
                self.listAttributeUserChoose.append(IndexPath(row: -1, section: -1))
            }
        }
        
        if self.type == 0 {
            self.btn_ConfirmChooseSize.setTitle("Thêm vào giỏ hàng", for: .normal)
            
        } else {
            self.btn_ConfirmChooseSize.setTitle("Mua ngay", for: .normal)
        }
    }
    
    @IBAction func onAddCartAction(_ sender: Any) {
        var final = false
        
        let duplicates = self.listAttributes.allIndex(of: -1)
        self.indexNotChooseAttribute = duplicates ?? []
        
        for item in self.indexNotChooseAttribute {
            self.tbv_MaintableView.reloadSections(NSIndexSet(index: item) as IndexSet, with: .none)
        }
        
        for item in self.listAttributes {
            if item == -1 {
                final = false
                return
                
            } else {
                final = true
            }
        }
        
        if final {
            if self.product?.quantity == 0 {
                SKToast.shared.showToast(content: "Sản phẩm đã hết hàng")
                
            } else {
                self.dismiss(animated: false) {
                    if self.type == 0 {
                        self.delegate?.onAddCartByAlertAction(listAttributes: self.listAttributes)
                        
                    } else {
                        self.delegate?.onBuyNowByAlertAction(listAttributes: self.listAttributes)
                    }
                }
            }
        }
    }
    
    @IBAction func onPresentExchangeSizeTable(_ sender: Any) {
        let controller = AlertExchangeSizeViewController()
        controller.modalPresentationStyle = .custom
        controller.product = self.product
        self.present(controller, animated: false)
    }
}

extension AlertSizeProductViewController {
    func getAllAttributeId(listAttribute: [Attribute]) -> [Int] {
        var attributeArray = [Int]()
        for item in listAttribute {
            attributeArray.append(item.id ?? -1)
        }
        return attributeArray
    }
}

//MARK: - UITableViewDataSource
extension AlertSizeProductViewController: UITableViewDataSource {
    func setInitTableView() {
        self.tbv_MaintableView.registerHeaderNib(ofType: AttributeHeader.self)
        self.tbv_MaintableView.registerNib(ofType: CellTableViewAttribute.self)
        self.tbv_MaintableView.registerFooterNib(ofType: AttributeFooter.self)
        
        self.tbv_MaintableView.dataSource = self
        self.tbv_MaintableView.delegate = self
        
        self.tbv_MaintableView.rowHeight = UITableView.automaticDimension
        self.tbv_MaintableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_MaintableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_MaintableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.product?.fullAttributes?.count ?? 0
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewAttribute.self, for: indexPath)
        if let listAttribute = self.product?.fullAttributes?[indexPath.section].productAttributes {
            cell.delegate = self
            cell.indexPath = indexPath
            cell.listUnavailableAttributeID = self.product?.unavailableAttributeId
            cell.listAttribute = listAttribute
            cell.indexSelected = self.listAttributeUserChoose[indexPath.section].item
                        
            if let item = self.product?.fullAttributes?[indexPath.section].productAttributes {
                cell.listAllAttributeID = self.getAllAttributeId(listAttribute: item)
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: AttributeHeader.self)
        header.lbl_title.text = self.product?.fullAttributes?[section].title ?? ""
        return header
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let header = tableView.dequeueHeaderView(ofType: AttributeFooter.self)
        header.lbl_title.text = "Vui lòng chọn " + (self.product?.fullAttributes?[section].title ?? "")
        
        if self.indexNotChooseAttribute.contains(section) {
            header.lbl_title.isHidden = false
            
        } else {
            header.lbl_title.isHidden = true
        }
        
        return header
    }
}

//MARK: - UITableViewDelegate
extension AlertSizeProductViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 24
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - Animation
extension AlertSizeProductViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ContentView.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertSizeProductViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        self.setAnimationHidden()
    }
    
    func setHeightForCellReviewsTag(height: CGFloat, indexPath: IndexPath?) {
        guard let _indexPath = indexPath else { return }
        self.product?.fullAttributes?[_indexPath.section].heightRow = height
        
        if self.isReloadData == true {
            self.isReloadData = false
            self.tbv_MaintableView.reloadRows(at: [_indexPath], with: .automatic)
        }
    }
}

//MARK: - CellTableViewAttributeDelagate
extension AlertSizeProductViewController: CellTableViewAttributeDelagate {
    func listAttributeSelected(cell: CellTableViewAttribute, attribute: Attribute, indexItem: Int) {
        let index = self.tbv_MaintableView.indexPath(for: cell)!.section
        self.currentIndex = index
        self.indexSection.append(index)
        self.listAttributes[index] = attribute.id ?? -1
        self.listAttributeUserChoose[index] = IndexPath(row: indexItem, section: index)
        self.changeProductImage(attributes: self.listAttributes)
        self.tbv_MaintableView.footerView(forSection: index)?.isHidden = true
    }
}

//MARK: - Change product image
extension AlertSizeProductViewController {
    func changeProductImage(attributes: [Int]) {
        self.storeService.getVariationProduct(id: self.product?.originalProduct ?? -1, attributeIDs: attributes) { [weak self] result in
            switch result.unwrapSuccessModel() {
            case .success(let model):
                self?.product = model
                self?.img_productImage.setImageWith(imageUrl: model.image ?? "")
                self?.reloadAllAttribute(currentIndex: self!.currentIndex)
                
            case .failure(let error):
                debugPrint(error)
                SKToast.shared.showToast(content: error.message)
            }
        }
    }
    
    func reloadAllAttribute(currentIndex: Int) {
        if currentIndex == self.listAttributes.count - 1 {
            return
        }
        
        let indexSet = IndexSet(currentIndex + 1 ... self.listAttributes.count - 1)
        for item in currentIndex + 1 ... self.listAttributes.count - 1 {
            self.listAttributes[item] = -1
            self.listAttributeUserChoose[item] = IndexPath(row: -1, section: -1)
        }
        self.tbv_MaintableView.reloadSections(indexSet, with: .automatic)
        
        print("<listAttributeSelected> listAttributes \(self.listAttributes)")
        print("<listAttributeSelected> \(self.listAttributeUserChoose)")
        print("<listAttributeSelected> current index \(self.currentIndex)")
        print("<listAttributeSelected>------------------------------------")
    }
}
