//
//  CellCollectionViewAttribute.swift
//  1SK
//
//  Created by Tiến Trần on 10/08/2022.
//

import UIKit

class CellCollectionViewAttribute: UICollectionViewCell {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var lbl_Content: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override var isSelected: Bool {
        didSet {
            self.setUIStateView(isSelected: isSelected)
        }
    }
    
    func setUIStateView(isSelected: Bool) {
        if isSelected {
            self.view_Bg.borderColor = UIColor(hex: "FEB11A")
            self.lbl_Content.textColor = UIColor(hex: "FEB11A")

        } else {
            self.view_Bg.borderColor = UIColor(hex: "F1F1F1")
            self.lbl_Content.textColor = .black
        }
    }
}
