//
//  CellTableViewAttribute.swift
//  1SK
//
//  Created by Valerian on 13/08/2022.
//

import UIKit

protocol CellTableViewAttributeDelagate {
    func listAttributeSelected(cell: CellTableViewAttribute, attribute: Attribute, indexItem: Int)
    func setHeightForCellReviewsTag(height: CGFloat, indexPath: IndexPath?)
}

class CellTableViewAttribute: UITableViewCell {
    
    @IBOutlet weak var coll_AttributeTag: UICollectionView!
    var indexPath: IndexPath?
    var delegate: CellTableViewAttributeDelagate?
    
    var listUnavailableAttributeID: [Int]?
    var listAllAttributeID: [Int]?
    var indexSelected: Int?
    var listAttribute: [Attribute]? {
        didSet {
            self.coll_AttributeTag.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let heightColl = self.coll_AttributeTag.contentSize.height
                self.delegate?.setHeightForCellReviewsTag(height: heightColl, indexPath: self.indexPath)
            }
        }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        self.coll_AttributeTag.layoutIfNeeded()
        self.layoutIfNeeded()
        let contentSize = self.coll_AttributeTag.collectionViewLayout.collectionViewContentSize
        return CGSize(width: contentSize.width, height: contentSize.height + 10)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.setInitUICollectionView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}

// MARK: UICollectionViewDataSource
extension CellTableViewAttribute: UICollectionViewDataSource, UICollectionViewDelegate {
    private func setInitUICollectionView() {
        self.coll_AttributeTag.registerNib(ofType: CellCollectionViewAttribute.self)
        self.coll_AttributeTag.dataSource = self
        self.coll_AttributeTag.delegate = self
        
        let layout = UICollectionViewLayoutCustom()
        layout.alignment = .left
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 5
        layout.estimatedItemSize = CGSize(width: 128, height: 38)
        self.coll_AttributeTag.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listAttribute?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewAttribute.self, for: indexPath)
        cell.lbl_Content.text = self.listAttribute?[indexPath.row].title ?? ""
        guard let listUnavailable = self.listUnavailableAttributeID else { return cell }
        
        if indexPath.row == self.indexSelected {
            cell.view_Bg.borderColor = UIColor(hex: "FEB11A")
            cell.lbl_Content.textColor = UIColor(hex: "FEB11A")
            
        } else {
            cell.view_Bg.borderColor = UIColor(hex: "F1F1F1")
            cell.lbl_Content.textColor = .black
        }
        
        if listUnavailable.contains(self.listAllAttributeID?[indexPath.row] ?? -1) {
            cell.lbl_Content.textColor = UIColor(hex: "C4C4C4")
            cell.lbl_Content.backgroundColor = .clear
            cell.view_Bg.borderColor = UIColor(hex: "F1F1F1")
            cell.view_Bg.backgroundColor = UIColor(hex: "EEEEEE")
            cell.isUserInteractionEnabled = false
            
        } else {
            cell.lbl_Content.backgroundColor = .clear
            cell.view_Bg.backgroundColor = .white
            cell.isUserInteractionEnabled = true
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.listAttributeSelected(cell: self, attribute: (self.listAttribute?[indexPath.row])!, indexItem: indexPath.item)
    }
}

//MARK: -UICollectionViewDelegateFlowLayout
extension CellTableViewAttribute: UICollectionViewDelegateFlowLayout {

}
