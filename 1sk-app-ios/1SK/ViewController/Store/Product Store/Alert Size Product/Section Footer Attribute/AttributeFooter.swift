//
//  AttributeFooter.swift
//  1SK
//
//  Created by Tiến Trần on 15/08/2022.
//

import UIKit

class AttributeFooter: UITableViewHeaderFooterView {
    @IBOutlet weak var lbl_title: UILabel!
    
    var title: String? {
        didSet {
            self.lbl_title.text = title ?? ""
        }
    }
}
