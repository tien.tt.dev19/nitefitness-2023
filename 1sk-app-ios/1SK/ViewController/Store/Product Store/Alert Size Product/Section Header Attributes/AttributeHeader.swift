//
//  AttributeHeader.swift
//  1SK
//
//  Created by Tiến Trần on 10/08/2022.
//

import UIKit

class AttributeHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var lbl_title: UILabel!
    
    var title: String? {
        didSet {
            self.lbl_title.text = title ?? ""
        }
    }
}
