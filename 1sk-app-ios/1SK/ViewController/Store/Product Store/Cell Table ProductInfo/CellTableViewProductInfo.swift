//
//  CellTableViewProductInfo.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//

import UIKit

class CellTableViewProductInfo: UITableViewCell {

    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(information: InformationProduct) {
        self.lbl_Title.text = information.name
        self.lbl_Content.text = information.content
    }
}
