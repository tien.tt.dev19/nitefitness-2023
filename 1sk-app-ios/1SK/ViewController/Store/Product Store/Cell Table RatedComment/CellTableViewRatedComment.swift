//
//  CellTableViewRatedComment.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//

import UIKit
import Cosmos

class CellTableViewRatedComment: UITableViewCell {

    @IBOutlet weak var view_Rate: CosmosView!
    @IBOutlet weak var lbl_Status: UILabel!
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_Time: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func config(review: ReviewModel) {
        self.view_Rate.rating = Double(review.star ?? 0)
        self.lbl_Status.text = review.subject
        self.lbl_Name.text = review.customer?.name
        self.lbl_Content.text = review.comment
        self.lbl_Time.text = Utils.shared.getTimeStringByTimeStamp(timeStamp: Double(review.createdAt ?? 0), timeFomat: "dd/MM/YYYY HH:mm:ss")
    }
}
