//
//  
//  ProductStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ProductStoreInteractorInputProtocol {
    func getListReviewProduct(id: Int, page: Int, perPage: Int)
    func getVariationProduct(productID: Int, attributeIDs: [Int])
    func getStrategiesProduct()
}

// MARK: - Interactor Output Protocol
protocol ProductStoreInteractorOutputProtocol: AnyObject {
    func onGetListReviewProductFinished(with result: Result<[ReviewModel], APIError>, page: Int)
    func onGetVariationProductFinished(with result: Result<ProductModel, APIError>)
    func onGetStrategiesProductFinished(with result: Result<[StrategiesModel], APIError>)
}

// MARK: - ProductStore InteractorInput
class ProductStoreInteractorInput {
    weak var output: ProductStoreInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - ProductStore InteractorInputProtocol
extension ProductStoreInteractorInput: ProductStoreInteractorInputProtocol {
    func getListReviewProduct(id: Int, page: Int, perPage: Int) {
        self.storeService?.getListReviewProduct(id: id, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListReviewProductFinished(with: result.unwrapSuccessModel(), page: page)
        })
    }
    
    func getVariationProduct(productID: Int, attributeIDs: [Int]) {
        self.storeService?.getVariationProduct(id: productID, attributeIDs: attributeIDs, completion: { [weak self] result in
            self?.output?.onGetVariationProductFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getStrategiesProduct() {
        self.storeService?.getStrategiesProduct(completion: { [weak self] result in
            self?.output?.onGetStrategiesProductFinished(with: result.unwrapSuccessModel())
        })
    }
}
