//
//  
//  ProductStoreRouter.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ProductStoreRouterProtocol {
    func popViewController()
    func showCartStore()
}

// MARK: - ProductStore Router
class ProductStoreRouter {
    weak var viewController: ProductStoreViewController?
    
    static func setupModule(product: ProductModel?) -> ProductStoreViewController {
        let viewController = ProductStoreViewController()
        let router = ProductStoreRouter()
        let interactorInput = ProductStoreInteractorInput()
        let viewModel = ProductStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.hidesBottomBarWhenPushed = true
        viewController.router = router
        viewModel.view = viewController
        viewModel.product = product
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ProductStore RouterProtocol
extension ProductStoreRouter: ProductStoreRouterProtocol {
    func popViewController() {
        self.viewController?.navigationController?.popViewController(animated: true)
    }
    
    func showCartStore() {
        let controller = CartStoreRouter.setupModule()
        self.viewController?.show(controller, sender: nil)
    }
}
