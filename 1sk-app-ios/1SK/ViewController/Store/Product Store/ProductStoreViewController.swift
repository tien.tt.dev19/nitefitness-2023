//
//  
//  ProductStoreViewController.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ProductStoreViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func reloadData_Infomation()
    func reloadData_Review()
    func reloadData_Strategies()
    func onBuyNowSuccess()
    func setAnimationBadgeCart()
    func presentAlertSizeProductVC()
}

// MARK: - ProductStore ViewController
class ProductStoreViewController: BaseViewController {
    var router: ProductStoreRouterProtocol!
    var viewModel: ProductStoreViewModelProtocol!
    
    @IBOutlet weak var view_nav_Fill_Bg: UIView!
    @IBOutlet weak var view_nav_Fill: UIView!
    @IBOutlet weak var view_nav_Blur: UIView!
    
    @IBOutlet weak var view_BadgeCartFill: UIView!
    @IBOutlet weak var view_BadgeCartBlur: UIView!
    
    @IBOutlet weak var lbl_CountCartFill: UILabel!
    @IBOutlet weak var lbl_CountCartBlur: UILabel!
    
    @IBOutlet weak var tbv_TableView: UITableView!
    
    @IBOutlet weak var stack_BuyProduct: UIStackView!
    @IBOutlet weak var btn_OrderProduct: UIButton_0!
    
    @IBOutlet weak var view_Order: UIView!
    
    var heightWebView: CGFloat?
    var isReloadSections = true
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationBadgeCart()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel.onViewDidAppear()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
        self.setInitNavigationBar()
        
        if let status = self.viewModel.getProduct()?.sellStatus {
            switch status {
            case "out_of_stock":
                self.stack_BuyProduct.isHidden = true
                self.btn_OrderProduct.isHidden = true
                self.view_Order.isHidden = true
                
            case "almost_arrived_warehouse":
                self.stack_BuyProduct.isHidden = true
                
            case "coming_soon":
                self.stack_BuyProduct.isHidden = true
                self.btn_OrderProduct.isHidden = true
                self.view_Order.isHidden = true
                
            case "stop_selling":
                self.stack_BuyProduct.isHidden = true
                self.btn_OrderProduct.isHidden = true
                self.view_Order.isHidden = true
                
            case "in_stock":
                self.btn_OrderProduct.isHidden = true
                
            default:
                break
            }
            
        } else {
            self.btn_OrderProduct.isHidden = true
        }

    }
    
    // MARK: - Action
    @IBAction func onBackAction(_ sender: Any) {
        self.router.popViewController()
    }
    
    @IBAction func onCartAction(_ sender: Any) {
        self.router.showCartStore()
    }
    
    @IBAction func onAddCartAction(_ sender: Any) {
        if let productAttributes = self.viewModel.getProduct()?.fullAttributes {
            if productAttributes.isEmpty {
                
                self.viewModel.onAddCartAction()
                return
            }
            
            self.showAlertSizeProductVC()
        }
    }
    
    @IBAction func onBuyNowAction(_ sender: Any) {
        
        if let productAttributes = self.viewModel.getProduct()?.fullAttributes {
            if productAttributes.isEmpty {
                
                self.viewModel.onBuyNowAction()
                return
            }
            
            self.showAlertSizeProductVCBeforeBuy()
        }
    }
    
    @IBAction func onOrderAction(_ sender: Any) {
        self.presentAlertRegisterOrderRouter()
    }
}

// MARK: - ProductStore ViewProtocol
extension ProductStoreViewController: ProductStoreViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func reloadData_Infomation() {
        let indexSet = IndexSet(integer: 2)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
    
    func reloadData_Review() {
        let indexSet = IndexSet(integer: 3)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
    
    func reloadData_Strategies() {
        let indexSet = IndexSet(integer: 1)
        self.tbv_TableView.reloadSections(indexSet, with: .automatic)
    }
    
    func onBuyNowSuccess() {
        self.router.showCartStore()
    }
    
    func presentAlertSizeProductVC() {
        let controller = AlertSizeProductViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.product = self.viewModel.getProduct()
        self.present(controller, animated: false)
    }
}

// MARK: UITableViewDataSource
extension ProductStoreViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderProduct.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderPromotion.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderInfomation.self)
        self.tbv_TableView.registerHeaderNib(ofType: SectionHeaderRatedComment.self)
        
        self.tbv_TableView.registerNib(ofType: CellTableViewProductInfo.self)
        self.tbv_TableView.registerNib(ofType: CellTableViewRatedComment.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
        
        self.tbv_TableView.rowHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedRowHeight = UITableView.automaticDimension
        
        self.tbv_TableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tbv_TableView.estimatedSectionHeaderHeight = UITableView.automaticDimension
    }
    
    // MARK: - UITableView - Sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
        
//        switch section {
//        case 1:
//            if self.viewModel.listStrategies.count > 0 {
//                return UITableView.automaticDimension
//            } else {
//                return 0
//            }
//        default:
//            return UITableView.automaticDimension
//        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch section {
        case 0:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderProduct.self)
            header.delegate = self
            if let product = self.viewModel.getProduct() {
                header.config(product: product)
                header.sellStatus = product.sellStatus
            }
            
            if let flashSaleDay = self.viewModel.getProduct()?.flashSale?.first {
                let endDate = flashSaleDay.endDate ?? ""
                let endTime = flashSaleDay.endTime ?? ""
                
                print("<flashSaleDay endDate> \(endDate)")
                print("<flashSaleDay endTime> \(endTime)")
                
                let endDay = endDate + " " + endTime
                let endDayToDate = endDay.toDate(.ymdSlashhms)
                let endDayTimestamp = endDayToDate?.toTimestamp() ?? 0
                let secondLeft = abs(Int(endDayTimestamp - Date().toTimestamp()))
                
                header.deinitTimer()
                header.onCountTimeAction(time: secondLeft)
            } else {
                header.configUIFlashSale()
            }
            
            return header
            
        case 1:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderPromotion.self)
            header.listStrategies = self.viewModel.listStrategies
            return header
            
        case 2:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderInfomation.self)
            
            header.delegate = self
            if let product = self.viewModel.getProduct() {
                header.config(product: product, height: self.heightWebView)
            }
            return header
            
        case 3:
            let header = tableView.dequeueHeaderView(ofType: SectionHeaderRatedComment.self)
            if let product = self.viewModel.getProduct() {
                header.config(product: product)
            }
            return header
            
        default:
            return nil
        }
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
            
        case 1:
            return 0
            
        case 2:
            return self.viewModel.numberOfRows(in: section)
            
        case 3:
            return self.viewModel.numberOfRows(in: section)
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return UITableViewCell()
            
        case 1:
            return UITableViewCell()
            
        case 2:
            let cell = tableView.dequeueCell(ofType: CellTableViewProductInfo.self, for: indexPath)
            if let item = self.viewModel.cellForRow_Information(at: indexPath) {
                cell.config(information: item)
            }
            return cell
            
        case 3:
            let cell = tableView.dequeueCell(ofType: CellTableViewRatedComment.self, for: indexPath)
            if let item = self.viewModel.cellForRow_Review(at: indexPath) {
                cell.config(review: item)
            }
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    
    func setAnimationBadgeCart() {
        guard gListCartProduct.count > 0 else {
            self.lbl_CountCartFill.text = "0"
            self.lbl_CountCartBlur.text = "0"
            self.view_BadgeCartFill.isHidden = true
            self.view_BadgeCartBlur.isHidden = true
            return
        }
        
        var totalBadge = 0
        for item in gListCartProduct {
            if let amount = item.amount {
                totalBadge += amount
            }
        }
        
        if totalBadge > 9 {
            self.lbl_CountCartFill.text = "9+"
            self.lbl_CountCartBlur.text = "9+"
            
        } else {
            self.lbl_CountCartFill.text = totalBadge.asString
            self.lbl_CountCartBlur.text = totalBadge.asString
        }
        
        
        self.view_BadgeCartFill.isHidden = false
        self.view_BadgeCartBlur.isHidden = false
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn) {
            // Scale your image
            self.view_BadgeCartFill.transform = CGAffineTransform.identity.scaledBy(x: 2, y: 2)
            self.view_BadgeCartBlur.transform = CGAffineTransform.identity.scaledBy(x: 2, y: 2)
            
        } completion: { _ in
            UIView.animate(withDuration: 0.5, animations: {
                // undo in 1 seconds
                self.view_BadgeCartFill.transform = CGAffineTransform.identity
                self.view_BadgeCartBlur.transform = CGAffineTransform.identity
            })
        }
    }
}

// MARK: UITableViewDelegate
extension ProductStoreViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //
    }
}

// MARK: - SectionHeaderInfomationDelegate
extension ProductStoreViewController: SectionHeaderInfomationDelegate {
    func onDidFinishLoadWebView(heightWebView: CGFloat) {
        self.heightWebView = heightWebView
        
        if self.isReloadSections {
            self.isReloadSections = false
            self.tbv_TableView.reloadData()
        }
    }
}

// MARK: - UIScrollViewDelegate
extension ProductStoreViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        self.setStateNavigationBar(contentOffset: contentOffset)
    }
}

// MARK: - SectionHeaderProductDelegate
extension ProductStoreViewController: SectionHeaderProductDelegate {
    func onTotalReviewAction() {
        let count = self.viewModel.numberOfRows(in: 3)
        if count > 0 {
            let indexPath = IndexPath(row: 0, section: 3)
            self.tbv_TableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            
        } else {
            self.tbv_TableView.scrollToBottom(animated: true)
        }
    }
}

// MARK: - Navigation Bar
extension ProductStoreViewController {
    func setInitNavigationBar() {
        self.view_nav_Fill.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
        self.view_nav_Fill.isHidden = true
        self.view_nav_Fill_Bg.isHidden = true
    }
    
    func setStateNavigationBar(contentOffset: CGFloat) {
        if contentOffset <= 0.0 {
            if self.view_nav_Fill.isHidden == false {
                UIView.animate(withDuration: 0.5) {
                    self.view_nav_Fill.alpha = 0
                    self.view_nav_Fill_Bg.alpha = 0
                    self.view.layoutIfNeeded()
                    
                } completion: { _ in
                    self.view_nav_Fill.isHidden = true
                    self.view_nav_Fill_Bg.isHidden = true
                }
            }
        }
        
        if contentOffset > 44.0 {
            if self.view_nav_Fill.isHidden == true {
                
                self.view_nav_Fill.isHidden = false
                self.view_nav_Fill.alpha = 0
                
                self.view_nav_Fill_Bg.isHidden = false
                self.view_nav_Fill_Bg.alpha = 0
                
                UIView.animate(withDuration: 0.5) {
                    self.view_nav_Fill.alpha = 1
                    self.view_nav_Fill_Bg.alpha = 1
                    self.view.layoutIfNeeded()
                    
                } completion: { _ in
                    //
                }
                
            }
        }
    }
}

// MARK: AlertSizeProductViewDelegate
extension ProductStoreViewController: AlertSizeProductViewDelegate {
    func showAlertSizeProductVC() {
        let controller = AlertSizeProductViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.type = 0
        controller.product = self.viewModel.getProduct()
        self.present(controller, animated: false)
    }
    
    func showAlertSizeProductVCBeforeBuy() {
        let controller = AlertSizeProductViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        controller.type = 1
        controller.product = self.viewModel.getProduct()
        self.present(controller, animated: false)
    }
    
    func onAddCartByAlertAction(listAttributes: [Int]) {
        self.viewModel.onGetVariationProduct(attributeIDs: listAttributes, with: 0)
    }
    
    func onBuyNowByAlertAction(listAttributes: [Int]) {
        self.viewModel.onGetVariationProduct(attributeIDs: listAttributes, with: 1)
    }
}

// MARK: AlertRegisterOrderViewDelegate
extension ProductStoreViewController: AlertRegisterOrderViewDelegate {
    func presentAlertRegisterOrderRouter() {
        let controller = AlertRegisterOrderRouter.setupModule()
        controller.modalPresentationStyle = .custom
        controller.product = self.viewModel.getProduct()
        self.present(controller, animated: false)
    }
    
    func onDidRegisterOrder() {
        
    }
}
