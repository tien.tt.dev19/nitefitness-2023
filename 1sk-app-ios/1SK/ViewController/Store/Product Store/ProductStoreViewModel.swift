//
//  
//  ProductStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ProductStoreViewModelProtocol {
    func onViewDidLoad()
    func onViewDidAppear()
    
    func numberOfRows(in section: Int) -> Int
    func cellForRow_Information(at indexPath: IndexPath) -> InformationProduct?
    func cellForRow_Review(at indexPath: IndexPath) -> ReviewModel?
    
    func getProduct() -> ProductModel?
    func onAddCartAction()
    func onBuyNowAction()
    
    func onGetVariationProduct(attributeIDs: [Int], with type: Int)
    
    var listStrategies: [StrategiesModel] { get set }
}

// MARK: - ProductStore ViewModel
class ProductStoreViewModel {
    weak var view: ProductStoreViewProtocol?
    private var interactor: ProductStoreInteractorInputProtocol

    init(interactor: ProductStoreInteractorInputProtocol) {
        self.interactor = interactor
    }

    var product: ProductModel?
    var type = -1
    var listReview: [ReviewModel] = []
    var listStrategies: [StrategiesModel] = []
    
    func getDataReview() {
        if let id = self.product?.id {
            self.interactor.getListReviewProduct(id: id, page: 1, perPage: 10)
        }
    }
    
    func getDataStrategies() {
        self.interactor.getStrategiesProduct()
    }
}

// MARK: - ProductStore ViewModelProtocol
extension ProductStoreViewModel: ProductStoreViewModelProtocol {
    func onViewDidLoad() {
        self.getDataStrategies()
        self.getDataReview()
    }
    
    func onViewDidAppear() {
        
    }
    
    // UItableView
    func numberOfRows(in section: Int) -> Int {
        switch section {
        case 2:
            return self.product?.information?.count ?? 0
            
        case 3:
            return self.listReview.count
            
        default:
            return 0
        }
        
    }
    
    func cellForRow_Information(at indexPath: IndexPath) -> InformationProduct? {
        return self.product?.information?[indexPath.row]
    }
    
    func cellForRow_Review(at indexPath: IndexPath) -> ReviewModel? {
        return self.listReview[indexPath.row]
    }
    
    // Data
    func getProduct() -> ProductModel? {
        return self.product
    }
    
    func onAddCartAction() {
        self.view?.showHud()
        if let product = gListCartProduct.first(where: {$0.id == self.product?.id}) {
            product.name = self.product?.name
            product.image = self.product?.image
            product.price = self.product?.frontSalePriceWithTaxes
            product.pricerOld = self.product?.priceWithTaxes
            product.isSalePrice = self.product?.isSalePrice
            product.isSelected = true
            if let attributes = self.product?.attributes {
                for item in attributes {
                    let tempAttribute = AttributeProduct()
                    tempAttribute.id = item.id
                    tempAttribute.title = item.title
                    tempAttribute.code = item.code
                    tempAttribute.value = item.value
                    product.attributes?.append(tempAttribute)
                }
            }

            if let amount = product.amount {
                product.amount = amount + 1
            } else {
                product.amount = 1
            }

            Shared.instance.cartProductSchema = gListCartProduct

        } else {
            let product = CartProductSchema()
            product.id = self.product?.id
            product.name = self.product?.name
            product.image = self.product?.image
            product.price = self.product?.frontSalePriceWithTaxes
            product.pricerOld = self.product?.priceWithTaxes
            product.isSalePrice = self.product?.isSalePrice
            product.amount = 1
            product.isSelected = true
            if let attributes = self.product?.attributes {
                for item in attributes {
                    let tempAttribute = AttributeProduct()
                    tempAttribute.id = item.id
                    tempAttribute.title = item.title
                    tempAttribute.code = item.code
                    tempAttribute.value = item.value
                    product.attributes?.append(tempAttribute)
                }
            }

            gListCartProduct.insert(product, at: 0)

            Shared.instance.cartProductSchema = gListCartProduct
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.view?.hideHud()
            self.view?.setAnimationBadgeCart()
        }
    }
    
    func onBuyNowAction() {
        self.view?.showHud()
        
        for item in gListCartProduct {
            item.isSelected = false
        }
        Shared.instance.cartProductSchema = gListCartProduct
        sleep(UInt32(0.5))
        
        if let product = gListCartProduct.first(where: {$0.id == self.product?.id}) {
            product.name = self.product?.name
            product.image = self.product?.image
            product.price = self.product?.frontSalePriceWithTaxes
            product.pricerOld = self.product?.priceWithTaxes
            product.isSalePrice = self.product?.isSalePrice
            product.isSelected = true
        
            if let attributes = self.product?.attributes {
                for item in attributes {
                    let tempAttribute = AttributeProduct()
                    tempAttribute.id = item.id
                    tempAttribute.title = item.title
                    tempAttribute.code = item.code
                    tempAttribute.value = item.value
                    product.attributes?.append(tempAttribute)
                }
            }
            
            if let amount = product.amount {
                product.amount = amount + 1
            } else {
                product.amount = 1
            }
            
            Shared.instance.cartProductSchema = gListCartProduct
            
        } else {
            let product = CartProductSchema()
            product.id = self.product?.id
            product.name = self.product?.name
            product.image = self.product?.image
            product.price = self.product?.frontSalePriceWithTaxes
            product.pricerOld = self.product?.priceWithTaxes
            product.isSalePrice = self.product?.isSalePrice
            product.amount = 1
            product.isSelected = true
            if let attributes = self.product?.attributes {
                for item in attributes {
                    let tempAttribute = AttributeProduct()
                    tempAttribute.id = item.id
                    tempAttribute.title = item.title
                    tempAttribute.code = item.code
                    tempAttribute.value = item.value
                    product.attributes?.append(tempAttribute)
                }
            }
            
            gListCartProduct.insert(product, at: 0)
            
            Shared.instance.cartProductSchema = gListCartProduct
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.view?.hideHud()
            self.view?.onBuyNowSuccess()
            self.view?.setAnimationBadgeCart()
        }
    }
    
    func onGetVariationProduct(attributeIDs: [Int], with type: Int) {
        self.view?.showHud()
        self.type = type
        self.interactor.getVariationProduct(productID: self.product?.originalProduct ?? -1, attributeIDs: attributeIDs)
        print("<onGetVariationProductFinished> productID id: \(self.product?.originalProduct ?? 0)")
        print("<onGetVariationProductFinished> attributeIDs id: \(attributeIDs)")
    }
}

// MARK: - ProductStore InteractorOutputProtocol
extension ProductStoreViewModel: ProductStoreInteractorOutputProtocol {
    func onGetListReviewProductFinished(with result: Result<[ReviewModel], APIError>, page: Int) {
        switch result {
        case .success(let model):
            self.listReview = model
            self.view?.reloadData_Review()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetVariationProductFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.product = model
            
            if self.type == 0 {
                print("<onGetVariationProductFinished> AddCart id: \(self.product?.id ?? 0)")
                self.onAddCartAction()
                
            } else {
                print("<onGetVariationProductFinished> BuyNow id: \(self.product?.id ?? 0)")

                self.onBuyNowAction()
            }
            
        case .failure(let error):
            debugPrint(error)
            SKToast.shared.showToast(content: error.message)
            self.view?.hideHud()
        }
    }
    
    func onGetStrategiesProductFinished(with result: Result<[StrategiesModel], APIError>) {
        switch result {
        case .success(let model):
            self.listStrategies = model
            self.view?.reloadData_Strategies()
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
