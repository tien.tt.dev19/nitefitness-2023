//
//  SectionHeaderInfomation.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit
import WebKit

protocol SectionHeaderInfomationDelegate: AnyObject {
    func onDidFinishLoadWebView(heightWebView: CGFloat)
}

class SectionHeaderInfomation: UITableViewHeaderFooterView {
    @IBOutlet weak var web_WebView: WKWebView!
    @IBOutlet weak var constraint_height_Webview: NSLayoutConstraint!
    
    weak var delegate: SectionHeaderInfomationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitWKWebView()
    }

    func config(product: ProductModel?, height: CGFloat?) {
        self.web_WebView.loadHTMLString(product?.content?.asHtmlStoreProductInfo ?? "", baseURL: nil)
        self.constraint_height_Webview.constant = height ?? 0
    }
}

// MARK: - WKNavigationDelegate
extension SectionHeaderInfomation: WKNavigationDelegate {
    func setInitWKWebView() {
        self.web_WebView.navigationDelegate = self
        self.web_WebView.scrollView.isScrollEnabled = false
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.web_WebView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_WebView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    if let `height` = height as? CGFloat {
                        print("web_WebView didFinish height: ", `height`)
                        self.delegate?.onDidFinishLoadWebView(heightWebView: height)
                    }
                })
            }
        })
    }
}
