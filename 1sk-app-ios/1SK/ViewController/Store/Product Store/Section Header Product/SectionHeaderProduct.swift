//
//  SectionHeaderProduct.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit
import ImageSlideshow
import Cosmos

protocol SectionHeaderProductDelegate: AnyObject {
    func onTotalReviewAction()
}

class SectionHeaderProduct: UITableViewHeaderFooterView {

    @IBOutlet weak var view_SlideShow: ImageSlideshow!
    
    @IBOutlet weak var lbl_Name: UILabel!
    @IBOutlet weak var lbl_TotalReview: UILabel!
    @IBOutlet weak var view_Rate: CosmosView!
    
    @IBOutlet weak var lbl_Price: UILabel!
    @IBOutlet weak var lbl_PriceOld: UILabel!
    
    @IBOutlet weak var view_Discount: UIView!
    @IBOutlet weak var lbl_Discount: UILabel!
    
    @IBOutlet weak var view_FlashSale: UIView!
    weak var delegate: SectionHeaderProductDelegate?
    
    @IBOutlet weak var lbl_day: UILabel!
    @IBOutlet weak var lbl_hour: UILabel!
    @IBOutlet weak var lbl_minute: UILabel!
    @IBOutlet weak var lbl_second: UILabel!
    
    @IBOutlet weak var view_SellStatus: UIView!
    @IBOutlet weak var img_SellStatus: UIImageView!
    @IBOutlet weak var lbl_SellStatus: UILabel!
    @IBOutlet weak var contraint_heightContentSellStatus: NSLayoutConstraint!
    
    var timer: Timer?
    var sellStatus: String? {
        didSet {
            if let title = sellStatus {
                switch title {
                case "out_of_stock":
                    self.img_SellStatus.image = R.image.ic_box_yellow()
                    self.lbl_SellStatus.textColor = UIColor(hex: "FB910F")
                    self.lbl_SellStatus.text = "Sản phẩm hết hàng"
                    
                case "stop_selling":
                    self.img_SellStatus.image = R.image.ic_box_red()
                    self.lbl_SellStatus.textColor = UIColor(red: 1, green: 0.188, blue: 0.188, alpha: 1)
                    self.lbl_SellStatus.text = "Sản phẩm ngừng bán"
                    
                case "coming_soon":
                    self.img_SellStatus.image = R.image.ic_box_gray()
                    self.lbl_SellStatus.textColor = UIColor(red: 0.451, green: 0.463, blue: 0.471, alpha: 1)
                    self.lbl_SellStatus.text = "Sản phẩm sắp ra mắt"

                case "almost_arrived_warehouse":
                    self.img_SellStatus.image = R.image.ic_box_gray()
                    self.lbl_SellStatus.textColor = .gray
                    self.lbl_SellStatus.text = "Sản phẩm sắp về"
                
                default:
                    break
                }
                
            } else {
                self.view_SellStatus.isHidden = true
                self.contraint_heightContentSellStatus.constant = 0
            }
        }
    }
    
    var endDate: Int = 0 {
        didSet {
            let days = self.endDate / 86400
            if days / 10 > 0 {
                self.lbl_day.text = String(days)
            } else {
                self.lbl_day.text = "0\(days)"
            }
                        
            let hour = self.endDate % 86400 / 3600
            
            if hour / 10 > 0 {
                self.lbl_hour.text = String(hour)
            } else {
                self.lbl_hour.text = "0\(hour)"
            }
            
            let minute = self.endDate % 3600 / 60
            if minute / 10 > 0 {
                self.lbl_minute.text = String(minute)
            } else {
                self.lbl_minute.text = "0\(minute)"
            }

            let second = self.endDate % 3600 % 60
            if second / 10 > 0 {
                self.lbl_second.text = String(second)
            } else {
                self.lbl_second.text = "0\(second)"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    @IBAction func onTotalReviewAction(_ sender: Any) {
        self.delegate?.onTotalReviewAction()
    }
    
    func config(product: ProductModel) {
        self.lbl_Name.text = product.name
        self.lbl_TotalReview.text = "\(product.reviewsCount ?? 0) đánh giá"
        self.view_Rate.rating = product.reviewsAvg ?? 0
        self.lbl_Price.text = "\(product.frontSalePriceWithTaxes?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.text = "\(product.priceWithTaxes?.formatNumber() ?? "0")đ"
        self.lbl_PriceOld.strikeThrough(true)
        
        if product.isSalePrice == true {
            self.view_Discount.isHidden = false
            self.lbl_Discount.text = "-\(product.salePercentage ?? 0)%"
            self.lbl_PriceOld.isHidden = false
            
        } else {
            self.view_Discount.isHidden = true
            self.lbl_PriceOld.isHidden = true
        }
        
        self.setUISliderBanner(sliderBanner: product.images)
    }
    
    private func setUISliderBanner(sliderBanner: [String]?) {
        guard let listBanners = sliderBanner, listBanners.count > 0 else {
            return
        }
        
        var imageSource: [InputSource] = []
        for image in listBanners {
            if let url = URL(string: image) {
                let kingfisherSource = KingfisherSource(url: url, placeholder: R.image.img_default(), options: nil)
                imageSource.append(kingfisherSource)
            }
        }
        
        self.setInitImageSlideshow(imageSource: imageSource)
    }
}

// MARK: ImageSlideshowDelegate
extension SectionHeaderProduct: ImageSlideshowDelegate {
    func setInitImageSlideshow(imageSource: [InputSource]) {
        self.view_SlideShow.slideshowInterval = 3.0
        
        self.view_SlideShow.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        self.view_SlideShow.contentScaleMode = UIViewContentMode.scaleAspectFit

        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = R.color.mainColor()
        pageControl.pageIndicatorTintColor = UIColor.lightGray.withAlphaComponent(0.6)
        self.view_SlideShow.pageIndicator = pageControl

        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        self.view_SlideShow.activityIndicator = DefaultActivityIndicator()
        self.view_SlideShow.delegate = self

        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        self.view_SlideShow.setImageInputs(imageSource)

        self.view_SlideShow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureSlideshow)))
    }
    
    @objc func onTapGestureSlideshow() {
        //
    }
    
    // ImageSlideshowDelegate
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        
    }
    
    func onCountTimeAction(time: Int) {
        self.endDate = time
        self.timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { timer in
            DispatchQueue.main.async {
                self.endDate  -= 1
            }
        })
        
        RunLoop.main.add(self.timer!, forMode: .common)
    }
    
    func deinitTimer() {
        self.timer?.invalidate()
        self.timer = nil
    }
    
    func configUIFlashSale() {
        self.view_FlashSale.isHidden = true
    }
}
