//
//  CellCollectionViewPromotion.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//

import UIKit

class CellCollectionViewPromotion: UICollectionViewCell {

    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var img_Icon: UIImageView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var lbl_Content: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func config(strategies: StrategiesModel?) {
        self.view_Bg.backgroundColor = UIColor.init(hex: strategies?.backgroundColor ?? "ffffff")
        self.img_Icon.setImageWith(imageUrl: strategies?.image ?? "")
        self.lbl_Title.text = strategies?.title
        self.lbl_Content.text = strategies?.description
    }
}
