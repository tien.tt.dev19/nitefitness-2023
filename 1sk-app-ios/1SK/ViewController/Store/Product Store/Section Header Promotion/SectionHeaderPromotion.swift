//
//  SectionHeaderPromotion.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit

class SectionHeaderPromotion: UITableViewHeaderFooterView {
    
    @IBOutlet weak var coll_Promotion: UICollectionView!
    
    var listStrategies: [StrategiesModel] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setInitUICollectionView()
    }

    func onReloadData() {
        self.coll_Promotion.reloadData()
    }
}

// MARK: UICollectionViewDataSource
extension SectionHeaderPromotion: UICollectionViewDataSource, UICollectionViewDelegate {
    private func setInitUICollectionView() {
        self.coll_Promotion.registerNib(ofType: CellCollectionViewPromotion.self)
        
        self.coll_Promotion.dataSource = self
        self.coll_Promotion.delegate = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.estimatedItemSize = CGSize(width: 140, height: 90)
        self.coll_Promotion.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listStrategies.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewPromotion.self, for: indexPath)
        cell.config(strategies: self.listStrategies[indexPath.row])
        return cell
    }
}
