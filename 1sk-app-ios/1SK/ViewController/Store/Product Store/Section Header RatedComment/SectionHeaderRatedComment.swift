//
//  SectionHeaderRatedComment.swift
//  1SK
//
//  Created by Thaad on 10/03/2022.
//

import UIKit
import Cosmos

class SectionHeaderRatedComment: UITableViewHeaderFooterView {

    @IBOutlet weak var lbl_TotalReview: UILabel!
    @IBOutlet weak var lbl_TotalRate: UILabel!
    @IBOutlet weak var view_Rate: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func config(product: ProductModel) {
        self.lbl_TotalReview.text = "\(product.reviewsCount ?? 0) đánh giá\ntừ khách hàng"
        self.lbl_TotalRate.text = "\(product.reviewsAvg ?? 0)/5"
        self.view_Rate.rating = product.reviewsAvg ?? 0
    }
}
