//
//  CellTableViewHistorySearch.swift
//  1SK
//
//  Created by Thaad on 18/11/2022.
//

import UIKit

protocol CellTableViewHistorySearchDelegate: AnyObject {
    func onRemoveItemSearchAction(index: Int?, keyword: String?)
}

class CellTableViewHistorySearch: UITableViewCell {

    @IBOutlet weak var lbl_Text: UILabel!
    
    weak var delegate: CellTableViewHistorySearchDelegate?
    
    var index: Int?
    var keyword: String? {
        didSet {
            self.lbl_Text.text = self.keyword
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func onRemoveAction(_ sender: Any) {
        self.delegate?.onRemoveItemSearchAction(index: self.index, keyword: self.keyword)
    }
    
}
