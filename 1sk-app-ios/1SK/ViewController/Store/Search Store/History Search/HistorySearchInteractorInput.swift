//
//  
//  HistorySearchInteractorInput.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol HistorySearchInteractorInputProtocol {
    func getListHistorySeach()
    func setDeleteSearchHistory(index: Int, keyword: String)
}

// MARK: - Interactor Output Protocol
protocol HistorySearchInteractorOutputProtocol: AnyObject {
    func onGetListHistorySeachFinished(with result: Result<[String], APIError>)
    func onSetDeleteSearchHistoryFinished(with result: Result<EmptyModel, APIError>, index: Int)
}

// MARK: - HistorySearch InteractorInput
class HistorySearchInteractorInput {
    weak var output: HistorySearchInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - HistorySearch InteractorInputProtocol
extension HistorySearchInteractorInput: HistorySearchInteractorInputProtocol {
    func getListHistorySeach() {
        self.storeService?.getListHistorySeach(completion: { [weak self] result in
            self?.output?.onGetListHistorySeachFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func setDeleteSearchHistory(index: Int, keyword: String) {
        self.storeService?.setDeleteSearchHistory(by: keyword) { [weak self] result in
            self?.output?.onSetDeleteSearchHistoryFinished(with: result.unwrapSuccessModel(), index: index)
        }
    }
}
