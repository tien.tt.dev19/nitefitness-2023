//
//  
//  HistorySearchRouter.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol HistorySearchRouterProtocol {

}

// MARK: - HistorySearch Router
class HistorySearchRouter {
    weak var viewController: HistorySearchViewController?
    
    static func setupModule(delegate: HistorySearchViewDelegate?) -> HistorySearchViewController {
        let viewController = HistorySearchViewController()
        let router = HistorySearchRouter()
        let interactorInput = HistorySearchInteractorInput()
        let viewModel = HistorySearchViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewController.delegate = delegate
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - HistorySearch RouterProtocol
extension HistorySearchRouter: HistorySearchRouterProtocol {
    
}
