//
//  
//  HistorySearchViewController.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

protocol HistorySearchViewDelegate: AnyObject {
    func onDidSelectHistorySearch(keyword: String?)
}

// MARK: - ViewProtocol
protocol HistorySearchViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
}

// MARK: - HistorySearch ViewController
class HistorySearchViewController: BaseViewController {
    var router: HistorySearchRouterProtocol!
    var viewModel: HistorySearchViewModelProtocol!
    weak var delegate: HistorySearchViewDelegate?
    
    @IBOutlet weak var tbv_TableView: UITableView!
    @IBOutlet weak var view_NotData: UIStackView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITableView()
    }
    
    // MARK: - Action
    
}

// MARK: - HistorySearch ViewProtocol
extension HistorySearchViewController: HistorySearchViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.tbv_TableView.reloadData()
        
        if self.viewModel.listHistorySearch.count > 0 {
            self.view_NotData.isHidden = true
        } else {
            self.view_NotData.isHidden = false
        }
    }
}

// MARK: - UITableViewDataSource
extension HistorySearchViewController: UITableViewDataSource {
    func setInitUITableView() {
        self.tbv_TableView.registerNib(ofType: CellTableViewHistorySearch.self)
        
        self.tbv_TableView.dataSource = self
        self.tbv_TableView.delegate = self
    }
    
    // MARK: - UITableView - Cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.listHistorySearch.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: CellTableViewHistorySearch.self, for: indexPath)
        cell.delegate = self
        cell.index = indexPath.row
        cell.keyword = self.viewModel.listHistorySearch[indexPath.row]
        return cell
    }
}

// MARK: - UITableViewDelegate
extension HistorySearchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let keyword = self.viewModel.listHistorySearch[indexPath.row]
        self.delegate?.onDidSelectHistorySearch(keyword: keyword)
    }
}

// MARK: - CellTableViewHistorySearchDelegate
extension HistorySearchViewController: CellTableViewHistorySearchDelegate {
    func onRemoveItemSearchAction(index: Int?, keyword: String?) {
        self.viewModel.onRemoveItemSearchAction(index: index, keyword: keyword)
    }
}
