//
//  
//  HistorySearchViewModel.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol HistorySearchViewModelProtocol {
    func onViewDidLoad()
    func getHistorySeach()
    func onRemoveItemSearchAction(index: Int?, keyword: String?)
    
    var listHistorySearch: [String] { get set }
}

// MARK: - HistorySearch ViewModel
class HistorySearchViewModel {
    weak var view: HistorySearchViewProtocol?
    private var interactor: HistorySearchInteractorInputProtocol

    init(interactor: HistorySearchInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listHistorySearch: [String] = []
}

// MARK: - HistorySearch ViewModelProtocol
extension HistorySearchViewModel: HistorySearchViewModelProtocol {
    func onViewDidLoad() {
        self.view?.showHud()
    }
    
    func getHistorySeach() {
        
        self.interactor.getListHistorySeach()
    }
    
    func onRemoveItemSearchAction(index: Int?, keyword: String?) {
        guard let _index = index, _index >= 0 else {
            return
        }
        guard let _keyword = keyword, _keyword.count > 0 else {
            return
        }
        self.view?.showHud()
        self.interactor.setDeleteSearchHistory(index: _index, keyword: _keyword)
    }
}

// MARK: - HistorySearch InteractorOutputProtocol
extension HistorySearchViewModel: HistorySearchInteractorOutputProtocol {
    func onGetListHistorySeachFinished(with result: Result<[String], APIError>) {
        switch result {
        case .success(let model):
            self.listHistorySearch = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onSetDeleteSearchHistoryFinished(with result: Result<EmptyModel, APIError>, index: Int) {
        switch result {
        case .success:
            self.listHistorySearch.remove(at: index)
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
