//
//  
//  ProductSearchInteractorInput.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol ProductSearchInteractorInputProtocol {
    func getProductDetail(id: Int)
}

// MARK: - Interactor Output Protocol
protocol ProductSearchInteractorOutputProtocol: AnyObject {
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
}

// MARK: - ProductSearch InteractorInput
class ProductSearchInteractorInput {
    weak var output: ProductSearchInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - ProductSearch InteractorInputProtocol
extension ProductSearchInteractorInput: ProductSearchInteractorInputProtocol {
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
