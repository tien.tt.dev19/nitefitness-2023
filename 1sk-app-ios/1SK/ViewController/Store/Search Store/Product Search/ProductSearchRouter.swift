//
//  
//  ProductSearchRouter.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol ProductSearchRouterProtocol {
    func showProductStore(product: ProductModel?)
}

// MARK: - ProductSearch Router
class ProductSearchRouter {
    weak var viewController: ProductSearchViewController?
    
    static func setupModule() -> ProductSearchViewController {
        let viewController = ProductSearchViewController()
        let router = ProductSearchRouter()
        let interactorInput = ProductSearchInteractorInput()
        let viewModel = ProductSearchViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - ProductSearch RouterProtocol
extension ProductSearchRouter: ProductSearchRouterProtocol {
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
}
