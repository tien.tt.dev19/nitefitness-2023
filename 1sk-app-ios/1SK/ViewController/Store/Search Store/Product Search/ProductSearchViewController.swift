//
//  
//  ProductSearchViewController.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol ProductSearchViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onShowProductStore(product: ProductModel?)
}

// MARK: - ProductSearch ViewController
class ProductSearchViewController: BaseViewController {
    var router: ProductSearchRouterProtocol!
    var viewModel: ProductSearchViewModelProtocol!
    
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var coll_CollectionView: UICollectionView!
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUICollectionView()
    }
    
    // MARK: - Action
    
}

// MARK: - ProductSearch ViewProtocol
extension ProductSearchViewController: ProductSearchViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.lbl_Title.text = "Tìm thấy \(self.viewModel.listProductSearch.count) sản phẩm"
        self.coll_CollectionView.reloadData()
    }
    
    func onShowProductStore(product: ProductModel?) {
        self.router.showProductStore(product: product)
    }
}

// MARK: UICollectionViewDataSource
extension ProductSearchViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_CollectionView.registerNib(ofType: CellCollectionViewProduct.self)
        
        self.coll_CollectionView.dataSource = self
        self.coll_CollectionView.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
        layout.itemSize = CGSize(width: width, height: height)
        self.coll_CollectionView.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.listProductSearch.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
        cell.config(product: self.viewModel.listProductSearch[indexPath.row])
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension ProductSearchViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.onDidSelectItemAt(indexPath: indexPath)
    }
}
