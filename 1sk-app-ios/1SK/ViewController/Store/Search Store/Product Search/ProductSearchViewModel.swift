//
//  
//  ProductSearchViewModel.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol ProductSearchViewModelProtocol {
    func onViewDidLoad()
    func onDidSelectItemAt(indexPath: IndexPath)
    
    var listProductSearch: [ProductModel] { get set }
}

// MARK: - ProductSearch ViewModel
class ProductSearchViewModel {
    weak var view: ProductSearchViewProtocol?
    private var interactor: ProductSearchInteractorInputProtocol

    init(interactor: ProductSearchInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listProductSearch: [ProductModel] = []
}

// MARK: - ProductSearch ViewModelProtocol
extension ProductSearchViewModel: ProductSearchViewModelProtocol {
    func onViewDidLoad() {
        //
    }
    
    func onDidSelectItemAt(indexPath: IndexPath) {
        guard let productId = self.listProductSearch[indexPath.row].id else {
            return
        }
        self.view?.showHud()
        self.interactor.getProductDetail(id: productId)
    }
}

// MARK: - ProductSearch InteractorOutputProtocol
extension ProductSearchViewModel: ProductSearchInteractorOutputProtocol {
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onShowProductStore(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
