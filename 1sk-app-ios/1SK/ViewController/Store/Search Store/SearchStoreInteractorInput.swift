//
//  
//  SearchStoreInteractorInput.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol SearchStoreInteractorInputProtocol {
    func getListProductSearch(keyword: String, page: Int, perPage: Int)
}

// MARK: - Interactor Output Protocol
protocol SearchStoreInteractorOutputProtocol: AnyObject {
    func onGetListProductSearchFinished(with result: Result<[ProductModel], APIError>)
}

// MARK: - SearchStore InteractorInput
class SearchStoreInteractorInput {
    weak var output: SearchStoreInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - SearchStore InteractorInputProtocol
extension SearchStoreInteractorInput: SearchStoreInteractorInputProtocol {
    func getListProductSearch(keyword: String, page: Int, perPage: Int) {
        self.storeService?.getListProduct(by: keyword, page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductSearchFinished(with: result.unwrapSuccessModel())
        })
    }
}
