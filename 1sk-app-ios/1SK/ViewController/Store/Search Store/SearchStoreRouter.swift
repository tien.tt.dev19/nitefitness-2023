//
//  
//  SearchStoreRouter.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol SearchStoreRouterProtocol {

}

// MARK: - SearchStore Router
class SearchStoreRouter {
    weak var viewController: SearchStoreViewController?
    static func setupModule() -> SearchStoreViewController {
        let viewController = SearchStoreViewController()
        let router = SearchStoreRouter()
        let interactorInput = SearchStoreInteractorInput()
        let viewModel = SearchStoreViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.hidesBottomBarWhenPushed = true
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - SearchStore RouterProtocol
extension SearchStoreRouter: SearchStoreRouterProtocol {
    
}
