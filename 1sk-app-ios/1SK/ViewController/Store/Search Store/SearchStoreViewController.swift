//
//  
//  SearchStoreViewController.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit
import SnapKit

enum StateContainerSearch {
    case HISTORY
    case PRODUCT([ProductModel])
    case SUGGEST
}

// MARK: - ViewProtocol
protocol SearchStoreViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func setChangeStateUI()
}

// MARK: - SearchStore ViewController
class SearchStoreViewController: BaseViewController {
    var router: SearchStoreRouterProtocol!
    var viewModel: SearchStoreViewModelProtocol!
    
    @IBOutlet weak var tf_Search: UITextField!
    @IBOutlet weak var img_Search: UIImageView!
    
    @IBOutlet weak var view_Container: UIView!
    
    private lazy var historySearch = HistorySearchRouter.setupModule(delegate: self)
    private lazy var productSearch = ProductSearchRouter.setupModule()
    private lazy var suggestSearch = SuggestSearchRouter.setupModule()
    
    private var timeDelay: Double = 1
    private var timer: Timer?
    
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent //.default for black style
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUITextField()
        self.setStateChildViewContainer(state: .HISTORY)
    }
    
    // MARK: - Action
    @IBAction func handlePopViewController(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

// MARK: - SearchStore ViewProtocol
extension SearchStoreViewController: SearchStoreViewProtocol {
    func showHud() {
        self.showProgressHud()
    }

    func hideHud() {
        self.hideProgressHud()
    }
    
    func setChangeStateUI() {
        if let listProduct = self.viewModel.listProductSearch, listProduct.count > 0 {
            self.setStateChildViewContainer(state: .PRODUCT(listProduct))
            
        } else {
            self.setStateChildViewContainer(state: .SUGGEST)
        }
    }

}


// MARK: - UITextFieldDelegate
extension SearchStoreViewController: UITextFieldDelegate {
    func setInitUITextField() {
        self.tf_Search.delegate = self
        self.tf_Search.addTarget(self, action: #selector(self.textFieldEditingChanged(_:)), for: .editingChanged)
        self.img_Search.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onRemoveSearchAction(_:))))
    }
    
    @objc private func textFieldEditingChanged(_ sender: UITextField) {
        guard let text = sender.text else { return }
        if String.isNilOrEmpty(text) {
            self.img_Search.image = R.image.ic_search_store1()
            
        } else {
            self.img_Search.image = R.image.ic_cancel()
        }
    }
    
    @objc private func onRemoveSearchAction(_ sender: UITapGestureRecognizer) {
        if self.img_Search.image == R.image.ic_cancel() {
            self.tf_Search.text = ""
            self.tf_Search.becomeFirstResponder()
            
            self.img_Search.image = R.image.ic_search_store1()
            
            self.setStateChildViewContainer(state: .HISTORY)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard let keyword = textField.text, keyword.count > 0 else {
            SKToast.shared.showToast(content: "Bạn vui lòng nhập từ khóa tìm kiếm để tiếp tục")
            return false
        }
        self.tf_Search.resignFirstResponder()
        self.viewModel.onSearchProductAction(keyword: keyword)
        return true
    }
}

// MARK: - Init Container
extension SearchStoreViewController {
    private func setStateChildViewContainer(state: StateContainerSearch) {
        switch state {
        case .HISTORY:
            self.addChildView(childViewController: self.historySearch)
            self.historySearch.viewModel.getHistorySeach()
            
        case .PRODUCT(let listProduct):
            self.productSearch.viewModel.listProductSearch = listProduct
            self.addChildView(childViewController: self.productSearch)
            self.productSearch.onReloadData()
            
        case .SUGGEST:
            self.addChildView(childViewController: self.suggestSearch)
        }
    }
    
    private func addChildView(childViewController: UIViewController) {
        self.addChild(childViewController)
        childViewController.view.translatesAutoresizingMaskIntoConstraints = false
        self.view_Container.addSubview(childViewController.view)
        childViewController.view.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        childViewController.didMove(toParent: self)
    }
}

// MARK: HistorySearchViewDelegate
extension SearchStoreViewController: HistorySearchViewDelegate {
    func onDidSelectHistorySearch(keyword: String?) {
        guard let _keyword = keyword, _keyword.count > 0 else {
            return
        }
        self.tf_Search.text = _keyword
        self.tf_Search.resignFirstResponder()
        self.textFieldEditingChanged(self.tf_Search)
        self.viewModel.onSearchProductAction(keyword: _keyword)
    }
}
