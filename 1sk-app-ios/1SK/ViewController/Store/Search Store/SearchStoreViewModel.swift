//
//  
//  SearchStoreViewModel.swift
//  1SK
//
//  Created by Thaad on 09/03/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol SearchStoreViewModelProtocol {
    func onViewDidLoad()
    func onSearchProductAction(keyword: String)
    
    var listProductSearch: [ProductModel]? { get set }
}

// MARK: - SearchStore ViewModel
class SearchStoreViewModel {
    weak var view: SearchStoreViewProtocol?
    private var interactor: SearchStoreInteractorInputProtocol
    
    init(interactor: SearchStoreInteractorInputProtocol) {
        self.interactor = interactor
    }
    
    var listProductSearch: [ProductModel]?
    
}

// MARK: - SearchStore ViewModelProtocol
extension SearchStoreViewModel: SearchStoreViewModelProtocol {
    func onViewDidLoad() {
        //
    }
    
    func onSearchProductAction(keyword: String) {
        self.view?.showHud()
        self.interactor.getListProductSearch(keyword: keyword, page: 1, perPage: 100)
    }
    
}

// MARK: - SearchStore InteractorOutputProtocol
extension SearchStoreViewModel: SearchStoreInteractorOutputProtocol {
    func onGetListProductSearchFinished(with result: Result<[ProductModel], APIError>) {
        switch result {
        case .success(let model):
            self.listProductSearch = model
            self.view?.setChangeStateUI()
            
        case .failure:
            break
        }
        self.view?.hideHud()
    }
}
