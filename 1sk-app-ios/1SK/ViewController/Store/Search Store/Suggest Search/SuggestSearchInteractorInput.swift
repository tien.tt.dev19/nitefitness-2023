//
//  
//  SuggestSearchInteractorInput.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol SuggestSearchInteractorInputProtocol {
    func getListProductSuggest(page: Int, perPage: Int)
    func getProductDetail(id: Int)
}

// MARK: - Interactor Output Protocol
protocol SuggestSearchInteractorOutputProtocol: AnyObject {
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>)
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>)
}

// MARK: - SuggestSearch InteractorInput
class SuggestSearchInteractorInput {
    weak var output: SuggestSearchInteractorOutputProtocol?
    var storeService: StoreServiceProtocol?
}

// MARK: - SuggestSearch InteractorInputProtocol
extension SuggestSearchInteractorInput: SuggestSearchInteractorInputProtocol {
    func getListProductSuggest(page: Int, perPage: Int) {
        self.storeService?.getListProductSuggest(page: page, perPage: perPage, completion: { [weak self] result in
            self?.output?.onGetListProductSuggestFinished(with: result.unwrapSuccessModel())
        })
    }
    
    func getProductDetail(id: Int) {
        self.storeService?.getProductDetail(id: id, completion: { [weak self] result in
            self?.output?.onGetProductDetailFinished(with: result.unwrapSuccessModel())
        })
    }
}
