//
//  
//  SuggestSearchRouter.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol SuggestSearchRouterProtocol {
    func showProductStore(product: ProductModel?)
}

// MARK: - SuggestSearch Router
class SuggestSearchRouter {
    weak var viewController: SuggestSearchViewController?
    
    static func setupModule() -> SuggestSearchViewController {
        let viewController = SuggestSearchViewController()
        let router = SuggestSearchRouter()
        let interactorInput = SuggestSearchInteractorInput()
        let viewModel = SuggestSearchViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        interactorInput.output = viewModel
        interactorInput.storeService = StoreService()
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - SuggestSearch RouterProtocol
extension SuggestSearchRouter: SuggestSearchRouterProtocol {
    func showProductStore(product: ProductModel?) {
        let controller = ProductStoreRouter.setupModule(product: product)
        self.viewController?.show(controller, sender: nil)
    }
}
