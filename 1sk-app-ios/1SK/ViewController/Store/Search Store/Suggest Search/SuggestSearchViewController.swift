//
//  
//  SuggestSearchViewController.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - ViewProtocol
protocol SuggestSearchViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
    func onReloadData()
    func onShowProductStore(product: ProductModel?)
}

// MARK: - SuggestSearch ViewController
class SuggestSearchViewController: BaseViewController {
    var router: SuggestSearchRouterProtocol!
    var viewModel: SuggestSearchViewModelProtocol!
    
    @IBOutlet weak var coll_Suggest: UICollectionView!
    @IBOutlet weak var constraint_height_CollSuggest: NSLayoutConstraint!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitUICollectionView()
    }
    
    // MARK: - Action
    
}

// MARK: - SuggestSearch ViewProtocol
extension SuggestSearchViewController: SuggestSearchViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
    
    func onReloadData() {
        self.coll_Suggest.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.constraint_height_CollSuggest.constant = self.coll_Suggest.contentSize.height
        }
    }
    
    func onShowProductStore(product: ProductModel?) {
        self.router.showProductStore(product: product)
    }
}

// MARK: UICollectionViewDataSource
extension SuggestSearchViewController: UICollectionViewDataSource {
    private func setInitUICollectionView() {
        self.coll_Suggest.registerNib(ofType: CellCollectionViewProduct.self)
        
        self.coll_Suggest.dataSource = self
        self.coll_Suggest.delegate = self
        
        let width = (Constant.Screen.width - 48) / 2
        let height = width * (210/164)
        
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 16
        layout.minimumInteritemSpacing = 16
        layout.sectionInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        layout.itemSize = CGSize(width: width, height: height)
        self.coll_Suggest.collectionViewLayout = layout
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.listProductSuggest.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeuCell(ofType: CellCollectionViewProduct.self, for: indexPath)
        cell.config(product: self.viewModel.listProductSuggest[indexPath.row])
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension SuggestSearchViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewModel.onDidSelectItemAt(indexPath: indexPath)
    }
}
