//
//  
//  SuggestSearchViewModel.swift
//  1SK
//
//  Created by Thaad on 01/04/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol SuggestSearchViewModelProtocol {
    func onViewDidLoad()
    func onDidSelectItemAt(indexPath: IndexPath)
    
    var listProductSuggest: [ProductModel] { get set }
}

// MARK: - SuggestSearch ViewModel
class SuggestSearchViewModel {
    weak var view: SuggestSearchViewProtocol?
    private var interactor: SuggestSearchInteractorInputProtocol

    init(interactor: SuggestSearchInteractorInputProtocol) {
        self.interactor = interactor
    }

    var listProductSuggest: [ProductModel] = []
}

// MARK: - SuggestSearch ViewModelProtocol
extension SuggestSearchViewModel: SuggestSearchViewModelProtocol {
    func onViewDidLoad() {
        self.interactor.getListProductSuggest(page: 1, perPage: 6)
    }
    
    func onDidSelectItemAt(indexPath: IndexPath) {
        guard let productId = self.listProductSuggest[indexPath.row].id else {
            return
        }
        self.view?.showHud()
        self.interactor.getProductDetail(id: productId)
    }
}

// MARK: - SuggestSearch InteractorOutputProtocol
extension SuggestSearchViewModel: SuggestSearchInteractorOutputProtocol {
    func onGetListProductSuggestFinished(with result: Result<[ProductModel], APIError>) {
        switch result {
        case .success(let model):
            self.listProductSuggest = model
            self.view?.onReloadData()
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
    
    func onGetProductDetailFinished(with result: Result<ProductModel, APIError>) {
        switch result {
        case .success(let model):
            self.view?.onShowProductStore(product: model)
            
        case .failure(let error):
            debugPrint(error)
        }
        self.view?.hideHud()
    }
}
