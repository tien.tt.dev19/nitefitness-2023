//
//  AlertNetworkViewController.swift
//  1SK
//
//  Created by Thaad on 14/11/2022.
//

import UIKit

class AlertNetworkViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setInitNotificationCenterNetWork()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    @IBAction func onRetryAction(_ sender: Any) {
        if gIsInternetConnectionAvailable {
            self.setAnimationHidden()
            
        } else {
            SKToast.shared.showToast(content: "Chưa có kết nối mạng.\nVui lòng kiểm tra kết nối mạng của bạn.")
        }
    }
}

// MARK: - Animation
extension AlertNetworkViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn) {
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ContentView.constant = 0
            self.view.layoutIfNeeded()
        }
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            gIsShowAlertNetworkView = false
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Animation
extension AlertNetworkViewController {
    private func setInitNotificationCenterNetWork() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onInternetConnectionAvailable), name: .connectionAvailable, object: nil)
    }

    @objc func onInternetConnectionAvailable() {
        self.setAnimationHidden()
    }
}
