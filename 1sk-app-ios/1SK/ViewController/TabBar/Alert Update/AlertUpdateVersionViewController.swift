//
//  AlertUpdateVersionViewController.swift
//  1SK
//
//  Created by vuongbachthu on 10/31/21.
//

import UIKit

protocol AlertUpdateVersionViewDelegate: AnyObject {
    func onAlertUpdateAction()
}

class AlertUpdateVersionViewController: UIViewController {
    @IBOutlet weak var view_Bg: UIView!
    @IBOutlet weak var view_Content: UIView!
    @IBOutlet weak var constraint_bottom_ContentView: NSLayoutConstraint!
    
    @IBOutlet weak var lbl_Content: UILabel!
    
    @IBOutlet weak var stack_SkipUpdate: UIStackView!
    @IBOutlet weak var stack_RequiredUpdate: UIStackView!
    
    weak var delegate: AlertUpdateVersionViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitGestureView()
        self.onSetData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setAnimationShow()
    }
    
    func onSetData() {
        if gVersion.systemVersionNumber ?? 0 < gVersion.minVersionNumber ?? 0 {
            self.stack_SkipUpdate.isHidden = true
            self.stack_RequiredUpdate.isHidden = false
            
            self.lbl_Content.text = "Phiên bản 1SK hiện tại của bạn đã hết hỗ trợ. Bạn vui lòng cập nhật phiên bản mới nhất."
            
        } else if gVersion.systemVersionNumber ?? 0 < gVersion.latestVersionNumber ?? 0 {
            self.stack_SkipUpdate.isHidden = false
            self.stack_RequiredUpdate.isHidden = true
            
            self.lbl_Content.text = "Ứng dụng đã có phiên bản mới. Bạn vui lòng cập nhật để có trải nghiệm tốt nhất!"
        }
    }
    
    @IBAction func onSkipAction(_ sender: Any) {
        self.setAnimationHidden()
    }
    
    @IBAction func onUpdateAction(_ sender: Any) {
        self.delegate?.onAlertUpdateAction()
        if gVersion.systemVersionNumber ?? 0 < gVersion.minVersionNumber ?? 0 {
            return
        }
        //self.setAnimationHidden()
    }
}

// MARK: - Animation
extension AlertUpdateVersionViewController {
    func setAnimationShow() {
        self.view_Bg.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view_Bg.alpha = 0
        
        UIView.animate(withDuration: 0.5, animations: { () -> Void in
            self.view_Bg.alpha = 0.5
            self.view_Bg.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.constraint_bottom_ContentView.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    func setAnimationHidden() {
        UIView.animate(withDuration: 0.5) {
            self.view_Bg.alpha = 0.0
            self.constraint_bottom_ContentView.constant = -self.view_Content.height
            self.view.layoutIfNeeded()
            
        } completion: { _ in
            self.dismiss(animated: false, completion: nil)
        }
    }
}

// MARK: - Gesture View
extension AlertUpdateVersionViewController {
    func setInitGestureView() {
        self.view_Bg.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.onTapGestureAction)))
    }
    
    @objc func onTapGestureAction() {
        if gVersion.systemVersionNumber ?? 0 < gVersion.minVersionNumber ?? 0 {
            return
        }
        self.setAnimationHidden()
    }
}
