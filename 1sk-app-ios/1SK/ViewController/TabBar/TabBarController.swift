//
//  MainTabbarController.swift
//  1SK
//
//  Created by tuyenvx on 12/01/2021.
//

import UIKit

// MARK: UITabBarController
class TabBarController: UITabBarController {
    
    var exerciseService = ExerciseService()
    var isShowAlertUpdateVersionView = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setInitTabBarController()
        self.setInitNotificationCenter()
        self.setInitSocketIOConnection()
        //self.setVerifyPhoneNumber()
        
        self.setFetchRemoteConfig()
        self.setEnvironmentConfigure()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        let navigation = self.selectedViewController as? BaseNavigationController
        return navigation?.supportedInterfaceOrientations ?? .portrait
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    private func setInitSocketIOConnection() {
        SocketHelperCare.share.initConnectionSocket()
        SocketHelperTimer.share.initConnectionSocket()
        //SocketHelperFitness.share.initConnectionSocket()
        SocketHelperID.share.initConnectionSocket()
    }
    
    private func setVerifyPhoneNumber() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            if gUser?.mobileVerifiedAt == nil {
                let controller = VerifyPhoneRouter.setupModule()
                controller.modalPresentationStyle = .fullScreen
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    private func setShadowTabBar() {
        self.tabBar.layer.shadowColor = UIColor.lightGray.cgColor
        self.tabBar.layer.shadowOpacity = 0.5
        self.tabBar.layer.shadowOffset = CGSize.zero
        self.tabBar.layer.shadowRadius = 5
        self.tabBar.layer.borderColor = UIColor.clear.cgColor
        self.tabBar.layer.borderWidth = 0
        self.tabBar.clipsToBounds = false
        self.tabBar.backgroundColor = UIColor.white
    }
    
    // MARK: Init
    private func setInitTabBarController() {
        self.tabBar.layer.borderColor = R.color.line_2()!.cgColor
        self.tabBar.layer.borderWidth = 0.5
        
        UITabBar.appearance().shadowImage = UIImage()
        UITabBar.appearance().backgroundImage = UIImage()
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().backgroundColor = .white
        UITabBar.appearance().tintColor = R.color.mainColor()
        UITabBar.appearance().unselectedItemTintColor = UIColor.init(hex: "8A8A8A")
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().barStyle = .default
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .regular)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .semibold)], for: .selected)
        
        // MARK: Page 0
        let tab_0 = HomeAppRouter.setupModule()
        tab_0.tabBarItem = TabbarItem.HOME.item
        
        let navigationTab_0 = BaseNavigationController(rootViewController: tab_0)
        navigationTab_0.setHiddenNavigationBarViewControllers([HomeAppViewController.self,
                                                               BlogDetailViewController.self,
                                                               PlayVideoViewController.self,
                                                               PlayVideoFullViewController.self,
                                                               PlayVideoFullFitViewController.self,
                                                               MyWorkoutViewController.self,
                                                               DoExerciseViewController.self,
                                                               FinishWorkoutViewController.self,
                                                               DetailAppointmentViewController.self,
                                                               DoctorDetailViewController.self,
                                                               DetailRacingViewController.self,
                                                               HealthRecordsViewController.self,
                                                               ProductStoreViewController.self,
                                                               MainProfileViewController.self,
                                                               CategoryStoreViewController.self,
                                                               CartStoreViewController.self,
                                                               DeliveryInformationViewController.self,
                                                               PaymentSuccessViewController.self,
                                                               CartStoreViewController.self,
                                                               PaymentStoreViewController.self,
                                                               OrderDetailViewController.self])
        
        let tabFitness = CategoryVideoFitRouter.setupModule()
        tabFitness.tabBarItem = TabbarItem.FITNESS.item
        
        let navigationTab_fitness = BaseNavigationController(rootViewController: tabFitness)
        navigationTab_fitness.setHiddenNavigationBarViewControllers([VideoFitDetailPlayerViewController.self,
                                                                     PlayVideoFullFitViewController.self])
        
        // MARK: Page 1
        let tab_1 = HomeHealthyRouter.setupModule()
        tab_1.tabBarItem = TabbarItem.HEALTHY.item
        
        let navigationTab_1 = BaseNavigationController(rootViewController: tab_1)
        navigationTab_1.setHiddenNavigationBarViewControllers([HomeFitViewController.self,
                                                               PracticeViewController.self,
                                                               MyWorkoutViewController.self,
                                                               EditListExerciseViewController.self,
                                                               DoExerciseViewController.self,
                                                               FinishWorkoutViewController.self,
                                                               SearchFitViewController.self,
                                                               BikeWorkoutViewController.self,
                                                               WorkoutDetailViewController.self,
                                                               BicycleResultViewController.self,
                                                               HomeFitContainerViewController.self,
                                                               VideoFitDetailPlayerViewController.self,
                                                               DetailRacingViewController.self,
                                                               PlayVideoViewController.self,
                                                               PlayVideoFullViewController.self,
                                                               PlayVideoFullFitViewController.self,
                                                               DetailAppointmentViewController.self,
                                                               DoctorDetailViewController.self,
                                                               HomeHealthyViewController.self])
        
        
        // MARK: Page 2
        let tab_2 = HomeStoreRouter.setupModule()
        tab_2.tabBarItem = TabbarItem.STORE.item
        
        let navigationTab_2 = BaseNavigationController(rootViewController: tab_2)
        navigationTab_2.setHiddenNavigationBarViewControllers([HomeStoreLandingViewController.self,
                                                               HomeStoreViewController.self,
                                                               CategoryStoreViewController.self,
                                                               ProductStoreViewController.self,
                                                               SearchStoreViewController.self,
                                                               PaymentSuccessViewController.self,
                                                               CartStoreViewController.self,
                                                               PaymentStoreViewController.self,
                                                               DeliveryInformationViewController.self,
                                                               OrderDetailViewController.self])
        
        // MARK: Page 3
        let tab_3 = HomeMessageRouter.setupModule()
        tab_3.tabBarItem = TabbarItem.MESSAGE.item
        
        let navigationTab_3 = BaseNavigationController(rootViewController: tab_3)
        navigationTab_3.setHiddenNavigationBarViewControllers([CareViewController.self,
                                                               HealthRecordsViewController.self,
                                                               PhotoPickerViewController.self,
                                                               BlogDetailViewController.self,
                                                               PlayVideoViewController.self,
                                                               PlayVideoFullViewController.self,
                                                               PlayVideoFullFitViewController.self,
                                                               DetailAppointmentViewController.self,
                                                               DoctorDetailViewController.self,
                                                               HomeMessageViewController.self])
        
        // MARK: Page 4
        let tab_4 = MeRouter.setupModule()
        tab_4.tabBarItem = TabbarItem.PROFILE.item
        
        let navigationTab_4 = BaseNavigationController(rootViewController: tab_4)
        navigationTab_4.setHiddenNavigationBarViewControllers([MeViewController.self,
                                                               DetailAppointmentViewController.self,
                                                               OrderDetailViewController.self,
                                                               UpdateProfileViewController.self])
        
        // MARK:  Add Tabbar
//        self.viewControllers = [navigationTab_0, navigationTab_1, navigationTab_2, navigationTab_3, navigationTab_4 ]
        self.viewControllers = [navigationTab_1, navigationTab_fitness, navigationTab_2, navigationTab_3, navigationTab_4 ]
        self.delegate = self
        gTabBarController = self
    }
}

// MARK: UITabBarControllerDelegate
extension TabBarController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        switch viewController.tabBarItem.tag {
        case 1:
            return true
            
        case 2:
            guard Configure.shared.isLogged() == true else {
                self.presentLoginRouter()
                return false
            }
            return true
        
        case 3:
            return true
            
        case 4:
            guard Configure.shared.isLogged() == true else {
                self.presentLoginRouter()
                return false
            }
            return true
            
        case 5:
            guard Configure.shared.isLogged() == true else {
                self.presentLoginRouter()
                return false
            }
            return true
            
        default:
            return true
        }
    }
}

// MARK: - LoginViewDelegate
extension TabBarController: LoginViewDelegate {
    func presentLoginRouter() {
        let controller = LoginRouter.setupModule(delegate: self)
        controller.modalPresentationStyle = .custom
        self.present(controller, animated: true)
    }
    
    func onLoginSuccess() {
        SKToast.shared.showToast(content: "Đăng nhập thành công")
    }
}

// MARK: NotificationCenter
extension TabBarController {
    func setInitNotificationCenter() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.onTokenExpire), name: .tokenExpire, object: nil)
    }
    
    @objc func onTokenExpire() {
        guard KeyChainManager.shared.accessToken != nil else {
            return
        }
        self.onLogoutAction()
    }
    
    func onLogoutAction() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.onHandleUserLogout()
        }
    }
}

// MARK: AlertUpdateVersionViewDelegate
extension TabBarController: AlertUpdateVersionViewDelegate {
    private func setFetchRemoteConfig() {
        RemoteConfigManager.shared.fetchRemoteConfig { success, error in
            //print("<version> fetchRemoteConfig: success: \(success) - error: \(error.debugDescription)")
            if success {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.onCheckUpdateVersion()
                }
            }
        }
    }
    
    private func onCheckUpdateVersion() {
        guard APP_ENV == .PRO else {
            return
        }
        guard self.isShowAlertUpdateVersionView == true else {
            return
        }
        guard let latestVersionNumber = gVersion.latestVersionNumber else {
            return
        }
        guard latestVersionNumber > gVersion.systemVersionNumber ?? 0 else {
            return
        }
        
        self.isShowAlertUpdateVersionView = false
        let controller = AlertUpdateVersionViewController()
        controller.modalPresentationStyle = .custom
        controller.delegate = self
        UIApplication.shared.visibleViewController?.present(controller, animated: false)
    }
    
    func onAlertUpdateAction() {
        if let url = URL(string: APP_APPSTORE), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}

extension TabBarController {
    func setEnvironmentConfigure() {
        #if STAGING_DEBUG || STAGING_RELEASE
            print("Environment: STAGING")

        #elseif DEVELOP_DEBUG || DEVELOP_RELEASE
            print("Environment: DEVELOP")

        #elseif PRODUCT_DEBUG || PRODUCT_RELEASE
            print("Environment: PRODUCT")
        
        #else // OTHER
            print("Environment: OTHER")

        #endif
    }
}
