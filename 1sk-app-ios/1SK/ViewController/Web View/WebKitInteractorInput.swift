//
//  
//  WebKitInteractorInput.swift
//  1SK
//
//  Created by Thaad on 23/12/2022.
//
//

import UIKit

// MARK: - Interactor Input Protocol
protocol WebKitInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int)
    func set_DataNameFunction(param1: String, param2: Int)
}

// MARK: - Interactor Output Protocol
protocol WebKitInteractorOutputProtocol: AnyObject {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>)
}

// MARK: - WebKit InteractorInput
class WebKitInteractorInput {
    weak var output: WebKitInteractorOutputProtocol?
}

// MARK: - WebKit InteractorInputProtocol
extension WebKitInteractorInput: WebKitInteractorInputProtocol {
    func get_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
    
    func set_DataNameFunction(param1: String, param2: Int) {
        // Connect API
        // Data output
    }
}
