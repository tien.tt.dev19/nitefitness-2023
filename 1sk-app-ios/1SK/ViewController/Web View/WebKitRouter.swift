//
//  
//  WebKitRouter.swift
//  1SK
//
//  Created by Thaad on 23/12/2022.
//
//

import UIKit

// MARK: - RouterProtocol
protocol WebKitRouterProtocol {
    func onDismiss()
}

// MARK: - WebKit Router
class WebKitRouter {
    weak var viewController: WebKitViewController?
    
    static func setupModule(title: String?, url: String?) -> WebKitViewController {
        let viewController = WebKitViewController()
        let router = WebKitRouter()
        let interactorInput = WebKitInteractorInput()
        let viewModel = WebKitViewModel(interactor: interactorInput)
        
        viewController.viewModel = viewModel
        viewController.router = router
        viewModel.view = viewController
        viewModel.title = title
        viewModel.url = url
        interactorInput.output = viewModel
        router.viewController = viewController
        
        return viewController
    }
}

// MARK: - WebKit RouterProtocol
extension WebKitRouter: WebKitRouterProtocol {
    func onDismiss() {
        self.viewController?.dismiss(animated: true)
    }
}
