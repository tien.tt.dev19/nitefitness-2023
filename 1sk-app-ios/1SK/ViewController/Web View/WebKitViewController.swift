//
//  
//  WebKitViewController.swift
//  1SK
//
//  Created by Thaad on 23/12/2022.
//
//

import UIKit
import WebKit

// MARK: - ViewProtocol
protocol WebKitViewProtocol: AnyObject {
    func showHud()
    func hideHud()
    
}

// MARK: - WebKit ViewController
class WebKitViewController: BaseViewController {
    var router: WebKitRouterProtocol!
    var viewModel: WebKitViewModelProtocol!
    
    
    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var lbl_Title: UILabel!
    @IBOutlet weak var web_WebView: WKWebView!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupInit()
        self.viewModel.onViewDidLoad()
    }
    
    // MARK: - Init
    private func setupInit() {
        self.setInitLayout()
        self.setInitWKWebView(url: self.viewModel.url)
    }
    
    func setInitLayout() {
        self.lbl_Title.text = self.viewModel.title
        self.view_Nav.addShadow(width: 0, height: 6, color: .black, radius: 3, opacity: 0.05)
    }
    
    // MARK: - Action
    @IBAction func onDismissAction(_ sender: Any) {
        self.router.onDismiss()
    }
}

// MARK: - WebKit ViewProtocol
extension WebKitViewController: WebKitViewProtocol {
    func showHud() {
        self.showProgressHud()
    }
    
    func hideHud() {
        self.hideProgressHud()
    }
}

// MARK: - WKNavigationDelegate
extension WebKitViewController: WKNavigationDelegate, WKUIDelegate {
    func setInitWKWebView(url: String?) {
        //self.web_WebView.scrollView.showsHorizontalScrollIndicator = false
        //self.web_WebView.scrollView.pinchGestureRecognizer?.isEnabled = false
        
        self.web_WebView.navigationDelegate = self
        self.web_WebView.uiDelegate = self
        self.web_WebView.allowsBackForwardNavigationGestures = true
        
        self.web_WebView.isOpaque = false
        self.web_WebView.backgroundColor = UIColor.clear
        self.web_WebView.scrollView.backgroundColor = UIColor.clear
        
        self.web_WebView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        self.web_WebView.addObserver(self, forKeyPath: #keyPath(WKWebView.title), options: .new, context: nil)
        
        if #available(iOS 14.0, *) {
            let preferences = WKWebpagePreferences()
            preferences.allowsContentJavaScript = true
            self.web_WebView.configuration.defaultWebpagePreferences = preferences
            
        } else {
            self.web_WebView.configuration.preferences.javaScriptEnabled = true
            /// user interaction없이 윈도우 창을 열 수 있는지 여부를 나타냄. iOS에서는 기본값이 false이다.
            /// Indicates whether a window can be opened without user interaction. On iOS, the default is false.
            self.web_WebView.configuration.preferences.javaScriptCanOpenWindowsAutomatically = true
        }
        
        if let _url = URL(string: url ?? "") {
            self.showHud()
            self.web_WebView.load(URLRequest(url: _url))
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey: Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            print("<webView> observeValue estimatedProgress:", Float(self.web_WebView.estimatedProgress))
        }
        if keyPath == "title" {
            if let title = self.web_WebView.title {
                print("<webView> observeValue title:", title)
            }
        }
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if let url = navigationAction.request.url {
            print("<webView> decidePolicyFor url:", url)
        }
        
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        NSLog("<webView> didFinish")
        self.hideHud()
        self.web_WebView.evaluateJavaScript("document.readyState", completionHandler: { (complete, error) in
            if complete != nil {
                self.web_WebView.evaluateJavaScript("document.body.scrollHeight", completionHandler: { (height, error) in
                    print("<webView> didFinish evaluateJavaScript")
                })
            }
        })
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("<webView> didFailProvisionalNavigation did fail provisional navigation %@", error as NSError)
    }


    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("<webView> didFail did fail navigation %@", error as NSError)
    }
    
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
        let ac = UIAlertController(title: "Hey, listen!", message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(ac, animated: true)
        completionHandler()
    }
}
