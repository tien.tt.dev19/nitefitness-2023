//
//  
//  WebKitViewModel.swift
//  1SK
//
//  Created by Thaad on 23/12/2022.
//
//

import UIKit

// MARK: - ViewModelProtocol
protocol WebKitViewModelProtocol {
    func onViewDidLoad()
    
    var title: String? { get set }
    var url: String? { get set }
}

// MARK: - WebKit ViewModel
class WebKitViewModel {
    weak var view: WebKitViewProtocol?
    private var interactor: WebKitInteractorInputProtocol

    init(interactor: WebKitInteractorInputProtocol) {
        self.interactor = interactor
    }

    var title: String?
    var url: String?
}

// MARK: - WebKit ViewModelProtocol
extension WebKitViewModel: WebKitViewModelProtocol {
    func onViewDidLoad() {
        // Begin functions
    }
}

// MARK: - WebKit InteractorOutputProtocol
extension WebKitViewModel: WebKitInteractorOutputProtocol {
    func onGet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
            // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
    
    func onSet_DataNameFunction_Finished(with result: Result<EmptyModel, APIError>) {
        switch result {
        case .success(let model):
                // Handle Data
            break
            
        case .failure(let error):
            debugPrint(error)
        }
    }
}
