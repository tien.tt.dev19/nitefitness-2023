//
//  
//  WebviewSlideConstract.swift
//  1SK
//
//  Created by vuongbachthu on 7/30/21.
//
//

import UIKit

// MARK: - View
protocol WebviewSlideViewProtocol: AnyObject {
    func onViewDidLoad(url: String, title: String)
}

// MARK: - Presenter
protocol WebviewSlidePresenterProtocol {
    func onViewDidLoad()
    
    func onShowHub()
    func onHiddenHub()
}

// MARK: - Interactor Input
protocol WebviewSlideInteractorInputProtocol {

}

// MARK: - Interactor Output
protocol WebviewSlideInteractorOutputProtocol: AnyObject {
    
}

// MARK: - Router
protocol WebviewSlideRouterProtocol:BaseRouterProtocol {

}
