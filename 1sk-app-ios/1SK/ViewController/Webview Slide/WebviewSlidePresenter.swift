//
//  
//  WebviewSlidePresenter.swift
//  1SK
//
//  Created by vuongbachthu on 7/30/21.
//
//

import UIKit

class WebviewSlidePresenter {

    weak var view: WebviewSlideViewProtocol?
    private var interactor: WebviewSlideInteractorInputProtocol
    private var router: WebviewSlideRouterProtocol

    var urlRequest = DEFAULT_URL
    var title = ""
    
    init(interactor: WebviewSlideInteractorInputProtocol,
         router: WebviewSlideRouterProtocol) {
        self.interactor = interactor
        self.router = router
    }

}

// MARK: - WebviewSlidePresenterProtocol
extension WebviewSlidePresenter: WebviewSlidePresenterProtocol {
    func onViewDidLoad() {
        self.view?.onViewDidLoad(url: self.urlRequest, title: self.title)
    }
    
    func onShowHub() {
        self.router.showHud()
    }
    
    func onHiddenHub() {
        self.router.hideHud()
    }
}

// MARK: - WebviewSlideInteractorOutput 
extension WebviewSlidePresenter: WebviewSlideInteractorOutputProtocol {

}
