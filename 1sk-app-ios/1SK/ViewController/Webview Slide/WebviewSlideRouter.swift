//
//  
//  WebviewSlideRouter.swift
//  1SK
//
//  Created by vuongbachthu on 7/30/21.
//
//

import UIKit

class WebviewSlideRouter: BaseRouter {
//    weak var viewController: WebviewSlideViewController?
    static func setupModule(with url: String, title: String) -> WebviewSlideViewController {
        let viewController = WebviewSlideViewController()
        let router = WebviewSlideRouter()
        let interactorInput = WebviewSlideInteractorInput()
        let presenter = WebviewSlidePresenter(interactor: interactorInput, router: router)
        viewController.presenter = presenter
        presenter.view = viewController
        presenter.urlRequest = url
        presenter.title = title
        interactorInput.output = presenter
        router.viewController = viewController
        return viewController
    }
}

// MARK: - WebviewSlideRouterProtocol
extension WebviewSlideRouter: WebviewSlideRouterProtocol {
    
}
