//
//  
//  WebviewSlideViewController.swift
//  1SK
//
//  Created by vuongbachthu on 7/30/21.
//
//

import UIKit
import WebKit

class WebviewSlideViewController: BaseViewController {

    @IBOutlet weak var view_Nav: UIView!
    @IBOutlet weak var web_WebView: WKWebView!
    @IBOutlet weak var lbl_Title: UILabel!
    
    var presenter: WebviewSlidePresenterProtocol!
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initSetup()
        self.presenter.onViewDidLoad()
    }
    
    // MARK: - Setup
    private func initSetup() {
        self.view_Nav.addShadow(width: 0, height: 4, color: .black, radius: 4, opacity: 0.1)
    }
    
    func setInitWKWebView(urlRequest: String) {
        self.web_WebView.scrollView.showsHorizontalScrollIndicator = false
        self.web_WebView.scrollView.pinchGestureRecognizer?.isEnabled = false
        self.web_WebView.navigationDelegate = self
        if let url = URL(string: urlRequest) {
            self.web_WebView.load(URLRequest(url: url))
        }
    }
    
    // MARK: - Action
    @IBAction func onCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - WebviewSlideViewProtocol
extension WebviewSlideViewController: WebviewSlideViewProtocol {
    func onViewDidLoad(url: String, title: String) {
        self.lbl_Title.text = title
        self.setInitWKWebView(urlRequest: url)
    }
}

// MARK: - WebviewViewProtocol
extension WebviewSlideViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.presenter.onHiddenHub()
    }
}
